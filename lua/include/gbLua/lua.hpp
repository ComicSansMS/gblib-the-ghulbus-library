/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_LUA_INCLUDE_GUARD_LUA_HPP_
#define GB_LUA_INCLUDE_GUARD_LUA_HPP_

#include <gbLua/config.hpp>

#include <string>
#include <vector>

#include <boost/utility.hpp>

#include <gbLua/lua_exception.hpp>
#include <gbLua/lua_value.hpp>

extern "C" {
    struct lua_State;
}

/** Lua scripting module
 */
namespace GB_LUA_NAMESPACE {

    /** A Lua VM
     */
    class gbLua: boost::noncopyable {
    public:
        /** C Function callable from Lua
         */
        typedef int (*CFunction)(lua_State*);
        /** Info struct for loading libraries via gbLua::OpenLibrary() or gbLua::PreloadLibrary()
         */
        struct LibraryRegisterInfo {
            std::string name;
            CFunction func;
        public:
            LibraryRegisterInfo(std::string const& lib_name, CFunction lib_func):name(lib_name), func(lib_func) {};
        };
        /** Identifier for Lua standard library modules
         */
        enum StandardLibraryModule_T {
            LUA_STDLIB_BASE,
            LUA_STDLIB_BIT32,
            LUA_STDLIB_COROUTINE,
            LUA_STDLIB_DEBUG,
            LUA_STDLIB_IO,
            LUA_STDLIB_MATH,
            LUA_STDLIB_OS,
            LUA_STDLIB_PACKAGE,
            LUA_STDLIB_STRING,
            LUA_STDLIB_TABLE
        };
        /** Info struct for loading standard libraries via gbLua::OpenStandardLibrary
         */
        struct StandardLibraryRegisterInfo {
            std::string name;
            StandardLibraryModule_T module;
        public:
            StandardLibraryRegisterInfo(StandardLibraryModule_T lib_mod) : module(lib_mod) {}
            StandardLibraryRegisterInfo(std::string const& lib_name, StandardLibraryModule_T lib_mod)
                :name(lib_name), module(lib_mod) {}
        };
    private:
        lua_State* m_Lua;                                        ///< the underlying Lua state
        std::vector<LibraryRegisterInfo> m_LoadedLibs;           ///< libraries loaded and ready for use
        std::vector<LibraryRegisterInfo> m_PreloadedLibs;        ///< libraries preloaded and available via require()
    public:
        /** Constructor
         * @throw Exceptions::LuaRuntimeError
         */
        gbLua();
        /** Constructor
         * @param[in] file Path to a lua script file that will be executed after construction
         * @throw Exceptions::LuaRuntimeError Exceptions::LuaSyntaxError
         */
        gbLua(std::string const& file);
        /** Destructor
         */
        ~gbLua();
        /** Load a module from the Lua standard library.
         * gbLua by default loads the base, table, string, math and bit32 modules.
         * All other modules have to be loaded explicitly by calling this function.
         * @param[in] lib The library to load.
         */
        void OpenStandardLibrary(StandardLibraryRegisterInfo const& lib_info);
        /** Load a library of C functions into Lua
         */
        void OpenLibrary(LibraryRegisterInfo const& lib_info);
        /** Make a library of C functions available to Lua's require()
         */
        void PreloadLibrary(LibraryRegisterInfo const& lib_info);
        /** Run a lua script from file
         * @throw Exceptions::LuaRuntimeError Exceptions::LuaSyntaxError
         */
        void RunFile(std::string const& file);
        /** Run a lua script from string
         * @throw Exceptions::LuaRuntimeError Exceptions::LuaSyntaxError
         */
        void RunString(std::string const& str);
        /** Run a lua script from string
         * @throw Exceptions::LuaRuntimeError Exceptions::LuaSyntaxError
         */
        void RunString(char const* str);
        /** Retrieve a global variable
         * @param[in] name Identifier of a global Lua variable
         * @return The requested variable; Will be Nil if the variable was not found
         */
        gbLuaValuePtr GetGlobal(std::string const& name);
        /** Retrieve a global variable
         * @param[in] name Identifier of a global Lua variable
         * @return The requested variable; Will be Nil if the variable was not found
         */
        gbLuaValuePtr GetGlobal(char const* name);
        /** Set a global variable
         */
        void SetGlobal(std::string const& name, gbLuaValue const& val);
        /** Set a global variable
         */
        void SetGlobal(char const* name, gbLuaValue const& val);
        /** Set a global variable to a string value
         */
        void SetGlobal(std::string const& name, std::string const& val_str);
        /** Set a global variable to a string value
         */
        void SetGlobal(char const* name, std::string const& val_str);
        /** Set a global variable to a string value
         */
        void SetGlobal(std::string const& name, char const* val_str);
        /** Set a global variable to a string value
         */
        void SetGlobal(char const* name, char const* val_str);
        /** Set a global variable to a number value
         */
        void SetGlobal(std::string const& name, double val_f);
        /** Set a global variable to a number value
         */
        void SetGlobal(char const* name, double val_f);
        /** Create a new table under the given name
         */
        TablePtr CreateTable(std::string const& name);
        /** Create a new table under the given name
         */
        TablePtr CreateTable(char const* name);
        /** Create a new anonymous table
         */
        TablePtr CreateTable();
    private:
        void Init();
    };

}

#endif


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_LUA_INCLUDE_GUARD_STACK_GUARD_HPP_
#define GB_LUA_INCLUDE_GUARD_STACK_GUARD_HPP_

#include <gbLua/config.hpp>

#include <gbLua/lua_exception.hpp>

#include <boost/utility.hpp>

extern "C" {
    struct lua_State;
}

namespace GB_LUA_NAMESPACE {

    /** Restores stack to initial size upon destruction
     */
    class StackGuard: boost::noncopyable {
    private:
        lua_State* const m_Lua;            ///< underlying lua vm
        int m_TargetStackSize;            ///< target stack size after destruction
        bool m_DoGuard;
    public:
        /** Constructor
         * Causes the stack to be capped to the same size it has upon construction;
         * Use the two parameter constructor if the stack size is supposed to change
         */
        StackGuard(lua_State* lua);
        /** Constructor
         * Causes the stack to be capped to the current size + offset
         */
        StackGuard(lua_State* lua, int offset);
        /** Destructor
         * Pops the required number of elements to reach the requested target size
         * @attention If the stack has shrunk below the target size, a message will
         *            be logged via DIAG_LOG's warning stream
         */
        ~StackGuard();
        /** Release the StackGuard
         * Once released, the StackGuard Destructor does nothing at all
         */
        void Release();
    };

}

#endif


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_LUA_INCLUDE_GUARD_STACK_MONITOR_HPP_
#define GB_LUA_INCLUDE_GUARD_STACK_MONITOR_HPP_

#include <gbLua/config.hpp>
#include <gbLua/lua_exception.hpp>

#include <string>

#include <boost/utility.hpp>

extern "C" {
    struct lua_State;
}

namespace GB_LUA_NAMESPACE {

    /** Monitors Lua stack sizes for debugging purposes
     * @note gbLua uses StackMonitor for its internal operations if
     *       the GB_LUA_DIAGNOSTIC_STACK_CONSISTENCY_CHECKS_ENABLED
     *       preprocessor flag is set (default for Debug builds).
     */
    class StackMonitor: boost::noncopyable {
    private:
        lua_State* const m_Lua;            ///< underlying lua vm
        int const m_ExpectedStackValue;    ///< expected stack size upon destruction
        bool m_DoCheck;                    ///< true iff the monitor was not released
        std::string const m_Func;        ///< function name
    public:
        /** Constructor
         * Expects the stack size to have changed by expected_stack_change by its destruction
         */
        StackMonitor(lua_State* lua, int expected_stack_change);
        /** Constructor
         * Expects the stack size to have changed by expected_stack_change by its destruction
         */
        StackMonitor(lua_State* lua, int expected_stack_change, char const* function_name);
        /** Destructor
         * Checks the stack size and sends a message to
         * DIAG_LOG's warning stream if it is off.
         */
        ~StackMonitor();
        /** Release the monitor
         * Once released, the StackMonitor destructor does nothing at all.
         */
        void Release();
    };

}

#endif


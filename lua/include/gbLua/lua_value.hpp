/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#ifndef GB_LUA_INCLUDE_GUARD_LUA_VALUE_HPP_
#define GB_LUA_INCLUDE_GUARD_LUA_VALUE_HPP_

#include <gbLua/config.hpp>

#include <iterator>
#include <memory>
#include <iosfwd>
#include <string>
#include <vector>

#include <boost/static_assert.hpp>
#include <boost/utility.hpp>

extern "C" {
    struct lua_State;
}

namespace GB_LUA_NAMESPACE {

    /** Lua Types
     */
    enum LUA_TYPE {
        LUA_TYPE_NIL,            ///< nil
        LUA_TYPE_BOOLEAN,        ///< boolean
        LUA_TYPE_NUMBER,        ///< number
        LUA_TYPE_STRING,        ///< string
        LUA_TYPE_FUNCTION,        ///< function
        LUA_TYPE_USERDATA,        ///< userdata
        LUA_TYPE_THREAD,        ///< thread
        LUA_TYPE_TABLE            ///< table
    };

    /** Base class for storing Lua values
     */
    class gbLuaValue;
    /** Smart Pointer to gbLuaValue
     */
    typedef std::unique_ptr<gbLuaValue> gbLuaValuePtr;

    /** Converts a LUA_T* value from lua.h to a LUA_TYPE
     */
    LUA_TYPE ConvertLuaType(int t);

    /** Check if a gbLuaValue is of type LUA_TYPE_NIL
     */
    bool IsNil(gbLuaValue const& v);

    /** @defgroup lua_value Lua Value Types
     * @{
     */
    class gbLuaValue: boost::noncopyable {
    public:
        /** Destructor
         */
        virtual ~gbLuaValue() {}
        /** Retrieve the actual type of the value
         */
        virtual LUA_TYPE GetType() const=0;
        /** Polymorphic constructor
         */
        virtual gbLuaValuePtr Clone() const=0;
        /** Automated conversion to gbLuaValuePtr
         * @note Conversion actually clones the underlying gbLuaValuePtr,
         *       which requires allocating a new gbLuaValue on the heap
         *       and performing a potentially costly copy operation.
         *       The main motivation for this function is easy
         *       assignment to gbLuaValuePtr, e.g. when assigning to
         *       table iterators. Be aware of the potential performance
         *       hit though!
         */
        operator gbLuaValuePtr() const { return Clone(); }
    };

    /** nil Value
     */
    class Nil: public gbLuaValue {
    public:
        Nil();
        ~Nil();
        /** @name gbLuaValue
         * @{
         */
        LUA_TYPE GetType() const;
        gbLuaValuePtr Clone() const;
        /// @}
    };

    /** boolean Value
     */
    class Boolean: public gbLuaValue {
    private:
        bool b_;            ///< storage
    public:
        Boolean(bool b);
        ~Boolean();
        /** @name gbLuaValue
         * @{
         */
        LUA_TYPE GetType() const;
        gbLuaValuePtr Clone() const;
        /// @}
        bool Get() const;
        void Set(bool b);
        Boolean(Boolean const& rhs);
        Boolean& operator=(Boolean const& rhs);
    };

    /** number Value
     */
    class Number: public gbLuaValue {
    private:
        double f_;            ///< storage
    public:
        Number(double f);
        ~Number();
        /** @name gbLuaValue
         * @{
         */
        LUA_TYPE GetType() const;
        gbLuaValuePtr Clone() const;
        /// @}
        double Get() const;
        void Set(double f);
        Number(Number const& rhs);
        Number& operator=(Number const& rhs);
    };

    /** string Value
     */
    class String: public gbLuaValue {
    private:
        std::string str_;        ///< storage
    public:
        String(std::string const& str);
        String(std::string&& str);
        String(char const* str);
        ~String();
        /** @name gbLuaValue
         * @{
         */
        LUA_TYPE GetType() const;
        gbLuaValuePtr Clone() const;
        /// @}
        std::string const& Get() const;
        void Set(std::string const& str);
        void Set(std::string&& str);
        void Set(char const* str);
        String(String const& rhs);
        String(String&& rhs);
        String& operator=(String const& rhs);
    };

    /** function Value
     */
    class Function: public gbLuaValue {
    private:
        lua_State* m_Lua;
        int m_Ref;
    public:
        Function(lua_State* internal_lua_state);
        ~Function();
        Function(Function const& rhs);
        Function(Function&& rhs);
        Function& operator=(Function const& rhs);
        /** @name gbLuaValue
         * @{
         */
        LUA_TYPE GetType() const;
        gbLuaValuePtr Clone() const;
        /// @}
        std::vector<gbLuaValuePtr> operator()();
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6, gbLuaValue const& arg7);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6, gbLuaValue const& arg7, gbLuaValue const& arg8);
        std::vector<gbLuaValuePtr> operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6, gbLuaValue const& arg7, gbLuaValue const& arg8, gbLuaValue const& arg9);
        std::vector<gbLuaValuePtr> operator()(std::vector<gbLuaValue const*> const& args);
        /// @cond
        void _internal_PushTable(lua_State* internal_lua_state) const;
        /// @endcond
    };

    /** userdata Value
     */
    class Userdata: public gbLuaValue {
        /// @todo
    };

    /** thread Value
     */
    class Thread: public gbLuaValue {
        /// @todo
    };

    class Table;
    typedef std::unique_ptr<Table> TablePtr;

    /** table Value
     */
    class Table: public gbLuaValue {
    public:
        /** Key/Value pair used as value type for iterators
         * Manually constructed KeyValuePairs always have key.get() == nullptr.
         * For KeyValuePairs obtained through a table iterator, key
         * points to a valid gbLuaValue.
         */
        struct KeyValuePair {
            std::unique_ptr<gbLuaValue const> key;
            std::unique_ptr<gbLuaValue const> value;
            /** Constructor
             */
            KeyValuePair()
                :key(nullptr), value(nullptr)
            {
            }
            /** Copy construction
             */
            KeyValuePair(KeyValuePair const& rhs)
                :key((rhs.key)?(rhs.key->Clone()):nullptr), value((rhs.value)?(rhs.value->Clone()):nullptr)
            {
            }
            /** Move construction
             */
            KeyValuePair(KeyValuePair&& rhs)
                :key(rhs.key.release()), value(rhs.value.release())
            {
            }
            /** Copy assignment
             */
            KeyValuePair& operator=(KeyValuePair const& rhs)
            {
                if(&rhs != this) {
                    key = ((rhs.key)?(rhs.key->Clone()):nullptr);
                    value = ((rhs.value)?(rhs.value->Clone()):nullptr);
                }
                return *this;
            }
            /** Move assignment
             */
            KeyValuePair& operator=(KeyValuePair&& rhs)
            {
                if(&rhs != this) {
                    key.swap(rhs.key);
                    value.swap(rhs.value);
                }
                return *this;
            }
        };
        /** Table iterator base class
         * An STL-style forward iterator for tables.
         * Table iterators behave exactly like the ones obtained through Lua's pairs() function.
         * @attention All restrictions of Lua iterators apply. In particular, adding new
         *            keys to a table yields undefined behavior. Modifying or deleting existing
         *            fields is fine though. Look at the next() function from Lua's basic library
         *            for further details.
         */
        class iterator_base: public std::iterator<std::forward_iterator_tag, KeyValuePair> {
            friend class Table;
        protected:
            lua_State* m_Lua;
            int m_Ref;
            KeyValuePair m_CurrentEntry;
        public:
            /** Equality comparison
             */
            bool operator==(iterator_base const& rhs) const;
            /** Inequality comparison
             */
            bool operator!=(iterator_base const& rhs) const;
        protected:
            /** Copy Constructor
             */
            iterator_base(iterator_base const& it);
            /** Move Constructor
             */
            iterator_base(iterator_base&& it);
            /** Constructor
             */
            iterator_base(lua_State* lua, int table_ref);
            /** Increments iterator
             */
            void next();
        private:
            /** Private default constructor (not implemented)
             */
            iterator_base();
        };
        /** Read-only iterator
         * Iterators were designed to mimic the behavior of Lua's for .. in pairs() idiom.
         * Therefore, all changes made to values obtained through the iterator are lost.
         * Changes to the table may be applied by calling one of the Table's Set() functions
         * with a key obtained through an iterator. Be aware though that changing the table
         * may invalidate the iterator (see @ref iterator_base).
         */
        class iterator: public iterator_base {
            friend class Table;
        public:
            typedef KeyValuePair const& reference;
            typedef KeyValuePair const* pointer;
        public:
            /** Copy construction
             */
            iterator(iterator const& it);
            /** Move construction
             */
            iterator(iterator&& it);
            /** @name Operators
             * @{
             */
            iterator& operator++();
            iterator operator++(int);
            reference operator*();
            pointer operator->();
            /// @}
        private:
            /** Constructor
             */
            iterator(lua_State* lua, int table_ref);
        };
        /** Index/Value pair used as value type for array iterators
         * Manually constructed IndexValuePairs always have index == 0 and value.get() == nullptr.
         */
        struct IndexValuePair {
            int index;
            std::unique_ptr<gbLuaValue const> value;
            /** Constructor
             */
            IndexValuePair()
                :index(0), value(nullptr)
            {
            }
            /** Copy construction
             */
            IndexValuePair(IndexValuePair const& rhs)
                :index(rhs.index), value((rhs.value)?(rhs.value->Clone()):nullptr)
            {
            }
            /** Move construction
             */
            IndexValuePair(IndexValuePair&& rhs)
                :index(rhs.index), value(rhs.value.release())
            {
            }
        };
        /** Table array iterator base class
         * An STL-style forward iterator for arrays.
         * Array iterators behave exactly like the ones obtained through Lua's ipairs() function.
         * @attention Unlike Lua's ipairs() iterators, array iterators ignore changes made to
         *            the table after their construction. That is, the iterator will return
         *            all entries t[1]..t[size] from table t where size is the value returned
         *            by Table::GetSize() upon construction of the iterator.
         */
        class array_iterator_base: public std::iterator<std::random_access_iterator_tag, IndexValuePair, int> {
            friend class Table;
        protected:
            Table const* m_Table;
            int m_CurrentIndex;
            IndexValuePair m_CurrentEntry;
        public:
            /** Equality comparison
             */
            bool operator==(array_iterator_base const& rhs) const;
            /** Inequality comparison
             */
            bool operator!=(array_iterator_base const& rhs) const;
            /** Less-than comparison
             */
            bool operator<(array_iterator_base const& rhs) const;
            /** Greater-than comparison
             */
            bool operator>(array_iterator_base const& rhs) const;
            /** Less-equal comparison
             */
            bool operator<=(array_iterator_base const& rhs) const;
            /** Greater-equal comparison
             */
            bool operator>=(array_iterator_base const& rhs) const;
        protected:
            /** Copy Constructor
             */
            array_iterator_base(array_iterator_base const& it);
            /** Constructor
             */
            array_iterator_base(Table const* t, int start_index);
        private:
            /** Private default constructor (not implemented)
             */
            array_iterator_base();
        };
        /** Read-only array iterator
         */
        class array_iterator: public array_iterator_base {
            friend class Table;
        public:
            //typedef IndexValuePair value;
            typedef IndexValuePair const& reference;
            typedef IndexValuePair const* pointer;
        public:
            /** Copy construction
             */
            array_iterator(array_iterator const& it);
            /** @name Operators
             * @{
             */
            array_iterator& operator++();
            array_iterator operator++(int);
            array_iterator& operator--();
            array_iterator operator--(int);
            array_iterator operator+=(difference_type offset);
            array_iterator operator+(difference_type offset) const;
            array_iterator operator-=(difference_type offset);
            array_iterator operator-(difference_type offset) const;
            difference_type operator-(array_iterator const& it) const;
            value_type operator[](difference_type offset) const;
            reference operator*();
            pointer operator->();
            /// @}
        private:
            /** Constructor
             */
            array_iterator(Table const* t, int start_index);
        };
    private:
        lua_State* m_Lua;
        int m_Ref;
    public:
        Table(lua_State* internal_lua_state);
        ~Table();
        /** @name gbLuaValue
         * @{
         */
        LUA_TYPE GetType() const;
        gbLuaValuePtr Clone() const;
        /// @}
        /** Clone method returning a TablePtr
         * Behaves exactly like Clone() except that the
         * return value is typed to TablePtr
         */
        TablePtr CloneTable() const;
        /** Get the size of the table
         * Equivalent to Lua's #-operator
         */
        int GetSize() const;
        /** Check whether the table is empty
         * @return cbegin() == cend()
         */
        bool IsEmpty() const;
        /** @name Getters
         * @{
         */
        gbLuaValuePtr Get(gbLuaValue const& key) const;
        gbLuaValuePtr Get(std::string const& key) const;
        gbLuaValuePtr Get(char const* key) const;
        gbLuaValuePtr Get(int index) const;
        TablePtr GetTable(gbLuaValue const& key) const;
        TablePtr GetTable(std::string const& key) const;
        TablePtr GetTable(char const* key) const;
        TablePtr GetTable(int index) const;
        /// @}
        /** Setters
         * @{
         */
        void Set(gbLuaValue const& key, gbLuaValue const& value);
        void Set(gbLuaValue const& key, gbLuaValue&& value);
        void Set(std::string const& key, gbLuaValue const& value);
        void Set(std::string const& key, gbLuaValue&& value);
        void Set(std::string const& key, double value);
        void Set(std::string const& key, std::string const& value);
        void Set(std::string const& key, char const* value);
        void Set(char const* key, gbLuaValue const& value);
        void Set(char const* key, gbLuaValue&& value);
        void Set(char const* key, double value);
        void Set(char const* key, std::string const& value);
        void Set(char const* key, char const* value);
        void Set(int index, gbLuaValue const& value);
        void Set(int index, gbLuaValue&& value);
        void Set(int index, double value);
        void Set(int index, std::string const& value);
        void Set(int index, char const* value);
        /// @}
        bool operator==(Table const & rhs);
        /** @name Iterator access
         * @{
         */
        iterator begin() const;
        iterator end() const;
        array_iterator ibegin() const;
        array_iterator iend() const;
        /// @}
        /** @name Copy construction
         * @{
         */
        Table(Table const& rhs);
        Table(Table&& rhs);
        Table& operator=(Table const& rhs);
        /// @}
        /// @cond
        void _internal_PushTable(lua_State* internal_lua_state) const;
        void const* _internal_GetPointer() const;
        /// @endcond
    };

    /// @}


    /** @defgroup lua_value_conversion Convenience functions for Lua type conversion
     * @{
     */
    template<typename T>
    T& ConvertTo(gbLuaValue& v);

    template<typename T>
    T& ConvertTo(gbLuaValuePtr const& v) {
        return ConvertTo<T>(*v);
    }

    template<> inline
    Boolean& ConvertTo(gbLuaValue& v) {
        return dynamic_cast<Boolean&>(v);
    }

    template<> inline
    Number& ConvertTo(gbLuaValue& v) {
        return dynamic_cast<Number&>(v);
    }

    template<> inline
    String& ConvertTo(gbLuaValue& v) {
        return dynamic_cast<String&>(v);
    }

    template<> inline
    Function& ConvertTo(gbLuaValue& v) {
        return dynamic_cast<Function&>(v);
    }

    template<> inline
    Userdata& ConvertTo(gbLuaValue& v) {
        return dynamic_cast<Userdata&>(v);
    }

    template<> inline
    Thread& ConvertTo(gbLuaValue& v) {
        return dynamic_cast<Thread&>(v);
    }

    template<> inline
    Table& ConvertTo(gbLuaValue& v) {
        return dynamic_cast<Table&>(v);
    }

    template<typename T> inline
    T& ConvertTo(gbLuaValue& v) {
        BOOST_STATIC_ASSERT_MSG(sizeof(T) == 0, "Can only convert to Lua value type");
    }

    template<typename T>
    T const& ConvertTo(gbLuaValue const& v);

    template<typename T>
    T const& ConvertTo(std::unique_ptr<gbLuaValue const> const& v) {
        return ConvertTo<T>(*v);
    }

    template<> inline
    Boolean const& ConvertTo(gbLuaValue const& v) {
        return dynamic_cast<Boolean const&>(v);
    }

    template<> inline
    Number const& ConvertTo(gbLuaValue const& v) {
        return dynamic_cast<Number const&>(v);
    }

    template<> inline
    String const& ConvertTo(gbLuaValue const& v) {
        return dynamic_cast<String const&>(v);
    }

    template<> inline
    Function const& ConvertTo(gbLuaValue const& v) {
        return dynamic_cast<Function const&>(v);
    }

    template<> inline
    Userdata const& ConvertTo(gbLuaValue const& v) {
        return dynamic_cast<Userdata const&>(v);
    }

    template<> inline
    Thread const& ConvertTo(gbLuaValue const& v) {
        return dynamic_cast<Thread const&>(v);
    }

    template<> inline
    Table const& ConvertTo(gbLuaValue const& v) {
        return dynamic_cast<Table const&>(v);
    }

    template<typename T> inline
    T const& ConvertTo(gbLuaValue const& v) {
        BOOST_STATIC_ASSERT_MSG(sizeof(T) == 0, "Can only convert to Lua value type");
    }
    /// @}
}

/** @addtogroup ostream_inserter Ostream Inserters
 * @{
 */
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::LUA_TYPE t);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::gbLuaValue const& v);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Boolean const& v);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Function const& v);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Nil const&);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Number const& v);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::String const& v);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Table const& v);
std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Thread const& v);
/// @}

#endif


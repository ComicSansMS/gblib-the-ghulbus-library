/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_LUA_INCLUDE_GUARD_LUA_EXCEPTION_HPP_
#define GB_LUA_INCLUDE_GUARD_LUA_EXCEPTION_HPP_

#include <gbLua/config.hpp>
#include <gbLua/lua_value.hpp>

#include <string>
#include <memory>

#include <diagnostic/exception.hpp>

namespace GB_LUA_NAMESPACE {

    /** Namespace for exception info records
     */
    namespace Exception_InfoRecords {
    }

    /** Namespace for exception info tags
     */
    namespace Exception_InfoTags {
        struct exception_tag_lua {};
    }
    /** Namespace for exceptions
     */
    namespace Exceptions {
        /** Lua Exception base class
         * @note All LuaException are usually decorated with an Exception_Info::info_lua_error
         */
        class LuaException: public Diagnostic::Exception {
        public:
            virtual ~LuaException() throw();
        };
        /** Exception indicating an error during script execution
         */
        class LuaRuntimeError: public LuaException {
        public:
            char const* what() const throw();
        };
        /** Exception indicating an error during memory allocation
         */
        class LuaMemoryAllocationError: public LuaException {
        public:
            char const* what() const throw();
        };
        /** Exception indicating an error occured while running the error message handler
         */
        class LuaErrorInMessageHandler: public LuaException {
        public:
            char const* what() const throw();
        };
        /** Exception indicating an error occured in a __gc metamethod
         */
        class LuaErrorInGarbageCollectorMetamethod: public LuaException {
        public:
            char const* what() const throw();
        };
        /** Exception indicating an error during script execution due to bad syntax
         */
        class LuaSyntaxError: public LuaException {
        public:
            char const* what() const throw();
        };
        /** Exception indicating an error due to type mismatch
         */
        class LuaTypeError: public LuaException {
        private:
            LUA_TYPE expected_;
            LUA_TYPE actual_;
            std::string errmsg_;
        public:
            LuaTypeError(LUA_TYPE expected, LUA_TYPE actual);
            LuaTypeError(LUA_TYPE expected, gbLuaValue const& value);
            LuaTypeError(LUA_TYPE expected, gbLuaValuePtr const& value);
            ~LuaTypeError() throw();
            LUA_TYPE GetExpectedType() const;
            LUA_TYPE GetActualType() const;
            char const* what() const throw();
        };
    }

    namespace Exception_Info {
        /** Exception decorator for Lua error details
         * Contains a string with a detailed error message supplied by the Lua VM
         */
        typedef boost::error_info<Exception_InfoTags::exception_tag_lua, std::string> info_lua_error;
    }

}


#endif


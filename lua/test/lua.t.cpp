/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gtest/gtest.h>

#include <gbLua/lua.hpp>
#include <utility>

#include <diagnostic/diagnostic.hpp>

TEST(LuaValue, Nil)
{
    using namespace GhulbusLua;
    Nil nil;
    EXPECT_EQ(LUA_TYPE_NIL, nil.GetType());
    EXPECT_EQ(LUA_TYPE_NIL, nil.Clone()->GetType());
}

TEST(LuaValue, Number)
{
    using namespace GhulbusLua;
    Number n(3.141);
    EXPECT_EQ(3.141, n.Get());
    EXPECT_EQ(LUA_TYPE_NUMBER, n.GetType());
    Number nc(n);
    EXPECT_EQ(n.Get(), nc.Get());
    EXPECT_EQ(n.GetType(), nc.GetType());
    n.Set(42);
    EXPECT_EQ(42, n.Get());
    EXPECT_NE(n.Get(), nc.Get());
    gbLuaValuePtr n2 = n.Clone();
    EXPECT_EQ(LUA_TYPE_NUMBER, n2->GetType());
    EXPECT_EQ(n.Get(), dynamic_cast<Number&>(*n2).Get());
}

TEST(LuaValue, Boolean)
{
    using namespace GhulbusLua;
    Boolean b(true);
    Boolean b2(false);
    EXPECT_TRUE(b.Get());
    EXPECT_FALSE(b2.Get());
    Boolean bc(b);
    EXPECT_EQ(b.Get(), bc.Get());
    Boolean bc2(b2);
    EXPECT_EQ(b2.Get(), bc2.Get());
    EXPECT_EQ(LUA_TYPE_BOOLEAN, bc2.GetType());
    EXPECT_EQ(b.GetType(), bc2.GetType());
    b.Set(true);
    EXPECT_TRUE(b.Get());
    gbLuaValuePtr clone1 = bc.Clone();
    gbLuaValuePtr clone2 = bc2.Clone();
    EXPECT_EQ(LUA_TYPE_BOOLEAN, clone1->GetType());
    EXPECT_EQ(LUA_TYPE_BOOLEAN, clone2->GetType());
    EXPECT_TRUE(dynamic_cast<Boolean&>(*clone1).Get());
    EXPECT_FALSE(dynamic_cast<Boolean&>(*clone2).Get());
}

TEST(LuaValue, String)
{
    using namespace GhulbusLua;
    String str1("foo");
    EXPECT_EQ(LUA_TYPE_STRING, str1.GetType());
    EXPECT_STREQ("foo", str1.Get().c_str());
    String str2("bar");
    EXPECT_EQ(std::string("bar"), str2.Get());
    str2.Set(str1.Get());
    EXPECT_EQ(str1.Get(), str2.Get());
    str2.Set("bar");
    String copy(str1);
    EXPECT_EQ(LUA_TYPE_STRING, copy.GetType());
    EXPECT_EQ(str1.Get(), copy.Get());
    gbLuaValuePtr clone = str2.Clone();
    EXPECT_EQ(LUA_TYPE_STRING, clone->GetType());
    EXPECT_EQ(str2.Get(), dynamic_cast<String&>(*clone).Get());
    String moved(std::move(str1));
    EXPECT_EQ(copy.Get(), moved.Get());
}

TEST(Lua, Construction)
{
    using namespace GhulbusLua;
    std::shared_ptr<gbLua> lua;
    ASSERT_NO_THROW(lua = std::make_shared<gbLua>());
    EXPECT_EQ(LUA_TYPE_TABLE, lua->GetGlobal("_G")->GetType());
}

TEST(Lua, RunString)
{
    using namespace GhulbusLua;
    gbLua lua;
    ASSERT_NO_THROW(lua.RunString("foostr = \"bar\"; foonumber = 42; \n\t foobool = true; \n foonil = nil"));
    gbLuaValuePtr foostr = lua.GetGlobal("foostr");
    EXPECT_EQ(LUA_TYPE_STRING, foostr->GetType());
    EXPECT_EQ(std::string("bar"), dynamic_cast<String&>(*foostr).Get());
    gbLuaValuePtr foonumber = lua.GetGlobal("foonumber");
    EXPECT_EQ(LUA_TYPE_NUMBER, foonumber->GetType());
    EXPECT_EQ(42, dynamic_cast<Number&>(*foonumber).Get());
    gbLuaValuePtr foobool = lua.GetGlobal("foobool");
    EXPECT_EQ(LUA_TYPE_BOOLEAN, foobool->GetType());
    EXPECT_TRUE(dynamic_cast<Boolean&>(*foobool).Get());
    gbLuaValuePtr foonil = lua.GetGlobal("foonil");
    EXPECT_EQ(LUA_TYPE_NIL, foonil->GetType());
    EXPECT_TRUE(IsNil(*foonil));
    EXPECT_THROW(lua.RunString("if testtable then print(\"end missing!\");"), Exceptions::LuaSyntaxError);
}

TEST(Lua, StandardLibraries)
{
    using namespace GhulbusLua;
    gbLua lua;
    EXPECT_NO_THROW(lua.RunString("table.sort({1,42,3});"));
    lua.RunString("unsorted_t = {5, 3, 1, 2, 4}");
    TablePtr t = lua.CreateTable("unsorted_t");
    /*
    std::sort(t->begin(), t->end(), [](Table::KeyValuePair const& lhs, Table::KeyValuePair const& rhs) -> bool {
        return true;
    });
    */
}

TEST(Lua, ConvertTo)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("teststr = \"a string\"; testnum = 12345.6; testbool = true; testtable = { 1, \"foo\", {3} };");
    gbLuaValuePtr v = lua.GetGlobal("teststr");
    ASSERT_EQ(LUA_TYPE_STRING, v->GetType());
    EXPECT_STREQ("a string", ConvertTo<String>(v).Get().c_str());
    v = lua.GetGlobal("testnum");
    ASSERT_EQ(LUA_TYPE_NUMBER, v->GetType());
    EXPECT_EQ(12345.6, ConvertTo<Number>(v).Get());
    v = lua.GetGlobal("testbool");
    ASSERT_EQ(LUA_TYPE_BOOLEAN, v->GetType());
    EXPECT_TRUE(ConvertTo<Boolean>(v).Get());
    v = lua.GetGlobal("testtable");
    ASSERT_EQ(LUA_TYPE_TABLE, v->GetType());
    EXPECT_EQ(3, ConvertTo<Table>(v).GetSize());
}


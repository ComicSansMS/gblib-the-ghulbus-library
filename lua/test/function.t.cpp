/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gtest/gtest.h>

#include <gbLua/lua.hpp>
#include <utility>

#include <diagnostic/diagnostic.hpp>

TEST(Function, Construction)
{
    using namespace GhulbusLua;
    gbLua lua;
    EXPECT_TRUE(IsNil(*lua.GetGlobal("test_function")));
    lua.RunString("function test_function() end");
    gbLuaValuePtr val = lua.GetGlobal("test_function");
    ASSERT_EQ(LUA_TYPE_FUNCTION, val->GetType());
    EXPECT_NO_THROW((void)dynamic_cast<Function*>(val.get()));
}

TEST(Function, Invocation)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("function test_function() end");
    EXPECT_NO_THROW(ConvertTo<Function>(*lua.GetGlobal("test_function"))());
}

TEST(Function, ReturnValues)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("function get_result() return 42; end");
    std::vector<gbLuaValuePtr> result = ConvertTo<Function>(*lua.GetGlobal("get_result"))();
    ASSERT_EQ(1, result.size());
    ASSERT_EQ(LUA_TYPE_NUMBER, result[0]->GetType());
    auto& n = ConvertTo<Number>(*result[0]);
    n.Get();
    EXPECT_EQ(42.0, ConvertTo<Number>(*result[0]).Get());
}

TEST(Function, InvocationWithParameters)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("function add_numbers(a, b) return a + b; end");
    gbLuaValuePtr val = lua.GetGlobal("add_numbers");
    Function& func = ConvertTo<Function>(val);
    std::vector<gbLuaValuePtr> res = func(Number(17.0), Number(23.0));
    ASSERT_EQ(1, res.size());
    ASSERT_EQ(LUA_TYPE_NUMBER, res[0]->GetType());
    EXPECT_EQ(40.0, ConvertTo<Number>(*res[0]).Get());
}

TEST(Function, FunctionCallThatThrows)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("function index(t, i) return t[i]; end");
    gbLuaValuePtr val = lua.GetGlobal("index");
    Function& func = ConvertTo<Function>(val);
    EXPECT_THROW(func(Nil(), Nil()), Exceptions::LuaRuntimeError);
}


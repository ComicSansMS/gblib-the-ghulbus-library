/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gtest/gtest.h>

#include <gbLua/lua.hpp>
#include <algorithm>
#include <utility>
#include <sstream>

#include <diagnostic/diagnostic.hpp>

TEST(Table, Getters)
{
    using namespace GhulbusLua;
    gbLua lua;
    ASSERT_NO_THROW(lua.RunString("testtable = {4, 8, 15, 16, 23, 42}; another_test = { foo='bar', [3] = 3.141, mybool=true };"));
    {
        gbLuaValuePtr tptr = lua.GetGlobal("testtable");
        ASSERT_EQ(LUA_TYPE_TABLE, tptr->GetType());
        Table& table = dynamic_cast<Table&>(*tptr);

        ASSERT_EQ(6, table.GetSize());
        lua.RunString("testtable = nil");
        EXPECT_EQ(6, table.GetSize());
    
        ASSERT_EQ(LUA_TYPE_NUMBER, table.Get(1)->GetType());
        EXPECT_EQ(4,  dynamic_cast<Number&>(*table.Get(1)).Get());
        EXPECT_EQ(8,  dynamic_cast<Number&>(*table.Get(2)).Get());
        EXPECT_EQ(15, dynamic_cast<Number&>(*table.Get(3)).Get());
        EXPECT_EQ(16, dynamic_cast<Number&>(*table.Get(4)).Get());
        EXPECT_EQ(23, dynamic_cast<Number&>(*table.Get(5)).Get());
        EXPECT_EQ(42, dynamic_cast<Number&>(*table.Get(6)).Get());
    }
    {
        TablePtr tptr = lua.CreateTable("another_test");
        ASSERT_EQ(LUA_TYPE_TABLE, tptr->GetType());
        Table& t2 = dynamic_cast<Table&>(*tptr);
        gbLuaValuePtr vptr = t2.Get("foo");
        ASSERT_EQ(LUA_TYPE_STRING, vptr->GetType());
        EXPECT_STREQ("bar", dynamic_cast<String&>(*vptr).Get().c_str());
    
        vptr = t2.Get(Number(3));
        ASSERT_EQ(LUA_TYPE_NUMBER, vptr->GetType());
        EXPECT_EQ(3.141, dynamic_cast<Number&>(*vptr).Get());

        vptr = t2.Get(String("mybool"));
        ASSERT_EQ(LUA_TYPE_BOOLEAN, vptr->GetType());
        EXPECT_TRUE(dynamic_cast<Boolean&>(*vptr).Get());
    }
}

TEST(Table, Creation)
{
    using namespace GhulbusLua;
    gbLua lua;
    {
        TablePtr tptr = lua.CreateTable();
        EXPECT_EQ(0, tptr->GetSize());
    }
    {
        TablePtr tptr = lua.CreateTable(std::string("new_table"));
        EXPECT_EQ(0, tptr->GetSize());
    }
    {
        TablePtr tptr = lua.CreateTable("new_table");
        EXPECT_EQ(0, tptr->GetSize());
    }
    lua.RunString("occupied = true;");
    EXPECT_THROW(lua.CreateTable("occupied"), Exceptions::LuaRuntimeError);
}

TEST(Table, EqualityOp)
{
    using namespace GhulbusLua;
    gbLua lua;
    TablePtr t1 = lua.CreateTable();
    EXPECT_TRUE(*t1 == *t1);
    TablePtr t2 = lua.CreateTable();
    EXPECT_FALSE(*t1 == *t2);

    gbLua lua2;
    TablePtr t3 = lua2.CreateTable();
    EXPECT_THROW((void)(*t3 == *t2), Diagnostic::Exceptions::InvalidArgument);
}

TEST(Table, Setters)
{
    using namespace GhulbusLua;
    gbLua lua;
    {
        gbLuaValuePtr val;
        TablePtr tptr = lua.CreateTable("new_table");
        EXPECT_EQ(0, tptr->GetSize());

        tptr->Set(String("foo"), Number(123.321));
        val = tptr->Get("foo");
        ASSERT_EQ(LUA_TYPE_NUMBER, val->GetType());
        EXPECT_EQ(123.321, dynamic_cast<Number&>(*val).Get());

        tptr->Set(*val, *val);
        val = tptr->Get(*val);
        ASSERT_EQ(LUA_TYPE_NUMBER, val->GetType());
        EXPECT_EQ(123.321, dynamic_cast<Number&>(*val).Get());

        tptr->Set(std::string("bar"), Number(1));
        val = tptr->Get("bar");
        ASSERT_EQ(LUA_TYPE_NUMBER, val->GetType());
        EXPECT_EQ(1, dynamic_cast<Number&>(*val).Get());

        tptr->Set(std::string("bar_again"), *val);
        val = tptr->Get("bar_again");
        ASSERT_EQ(LUA_TYPE_NUMBER, val->GetType());
        EXPECT_EQ(1, dynamic_cast<Number&>(*val).Get());

        tptr->Set(std::string("pi"), 3.141);
        val = tptr->Get("pi");
        ASSERT_EQ(LUA_TYPE_NUMBER, val->GetType());
        EXPECT_EQ(3.141, dynamic_cast<Number&>(*val).Get());

        tptr->Set(std::string("narf"), std::string("zort"));
        val = tptr->Get("narf");
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("zort", dynamic_cast<String&>(*val).Get().c_str());

        tptr->Set("narf", std::string("poit"));
        val = tptr->Get("narf");
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("poit", dynamic_cast<String&>(*val).Get().c_str());

        tptr->Set("A longer string for a change", Boolean(true));
        val = tptr->Get("A longer string for a change");
        ASSERT_EQ(LUA_TYPE_BOOLEAN, val->GetType());
        EXPECT_TRUE(dynamic_cast<Boolean&>(*val).Get());

        tptr->Set("Look, it's me!", *tptr);
        val = tptr->Get("Look, it's me!");
        ASSERT_EQ(LUA_TYPE_TABLE, val->GetType());
        EXPECT_TRUE(*tptr == dynamic_cast<Table&>(*val));

        tptr->Set("more numbers...", 2.718281828);
        val = tptr->Get("more numbers...");
        ASSERT_EQ(LUA_TYPE_NUMBER, val->GetType());
        EXPECT_EQ(2.718281828, dynamic_cast<Number&>(*val).Get());

        tptr->Set("now, for a string", std::string("Take me, I'm stringy!"));
        val = tptr->Get("now, for a string");
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("Take me, I'm stringy!", dynamic_cast<String&>(*val).Get().c_str());

        tptr->Set("hey, ", "you!");
        val = tptr->Get("hey, ");
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("you!", dynamic_cast<String&>(*val).Get().c_str());

        EXPECT_EQ(0, tptr->GetSize());
        tptr->Set(1, std::string("One"));
        EXPECT_EQ(1, tptr->GetSize());
        val = tptr->Get(1);
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("One", dynamic_cast<String&>(*val).Get().c_str());

        tptr->Set(2, *val);
        EXPECT_EQ(2, tptr->GetSize());
        val = tptr->Get(2);
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("One", dynamic_cast<String&>(*val).Get().c_str());

        tptr->Set(3, String("3"));
        val = tptr->Get(3);
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("3", dynamic_cast<String&>(*val).Get().c_str());

        tptr->Set(10, 14e-7);
        EXPECT_EQ(3, tptr->GetSize());
        val = tptr->Get(10);
        ASSERT_EQ(LUA_TYPE_NUMBER, val->GetType());
        EXPECT_EQ(14e-7, dynamic_cast<Number&>(*val).Get());

        tptr->Set(10, std::string("Almost there..."));
        val = tptr->Get(10);
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("Almost there...", dynamic_cast<String&>(*val).Get().c_str());

        tptr->Set(10, "Last one.");
        val = tptr->Get(10);
        ASSERT_EQ(LUA_TYPE_STRING, val->GetType());
        EXPECT_STREQ("Last one.", dynamic_cast<String&>(*val).Get().c_str());
    }
}

TEST(Table, CopyConstruction)
{
    using namespace GhulbusLua;
    gbLua lua;
    TablePtr t1 = lua.CreateTable();
    EXPECT_TRUE(*t1 == *t1);
    TablePtr t2 = lua.CreateTable();
    EXPECT_FALSE(*t1 == *t2);
    Table t3(*t1);
    EXPECT_TRUE(t3 == *t1);
    EXPECT_FALSE(t3 == *t2);
    Table t4(std::move(*t1));
    EXPECT_FALSE(t4 == *t1);
    EXPECT_TRUE(t4 == t3);
    t4 = *t2;
    EXPECT_FALSE(t4 == t3);
    EXPECT_FALSE(t4 == *t1);
    EXPECT_TRUE(t4 == *t2);
}

TEST(Table, KeyValuePair)
{
    using namespace GhulbusLua;
    Table::KeyValuePair kv;
    EXPECT_FALSE(kv.key);
    EXPECT_FALSE(kv.value);

    gbLua lua;
    lua.RunString("t = { 42 };");
    auto tmp = lua.GetGlobal("t");
    kv = *dynamic_cast<Table const&>(*tmp).begin();
    ASSERT_TRUE(static_cast<bool>(kv.value));
    ASSERT_EQ(LUA_TYPE_NUMBER, kv.value->GetType());
    EXPECT_EQ(42, dynamic_cast<Number const&>(*kv.value).Get());

    Table::KeyValuePair kv_copy(kv);
    ASSERT_TRUE(static_cast<bool>(kv_copy.value));
    ASSERT_EQ(LUA_TYPE_NUMBER, kv_copy.value->GetType());
    EXPECT_EQ(dynamic_cast<Number const&>(*kv.value).Get(), dynamic_cast<Number const&>(*kv_copy.value).Get());

    Table::KeyValuePair kv_move(std::move(kv_copy));
    ASSERT_TRUE(static_cast<bool>(kv_move.value));
    ASSERT_EQ(LUA_TYPE_NUMBER, kv_move.value->GetType());
    EXPECT_EQ(42, dynamic_cast<Number const&>(*kv_move.value).Get());
    EXPECT_FALSE(kv_copy.key);
    EXPECT_FALSE(kv_copy.value);

    Table::KeyValuePair copy_from_null(kv_copy);
    EXPECT_FALSE(copy_from_null.key);
    EXPECT_FALSE(copy_from_null.value);

    Table::KeyValuePair move_from_null(std::move(kv_copy));
    EXPECT_FALSE(move_from_null.key);
    EXPECT_FALSE(move_from_null.value);
}

TEST(Table, Iterators)
{
    using namespace GhulbusLua;
    gbLua lua;
    ASSERT_NO_THROW(lua.RunString("testtable = {4, 8, 15, 16, 23, 42}; another_test = { foo='bar', [3] = 3.141, MyBool=true };"));
    gbLuaValuePtr v = lua.GetGlobal("another_test");
    ASSERT_EQ(LUA_TYPE_TABLE, v->GetType());

    Table& t = dynamic_cast<Table&>(*v);
    Table::iterator it_end = t.end();
    for(Table::iterator it = t.begin(); it != it_end; ++it) {
        if(it->key->GetType() == LUA_TYPE_STRING) {
            String const& key = dynamic_cast<String const&>(*it->key);
            if(key.Get() == "foo") {
                ASSERT_EQ(LUA_TYPE_STRING, it->value->GetType());
                String const& val = dynamic_cast<String const&>(*it->value);
                EXPECT_STREQ("bar", val.Get().c_str());
            } else {
                ASSERT_STREQ("MyBool", key.Get().c_str());
                ASSERT_EQ(LUA_TYPE_BOOLEAN, it->value->GetType());
                Boolean const& val = dynamic_cast<Boolean const&>(*it->value);
                EXPECT_TRUE(val.Get());
            }
        } else {
            ASSERT_EQ(LUA_TYPE_NUMBER, it->key->GetType());
            Number const& key = dynamic_cast<Number const&>(*it->key);
            EXPECT_EQ(3, key.Get());
            ASSERT_EQ(LUA_TYPE_NUMBER, it->value->GetType());
            Number const& val = dynamic_cast<Number const&>(*it->value);
            EXPECT_EQ(3.141, val.Get());
        }
    }

    for(Table::iterator it = t.begin(); it != t.end(); ++it) {
        if(it->key->GetType() == LUA_TYPE_NUMBER) {
            ASSERT_EQ(LUA_TYPE_NUMBER, it->value->GetType());
            t.Set(*(it->key), String("A new world!"));
        }
    }
    gbLuaValuePtr changed_entry = t.Get(3);
    ASSERT_EQ(LUA_TYPE_STRING, changed_entry->GetType());
    EXPECT_STREQ("A new world!", dynamic_cast<String&>(*changed_entry).Get().c_str());

    t.Set("MyBool", Nil());
    std::for_each(t.begin(), t.end(), [](Table::KeyValuePair const& kv) {
        using namespace GhulbusLua;
        EXPECT_EQ(LUA_TYPE_STRING, kv.value->GetType());
    });

    TablePtr t2 = lua.CreateTable("testtable");
    std::for_each(t2->begin(), t2->end(), [&t2](Table::KeyValuePair const& kv) {
        using namespace GhulbusLua;
        EXPECT_EQ(LUA_TYPE_NUMBER, kv.key->GetType());
        EXPECT_EQ(LUA_TYPE_NUMBER, kv.value->GetType());
        t2->Set(*kv.key, Nil());
    });
    EXPECT_EQ(0, t2->GetSize());
}

TEST(Table, ArrayIterators)
{
    using namespace GhulbusLua;
    gbLua lua;
    ASSERT_NO_THROW(lua.RunString("testtable = {4, 8, 15, 16, 23, 42};"));
    gbLuaValuePtr v = lua.GetGlobal("testtable");
    ASSERT_EQ(LUA_TYPE_TABLE, v->GetType());

    auto t = ConvertTo<Table>(v);
    std::for_each(t.ibegin(), t.iend(), [](Table::IndexValuePair const& iv) {
        using namespace GhulbusLua;
        auto v = ConvertTo<Number>(*iv.value);
        switch(iv.index) {
        case 1: EXPECT_EQ(4, v.Get()); break;
        case 2: EXPECT_EQ(8, v.Get()); break;
        case 3: EXPECT_EQ(15, v.Get()); break;
        case 4: EXPECT_EQ(16, v.Get()); break;
        case 5: EXPECT_EQ(23, v.Get()); break;
        case 6: EXPECT_EQ(42, v.Get()); break;
        default:
            EXPECT_TRUE(false);
            break;
        }
    });
}

TEST(Table, NestedTables)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("\
                  t = { \
                    [42] = { 'awesome_number_table' }, \
                    foo = { 'string_table' }, \
                    bar = { 'another string table' }, \
                    [true] = { 'bool_table' } \
                  };\
                  bad_t = { \
                    [42] = 1, \
                    foo = 2, \
                    bar = 3, \
                    [true] = 4 \
                  };");
    TablePtr table = lua.CreateTable("t");
    gbLuaValuePtr v = table->GetTable(std::string("foo"))->Get(1);
    ASSERT_EQ(LUA_TYPE_STRING, v->GetType());
    EXPECT_STREQ("string_table", dynamic_cast<String&>(*v).Get().c_str());

    v = table->GetTable("bar")->Get(1);
    ASSERT_EQ(LUA_TYPE_STRING, v->GetType());
    EXPECT_STREQ("another string table", dynamic_cast<String&>(*v).Get().c_str());

    v = table->GetTable(42)->Get(1);
    ASSERT_EQ(LUA_TYPE_STRING, v->GetType());
    EXPECT_STREQ("awesome_number_table", dynamic_cast<String&>(*v).Get().c_str());

    v = table->GetTable(Boolean(true))->Get(1);
    ASSERT_EQ(LUA_TYPE_STRING, v->GetType());
    EXPECT_STREQ("bool_table", dynamic_cast<String&>(*v).Get().c_str());

    table = lua.CreateTable("bad_t");
    EXPECT_THROW(table->GetTable(std::string("foo")), Exceptions::LuaRuntimeError);
    EXPECT_THROW(table->GetTable("bar"), Exceptions::LuaRuntimeError);
    EXPECT_THROW(table->GetTable(42), Exceptions::LuaRuntimeError);
    EXPECT_THROW(table->GetTable(Boolean(true)), Exceptions::LuaRuntimeError);
}

TEST(Table, IsEmpty)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("t = { t2 = {} };");

    TablePtr t = lua.CreateTable("t");
    EXPECT_FALSE(t->IsEmpty());
    TablePtr t2 = t->GetTable("t2");
    EXPECT_TRUE(t2->IsEmpty());
}

TEST(Table, Clone)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("t = { 12345, 6789 };");
    TablePtr t = lua.CreateTable("t");
    auto v = t->Get(1);
    ASSERT_EQ(LUA_TYPE_NUMBER, v->GetType());
    EXPECT_EQ(12345, dynamic_cast<Number&>(*v).Get());

    TablePtr t_tabclone = t->CloneTable();
    v = t->Get(2);
    ASSERT_EQ(LUA_TYPE_NUMBER, v->GetType());
    EXPECT_EQ(6789, dynamic_cast<Number&>(*v).Get());

    gbLuaValuePtr t_clone = t_tabclone->Clone();
    ASSERT_EQ(LUA_TYPE_TABLE, t_clone->GetType());
    v = dynamic_cast<Table&>(*t_clone).Get(1);
    ASSERT_EQ(LUA_TYPE_NUMBER, v->GetType());
    EXPECT_EQ(12345, dynamic_cast<Number&>(*v).Get());
}

TEST(Table, PrintCycles)
{
    using namespace GhulbusLua;
    gbLua lua;
    lua.RunString("t = { { 1, 2, 3, { 1, 2, { } }, 5 }, 3 }; t[1][4][3][1] = t;");
    std::ostringstream osstr;
    EXPECT_NO_THROW(osstr << (*lua.GetGlobal("t")));
}


/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_LUA_INCLUDE_GUARD_INTERNAL_HPP_
#define GB_LUA_INCLUDE_GUARD_INTERNAL_HPP_

#ifndef GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_
#    error Do not include lua_internal.hpp! What are you even doing in here?!
#endif

#include <gbLua/lua_value.hpp>
#include <gbLua/stack_monitor.hpp>
#include <gbLua/stack_guard.hpp>

#include <gbUtil/missing_features.hpp>

inline GB_LUA_NAMESPACE::gbLuaValuePtr GetElementFromStack(lua_State* lua, int idx)
{
    // lua stack: +0
    using namespace GB_LUA_NAMESPACE;
    StackMonitor const CHECK_STACK(lua, 0);
    LUA_TYPE const type = ConvertLuaType(lua_type(lua, idx));
    switch(type) {
    case LUA_TYPE_NIL:
        return std::make_unique<Nil>();
    case LUA_TYPE_BOOLEAN:
        return std::make_unique<Boolean>(lua_toboolean(lua, idx) != 0);
    case LUA_TYPE_NUMBER:
        return std::make_unique<Number>(lua_tonumber(lua, idx));
    case LUA_TYPE_STRING:
        return std::make_unique<String>(lua_tostring(lua, idx));
    case LUA_TYPE_TABLE:
        lua_pushvalue(lua, idx);
        return std::make_unique<Table>(lua);
    case LUA_TYPE_FUNCTION:
        lua_pushvalue(lua, idx);
        return std::make_unique<Function>(lua);
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Type not implemented");
        /*
            LUA_TYPE_USERDATA,
            LUA_TYPE_THREAD
        */
    }
}

inline void PushElement(lua_State* lua, GB_LUA_NAMESPACE::gbLuaValue const& val)
{
    using namespace GB_LUA_NAMESPACE;
    StackMonitor const CHECK_STACK(lua, 1);
    switch(val.GetType()) {
    case LUA_TYPE_NIL:
        lua_pushnil(lua);
        break;
    case LUA_TYPE_BOOLEAN:
        lua_pushboolean(lua, (dynamic_cast<Boolean const&>(val).Get())?1:0);
        break;
    case LUA_TYPE_NUMBER:
        lua_pushnumber(lua, dynamic_cast<Number const&>(val).Get());
        break;
    case LUA_TYPE_STRING:
        {
            std::string const& str = dynamic_cast<String const&>(val).Get();
            lua_pushlstring(lua, str.c_str(), str.length());
        } break;
    case LUA_TYPE_TABLE:
        dynamic_cast<Table const&>(val)._internal_PushTable(lua);
        break;
    case LUA_TYPE_FUNCTION:
        dynamic_cast<Function const&>(val)._internal_PushTable(lua);
        break;
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Type not implemented");
        /*
            LUA_TYPE_USERDATA,
            LUA_TYPE_THREAD
        */
    
    }
}



#endif



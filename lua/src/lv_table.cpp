/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua_value.hpp>
#include <gbLua/lua_exception.hpp>
#include <gbLua/lua.hpp>
#include <gbLua/stack_monitor.hpp>
#include <gbLua/stack_guard.hpp>
#ifdef GB_LUA_DIAGNOSTIC_STACK_CONSISTENCY_CHECKS_ENABLED
#    define STACK_CHECK(n) ::GB_LUA_NAMESPACE::StackMonitor const localStackMonitor(m_Lua, n, DIAG_HELPER_LOCATION_INFO_FUNCTION)
#    define STACK_CHECK_DISABLE() const_cast<StackMonitor&>(localStackMonitor).Release()
#else
#    define STACK_CHECK(n) void(0)
#    define STACK_CHECK_DISABLE() void(0)
#endif

#include <diagnostic/assert.hpp>
#include <gbUtil/missing_features.hpp>

#include <utility>

#include <lua.hpp>

#define GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_
#include "lua_internal.hpp"
#undef GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_

namespace GB_LUA_NAMESPACE {

Table::Table(lua_State* internal_lua_state)
    :m_Lua(internal_lua_state), m_Ref(luaL_ref(internal_lua_state, LUA_REGISTRYINDEX))
{
    DIAG_ASSERT(m_Ref != LUA_REFNIL);
    DIAG_ASSERT(m_Ref != LUA_NOREF);
}

Table::~Table()
{
    STACK_CHECK(0);
    luaL_unref(m_Lua, LUA_REGISTRYINDEX, m_Ref);
}

Table::Table(Table const& rhs)
    :m_Lua(rhs.m_Lua), m_Ref(LUA_REFNIL)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, rhs.m_Ref);
    m_Ref = luaL_ref(m_Lua, LUA_REGISTRYINDEX);
}

Table::Table(Table&& rhs)
    :m_Lua(rhs.m_Lua), m_Ref(rhs.m_Ref)
{
    rhs.m_Ref = LUA_REFNIL;
}

Table& Table::operator=(Table const& rhs)
{
    STACK_CHECK(0);
    if(&rhs != this) {
        lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, rhs.m_Ref);
        luaL_unref(m_Lua, LUA_REGISTRYINDEX, m_Ref);
        m_Ref = luaL_ref(m_Lua, LUA_REGISTRYINDEX);
    }
    return *this;
}

LUA_TYPE Table::GetType() const
{
    return LUA_TYPE_TABLE;
}

gbLuaValuePtr Table::Clone() const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    return std::make_unique<Table>(m_Lua);    //-1
}

TablePtr Table::CloneTable() const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    return std::make_unique<Table>(m_Lua);    //-1
}

int Table::GetSize() const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    DIAG_ASSERT(lua_type(m_Lua, -1) == LUA_TTABLE);
    size_t ret = lua_rawlen(m_Lua, -1);
    lua_pop(m_Lua, 1);
    return static_cast<int>(ret);
}

bool Table::IsEmpty() const
{
    return (begin() == end());
}

gbLuaValuePtr Table::Get(gbLuaValue const& key) const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    PushElement(m_Lua, key);    //+1
    lua_gettable(m_Lua, -2);    //+0
    gbLuaValuePtr ret = GetElementFromStack(m_Lua, -1);    //+0
    lua_pop(m_Lua, 2);    //-2
    return ret;
}

gbLuaValuePtr Table::Get(std::string const& key) const
{
    return Get(key.c_str());
}

gbLuaValuePtr Table::Get(char const* key) const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    lua_getfield(m_Lua, -1, key);    //+1
    gbLuaValuePtr ret = GetElementFromStack(m_Lua, -1);    //+0
    lua_pop(m_Lua, 2);    //-2
    return ret;
}

gbLuaValuePtr Table::Get(int index) const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    lua_rawgeti(m_Lua, -1, index);    //+1
    gbLuaValuePtr ret = GetElementFromStack(m_Lua, -1);    //+0
    lua_pop(m_Lua, 2);    //-2
    return ret;
}

TablePtr Table::GetTable(gbLuaValue const& key) const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    PushElement(m_Lua, key);    //+1
    lua_gettable(m_Lua, -2);    //+0
    switch(lua_type(m_Lua, -1)) {
    case LUA_TTABLE:
        break;
    case LUA_TNIL:
        lua_pop(m_Lua, 1); //-1
        lua_newtable(m_Lua); //+1
        lua_pushvalue(m_Lua, -1); //+1
        PushElement(m_Lua, key); //+1
        lua_settable(m_Lua, -2); //-2
        break;
    default:
        lua_pop(m_Lua, 2);
        DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError(),
                              "Key does not name a table");
    }
    TablePtr ret = std::make_unique<Table>(m_Lua);    //-1
    lua_pop(m_Lua, 1);    //-1
    return ret;
}

TablePtr Table::GetTable(std::string const& key) const
{
    return GetTable(key.c_str());
}

TablePtr Table::GetTable(char const* key) const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    lua_getfield(m_Lua, -1, key);    //+1
    switch(lua_type(m_Lua, -1)) {
    case LUA_TTABLE:
        break;
    case LUA_TNIL:
        lua_pop(m_Lua, 1); //-1
        lua_newtable(m_Lua); //+1
        lua_pushvalue(m_Lua, -1); //+1
        lua_setfield(m_Lua, -2, key);    //-1
        break;
    default:
        lua_pop(m_Lua, 2);
        DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError(),
                              "Key does not name a table");
    }
    TablePtr ret = std::make_unique<Table>(m_Lua);    //-1
    lua_pop(m_Lua, 1);    //-1
    return ret;
}

TablePtr Table::GetTable(int index) const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    lua_rawgeti(m_Lua, -1, index);    //+1
    switch(lua_type(m_Lua, -1)) {
    case LUA_TTABLE:
        break;
    case LUA_TNIL:
        lua_pop(m_Lua, 1); //-1
        lua_newtable(m_Lua); //+1
        lua_pushvalue(m_Lua, -1); //+1
        lua_rawseti(m_Lua, -2, index);    //-1
        break;
    default:
        lua_pop(m_Lua, 2);
        DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError(),
                              "Key does not name a table");
    }
    TablePtr ret = std::make_unique<Table>(m_Lua);    //-1
    lua_pop(m_Lua, 1);    //-1
    return ret;
}

void Table::Set(gbLuaValue const& key, gbLuaValue const& value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    PushElement(m_Lua, key);
    PushElement(m_Lua, value);
    lua_settable(m_Lua, -3);
    lua_pop(m_Lua, 1);
}

void Table::Set(gbLuaValue const& key, gbLuaValue&& value)
{
    Set(key, value);
}

void Table::Set(std::string const& key, gbLuaValue const& value)
{
    Set(key.c_str(), value);
}

void Table::Set(std::string const& key, gbLuaValue&& value)
{
    Set(key.c_str(), std::move(value));
}

void Table::Set(std::string const& key, double value)
{
    Set(key.c_str(), value);
}

void Table::Set(std::string const& key, std::string const& value)
{
    Set(key.c_str(), value);
}

void Table::Set(std::string const& key, char const* value)
{
    Set(key.c_str(), value);
}

void Table::Set(char const* key, gbLuaValue const& value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    PushElement(m_Lua, value);
    lua_setfield(m_Lua, -2, key);
    lua_pop(m_Lua, 1);
}

void Table::Set(char const* key, gbLuaValue&& value)
{
    Set(key, value);
}

void Table::Set(char const* key, double value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    lua_pushnumber(m_Lua, value);
    lua_setfield(m_Lua, -2, key);
    lua_pop(m_Lua, 1);
}

void Table::Set(char const* key, std::string const& value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    lua_pushlstring(m_Lua, value.c_str(), value.length());
    lua_setfield(m_Lua, -2, key);
    lua_pop(m_Lua, 1);
}

void Table::Set(char const* key, char const* value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    lua_pushstring(m_Lua, value);
    lua_setfield(m_Lua, -2, key);
    lua_pop(m_Lua, 1);
}

void Table::Set(int index, gbLuaValue const& value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    PushElement(m_Lua, value);
    lua_rawseti(m_Lua, -2, index);
    lua_pop(m_Lua, 1);
}

void Table::Set(int index, gbLuaValue&& value)
{
    Set(index, value);
}

void Table::Set(int index, double value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    lua_pushnumber(m_Lua, value);
    lua_rawseti(m_Lua, -2, index);
    lua_pop(m_Lua, 1);
}

void Table::Set(int index, std::string const& value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    lua_pushlstring(m_Lua, value.c_str(), value.length());
    lua_rawseti(m_Lua, -2, index);
    lua_pop(m_Lua, 1);
}

void Table::Set(int index, char const* value)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    lua_pushstring(m_Lua, value);
    lua_rawseti(m_Lua, -2, index);
    lua_pop(m_Lua, 1);
}

bool Table::operator==(Table const & rhs)
{
    STACK_CHECK(0);
    if(m_Lua != rhs.m_Lua) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Tried to compare Lua tables from different instances" );
    }
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, rhs.m_Ref);
    bool ret = (lua_rawequal(m_Lua, -1, -2) == 1);
    lua_pop(m_Lua, 2);
    return ret;
}

Table::iterator Table::begin() const
{
    return iterator(m_Lua, m_Ref);
}

Table::iterator Table::end() const
{
    return iterator(m_Lua, LUA_REFNIL);
}

Table::array_iterator Table::ibegin() const
{
    return array_iterator(this, 1);
}

Table::array_iterator Table::iend() const
{
    return array_iterator(this, GetSize()+1);
}

void Table::_internal_PushTable(lua_State* internal_lua_state) const
{
    STACK_CHECK(+1);
    if(internal_lua_state != m_Lua) {
        STACK_CHECK_DISABLE();
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Lua state pointer mismatch");
    }
    lua_rawgeti(internal_lua_state, LUA_REGISTRYINDEX, m_Ref);
}

void const* Table::_internal_GetPointer() const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);
    void const* ret = lua_topointer(m_Lua, -1);
    lua_pop(m_Lua, 1);
    return ret;
}

}

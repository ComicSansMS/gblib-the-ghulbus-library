/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua_exception.hpp>
#include <sstream>

namespace GB_LUA_NAMESPACE {

namespace Exceptions {

LuaException::~LuaException() throw()
{
}

char const* LuaRuntimeError::what() const throw()
{
    return "Lua runtime error";
}

char const* LuaMemoryAllocationError::what() const throw()
{
    return "Lua memory allocation error";
}

char const* LuaErrorInMessageHandler::what() const throw()
{
    return "Lua error while running error message handler";
}

char const* LuaErrorInGarbageCollectorMetamethod::what() const throw()
{
    return "Lua error while running garbage collector metamethod";
}

char const* LuaSyntaxError::what() const throw()
{
    return "Syntax error in Lua code";
}

inline std::string LuaTypeErrorErrorMsg(LUA_TYPE expected, LUA_TYPE actual) {
    std::ostringstream err;
    err << "Lua type mismatch (expected: " << expected << "; actual: " << actual << ")";
    return err.str();
}

LuaTypeError::LuaTypeError(LUA_TYPE expected, LUA_TYPE actual)
    :expected_(expected), actual_(actual),
     errmsg_(LuaTypeErrorErrorMsg(expected, actual))
{
}

LuaTypeError::LuaTypeError(LUA_TYPE expected, gbLuaValue const& value)
    :expected_(expected), actual_(value.GetType()),
     errmsg_(LuaTypeErrorErrorMsg(expected, value.GetType()))
{
}

LuaTypeError::LuaTypeError(LUA_TYPE expected, gbLuaValuePtr const& value)
    :expected_(expected), actual_(value->GetType()),
     errmsg_(LuaTypeErrorErrorMsg(expected, value->GetType()))
{
}

LuaTypeError::~LuaTypeError() throw()
{
}

LUA_TYPE LuaTypeError::GetExpectedType() const
{
    return expected_;
}

LUA_TYPE LuaTypeError::GetActualType() const
{
    return actual_;
}

char const* LuaTypeError::what() const throw()
{
    return errmsg_.c_str();
}

}
}



/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/stack_monitor.hpp>

#include <diagnostic/diagnostic.hpp>

#include <lua.hpp>

namespace GB_LUA_NAMESPACE {

StackMonitor::StackMonitor(lua_State* lua, int expected_stack_change)
    :m_Lua(lua), m_ExpectedStackValue(lua_gettop(lua) + expected_stack_change), m_DoCheck(true), m_Func("")
{
}

StackMonitor::StackMonitor(lua_State* lua, int expected_stack_change, char const* function_name)
    :m_Lua(lua), m_ExpectedStackValue(lua_gettop(lua) + expected_stack_change), m_DoCheck(true),
     m_Func((function_name)?(function_name):"")
{
}

StackMonitor::~StackMonitor()
{
    if(m_DoCheck) {
        int const stack_value = lua_gettop(m_Lua);
        if(stack_value != m_ExpectedStackValue) {
            if(m_Func.empty()) {
                DIAG_LOG(WARNING, "Possible Lua stack corruption; Stack off by " << (stack_value - m_ExpectedStackValue));
            } else {
                DIAG_LOG(WARNING, "Possible Lua stack corruption in " << m_Func.c_str() <<
                                  "; Stack off by " << (stack_value - m_ExpectedStackValue));
            }
        }
    }
}

void StackMonitor::Release()
{
    m_DoCheck = false;
}

}


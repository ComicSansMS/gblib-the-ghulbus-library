/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua_value.hpp>
#include <gbLua/lua_exception.hpp>
#include <gbLua/lua.hpp>
#include <gbLua/stack_monitor.hpp>
#include <gbLua/stack_guard.hpp>
#ifdef GB_LUA_DIAGNOSTIC_STACK_CONSISTENCY_CHECKS_ENABLED
#    define STACK_CHECK(n) ::GB_LUA_NAMESPACE::StackMonitor const localStackMonitor(m_Lua, n)
#    define STACK_CHECK_DISABLE() const_cast<StackMonitor&>(localStackMonitor).Release()
#else
#    define STACK_CHECK(n) void(0)
#    define STACK_CHECK_DISABLE() void(0)
#endif
#define STACK_GUARD(n) ::GB_LUA_NAMESPACE::StackGuard const localStackGuard(m_Lua, n)

#include <diagnostic/assert.hpp>

#include <utility>

#include <lua.hpp>

#define GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_
#include "lua_internal.hpp"
#undef GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_

namespace GB_LUA_NAMESPACE {

Table::iterator_base::iterator_base(lua_State* lua, int table_ref)
    :m_Lua(lua), m_Ref(table_ref)
{
    STACK_CHECK(0);
    // lua's next() returns first key if index is nil
    const_cast<std::unique_ptr<gbLuaValue const>&>(m_CurrentEntry.key) = std::make_unique<Nil>();
    next();
}

Table::iterator_base::iterator_base(iterator_base const& it)
    :m_Lua(it.m_Lua), m_Ref(it.m_Ref), m_CurrentEntry(it.m_CurrentEntry)
{
}

Table::iterator_base::iterator_base(iterator_base&& it)
    :m_Lua(it.m_Lua), m_Ref(it.m_Ref), m_CurrentEntry(std::move(it.m_CurrentEntry))
{
}

bool Table::iterator_base::operator==(iterator_base const& rhs) const
{
    if((m_Lua != rhs.m_Lua) || (m_Ref != rhs.m_Ref)) {
        return false;
    }
    // end iterators have m_Ref == REFNIL
    if(m_Ref == LUA_REFNIL) { return true; }
    // compare key types
    LUA_TYPE const key_type = m_CurrentEntry.key->GetType();
    if(key_type != rhs.m_CurrentEntry.key->GetType()) {
        return false;
    }
    // compare keys
    if(key_type == LUA_TYPE_NUMBER) {
        return (dynamic_cast<Number const&>(*m_CurrentEntry.key).Get() == dynamic_cast<Number const&>(*rhs.m_CurrentEntry.key).Get());
    } else {
        DIAG_ASSERT(key_type == LUA_TYPE_STRING);
        return (dynamic_cast<String const&>(*m_CurrentEntry.key).Get() == dynamic_cast<String const&>(*rhs.m_CurrentEntry.key).Get());
    }
}

bool Table::iterator_base::operator!=(iterator_base const& rhs) const
{
    return !(operator==(rhs));
}

void Table::iterator_base::next()
{
    STACK_CHECK(0);
    STACK_GUARD(0);
    if(m_Ref != LUA_REFNIL) {
        lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
        PushElement(m_Lua, *m_CurrentEntry.key);
        int const ret = lua_next(m_Lua, -2);        //-1,+[0|2]
        if(ret != 0) { 
            const_cast<std::unique_ptr<gbLuaValue const>&>(m_CurrentEntry.key) = GetElementFromStack(m_Lua, -2);
            m_CurrentEntry.value = GetElementFromStack(m_Lua, -1);
            lua_pop(m_Lua, 3);    //-3
        } else {
            // end
            m_Ref = LUA_REFNIL;
            lua_pop(m_Lua, 1);
        }
    }
}

Table::iterator::iterator(lua_State* lua, int table_ref)
    :iterator_base(lua, table_ref)
{
}

Table::iterator::iterator(iterator const& it)
    :iterator_base(it)
{
}

Table::iterator::iterator(iterator&& it)
    :iterator_base(std::move(it))
{
}

Table::iterator& Table::iterator::operator++()
{
    next();
    return *this;
}

Table::iterator Table::iterator::operator++(int)
{
    iterator ret(*this);
    next();
    return ret;
}

Table::iterator::reference Table::iterator::operator*()
{
    return m_CurrentEntry;
}

Table::iterator::pointer Table::iterator::operator->()
{
    return &m_CurrentEntry;
}

//*
bool Table::array_iterator_base::operator==(array_iterator_base const& rhs) const
{
    DIAG_CHECK(m_Table == rhs.m_Table);
    return m_CurrentIndex == rhs.m_CurrentIndex;
}

bool Table::array_iterator_base::operator!=(array_iterator_base const& rhs) const
{
    DIAG_CHECK(m_Table == rhs.m_Table);
    return m_CurrentIndex != rhs.m_CurrentIndex;
}

bool Table::array_iterator_base::operator<(array_iterator_base const& rhs) const
{
    DIAG_CHECK(m_Table == rhs.m_Table);
    return m_CurrentIndex < rhs.m_CurrentIndex;
}

bool Table::array_iterator_base::operator>(array_iterator_base const& rhs) const
{
    DIAG_CHECK(m_Table == rhs.m_Table);
    return m_CurrentIndex > rhs.m_CurrentIndex;
}

bool Table::array_iterator_base::operator<=(array_iterator_base const& rhs) const
{
    DIAG_CHECK(m_Table == rhs.m_Table);
    return m_CurrentIndex <= rhs.m_CurrentIndex;
}

bool Table::array_iterator_base::operator>=(array_iterator_base const& rhs) const
{
    DIAG_CHECK(m_Table == rhs.m_Table);
    return m_CurrentIndex >= rhs.m_CurrentIndex;
}

Table::array_iterator_base::array_iterator_base(array_iterator_base const& it)
    :m_Table(it.m_Table), m_CurrentIndex(it.m_CurrentIndex)
{
}

Table::array_iterator_base::array_iterator_base(Table const* t, int start_index)
    :m_Table(t), m_CurrentIndex(start_index)
{
}

Table::array_iterator::array_iterator(array_iterator const& it)
    :array_iterator_base(it.m_Table, it.m_CurrentIndex)
{
}

Table::array_iterator& Table::array_iterator::operator++()
{
    ++m_CurrentIndex;
    return *this;
}

Table::array_iterator Table::array_iterator::operator++(int)
{
    array_iterator ret(*this);
    ++m_CurrentIndex;
    return ret;
}

Table::array_iterator& Table::array_iterator::operator--()
{
    --m_CurrentIndex;
    return *this;
}

Table::array_iterator Table::array_iterator::operator--(int)
{
    array_iterator ret(*this);
    --m_CurrentIndex;
    return ret;
}

Table::array_iterator Table::array_iterator::operator+=(difference_type offset)
{
    m_CurrentIndex += offset;
    return *this;
}

Table::array_iterator Table::array_iterator::operator+(difference_type offset) const
{
    return array_iterator(m_Table, m_CurrentIndex + offset);
}

Table::array_iterator Table::array_iterator::operator-=(difference_type offset)
{
    m_CurrentIndex -= offset;
    return *this;
}

Table::array_iterator Table::array_iterator::operator-(difference_type offset) const
{
    return array_iterator(m_Table, m_CurrentIndex - offset);
}

Table::array_iterator::difference_type Table::array_iterator::operator-(array_iterator const& it) const
{
    return m_CurrentIndex - it.m_CurrentIndex;
}

Table::array_iterator::value_type Table::array_iterator::operator[](difference_type offset) const
{
    IndexValuePair ret;
    ret.index = m_CurrentIndex + offset;
    ret.value = m_Table->Get(m_CurrentIndex+offset);
    return ret;
}

Table::array_iterator::reference Table::array_iterator::operator*()
{
    if(m_CurrentEntry.index != m_CurrentIndex) {
        m_CurrentEntry.value = m_Table->Get(m_CurrentIndex);
        m_CurrentEntry.index = m_CurrentIndex;
    }
    return m_CurrentEntry;
}

Table::array_iterator::pointer Table::array_iterator::operator->()
{
    if(m_CurrentEntry.index != m_CurrentIndex) {
        m_CurrentEntry.value = m_Table->Get(m_CurrentIndex);
        m_CurrentEntry.index = m_CurrentIndex;
    }
    return &m_CurrentEntry;
}

Table::array_iterator::array_iterator(Table const* t, int start_index)
    :array_iterator_base(t, start_index)
{
}
//*/
}

/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua.hpp>
#include <gbLua/stack_monitor.hpp>
#include <gbLua/stack_guard.hpp>
#ifdef GB_LUA_DIAGNOSTIC_STACK_CONSISTENCY_CHECKS_ENABLED
#    define STACK_CHECK(n) ::GB_LUA_NAMESPACE::StackMonitor const localStackMonitor(m_Lua, n, DIAG_HELPER_LOCATION_INFO_FUNCTION)
#    define STACK_CHECK_DISABLE() const_cast<StackMonitor&>(localStackMonitor).Release()
#else
#    define STACK_CHECK(n) void(0)
#    define STACK_CHECK_DISABLE() void(0)
#endif
#define STACK_GUARD(n) ::GB_LUA_NAMESPACE::StackGuard const localStackGuard(m_Lua, n)

#include <diagnostic/diagnostic.hpp>
#include <gbUtil/missing_features.hpp>

#include <lua.hpp>

#define GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_
#include "lua_internal.hpp"
#undef GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_

using GB_LUA_NAMESPACE::Exception_Info::info_lua_error;

namespace GB_LUA_NAMESPACE {

gbLua::gbLua()
    :m_Lua(luaL_newstate())
{
    Init();
}

gbLua::gbLua(std::string const& file)
    :m_Lua(luaL_newstate())
{
    Init();
    RunFile(file);
}

gbLua::~gbLua()
{
    if(m_Lua) {
        lua_close(m_Lua);
    }
}

void gbLua::Init()
{
    STACK_CHECK(0);
    lua_gc(m_Lua, LUA_GCSTOP, 0);    // stop collector during initialization
    OpenStandardLibrary(LUA_STDLIB_BASE);
    OpenStandardLibrary(LUA_STDLIB_TABLE);
    OpenStandardLibrary(LUA_STDLIB_STRING);
    OpenStandardLibrary(LUA_STDLIB_BIT32);
    OpenStandardLibrary(LUA_STDLIB_MATH);
    lua_gc(m_Lua, LUA_GCRESTART, 0);
}

void gbLua::OpenStandardLibrary(StandardLibraryRegisterInfo const& lib_info)
{
    switch(lib_info.module) {
    case LUA_STDLIB_BASE:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? "_G" : lib_info.name, luaopen_base));
        break;
    case LUA_STDLIB_BIT32:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_BITLIBNAME : lib_info.name, luaopen_bit32));
        break;
    case LUA_STDLIB_COROUTINE:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_COLIBNAME : lib_info.name, luaopen_coroutine));
        break;
    case LUA_STDLIB_DEBUG:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_DBLIBNAME : lib_info.name, luaopen_debug));
        break;
    case LUA_STDLIB_IO:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_IOLIBNAME : lib_info.name, luaopen_io));
        break;
    case LUA_STDLIB_MATH:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_MATHLIBNAME : lib_info.name, luaopen_math));
        break;
    case LUA_STDLIB_OS:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_OSLIBNAME : lib_info.name, luaopen_os));
        break;
    case LUA_STDLIB_PACKAGE:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_LOADLIBNAME : lib_info.name, luaopen_package));
        break;
    case LUA_STDLIB_STRING:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_STRLIBNAME : lib_info.name, luaopen_string));
        break;
    case LUA_STDLIB_TABLE:
        OpenLibrary(LibraryRegisterInfo(lib_info.name.empty() ? LUA_TABLIBNAME : lib_info.name, luaopen_table));
        break;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(), "Unknown library parameter");
    }
}

void gbLua::OpenLibrary(LibraryRegisterInfo const& lib_info)
{
    STACK_CHECK(0);
    STACK_GUARD(0);
    // call open functions and set results to global table
    luaL_requiref(m_Lua, lib_info.name.c_str(), lib_info.func, 1);
    gbLuaValuePtr lib = GetElementFromStack(m_Lua, -1);
    if(lib->GetType() == LUA_TYPE_NIL) {
        DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError(),
                              std::string("Error loading library ") + lib_info.name + " into Lua" );
    }
    lua_pop(m_Lua, 1);
    m_LoadedLibs.push_back(lib_info);
}

void gbLua::PreloadLibrary(LibraryRegisterInfo const& lib_info)
{
    // add open functions into 'package.preload' table
    STACK_CHECK(0);
    luaL_getsubtable(m_Lua, LUA_REGISTRYINDEX, "_PRELOAD");
    lua_pushcfunction(m_Lua, lib_info.func);
    lua_setfield(m_Lua, -2, lib_info.name.c_str());
    lua_pop(m_Lua, 1);
    m_PreloadedLibs.push_back(lib_info);
}

void gbLua::RunFile(std::string const& file)
{
    STACK_CHECK(0);
    STACK_GUARD(0);
    int ret = luaL_loadfile(m_Lua, file.c_str());
    if(ret != LUA_OK) {
        info_lua_error err((lua_isstring(m_Lua, -1))?lua_tostring(m_Lua, -1):"Unknown error");
        Diagnostic::Exception_Info::file_name err_fname(file);
        if(ret == LUA_ERRSYNTAX) {
            DIAG_THROW_EXCEPTION( Exceptions::LuaSyntaxError() <<  err << err_fname,
                                  "Syntax error" );
        } else {
            DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError() <<  err << err_fname,
                                  "Runtime error while loading script" );
        }
    }
    ret = lua_pcall(m_Lua, 0, 0, 0);
    if(ret != LUA_OK) {
        info_lua_error err((lua_isstring(m_Lua, -1))?lua_tostring(m_Lua, -1):"Unknown error");
        Diagnostic::Exception_Info::file_name err_fname(file);
        DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError() <<  err << err_fname,
                              "Error while running script from file" );
    }
}

void gbLua::RunString(std::string const& str)
{
    RunString(str.c_str());
}

void gbLua::RunString(char const* str)
{
    STACK_CHECK(0);
    STACK_GUARD(0);
    int ret = luaL_loadstring(m_Lua, str);
    if(ret != LUA_OK) {
        info_lua_error err((lua_isstring(m_Lua, -1))?lua_tostring(m_Lua, -1):"Unknown error");
        if(ret == LUA_ERRSYNTAX) {
            DIAG_THROW_EXCEPTION( Exceptions::LuaSyntaxError() <<  err,
                                  "Syntax error" );
        } else {
            DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError() <<  err,
                                  "Runtime error while loading string" );
        }
    }
    ret = lua_pcall(m_Lua, 0, 0, 0);
    if(ret != LUA_OK) {
        info_lua_error err((lua_isstring(m_Lua, -1))?lua_tostring(m_Lua, -1):"Unknown error");
        DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError() <<  err,
                              "Error while running script from file" );
    }
}

gbLuaValuePtr gbLua::GetGlobal(std::string const& name)
{
    return GetGlobal(name.c_str());
}

gbLuaValuePtr gbLua::GetGlobal(char const* name)
{
    STACK_CHECK(0);
    lua_getglobal(m_Lua, name);
    gbLuaValuePtr ret = GetElementFromStack(m_Lua, -1);
    lua_pop(m_Lua, 1);
    return ret;
}

void gbLua::SetGlobal(std::string const& name, gbLuaValue const& val)
{
    SetGlobal(name.c_str(), val);
}

void gbLua::SetGlobal(char const* name, gbLuaValue const& val)
{
    STACK_CHECK(0);
    PushElement(m_Lua, val);
    lua_setglobal(m_Lua, name);
}

void gbLua::SetGlobal(std::string const& name, std::string const& val_str)
{
    SetGlobal(name.c_str(), String(val_str));
}

void gbLua::SetGlobal(char const* name, std::string const& val_str)
{
    SetGlobal(name, String(val_str));
}

void gbLua::SetGlobal(std::string const& name, char const* val_str)
{
    SetGlobal(name.c_str(), String(val_str));
}

void gbLua::SetGlobal(char const* name, char const* val_str)
{
    SetGlobal(name, String(val_str));
}

void gbLua::SetGlobal(std::string const& name, double val_f)
{
    SetGlobal(name.c_str(), Number(val_f));
}

void gbLua::SetGlobal(char const* name, double val_f)
{
    SetGlobal(name, Number(val_f));
}

TablePtr gbLua::CreateTable(std::string const& name)
{
    return CreateTable(name.c_str());
}

TablePtr gbLua::CreateTable(char const* name)
{
    STACK_CHECK(0);
    STACK_GUARD(0);
    lua_getglobal(m_Lua, name);    //+1
    switch(ConvertLuaType(lua_type(m_Lua, -1))) {
    case LUA_TYPE_TABLE:
        // lua table of given name exists; proceed
        break;
    case LUA_TYPE_NIL:
        // table does not exist and name is vacant; create table and proceed
        lua_newtable(m_Lua);        //+1
        lua_setglobal(m_Lua, name);    //-1
        lua_pop(m_Lua, 1);            //-1
        lua_getglobal(m_Lua, name);    //+1
        break;
    default:
        // name is occupied, throw
        DIAG_THROW_EXCEPTION( Exceptions::LuaRuntimeError(),
                              std::string("Cannot create table of name \'") + name + "\'; Name is taken" );
    }
    DIAG_ASSERT(lua_type(m_Lua, -1) == LUA_TTABLE);
    return std::make_unique<Table>(m_Lua);    //-1
}

TablePtr gbLua::CreateTable()
{
    STACK_CHECK(0);
    lua_newtable(m_Lua);    //+1
    return std::make_unique<Table>(m_Lua);    //-1
}

}


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua_value.hpp>

#include <diagnostic/diagnostic.hpp>

#include <lua.hpp>

namespace GB_LUA_NAMESPACE {

LUA_TYPE ConvertLuaType(int t)
{
    switch(t) {
    case LUA_TNIL:
        return LUA_TYPE_NIL;
    case LUA_TBOOLEAN:
        return LUA_TYPE_BOOLEAN;
    case LUA_TLIGHTUSERDATA:
        return LUA_TYPE_USERDATA;
    case LUA_TNUMBER:
        return LUA_TYPE_NUMBER;
    case LUA_TSTRING:
        return LUA_TYPE_STRING;
    case LUA_TTABLE:
        return LUA_TYPE_TABLE;
    case LUA_TFUNCTION:
        return LUA_TYPE_FUNCTION;
    case LUA_TUSERDATA:
        return LUA_TYPE_USERDATA;
    case LUA_TTHREAD:
        return LUA_TYPE_THREAD;
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Unknown Lua type identifier");
    }
}

bool IsNil(gbLuaValue const& v)
{
    return (v.GetType() == LUA_TYPE_NIL);
}

}

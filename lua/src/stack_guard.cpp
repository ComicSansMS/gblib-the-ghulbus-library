/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/stack_guard.hpp>

#include <diagnostic/diagnostic.hpp>

#include <lua.hpp>

namespace GB_LUA_NAMESPACE {

StackGuard::StackGuard(lua_State* lua)
    :m_Lua(lua), m_TargetStackSize(lua_gettop(lua)), m_DoGuard(true)
{
}

StackGuard::StackGuard(lua_State* lua, int offset)
    :m_Lua(lua), m_TargetStackSize(lua_gettop(lua) + offset), m_DoGuard(true)
{
}

StackGuard::~StackGuard()
{
    if(m_DoGuard) {
        int const stack_size_difference = lua_gettop(m_Lua) - m_TargetStackSize;
        if(stack_size_difference < 0) {
            DIAG_LOG(WARNING, "Stack guard encountered short Lua stack; Stack off by " << stack_size_difference);
        } else if(stack_size_difference > 0) {
            lua_pop(m_Lua, stack_size_difference);
        }
    }
}

void StackGuard::Release()
{
    m_DoGuard = false;
}

}


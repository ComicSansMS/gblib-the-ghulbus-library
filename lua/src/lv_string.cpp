/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua_value.hpp>
#include <gbUtil/missing_features.hpp>

namespace GB_LUA_NAMESPACE {

String::String(std::string const& str)
    :str_(str)
{
}

String::String(std::string&& str)
    :str_(std::move(str))
{
}

String::String(char const* str)
    :str_(str)
{
}

String::~String()
{
}

LUA_TYPE String::GetType() const
{
    return LUA_TYPE_STRING;
}

gbLuaValuePtr String::Clone() const
{
    return std::make_unique<String>(str_);
}

std::string const& String::Get() const
{
    return str_;
}

void String::Set(std::string const& str)
{
    str_ = str;
}

void String::Set(std::string&& str)
{
    str_.swap(str);
}

void String::Set(char const* str)
{
    str_ = str;
}

String::String(String const& rhs)
    :str_(rhs.str_)
{
}

String::String(String&& rhs)
    :str_(std::move(rhs.str_))
{
}

String& String::operator=(String const& rhs)
{
    if(&rhs != this) {
        Set(rhs.str_);
    }
    return *this;
}

}

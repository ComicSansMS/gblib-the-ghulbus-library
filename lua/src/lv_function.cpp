/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua_value.hpp>
#include <gbLua/lua_exception.hpp>
#include <gbLua/stack_monitor.hpp>
#include <gbLua/stack_guard.hpp>

#include <diagnostic/assert.hpp>

#include <gbUtil/missing_features.hpp>

#ifdef GB_LUA_DIAGNOSTIC_STACK_CONSISTENCY_CHECKS_ENABLED
#    define STACK_CHECK(n) ::GB_LUA_NAMESPACE::StackMonitor const localStackMonitor(m_Lua, n, DIAG_HELPER_LOCATION_INFO_FUNCTION)
#    define STACK_CHECK_DISABLE() const_cast<StackMonitor&>(localStackMonitor).Release()
#else
#    define STACK_CHECK(n) void(0)
#    define STACK_CHECK_DISABLE() void(0)
#endif

#include <lua.hpp>

#define GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_
#include "lua_internal.hpp"
#undef GB_LUA_INCLUDE_TOKEN_INTERNAL_HPP_

namespace GB_LUA_NAMESPACE {

namespace {
    inline void HandlePCallError(int err, std::string const& error_msg)
    {
        switch(err)
        {
        case LUA_ERRRUN:
            DIAG_THROW_EXCEPTION(Exceptions::LuaRuntimeError(), error_msg);
        case LUA_ERRMEM:
            DIAG_THROW_EXCEPTION(Exceptions::LuaMemoryAllocationError(), error_msg);
        case LUA_ERRERR:
            DIAG_THROW_EXCEPTION(Exceptions::LuaErrorInMessageHandler(), error_msg);
        case LUA_ERRGCMM:
            DIAG_THROW_EXCEPTION(Exceptions::LuaErrorInGarbageCollectorMetamethod(), error_msg);
        default:
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown error code");
        }
    }
}

Function::Function(lua_State* internal_lua_state)
    :m_Lua(internal_lua_state), m_Ref(luaL_ref(internal_lua_state, LUA_REGISTRYINDEX))
{
    DIAG_ASSERT(m_Ref != LUA_REFNIL);
    DIAG_ASSERT(m_Ref != LUA_NOREF);
}

Function::~Function()
{
    STACK_CHECK(0);
    luaL_unref(m_Lua, LUA_REGISTRYINDEX, m_Ref);
}

Function::Function(Function const& rhs)
    :m_Lua(rhs.m_Lua), m_Ref(LUA_REFNIL)
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, rhs.m_Ref);
    m_Ref = luaL_ref(m_Lua, LUA_REGISTRYINDEX);
}

Function::Function(Function&& rhs)
    :m_Lua(rhs.m_Lua), m_Ref(rhs.m_Ref)
{
    rhs.m_Ref = LUA_REFNIL;
}

Function& Function::operator=(Function const& rhs)
{
    STACK_CHECK(0);
    if(&rhs != this) {
        lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, rhs.m_Ref);
        luaL_unref(m_Lua, LUA_REGISTRYINDEX, m_Ref);
        m_Ref = luaL_ref(m_Lua, LUA_REGISTRYINDEX);
    }
    return *this;
}

LUA_TYPE Function::GetType() const
{
    return LUA_TYPE_FUNCTION;
}

gbLuaValuePtr Function::Clone() const
{
    STACK_CHECK(0);
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    return std::make_unique<Function>(m_Lua);    //-1
}

std::vector<gbLuaValuePtr> Function::operator()()
{
    STACK_CHECK(0);
    std::vector<gbLuaValuePtr> ret;
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    int stack_top = lua_gettop(m_Lua);
    int res = lua_pcall(m_Lua, 0, LUA_MULTRET, 0);        //-1 & +n_ret
    if(res != LUA_OK) {
        gbLuaValuePtr err_msg = GetElementFromStack(m_Lua, -1);
        DIAG_ASSERT(err_msg->GetType() == LUA_TYPE_STRING);
        lua_pop(m_Lua, 1);
        HandlePCallError(res, ConvertTo<String>(err_msg).Get());
    }
    int new_top = lua_gettop(m_Lua);
    for(int i=stack_top; i<=new_top; ++i) {
        ret.push_back(GetElementFromStack(m_Lua, i));
    }
    if(!ret.empty()) {
        lua_pop(m_Lua, (new_top - stack_top) + 1);    // -n_ret
    }
    return ret;
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(1);
    args.push_back(&arg1);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(2);
    args.push_back(&arg1);
    args.push_back(&arg2);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(3);
    args.push_back(&arg1);
    args.push_back(&arg2);
    args.push_back(&arg3);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(4);
    args.push_back(&arg1);
    args.push_back(&arg2);
    args.push_back(&arg3);
    args.push_back(&arg4);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(5);
    args.push_back(&arg1);
    args.push_back(&arg2);
    args.push_back(&arg3);
    args.push_back(&arg4);
    args.push_back(&arg5);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(6);
    args.push_back(&arg1);
    args.push_back(&arg2);
    args.push_back(&arg3);
    args.push_back(&arg4);
    args.push_back(&arg5);
    args.push_back(&arg6);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6, gbLuaValue const& arg7)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(7);
    args.push_back(&arg1);
    args.push_back(&arg2);
    args.push_back(&arg3);
    args.push_back(&arg4);
    args.push_back(&arg5);
    args.push_back(&arg6);
    args.push_back(&arg7);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6, gbLuaValue const& arg7, gbLuaValue const& arg8)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(8);
    args.push_back(&arg1);
    args.push_back(&arg2);
    args.push_back(&arg3);
    args.push_back(&arg4);
    args.push_back(&arg5);
    args.push_back(&arg6);
    args.push_back(&arg7);
    args.push_back(&arg8);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(gbLuaValue const& arg1, gbLuaValue const& arg2, gbLuaValue const& arg3, gbLuaValue const& arg4, gbLuaValue const& arg5, gbLuaValue const& arg6, gbLuaValue const& arg7, gbLuaValue const& arg8, gbLuaValue const& arg9)
{
    std::vector<gbLuaValue const*> args;
    args.reserve(9);
    args.push_back(&arg1);
    args.push_back(&arg2);
    args.push_back(&arg3);
    args.push_back(&arg4);
    args.push_back(&arg5);
    args.push_back(&arg6);
    args.push_back(&arg7);
    args.push_back(&arg8);
    args.push_back(&arg9);
    return (*this)(args);
}

std::vector<gbLuaValuePtr> Function::operator()(std::vector<gbLuaValue const*> const& args)
{
    STACK_CHECK(0);
    std::vector<gbLuaValuePtr> ret;
    lua_rawgeti(m_Lua, LUA_REGISTRYINDEX, m_Ref);    //+1
    int stack_top = lua_gettop(m_Lua);
    std::for_each(cbegin(args), cend(args), [this](gbLuaValue const* const arg_ptr) {
        PushElement(m_Lua, *arg_ptr);
    });                                                //+n_arg
    int res = lua_pcall(m_Lua, static_cast<int>(args.size()), LUA_MULTRET, 0);    //-(1+n_arg) & +n_ret
    if(res != LUA_OK) {
        gbLuaValuePtr err_msg = GetElementFromStack(m_Lua, -1);
        DIAG_ASSERT(err_msg->GetType() == LUA_TYPE_STRING);
        lua_pop(m_Lua, 1);
        HandlePCallError(res, ConvertTo<String>(err_msg).Get());
    }
    int new_top = lua_gettop(m_Lua);
    for(int i=stack_top; i<=new_top; ++i) {
        ret.push_back(GetElementFromStack(m_Lua, i));
    }
    if(!ret.empty()) {
        lua_pop(m_Lua, (new_top - stack_top) + 1);    // -n_ret
    }
    return ret;
}

void Function::_internal_PushTable(lua_State* internal_lua_state) const
{
    STACK_CHECK(+1);
    if(internal_lua_state != m_Lua) {
        STACK_CHECK_DISABLE();
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Lua state pointer mismatch");
    }
    lua_rawgeti(internal_lua_state, LUA_REGISTRYINDEX, m_Ref);
}

}

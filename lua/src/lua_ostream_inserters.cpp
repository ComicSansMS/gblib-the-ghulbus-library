/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gbLua/lua_value.hpp>

#include <algorithm>
#include <ostream>
#include <vector>

#include <diagnostic/diagnostic.hpp>

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::LUA_TYPE t)
{
    using namespace GB_LUA_NAMESPACE;
    switch(t) {
    case LUA_TYPE_NIL:
        return os << "nil";
    case LUA_TYPE_BOOLEAN:
        return os << "boolean";
    case LUA_TYPE_NUMBER:
        return os << "number";
    case LUA_TYPE_STRING:
        return os << "string";
    case LUA_TYPE_FUNCTION:
        return os << "function";
    case LUA_TYPE_USERDATA:
        return os << "userdata";
    case LUA_TYPE_THREAD:
        return os << "thread";
    case LUA_TYPE_TABLE:
        return os << "table";
    default:
        DIAG_ASSERT(false);
        return os;
    }
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::gbLuaValue const& v)
{
    using namespace GB_LUA_NAMESPACE;
    switch(v.GetType()) {
    case LUA_TYPE_NIL:
        return os << dynamic_cast<Nil const&>(v);
    case LUA_TYPE_BOOLEAN:
        return os << dynamic_cast<Boolean const&>(v);
    case LUA_TYPE_NUMBER:
        return os << dynamic_cast<Number const&>(v);
    case LUA_TYPE_STRING:
        return os << dynamic_cast<String const&>(v);
    case LUA_TYPE_FUNCTION:
        return os << dynamic_cast<Function const&>(v);
    case LUA_TYPE_USERDATA:
        return os << dynamic_cast<Userdata const&>(v);
    case LUA_TYPE_THREAD:
        return os << dynamic_cast<Thread const&>(v);
    case LUA_TYPE_TABLE:
        return os << dynamic_cast<Table const&>(v);
    default:
        DIAG_ASSERT(false);
        return os;
    }
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Boolean const& v)
{
    return os << "<boolean>=" << (v.Get()?"true":"false");
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Function const& /* v */)
{
    return os << "<function>";
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Nil const&)
{
    return os << "<nil>";
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Number const& v)
{
    return os << "<number>=" << v.Get();
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::String const& v)
{
    return os << "<string>=\'" << v.Get().c_str() << "\'";
}

void PrintTable(std::ostream& os, GB_LUA_NAMESPACE::Table const& v, std::vector<void const*>& depth)
{
    os << "<table>={\n";
    std::for_each(v.begin(), v.end(), [&os,&depth](GB_LUA_NAMESPACE::Table::KeyValuePair const& kv) {
        for(unsigned int i=0; i<depth.size()-1; ++i) { os << "  "; }
        if(kv.key->GetType() == GB_LUA_NAMESPACE::LUA_TYPE_TABLE) {
            os << "[<table>]=";
        } else {
            os << "[" << (*kv.key) << "] = ";
        }
        if(kv.value->GetType() != GB_LUA_NAMESPACE::LUA_TYPE_TABLE) {
            os << (*kv.value) << "\n";
        } else {
            void const* t_ptr = dynamic_cast<GB_LUA_NAMESPACE::Table const&>(*kv.value)._internal_GetPointer();
            bool cyclical_ref = false;
            for(unsigned int i=0; i<depth.size(); ++i) {
                if(depth[i] == t_ptr) {
                    os << "<table>=(lvl" << i << ")\n";
                    cyclical_ref = true;
                    break;
                }
            }
            if(!cyclical_ref) {
                depth.push_back(t_ptr);
                PrintTable(os, dynamic_cast<GB_LUA_NAMESPACE::Table const&>(*kv.value), depth);
                depth.pop_back();
            }
        }
    });
    for(unsigned int i=0; i<depth.size()-1; ++i) { os << "  "; }
    os << "}\n";
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Table const& v)
{
    std::vector<void const*> depth_record;
    depth_record.push_back(v._internal_GetPointer());
    PrintTable(os, v, depth_record);
    return os;
}

std::ostream& operator<<(std::ostream& os, GB_LUA_NAMESPACE::Thread const& /* v */)
{
    return os << "<thread>";
}


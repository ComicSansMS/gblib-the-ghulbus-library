include(CMakeParseArguments)
include(GenerateExportHeader)

function(gbLib_SetVersionNumber PREFIX VERSION_MAJOR VERSION_MINOR VERSION_PATCH)
  # version number
  set(${PREFIX}_VERSION_MAJOR ${VERSION_MAJOR} PARENT_SCOPE)
  set(${PREFIX}_VERSION_MINOR ${VERSION_MINOR} PARENT_SCOPE)
  set(${PREFIX}_VERSION_PATCH ${VERSION_PATCH} PARENT_SCOPE)
  set(${PREFIX}_VERSION
    "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}"
    PARENT_SCOPE)
endfunction()

function(gbLib_ResolveDependencies NAME)
  if(NOT TARGET ${NAME})
    message(FATAL_ERROR "'${NAME}' is not a valid target")
  endif()
  foreach(dep ${ARGN})
    if(NOT TARGET ${dep})
        message(FATAL_ERROR "Unknown dependency '${dep}' requested for ${NAME}")
    endif()
    if(NOT __LIB_${dep}_IS_HEADER_ONLY)
        list(APPEND target_LINK_DEPENDENCIES ${dep})
    endif()
  endforeach()
  target_link_libraries(${NAME} ${target_LINK_DEPENDENCIES})
endfunction()

function(gbLib_AddLibrary NAME)
  cmake_parse_arguments(arg
    "STATIC;NO_INSTALL"
    "EXPORT_HEADER"
    "SOURCES;HEADERS;TESTS;INCLUDE_DIRECTORIES;PUBLIC_INCLUDE_DIRECTORIES;DEPENDENCIES;LIBRARIES;DLLS;DEFINES"
    ${ARGN})

  # add library target
  if(arg_SOURCES)
    if(arg_STATIC)
      # static lib
      add_library(${NAME} ${arg_SOURCES} ${arg_HEADERS})
      set_property(TARGET ${NAME} APPEND PROPERTY COMPILE_DEFINITIONS "GBLIB_${NAME}_STATIC_DEFINE")
    else()
      # dynamic lib + export header
      add_library(${NAME} SHARED ${arg_SOURCES} ${arg_HEADERS})
      add_compiler_export_flags(tmp_EXPORT_FLAGS)
      set_property(TARGET ${NAME} APPEND PROPERTY COMPILE_FLAGS ${tmp_EXPORT_FLAGS})
      generate_export_header(${NAME} BASE_NAME "GBLIB_${NAME}")
      set_property(TARGET ${NAME} APPEND PROPERTY INCLUDE_DIRECTORIES ${CMAKE_CURRENT_BINARY_DIR})
    endif()
  else()
    # header-only library
    add_custom_target(${NAME} SOURCES ${arg_HEADERS})
    set_property(TARGET ${NAME} PROPERTY GBLIB_IS_HEADER_ONLY "TRUE")
  endif()

  # dependencies
  foreach(dep ${arg_DEPENDENCIES})
    if(NOT TARGET ${dep})
      message(FATAL_ERROR "Dependency '${dep}' for ${NAME} does not name a valid target")
    endif()
    get_property(dep_includes TARGET ${dep} PROPERTY INCLUDE_DIRECTORIES)
    list(APPEND target_INCLUDE_DIRECTORIES ${dep_includes})
    get_property(is_header_only TARGET ${dep} PROPERTY GBLIB_IS_HEADER_ONLY)
    if(NOT is_header_only)
        list(APPEND target_LINK_DEPENDENCIES ${dep})
    endif()
  endforeach()
  target_link_libraries(${NAME} ${target_LINK_DEPENDENCIES})

  list(APPEND target_INCLUDE_DIRECTORIES ${arg_INCLUDE_DIRECTORIES} ${arg_PUBLIC_INCLUDE_DIRECTORIES})
  list(REMOVE_DUPLICATES target_INCLUDE_DIRECTORIES)
  target_include_directories(${NAME} PUBLIC ${target_INCLUDE_DIRECTORIES})
  
  # libraries
  target_link_libraries(${NAME} ${arg_LIBRARIES})

  # dlls
  if(WIN32)
    foreach(dll ${arg_DLLS})
      if(NOT EXISTS ${dll})
        message(FATAL_ERROR "Required dll not found ${dll}")
      endif()
    endforeach()
    file(COPY ${arg_DLLS} DESTINATION ${PROJECT_BINARY_DIR})
  endif()

  # compiler-specific options
  if((CMAKE_CXX_COMPILER_ID STREQUAL "Clang") OR (CMAKE_CXX_COMPILER_ID STREQUAL "GNU"))
    set_property(TARGET ${NAME} APPEND PROPERTY COMPILE_FLAGS "-pedantic -Wall")
  endif()
  if(MSVC)
    set_property(TARGET ${NAME} APPEND PROPERTY COMPILE_FLAGS "/W4")
  endif()
  
  # defines
  if(arg_DEFINES)
    target_compile_definitions(${NAME} PUBLIC ${arg_DEFINES})
  endif()

  # tests
  if(gbLib_BUILD_TESTS AND arg_TESTS)
    add_executable(test_${NAME} ${arg_TESTS})
    if(NOT WIN32)
      find_package(Threads REQUIRED)
      set(GTEST_ADDITIONAL_DEPENDENCIES ${CMAKE_THREAD_LIBS_INIT})
    endif()
    target_link_libraries(test_${NAME} gtest gtest_main ${GTEST_ADDITIONAL_DEPENDENCIES})
    if(arg_SOURCES)
      target_link_libraries(test_${NAME} ${NAME})
    endif()
    get_property(gtest_includes TARGET gtest PROPERTY INCLUDE_DIRECTORIES)
    set_property(TARGET test_${NAME} APPEND PROPERTY
      INCLUDE_DIRECTORIES ${CMAKE_CURRENT_BINARY_DIR} ${gtest_includes})
    set_property(TARGET test_${NAME} APPEND PROPERTY COMPILE_DEFINITIONS GTEST_HAS_TR1_TUPLE=0)
    add_test(NAME "Test${NAME}" COMMAND test_${NAME})
  endif()
  
  if(NOT arg_NO_INSTALL)
    install(TARGETS ${NAME} DESTINATION lib)
    #install(FILES ${arg_HEADERS} DESTINATION include)
    foreach(inc_dir ${arg_PUBLIC_INCLUDE_DIRECTORIES})
      install(DIRECTORY ${inc_dir} DESTINATION . 
              FILES_MATCHING PATTERN "*.hpp" PATTERN "*.h"
              PATTERN "_template.*" EXCLUDE)
    endforeach()
    export(TARGETS ${arg_DEPENDENCIES} ${NAME} FILE "${CMAKE_BINARY_DIR}/cmake/${NAME}Targets.cmake")
    install(FILES "${CMAKE_BINARY_DIR}/cmake/${NAME}Targets.cmake" DESTINATION cmake)
  endif()
endfunction()

function(gbLib_getDllFromLib out filepath dll_suffix)
  if(WIN32)
    foreach(element ${filepath})
      if(${element} STREQUAL "debug")
        set(in_debug TRUE)
        set(in_optimized FALSE)
      elseif(${element} STREQUAL "optimized")
        set(in_debug FALSE)
        set(in_optimized TRUE)
      else()
        get_filename_component(file_base ${element} PATH)
        get_filename_component(file_name ${element} NAME_WE)
        set(acc ${acc} "${file_base}/${file_name}${dll_suffix}.dll")
        if(in_debug)
          set(acc_deb ${acc_deb} "${file_base}/${file_name}${dll_suffix}.dll")
        elseif(in_optimized)
          set(acc_rel ${acc_rel} "${file_base}/${file_name}${dll_suffix}.dll")
        endif()
        set(in_debug FALSE)
        set(in_optimized FALSE)
      endif()
    endforeach()
    set(${out} ${acc} PARENT_SCOPE)
    set(${out}_DEBUG ${acc_deb} PARENT_SCOPE)
    set(${out}_OPTIMIZED ${acc_rel} PARENT_SCOPE)
  else()
    set(${out} "" PARENT_SCOPE)
    set(${out}_DEBUG "" PARENT_SCOPE)
    set(${out}_OPTIMIZED "" PARENT_SCOPE)
  endif()
endfunction()

# Initial setup
enable_testing()
add_definitions(-DBOOST_ALL_NO_LIB)
option(gbLib_BUILD_TESTS "Toggles building of unit tests" ON)
install(DIRECTORY ${CMAKE_CURRENT_LIST_DIR} DESTINATION .)

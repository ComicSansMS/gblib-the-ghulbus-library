# - Locate FreeType library
# This module defines
#  FREETYPE24_LIBRARIES, the library to link against
#  FREETYPE24_FOUND, if false, do not try to link to FREETYPE
#  FREETYPE24_INCLUDE_DIRS, where to find headers.
#  This is the concatenation of the paths:
#  FREETYPE24_INCLUDE_DIR_ft2build
#  FREETYPE24_INCLUDE_DIR_freetype2
#
# $FREETYPE_DIR is an environment variable that would
# correspond to the ./configure --prefix=$FREETYPE_DIR
# used in building FREETYPE.

#=============================================================================
# Copyright 2007-2009 Kitware, Inc.
#
# Distributed under the OSI-approved BSD License (the "License");
# see accompanying file Copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the License for more information.
#=============================================================================
# (To distribute this file outside of CMake, substitute the full
#  License text for the above reference.)

# Created by Eric Wing.
# Modifications by Alexander Neundorf.
# This file has been renamed to "FindFreetype.cmake" instead of the correct
# "FindFreeType.cmake" in order to be compatible with the one from KDE4, Alex.
# Modified by Andreas Weis.

# Ugh, FreeType seems to use some #include trickery which
# makes this harder than it should be. It looks like they
# put ft2build.h in a common/easier-to-find location which
# then contains a #include to a more specific header in a
# more specific location (#include <freetype/config/ftheader.h>).
# Then from there, they need to set a bunch of #define's
# so you can do something like:
# #include FT_FREETYPE_H
# Unfortunately, using CMake's mechanisms like INCLUDE_DIRECTORIES()
# wants explicit full paths and this trickery doesn't work too well.
# I'm going to attempt to cut out the middleman and hope
# everything still works.
FIND_PATH(FREETYPE24_INCLUDE_DIR_ft2build ft2build.h
  HINTS
  $ENV{FREETYPE_DIR}
  $ENV{FREETYPE_DIR}/include
  PATHS
  /usr/local/X11R6/include
  /usr/local/X11/include
  /usr/freeware/include
)

FIND_PATH(FREETYPE24_INCLUDE_DIR_freetype2 freetype/config/ftheader.h
  HINTS
  $ENV{FREETYPE_DIR}/include/freetype2
  $ENV{FREETYPE_DIR}/include
  PATHS
  /usr/local/X11R6/include
  /usr/local/X11/include
  /usr/freeware/include
  PATH_SUFFIXES freetype2
)

FIND_LIBRARY(FREETYPE24_LIBRARY
  NAMES freetype libfreetype freetype248
  HINTS
  $ENV{FREETYPE_DIR}
  $ENV{FREETYPE_DIR}/objs/win32
  PATH_SUFFIXES lib64 lib vc2010 vc2008 vc2005
  PATHS
  /usr/local/X11R6
  /usr/local/X11
  /usr/freeware
)

# set the user variables
IF(FREETYPE24_INCLUDE_DIR_ft2build AND FREETYPE24_INCLUDE_DIR_freetype2)
  SET(FREETYPE24_INCLUDE_DIRS "${FREETYPE24_INCLUDE_DIR_ft2build};${FREETYPE24_INCLUDE_DIR_freetype2}")
ENDIF(FREETYPE24_INCLUDE_DIR_ft2build AND FREETYPE24_INCLUDE_DIR_freetype2)
SET(FREETYPE24_LIBRARIES "${FREETYPE24_LIBRARY}")

# handle the QUIETLY and REQUIRED arguments and set FREETYPE24_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE("${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake")
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Freetype  DEFAULT_MSG  FREETYPE24_LIBRARY  FREETYPE24_INCLUDE_DIRS)


MARK_AS_ADVANCED(FREETYPE24_LIBRARY FREETYPE24_INCLUDE_DIR_freetype2 FREETYPE24_INCLUDE_DIR_ft2build)

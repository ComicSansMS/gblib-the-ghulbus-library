
#ifndef GB_MATH_INCLUDE_GUARD_AABB_HPP_
#define GB_MATH_INCLUDE_GUARD_AABB_HPP_

#include <gbMath/config.hpp>
#include <gbMath/vector3.hpp>
#include <gbMath/util.hpp>

#include <algorithm>

namespace GB_MATH_NAMESPACE {
    template<typename T=GB_MATH_BASE_TYPE>
    class AABB {
    public:
        enum Axis {
            AXIS_X=0,
            AXIS_Y=1,
            AXIS_Z=2
        };
    private:
        Vector3<T> min_, max_;
    public:
        AABB()
            :min_(MaxOrInf<T>(), MaxOrInf<T>(), MaxOrInf<T>()),
             max_(-MaxOrInf<T>(), -MaxOrInf<T>(), -MaxOrInf<T>())
        {
        }

        AABB(Vector3<T> const& p)
            :min_(p), max_(p)
        {
        }

        AABB(Vector3<T> const& p1, Vector3<T> const& p2)
            :min_( std::min(p1.x_, p2.x_), std::min(p1.y_, p2.y_), std::min(p1.z_, p2.z_) ),
             max_( std::max(p1.x_, p2.x_), std::max(p1.y_, p2.y_), std::max(p1.z_, p2.z_) )
        {
        }

        Vector3<T> const& GetMin() const
        {
            return min_;
        }

        Vector3<T> const& GetMax() const
        {
            return max_;
        }

        Vector3<T> GetExtent() const
        {
            return max_ - min_;
        }

        Vector3<T> GetHalfAxes() const
        {
            return ((max_ + min_) * (static_cast<T>(1)/static_cast<T>(2)));
        }

        AABB<T> GetUnion(Vector3<T> const& v) const
        {
            AABB ret(*this);
            ret.UnionSelf(v);
            return ret;
        }

        void UnionSelf(Vector3<T> const& v)
        {
            min_.x_ = std::min(min_.x_, v.x_);
            min_.y_ = std::min(min_.y_, v.y_);
            min_.z_ = std::min(min_.z_, v.z_);

            max_.x_ = std::max(max_.x_, v.x_);
            max_.y_ = std::max(max_.y_, v.y_);
            max_.z_ = std::max(max_.z_, v.z_);
        }

        AABB<T> GetUnion(AABB<T> const& b) const
        {
            AABB<T> ret(*this);
            ret.Union(b);
            return ret;
        }

        void UnionSelf(AABB<T> const& b)
        {
            min_.x_ = std::min(min_.x_, b.min_.x_);
            min_.y_ = std::min(min_.y_, b.min_.y_);
            min_.z_ = std::min(min_.z_, b.min_.z_);

            max_.x_ = std::max(max_.x_, b.max_.x_);
            max_.y_ = std::max(max_.y_, b.max_.y_);
            max_.z_ = std::max(max_.z_, b.max_.z_);
        }

        bool Overlaps(AABB<T> const& b) const
        {
            bool x = (max_.x_ >= b.min_.x_) && (min_.x_ <= b.max_.x_);
            bool y = (max_.y_ >= b.min_.y_) && (min_.y_ <= b.max_.y_);
            bool z = (max_.z_ >= b.min_.z_) && (min_.z_ <= b.max_.z_);
            return (x && y && z);
        }

        bool Inside(Vector3<T> const& v) const
        {
            return ( (v.x_ >= min_.x_) && (v.x_ <= max_.x_) &&
                     (v.y_ >= min_.y_) && (v.y_ <= max_.y_) &&
                     (v.z_ >= min_.z_) && (v.z_ <= max_.z_) );
        }

        void Expand(T delta)
        {
            min_ -= Vector3<T>(delta, delta, delta);
            max_ += Vector3<T>(delta, delta, delta);
        }

        T Volume() const
        {
            Vector3<T> d = max_ - min_;
            return d.x_ * d.y_ * d.z_;
        }

        Axis MaximumExtent() const
        {
            Vector3<T> d = max_ - min_;
            if((d.x_ > d.y_) && (d.x_ > d.z_)) {
                return AXIS_X;
            } else if(d.y_ > d.z_) {
                return AXIS_Y;
            } else {
                return AXIS_Z;
            }
        }

        Vector3<T> Lerp(T tx, T ty, T tz) const
        {
            return Vector3<T>( Lerp(tx, min_.x_, max_.x_),
                               Lerp(ty, min_.y_, max_.y_),
                               Lerp(tz, min_.z_, max_.z_) );
        }

        Vector3<T> Offset(Vector3<T> const& p) const
        {
            return Vector3<T>( (p.x_ - min_.x_) / (max_.x_ - min_.x_),
                               (p.y_ - min_.y_) / (max_.y_ - min_.y_),
                               (p.z_ - min_.z_) / (max_.z_ - min_.z_) );
        }

        T GetDiagonalSquared() const
        {
            T const dx = (max_.x_ - min_.x_);
            T const dy = (max_.y_ - min_.y_);
            T const dz = (max_.z_ - min_.z_);
            return dx*dx + dy*dy + dz*dz;
        }
    };
}

#endif

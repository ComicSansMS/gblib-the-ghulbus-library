
#ifndef GB_MATH_INCLUDE_GUARD_MATRIX_OP_HPP_
#define GB_MATH_INCLUDE_GUARD_MATRIX_OP_HPP_

#include <gbMath/config.hpp>

#include <gbMath/float_util.hpp>
#include <gbMath/matrix2.hpp>
#include <gbMath/matrix3.hpp>
#include <gbMath/matrix4.hpp>
#include <gbMath/vector2.hpp>
#include <gbMath/vector3.hpp>
#include <gbMath/vector4.hpp>

#include <cmath>

namespace GB_MATH_NAMESPACE {

    template<typename T>
    Vector2<T> TransformVector(Matrix2<T> const& m, Vector2<T> const& v) {
        return Vector2<T>( Get<1,1>(m)*v.x_ + Get<1,2>(m)*v.y_,
                           Get<2,1>(m)*v.x_ + Get<2,2>(m)*v.y_ );
    }

    template<typename T>
    Vector3<T> TransformVector(Matrix3<T> const& m, Vector3<T> const& v) {
        return Vector3<T>( Get<1,1>(m)*v.x_ + Get<1,2>(m)*v.y_ + Get<1,3>(m)*v.z_,
                           Get<2,1>(m)*v.x_ + Get<2,2>(m)*v.y_ + Get<2,3>(m)*v.z_,
                           Get<3,1>(m)*v.x_ + Get<3,2>(m)*v.y_ + Get<3,3>(m)*v.z_ );
    }

    template<typename T>
    Vector4<T> TransformVector(Matrix4<T> const& m, Vector4<T> const& v) {
        return Vector4<T>( Get<1,1>(m)*v.x_ + Get<1,2>(m)*v.y_ + Get<1,3>(m)*v.z_ + Get<1,4>(m)*v.w_,
                           Get<2,1>(m)*v.x_ + Get<2,2>(m)*v.y_ + Get<2,3>(m)*v.z_ + Get<2,4>(m)*v.w_, 
                           Get<3,1>(m)*v.x_ + Get<3,2>(m)*v.y_ + Get<3,3>(m)*v.z_ + Get<3,4>(m)*v.w_,
                           Get<4,1>(m)*v.x_ + Get<4,2>(m)*v.y_ + Get<4,3>(m)*v.z_ + Get<4,4>(m)*v.w_ );
    }

    template<typename T>
    Matrix4<T> ExpandToMatrix4(Matrix3<T> const& m) {
        T const one = FloatUtil::One<T>();
        T const zero = FloatUtil::Zero<T>();
        return Matrix4<T>( Get<1,1>(m), Get<1,2>(m), Get<1,3>(m), zero,
                           Get<2,1>(m), Get<2,2>(m), Get<2,3>(m), zero,
                           Get<3,1>(m), Get<3,2>(m), Get<3,3>(m), zero,
                                  zero,        zero,        zero,  one );
    }

    template<typename T>
    Matrix3<T> GetUpperMatrix3(Matrix4<T> const& m) {
        return Matrix3<T>( Get<1,1>(m), Get<1,2>(m), Get<1,3>(m),
                           Get<2,1>(m), Get<2,2>(m), Get<2,3>(m),
                           Get<3,1>(m), Get<3,2>(m), Get<3,3>(m) );
    }


    namespace Matrix {

        template<typename T>
        Matrix2<T> Rot2D(T phi) {
            T const cos = std::cos(phi);
            T const sin = std::sin(phi);
            return Matrix2<T>( cos,-sin,
                               sin, cos );
        }

        template<typename T>
        Matrix3<T> Rot3D_X(T phi) {
            T const cos = std::cos(phi);
            T const sin = std::sin(phi);
            T const one = FloatUtil::One<T>();
            T const zero = FloatUtil::Zero<T>();
            return Matrix3<T>( one, zero, zero,
                              zero,  cos, -sin,
                              zero,  sin,  cos );
        }

        template<typename T>
        Matrix3<T> Rot3D_Y(T phi) {
            T const cos = std::cos(phi);
            T const sin = std::sin(phi);
            T const one = FloatUtil::One<T>();
            T const zero = FloatUtil::Zero<T>();
            return Matrix3<T>( cos, zero,  sin,
                              zero,  one, zero,
                              -sin, zero,  cos );
        }

        template<typename T>
        Matrix3<T> Rot3D_Z(T phi) {
            T const cos = std::cos(phi);
            T const sin = std::sin(phi);
            T const one = FloatUtil::One<T>();
            T const zero = FloatUtil::Zero<T>();
            return Matrix3<T>( cos, -sin, zero,
                               sin,  cos, zero,
                              zero, zero,  one );
        }

        template<typename T>
        Matrix3<T> Rot3D_AngleAxis(T angle, Vector3<T> axis) {
            axis.NormalizeSelf();
            T const cos = std::cos(angle);
            T const one_cos = FloatUtil::One<T>() - cos;
            T const sin = std::sin(angle);
            return Matrix3<T>( cos + one_cos*axis.x_*axis.x_, one_cos*axis.x_*axis.y_ - sin*axis.z_, one_cos*axis.x_*axis.z_ + sin*axis.y_,
                               one_cos*axis.x_*axis.y_ + sin*axis.z_, cos + one_cos*axis.y_*axis.y_, one_cos*axis.y_*axis.z_ - sin*axis.x_,
                               one_cos*axis.x_*axis.z_ - sin*axis.y_, one_cos*axis.y_*axis.z_ + sin*axis.x_, cos + one_cos*axis.z_*axis.z_ );
        }

        template<typename T>
        Matrix3<T> Scale3D(T s) {
            T const zero = FloatUtil::Zero<T>();
            return Matrix3<T>(    s, zero, zero,
                               zero,    s, zero,
                               zero, zero,    s );
        }

        template<typename T>
        Matrix4<T> Translate4D(Vector3<T> const& v) {
            T const one = FloatUtil::One<T>();
            T const zero = FloatUtil::Zero<T>();
            return Matrix4<T>(  one, zero, zero, v.x_,
                               zero,  one, zero, v.y_,
                               zero, zero,  one, v.z_,
                               zero, zero, zero,  one );
        }
    }

}

#endif

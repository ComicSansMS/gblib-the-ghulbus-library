
#ifndef GB_MATH_INCLUDE_GUARD_MATRIX_HPP_
#define GB_MATH_INCLUDE_GUARD_MATRIX_HPP_

#include <gbMath/config.hpp>

#include <gbMath/matrix2.hpp>
#include <gbMath/matrix3.hpp>
#include <gbMath/matrix4.hpp>

#endif

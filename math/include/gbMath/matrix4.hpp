
#ifndef GB_MATH_INCLUDE_GUARD_MATRIX4_HPP_
#define GB_MATH_INCLUDE_GUARD_MATRIX4_HPP_

#include <gbMath/config.hpp>
#include <gbMath/float_util.hpp>
#include <diagnostic/exception.hpp>

#include <ostream>
#include <type_traits>

namespace GB_MATH_NAMESPACE {

    template<typename T>
    class Matrix4;

    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==1), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._11;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==2), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._12;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==3), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._13;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==4), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._14;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==1), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._21;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==2), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._22;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==3), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._23;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==4), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._24;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==1), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._31;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==2), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._32;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==3), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._33;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==4), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._34;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==1), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._41;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==2), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._42;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==3), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._43;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==4), T>::type&
    Get(Matrix4<T>& mat) {
        return mat.m_._44;
    }
    template<size_t I, size_t J, typename T>
    typename std::enable_if<(I<=0 || I>4 || J<=0 || J>4), T>::type&
    Get(Matrix4<T>&) {
        static_assert(sizeof(T) == 0, "Invalid matrix index");
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==1), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._11;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==2), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._12;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==3), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._13;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==4), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._14;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==1), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._21;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==2), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._22;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==3), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._23;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==4), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._24;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==1), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._31;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==2), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._32;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==3), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._33;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==4), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._34;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==1), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._41;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==2), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._42;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==3), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._43;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==4 && J==4), T>::type const&
    Get(Matrix4<T> const& mat) {
        return mat.m_._44;
    }
    template<size_t I, size_t J, typename T>
    typename std::enable_if<(I<=0 || I>4 || J<=0 || J>4), T>::type const&
    Get(Matrix4<T> const&) {
        static_assert(sizeof(T) == 0, "Invalid matrix index");
    }

    /** 3x3 matrix
     * @ingroup gbmath
     */
    template<typename T>
    class Matrix4 {
        friend T& Get<1,1,T>(Matrix4<T>&);
        friend T& Get<1,2,T>(Matrix4<T>&);
        friend T& Get<1,3,T>(Matrix4<T>&);
        friend T& Get<1,4,T>(Matrix4<T>&);
        friend T& Get<2,1,T>(Matrix4<T>&);
        friend T& Get<2,2,T>(Matrix4<T>&);
        friend T& Get<2,3,T>(Matrix4<T>&);
        friend T& Get<2,4,T>(Matrix4<T>&);
        friend T& Get<3,1,T>(Matrix4<T>&);
        friend T& Get<3,2,T>(Matrix4<T>&);
        friend T& Get<3,3,T>(Matrix4<T>&);
        friend T& Get<3,4,T>(Matrix4<T>&);
        friend T& Get<4,1,T>(Matrix4<T>&);
        friend T& Get<4,2,T>(Matrix4<T>&);
        friend T& Get<4,3,T>(Matrix4<T>&);
        friend T& Get<4,4,T>(Matrix4<T>&);
        friend T const& Get<1,1,T>(Matrix4<T> const&);
        friend T const& Get<1,2,T>(Matrix4<T> const&);
        friend T const& Get<1,3,T>(Matrix4<T> const&);
        friend T const& Get<1,4,T>(Matrix4<T> const&);
        friend T const& Get<2,1,T>(Matrix4<T> const&);
        friend T const& Get<2,2,T>(Matrix4<T> const&);
        friend T const& Get<2,3,T>(Matrix4<T> const&);
        friend T const& Get<2,4,T>(Matrix4<T> const&);
        friend T const& Get<3,1,T>(Matrix4<T> const&);
        friend T const& Get<3,2,T>(Matrix4<T> const&);
        friend T const& Get<3,3,T>(Matrix4<T> const&);
        friend T const& Get<3,4,T>(Matrix4<T> const&);
        friend T const& Get<4,1,T>(Matrix4<T> const&);
        friend T const& Get<4,2,T>(Matrix4<T> const&);
        friend T const& Get<4,3,T>(Matrix4<T> const&);
        friend T const& Get<4,4,T>(Matrix4<T> const&);
    private:
        struct Matrix4_T {
            T _11, _12, _13, _14;
            T _21, _22, _23, _24;
            T _31, _32, _33, _34;
            T _41, _42, _43, _44;
        } m_;
    public:
        /** @name Construction
         * @{
         */
        Matrix4() {
            m_._11 = 1; m_._12 = 0; m_._13 = 0; m_._14 = 0;
            m_._21 = 0; m_._22 = 1; m_._23 = 0; m_._24 = 0;
            m_._31 = 0; m_._32 = 0; m_._33 = 1; m_._34 = 0;
            m_._41 = 0; m_._42 = 0; m_._43 = 0; m_._44 = 1;
        }
        Matrix4(T m11, T m12, T m13, T m14, T m21, T m22, T m23, T m24, T m31, T m32, T m33, T m34, T m41, T m42, T m43, T m44) {
            m_._11 = m11; m_._12 = m12; m_._13 = m13; m_._14 = m14;
            m_._21 = m21; m_._22 = m22; m_._23 = m23; m_._24 = m24;
            m_._31 = m31; m_._32 = m32; m_._33 = m33; m_._34 = m34;
            m_._41 = m41; m_._42 = m42; m_._43 = m43; m_._44 = m44;
        }
        Matrix4(T const* p) {
            m_._11 = p[ 0]; m_._12 = p[ 1]; m_._13 = p[ 2]; m_._14 = p[ 3];
            m_._21 = p[ 4]; m_._22 = p[ 5]; m_._23 = p[ 6]; m_._24 = p[ 7];
            m_._31 = p[ 8]; m_._32 = p[ 9]; m_._33 = p[10]; m_._34 = p[11];
            m_._41 = p[12]; m_._42 = p[13]; m_._43 = p[14]; m_._44 = p[15];
        }
        template<typename U>
        explicit Matrix4(Matrix4<U> const& rhs) {
            m_._11 = static_cast<T>(Get<1,1>(rhs)); m_._12 = static_cast<T>(Get<1,2>(rhs));
            m_._13 = static_cast<T>(Get<1,3>(rhs)); m_._14 = static_cast<T>(Get<1,4>(rhs));
            m_._21 = static_cast<T>(Get<2,1>(rhs)); m_._22 = static_cast<T>(Get<2,2>(rhs));
            m_._23 = static_cast<T>(Get<2,3>(rhs)); m_._24 = static_cast<T>(Get<2,4>(rhs));
            m_._31 = static_cast<T>(Get<3,1>(rhs)); m_._32 = static_cast<T>(Get<3,2>(rhs));
            m_._33 = static_cast<T>(Get<3,3>(rhs)); m_._34 = static_cast<T>(Get<3,4>(rhs));
            m_._41 = static_cast<T>(Get<4,1>(rhs)); m_._42 = static_cast<T>(Get<4,2>(rhs));
            m_._43 = static_cast<T>(Get<4,3>(rhs)); m_._44 = static_cast<T>(Get<4,4>(rhs));
        }
        /// @}
        /** @name Member access
         * @{
         */
        operator T*() {
            return &m_._11;
        }
        operator T const*() const {
            return &m_._11;
        }
        T& operator()(size_t i, size_t j) {
            return static_cast<T*>(*this)[(i-1)*4 + (j-1)];
        }
        T const& operator()(size_t i, size_t j) const {
            return static_cast<T const*>(*this)[(i-1)*4 + (j-1)];
        }
        /// @}
        /** @name Arithmetic
         * @{
         */
        Matrix4<T>& operator+=(Matrix4<T> const& rhs) {
            Get<1,1>(*this) += Get<1,1>(rhs);
            Get<1,2>(*this) += Get<1,2>(rhs);
            Get<1,3>(*this) += Get<1,3>(rhs);
            Get<1,4>(*this) += Get<1,4>(rhs);

            Get<2,1>(*this) += Get<2,1>(rhs);
            Get<2,2>(*this) += Get<2,2>(rhs);
            Get<2,3>(*this) += Get<2,3>(rhs);
            Get<2,4>(*this) += Get<2,4>(rhs);

            Get<3,1>(*this) += Get<3,1>(rhs);
            Get<3,2>(*this) += Get<3,2>(rhs);
            Get<3,3>(*this) += Get<3,3>(rhs);
            Get<3,4>(*this) += Get<3,4>(rhs);

            Get<4,1>(*this) += Get<4,1>(rhs);
            Get<4,2>(*this) += Get<4,2>(rhs);
            Get<4,3>(*this) += Get<4,3>(rhs);
            Get<4,4>(*this) += Get<4,4>(rhs);
            return *this;
        }
        Matrix4<T> operator+(Matrix4<T> const& rhs) const {
            Matrix4<T> ret(*this);
            ret += rhs;
            return ret;
        }
        Matrix4<T>& operator-=(Matrix4<T> const& rhs) {
            Get<1,1>(*this) -= Get<1,1>(rhs);
            Get<1,2>(*this) -= Get<1,2>(rhs);
            Get<1,3>(*this) -= Get<1,3>(rhs);
            Get<1,4>(*this) -= Get<1,4>(rhs);

            Get<2,1>(*this) -= Get<2,1>(rhs);
            Get<2,2>(*this) -= Get<2,2>(rhs);
            Get<2,3>(*this) -= Get<2,3>(rhs);
            Get<2,4>(*this) -= Get<2,4>(rhs);

            Get<3,1>(*this) -= Get<3,1>(rhs);
            Get<3,2>(*this) -= Get<3,2>(rhs);
            Get<3,3>(*this) -= Get<3,3>(rhs);
            Get<3,4>(*this) -= Get<3,4>(rhs);

            Get<4,1>(*this) -= Get<4,1>(rhs);
            Get<4,2>(*this) -= Get<4,2>(rhs);
            Get<4,3>(*this) -= Get<4,3>(rhs);
            Get<4,4>(*this) -= Get<4,4>(rhs);
            return *this;
        }
        Matrix4<T> operator-(Matrix4<T> const& rhs) const {
            Matrix4<T> ret(*this);
            ret -= rhs;
            return ret;
        }
        Matrix4<T> operator-() const {
            return Matrix4<T>(
                    -Get<1,1>(*this),
                    -Get<1,2>(*this),
                    -Get<1,3>(*this),
                    -Get<1,4>(*this),

                    -Get<2,1>(*this),
                    -Get<2,2>(*this),
                    -Get<2,3>(*this),
                    -Get<2,4>(*this),

                    -Get<3,1>(*this),
                    -Get<3,2>(*this),
                    -Get<3,3>(*this),
                    -Get<3,4>(*this),

                    -Get<4,1>(*this),
                    -Get<4,2>(*this),
                    -Get<4,3>(*this),
                    -Get<4,4>(*this)
                );
        }
        Matrix4<T>& operator*=(T const& f) {
            Get<1,1>(*this) *= f;
            Get<1,2>(*this) *= f;
            Get<1,3>(*this) *= f;
            Get<1,4>(*this) *= f;

            Get<2,1>(*this) *= f;
            Get<2,2>(*this) *= f;
            Get<2,3>(*this) *= f;
            Get<2,4>(*this) *= f;

            Get<3,1>(*this) *= f;
            Get<3,2>(*this) *= f;
            Get<3,3>(*this) *= f;
            Get<3,4>(*this) *= f;

            Get<4,1>(*this) *= f;
            Get<4,2>(*this) *= f;
            Get<4,3>(*this) *= f;
            Get<4,4>(*this) *= f;
            return *this;
        }
        Matrix4<T> operator*(T const& f) const {
            Matrix4<T> ret(*this);
            ret *= f;
            return ret;
        }
        Matrix4<T>& operator/=(T const& f) {
            Get<1,1>(*this) /= f;
            Get<1,2>(*this) /= f;
            Get<1,3>(*this) /= f;
            Get<1,4>(*this) /= f;

            Get<2,1>(*this) /= f;
            Get<2,2>(*this) /= f;
            Get<2,3>(*this) /= f;
            Get<2,4>(*this) /= f;

            Get<3,1>(*this) /= f;
            Get<3,2>(*this) /= f;
            Get<3,3>(*this) /= f;
            Get<3,4>(*this) /= f;

            Get<4,1>(*this) /= f;
            Get<4,2>(*this) /= f;
            Get<4,3>(*this) /= f;
            Get<4,4>(*this) /= f;
            return *this;
        }
        Matrix4<T> operator/(T const& f) const {
            Matrix4<T> ret(*this);
            ret /= f;
            return ret;
        }
        Matrix4<T>& operator*=(Matrix4<T> const& rhs) {
            (*this) = (*this) * rhs;
            return *this;
        }
        Matrix4<T> operator*(Matrix4<T> const& rhs) const {
            Matrix4 const& m = *this;
            return Matrix4<T>(
                    Get<1,1>(m)*Get<1,1>(rhs) + Get<1,2>(m)*Get<2,1>(rhs) + Get<1,3>(m)*Get<3,1>(rhs) + Get<1,4>(m)*Get<4,1>(rhs),
                    Get<1,1>(m)*Get<1,2>(rhs) + Get<1,2>(m)*Get<2,2>(rhs) + Get<1,3>(m)*Get<3,2>(rhs) + Get<1,4>(m)*Get<4,2>(rhs),
                    Get<1,1>(m)*Get<1,3>(rhs) + Get<1,2>(m)*Get<2,3>(rhs) + Get<1,3>(m)*Get<3,3>(rhs) + Get<1,4>(m)*Get<4,3>(rhs),
                    Get<1,1>(m)*Get<1,4>(rhs) + Get<1,2>(m)*Get<2,4>(rhs) + Get<1,3>(m)*Get<3,4>(rhs) + Get<1,4>(m)*Get<4,4>(rhs),

                    Get<2,1>(m)*Get<1,1>(rhs) + Get<2,2>(m)*Get<2,1>(rhs) + Get<2,3>(m)*Get<3,1>(rhs) + Get<2,4>(m)*Get<4,1>(rhs),
                    Get<2,1>(m)*Get<1,2>(rhs) + Get<2,2>(m)*Get<2,2>(rhs) + Get<2,3>(m)*Get<3,2>(rhs) + Get<2,4>(m)*Get<4,2>(rhs),
                    Get<2,1>(m)*Get<1,3>(rhs) + Get<2,2>(m)*Get<2,3>(rhs) + Get<2,3>(m)*Get<3,3>(rhs) + Get<2,4>(m)*Get<4,3>(rhs),
                    Get<2,1>(m)*Get<1,4>(rhs) + Get<2,2>(m)*Get<2,4>(rhs) + Get<2,3>(m)*Get<3,4>(rhs) + Get<2,4>(m)*Get<4,4>(rhs),

                    Get<3,1>(m)*Get<1,1>(rhs) + Get<3,2>(m)*Get<2,1>(rhs) + Get<3,3>(m)*Get<3,1>(rhs) + Get<3,4>(m)*Get<4,1>(rhs),
                    Get<3,1>(m)*Get<1,2>(rhs) + Get<3,2>(m)*Get<2,2>(rhs) + Get<3,3>(m)*Get<3,2>(rhs) + Get<3,4>(m)*Get<4,2>(rhs),
                    Get<3,1>(m)*Get<1,3>(rhs) + Get<3,2>(m)*Get<2,3>(rhs) + Get<3,3>(m)*Get<3,3>(rhs) + Get<3,4>(m)*Get<4,3>(rhs),
                    Get<3,1>(m)*Get<1,4>(rhs) + Get<3,2>(m)*Get<2,4>(rhs) + Get<3,3>(m)*Get<3,4>(rhs) + Get<3,4>(m)*Get<4,4>(rhs),

                    Get<4,1>(m)*Get<1,1>(rhs) + Get<4,2>(m)*Get<2,1>(rhs) + Get<4,3>(m)*Get<3,1>(rhs) + Get<4,4>(m)*Get<4,1>(rhs),
                    Get<4,1>(m)*Get<1,2>(rhs) + Get<4,2>(m)*Get<2,2>(rhs) + Get<4,3>(m)*Get<3,2>(rhs) + Get<4,4>(m)*Get<4,2>(rhs),
                    Get<4,1>(m)*Get<1,3>(rhs) + Get<4,2>(m)*Get<2,3>(rhs) + Get<4,3>(m)*Get<3,3>(rhs) + Get<4,4>(m)*Get<4,3>(rhs),
                    Get<4,1>(m)*Get<1,4>(rhs) + Get<4,2>(m)*Get<2,4>(rhs) + Get<4,3>(m)*Get<3,4>(rhs) + Get<4,4>(m)*Get<4,4>(rhs)
                );
        }
        /// @}
        /** @name Misc
         * @{
         */
        void TransposeSelf() {
            std::swap(Get<1,2>(*this), Get<2,1>(*this));
            std::swap(Get<1,3>(*this), Get<3,1>(*this));
            std::swap(Get<1,4>(*this), Get<4,1>(*this));
            std::swap(Get<2,3>(*this), Get<3,2>(*this));
            std::swap(Get<2,4>(*this), Get<4,2>(*this));
            std::swap(Get<4,3>(*this), Get<3,4>(*this));
        }
        Matrix4<T> GetTranspose() const {
            Matrix4<T> ret(*this);
            ret.TransposeSelf();
            return ret;
        }
        T GetDeterminant() const {
            Matrix4<T> const& m = *this;
            return ( Get<1,1>(m) * (Get<2,2>(m)*Get<3,3>(m)*Get<4,4>(m) + Get<2,3>(m)*Get<3,4>(m)*Get<4,2>(m) + Get<2,4>(m)*Get<3,2>(m)*Get<4,3>(m) - Get<4,2>(m)*Get<3,3>(m)*Get<2,4>(m) - Get<4,3>(m)*Get<3,4>(m)*Get<2,2>(m) - Get<4,4>(m)*Get<3,2>(m)*Get<2,3>(m)) -
                     Get<1,2>(m) * (Get<2,1>(m)*Get<3,3>(m)*Get<4,4>(m) + Get<2,3>(m)*Get<3,4>(m)*Get<4,1>(m) + Get<2,4>(m)*Get<3,1>(m)*Get<4,3>(m) - Get<4,1>(m)*Get<3,3>(m)*Get<2,4>(m) - Get<4,3>(m)*Get<3,4>(m)*Get<2,1>(m) - Get<4,4>(m)*Get<3,1>(m)*Get<2,3>(m)) +
                     Get<1,3>(m) * (Get<2,1>(m)*Get<3,2>(m)*Get<4,4>(m) + Get<2,2>(m)*Get<3,4>(m)*Get<4,1>(m) + Get<2,4>(m)*Get<3,1>(m)*Get<4,2>(m) - Get<4,1>(m)*Get<3,2>(m)*Get<2,4>(m) - Get<4,2>(m)*Get<3,4>(m)*Get<2,1>(m) - Get<4,4>(m)*Get<3,1>(m)*Get<2,2>(m)) -
                     Get<1,4>(m) * (Get<2,1>(m)*Get<3,2>(m)*Get<4,3>(m) + Get<2,2>(m)*Get<3,3>(m)*Get<4,1>(m) + Get<2,3>(m)*Get<3,1>(m)*Get<4,2>(m) - Get<4,1>(m)*Get<3,2>(m)*Get<2,3>(m) - Get<4,2>(m)*Get<3,3>(m)*Get<2,1>(m) - Get<4,3>(m)*Get<3,1>(m)*Get<2,2>(m)));
        }
        bool IsIdentity() const {
            return IsIdentity_Impl<std::is_integral<T>::value>();
        }
        bool IsOrthogonal() const {
            return IsOrthogonal_Impl<std::is_integral<T>::value>();
        }
        bool IsInvertible() const {
            return !IsZero<std::is_integral<T>::value>(GetDeterminant());
        }
        /** Gives the inverse of the matrix
         * To allow lossless computation of the inverse for integral types,
         * the resulting inverse matrix is scaled by a factor given as
         * output parameter. To obtain the true inverse, multiply the returned
         * matrix by 1/out_factor.
         * @param[in] out_factor Scale factor of the inverse
         * @return Inverse of the matrix scaled by a factor out_factor
         */
        Matrix4<T> GetInverse(T* out_factor) const {
            Matrix4<T> const& m = *this;
            // 2x2 sub-determinants required to calculate 4x4 determinant
            T const det2_11_22 = Get<1,1>(m) * Get<2,2>(m) - Get<2,1>(m) * Get<1,2>(m);
            T const det2_11_23 = Get<1,1>(m) * Get<2,3>(m) - Get<2,1>(m) * Get<1,3>(m);
            T const det2_11_24 = Get<1,1>(m) * Get<2,4>(m) - Get<2,1>(m) * Get<1,4>(m);
            T const det2_12_23 = Get<1,2>(m) * Get<2,3>(m) - Get<2,2>(m) * Get<1,3>(m);
            T const det2_12_24 = Get<1,2>(m) * Get<2,4>(m) - Get<2,2>(m) * Get<1,4>(m);
            T const det2_13_24 = Get<1,3>(m) * Get<2,4>(m) - Get<2,3>(m) * Get<1,4>(m);

            // 3x3 sub-determinants required to calculate 4x4 determinant
            T const det3_201_012 = Get<3,1>(m) * det2_12_23 - Get<3,2>(m) * det2_11_23 + Get<3,3>(m) * det2_11_22;
            T const det3_201_013 = Get<3,1>(m) * det2_12_24 - Get<3,2>(m) * det2_11_24 + Get<3,4>(m) * det2_11_22;
            T const det3_201_023 = Get<3,1>(m) * det2_13_24 - Get<3,3>(m) * det2_11_24 + Get<3,4>(m) * det2_11_23;
            T const det3_201_123 = Get<3,2>(m) * det2_13_24 - Get<3,3>(m) * det2_12_24 + Get<3,4>(m) * det2_12_23;

            T const det = ( - det3_201_123 * Get<4,1>(m) + det3_201_023 * Get<4,2>(m) - det3_201_013 * Get<4,3>(m) + det3_201_012 * Get<4,4>(m) );

            if(IsZero<std::is_integral<T>::value>(det)) {
                *out_factor = 0;
                return Matrix4<T>( 0,0,0,0,
                                   0,0,0,0,
                                   0,0,0,0,
                                   0,0,0,0 );
            }
            *out_factor = det;

            // remaining 2x2 sub-determinants
            T const det2_11_42 = Get<1,1>(m) * Get<4,2>(m) - Get<4,1>(m) * Get<1,2>(m);
            T const det2_11_43 = Get<1,1>(m) * Get<4,3>(m) - Get<4,1>(m) * Get<1,3>(m);
            T const det2_11_44 = Get<1,1>(m) * Get<4,4>(m) - Get<4,1>(m) * Get<1,4>(m);
            T const det2_12_43 = Get<1,2>(m) * Get<4,3>(m) - Get<4,2>(m) * Get<1,3>(m);
            T const det2_12_44 = Get<1,2>(m) * Get<4,4>(m) - Get<4,2>(m) * Get<1,4>(m);
            T const det2_13_44 = Get<1,3>(m) * Get<4,4>(m) - Get<4,3>(m) * Get<1,4>(m);

            T const det2_21_42 = Get<2,1>(m) * Get<4,2>(m) - Get<4,1>(m) * Get<2,2>(m);
            T const det2_21_43 = Get<2,1>(m) * Get<4,3>(m) - Get<4,1>(m) * Get<2,3>(m);
            T const det2_21_44 = Get<2,1>(m) * Get<4,4>(m) - Get<4,1>(m) * Get<2,4>(m);
            T const det2_22_43 = Get<2,2>(m) * Get<4,3>(m) - Get<4,2>(m) * Get<2,3>(m);
            T const det2_22_44 = Get<2,2>(m) * Get<4,4>(m) - Get<4,2>(m) * Get<2,4>(m);
            T const det2_23_44 = Get<2,3>(m) * Get<4,4>(m) - Get<4,3>(m) * Get<2,4>(m);

            // remaining 3x3 sub-determinants
            T const det3_203_012 = Get<3,1>(m) * det2_12_43 - Get<3,2>(m) * det2_11_43 + Get<3,3>(m) * det2_11_42;
            T const det3_203_013 = Get<3,1>(m) * det2_12_44 - Get<3,2>(m) * det2_11_44 + Get<3,4>(m) * det2_11_42;
            T const det3_203_023 = Get<3,1>(m) * det2_13_44 - Get<3,3>(m) * det2_11_44 + Get<3,4>(m) * det2_11_43;
            T const det3_203_123 = Get<3,2>(m) * det2_13_44 - Get<3,3>(m) * det2_12_44 + Get<3,4>(m) * det2_12_43;

            T const det3_213_012 = Get<3,1>(m) * det2_22_43 - Get<3,2>(m) * det2_21_43 + Get<3,3>(m) * det2_21_42;
            T const det3_213_013 = Get<3,1>(m) * det2_22_44 - Get<3,2>(m) * det2_21_44 + Get<3,4>(m) * det2_21_42;
            T const det3_213_023 = Get<3,1>(m) * det2_23_44 - Get<3,3>(m) * det2_21_44 + Get<3,4>(m) * det2_21_43;
            T const det3_213_123 = Get<3,2>(m) * det2_23_44 - Get<3,3>(m) * det2_22_44 + Get<3,4>(m) * det2_22_43;

            T const det3_301_012 = Get<4,1>(m) * det2_12_23 - Get<4,2>(m) * det2_11_23 + Get<4,3>(m) * det2_11_22;
            T const det3_301_013 = Get<4,1>(m) * det2_12_24 - Get<4,2>(m) * det2_11_24 + Get<4,4>(m) * det2_11_22;
            T const det3_301_023 = Get<4,1>(m) * det2_13_24 - Get<4,3>(m) * det2_11_24 + Get<4,4>(m) * det2_11_23;
            T const det3_301_123 = Get<4,2>(m) * det2_13_24 - Get<4,3>(m) * det2_12_24 + Get<4,4>(m) * det2_12_23;

            return Matrix4<T>( -det3_213_123,
                               +det3_203_123,
                               +det3_301_123,
                               -det3_201_123,
                               +det3_213_023,
                               -det3_203_023,
                               -det3_301_023,
                               +det3_201_023,
                               -det3_213_013,
                               +det3_203_013,
                               +det3_301_013,
                               -det3_201_013,
                               +det3_213_012,
                               -det3_203_012,
                               -det3_301_012,
                               +det3_201_012 );
        }
        Matrix4<T> GetInverse() const {
            T scale_factor;
            Matrix4<T> ret = GetInverse(&scale_factor);
            return ret * (FloatUtil::One<T>() / scale_factor);
        }
        /// @}
    private:
        template<bool IsIntegral> inline
        typename std::enable_if<IsIntegral, bool>::type
        IsZero(T n) const {
            return n == 0;
        }
        template<bool IsIntegral> inline
        typename std::enable_if<!IsIntegral, bool>::type
        IsZero(T f) const {
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            return FloatUtil::IsZero(f, max_abs_dist);
        }
        template<bool IsIntegral>
        typename std::enable_if<IsIntegral, bool>::type
        IsIdentity_Impl() const {
            Matrix4<T> const& m = *this;
            return (Get<1,1>(m) == 1) && (Get<1,2>(m) == 0) && (Get<1,3>(m) == 0) && (Get<1,4>(m) == 0) &&
                   (Get<2,1>(m) == 0) && (Get<2,2>(m) == 1) && (Get<2,3>(m) == 0) && (Get<2,4>(m) == 0) &&
                   (Get<3,1>(m) == 0) && (Get<3,2>(m) == 0) && (Get<3,3>(m) == 1) && (Get<3,4>(m) == 0) &&
                   (Get<4,1>(m) == 0) && (Get<4,2>(m) == 0) && (Get<4,3>(m) == 0) && (Get<4,4>(m) == 1);
        }
        template<bool IsIntegral>
        typename std::enable_if<!IsIntegral, bool>::type
        IsIdentity_Impl() const {
            using FloatUtil::Equal;
            using FloatUtil::One;
            T const max_ulp_dist = static_cast<T>(80.0);
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            Matrix4<T> const& m = *this;
            return IsZero<IsIntegral>(Get<1,2>(m)) && IsZero<IsIntegral>(Get<1,3>(m)) && IsZero<IsIntegral>(Get<1,4>(m)) &&
                   IsZero<IsIntegral>(Get<2,1>(m)) && IsZero<IsIntegral>(Get<2,3>(m)) && IsZero<IsIntegral>(Get<2,4>(m)) &&
                   IsZero<IsIntegral>(Get<3,1>(m)) && IsZero<IsIntegral>(Get<3,2>(m)) && IsZero<IsIntegral>(Get<3,4>(m)) &&
                   IsZero<IsIntegral>(Get<4,1>(m)) && IsZero<IsIntegral>(Get<4,2>(m)) && IsZero<IsIntegral>(Get<4,3>(m)) &&
                   Equal(Get<1,1>(m), One<T>(), max_ulp_dist, max_abs_dist) &&
                   Equal(Get<2,2>(m), One<T>(), max_ulp_dist, max_abs_dist) &&
                   Equal(Get<3,3>(m), One<T>(), max_ulp_dist, max_abs_dist) &&
                   Equal(Get<4,4>(m), One<T>(), max_ulp_dist, max_abs_dist);
        }
        template<bool IsIntegral>
        typename std::enable_if<IsIntegral, bool>::type
        IsOrthogonal_Impl() const {
            T f = GetDeterminant();
            if(f != 1) { return false; }
            Matrix4<T> t(*this);
            t.TransposeSelf();
            t *= (*this);
            return t.IsIdentity();
        }
        template<bool IsIntegral>
        typename std::enable_if<!IsIntegral, bool>::type
        IsOrthogonal_Impl() const {
            T const max_ulp_dist = static_cast<T>(80.0);
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            T f = GetDeterminant();
            if(!FloatUtil::Equal(f, FloatUtil::One<T>(), max_ulp_dist, max_abs_dist)) { return false; }
            Matrix4<T> t(*this);
            t.TransposeSelf();
            t *= (*this);
            return t.IsIdentity();
        }
    };

    /** @name Global operators Matrix4<T>
     * @{
     */
    template<typename T>
    bool operator==(Matrix4<T> const& m1, Matrix4<T> const& m2) {
        return (Get<1,1>(m1) == Get<1,1>(m2)) && (Get<1,2>(m1) == Get<1,2>(m2)) &&
               (Get<1,3>(m1) == Get<1,3>(m2)) && (Get<1,4>(m1) == Get<1,4>(m2)) &&
               (Get<2,1>(m1) == Get<2,1>(m2)) && (Get<2,2>(m1) == Get<2,2>(m2)) &&
               (Get<2,3>(m1) == Get<2,3>(m2)) && (Get<2,4>(m1) == Get<2,4>(m2)) &&
               (Get<3,1>(m1) == Get<3,1>(m2)) && (Get<3,2>(m1) == Get<3,2>(m2)) &&
               (Get<3,3>(m1) == Get<3,3>(m2)) && (Get<3,4>(m1) == Get<3,4>(m2)) &&
               (Get<4,1>(m1) == Get<4,1>(m2)) && (Get<4,2>(m1) == Get<4,2>(m2)) &&
               (Get<4,3>(m1) == Get<4,3>(m2)) && (Get<4,4>(m1) == Get<4,4>(m2));
    }

    template<typename T>
    bool operator!=(Matrix4<T> const& m1, Matrix4<T> const& m2) {
        return !(m1 == m2);
    }

    template<typename T>
    Matrix4<T> operator*(T const& f, Matrix4<T> const& mat) {
        return mat * f;
    }

    template<typename T>
    void Identity(Matrix4<T>& m) {
        Get<1,1>(m) = 1; Get<1,2>(m) = 0; Get<1,3>(m) = 0; Get<1,4>(m) = 0;
        Get<2,1>(m) = 0; Get<2,2>(m) = 1; Get<2,3>(m) = 0; Get<2,4>(m) = 0;
        Get<3,1>(m) = 0; Get<3,2>(m) = 0; Get<3,3>(m) = 1; Get<3,4>(m) = 0;
        Get<4,1>(m) = 0; Get<4,2>(m) = 0; Get<4,3>(m) = 0; Get<4,4>(m) = 1;
    }

    template<typename T>
    void Null(Matrix4<T>& m) {
        Get<1,1>(m) = 0; Get<1,2>(m) = 0; Get<1,3>(m) = 0; Get<1,4>(m) = 0;
        Get<2,1>(m) = 0; Get<2,2>(m) = 0; Get<2,3>(m) = 0; Get<2,4>(m) = 0;
        Get<3,1>(m) = 0; Get<3,2>(m) = 0; Get<3,3>(m) = 0; Get<3,4>(m) = 0;
        Get<4,1>(m) = 0; Get<4,2>(m) = 0; Get<4,3>(m) = 0; Get<4,4>(m) = 0;
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, Matrix4<T> const& m)
    {
        return os << "[" << Get<1,1>(m) << ", " << Get<1,2>(m) << ", " << Get<1,3>(m) << ", " << Get<1,4>(m) << "]\n"
                  << "[" << Get<2,1>(m) << ", " << Get<2,2>(m) << ", " << Get<2,3>(m) << ", " << Get<2,4>(m) << "]\n"
                  << "[" << Get<3,1>(m) << ", " << Get<3,2>(m) << ", " << Get<3,3>(m) << ", " << Get<3,4>(m) << "]\n"
                  << "[" << Get<4,1>(m) << ", " << Get<4,2>(m) << ", " << Get<4,3>(m) << ", " << Get<4,4>(m) << "]";
    }
    /// @}

    /** @name Matrix4 typedefs
     * @{
     */
    typedef Matrix4<float> Matrix4f;
    typedef Matrix4<double> Matrix4d;
    typedef Matrix4<int> Matrix4i;
    typedef Matrix4<unsigned int> Matrix4ui;
    /// @}
}

#endif

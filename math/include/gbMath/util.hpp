
#ifndef GB_MATH_INCLUDE_GUARD_UTIL_HPP_
#define GB_MATH_INCLUDE_GUARD_UTIL_HPP_

#include <gbMath/config.hpp>

#include <diagnostic/assert.hpp>

#include <cmath>
#include <cstdlib>
#include <limits>

namespace GB_MATH_NAMESPACE {
    template<typename T>
    T Abs(T f) {
        if(f < 0) {
            return f * -1;
        }
        return f;
    }
    
    template<>
    inline float Abs<float>(float f) {
        return std::fabs(f);
    }
    
    template<>
    inline double Abs<double>(double f) {
        return std::fabs(f);
    }

    template<>
    inline long double Abs<long double>(long double f) {
        return std::fabs(f);
    }

    template<>
    inline int Abs<int>(int i) {
        return std::abs(i);
    }

    template<>
    inline long Abs<long>(long i) {
        return std::abs(i);
    }

    template<typename T>
    T MaxOrInf()
    {
        return ( (std::numeric_limits<T>::has_infinity) ?
                 (std::numeric_limits<T>::infinity()) :
                 (std::numeric_limits<T>::max()) );
    }

    template<typename T>
    T Lerp(T t, T v1, T v2)
    {
        DIAG_CHECK((t >= 0) && (t <= 1));
        return (1-t)*v1 + t*v2;
    }

    template<typename T>
    T Clamp(T v, T low, T high)
    {
        DIAG_CHECK(low <= high);
        if(v < low) { return low; }
        else if(v > high) { return high; }
        else { return v; }
    }

    /** Solves a quadratic equation of the form ax^2 + bx + c = 0
     */
    template<typename T>
    bool Quadratic(T a, T b, T c, T* out_x1, T* out_x2)
    {
        T const discrim = b*b - 4*a*c;
        if(discrim <= 0) {
            return false;
        }
        T const root = static_cast<T>(std::sqrt(discrim));
        T q;
        if(b < 0) {
            q = static_cast<T>(-0.5) * (b - root);
        } else {
            q = static_cast<T>(-0.5) * (b + root);
        }
        *out_x1 = q / a;
        *out_x2 = c / q;
        if(*out_x1 > *out_x2) { std::swap(*out_x1, *out_x2); }
        return true;
    }
}

#endif

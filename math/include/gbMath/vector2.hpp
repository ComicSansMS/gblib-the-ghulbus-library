
#ifndef GB_MATH_INCLUDE_GUARD_VECTOR2_HPP_
#define GB_MATH_INCLUDE_GUARD_VECTOR2_HPP_

#include <ostream>
#include <cmath>

#include <gbMath/constants.hpp>

namespace GB_MATH_NAMESPACE {
    template<typename T>
    class Vector2 {
    public:
        T x_, y_;
    public:
        Vector2()
            :x_(0), y_(0)
        {
        }

        Vector2(T x, T y)
            :x_(x), y_(y)
        {
        }

        Vector2(T const* p)
            :x_(p[0]), y_(p[1])
        {
        }

        Vector2(Vector2<T> const& rhs)
            :x_(rhs.x_), y_(rhs.y_)
        {
        }

        template<typename U>
        explicit Vector2(Vector2<U> const& rhs)
            :x_(static_cast<T>(rhs.x_)), y_(static_cast<T>(rhs.y_))
        {
        }

        Vector2<T>& operator=(Vector2<T> const& rhs)
        {
            if(&rhs != this) {
                x_ = rhs.x_;
                y_ = rhs.y_;
            }
            return *this;
        }

        Vector2<T> operator-() const
        {
            return Vector2<T>(-x_, -y_);
        }

        Vector2<T>& operator+=(Vector2<T> const& rhs)
        {
            x_ += rhs.x_;
            y_ += rhs.y_;
            return *this;
        }

        Vector2<T>& operator-=(Vector2<T> const& rhs)
        {
            x_ -= rhs.x_;
            y_ -= rhs.y_;
            return *this;
        }

        Vector2<T>& operator*=(T f)
        {
            x_ *= f;
            y_ *= f;
            return *this;
        }

        Vector2<T>& operator/=(T f)
        {
            x_ /= f;
            y_ /= f;
            return *this;
        }

        T GetLength() const
        {
            double tmp = static_cast<double>( x_*x_ + y_*y_ );
            return static_cast<T>( std::sqrt(tmp) );
        }

        T GetLengthSq() const
        {
            return x_*x_ + y_*y_;
        }

        void NormalizeSelf()
        {
            T const len = GetLength();
            x_ /= len;
            y_ /= len;
        }

        Vector2<T> GetNormalized() const
        {
            Vector2<T> ret = *this;
            ret.NormalizeSelf();
            return ret;
        }

        T CheckLinearDependence(Vector2<T> const& v) const
        {
            T t = v.x_ / x_;
            if(std::fabs((y_ * t) - v.y_) > static_cast<T>(Constf::EPSILON)) {
                return 0;
            }
            return t;
        }
    };

#    ifdef WIN32
    template<>
    inline float Vector2<float>::GetLength() const
    {
        return std::sqrtf(x_*x_ + y_*y_);
    }
#    endif

    template<typename T>
    Vector2<T> operator+(Vector2<T> const& lhs, Vector2<T> const& rhs)
    {
        return Vector2<T>(lhs.x_ + rhs.x_, lhs.y_ + rhs.y_);
    }

    template<typename T>
    Vector2<T> operator-(Vector2<T> const& lhs, Vector2<T> const& rhs)
    {
        return Vector2<T>(lhs.x_ - rhs.x_, lhs.y_ - rhs.y_);
    }

    template<typename T>
    Vector2<T> operator*(Vector2<T> const& lhs, T f)
    {
        return Vector2<T>(lhs.x_ * f, lhs.y_ * f);
    }

    template<typename T>
    Vector2<T> operator*(T f, Vector2<T> const& rhs)
    {
        return (rhs * f);
    }

    template<typename T>
    Vector2<T> operator/(Vector2<T> const& lhs, T f)
    {
        return Vector2<T>( lhs.x_ / f,
                           lhs.y_ / f );
    }

    template<typename T>
    T Dot(Vector2<T> const& lhs, Vector2<T> const& rhs)
    {
        return lhs.x_*rhs.x_ + lhs.y_*rhs.y_;
    }

    template<typename T>
    bool operator==(Vector2<T> const& lhs, Vector2<T> const& rhs)
    {
        return ( (lhs.x_ == rhs.x_) &&
                 (lhs.y_ == rhs.y_) );
    }

    template<typename T>
    bool operator!=(Vector2<T> const& lhs, Vector2<T> const& rhs)
    {
        return ( (lhs.x_ != rhs.x_) ||
                 (lhs.y_ != rhs.y_) );
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, Vector2<T> const& v)
    {
        return os << "(" << v.x_ << ", " << v.y_ << ")";
    }

    typedef Vector2<float> Vector2f;
    typedef Vector2<double> Vector2d;
    typedef Vector2<int> Vector2i;
    typedef Vector2<unsigned int> Vector2ui;
}

#endif


#ifndef GB_MATH_INCLUDE_GUARD_FLOAT_UTIL_HPP_
#define GB_MATH_INCLUDE_GUARD_FLOAT_UTIL_HPP_

#include <gbMath/config.hpp>
#include <gbMath/constants.hpp>

#include <diagnostic/assert.hpp>
#include <diagnostic/exception.hpp>

#include <cmath>
#include <cstdint>
#include <limits>
#include <type_traits>
#include <utility>

#include <boost/math/special_functions/next.hpp>

namespace GB_MATH_NAMESPACE {
    
    namespace FloatUtil {

        /** Inspect the sign, mantissa and exponent bit fields of floats
         */
        template<typename T>
        class FloatInspector;

        template<>
        class FloatInspector<float> {
            static_assert(std::numeric_limits<float>::is_iec559, "float is not IEEE compliant");
            static_assert(std::numeric_limits<float>::has_denorm == std::denorm_present, "float is not IEEE compliant");
        private:
            union Float_T {
                float f_;
#            ifdef _DEBUG
                struct IntField_T {
                    uint32_t mantissa : 23;
                    uint32_t exponent : 8;
                    uint32_t sign : 1;
                } bits;
#            endif
            } m_;
            int32_t i_;
        public:
            FloatInspector(float f) {
                m_.f_ = f;
                i_ = *reinterpret_cast<int32_t*>(&m_.f_);
            }
            uint32_t Mantissa() const {
                return i_ & ((1 << 23) - 1);
            }
            uint32_t Exponent() const {
                return (i_ >> 23) & 0xFF;
            }
            uint32_t Sign() const {
                return (i_ >> 31) & 0x01;
            }
            
        };

        template<>
        class FloatInspector<double> {
            static_assert(std::numeric_limits<double>::is_iec559, "float is not IEEE compliant");
            static_assert(std::numeric_limits<double>::has_denorm == std::denorm_present, "float is not IEEE compliant");
        private:
            union Double_T {
                double f_;
#            ifdef _DEBUG
                struct IntField_T {
                    uint64_t mantissa : 52;
                    uint64_t exponent : 11;
                    uint64_t sign : 1;
                } bits;
#            endif
            } m_;
            int64_t i_;
        public:
            FloatInspector(double f) {
                m_.f_ = f;
                i_ = *reinterpret_cast<int64_t*>(&m_.f_);
            }
            uint64_t Mantissa() const {
                return i_ & ((static_cast<uint64_t>(1) << 52) - 1);
            }
            uint64_t Exponent() const {
                return (i_ >> 52) & 0x07FF;
            }
            uint64_t Sign() const {
                return (i_ >> 63) & 0x01;
            }
        };

        template<typename T>
        class FloatInspector {
            static_assert(sizeof(T) == 0, "FloatInspector is only defined for float and double types");
        };

        /** Construct a float from mantissa, exponent and sign bit fields
         */
        template<typename T>
        typename std::enable_if<std::is_same<T,float>::value, T>::type
        BuildFloat(uint32_t mantissa, uint32_t exponent, uint32_t sign)
        {
            if(mantissa >= (1<<23)) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Mantissa too big (must be <2^23)" );
            } else if(exponent >= (1<<8)) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Exponent too big (must be <2^8)" );
            } else if(sign > 1) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Invalid sign value (must be 0 or 1)" );
            }
            uint32_t i = (mantissa | (exponent << 23) | (sign << 31));
            return *reinterpret_cast<float*>(&i);
        }

        /** Construct a double from mantissa, exponent and sign bit fields
         */
        template<typename T>
        typename std::enable_if<std::is_same<T,double>::value, T>::type
        BuildFloat(uint64_t mantissa, uint64_t exponent, uint64_t sign)
        {
            if(mantissa >= (static_cast<uint64_t>(1)<<52)) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Mantissa too big (must be <2^52)" );
            } else if(exponent >= (1<<11)) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Exponent too big (must be <2^11)" );
            } else if(sign > 1) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Invalid sign value (must be 0 or 1)" );
            }
            uint64_t i = (mantissa | (exponent << 52) | (sign << 63));
            return *reinterpret_cast<double*>(&i);
        }

        template<typename T, typename A>
        typename std::enable_if<(!std::is_same<T,double>::value) && (!std::is_same<T,float>::value), T>::type
        BuildFloat(A,A,A) {
            static_assert(sizeof(T) == 0, "BuildFloat is only defined for float and double types");
        }

        /** Templated constant 1
         */
        template<typename T> inline
        typename std::enable_if<std::is_same<T,float>::value, T>::type
        One() {
            return 1.0f;
        }

        template<typename T> inline
        typename std::enable_if<std::is_same<T,double>::value, T>::type
        One() {
            return 1.0;
        }

        template<typename T>
        typename std::enable_if<(!std::is_same<T,double>::value) && (!std::is_same<T,float>::value), T>::type
        One() {
            static_assert(sizeof(T) == 0, "One is only defined for float and double types");
        }

        /** Templated constant 0
         */
        template<typename T> inline
        typename std::enable_if<std::is_same<T,float>::value, T>::type
        Zero() {
            return 0.0f;
        }

        template<typename T> inline
        typename std::enable_if<std::is_same<T,double>::value, T>::type
        Zero() {
            return 0.0;
        }

        template<typename T>
        typename std::enable_if<(!std::is_same<T,double>::value) && (!std::is_same<T,float>::value), T>::type
        Zero() {
            static_assert(sizeof(T) == 0, "Zero is only defined for float and double types");
        }

        /** Templated constant EPSILON from constants.hpp
         */
        template<typename T> inline
        typename std::enable_if<std::is_same<T,float>::value, T>::type
        Epsilon() {
            return Constf::EPSILON;
        }

        template<typename T> inline
        typename std::enable_if<std::is_same<T,double>::value, T>::type
        Epsilon() {
            return Const::EPSILON;
        }

        template<typename T>
        typename std::enable_if<(!std::is_same<T,double>::value) && (!std::is_same<T,float>::value), T>::type
        Epsilon() {
            static_assert(sizeof(T) == 0, "Epsilon is only defined for float and double types");
        }

        /** Approximate equality based on absolute distances.
         * @tparam T Floating point type; Must be either float or double
         * @param[in] f1 A number
         * @param[in] f2 Another number
         * @param[in] max_delta_abs Maximum absolute distance
         * @return true iff the the absolute distance between f1 and f2 is <= max_delta_abs
         * @note Since the absolute distance between two adjacent floats
         *       increases with bigger numbers, this function only gives
         *       meaningful results if the size of the numbers is known
         *       beforehand and an accordingly sized max_delta_abs can be chosen.
         */
        template<typename T>
        inline bool Equal_AbsDelta(T f1, T f2, T max_delta_abs)
        {
            auto const dist = std::fabs(f1 - f2);
            return (dist <= max_delta_abs);
        }

        /** Approximate equality based on relative distances.
         * @tparam T Floating point type; Must be either float or double
         * @param[in] f1 A number
         * @param[in] f2 Another number
         * @param[in] max_delta_rel Maximum relative distance
         * @return true iff the the absolute distance between f1 and f2 is <= max(f1,f2) * max_delta_rel
         */
        template<typename T>
        inline bool Equal_RelDelta(T f1, T f2, T max_delta_rel)
        {
            T const diff = std::fabs(f1 - f2);
            T const max_f = std::max(std::fabs(f1), std::fabs(f2));
            return (diff <= max_f * max_delta_rel);
        }

        /** Approximate equality based on ULP distance.
         * The unit of least precision (ULP) distance between two floats
         * is the distance between their integer representations.
         * @tparam T Floating point type; Must be either float or double
         * @param[in] f1 A number
         * @param[in] f2 Another number
         * @param[in] max_ulp_distance Max ULP distance
         * @return true iff the ULP distance between f1 and f2 is <= max_ulp_distance
         * @note The ULP distance is always whole number. The type of max_ulp_distance
         *       is a floating point type for performance reasons only. The minimum
         *       ULP between two numbers that are not exactly equal is 1.
         * @attention ULP distance is only defined for values of the same sign.
         * @attention ULP distance does not work for comparing values against
         *            special values like 0, +/-Inf or NaN
         */
        template<typename T>
        inline bool Equal_ULP(T f1, T f2, T max_ulp_distance)
        {
            T const dist = boost::math::float_distance(f1, f2);
            return (std::fabs(dist) <= max_ulp_distance);
        }

        /** Approximate check for equality to 0
         * @param[in] f A number
         * @param[in] max_delta_abs Maximum absolute distance
         * @return true iff f1 is at most max_delta_abs away from 0
         */
        template<typename T>
        inline bool IsZero(T f, T max_delta_abs)
        {
            return Equal_AbsDelta(f, Zero<T>(), max_delta_abs);
        }

        /** Exact check for equality to 0
         * @param[in] f A number
         * @return true iff f is 0 (0.0f or -0.0f)
         */
        template<typename T>
        inline bool IsZero(T f)
        {
            return ((f == Zero<T>()) || (f == -Zero<T>()));
        }

        /** Check for NaN
         * @tparam T Floating point type; Must be either float or double
         * @param[in] f A number
         * @return true iff f is NaN (f != f)
         */
        template<typename T>
        inline bool IsNan(T f)
        {
            return (f != f);
        }

        /** Sign check
         * @param[in] f A number
         * @return true iff (f < 0)
         */
        template<typename T>
        inline bool IsNegative(T f)
        {
            return (f < Zero<T>());
        }

        /** Approximate floating point equality
         * @tparam T Floating point type; Must be either float or double
         * @param[in] f1 A number
         * @param[in] f2 Another number
         * @param[in] max_ulp_distance Max ULP distance
         * @param[in] max_delta_abs Maximum absolute distance
         * @return true iff f1 and f2 are approximately equal. See Notes for details.
         * @note This function provides a complex and sophisticated approximate
         *       equality check for floats. It uses ULP distance comparison
         *       from Equal_ULP() with a safety-net of absolute distance
         *       equality for values near 0.
         */
        template<typename T>
        inline bool Equal(T f1, T f2, T max_ulp_distance, T max_delta_abs)
        {
            if(IsNan(f1) || IsNan(f2)) {
                return false;
            } else if((f1 == f2) || Equal_AbsDelta(f1, f2, max_delta_abs)) {
                return true;
            } else if(IsNegative(f1) != IsNegative(f2)) {
                return false;
            } else {
                return Equal_ULP(f1, f2, max_ulp_distance);
            }
        }
    }

}

#endif

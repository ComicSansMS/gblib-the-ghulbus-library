
#ifndef GB_MATH_INCLUDE_GUARD_MATRIX2_HPP_
#define GB_MATH_INCLUDE_GUARD_MATRIX2_HPP_

#include <gbMath/config.hpp>
#include <gbMath/float_util.hpp>

#include <ostream>
#include <type_traits>

namespace GB_MATH_NAMESPACE {

    template<typename T>
    class Matrix2;

    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==1), T>::type&
    Get(Matrix2<T>& mat) {
        return mat.m_._11;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==2), T>::type&
    Get(Matrix2<T>& mat) {
        return mat.m_._12;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==1), T>::type&
    Get(Matrix2<T>& mat) {
        return mat.m_._21;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==2), T>::type&
    Get(Matrix2<T>& mat) {
        return mat.m_._22;
    }
    template<size_t I, size_t J, typename T>
    typename std::enable_if<(I<=0 || I>2 || J<=0 || J>2), T>::type&
    Get(Matrix2<T>&) {
        static_assert(sizeof(T) == 0, "Invalid matrix index");
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==1), T>::type const&
    Get(Matrix2<T> const& mat) {
        return mat.m_._11;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==2), T>::type const&
    Get(Matrix2<T> const& mat) {
        return mat.m_._12;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==1), T>::type const&
    Get(Matrix2<T> const& mat) {
        return mat.m_._21;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==2), T>::type const&
    Get(Matrix2<T> const& mat) {
        return mat.m_._22;
    }
    template<size_t I, size_t J, typename T>
    typename std::enable_if<(I<=0 || I>2 || J<=0 || J>2), T>::type const&
    Get(Matrix2<T> const&) {
        static_assert(sizeof(T) == 0, "Invalid matrix index");
    }

    /** 2x2 matrix
     * @ingroup gbmath
     */
    template<typename T>
    class Matrix2 {
        friend T& Get<1,1,T>(Matrix2<T>&);
        friend T& Get<1,2,T>(Matrix2<T>&);
        friend T& Get<2,1,T>(Matrix2<T>&);
        friend T& Get<2,2,T>(Matrix2<T>&);
        friend T const& Get<1,1,T>(Matrix2<T> const&);
        friend T const& Get<1,2,T>(Matrix2<T> const&);
        friend T const& Get<2,1,T>(Matrix2<T> const&);
        friend T const& Get<2,2,T>(Matrix2<T> const&);
    private:
        struct Matrix2_T {
            T _11, _12;
            T _21, _22;
        } m_;
    public:
        /** @name Construction
         * @{
         */
        Matrix2() {
            m_._11 = 1; m_._12 = 0;
            m_._21 = 0; m_._22 = 1;
        }
        Matrix2(T m11, T m12, T m21, T m22) {
            m_._11 = m11; m_._12 = m12;
            m_._21 = m21; m_._22 = m22;
        }
        Matrix2(T const* p) {
            m_._11 = p[0]; m_._12 = p[1];
            m_._21 = p[2]; m_._22 = p[3];
        }
        template<typename U>
        explicit Matrix2(Matrix2<U> const& rhs) {
            m_._11 = static_cast<T>(Get<1,1>(rhs)); m_._12 = static_cast<T>(Get<1,2>(rhs));
            m_._21 = static_cast<T>(Get<2,1>(rhs)); m_._22 = static_cast<T>(Get<2,2>(rhs));
        }
        /// @}
        /** @name Member access
         * @{
         */
        operator T*() {
            return &m_._11;
        }
        operator T const*() const {
            return &m_._11;
        }
        T& operator()(size_t i, size_t j) {
            return static_cast<T*>(*this)[(i-1)*2 + (j-1)];
        }
        T const& operator()(size_t i, size_t j) const {
            return static_cast<T const*>(*this)[(i-1)*2 + (j-1)];
        }
        /// @}
        /** @name Arithmetic
         * @{
         */
        Matrix2<T>& operator+=(Matrix2<T> const& rhs) {
            Get<1,1>(*this) += Get<1,1>(rhs);
            Get<1,2>(*this) += Get<1,2>(rhs);
            Get<2,1>(*this) += Get<2,1>(rhs);
            Get<2,2>(*this) += Get<2,2>(rhs);
            return *this;
        }
        Matrix2<T> operator+(Matrix2<T> const& rhs) const {
            Matrix2<T> ret(*this);
            ret += rhs;
            return ret;
        }
        Matrix2<T>& operator-=(Matrix2<T> const& rhs) {
            Get<1,1>(*this) -= Get<1,1>(rhs);
            Get<1,2>(*this) -= Get<1,2>(rhs);
            Get<2,1>(*this) -= Get<2,1>(rhs);
            Get<2,2>(*this) -= Get<2,2>(rhs);
            return *this;
        }
        Matrix2<T> operator-(Matrix2<T> const& rhs) const {
            Matrix2<T> ret(*this);
            ret -= rhs;
            return ret;
        }
        Matrix2<T> operator-() const {
            return Matrix2<T>( 
                    -Get<1,1>(*this),
                    -Get<1,2>(*this),
                    -Get<2,1>(*this),
                    -Get<2,2>(*this)
                );
        }
        Matrix2<T>& operator*=(T const& f) {
            Get<1,1>(*this) *= f;
            Get<1,2>(*this) *= f;
            Get<2,1>(*this) *= f;
            Get<2,2>(*this) *= f;
            return *this;
        }
        Matrix2<T> operator*(T const& f) const {
            Matrix2<T> ret(*this);
            ret *= f;
            return ret;
        }
        Matrix2<T>& operator/=(T const& f) {
            Get<1,1>(*this) /= f;
            Get<1,2>(*this) /= f;
            Get<2,1>(*this) /= f;
            Get<2,2>(*this) /= f;
            return *this;
        }
        Matrix2<T> operator/(T const& f) const {
            Matrix2<T> ret(*this);
            ret /= f;
            return ret;
        }
        Matrix2<T>& operator*=(Matrix2<T> const& rhs) {
            (*this) = (*this) * rhs;
            return *this;
        }
        Matrix2<T> operator*(Matrix2<T> const& rhs) const {
            return Matrix2<T>(
                    Get<1,1>(*this)*Get<1,1>(rhs) + Get<1,2>(*this)*Get<2,1>(rhs),
                    Get<1,1>(*this)*Get<1,2>(rhs) + Get<1,2>(*this)*Get<2,2>(rhs),

                    Get<2,1>(*this)*Get<1,1>(rhs) + Get<2,2>(*this)*Get<2,1>(rhs),
                    Get<2,1>(*this)*Get<1,2>(rhs) + Get<2,2>(*this)*Get<2,2>(rhs)
                );
        }
        /// @}
        /** @name Misc
         * @{
         */
        void TransposeSelf() {
            std::swap(Get<1,2>(*this), Get<2,1>(*this));
        }
        Matrix2<T> GetTranspose() const {
            Matrix2<T> ret(*this);
            ret.TransposeSelf();
            return ret;
        }
        T GetDeterminant() const {
            return Get<1,1>(*this) * Get<2,2>(*this) - Get<2,1>(*this) * Get<1,2>(*this);
        }
        bool IsIdentity() const {
            return IsIdentity_Impl<std::is_integral<T>::value>();
        }
        bool IsOrthogonal() const {
            return IsOrthogonal_Impl<std::is_integral<T>::value>();
        }
        bool IsInvertible() const {
            return !IsZero<std::is_integral<T>::value>(GetDeterminant());
        }
        /** Gives the inverse of the matrix
         * To allow lossless computation of the inverse for integral types,
         * the resulting inverse matrix is scaled by a factor given as
         * output parameter. To obtain the true inverse, multiply the returned
         * matrix by 1/out_factor.
         * @param[in] out_factor Scale factor of the inverse
         * @return Inverse of the matrix scaled by a factor out_factor
         */
        Matrix2<T> GetInverse(T* out_factor) const {
            T const det = GetDeterminant();
            if(IsZero<std::is_integral<T>::value>(det)) {
                *out_factor = 0;
                return Matrix2<T>(0, 0, 0, 0);
            }
            *out_factor = det;
            Matrix2<T> const& m = *this;
            return Matrix2<T>( Get<2,2>(m), -Get<1,2>(m),
                              -Get<2,1>(m),  Get<1,1>(m) );
        }
        Matrix2<T> GetInverse() const {
            T scale_factor;
            Matrix2<T> ret = GetInverse(&scale_factor);
            return ret * (FloatUtil::One<T>() / scale_factor);
        }
        /// @}
    private:
        template<bool IsIntegral> inline
        typename std::enable_if<IsIntegral, bool>::type
        IsZero(T n) const {
            return n == 0;
        }
        template<bool IsIntegral> inline
        typename std::enable_if<!IsIntegral, bool>::type
        IsZero(T f) const {
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            return FloatUtil::IsZero(f, max_abs_dist);
        }
        template<bool IsIntegral>
        typename std::enable_if<IsIntegral, bool>::type
        IsIdentity_Impl() const {
            Matrix2<T> const& m = *this;
            return (Get<1,1>(m) == 1) && (Get<1,2>(m) == 0) &&
                   (Get<2,1>(m) == 0) && (Get<2,2>(m) == 1);
        }
        template<bool IsIntegral>
        typename std::enable_if<!IsIntegral, bool>::type
        IsIdentity_Impl() const {
            using FloatUtil::Equal;
            using FloatUtil::One;
            T const max_ulp_dist = static_cast<T>(80.0);
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            Matrix2<T> const& m = *this;
            return IsZero<IsIntegral>(Get<1,2>(m)) && IsZero<IsIntegral>(Get<2,1>(m)) &&
                   Equal(Get<1,1>(m), One<T>(), max_ulp_dist, max_abs_dist) &&
                   Equal(Get<2,2>(m), One<T>(), max_ulp_dist, max_abs_dist);
        }
        template<bool IsIntegral>
        typename std::enable_if<IsIntegral, bool>::type
        IsOrthogonal_Impl() const {
            T f = GetDeterminant();
            if(f != 1) { return false; }
            Matrix2<T> t(*this);
            t.TransposeSelf();
            t *= (*this);
            return t.IsIdentity();
        }
        template<bool IsIntegral>
        typename std::enable_if<!IsIntegral, bool>::type
        IsOrthogonal_Impl() const {
            T const max_ulp_dist = static_cast<T>(80.0);
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            T f = GetDeterminant();
            if(!FloatUtil::Equal(f, FloatUtil::One<T>(), max_ulp_dist, max_abs_dist)) { return false; }
            Matrix2<T> t(*this);
            t.TransposeSelf();
            t *= (*this);
            return t.IsIdentity();
        }
    };

    /** @name Global operators Matrix2<T>
     * @{
     */
    template<typename T>
    bool operator==(Matrix2<T> const& m1, Matrix2<T> const& m2) {
        return (Get<1,1>(m1) == Get<1,1>(m2)) && (Get<1,2>(m1) == Get<1,2>(m2)) &&
               (Get<2,1>(m1) == Get<2,1>(m2)) && (Get<2,2>(m1) == Get<2,2>(m2));
    }

    template<typename T>
    bool operator!=(Matrix2<T> const& m1, Matrix2<T> const& m2) {
        return !(m1 == m2);
    }

    template<typename T>
    Matrix2<T> operator*(T const& f, Matrix2<T> const& mat) {
        return mat * f;
    }

    template<typename T>
    void Identity(Matrix2<T>& m) {
        Get<1,1>(m) = 1; Get<1,2>(m) = 0;
        Get<2,1>(m) = 0; Get<2,2>(m) = 1;
    }

    template<typename T>
    void Null(Matrix2<T>& m) {
        Get<1,1>(m) = 0; Get<1,2>(m) = 0;
        Get<2,1>(m) = 0; Get<2,2>(m) = 0;
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, Matrix2<T> const& m)
    {
        return os << "[" << Get<1,1>(m) << ", " << Get<1,2>(m) << "]\n"
                  << "[" << Get<2,1>(m) << ", " << Get<2,2>(m) << "]";
    }
    /// @}

    /** @name Matrix2 typedefs
     * @{
     */
    typedef Matrix2<float> Matrix2f;
    typedef Matrix2<double> Matrix2d;
    typedef Matrix2<int> Matrix2i;
    typedef Matrix2<unsigned int> Matrix2ui;
    /// @}
}

#endif


#ifndef GB_MATH_INCLUDE_GUARD_VECTOR4_HPP_
#define GB_MATH_INCLUDE_GUARD_VECTOR4_HPP_

namespace GB_MATH_NAMESPACE {

    template<typename T>
    class Vector4 {
    public:
        T x_, y_, z_, w_;
    public:
        Vector4()
            :x_(0), y_(0), z_(0), w_(0)
        {
        }

        Vector4(T x, T y, T z, T w)
            :x_(x), y_(y), z_(z), w_(w)
        {
        }

        Vector4(T const* p)
            :x_(p[0]), y_(p[1]), z_(p[2]), w_(p[3])
        {
        }

        Vector4(Vector4<T> const& rhs)
            :x_(rhs.x_), y_(rhs.y_), z_(rhs.z_), w_(rhs.w_)
        {
        }

        template<typename U>
        explicit Vector4(Vector4<U> const& rhs)
            :x_(static_cast<T>(rhs.x_)), y_(static_cast<T>(rhs.y_)), z_(static_cast<T>(rhs.z_)), w_(static_cast<T>(rhs.w_))
        {
        }

        Vector4<T>& operator=(Vector4<T> const& rhs)
        {
            if(&rhs != this) {
                x_ = rhs.x_;
                y_ = rhs.y_;
                z_ = rhs.z_;
                w_ = rhs.w_;
            }
            return *this;
        }

        Vector4<T> operator-() const
        {
            return Vector4<T>(-x_, -y_, -z_, -w_);
        }

        Vector4<T>& operator+=(Vector4<T> const& rhs)
        {
            x_ += rhs.x_;
            y_ += rhs.y_;
            z_ += rhs.z_;
            w_ += rhs.w_;
            return *this;
        }

        Vector4<T>& operator-=(Vector4<T> const& rhs)
        {
            x_ -= rhs.x_;
            y_ -= rhs.y_;
            z_ -= rhs.z_;
            w_ -= rhs.w_;
            return *this;
        }

        Vector4<T>& operator*=(T f)
        {
            x_ *= f;
            y_ *= f;
            z_ *= f;
            w_ *= f;
            return *this;
        }

        Vector4<T>& operator/=(T f)
        {
            x_ /= f;
            y_ /= f;
            z_ /= f;
            w_ /= f;
            return *this;
        }

        T GetLength() const
        {
            double tmp = static_cast<double>( x_*x_ + y_*y_ + z_*z_ + w_*w_);
            return static_cast<T>( std::sqrt(tmp) );
        }

        T GetLengthSq() const
        {
            return x_*x_ + y_*y_ + z_*z_ + w_*w_;
        }

        void NormalizeSelf()
        {
            T const len = GetLength();
            x_ /= len;
            y_ /= len;
            z_ /= len;
            w_ /= len;
        }

        Vector4<T> GetNormalized() const
        {
            Vector4<T> ret = *this;
            ret.NormalizeSelf();
            return ret;
        }
    };

#    ifdef WIN32
    template<>
    inline float Vector4<float>::GetLength() const
    {
        return std::sqrtf(x_*x_ + y_*y_ + z_*z_ + w_*w_);
    }
#    endif

    template<typename T>
    Vector4<T> operator+(Vector4<T> const& lhs, Vector4<T> const& rhs)
    {
        return Vector4<T>(lhs.x_ + rhs.x_, lhs.y_ + rhs.y_, lhs.z_ + rhs.z_, lhs.w_ + rhs.w_);
    }

    template<typename T>
    Vector4<T> operator-(Vector4<T> const& lhs, Vector4<T> const& rhs)
    {
        return Vector4<T>(lhs.x_ - rhs.x_, lhs.y_ - rhs.y_, lhs.z_ - rhs.z_, lhs.w_ + rhs.w_);
    }

    template<typename T>
    Vector4<T> operator*(Vector4<T> const& lhs, T f)
    {
        return Vector4<T>(lhs.x_ * f, lhs.y_ * f, lhs.z_ * f, lhs.w_ * f);
    }

    template<typename T>
    Vector4<T> operator*(T f, Vector4<T> const& rhs)
    {
        return (rhs * f);
    }

    template<typename T>
    Vector4<T> operator/(Vector4<T> const& lhs, T f)
    {
        return Vector4<T>( lhs.x_ / f,
                           lhs.y_ / f,
                           lhs.z_ / f,
                           lhs.w_ / f );
    }

    template<typename T>
    T Dot(Vector4<T> const& lhs, Vector4<T> const& rhs)
    {
        return lhs.x_*rhs.x_ + lhs.y_*rhs.y_ + lhs.z_*rhs.z_ + lhs.w_*rhs.w_;
    }

    template<typename T>
    bool operator==(Vector4<T> const& lhs, Vector4<T> const& rhs)
    {
        return ( (lhs.x_ == rhs.x_) &&
                 (lhs.y_ == rhs.y_) &&
                 (lhs.z_ == rhs.z_) &&
                 (lhs.w_ == rhs.w_) );
    }

    template<typename T>
    bool operator!=(Vector4<T> const& lhs, Vector4<T> const& rhs)
    {
        return ( (lhs.x_ != rhs.x_) ||
                 (lhs.y_ != rhs.y_) ||
                 (lhs.z_ != rhs.z_) ||
                 (lhs.w_ != rhs.w_) );
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, Vector4<T> const& v)
    {
        return os << "(" << v.x_ << ", " << v.y_ << ", " << v.z_ << ", " << v.w_ << ")";
    }

    typedef Vector4<float> Vector4f;
    typedef Vector4<double> Vector4d;
    typedef Vector4<int> Vector4i;
    typedef Vector4<unsigned int> Vector4ui;
}

#endif

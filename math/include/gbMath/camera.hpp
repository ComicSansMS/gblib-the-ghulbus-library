
#ifndef GB_MATH_INCLUDE_GUARD_CAMERA_HPP_
#define GB_MATH_INCLUDE_GUARD_CAMERA_HPP_

#include <gbMath/config.hpp>
#include <gbMath/matrix4.hpp>
#include <gbMath/vector3.hpp>

#include <type_traits>

namespace GB_MATH_NAMESPACE {

    /** 3d camera
     */
    template<typename T>
    class Camera {
        static_assert(std::is_floating_point<T>::value, "Camera template can only be instantiated with floating point types" );
    private:
        /** Cache
         */
        struct Cache_T {
            Matrix4<T> CameraMatrix;
            bool CameraMatrix_IsValid;
            Vector3f CameraRight;
            bool CameraRight_IsValid;
            Vector3f CameraUp;
            bool CameraUp_IsValid;
            Vector3f CameraView;
            bool CameraView_IsValid;
        };
    private:
        Vector3<T> m_Position;                    ///< Position of camera center
        Vector3<T> m_Target;                    ///< Position of camera target
        Vector3<T> m_Up;                        ///< Up vector
        mutable Cache_T m_Cache;                ///< Cache
    public:
        /** Constructor.
         * Constructs a camera at (0,0,1), looking at the origin with the up-vector pointing in +y
         */
        Camera()
            :m_Position(0.0, 0.0, 1.0), m_Up(0.0, 1.0, 0.0)
        {
            InvalidateCache();
        }
        /** Constructor
         * @param[in] position Position of the camera center (eye point)
         * @param[in] target Position of the camera target (lookat point)
         * @param[in] up Camera up vector
         */
        Camera(Vector3<T> const& position, Vector3<T> const& target, Vector3<T> const& up)
            :m_Position(position), m_Target(target), m_Up(up)
        {
            InvalidateCache();
        }
        /** @name Get/Set
         * @{
         */
        Vector3<T> const& GetPosition() const {
            return m_Position;
        }
        Vector3<T> const& GetTarget() const {
            return m_Target;
        }
        Vector3<T> const& GetUp() const {
            return m_Up;
        }
        void SetPosition(Vector3<T> const& p) {
            InvalidateCache();
            m_Position = p;
        }
        void SetTarget(Vector3<T> const& t) {
            InvalidateCache();
            m_Target = t;
        }
        void SetUp(Vector3<T> const& up) {
            InvalidateCache();
            m_Up = up;
            m_Up.NormalizeSelf();
        }
        /// @}
        /** Get the camera matrix
         * @return A left-handed view transformation matrix
         */
        Matrix4<T> const& GetCameraMatrix() const
        {
            if(!m_Cache.CameraMatrix_IsValid) {
                Vector3<T> view = GetCameraView();
                Vector3<T> right = GetCameraRight();
                Vector3<T> up = GetCameraUp();
                Matrix4f& mat = m_Cache.CameraMatrix;
                // View-base coordinate axes
                Get<1,1>(mat) = right.x_;
                Get<1,2>(mat) = right.y_;
                Get<1,3>(mat) = right.z_;

                Get<2,1>(mat) = up.x_;
                Get<2,2>(mat) = up.y_;
                Get<2,3>(mat) = up.z_;

                Get<3,1>(mat) = view.x_;
                Get<3,2>(mat) = view.y_;
                Get<3,3>(mat) = view.z_;

                Get<4,1>(mat) = 0.0;
                Get<4,2>(mat) = 0.0;
                Get<4,3>(mat) = 0.0;
                Get<4,4>(mat) = 1.0;
                // Translation of origin
                Get<1,4>(mat) = -Dot(right, m_Position);
                Get<2,4>(mat) = -Dot(up, m_Position);
                Get<3,4>(mat) = -Dot(view, m_Position);
                m_Cache.CameraMatrix_IsValid = true;
            }
            return m_Cache.CameraMatrix;
        }
        /** Get the camera view vector
         * @return Unit vector pointing from position to target,
         *         corresponding to the Z-axis of the view coordinate system
         */
        Vector3<T> GetCameraView() const {
            if(!m_Cache.CameraView_IsValid) {
                m_Cache.CameraView = (m_Target - m_Position).GetNormalized();
                m_Cache.CameraView_IsValid = true;
            }
            return m_Cache.CameraView;
        }
        /** Get the camera right vector
         * @return Unit vector pointing along the X-axis of the view coordinate system
         */
        Vector3<T> GetCameraRight() const {
            if(!m_Cache.CameraRight_IsValid) {
                m_Cache.CameraRight = Cross(m_Up, GetCameraView()).GetNormalized();
                m_Cache.CameraRight_IsValid = true;
            }
            return m_Cache.CameraRight;
        }
        /** Get the camera up vector
         * @return Unit vector pointing along the Y-axis of the view coordinate system
         */
        Vector3<T> GetCameraUp() const {
            if(!m_Cache.CameraUp_IsValid) {
                m_Cache.CameraUp = Cross(GetCameraView(), GetCameraRight());
                m_Cache.CameraUp_IsValid = true;
            }
            return m_Cache.CameraUp;
        }
    private:
        void InvalidateCache() {
            m_Cache.CameraMatrix_IsValid = false;
            m_Cache.CameraRight_IsValid = false;
            m_Cache.CameraUp_IsValid = false;
            m_Cache.CameraView_IsValid = false;
        }
    };

}

#endif


#ifndef GB_MATH_INCLUDE_GUARD_VECTOR3_HPP_
#define GB_MATH_INCLUDE_GUARD_VECTOR3_HPP_

#include <cmath>
#include <ostream>

namespace GB_MATH_NAMESPACE {

    template<typename T>
    class Vector3 {
    public:
        T x_, y_, z_;
    public:
        Vector3()
            :x_(0), y_(0), z_(0)
        {
        }

        Vector3(T x, T y, T z)
            :x_(x), y_(y), z_(z)
        {
        }

        Vector3(T const* p)
            :x_(p[0]), y_(p[1]), z_(p[2])
        {
        }

        Vector3(Vector3<T> const& rhs)
            :x_(rhs.x_), y_(rhs.y_), z_(rhs.z_)
        {
        }

        template<typename U>
        explicit Vector3(Vector3<U> const& rhs)
            :x_(static_cast<T>(rhs.x_)), y_(static_cast<T>(rhs.y_)), z_(static_cast<T>(rhs.z_))
        {
        }

        Vector3<T>& operator=(Vector3<T> const& rhs)
        {
            if(&rhs != this) {
                x_ = rhs.x_;
                y_ = rhs.y_;
                z_ = rhs.z_;
            }
            return *this;
        }

        Vector3<T> operator-() const
        {
            return Vector3<T>(-x_, -y_, -z_);
        }


        Vector3<T>& operator+=(Vector3<T> const& rhs)
        {
            x_ += rhs.x_;
            y_ += rhs.y_;
            z_ += rhs.z_;
            return *this;
        }

        Vector3<T>& operator-=(Vector3<T> const& rhs)
        {
            x_ -= rhs.x_;
            y_ -= rhs.y_;
            z_ -= rhs.z_;
            return *this;
        }

        Vector3<T>& operator*=(T f)
        {
            x_ *= f;
            y_ *= f;
            z_ *= f;
            return *this;
        }

        Vector3<T>& operator/=(T f)
        {
            x_ /= f;
            y_ /= f;
            z_ /= f;
            return *this;
        }

        T GetLength() const
        {
            double tmp = static_cast<double>( x_*x_ + y_*y_ + z_*z_ );
            return static_cast<T>( std::sqrt(tmp) );
        }

        T GetLengthSq() const
        {
            return x_*x_ + y_*y_ + z_*z_;
        }

        void NormalizeSelf()
        {
            T const len = GetLength();
            x_ /= len;
            y_ /= len;
            z_ /= len;
        }

        Vector3<T> GetNormalized() const
        {
            Vector3<T> ret = *this;
            ret.NormalizeSelf();
            return ret;
        }
    };

#    ifdef WIN32
    template<>
    inline float Vector3<float>::GetLength() const
    {
        return std::sqrtf(x_*x_ + y_*y_ + z_*z_);
    }
#    endif

    template<typename T>
    Vector3<T> operator+(Vector3<T> const& lhs, Vector3<T> const& rhs)
    {
        return Vector3<T>(lhs.x_ + rhs.x_, lhs.y_ + rhs.y_, lhs.z_ + rhs.z_);
    }

    template<typename T>
    Vector3<T> operator-(Vector3<T> const& lhs, Vector3<T> const& rhs)
    {
        return Vector3<T>(lhs.x_ - rhs.x_, lhs.y_ - rhs.y_, lhs.z_ - rhs.z_);
    }

    template<typename T>
    Vector3<T> operator*(Vector3<T> const& lhs, T f)
    {
        return Vector3<T>(lhs.x_ * f, lhs.y_ * f, lhs.z_ * f);
    }

    template<typename T>
    Vector3<T> operator*(T f, Vector3<T> const& rhs)
    {
        return (rhs * f);
    }

    template<typename T>
    Vector3<T> operator/(Vector3<T> const& lhs, T f)
    {
        return Vector3<T>( lhs.x_ / f,
                           lhs.y_ / f,
                           lhs.z_ / f );
    }

    template<typename T>
    T Dot(Vector3<T> const& lhs, Vector3<T> const& rhs)
    {
        return lhs.x_*rhs.x_ + lhs.y_*rhs.y_ + lhs.z_*rhs.z_;
    }

    template<typename T>
    Vector3<T> Cross(Vector3<T> const& lhs, Vector3<T> const& rhs)
    {
        return Vector3<T>(
                lhs.y_*rhs.z_ - lhs.z_*rhs.y_,
                lhs.z_*rhs.x_ - lhs.x_*rhs.z_,
                lhs.x_*rhs.y_ - lhs.y_*rhs.x_
            );
    }

    template<typename T>
    bool operator==(Vector3<T> const& lhs, Vector3<T> const& rhs)
    {
        return ( (lhs.x_ == rhs.x_) &&
                 (lhs.y_ == rhs.y_) &&
                 (lhs.z_ == rhs.z_) );
    }

    template<typename T>
    bool operator!=(Vector3<T> const& lhs, Vector3<T> const& rhs)
    {
        return ( (lhs.x_ != rhs.x_) ||
                 (lhs.y_ != rhs.y_) ||
                 (lhs.z_ != rhs.z_) );
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, Vector3<T> const& v)
    {
        return os << "(" << v.x_ << ", " << v.y_ << ", " << v.z_ << ")";
    }

    typedef Vector3<float> Vector3f;
    typedef Vector3<double> Vector3d;
    typedef Vector3<int> Vector3i;
    typedef Vector3<unsigned int> Vector3ui;
}

#endif

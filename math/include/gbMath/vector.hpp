
#ifndef GB_MATH_INCLUDE_GUARD_VECTOR_HPP_
#define GB_MATH_INCLUDE_GUARD_VECTOR_HPP_

#include <gbMath/config.hpp>

#include <diagnostic/assert.hpp>

#include <gbMath/vector2.hpp>
#include <gbMath/vector3.hpp>
#include <gbMath/vector4.hpp>
#include <gbMath/constants.hpp>
#include <gbMath/util.hpp>

#include <cmath>

namespace GB_MATH_NAMESPACE {

    /** Constructs an orthonormal basis from a single vector
     * @param[in] v1 Input vector; Must be normalized
     * @param[out] v2 Output vector; Will be perpendicular to v1 and v3
     * @param[out] v3 Output vector; Will be perpendicular to v1 and v2
     */
    template<typename T>
    void CoordinateSystem(Vector3<T> const& v1, Vector3<T>* v2, Vector3<T>* v3) {
        DIAG_ASSERT(Abs(1.0 - Vector3d(v1).GetLength()) < Const::EPSILON);
        if(Abs(v1.x_) > Abs(v1.y_)) {
            double inv_len = 1.0 / std::sqrt(v1.x_*v1.x_ + v1.z_*v1.z_);
            v2->x_ = static_cast<T>(-v1.z_ * inv_len);
            v2->y_ = 0;
            v2->z_ = static_cast<T>(v1.x_ * inv_len);
        } else {
            double inv_len = 1.0 / std::sqrt(v1.y_*v1.y_ + v1.z_*v1.z_);
            v2->x_ = 0;
            v2->y_ = static_cast<T>(v1.z_ * inv_len);
            v2->z_ = static_cast<T>(-v1.y_ * inv_len);
        }
        *v3 = Cross(v1, *v2);
    }

    int ExportMe();
}

#endif

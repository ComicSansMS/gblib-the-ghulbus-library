
#ifndef GB_MATH_INCLUDE_GUARD_MATRIX3_HPP_
#define GB_MATH_INCLUDE_GUARD_MATRIX3_HPP_

#include <gbMath/config.hpp>
#include <gbMath/float_util.hpp>

#include <ostream>
#include <type_traits>

namespace GB_MATH_NAMESPACE {

    template<typename T>
    class Matrix3;

    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==1), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._11;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==2), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._12;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==3), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._13;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==1), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._21;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==2), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._22;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==3), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._23;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==1), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._31;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==2), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._32;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==3), T>::type&
    Get(Matrix3<T>& mat) {
        return mat.m_._33;
    }
    template<size_t I, size_t J, typename T>
    typename std::enable_if<(I<=0 || I>3 || J<=0 || J>3), T>::type&
    Get(Matrix3<T>&) {
        static_assert(sizeof(T) == 0, "Invalid matrix index");
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==1), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._11;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==2), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._12;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==1 && J==3), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._13;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==1), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._21;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==2), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._22;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==2 && J==3), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._23;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==1), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._31;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==2), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._32;
    }
    template<size_t I, size_t J, typename T> 
    typename std::enable_if<(I==3 && J==3), T>::type const&
    Get(Matrix3<T> const& mat) {
        return mat.m_._33;
    }
    template<size_t I, size_t J, typename T>
    typename std::enable_if<(I<=0 || I>3 || J<=0 || J>3), T>::type const&
    Get(Matrix3<T> const&) {
        static_assert(sizeof(T) == 0, "Invalid matrix index");
    }

    /** 3x3 matrix
     * @ingroup gbmath
     */
    template<typename T>
    class Matrix3 {
        friend T& Get<1,1,T>(Matrix3<T>&);
        friend T& Get<1,2,T>(Matrix3<T>&);
        friend T& Get<1,3,T>(Matrix3<T>&);
        friend T& Get<2,1,T>(Matrix3<T>&);
        friend T& Get<2,2,T>(Matrix3<T>&);
        friend T& Get<2,3,T>(Matrix3<T>&);
        friend T& Get<3,1,T>(Matrix3<T>&);
        friend T& Get<3,2,T>(Matrix3<T>&);
        friend T& Get<3,3,T>(Matrix3<T>&);
        friend T const& Get<1,1,T>(Matrix3<T> const&);
        friend T const& Get<1,2,T>(Matrix3<T> const&);
        friend T const& Get<1,3,T>(Matrix3<T> const&);
        friend T const& Get<2,1,T>(Matrix3<T> const&);
        friend T const& Get<2,2,T>(Matrix3<T> const&);
        friend T const& Get<2,3,T>(Matrix3<T> const&);
        friend T const& Get<3,1,T>(Matrix3<T> const&);
        friend T const& Get<3,2,T>(Matrix3<T> const&);
        friend T const& Get<3,3,T>(Matrix3<T> const&);
    private:
        struct Matrix3_T {
            T _11, _12, _13;
            T _21, _22, _23;
            T _31, _32, _33;
        } m_;
    public:
        /** @name Construction
         * @{
         */
        Matrix3() {
            m_._11 = 1; m_._12 = 0; m_._13 = 0;
            m_._21 = 0; m_._22 = 1; m_._23 = 0;
            m_._31 = 0; m_._32 = 0; m_._33 = 1;
        }
        Matrix3(T m11, T m12, T m13, T m21, T m22, T m23, T m31, T m32, T m33) {
            m_._11 = m11; m_._12 = m12; m_._13 = m13;
            m_._21 = m21; m_._22 = m22; m_._23 = m23;
            m_._31 = m31; m_._32 = m32; m_._33 = m33;
        }
        Matrix3(T const* p) {
            m_._11 = p[0]; m_._12 = p[1]; m_._13 = p[2];
            m_._21 = p[3]; m_._22 = p[4]; m_._23 = p[5];
            m_._31 = p[6]; m_._32 = p[7]; m_._33 = p[8];
        }
        template<typename U>
        explicit Matrix3(Matrix3<U> const& rhs) {
            m_._11 = static_cast<T>(Get<1,1>(rhs)); m_._12 = static_cast<T>(Get<1,2>(rhs));
            m_._13 = static_cast<T>(Get<1,3>(rhs));
            m_._21 = static_cast<T>(Get<2,1>(rhs)); m_._22 = static_cast<T>(Get<2,2>(rhs));
            m_._23 = static_cast<T>(Get<2,3>(rhs));
            m_._31 = static_cast<T>(Get<3,1>(rhs)); m_._32 = static_cast<T>(Get<3,2>(rhs));
            m_._33 = static_cast<T>(Get<3,3>(rhs));
        }
        /// @}
        /** @name Member access
         * @{
         */
        operator T*() {
            return &m_._11;
        }
        operator T const*() const {
            return &m_._11;
        }
        T& operator()(size_t i, size_t j) {
            return static_cast<T*>(*this)[(i-1)*3 + (j-1)];
        }
        T const& operator()(size_t i, size_t j) const {
            return static_cast<T const*>(*this)[(i-1)*3 + (j-1)];
        }
        /// @}
        /** @name Arithmetic
         * @{
         */
        Matrix3<T>& operator+=(Matrix3<T> const& rhs) {
            Get<1,1>(*this) += Get<1,1>(rhs);
            Get<1,2>(*this) += Get<1,2>(rhs);
            Get<1,3>(*this) += Get<1,3>(rhs);

            Get<2,1>(*this) += Get<2,1>(rhs);
            Get<2,2>(*this) += Get<2,2>(rhs);
            Get<2,3>(*this) += Get<2,3>(rhs);

            Get<3,1>(*this) += Get<3,1>(rhs);
            Get<3,2>(*this) += Get<3,2>(rhs);
            Get<3,3>(*this) += Get<3,3>(rhs);
            return *this;
        }
        Matrix3<T> operator+(Matrix3<T> const& rhs) const {
            Matrix3<T> ret(*this);
            ret += rhs;
            return ret;
        }
        Matrix3<T>& operator-=(Matrix3<T> const& rhs) {
            Get<1,1>(*this) -= Get<1,1>(rhs);
            Get<1,2>(*this) -= Get<1,2>(rhs);
            Get<1,3>(*this) -= Get<1,3>(rhs);

            Get<2,1>(*this) -= Get<2,1>(rhs);
            Get<2,2>(*this) -= Get<2,2>(rhs);
            Get<2,3>(*this) -= Get<2,3>(rhs);

            Get<3,1>(*this) -= Get<3,1>(rhs);
            Get<3,2>(*this) -= Get<3,2>(rhs);
            Get<3,3>(*this) -= Get<3,3>(rhs);
            return *this;
        }
        Matrix3<T> operator-(Matrix3<T> const& rhs) const {
            Matrix3<T> ret(*this);
            ret -= rhs;
            return ret;
        }
        Matrix3<T> operator-() const {
            return Matrix3<T>(
                    -Get<1,1>(*this),
                    -Get<1,2>(*this),
                    -Get<1,3>(*this),

                    -Get<2,1>(*this),
                    -Get<2,2>(*this),
                    -Get<2,3>(*this),

                    -Get<3,1>(*this),
                    -Get<3,2>(*this),
                    -Get<3,3>(*this)
                );
        }
        Matrix3<T>& operator*=(T const& f) {
            Get<1,1>(*this) *= f;
            Get<1,2>(*this) *= f;
            Get<1,3>(*this) *= f;

            Get<2,1>(*this) *= f;
            Get<2,2>(*this) *= f;
            Get<2,3>(*this) *= f;

            Get<3,1>(*this) *= f;
            Get<3,2>(*this) *= f;
            Get<3,3>(*this) *= f;
            return *this;
        }
        Matrix3<T> operator*(T const& f) const {
            Matrix3<T> ret(*this);
            ret *= f;
            return ret;
        }
        Matrix3<T>& operator/=(T const& f) {
            Get<1,1>(*this) /= f;
            Get<1,2>(*this) /= f;
            Get<1,3>(*this) /= f;

            Get<2,1>(*this) /= f;
            Get<2,2>(*this) /= f;
            Get<2,3>(*this) /= f;

            Get<3,1>(*this) /= f;
            Get<3,2>(*this) /= f;
            Get<3,3>(*this) /= f;
            return *this;
        }
        Matrix3<T> operator/(T const& f) const {
            Matrix3<T> ret(*this);
            ret /= f;
            return ret;
        }
        Matrix3<T>& operator*=(Matrix3<T> const& rhs) {
            (*this) = (*this) * rhs;
            return *this;
        }
        Matrix3<T> operator*(Matrix3<T> const& rhs) const {
            return Matrix3<T>(
                    Get<1,1>(*this)*Get<1,1>(rhs) + Get<1,2>(*this)*Get<2,1>(rhs) + Get<1,3>(*this)*Get<3,1>(rhs),
                    Get<1,1>(*this)*Get<1,2>(rhs) + Get<1,2>(*this)*Get<2,2>(rhs) + Get<1,3>(*this)*Get<3,2>(rhs),
                    Get<1,1>(*this)*Get<1,3>(rhs) + Get<1,2>(*this)*Get<2,3>(rhs) + Get<1,3>(*this)*Get<3,3>(rhs),

                    Get<2,1>(*this)*Get<1,1>(rhs) + Get<2,2>(*this)*Get<2,1>(rhs) + Get<2,3>(*this)*Get<3,1>(rhs),
                    Get<2,1>(*this)*Get<1,2>(rhs) + Get<2,2>(*this)*Get<2,2>(rhs) + Get<2,3>(*this)*Get<3,2>(rhs),
                    Get<2,1>(*this)*Get<1,3>(rhs) + Get<2,2>(*this)*Get<2,3>(rhs) + Get<2,3>(*this)*Get<3,3>(rhs),

                    Get<3,1>(*this)*Get<1,1>(rhs) + Get<3,2>(*this)*Get<2,1>(rhs) + Get<3,3>(*this)*Get<3,1>(rhs),
                    Get<3,1>(*this)*Get<1,2>(rhs) + Get<3,2>(*this)*Get<2,2>(rhs) + Get<3,3>(*this)*Get<3,2>(rhs),
                    Get<3,1>(*this)*Get<1,3>(rhs) + Get<3,2>(*this)*Get<2,3>(rhs) + Get<3,3>(*this)*Get<3,3>(rhs)
                );
        }
        /// @}
        /** @name Misc
         * @{
         */
        void TransposeSelf() {
            std::swap(Get<1,2>(*this), Get<2,1>(*this));
            std::swap(Get<1,3>(*this), Get<3,1>(*this));
            std::swap(Get<2,3>(*this), Get<3,2>(*this));
        }
        Matrix3<T> GetTranspose() const {
            Matrix3<T> ret(*this);
            ret.TransposeSelf();
            return ret;
        }
        T GetDeterminant() const {
            return Get<1,1>(*this) * Get<2,2>(*this) * Get<3,3>(*this) +
                   Get<1,2>(*this) * Get<2,3>(*this) * Get<3,1>(*this) +
                   Get<1,3>(*this) * Get<2,1>(*this) * Get<3,2>(*this) -

                   Get<3,1>(*this) * Get<2,2>(*this) * Get<1,3>(*this) -
                   Get<3,2>(*this) * Get<2,3>(*this) * Get<1,1>(*this) -
                   Get<3,3>(*this) * Get<2,1>(*this) * Get<1,2>(*this);
        }
        bool IsIdentity() const {
            return IsIdentity_Impl<std::is_integral<T>::value>();
        }
        bool IsOrthogonal() const {
            return IsOrthogonal_Impl<std::is_integral<T>::value>();
        }
        bool IsInvertible() const {
            return !IsZero<std::is_integral<T>::value>(GetDeterminant());
        }
        /** Gives the inverse of the matrix
         * To allow lossless computation of the inverse for integral types,
         * the resulting inverse matrix is scaled by a factor given as
         * output parameter. To obtain the true inverse, multiply the returned
         * matrix by 1/out_factor.
         * @param[in] out_factor Scale factor of the inverse
         * @return Inverse of the matrix scaled by a factor out_factor
         */
        Matrix3<T> GetInverse(T* out_factor) const {
            Matrix3<T> const& m = *this;
            // 2x2 sub-determinants required to calculate 3x3 determinant
            T const det2_22_33 = Get<2,2>(m) * Get<3,3>(m) - Get<3,2>(m) * Get<2,3>(m);
            T const det2_21_33 = Get<2,1>(m) * Get<3,3>(m) - Get<3,1>(m) * Get<2,3>(m);
            T const det2_21_32 = Get<2,1>(m) * Get<3,2>(m) - Get<3,1>(m) * Get<2,2>(m);

            T det = ( Get<1,1>(m) * det2_22_33 - Get<1,2>(m) * det2_21_33 + Get<1,3>(m) * det2_21_32 );

            if(IsZero<std::is_integral<T>::value>(det)) {
                *out_factor = 0;
                return Matrix3<T>( 0,0,0,
                                   0,0,0,
                                   0,0,0 );
            }
            *out_factor = det;

            // remaining 2x2 sub-determinants
            T const det2_12_33 = Get<1,2>(m) * Get<3,3>(m) - Get<3,2>(m) * Get<1,3>(m);
            T const det2_12_23 = Get<1,2>(m) * Get<2,3>(m) - Get<2,2>(m) * Get<1,3>(m);
            T const det2_11_33 = Get<1,1>(m) * Get<3,3>(m) - Get<3,1>(m) * Get<1,3>(m);
            T const det2_11_23 = Get<1,1>(m) * Get<2,3>(m) - Get<2,1>(m) * Get<1,3>(m);
            T const det2_11_32 = Get<1,1>(m) * Get<3,2>(m) - Get<3,1>(m) * Get<1,2>(m);
            T const det2_11_22 = Get<1,1>(m) * Get<2,2>(m) - Get<2,1>(m) * Get<1,2>(m);

            return Matrix3<T>(  det2_22_33, -det2_12_33,  det2_12_23,
                               -det2_21_33,  det2_11_33, -det2_11_23,
                                det2_21_32, -det2_11_32,  det2_11_22 );
        }
        Matrix3<T> GetInverse() const {
            T scale_factor;
            Matrix3<T> ret = GetInverse(&scale_factor);
            return ret * (FloatUtil::One<T>() / scale_factor);
        }
        /// @}
    private:
        template<bool IsIntegral> inline
        typename std::enable_if<IsIntegral, bool>::type
        IsZero(T n) const {
            return n == 0;
        }
        template<bool IsIntegral> inline
        typename std::enable_if<!IsIntegral, bool>::type
        IsZero(T f) const {
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            return FloatUtil::IsZero(f, max_abs_dist);
        }
        template<bool IsIntegral>
        typename std::enable_if<IsIntegral, bool>::type
        IsIdentity_Impl() const {
            Matrix3<T> const& m = *this;
            return (Get<1,1>(m) == 1) && (Get<1,2>(m) == 0) && (Get<1,3>(m) == 0) &&
                   (Get<2,1>(m) == 0) && (Get<2,2>(m) == 1) && (Get<2,3>(m) == 0) &&
                   (Get<3,1>(m) == 0) && (Get<3,2>(m) == 0) && (Get<3,3>(m) == 1);
        }
        template<bool IsIntegral>
        typename std::enable_if<!IsIntegral, bool>::type
        IsIdentity_Impl() const {
            using FloatUtil::Equal;
            using FloatUtil::One;
            T const max_ulp_dist = static_cast<T>(80.0);
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            Matrix3<T> const& m = *this;
            return IsZero<IsIntegral>(Get<1,2>(m)) && IsZero<IsIntegral>(Get<1,3>(m)) &&
                   IsZero<IsIntegral>(Get<2,1>(m)) && IsZero<IsIntegral>(Get<2,3>(m)) &&
                   IsZero<IsIntegral>(Get<3,1>(m)) && IsZero<IsIntegral>(Get<3,2>(m)) &&
                   Equal(Get<1,1>(m), One<T>(), max_ulp_dist, max_abs_dist) &&
                   Equal(Get<2,2>(m), One<T>(), max_ulp_dist, max_abs_dist) &&
                   Equal(Get<3,3>(m), One<T>(), max_ulp_dist, max_abs_dist);
        }
        template<bool IsIntegral>
        typename std::enable_if<IsIntegral, bool>::type
        IsOrthogonal_Impl() const {
            T f = GetDeterminant();
            if(f != 1) { return false; }
            Matrix3<T> t(*this);
            t.TransposeSelf();
            t *= (*this);
            return t.IsIdentity();
        }
        template<bool IsIntegral>
        typename std::enable_if<!IsIntegral, bool>::type
        IsOrthogonal_Impl() const {
            T const max_ulp_dist = static_cast<T>(80.0);
            T const max_abs_dist = static_cast<T>(2.0)*std::numeric_limits<T>::epsilon();
            T f = GetDeterminant();
            if(!FloatUtil::Equal(f, FloatUtil::One<T>(), max_ulp_dist, max_abs_dist)) { return false; }
            Matrix3<T> t(*this);
            t.TransposeSelf();
            t *= (*this);
            return t.IsIdentity();
        }
    };

    /** @name Global operators Matrix3<T>
     * @{
     */
    template<typename T>
    bool operator==(Matrix3<T> const& m1, Matrix3<T> const& m2) {
        return (Get<1,1>(m1) == Get<1,1>(m2)) && (Get<1,2>(m1) == Get<1,2>(m2)) &&
               (Get<1,3>(m1) == Get<1,3>(m2)) &&
               (Get<2,1>(m1) == Get<2,1>(m2)) && (Get<2,2>(m1) == Get<2,2>(m2)) &&
               (Get<2,3>(m1) == Get<2,3>(m2)) &&
               (Get<3,1>(m1) == Get<3,1>(m2)) && (Get<3,2>(m1) == Get<3,2>(m2)) &&
               (Get<3,3>(m1) == Get<3,3>(m2));
    }

    template<typename T>
    bool operator!=(Matrix3<T> const& m1, Matrix3<T> const& m2) {
        return !(m1 == m2);
    }

    template<typename T>
    Matrix3<T> operator*(T const& f, Matrix3<T> const& mat) {
        return mat * f;
    }

    template<typename T>
    void Identity(Matrix3<T>& m) {
        Get<1,1>(m) = 1; Get<1,2>(m) = 0; Get<1,3>(m) = 0;
        Get<2,1>(m) = 0; Get<2,2>(m) = 1; Get<2,3>(m) = 0;
        Get<3,1>(m) = 0; Get<3,2>(m) = 0; Get<3,3>(m) = 1;
    }

    template<typename T>
    void Null(Matrix3<T>& m) {
        Get<1,1>(m) = 0; Get<1,2>(m) = 0; Get<1,3>(m) = 0;
        Get<2,1>(m) = 0; Get<2,2>(m) = 0; Get<2,3>(m) = 0;
        Get<3,1>(m) = 0; Get<3,2>(m) = 0; Get<3,3>(m) = 0;
    }

    template<typename T>
    std::ostream& operator<<(std::ostream& os, Matrix3<T> const& m)
    {
        return os << "[" << Get<1,1>(m) << ", " << Get<1,2>(m) << ", " << Get<1,3>(m) << "]\n"
                  << "[" << Get<2,1>(m) << ", " << Get<2,2>(m) << ", " << Get<2,3>(m) << "]\n"
                  << "[" << Get<3,1>(m) << ", " << Get<3,2>(m) << ", " << Get<3,3>(m) << "]";
    }
    /// @}

    /** @name Matrix3 typedefs
     * @{
     */
    typedef Matrix3<float> Matrix3f;
    typedef Matrix3<double> Matrix3d;
    typedef Matrix3<int> Matrix3i;
    typedef Matrix3<unsigned int> Matrix3ui;
    /// @}
}

#endif

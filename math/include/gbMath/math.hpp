
#ifndef GB_MATH_INCLUDE_GUARD_MATH_HPP_
#define GB_MATH_INCLUDE_GUARD_MATH_HPP_

//#include <base/config.hpp>

#include <gbMath/constants.hpp>
#include <gbMath/util.hpp>
#include <gbMath/vector.hpp>
#include <gbMath/aabb.hpp>
#include <gbMath/matrix.hpp>
#include <gbMath/matrix_op.hpp>

namespace GB_MATH_NAMESPACE {

    /** @defgroup gbmath Mathematics
     */
}

#endif

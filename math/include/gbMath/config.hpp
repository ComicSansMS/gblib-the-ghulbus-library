
#ifndef GB_MATH_INCLUDE_GUARD_CONFIG_HPP_
#define GB_MATH_INCLUDE_GUARD_CONFIG_HPP_

/** Global typedefs
 */
typedef float GB_MATH_BASE_TYPE;

/** Global defines
 */
#define GB_MATH_NAMESPACE gbMath

#endif

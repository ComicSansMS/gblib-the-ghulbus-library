
#ifndef GB_MATH_INCLUDE_GUARD_AABB2_HPP_
#define GB_MATH_INCLUDE_GUARD_AABB2_HPP_

#include <gbMath/config.hpp>
#include <gbMath/vector2.hpp>
#include <gbMath/util.hpp>

#include <algorithm>

namespace GB_MATH_NAMESPACE {
    template<typename T=GB_MATH_BASE_TYPE>
    class AABB2 {
    public:
        enum Axis {
            AXIS_X=0,
            AXIS_Y=1
        };
    private:
        Vector2<T> min_, max_;
    public:
        AABB2()
            :min_(MaxOrInf<T>(), MaxOrInf<T>()),
            max_(-MaxOrInf<T>(), -MaxOrInf<T>())
        {
        }

        AABB2(Vector2<T> const& p)
            :min_(p), max_(p)
        {
        }

        AABB2(Vector2<T> const& p1, Vector2<T> const& p2)
            :min_(std::min(p1.x_, p2.x_), std::min(p1.y_, p2.y_)),
            max_(std::max(p1.x_, p2.x_), std::max(p1.y_, p2.y_))
        {
        }

        AABB2(Vector2<T> const& p, T width, T height)
            :min_(p), max_(p.x_ + width, p.y_ + height)
        {
        }

        Vector2<T> const& GetMin() const
        {
            return min_;
        }

        Vector2<T> const& GetMax() const
        {
            return max_;
        }

        Vector2<T> GetExtent() const
        {
            return max_ - min_;
        }

        Vector2<T> GetHalfAxes() const
        {
            return ((max_ + min_) * (static_cast<T>(1) / static_cast<T>(2)));
        }

        AABB2<T> GetUnion(Vector2<T> const& v) const
        {
            AABB2 ret(*this);
            ret.UnionSelf(v);
            return ret;
        }

        void UnionSelf(Vector2<T> const& v)
        {
            min_.x_ = std::min(min_.x_, v.x_);
            min_.y_ = std::min(min_.y_, v.y_);

            max_.x_ = std::max(max_.x_, v.x_);
            max_.y_ = std::max(max_.y_, v.y_);
        }

        AABB2<T> GetUnion(AABB2<T> const& b) const
        {
            AABB2<T> ret(*this);
            ret.Union(b);
            return ret;
        }

        void UnionSelf(AABB2<T> const& b)
        {
            min_.x_ = std::min(min_.x_, b.min_.x_);
            min_.y_ = std::min(min_.y_, b.min_.y_);

            max_.x_ = std::max(max_.x_, b.max_.x_);
            max_.y_ = std::max(max_.y_, b.max_.y_);
        }

        bool Overlaps(AABB2<T> const& b) const
        {
            bool x = (max_.x_ >= b.min_.x_) && (min_.x_ <= b.max_.x_);
            bool y = (max_.y_ >= b.min_.y_) && (min_.y_ <= b.max_.y_);
            return (x && y);
        }

        bool Inside(Vector2<T> const& v) const
        {
            return ((v.x_ >= min_.x_) && (v.x_ <= max_.x_) &&
                (v.y_ >= min_.y_) && (v.y_ <= max_.y_));
        }

        void Expand(T delta)
        {
            min_ -= Vector2<T>(delta, delta);
            max_ += Vector2<T>(delta, delta);
        }

        T Area() const
        {
            Vector2<T> d = max_ - min_;
            return d.x_ * d.y_;
        }

        Axis MaximumExtent() const
        {
            Vector2<T> d = max_ - min_;
            if((d.x_ > d.y_)) {
                return AXIS_X;
            }
            else {
                return AXIS_Y;
            }
        }

        T GetDiagonalSquared() const
        {
            T const dx = (max_.x_ - min_.x_);
            T const dy = (max_.y_ - min_.y_);
            return dx*dx + dy*dy;
        }
    };
}

#endif

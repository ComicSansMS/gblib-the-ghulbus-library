
#ifndef GB_MATH_INCLUDE_GUARD_CONSTANTS_HPP_
#define GB_MATH_INCLUDE_GUARD_CONSTANTS_HPP_

#include <gbMath/config.hpp>

namespace GB_MATH_NAMESPACE {
    namespace Const {
        const double PI           = 3.1415926535897932384626433832795;
        const double PI_2         = 6.283185307179586476925286766559;
        const double PI_1_2       = 1.5707963267948966192313216916398;
        const double PI_1_180     = 0.017453292519943295769236907684886;
        const double PI_1_180_INV = 57.295779513082320876798154814105;
        const double EPSILON      = 1e-15;
        const double E            = 2.7182818284590452353602874713527;
    }
    namespace Constf {
        const float PI           = 3.1415926535897932384626433832795f;
        const float PI_2         = 6.283185307179586476925286766559f;
        const float PI_1_2       = 1.5707963267948966192313216916398f;
        const float PI_1_180     = 0.017453292519943295769236907684886f;
        const float PI_1_180_INV = 57.295779513082320876798154814105f;
        const float EPSILON      = 1e-6f;
        const float E            = 2.7182818284590452353602874713527f;
    }
}

#endif


#ifndef GB_MATH_INCLUDE_GUARD_PROJECTION_HPP_
#define GB_MATH_INCLUDE_GUARD_PROJECTION_HPP_

#include <gbMath/config.hpp>
#include <gbMath/matrix4.hpp>

#include <cmath>
#include <type_traits>

namespace GB_MATH_NAMESPACE {

    template<typename T>
    Matrix4<T> ProjectionPerspective(T width, T height, T z_near, T z_far) {
        static_assert(std::is_floating_point<T>::value, "Projection matrix can only be built with floating point types" );
        T const two = static_cast<T>(2.0);
        return Matrix4<T>( two*z_near/width,               0.0,                  0.0,                         0.0,
                                        0.0, two*z_near/height,                  0.0,                         0.0,
                                        0.0,               0.0, z_far/(z_far-z_near), z_near*z_far/(z_near-z_far),
                                        0.0,               0.0,                 1.0f,                         0.0 );
    }

    template<typename T>
    Matrix4<T> ProjectionPerspectiveFov(T fov, T aspect_ratio, T z_near, T z_far) {
        static_assert(std::is_floating_point<T>::value, "Projection matrix can only be built with floating point types" );
        T const yscale = static_cast<T>(1.0)/std::tan(fov/static_cast<T>(2.0));
        T const xscale = yscale / aspect_ratio;
        return Matrix4<T>( xscale,    0.0,                    0.0,                          0.0,
                              0.0, yscale,                    0.0,                          0.0,
                              0.0,    0.0, z_far / (z_far-z_near), -z_near*z_far/(z_far-z_near),
                              0.0,    0.0,                    1.0,                          0.0 );
    }

    template<typename T>
    Matrix4<T> ProjectionPerspectiveFrustum(T left, T right, T bottom, T top, T z_near, T z_far) {
        static_assert(std::is_floating_point<T>::value, "Projection matrix can only be built with floating point types" );
        T const two = static_cast<T>(2.0);
        return Matrix4<T>(  two*z_near/(right-left),                       0.0,                  0.0,                         0.0,
                                                0.0,   two*z_near/(top-bottom),                  0.0,                         0.0,
                          (left+right)/(left-right), (top+bottom)/(bottom-top), z_far/(z_far-z_near), z_near*z_far/(z_near-z_far),
                                                0.0,                       0.0,                  1.0,                         0.0 );
    }

    template<typename T>
    Matrix4<T> ProjectionOrthographic(T width, T height, T z_near, T z_far) {
        static_assert(std::is_floating_point<T>::value, "Projection matrix can only be built with floating point types" );
        T const one = static_cast<T>(1.0);
        T const two = static_cast<T>(2.0);
        return Matrix4<T>( two/width,        0.0,                0.0,                   0.0,
                                 0.0, two/height,                0.0,                   0.0,
                                 0.0,        0.0, one/(z_far-z_near), z_near/(z_near-z_far),
                                 0.0,        0.0,                0.0,                   1.0 );
    }

    template<typename T>
    Matrix4<T> ProjectionOrthographicFrustum(T left, T right, T bottom, T top, T z_near, T z_far) {
        static_assert(std::is_floating_point<T>::value, "Projection matrix can only be built with floating point types" );
        T const one = static_cast<T>(1.0);
        T const two = static_cast<T>(2.0);
        return Matrix4<T>( two/(right-left),                0.0,                0.0, (left+right)/(left-right),
                                        0.0,   two/(top-bottom),                0.0, (top+bottom)/(bottom-top),
                                        0.0,                0.0, one/(z_far-z_near),     z_near/(z_near-z_far),
                                        0.0,                0.0,                0.0,                       1.0 );
    }

}

#endif

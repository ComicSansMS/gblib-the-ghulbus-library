
#include <gtest/gtest.h>

#include <gbMath/matrix3.hpp>
#include <gbMath/matrix_op.hpp>

TEST(Matrix3, Construction)
{
    using namespace gbMath;
    Matrix3<int> mat;
    EXPECT_EQ(1, (Get<1,1>(mat)));
    EXPECT_EQ(0, (Get<1,2>(mat)));
    EXPECT_EQ(0, (Get<1,3>(mat)));
    EXPECT_EQ(0, (Get<2,1>(mat)));
    EXPECT_EQ(1, (Get<2,2>(mat)));
    EXPECT_EQ(0, (Get<2,3>(mat)));
    EXPECT_EQ(0, (Get<3,1>(mat)));
    EXPECT_EQ(0, (Get<3,2>(mat)));
    EXPECT_EQ(1, (Get<3,3>(mat)));

    Matrix3<int> mat2(9, 8, 7, 6, 5, 4, 3, 2, 1);
    EXPECT_EQ(9, (Get<1,1>(mat2)));
    EXPECT_EQ(8, (Get<1,2>(mat2)));
    EXPECT_EQ(7, (Get<1,3>(mat2)));
    EXPECT_EQ(6, (Get<2,1>(mat2)));
    EXPECT_EQ(5, (Get<2,2>(mat2)));
    EXPECT_EQ(4, (Get<2,3>(mat2)));
    EXPECT_EQ(3, (Get<3,1>(mat2)));
    EXPECT_EQ(2, (Get<3,2>(mat2)));
    EXPECT_EQ(1, (Get<3,3>(mat2)));

    int arr[] = { 6, 7, 8, 9, 10, 11, 12, 13, 14 };
    Matrix3<int> mat3(arr);
    EXPECT_EQ( 6, (Get<1,1>(mat3)));
    EXPECT_EQ( 7, (Get<1,2>(mat3)));
    EXPECT_EQ( 8, (Get<1,3>(mat3)));
    EXPECT_EQ( 9, (Get<2,1>(mat3)));
    EXPECT_EQ(10, (Get<2,2>(mat3)));
    EXPECT_EQ(11, (Get<2,3>(mat3)));
    EXPECT_EQ(12, (Get<3,1>(mat3)));
    EXPECT_EQ(13, (Get<3,2>(mat3)));
    EXPECT_EQ(14, (Get<3,3>(mat3)));

    Matrix3<int> mat4(mat3);
    EXPECT_EQ( 6, (Get<1,1>(mat4)));
    EXPECT_EQ( 7, (Get<1,2>(mat4)));
    EXPECT_EQ( 8, (Get<1,3>(mat4)));
    EXPECT_EQ( 9, (Get<2,1>(mat4)));
    EXPECT_EQ(10, (Get<2,2>(mat4)));
    EXPECT_EQ(11, (Get<2,3>(mat4)));
    EXPECT_EQ(12, (Get<3,1>(mat4)));
    EXPECT_EQ(13, (Get<3,2>(mat4)));
    EXPECT_EQ(14, (Get<3,3>(mat4)));

    mat4 = mat2;
    EXPECT_EQ(9, (Get<1,1>(mat4)));
    EXPECT_EQ(8, (Get<1,2>(mat4)));
    EXPECT_EQ(7, (Get<1,3>(mat4)));
    EXPECT_EQ(6, (Get<2,1>(mat4)));
    EXPECT_EQ(5, (Get<2,2>(mat4)));
    EXPECT_EQ(4, (Get<2,3>(mat4)));
    EXPECT_EQ(3, (Get<3,1>(mat4)));
    EXPECT_EQ(2, (Get<3,2>(mat4)));
    EXPECT_EQ(1, (Get<3,3>(mat4)));
}

TEST(Matrix3, MemberAccess)
{
    using namespace gbMath;
    Matrix3<int> mat3(9, 8, 7, 6, 5, 4, 3, 2, 1);
    Matrix3<int> const& mat3_c = mat3;

    EXPECT_EQ((Get<1,1>(mat3)), mat3(1,1));
    EXPECT_EQ((Get<1,2>(mat3)), mat3(1,2));
    EXPECT_EQ((Get<1,3>(mat3)), mat3(1,3));
    EXPECT_EQ((Get<2,1>(mat3)), mat3(2,1));
    EXPECT_EQ((Get<2,2>(mat3)), mat3(2,2));
    EXPECT_EQ((Get<2,3>(mat3)), mat3(2,3));
    EXPECT_EQ((Get<3,1>(mat3)), mat3(3,1));
    EXPECT_EQ((Get<3,2>(mat3)), mat3(3,2));
    EXPECT_EQ((Get<3,3>(mat3)), mat3(3,3));

    EXPECT_EQ((Get<1,1>(mat3)), mat3_c(1,1));
    EXPECT_EQ((Get<1,2>(mat3)), mat3_c(1,2));
    EXPECT_EQ((Get<1,3>(mat3)), mat3_c(1,3));
    EXPECT_EQ((Get<2,1>(mat3)), mat3_c(2,1));
    EXPECT_EQ((Get<2,2>(mat3)), mat3_c(2,2));
    EXPECT_EQ((Get<2,3>(mat3)), mat3_c(2,3));
    EXPECT_EQ((Get<3,1>(mat3)), mat3_c(3,1));
    EXPECT_EQ((Get<3,2>(mat3)), mat3_c(3,2));
    EXPECT_EQ((Get<3,3>(mat3)), mat3_c(3,3));

    EXPECT_EQ((Get<1,1>(mat3)), (Get<1,1>(mat3_c)));
    EXPECT_EQ((Get<1,2>(mat3)), (Get<1,2>(mat3_c)));
    EXPECT_EQ((Get<1,3>(mat3)), (Get<1,3>(mat3_c)));
    EXPECT_EQ((Get<2,1>(mat3)), (Get<2,1>(mat3_c)));
    EXPECT_EQ((Get<2,2>(mat3)), (Get<2,2>(mat3_c)));
    EXPECT_EQ((Get<2,3>(mat3)), (Get<2,3>(mat3_c)));
    EXPECT_EQ((Get<3,1>(mat3)), (Get<3,1>(mat3_c)));
    EXPECT_EQ((Get<3,2>(mat3)), (Get<3,2>(mat3_c)));
    EXPECT_EQ((Get<3,3>(mat3)), (Get<3,3>(mat3_c)));

    int* m_ptr = mat3;
    EXPECT_EQ((Get<1,1>(mat3)), m_ptr[0]);
    EXPECT_EQ((Get<1,2>(mat3)), m_ptr[1]);
    EXPECT_EQ((Get<1,3>(mat3)), m_ptr[2]);
    EXPECT_EQ((Get<2,1>(mat3)), m_ptr[3]);
    EXPECT_EQ((Get<2,2>(mat3)), m_ptr[4]);
    EXPECT_EQ((Get<2,3>(mat3)), m_ptr[5]);
    EXPECT_EQ((Get<3,1>(mat3)), m_ptr[6]);
    EXPECT_EQ((Get<3,2>(mat3)), m_ptr[7]);
    EXPECT_EQ((Get<3,3>(mat3)), m_ptr[8]);

    int const* m_cptr = mat3_c;
    EXPECT_EQ((Get<1,1>(mat3)), m_cptr[0]);
    EXPECT_EQ((Get<1,2>(mat3)), m_cptr[1]);
    EXPECT_EQ((Get<1,3>(mat3)), m_cptr[2]);
    EXPECT_EQ((Get<2,1>(mat3)), m_cptr[3]);
    EXPECT_EQ((Get<2,2>(mat3)), m_cptr[4]);
    EXPECT_EQ((Get<2,3>(mat3)), m_cptr[5]);
    EXPECT_EQ((Get<3,1>(mat3)), m_cptr[6]);
    EXPECT_EQ((Get<3,2>(mat3)), m_cptr[7]);
    EXPECT_EQ((Get<3,3>(mat3)), m_cptr[8]);
}

TEST(Matrix3, Equality)
{
    using namespace gbMath;
    Matrix3<int> const mat1;
    Matrix3<int> mat2(mat1);
    EXPECT_TRUE(mat1 == mat2);
    EXPECT_TRUE(mat2 == mat1);
    EXPECT_FALSE(mat1 != mat2);
    EXPECT_FALSE(mat2 != mat1);
    Get<1,1>(mat2) = 5;
    EXPECT_TRUE(mat1 != mat2);
    EXPECT_TRUE(mat2 != mat1);
    EXPECT_FALSE(mat1 == mat2);
    EXPECT_FALSE(mat2 == mat1);
}

TEST(Matrix3, Arithmetic)
{
    using namespace gbMath;
    Matrix3<int> const mat1( 1, 2, 3, 4, 5, 6, 7, 8, 9);
    Matrix3<int> const mat2(10,11,12,13,14,15,16,17,18);
    
    Matrix3<int> sum = mat1 + mat2;
    EXPECT_EQ(Matrix3<int>(11,13,15,17,19,21,23,25,27), sum);
    sum += mat1;
    EXPECT_EQ(Matrix3<int>(12,15,18,21,24,27,30,33,36), sum);

    Matrix3<int> mul = mat1 * 3;
    EXPECT_EQ(Matrix3<int>(3,6,9,12,15,18,21,24,27), mul);
    mul *= 2;
    EXPECT_EQ(Matrix3<int>(6,12,18,24,30,36,42,48,54), mul);

    Matrix3<int> div = mul / 3;
    EXPECT_EQ(Matrix3<int>(2,4,6,8,10,12,14,16,18), div);
    div /= 2;
    EXPECT_EQ(mat1, div);

    Matrix3<int> neg = -mat1;
    EXPECT_EQ(Matrix3<int>(-1, -2, -3, -4, -5, -6, -7, -8, -9), neg);
    Matrix3<int> mat_null;
    Null(mat_null);
    EXPECT_EQ(mat_null, mat2 + (-mat2));

    Matrix3<int> sub = sum - (mat1 + mat2);
    EXPECT_EQ(mat1, sub);
    sub += -1 * mat1;
    EXPECT_EQ(mat_null, sub);
    sub -= mat1;
    EXPECT_EQ((-mat1), sub);
}

TEST(Matrix3, MatrixMultiplication)
{
    using namespace gbMath;
    Matrix3<int> const ident;
    Matrix3<int> const mat1( 1,2,3,
                             4,5,6,
                             7,8,9 );
    Matrix3<int> const mat2( 0, -1, 0,
                             1,  0, 0,
                             0,  0, 2 );

    Matrix3<int> mul = ident * mat1;
    EXPECT_EQ(mat1, mul);
    mul *= ident;
    EXPECT_EQ(mat1, mul);

    mul = mat1 * mat2;
    EXPECT_EQ( Matrix3<int>( 2, -1,  6,
                             5, -4, 12,
                             8, -7, 18),
               mul);
    mul = mat2 * mat1;
    EXPECT_EQ( Matrix3<int>( -4, -5, -6,
                              1,  2,  3,
                             14, 16, 18),
               mul);
}

TEST(Matrix3, Transpose)
{
    using namespace gbMath;
    Matrix3<int> mat1( 1,2,3,
                       4,5,6,
                       7,8,9 );
    mat1.TransposeSelf();
    EXPECT_EQ(Matrix3<int>( 1,4,7,
                            2,5,8,
                            3,6,9 ),
              mat1);

    Matrix3<int> mat2 = mat1.GetTranspose();
    EXPECT_EQ(Matrix3<int>(1,2,3,4,5,6,7,8,9), mat2);
}

TEST(Matrix3, Determinant)
{
    using namespace gbMath;
    Matrix3<int> const mat1;
    Matrix3<int> const mat2(1,2,3,4,5,6,7,8,9);
    Matrix3<int> const mat3(42,54,36,4,52,64,77,81,92);

    EXPECT_EQ(1, mat1.GetDeterminant());
    EXPECT_EQ(0, mat2.GetDeterminant());
    EXPECT_EQ(96960, mat3.GetDeterminant());
}

TEST(Matrix3, IsOrthogonal)
{
    using namespace gbMath;
    Matrix3<int> mat;
    EXPECT_TRUE(mat.IsOrthogonal());
    Get<2,2>(mat) = 5;
    EXPECT_FALSE(mat.IsOrthogonal());
    Matrix3<float> f_mat;
    EXPECT_TRUE(f_mat.IsOrthogonal());
    Get<2,2>(f_mat) = 5.0f;
    EXPECT_FALSE(f_mat.IsOrthogonal());

    Matrix3<float> f_mat_rot = Matrix::Rot3D_X(0.5f);
    EXPECT_TRUE(f_mat_rot.IsOrthogonal());
}

TEST(Matrix3, IsInvertible)
{
    using namespace gbMath;
    Matrix3<int> mat1;
    EXPECT_TRUE(mat1.IsInvertible());
    Get<2,2>(mat1) = 0;
    EXPECT_FALSE(mat1.IsInvertible());
    Matrix3<float> f_mat1;
    EXPECT_TRUE(f_mat1.IsInvertible());
    Get<2,2>(f_mat1) = 0.0f;
    EXPECT_FALSE(f_mat1.IsInvertible());
}

TEST(Matrix3, Inverse)
{
    using namespace gbMath;
    Matrix3<int> const mat1( 42,54,36,
                              4,51,64,
                             81,92,-5 );
    ASSERT_TRUE(mat1.IsInvertible());
    int factor;
    Matrix3<int> const mat_inv = mat1.GetInverse(&factor);
    EXPECT_EQ(mat1.GetDeterminant(), factor);
    EXPECT_EQ(Matrix3<int>(-6143, 3582, 1620, 5204, -3126, -2544, -3763, 510, 1926), mat_inv);

    Matrix3<int> mat2 = mat1 * mat_inv;
    mat2 /= factor;
    EXPECT_EQ(Matrix3<int>(), mat2);

    Matrix3<float> const f_mat(mat1);
    float ffactor;
    Matrix3<float> f_mat_inv = f_mat.GetInverse(&ffactor);
    EXPECT_EQ(static_cast<float>(factor), ffactor);
    Matrix3<float> f_ident = f_mat * f_mat_inv;
    f_ident /= ffactor;
    EXPECT_TRUE(f_ident.IsIdentity());
}

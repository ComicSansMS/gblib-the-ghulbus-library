#include <gtest/gtest.h>

#include <gbMath/matrix4.hpp>

TEST(Matrix4, Construction)
{
    using namespace gbMath;
    Matrix4<int> mat;
    EXPECT_EQ(1, (Get<1,1>(mat)));
    EXPECT_EQ(0, (Get<1,2>(mat)));
    EXPECT_EQ(0, (Get<1,3>(mat)));
    EXPECT_EQ(0, (Get<1,4>(mat)));
    EXPECT_EQ(0, (Get<2,1>(mat)));
    EXPECT_EQ(1, (Get<2,2>(mat)));
    EXPECT_EQ(0, (Get<2,3>(mat)));
    EXPECT_EQ(0, (Get<2,4>(mat)));
    EXPECT_EQ(0, (Get<3,1>(mat)));
    EXPECT_EQ(0, (Get<3,2>(mat)));
    EXPECT_EQ(1, (Get<3,3>(mat)));
    EXPECT_EQ(0, (Get<3,4>(mat)));
    EXPECT_EQ(0, (Get<4,1>(mat)));
    EXPECT_EQ(0, (Get<4,2>(mat)));
    EXPECT_EQ(0, (Get<4,3>(mat)));
    EXPECT_EQ(1, (Get<4,4>(mat)));

    Matrix4<int> mat2(16, 15, 14, 13, 12, 11, 10, 9,
                       8, 7, 6, 5, 4, 3, 2, 1);
    EXPECT_EQ(16, (Get<1,1>(mat2)));
    EXPECT_EQ(15, (Get<1,2>(mat2)));
    EXPECT_EQ(14, (Get<1,3>(mat2)));
    EXPECT_EQ(13, (Get<1,4>(mat2)));
    EXPECT_EQ(12, (Get<2,1>(mat2)));
    EXPECT_EQ(11, (Get<2,2>(mat2)));
    EXPECT_EQ(10, (Get<2,3>(mat2)));
    EXPECT_EQ( 9, (Get<2,4>(mat2)));
    EXPECT_EQ( 8, (Get<3,1>(mat2)));
    EXPECT_EQ( 7, (Get<3,2>(mat2)));
    EXPECT_EQ( 6, (Get<3,3>(mat2)));
    EXPECT_EQ( 5, (Get<3,4>(mat2)));
    EXPECT_EQ( 4, (Get<4,1>(mat2)));
    EXPECT_EQ( 3, (Get<4,2>(mat2)));
    EXPECT_EQ( 2, (Get<4,3>(mat2)));
    EXPECT_EQ( 1, (Get<4,4>(mat2)));

    int arr[] = {  6,  7,  8,  9,
                  10, 11, 12, 13,
                  14, 15, 16, 17,
                  18, 19, 20, 21 };
    Matrix4<int> mat3(arr);
    EXPECT_EQ( 6, (Get<1,1>(mat3)));
    EXPECT_EQ( 7, (Get<1,2>(mat3)));
    EXPECT_EQ( 8, (Get<1,3>(mat3)));
    EXPECT_EQ( 9, (Get<1,4>(mat3)));
    EXPECT_EQ(10, (Get<2,1>(mat3)));
    EXPECT_EQ(11, (Get<2,2>(mat3)));
    EXPECT_EQ(12, (Get<2,3>(mat3)));
    EXPECT_EQ(13, (Get<2,4>(mat3)));
    EXPECT_EQ(14, (Get<3,1>(mat3)));
    EXPECT_EQ(15, (Get<3,2>(mat3)));
    EXPECT_EQ(16, (Get<3,3>(mat3)));
    EXPECT_EQ(17, (Get<3,4>(mat3)));
    EXPECT_EQ(18, (Get<4,1>(mat3)));
    EXPECT_EQ(19, (Get<4,2>(mat3)));
    EXPECT_EQ(20, (Get<4,3>(mat3)));
    EXPECT_EQ(21, (Get<4,4>(mat3)));

    Matrix4<int> mat4(mat3);
    EXPECT_EQ( 6, (Get<1,1>(mat4)));
    EXPECT_EQ( 7, (Get<1,2>(mat4)));
    EXPECT_EQ( 8, (Get<1,3>(mat4)));
    EXPECT_EQ( 9, (Get<1,4>(mat4)));
    EXPECT_EQ(10, (Get<2,1>(mat4)));
    EXPECT_EQ(11, (Get<2,2>(mat4)));
    EXPECT_EQ(12, (Get<2,3>(mat4)));
    EXPECT_EQ(13, (Get<2,4>(mat4)));
    EXPECT_EQ(14, (Get<3,1>(mat4)));
    EXPECT_EQ(15, (Get<3,2>(mat4)));
    EXPECT_EQ(16, (Get<3,3>(mat4)));
    EXPECT_EQ(17, (Get<3,4>(mat4)));
    EXPECT_EQ(18, (Get<4,1>(mat4)));
    EXPECT_EQ(19, (Get<4,2>(mat4)));
    EXPECT_EQ(20, (Get<4,3>(mat4)));
    EXPECT_EQ(21, (Get<4,4>(mat4)));

    mat4 = mat2;
    EXPECT_EQ(16, (Get<1,1>(mat4)));
    EXPECT_EQ(15, (Get<1,2>(mat4)));
    EXPECT_EQ(14, (Get<1,3>(mat4)));
    EXPECT_EQ(13, (Get<1,4>(mat4)));
    EXPECT_EQ(12, (Get<2,1>(mat4)));
    EXPECT_EQ(11, (Get<2,2>(mat4)));
    EXPECT_EQ(10, (Get<2,3>(mat4)));
    EXPECT_EQ( 9, (Get<2,4>(mat4)));
    EXPECT_EQ( 8, (Get<3,1>(mat4)));
    EXPECT_EQ( 7, (Get<3,2>(mat4)));
    EXPECT_EQ( 6, (Get<3,3>(mat4)));
    EXPECT_EQ( 5, (Get<3,4>(mat4)));
    EXPECT_EQ( 4, (Get<4,1>(mat4)));
    EXPECT_EQ( 3, (Get<4,2>(mat4)));
    EXPECT_EQ( 2, (Get<4,3>(mat4)));
    EXPECT_EQ( 1, (Get<4,4>(mat4)));
}

TEST(Matrix4, MemberAccess)
{
    using namespace gbMath;
    Matrix4<int> mat4( 16,15,14,13,12,11,10, 9,
                        8, 7, 6, 5, 4, 3, 2, 1 );
    Matrix4<int> const& mat4_c = mat4;

    EXPECT_EQ((Get<1,1>(mat4)), mat4(1,1));
    EXPECT_EQ((Get<1,2>(mat4)), mat4(1,2));
    EXPECT_EQ((Get<1,3>(mat4)), mat4(1,3));
    EXPECT_EQ((Get<1,4>(mat4)), mat4(1,4));
    EXPECT_EQ((Get<2,1>(mat4)), mat4(2,1));
    EXPECT_EQ((Get<2,2>(mat4)), mat4(2,2));
    EXPECT_EQ((Get<2,3>(mat4)), mat4(2,3));
    EXPECT_EQ((Get<2,4>(mat4)), mat4(2,4));
    EXPECT_EQ((Get<3,1>(mat4)), mat4(3,1));
    EXPECT_EQ((Get<3,2>(mat4)), mat4(3,2));
    EXPECT_EQ((Get<3,3>(mat4)), mat4(3,3));
    EXPECT_EQ((Get<3,4>(mat4)), mat4(3,4));
    EXPECT_EQ((Get<4,1>(mat4)), mat4(4,1));
    EXPECT_EQ((Get<4,2>(mat4)), mat4(4,2));
    EXPECT_EQ((Get<4,3>(mat4)), mat4(4,3));
    EXPECT_EQ((Get<4,4>(mat4)), mat4(4,4));

    EXPECT_EQ((Get<1,1>(mat4)), mat4_c(1,1));
    EXPECT_EQ((Get<1,2>(mat4)), mat4_c(1,2));
    EXPECT_EQ((Get<1,3>(mat4)), mat4_c(1,3));
    EXPECT_EQ((Get<1,4>(mat4)), mat4_c(1,4));
    EXPECT_EQ((Get<2,1>(mat4)), mat4_c(2,1));
    EXPECT_EQ((Get<2,2>(mat4)), mat4_c(2,2));
    EXPECT_EQ((Get<2,3>(mat4)), mat4_c(2,3));
    EXPECT_EQ((Get<2,4>(mat4)), mat4_c(2,4));
    EXPECT_EQ((Get<3,1>(mat4)), mat4_c(3,1));
    EXPECT_EQ((Get<3,2>(mat4)), mat4_c(3,2));
    EXPECT_EQ((Get<3,3>(mat4)), mat4_c(3,3));
    EXPECT_EQ((Get<3,4>(mat4)), mat4_c(3,4));
    EXPECT_EQ((Get<4,1>(mat4)), mat4_c(4,1));
    EXPECT_EQ((Get<4,2>(mat4)), mat4_c(4,2));
    EXPECT_EQ((Get<4,3>(mat4)), mat4_c(4,3));
    EXPECT_EQ((Get<4,4>(mat4)), mat4_c(4,4));

    EXPECT_EQ((Get<1,1>(mat4)), (Get<1,1>(mat4_c)));
    EXPECT_EQ((Get<1,2>(mat4)), (Get<1,2>(mat4_c)));
    EXPECT_EQ((Get<1,3>(mat4)), (Get<1,3>(mat4_c)));
    EXPECT_EQ((Get<1,4>(mat4)), (Get<1,4>(mat4_c)));
    EXPECT_EQ((Get<2,1>(mat4)), (Get<2,1>(mat4_c)));
    EXPECT_EQ((Get<2,2>(mat4)), (Get<2,2>(mat4_c)));
    EXPECT_EQ((Get<2,3>(mat4)), (Get<2,3>(mat4_c)));
    EXPECT_EQ((Get<2,4>(mat4)), (Get<2,4>(mat4_c)));
    EXPECT_EQ((Get<3,1>(mat4)), (Get<3,1>(mat4_c)));
    EXPECT_EQ((Get<3,2>(mat4)), (Get<3,2>(mat4_c)));
    EXPECT_EQ((Get<3,3>(mat4)), (Get<3,3>(mat4_c)));
    EXPECT_EQ((Get<3,4>(mat4)), (Get<3,4>(mat4_c)));
    EXPECT_EQ((Get<4,1>(mat4)), (Get<4,1>(mat4_c)));
    EXPECT_EQ((Get<4,2>(mat4)), (Get<4,2>(mat4_c)));
    EXPECT_EQ((Get<4,3>(mat4)), (Get<4,3>(mat4_c)));
    EXPECT_EQ((Get<4,4>(mat4)), (Get<4,4>(mat4_c)));

    int* m_ptr = mat4;
    EXPECT_EQ((Get<1,1>(mat4)), m_ptr[ 0]);
    EXPECT_EQ((Get<1,2>(mat4)), m_ptr[ 1]);
    EXPECT_EQ((Get<1,3>(mat4)), m_ptr[ 2]);
    EXPECT_EQ((Get<1,4>(mat4)), m_ptr[ 3]);
    EXPECT_EQ((Get<2,1>(mat4)), m_ptr[ 4]);
    EXPECT_EQ((Get<2,2>(mat4)), m_ptr[ 5]);
    EXPECT_EQ((Get<2,3>(mat4)), m_ptr[ 6]);
    EXPECT_EQ((Get<2,4>(mat4)), m_ptr[ 7]);
    EXPECT_EQ((Get<3,1>(mat4)), m_ptr[ 8]);
    EXPECT_EQ((Get<3,2>(mat4)), m_ptr[ 9]);
    EXPECT_EQ((Get<3,3>(mat4)), m_ptr[10]);
    EXPECT_EQ((Get<3,4>(mat4)), m_ptr[11]);
    EXPECT_EQ((Get<4,1>(mat4)), m_ptr[12]);
    EXPECT_EQ((Get<4,2>(mat4)), m_ptr[13]);
    EXPECT_EQ((Get<4,3>(mat4)), m_ptr[14]);
    EXPECT_EQ((Get<4,4>(mat4)), m_ptr[15]);

    int const* m_cptr = mat4_c;
    EXPECT_EQ((Get<1,1>(mat4)), m_cptr[ 0]);
    EXPECT_EQ((Get<1,2>(mat4)), m_cptr[ 1]);
    EXPECT_EQ((Get<1,3>(mat4)), m_cptr[ 2]);
    EXPECT_EQ((Get<1,4>(mat4)), m_cptr[ 3]);
    EXPECT_EQ((Get<2,1>(mat4)), m_cptr[ 4]);
    EXPECT_EQ((Get<2,2>(mat4)), m_cptr[ 5]);
    EXPECT_EQ((Get<2,3>(mat4)), m_cptr[ 6]);
    EXPECT_EQ((Get<2,4>(mat4)), m_cptr[ 7]);
    EXPECT_EQ((Get<3,1>(mat4)), m_cptr[ 8]);
    EXPECT_EQ((Get<3,2>(mat4)), m_cptr[ 9]);
    EXPECT_EQ((Get<3,3>(mat4)), m_cptr[10]);
    EXPECT_EQ((Get<3,4>(mat4)), m_cptr[11]);
    EXPECT_EQ((Get<4,1>(mat4)), m_cptr[12]);
    EXPECT_EQ((Get<4,2>(mat4)), m_cptr[13]);
    EXPECT_EQ((Get<4,3>(mat4)), m_cptr[14]);
    EXPECT_EQ((Get<4,4>(mat4)), m_cptr[15]);
}

TEST(Matrix4, Equality)
{
    using namespace gbMath;
    Matrix4<int> const mat1;
    Matrix4<int> mat2(mat1);
    EXPECT_TRUE(mat1 == mat2);
    EXPECT_TRUE(mat2 == mat1);
    EXPECT_FALSE(mat1 != mat2);
    EXPECT_FALSE(mat2 != mat1);
    Get<1,1>(mat2) = 5;
    EXPECT_TRUE(mat1 != mat2);
    EXPECT_TRUE(mat2 != mat1);
    EXPECT_FALSE(mat1 == mat2);
    EXPECT_FALSE(mat2 == mat1);
}

TEST(Matrix4, Arithmetic)
{
    using namespace gbMath;
    Matrix4<int> const mat1( 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16);
    Matrix4<int> const mat2(17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32);
    
    Matrix4<int> sum = mat1 + mat2;
    EXPECT_EQ(Matrix4<int>(18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48), sum);
    sum += mat1;
    EXPECT_EQ(Matrix4<int>(19,22,25,28,31,34,37,40,43,46,49,52,55,58,61,64), sum);

    Matrix4<int> mul = mat1 * 3;
    EXPECT_EQ(Matrix4<int>(3, 6, 9,12,15,18,21,24,27,30,33,36,39,42,45,48), mul);
    mul *= 2;
    EXPECT_EQ(Matrix4<int>(6,12,18,24,30,36,42,48,54,60,66,72,78,84,90,96), mul);

    Matrix4<int> div = mul / 3;
    EXPECT_EQ(Matrix4<int>(2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32), div);
    div /= 2;
    EXPECT_EQ(mat1, div);

    Matrix4<int> neg = -mat1;
    EXPECT_EQ(Matrix4<int>(-1, -2, -3, -4, -5, -6, -7, -8, -9,-10,-11,-12,-13,-14,-15,-16), neg);
    Matrix4<int> mat_null;
    Null(mat_null);
    EXPECT_EQ(mat_null, mat2 + (-mat2));

    Matrix4<int> sub = sum - (mat1 + mat2);
    EXPECT_EQ(mat1, sub);
    sub += -1 * mat1;
    EXPECT_EQ(mat_null, sub);
    sub -= mat1;
    EXPECT_EQ((-mat1), sub);
}

TEST(Matrix4, MatrixMultiplication)
{
    using namespace gbMath;
    Matrix4<int> const ident;
    Matrix4<int> const mat1(  1, 2, 3, 4,
                              5, 6, 7, 8,
                              9,10,11,12,
                             13,14,15,16 );
    Matrix4<int> const mat2( 0, -1, 0, 0,
                             1,  0, 0, 2,
                             0,  0, 2, 0,
                             0,  4, 0, 0);

    Matrix4<int> mul = ident * mat1;
    EXPECT_EQ(mat1, mul);
    mul *= ident;
    EXPECT_EQ(mat1, mul);

    mul = mat1 * mat2;
    EXPECT_EQ( Matrix4<int>(  2, 15,  6,  4,
                              6, 27, 14, 12,
                             10, 39, 22, 20,
                             14, 51, 30, 28 ),
               mul);
    mul = mat2 * mat1;
    EXPECT_EQ( Matrix4<int>( -5, -6, -7, -8,
                             27, 30, 33, 36,
                             18, 20, 22, 24,
                             20, 24, 28, 32),
               mul);
}

TEST(Matrix4, Transpose)
{
    using namespace gbMath;
    Matrix4<int> mat1(  1, 2, 3, 4,
                        5, 6, 7, 8,
                        9,10,11,12,
                       13,14,15,16 );
    Matrix4<int> mat_orig(mat1);
    mat1.TransposeSelf();
    EXPECT_EQ(Matrix4<int>(  1, 5, 9,13,
                             2, 6,10,14,
                             3, 7,11,15,
                             4, 8,12,16),
              mat1);

    Matrix4<int> mat2 = mat1.GetTranspose();
    EXPECT_EQ(mat_orig, mat2);
}

TEST(Matrix4, Determinant)
{
    using namespace gbMath;
    Matrix4<int> const mat1;
    Matrix4<int> const mat2(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16);
    Matrix4<int> const mat3( 42,54,36,-7,
                              4,52,64,77,
                             81,92,-5,47,
                             11,-9,22, 6 );

    EXPECT_EQ(1, mat1.GetDeterminant());
    EXPECT_EQ(0, mat2.GetDeterminant());
    EXPECT_EQ(-12080892, mat3.GetDeterminant());
}

TEST(Matrix4, IsOrthogonal)
{
    using namespace gbMath;
    Matrix4<int> mat;
    EXPECT_TRUE(mat.IsOrthogonal());
    Get<2,2>(mat) = 5;
    EXPECT_FALSE(mat.IsOrthogonal());
    Matrix4<float> f_mat;
    EXPECT_TRUE(f_mat.IsOrthogonal());
    Get<2,2>(f_mat) = 5.0f;
    EXPECT_FALSE(f_mat.IsOrthogonal());
}

TEST(Matrix4, IsInvertible)
{
    using namespace gbMath;
    Matrix4<int> mat1;
    EXPECT_TRUE(mat1.IsInvertible());
    Get<2,2>(mat1) = 0;
    EXPECT_FALSE(mat1.IsInvertible());
    Matrix4<float> f_mat1;
    EXPECT_TRUE(f_mat1.IsInvertible());
    Get<2,2>(f_mat1) = 0.0f;
    EXPECT_FALSE(f_mat1.IsInvertible());
}

TEST(Matrix4, Inverse)
{
    using namespace gbMath;
    Matrix4<int> const mat1( 42,54,36,-7,
                              4,52,64,77,
                             81,92,-5,47,
                             11,-9,22, 6 );
    ASSERT_TRUE(mat1.IsInvertible());
    int factor;
    Matrix4<int> const mat_inv = mat1.GetInverse(&factor);
    EXPECT_EQ(mat1.GetDeterminant(), factor);
    EXPECT_EQ(Matrix4<int>( 34655,106409,-118960,-393298,-139177,-56431,21080,396698,-128545,
                            -54811,90908,-158674,199032,-78756,-83616,-115584), mat_inv);

    Matrix4<int> mat2 = mat1 * mat_inv;
    mat2 /= factor;
    EXPECT_EQ(Matrix4<int>(), mat2);

    Matrix4<float> const f_mat(mat1);
    float ffactor;
    Matrix4<float> f_mat_inv = f_mat.GetInverse(&ffactor);
    EXPECT_EQ(static_cast<float>(factor), ffactor);
    Matrix4<float> f_ident = f_mat * f_mat_inv;
    f_ident /= ffactor;
    EXPECT_TRUE(f_ident.IsIdentity());
}

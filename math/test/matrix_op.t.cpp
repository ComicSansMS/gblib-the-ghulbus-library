#include <gtest/gtest.h>

#include <gbMath/matrix_op.hpp>

TEST(MatrixOp, Matrix2VectorTransform)
{
    using namespace gbMath;
    Matrix2<float> const mat_ident;
    Vector2<float> v(17.0f, 42.0f);
    EXPECT_EQ(v, TransformVector(mat_ident, v));

    Matrix2<float> const mat_rot90(0.0f, 1.0f, -1.0f, 0.0f);
    EXPECT_EQ(v.y_, TransformVector(mat_rot90, v).x_);
    EXPECT_EQ(-v.x_, TransformVector(mat_rot90, v).y_);
}


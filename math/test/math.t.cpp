#include <gtest/gtest.h>

#include <gbMath/math.hpp>

#include <limits>

TEST(Util, AbsTest)
{
    using namespace gbMath;
    EXPECT_EQ(2, Abs(2));
    EXPECT_EQ(2, Abs(-2));

    EXPECT_EQ(1.23f, Abs(1.23f));
    EXPECT_EQ(1.23f, Abs(-1.23f));

    EXPECT_EQ(1.23, Abs(1.23));
    EXPECT_EQ(1.23, Abs(-1.23));
}


TEST(Util, MaxOrInf)
{
    using namespace gbMath;
    EXPECT_EQ(std::numeric_limits<float>::infinity(), MaxOrInf<float>());
    EXPECT_EQ(std::numeric_limits<double>::infinity(), MaxOrInf<double>());
    EXPECT_EQ(std::numeric_limits<int>::max(), MaxOrInf<int>());
    EXPECT_EQ(std::numeric_limits<char>::max(), MaxOrInf<char>());
    EXPECT_EQ(std::numeric_limits<short>::max(), MaxOrInf<short>());
    EXPECT_EQ(std::numeric_limits<long>::max(), MaxOrInf<long>());
    EXPECT_EQ(std::numeric_limits<unsigned int>::max(), MaxOrInf<unsigned int>());
    EXPECT_EQ(std::numeric_limits<unsigned char>::max(), MaxOrInf<unsigned char>());
    EXPECT_EQ(std::numeric_limits<unsigned short>::max(), MaxOrInf<unsigned short>());
    EXPECT_EQ(std::numeric_limits<unsigned long>::max(), MaxOrInf<unsigned long>());
}

TEST(Util, Lerp)
{
    using namespace gbMath;
    EXPECT_EQ(0.5, Lerp(0.5, 0.0, 1.0));
    EXPECT_EQ(187.5, Lerp(0.75, 150.0, 200.0));

    EXPECT_EQ(42, Lerp(1, 23, 42));
}

TEST(Util, Clamp)
{
    using namespace gbMath;
    EXPECT_EQ(2, Clamp(1, 2, 3));
    EXPECT_EQ(42.23, Clamp(42.23, 1.0, 100.0));
    EXPECT_EQ(-20.0, Clamp(-5.0, -MaxOrInf<double>(), -20.0));
    EXPECT_EQ(23.0, Clamp(42.0, 1.0, 23.0));
    EXPECT_EQ(0, Clamp(-1, 0, 100));
    EXPECT_EQ(15, Clamp(15, 15, 15));
}


#include <gtest/gtest.h>

#include <gbMath/float_util.hpp>
#include <gbMath/constants.hpp>


TEST(FloatUtil, FloatInspector)
{
    using namespace gbMath;
    FloatUtil::FloatInspector<float> f_one(1.0f);
    EXPECT_EQ(127u, f_one.Exponent());
    EXPECT_EQ(0u, f_one.Mantissa());
    EXPECT_EQ(0u, f_one.Sign());

    FloatUtil::FloatInspector<float> f_zero(0.0f);
    EXPECT_EQ(0u, f_zero.Exponent());
    EXPECT_EQ(0u, f_zero.Mantissa());
    EXPECT_EQ(0u, f_zero.Sign());

    FloatUtil::FloatInspector<float> f_negzero(-0.0f);
    EXPECT_EQ(0u, f_negzero.Exponent());
    EXPECT_EQ(0u, f_negzero.Mantissa());
    EXPECT_EQ(1u, f_negzero.Sign());

    FloatUtil::FloatInspector<float> f_inf(std::numeric_limits<float>::infinity());
    EXPECT_EQ(255u, f_inf.Exponent());
    EXPECT_EQ(0u, f_inf.Mantissa());
    EXPECT_EQ(0u, f_inf.Sign());

    FloatUtil::FloatInspector<float> f_nan(std::numeric_limits<float>::quiet_NaN());
    EXPECT_EQ(255u, f_nan.Exponent());
    EXPECT_NE(0u, f_nan.Mantissa());

    FloatUtil::FloatInspector<double> d_one(1.0);
    EXPECT_EQ(1023u, d_one.Exponent());
    EXPECT_EQ(0u, d_one.Mantissa());
    EXPECT_EQ(0u, d_one.Sign());

    FloatUtil::FloatInspector<double> d_zero(0.0);
    EXPECT_EQ(0u, d_zero.Exponent());
    EXPECT_EQ(0u, d_zero.Mantissa());
    EXPECT_EQ(0u, d_zero.Sign());

    FloatUtil::FloatInspector<double> d_negzero(-0.0);
    EXPECT_EQ(0u, d_negzero.Exponent());
    EXPECT_EQ(0u, d_negzero.Mantissa());
    EXPECT_EQ(1u, d_negzero.Sign());

    FloatUtil::FloatInspector<double> d_inf(std::numeric_limits<double>::infinity());
    EXPECT_EQ((1<<11)-1u, d_inf.Exponent());
    EXPECT_EQ(0u, d_inf.Mantissa());
    EXPECT_EQ(0u, d_inf.Sign());

    FloatUtil::FloatInspector<double> d_nan(std::numeric_limits<double>::quiet_NaN());
    EXPECT_EQ((1<<11)-1u, d_nan.Exponent());
    EXPECT_NE(0u, d_nan.Mantissa());
}

TEST(FloatUtil, FloatFactory)
{
    using namespace gbMath;
    EXPECT_EQ(1.0f, FloatUtil::BuildFloat<float>(0, 0x7F, 0));
    float f_nan = FloatUtil::BuildFloat<float>(1, 0xFF, 0);
    EXPECT_NE(f_nan, f_nan);
    EXPECT_THROW(FloatUtil::BuildFloat<float>(0, 0x100, 0), Diagnostic::Exceptions::InvalidArgument);
    FloatUtil::FloatInspector<float> f_pi(Constf::PI);
    EXPECT_EQ(Constf::PI, FloatUtil::BuildFloat<float>(f_pi.Mantissa(), f_pi.Exponent(), f_pi.Sign()));

    EXPECT_EQ(1.0f, FloatUtil::BuildFloat<double>(0, 0x3FF, 0));
    double d_nan = FloatUtil::BuildFloat<double>(1, 0x7FF, 0);
    EXPECT_NE(d_nan, d_nan);
    EXPECT_THROW(FloatUtil::BuildFloat<double>(0, 0x800, 0), Diagnostic::Exceptions::InvalidArgument);
    FloatUtil::FloatInspector<double> d_pi(Const::PI);
    EXPECT_EQ(Const::PI, FloatUtil::BuildFloat<double>(d_pi.Mantissa(), d_pi.Exponent(), d_pi.Sign()));
}

TEST(FloatUtil, Equality)
{
    using namespace gbMath;
    using namespace gbMath::FloatUtil;
    float f1 = 1.0f;
    float f1_off = 1.0f + 1e-5f;
    EXPECT_TRUE(Equal_AbsDelta(f1, f1, 0.0f));
    EXPECT_TRUE(Equal_AbsDelta(f1_off, f1_off, 0.0f));
    EXPECT_FALSE(Equal_AbsDelta(f1, f1_off, 0.0f));
    EXPECT_FALSE(Equal_AbsDelta(f1, f1_off, 0.999e-6f));
    EXPECT_TRUE(Equal_AbsDelta(f1, f1_off, 11e-6f));
    float fnan = std::numeric_limits<float>::quiet_NaN();
    EXPECT_FALSE(Equal_AbsDelta(f1, fnan, 1e32f));
    EXPECT_FALSE(Equal_AbsDelta(fnan, fnan, 1e32f));

    float f2 = 1e5f;
    float f2_off = 1e5f + 0.1f;
    EXPECT_FALSE(Equal_AbsDelta(f2, f2_off, 11e-6f));
    EXPECT_TRUE(Equal_RelDelta(f2, f2, 0.0f));
    EXPECT_TRUE(Equal_RelDelta(f2_off, f2_off, 0.0f));
    EXPECT_TRUE(Equal_RelDelta(f2, f2_off, 11e-6f));
    EXPECT_TRUE(Equal_RelDelta(f1, f1_off, 11e-6f));

    float f3 = 0.0f + std::numeric_limits<float>::epsilon();
    EXPECT_TRUE(IsZero(0.0f));
    EXPECT_TRUE(IsZero(-0.0f));
    EXPECT_FALSE(IsZero(f3));
    EXPECT_FALSE(IsZero(-f3));
    EXPECT_TRUE(IsZero(f3, std::numeric_limits<float>::epsilon()));
    EXPECT_TRUE(IsZero(-f3, std::numeric_limits<float>::epsilon()));

    EXPECT_FALSE(IsNan(f1) || IsNan(f2) || IsNan(f3));
    EXPECT_TRUE(IsNan(fnan));

    EXPECT_FALSE(IsNegative(f1));
    EXPECT_TRUE(IsNegative(f3-f1));

    float f1_eps = f1 + std::numeric_limits<float>::epsilon();
    EXPECT_TRUE(Equal_ULP(f1, f1, 0.0f));
    EXPECT_FALSE(Equal_ULP(f1, f1_eps, 0.0f));
    EXPECT_TRUE(Equal_ULP(f1, f1_eps, 1.0f));
    EXPECT_FALSE(Equal_ULP(f1, f1_off, 1.0f));
    EXPECT_TRUE(Equal_ULP(f1, f1_off, 100.0f));

    ///@todo Test Equal()
}


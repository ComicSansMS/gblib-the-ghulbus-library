#include <gtest/gtest.h>

#include <gbMath/matrix2.hpp>

TEST(Matrix2, Construction)
{
    using namespace gbMath;
    Matrix2<int> mat;
    EXPECT_EQ(1, (Get<1,1>(mat)));
    EXPECT_EQ(0, (Get<1,2>(mat)));
    EXPECT_EQ(0, (Get<2,1>(mat)));
    EXPECT_EQ(1, (Get<2,2>(mat)));

    Matrix2<int> mat2(5, 4, 3, 2);
    EXPECT_EQ(5, (Get<1,1>(mat2)));
    EXPECT_EQ(4, (Get<1,2>(mat2)));
    EXPECT_EQ(3, (Get<2,1>(mat2)));
    EXPECT_EQ(2, (Get<2,2>(mat2)));

    int arr[] = { 6, 7, 8, 9 };
    Matrix2<int> mat3(arr);
    EXPECT_EQ(6, (Get<1,1>(mat3)));
    EXPECT_EQ(7, (Get<1,2>(mat3)));
    EXPECT_EQ(8, (Get<2,1>(mat3)));
    EXPECT_EQ(9, (Get<2,2>(mat3)));

    Matrix2<int> mat4(mat3);
    EXPECT_EQ(6, (Get<1,1>(mat4)));
    EXPECT_EQ(7, (Get<1,2>(mat4)));
    EXPECT_EQ(8, (Get<2,1>(mat4)));
    EXPECT_EQ(9, (Get<2,2>(mat4)));

    mat4 = mat2;
    EXPECT_EQ(5, (Get<1,1>(mat4)));
    EXPECT_EQ(4, (Get<1,2>(mat4)));
    EXPECT_EQ(3, (Get<2,1>(mat4)));
    EXPECT_EQ(2, (Get<2,2>(mat4)));
}

TEST(Matrix2, MemberAccess)
{
    using namespace gbMath;
    Matrix2<int> mat2(5, 4, 3, 2);
    Matrix2<int> const& mat2_c = mat2;

    EXPECT_EQ((Get<1,1>(mat2)), mat2(1,1));
    EXPECT_EQ((Get<1,2>(mat2)), mat2(1,2));
    EXPECT_EQ((Get<2,1>(mat2)), mat2(2,1));
    EXPECT_EQ((Get<2,2>(mat2)), mat2(2,2));

    EXPECT_EQ((Get<1,1>(mat2)), mat2_c(1,1));
    EXPECT_EQ((Get<1,2>(mat2)), mat2_c(1,2));
    EXPECT_EQ((Get<2,1>(mat2)), mat2_c(2,1));
    EXPECT_EQ((Get<2,2>(mat2)), mat2_c(2,2));

    EXPECT_EQ((Get<1,1>(mat2)), (Get<1,1>(mat2_c)));
    EXPECT_EQ((Get<1,2>(mat2)), (Get<1,2>(mat2_c)));
    EXPECT_EQ((Get<2,1>(mat2)), (Get<2,1>(mat2_c)));
    EXPECT_EQ((Get<2,2>(mat2)), (Get<2,2>(mat2_c)));

    int* m_ptr = mat2;
    EXPECT_EQ((Get<1,1>(mat2)), m_ptr[0]);
    EXPECT_EQ((Get<1,2>(mat2)), m_ptr[1]);
    EXPECT_EQ((Get<2,1>(mat2)), m_ptr[2]);
    EXPECT_EQ((Get<2,2>(mat2)), m_ptr[3]);

    int const* m_cptr = mat2_c;
    EXPECT_EQ((Get<1,1>(mat2)), m_cptr[0]);
    EXPECT_EQ((Get<1,2>(mat2)), m_cptr[1]);
    EXPECT_EQ((Get<2,1>(mat2)), m_cptr[2]);
    EXPECT_EQ((Get<2,2>(mat2)), m_cptr[3]);
}

TEST(Matrix2, Equality)
{
    using namespace gbMath;
    Matrix2<int> const mat1;
    Matrix2<int> mat2(mat1);
    EXPECT_TRUE(mat1 == mat2);
    EXPECT_TRUE(mat2 == mat1);
    EXPECT_FALSE(mat1 != mat2);
    EXPECT_FALSE(mat2 != mat1);
    Get<1,1>(mat2) = 5;
    EXPECT_TRUE(mat1 != mat2);
    EXPECT_TRUE(mat2 != mat1);
    EXPECT_FALSE(mat1 == mat2);
    EXPECT_FALSE(mat2 == mat1);
}

TEST(Matrix2, Arithmetic)
{
    using namespace gbMath;
    Matrix2<int> const mat1(1,2,3,4);
    Matrix2<int> const mat2(5,6,7,8);
    
    Matrix2<int> sum = mat1 + mat2;
    EXPECT_EQ(Matrix2<int>(6,8,10,12), sum);
    sum += mat1;
    EXPECT_EQ(Matrix2<int>(7,10,13,16), sum);

    Matrix2<int> mul = mat1 * 3;
    EXPECT_EQ(Matrix2<int>(3,6,9,12), mul);
    mul *= 2;
    EXPECT_EQ(Matrix2<int>(6,12,18,24), mul);

    Matrix2<int> div = mul / 3;
    EXPECT_EQ(Matrix2<int>(2,4,6,8), div);
    div /= 2;
    EXPECT_EQ(mat1, div);

    Matrix2<int> neg = -mat1;
    EXPECT_EQ(Matrix2<int>(-1, -2, -3, -4), neg);
    EXPECT_EQ(Matrix2<int>(0,0,0,0), mat2 + (-mat2));

    Matrix2<int> sub = sum - (mat1 + mat2);
    EXPECT_EQ(mat1, sub);
    sub += -1 * mat1;
    EXPECT_EQ(Matrix2<int>(0,0,0,0), sub);
    sub -= mat1;
    EXPECT_EQ((-mat1), sub);
}

TEST(Matrix2, MatrixMultiplication)
{
    using namespace gbMath;
    Matrix2<int> const ident;
    Matrix2<int> const mat1(1,2,3,4);
    Matrix2<int> const mat2(0,-1,1,0);

    Matrix2<int> mul = ident * mat1;
    EXPECT_EQ(mat1, mul);
    mul *= ident;
    EXPECT_EQ(mat1, mul);

    mul = mat1 * mat2;
    EXPECT_EQ(Matrix2<int>(2, -1, 4, -3), mul);
    mul = mat2 * mat1;
    EXPECT_EQ(Matrix2<int>(-3, -4, 1, 2), mul);
}

TEST(Matrix2, Transpose)
{
    using namespace gbMath;
    Matrix2<int> mat1(1,2,3,4);
    mat1.TransposeSelf();
    EXPECT_EQ(Matrix2<int>(1,3,2,4), mat1);

    Matrix2<int> mat2 = mat1.GetTranspose();
    EXPECT_EQ(Matrix2<int>(1,2,3,4), mat2);
}

TEST(Matrix2, Determinant)
{
    using namespace gbMath;
    Matrix2<int> const mat1;
    Matrix2<int> const mat2(1,2,3,4);
    Matrix2<int> const mat3(1,-1,-1,1);

    EXPECT_EQ(1, mat1.GetDeterminant());
    EXPECT_EQ(-2, mat2.GetDeterminant());
    EXPECT_EQ(0, mat3.GetDeterminant());
}

TEST(Matrix2, IsOrthogonal)
{
    using namespace gbMath;
    Matrix2<int> mat;
    EXPECT_TRUE(mat.IsOrthogonal());
    Get<2,2>(mat) = 5;
    EXPECT_FALSE(mat.IsOrthogonal());
    Matrix2<float> f_mat;
    EXPECT_TRUE(f_mat.IsOrthogonal());
    Get<2,2>(f_mat) = 5.0f;
    EXPECT_FALSE(f_mat.IsOrthogonal());
}

TEST(Matrix2, IsInvertible)
{
    using namespace gbMath;
    Matrix2<int> mat1;
    EXPECT_TRUE(mat1.IsInvertible());
    Get<2,2>(mat1) = 0;
    EXPECT_FALSE(mat1.IsInvertible());
    Matrix2<float> f_mat1;
    EXPECT_TRUE(f_mat1.IsInvertible());
    Get<2,2>(f_mat1) = 0.0f;
    EXPECT_FALSE(f_mat1.IsInvertible());
}

TEST(Matrix2, Inverse)
{
    using namespace gbMath;
    Matrix2<int> const mat1( 42,54,
                              4,51 );
    ASSERT_TRUE(mat1.IsInvertible());
    int factor;
    Matrix2<int> const mat_inv = mat1.GetInverse(&factor);
    EXPECT_EQ(mat1.GetDeterminant(), factor);
    EXPECT_EQ(Matrix2<int>( 51,-54,-4,42), mat_inv);

    Matrix2<int> mat2 = mat1 * mat_inv;
    mat2 /= factor;
    EXPECT_EQ(Matrix2<int>(), mat2);

    Matrix2<float> const f_mat(mat1);
    float ffactor;
    Matrix2<float> f_mat_inv = f_mat.GetInverse(&ffactor);
    EXPECT_EQ(static_cast<float>(factor), ffactor);
    Matrix2<float> f_ident = f_mat * f_mat_inv;
    f_ident /= ffactor;
    EXPECT_TRUE(f_ident.IsIdentity());
}

#include <gtest/gtest.h>

#include <diagnostic/diagnostic.hpp>
#include <gbMath/math.hpp>

TEST(Vector2, DefConst)
{
    using namespace gbMath;
    Vector2d v2;
    EXPECT_EQ(0.0, v2.x_);
    EXPECT_EQ(0.0, v2.y_);
}

TEST(Vector2, Const)
{
    using namespace gbMath;
    {
        Vector2d v2(17.0, 42.0);
        EXPECT_EQ(17.0, v2.x_);
        EXPECT_EQ(42.0, v2.y_);
    }

    {
        double arr[] = {12.5, 54.6};
        Vector2d v2(arr);
        EXPECT_EQ(arr[0], v2.x_);
        EXPECT_EQ(arr[1], v2.y_);
    }
}

TEST(Vector2, CopyConst)
{
    using namespace gbMath;
    Vector2<double> v2a(1.2f, 3.4f);
    Vector2<double> v2b(v2a);
    EXPECT_EQ(v2b.x_, v2a.x_);
    EXPECT_EQ(v2b.y_, v2a.y_);

    Vector2<int> v2c(v2a);
    EXPECT_EQ(1, v2c.x_);
    EXPECT_EQ(3, v2c.y_);
}

TEST(Vector2, Assignment)
{
    using namespace gbMath;
    Vector2d v2a(1.2f, 3.4f);
    Vector2d v2b;
    ASSERT_NE(v2a.x_, v2b.x_);
    ASSERT_NE(v2a.y_, v2b.y_);

    v2b = v2a;

    EXPECT_EQ(v2a.x_, v2b.x_);
    EXPECT_EQ(v2a.y_, v2b.y_);
}

TEST(Vector2, Equality)
{
    using namespace gbMath;
    Vector2d v2a(1.2f, 3.4f);
    Vector2d v2b;

    EXPECT_FALSE(v2a == v2b);
    EXPECT_TRUE(v2a != v2b);
    v2a = v2b;
    EXPECT_TRUE(v2a == v2b);
    EXPECT_FALSE(v2a != v2b);
}

TEST(Vector2, UnaryMinus)
{
    using namespace gbMath;
    Vector2i v2a(2, 5);
    
    EXPECT_EQ((-v2a).x_, -(v2a.x_));
    EXPECT_EQ((-v2a).y_, -(v2a.y_));
    
    Vector2i v2b;
    EXPECT_EQ(v2b, (-v2b));
}

TEST(Vector2, Addition)
{
    using namespace gbMath;
    Vector2d v2a(1.2, 3.4);
    Vector2d v2b;

    v2b += v2a;
    EXPECT_TRUE(v2b == v2a);

    Vector2d v2c(4.5, 5.4);
    v2b = v2a + v2c;
    EXPECT_EQ(5.7, v2b.x_);
    EXPECT_EQ(8.8, v2b.y_);

    Vector2d v2d;
    v2b += -v2b;
    EXPECT_EQ(v2b, v2d);
}

TEST(Vector2, Subtraction)
{
    using namespace gbMath;
    Vector2i v2a(1, 2);
    Vector2i v2b;
    EXPECT_EQ(v2b, v2a-v2a);

    Vector2i v2c(25, -3);
    v2b = v2a - v2c;
    EXPECT_EQ(-24, v2b.x_);
    EXPECT_EQ(5, v2b.y_);
}

TEST(Vector2, Scale)
{
    using namespace gbMath;
    Vector2i v2(1, 2);
    v2 = v2 * 5;
    EXPECT_EQ(5, v2.x_);
    EXPECT_EQ(10, v2.y_);
    v2 *= 10;
    EXPECT_EQ(50, v2.x_);
    EXPECT_EQ(100, v2.y_);
    EXPECT_EQ(v2*42, 42*v2);

    v2 /= 5;
    EXPECT_EQ(10, v2.x_);
    EXPECT_EQ(20, v2.y_);
    v2 = v2 / 10;
    EXPECT_EQ(1, v2.x_);
    EXPECT_EQ(2, v2.y_);
}

TEST(Vector2, GetLength)
{
    using namespace gbMath;
    Vector2i v2;
    EXPECT_EQ(0, v2.GetLength());
    EXPECT_EQ(0, v2.GetLengthSq());
    v2 = Vector2i(2, 3);
    EXPECT_EQ(3, v2.GetLength());
    EXPECT_EQ(13, v2.GetLengthSq());

    Vector2d v2d(v2);
    EXPECT_EQ(std::sqrt(13.0), v2d.GetLength());
}

TEST(Vector2, Normalize)
{
    using namespace gbMath;
    Vector2d v2(2523.0, 23123.0);
    v2.NormalizeSelf();
    EXPECT_LT(Abs(v2.GetLength() - 1.0), Const::EPSILON);
}

TEST(Vector2, Dot)
{
    using namespace gbMath;
    Vector2i v2a(2, 3);
    Vector2i v2b(5, 6);
    EXPECT_EQ(28, Dot(v2a, v2b));
}

TEST(Vector2, LinDep)
{
    using namespace gbMath;
    Vector2d v2a(1.0, 2.0);
    Vector2d v2b(2.0, 4.0);
    Vector2d v2c(1.0, 3.0);
    Vector2d v2d(7.3, 14.6);
    
    EXPECT_EQ(2.0, v2a.CheckLinearDependence(v2b));
    EXPECT_EQ(0.0, v2a.CheckLinearDependence(v2c));
    EXPECT_EQ(3.65, v2b.CheckLinearDependence(v2d));
    EXPECT_EQ(7.3, v2a.CheckLinearDependence(v2d));
    EXPECT_EQ(1.0 / v2a.CheckLinearDependence(v2b), v2b.CheckLinearDependence(v2a));
    EXPECT_EQ(v2a.CheckLinearDependence(v2c), v2c.CheckLinearDependence(v2a));
    EXPECT_LT((1.0 / v2d.CheckLinearDependence(v2b) - v2b.CheckLinearDependence(v2d)), Const::EPSILON);

}


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gtest/gtest.h>

#include <utility>

#include <gbGraphics/color.hpp>
#include <gbGraphics/image.hpp>
#include <gbGraphics/graphics_exception.hpp>

TEST(gbImage, Construction)
{
    using namespace GhulbusGraphics;
    {
        gbImage img(12, 21);
        EXPECT_EQ(12, img.GetWidth());
        EXPECT_EQ(21, img.GetHeight());
    }
    {
        gbImage img1(0, 0);
        gbImage img2(0, 0, gbColor::ARGB(1, 2, 3, 4));
        gbImage img3(0, 0, nullptr);
        gbImage img4(0, 0, reinterpret_cast<GBCOLOR*>(0x12345));
    }
    {
        EXPECT_THROW(new gbImage(-1, 0), Diagnostic::Exceptions::InvalidArgument);
        EXPECT_THROW(new gbImage(0, -1), Diagnostic::Exceptions::InvalidArgument);
        EXPECT_THROW(new gbImage(0, -1, gbColor::ARGB(1,2,3,4)), Diagnostic::Exceptions::InvalidArgument);
        EXPECT_THROW(new gbImage(0, -1, nullptr), Diagnostic::Exceptions::InvalidArgument);
    }
    {
        gbImage img(10, 32, gbColor::ARGB(1, 2, 3, 4));
        EXPECT_EQ(10, img.GetWidth());
        EXPECT_EQ(32, img.GetHeight());
        ASSERT_FALSE(img.IsLocked());
        {
            SurfaceLock<gbImage> lock(img);
            EXPECT_TRUE(img.IsLocked());
            ASSERT_EQ(img.GetWidth(), lock.GetPitch());
            ASSERT_NE(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
            for(int i=0; i<img.GetWidth()*img.GetHeight(); ++i) {
                EXPECT_EQ(gbColor::ARGB(1, 2, 3, 4), lock.GetLockedData()[i]);
            }
        }
        EXPECT_FALSE(img.IsLocked());
    }
    {
        GBCOLOR foo[] = { gbColor::ARGB(1, 2, 3, 4), gbColor::ARGB(5, 6, 7, 8),
                          gbColor::ARGB(9, 10, 11, 12), gbColor::ARGB(4, 8, 15, 16),
                          gbColor::ARGB(23, 42, 64, 128), gbColor::ARGB(0, 0, 0, 1) };
        gbImage img(2, 3, foo);
        EXPECT_EQ(2, img.GetWidth());
        EXPECT_EQ(3, img.GetHeight());
        ASSERT_FALSE(img.IsLocked());
        SurfaceLock<gbImage> lock(img);
        EXPECT_TRUE(img.IsLocked());
        ASSERT_EQ(img.GetWidth(), lock.GetPitch());
        ASSERT_NE(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
        for(unsigned int i=0; i<sizeof(foo)/sizeof(foo[0]); ++i) {
            EXPECT_EQ(foo[i], lock.GetLockedData()[i]);
        }
    }
}

TEST(gbImage, CopyConstruction)
{
    using namespace GhulbusGraphics;
    GBCOLOR foo[] = { gbColor::ARGB(1, 2, 3, 4), gbColor::ARGB(5, 6, 7, 8),
                      gbColor::ARGB(9, 10, 11, 12), gbColor::ARGB(4, 8, 15, 16),
                      gbColor::ARGB(23, 42, 64, 128), gbColor::ARGB(0, 0, 0, 1) };
    gbImage img(2, 3, foo);
    gbImage img_copy(img);
    EXPECT_EQ(img.GetWidth(), img_copy.GetWidth());
    EXPECT_EQ(img.GetHeight(), img_copy.GetHeight());
    ASSERT_FALSE(img_copy.IsLocked());
    SurfaceLock<gbImage> lock(img_copy);
    EXPECT_TRUE(img_copy.IsLocked());
    ASSERT_EQ(img_copy.GetWidth(), lock.GetPitch());
    ASSERT_NE(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
    for(unsigned int i=0; i<sizeof(foo)/sizeof(foo[0]); ++i) {
        EXPECT_EQ(foo[i], lock.GetLockedData()[i]);
    }
}

TEST(gbImage, MoveConstruction)
{
    using namespace GhulbusGraphics;
    GBCOLOR foo[] = { gbColor::ARGB(1, 2, 3, 4), gbColor::ARGB(5, 6, 7, 8),
                      gbColor::ARGB(9, 10, 11, 12), gbColor::ARGB(4, 8, 15, 16),
                      gbColor::ARGB(23, 42, 64, 128), gbColor::ARGB(0, 0, 0, 1) };
    gbImage img(2, 3, foo);
    gbImage img_copy(std::move(img));
    EXPECT_EQ(2, img_copy.GetWidth());
    EXPECT_EQ(3, img_copy.GetHeight());
    EXPECT_TRUE(img.Release().empty());
    ASSERT_FALSE(img_copy.IsLocked());
    SurfaceLock<gbImage> lock(img_copy);
    EXPECT_TRUE(img_copy.IsLocked());
    ASSERT_EQ(img_copy.GetWidth(), lock.GetPitch());
    ASSERT_NE(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
    for(unsigned int i=0; i<sizeof(foo)/sizeof(foo[0]); ++i) {
        EXPECT_EQ(foo[i], lock.GetLockedData()[i]);
    }
}

TEST(gbImage, Release)
{
    using namespace GhulbusGraphics;
    gbImage img(42, 97);
    const int size = img.GetWidth()*img.GetHeight();
    {
        SurfaceLock<gbImage> lock(img);
        ASSERT_TRUE(img.IsLocked());
        EXPECT_NE(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
        EXPECT_EQ(img.GetWidth(), lock.GetPitch());
        ASSERT_NE(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
        EXPECT_THROW(img.Release(), Diagnostic::Exceptions::InvalidProtocol);
    }
    EXPECT_EQ(size, static_cast<int>(img.Release().size()));
    EXPECT_EQ(0, img.GetWidth());
    EXPECT_EQ(0, img.GetHeight());
    {
        SurfaceLock<gbImage> lock(img);
        ASSERT_TRUE(img.IsLocked());
        EXPECT_EQ(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
        EXPECT_EQ(0, lock.GetPitch());
        EXPECT_THROW(img.Release(), Diagnostic::Exceptions::InvalidProtocol);
    }
    EXPECT_NO_THROW(img.Release());
}

TEST(gbImage, Swap)
{
    using namespace GhulbusGraphics;
    GBCOLOR foo[] = { gbColor::ARGB(1, 2, 3, 4), gbColor::ARGB(5, 6, 7, 8),
                      gbColor::ARGB(9, 10, 11, 12), gbColor::ARGB(4, 8, 15, 16),
                      gbColor::ARGB(23, 42, 64, 128), gbColor::ARGB(0, 0, 0, 1) };
    gbImage img(2, 3, foo);
    gbImage img_copy(0, 0);
    img_copy.Swap(img);
    EXPECT_EQ(0, img.GetWidth());
    EXPECT_EQ(0, img.GetHeight());
    EXPECT_EQ(2, img_copy.GetWidth());
    EXPECT_EQ(3, img_copy.GetHeight());
    
    ASSERT_FALSE(img_copy.IsLocked());
    SurfaceLock<gbImage> lock(img_copy);
    EXPECT_TRUE(img_copy.IsLocked());
    ASSERT_EQ(img_copy.GetWidth(), lock.GetPitch());
    ASSERT_NE(static_cast<GBCOLOR*>(nullptr), lock.GetLockedData());
    for(unsigned int i=0; i<sizeof(foo)/sizeof(foo[0]); ++i) {
        EXPECT_EQ(foo[i], lock.GetLockedData()[i]);
    }

}


#version 150

in vec4 VertexPosition;
in vec3 VertexColor;
in vec3 VertexNormal;

out vec3 Color;

uniform vec4 LightPosition;
uniform vec3 Kd;                ///< Diffuse reflectivity
uniform vec3 Ld;                ///< Light source intensity

uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProject;
uniform mat4 uNormal;


void main()
{
    vec3 tnorm = normalize(uNormal * vec4(VertexNormal, 0.0)).xyz;
    vec4 eye = uView * uModel * VertexPosition;
    vec3 s = normalize(vec3(LightPosition - eye));
    Color = Ld * Kd * max(dot(s, tnorm), 0.0) * VertexColor;

    //Color = VertexColor;
    gl_Position = uProject * uView * uModel * VertexPosition;
}

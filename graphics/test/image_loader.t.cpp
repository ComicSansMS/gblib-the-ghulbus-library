/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gtest/gtest.h>

#include <test_config.hpp>
#include <gbGraphics/color.hpp>
#include <gbGraphics/image.hpp>
#include <gbGraphics/image_loader.hpp>

void TestImageRGBA(GhulbusGraphics::gbImageLoader& img_loader)
{
    using namespace GhulbusGraphics;
    const int IMAGE_WIDTH = 4;
    const int IMAGE_HEIGHT = 6;
    ASSERT_EQ(IMAGE_WIDTH, img_loader.GetImageWidth());
    ASSERT_EQ(IMAGE_HEIGHT, img_loader.GetImageHeight());
    gbImage img1(img_loader.GetImageCopy());
    ASSERT_EQ(IMAGE_WIDTH, img_loader.GetImageWidth());
    ASSERT_EQ(IMAGE_HEIGHT, img_loader.GetImageHeight());
    EXPECT_EQ(img_loader.GetImageWidth(), img1.GetWidth());
    EXPECT_EQ(img_loader.GetImageHeight(), img1.GetHeight());
    EXPECT_FALSE(img1.IsLocked());
    gbImage img2(img_loader.GetImage());
    EXPECT_EQ(0, img_loader.GetImageWidth());
    EXPECT_EQ(0, img_loader.GetImageHeight());
    EXPECT_EQ(img1.GetWidth(), img2.GetWidth());
    EXPECT_EQ(img1.GetHeight(), img2.GetHeight());
    ASSERT_FALSE(img2.IsLocked());
    SurfaceLock<gbImage> lock(img2);
    ASSERT_EQ(img2.GetWidth(), lock.GetPitch());

    EXPECT_EQ(gbColor::XRGB(0, 0, 0), lock.GetLockedData()[0]);
    EXPECT_EQ(gbColor::XRGB(255, 0, 0), lock.GetLockedData()[1]);
    EXPECT_EQ(gbColor::XRGB(0, 255, 0), lock.GetLockedData()[2]);
    EXPECT_EQ(gbColor::XRGB(0, 0, 255), lock.GetLockedData()[3]);
    
    EXPECT_EQ(gbColor::XRGB(0, 255, 255), lock.GetLockedData()[4]);
    EXPECT_EQ(gbColor::XRGB(255, 0, 255), lock.GetLockedData()[5]);
    EXPECT_EQ(gbColor::XRGB(255, 255, 0), lock.GetLockedData()[6]);
    EXPECT_EQ(gbColor::XRGB(255, 255, 255), lock.GetLockedData()[7]);
    
    EXPECT_EQ(gbColor::XRGB(0, 0, 0), lock.GetLockedData()[8]);
    EXPECT_EQ(gbColor::XRGB(64, 64, 64), lock.GetLockedData()[9]);
    EXPECT_EQ(gbColor::XRGB(128, 128, 128), lock.GetLockedData()[10]);
    EXPECT_EQ(gbColor::XRGB(192, 192, 192), lock.GetLockedData()[11]);
    
    EXPECT_EQ(gbColor::XRGB(64, 0, 0), lock.GetLockedData()[12]);
    EXPECT_EQ(gbColor::XRGB(128, 0, 0), lock.GetLockedData()[13]);
    EXPECT_EQ(gbColor::XRGB(192, 0, 0), lock.GetLockedData()[14]);
    EXPECT_EQ(gbColor::XRGB(255, 0, 0), lock.GetLockedData()[15]);

    EXPECT_EQ(gbColor::XRGB(0, 64, 0), lock.GetLockedData()[16]);
    EXPECT_EQ(gbColor::XRGB(0, 128, 0), lock.GetLockedData()[17]);
    EXPECT_EQ(gbColor::XRGB(0, 192, 0), lock.GetLockedData()[18]);
    EXPECT_EQ(gbColor::XRGB(0, 255, 0), lock.GetLockedData()[19]);

    EXPECT_EQ(gbColor::ARGB(64, 0, 0, 64), lock.GetLockedData()[20]);
    EXPECT_EQ(gbColor::ARGB(128, 0, 0, 128), lock.GetLockedData()[21]);
    EXPECT_EQ(gbColor::ARGB(192, 0, 0, 192), lock.GetLockedData()[22]);
    EXPECT_EQ(gbColor::ARGB(255, 0, 0, 255), lock.GetLockedData()[23]);
}

TEST(gbImageLoader, PNG)
{
#if defined GB_GRAPHICS_HAS_LIBPNG && defined GB_GRAPHICS_TEST_RESOURCE_DIR
    using namespace GhulbusGraphics;
    gbImageLoader img_loader(GB_GRAPHICS_TEST_RESOURCE_DIR "gfx/rgba.png", ImageFileCodec::PNG());
    TestImageRGBA(img_loader);
#endif
}


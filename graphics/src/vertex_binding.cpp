/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/vertex_binding.hpp>
#include <gbGraphics/program.hpp>

#include <diagnostic/diagnostic.hpp>

namespace GB_GRAPHICS_NAMESPACE {

gbVertexBinding::gbVertexBinding(gbProgramPtr program, gbVertexFormatBase const& vertex_format)
    :m_Program(program), m_VertexFormat(vertex_format.Clone())
{
}

gbVertexBinding::gbVertexBinding(gbVertexBinding&& rhs)
    :m_Program(std::move(rhs.m_Program)), m_VertexFormat(std::move(rhs.m_VertexFormat)),
     m_Bindings(std::move(rhs.m_Bindings))
{
}

void gbVertexBinding::Bind(int component_index, std::string const& attrib_name)
{
    int const attrib_idx = m_Program->GetVertexAttributeIndex(attrib_name);
    if(attrib_idx < 0) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(), 
                                "No attribute \'" + attrib_name + "\' in shader" );
    }
    auto const& attr_info = m_Program->GetVertexAttributeInfo(attrib_idx);
    auto const vertex_component_type = m_VertexFormat->GetComponentType(component_index);
    if(attr_info.Type != vertex_component_type) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                "Vertex component type does not match attribute type" );
    }
    DIAG_CHECK( std::find_if(std::begin(m_Bindings), std::end(m_Bindings), [&attr_info](Binding_T const& b) -> bool {
                    return (b.ShaderIndex == attr_info.LocationIndex);
                }) == std::end(m_Bindings) );
    Binding_T binding;
    binding.Type = attr_info.Type;
    binding.ShaderIndex = attr_info.LocationIndex;
    binding.VertexDataOffset = m_VertexFormat->ComponentRawOffset(component_index);
    binding.VertexDataSize = m_VertexFormat->ComponentRawSize(component_index);
    m_Bindings.push_back(binding);
}

std::vector<gbVertexBinding::Binding_T>::const_iterator gbVertexBinding::begin() const
{
    return m_Bindings.cbegin();
}

std::vector<gbVertexBinding::Binding_T>::const_iterator gbVertexBinding::end() const
{
    return m_Bindings.cend();
}

int gbVertexBinding::GetDataStride() const
{
    return m_VertexFormat->GetStride();
}

gbVertexFormatBase const* gbVertexBinding::GetVertexFormat() const
{
    return m_VertexFormat.get();
}

gbProgram const* gbVertexBinding::GetProgram() const
{
    return m_Program.get();
}

}

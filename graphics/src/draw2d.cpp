/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/draw2d.hpp>

#include <diagnostic/diagnostic.hpp>

inline void ADJ_RET(bool* old_ret, bool new_ret)
{
    *old_ret = (new_ret && *old_ret);
}

namespace GB_GRAPHICS_NAMESPACE {

static const int N_CHANNELS = sizeof(GBCOLOR) / sizeof(GBCOLOR_COMPONENT);

gbDraw2D::gbDraw2D(GBCOLOR* data, int width, int height, int pitch)
    :m_Data(data), m_width(width), m_height(height), m_pitch(pitch),
     m_clearcolor(gbColor::ARGB(0, 0, 0, 0))
{
}

gbDraw2D::gbDraw2D(LockableSurface& tex) 
    :m_Data(NULL), m_width(tex.GetWidth()), m_height(tex.GetHeight()), m_pitch(-1),
     m_clearcolor(gbColor::ARGB(0, 0, 0, 0)), m_TextureLock(tex, &m_Data, &m_pitch)
{
}

gbDraw2D::gbDraw2D(SurfaceLock<LockableSurface> const& target)
    :m_Data(target.GetLockedData()), m_width(target->GetWidth()), m_height(target->GetHeight()),
     m_pitch(target.GetPitch()), m_clearcolor(gbColor::ARGB(0, 0, 0, 0))
{
}

gbDraw2D::~gbDraw2D()
{
}

int gbDraw2D::GetWidth() const
{
    return m_width;
}

int gbDraw2D::GetHeight() const
{
    return m_height;
}

void gbDraw2D::SetClearColor(GBCOLOR c)
{
    m_clearcolor = c;
}

void gbDraw2D::Clear()
{
    for(int y=0; y<m_height; ++y) {
        for(int x=0; x<m_width; ++x) {
            m_Data[y * m_pitch + x] = m_clearcolor;
        }
    }
}

bool gbDraw2D::PutPixel(int x, int y, GBCOLOR c)
{
    if( (x<0) || (x > (m_width-1)) || (y<0) || (y > (m_height-1)) ) {
        return false;
    }
    m_Data[y * m_pitch + x] = c;
    return true;
}

bool gbDraw2D::BlendPixel(int x, int y, GBCOLOR c)
{
    ///@todo slooooow!
    if( (x<0) || (x > (m_width-1)) || (y<0) || (y > (m_height-1)) ) {
        return false;
    }
    GBCOLOR* pixel = &(m_Data[y * m_pitch + x]);
    *pixel = gbColor::Lerp(*pixel, c, gbColor::GetA(c));
    return true;
}

bool gbDraw2D::Line(int startX, int startY, int endX, int endY, GBCOLOR color)
{
    bool ret = true;
    int bres_lookup[8][4] = 
        {{  0,-1,-1,-1 },{  0,-1, 1,-1 },{  0, 1,-1, 1 },{  0, 1, 1, 1 },
            { -1, 0,-1,-1 },{  1, 0, 1,-1 },{ -1, 0,-1, 1 },{  1, 0, 1, 1 } };
    bool Z = true;
    //Bresenham's Line:
    int const bres_dx = endX - startX;        //line width
    int const bres_dy = endY - startY;        //line height
    int bres_da = abs(bres_dx);
    int bres_db = abs(bres_dy);

    if(bres_db > bres_da) {
        std::swap(bres_da, bres_db);
        Z = false;
    }
    int *m = bres_lookup[((bres_dx>=0)?1:0) + ((bres_dy>=0)?2:0) + (Z?4:0)];
    int bres_grad = 2* bres_db - bres_da;
    ADJ_RET( &ret, PutPixel(startX, startY, color) );
    for(int i = 0; i < bres_da; i++) {
        if(bres_grad >= 0) {
            startX += m[2];
            startY += m[3];
            bres_grad += 2 * bres_db - 2 * bres_da;
        } else {
            startX += m[0];
            startY += m[1];
            bres_grad += 2 * bres_db;
        }
        ADJ_RET( &ret, PutPixel(startX, startY, color) );
    }
    return ret;
}

bool gbDraw2D::HLine(int startX, int endX, int y, GBCOLOR color)
{
    //Clipping:
    if( (startX > m_width-1) || (endX<0) || (y<0) || (y>m_height-1) || (startX > endX) ) {
        return false;
    }
    bool ret = true;
    if(startX < 0)       { startX = 0;          ret = false; }
    if(endX > m_width-1) { endX   = m_width-1;  ret = false; }
    //Drawing:
    GBCOLOR* offset = &(m_Data[startX + (y * m_pitch)]);
    for(int i=startX; i<=endX; i++) {
        *(offset++) =  color;
    }
    return ret;
}

bool gbDraw2D::VLine(int x, int startY, int endY, GBCOLOR color)
{
    //Clipping:
    if( (x > m_width-1) || (x<0) || (endY<0) || (startY>m_height-1) || (startY > endY) ) {
        return false;
    }
    bool ret = true;
    if(startY < 0)        { startY = 0;           ret = false; }
    if(endY > m_height-1) { endY   = m_height-1;  ret = false; }
    //Drawing:
    GBCOLOR* offset = &(m_Data[x + (startY * m_pitch)]);
    for(int i=startY; i<=endY; i++) {
        *offset =  color;
        offset += m_pitch;
    }
    return ret;
}

bool gbDraw2D::HLineGradient(int startX, int endX, int y, GBCOLOR c1, GBCOLOR c2)
{
    //Color arithmetics:
    int r1 = gbColor::GetR(c1);
    int g1 = gbColor::GetG(c1);
    int b1 = gbColor::GetB(c1);
    int a1 = gbColor::GetA(c1);
    int r2 = gbColor::GetR(c2);
    int g2 = gbColor::GetG(c2);
    int b2 = gbColor::GetB(c2);
    int a2 = gbColor::GetA(c2);
    int dX = (endX - startX);
    int gr, gg, gb, ga;            //color gradients for rgb components
    gr = ((r2 - r1)<<16)/dX;
    gg = ((g2 - g1)<<16)/dX;
    gb = ((b2 - b1)<<16)/dX;
    ga = ((a2 - a1)<<16)/dX;

    //Clipping:
    if( (startX > m_width-1) || (endX<0) || (y<0) || (y>m_height-1) || (startX > endX) ) { 
        return false;
    }
    bool ret = true;
    int i = 0;
    if(startX < 0) { 
        i = -startX;
        startX = 0;
        ret = false; 
    }
    if(endX > m_width-1) {
        dX -= (endX-m_width-1);
        endX = m_width-1;
        ret = false;
    }

    //Drawing:
    GBCOLOR* offset = &(m_Data[startX + (y*m_pitch)]);
    for(; i<=dX; i++) {
        *(offset++) =  gbColor::ARGB( (a1 + ((ga * i)>>16) ), 
                                        (r1 + ((gr * i)>>16) ),
                                        (g1 + ((gg * i)>>16) ),
                                        (b1 + ((gb * i)>>16) ) );
    }
    return ret;
}

bool gbDraw2D::VLineGradient(int x, int startY, int endY, GBCOLOR c1, GBCOLOR c2)
{
    //Color arithmetics:
    int r1 = gbColor::GetR(c1);
    int g1 = gbColor::GetG(c1);
    int b1 = gbColor::GetB(c1);
    int a1 = gbColor::GetA(c1);
    int r2 = gbColor::GetR(c2);
    int g2 = gbColor::GetG(c2);
    int b2 = gbColor::GetB(c2);
    int a2 = gbColor::GetA(c2);
    int dY = (endY - startY);
    int gr, gg, gb, ga;            //color gradients for rgb components
    gr = ((r2 - r1)<<16)/dY;
    gg = ((g2 - g1)<<16)/dY;
    gb = ((b2 - b1)<<16)/dY;
    ga = ((a2 - a1)<<16)/dY;

    //Clipping:
    if( (x > m_width-1) || (x<0) || (endY<0) || (startY>m_height-1) || (startY > endY) ) {
        return false;
    }
    bool ret = true;
    int i = 0;
    if(startY < 0) {
        i = -startY;
        startY = 0;
        ret = false;
    }
    if(endY > m_height-1) {
        dY -= (endY-m_height-1);
        endY = m_height-1;
        ret = false;
    }
    //Drawing:
    GBCOLOR* offset = &(m_Data[x + (startY*m_pitch)]);
    for(; i<=dY; i++) {
        *offset =  gbColor::ARGB( (a1 + ((ga * i)>>16) ),
                                    (r1 + ((gr * i)>>16) ),
                                    (g1 + ((gg * i)>>16) ),
                                    (b1 + ((gb * i)>>16) ) );
        offset += m_pitch;
    }
    return ret;
}

bool gbDraw2D::Circle(int posX, int posY, int radius, GBCOLOR color) 
{
    int f = 1 - radius;
    int ddF_x = 0;  
    int ddF_y = -2 * radius;
    int x = 0;  
    int y = radius;
    bool ret = true;

    ADJ_RET( &ret, PutPixel(posX, posY + radius, color) );
    ADJ_RET( &ret, PutPixel(posX, posY - radius, color) );
    ADJ_RET( &ret, PutPixel(posX + radius, posY, color) );
    ADJ_RET( &ret, PutPixel(posX - radius, posY, color) );
     
    while(x < y) {
        if(f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;
     
        ADJ_RET( &ret, PutPixel(posX + x, posY + y, color) );
        ADJ_RET( &ret, PutPixel(posX - x, posY + y, color) );
        ADJ_RET( &ret, PutPixel(posX + x, posY - y, color) );
        ADJ_RET( &ret, PutPixel(posX - x, posY - y, color) );
        ADJ_RET( &ret, PutPixel(posX + y, posY + x, color) );
        ADJ_RET( &ret, PutPixel(posX - y, posY + x, color) );
        ADJ_RET( &ret, PutPixel(posX + y, posY - x, color) );
        ADJ_RET( &ret, PutPixel(posX - y, posY - x, color) );
    }
    return ret;
}

bool gbDraw2D::Disc(int posX, int posY, int radius, GBCOLOR color)
{
    int f = 1 - radius;
    int ddF_x = 0;  
    int ddF_y = -2 * radius;
    int x = 0;  
    int y = radius;
    bool ret = true;

    ADJ_RET( &ret, PutPixel(posX, posY + radius, color) );
    ADJ_RET( &ret, PutPixel(posX, posY - radius, color) );
    ADJ_RET( &ret, HLine(posX - radius, posX + radius, posY, color) );

    while(x < y) {
        if(f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;

        ADJ_RET( &ret, HLine(posX - x, posX + x, posY - y, color) );
        ADJ_RET( &ret, HLine(posX - x, posX + x, posY + y, color) );
        ADJ_RET( &ret, HLine(posX - y, posX + y, posY - x, color) );
        ADJ_RET( &ret, HLine(posX - y, posX + y, posY + x, color) );
    }
    return ret;
}

bool gbDraw2D::Ellipse(int posX, int posY, int radiusX, int radiusY, GBCOLOR color) 
{
    int rx_sq = 2*radiusX*radiusX;
    int ry_sq = 2*radiusY*radiusY;
    int dX = radiusY * radiusY * (1 - 2*radiusX);
    int dY = radiusX * radiusX;
    int x = radiusX;
    int y = 0;
    int x_error = ry_sq * radiusX;
    int y_error = 0;
    int b_error = 0;
    bool ret = true;
    //left/right:
    while( x_error >= y_error) {
        ADJ_RET( &ret, PutPixel(posX+x, posY+y, color) );
        ADJ_RET( &ret, PutPixel(posX-x, posY+y, color) );
        ADJ_RET( &ret, PutPixel(posX-x, posY-y, color) );
        ADJ_RET( &ret, PutPixel(posX+x, posY-y, color) );
        y++;
        y_error += rx_sq;
        b_error += dY;
        dY += rx_sq;
        if((2*b_error + dX) > 0) {
            x--;
            x_error -= ry_sq;
            b_error += dX;
            dX += ry_sq;
        }
    }

    dX = radiusY * radiusY;
    dY = radiusX * radiusX * (1 - 2*radiusY);
    x = 0;
    y = radiusY;
    x_error = 0;
    y_error = rx_sq * radiusY;        
    b_error = 0;
    //top/bottom:
    while( x_error <= y_error) {
        ADJ_RET( &ret, PutPixel(posX+x, posY+y, color) );
        ADJ_RET( &ret, PutPixel(posX-x, posY+y, color) );
        ADJ_RET( &ret, PutPixel(posX-x, posY-y, color) );
        ADJ_RET( &ret, PutPixel(posX+x, posY-y, color) );
        x++;
        x_error += ry_sq;
        b_error += dX;
        dX += ry_sq;
        if((2*b_error + dY) > 0) {
            y--;
            y_error -= rx_sq;
            b_error += dY;
            dY += rx_sq;
        }
    }
    return ret;
}

bool gbDraw2D::FilledEllipse(int posX, int posY, int radiusX, int radiusY, GBCOLOR color)
{
    int rx_sq = 2*radiusX*radiusX;
    int ry_sq = 2*radiusY*radiusY;
    int dX = radiusY * radiusY * (1 - 2*radiusX);
    int dY = radiusX * radiusX;
    int x = radiusX;
    int y = 0;
    int x_error = ry_sq * radiusX;
    int y_error = 0;
    int b_error = 0;
    bool ret = true;
    //left/right:
    while( x_error >= y_error) {
        ADJ_RET( &ret, HLine(posX-x, posX+x, posY-y, color) );
        ADJ_RET( &ret, HLine(posX-x, posX+x, posY+y, color) );
        y++;
        y_error += rx_sq;
        b_error += dY;
        dY += rx_sq;
        if((2*b_error + dX) > 0) {
            x--;
            x_error -= ry_sq;
            b_error += dX;
            dX += ry_sq;
        }
    }

    dX = radiusY * radiusY;
    dY = radiusX * radiusX * (1 - 2*radiusY);
    x = 0;
    y = radiusY;
    x_error = 0;
    y_error = rx_sq * radiusY;        
    b_error = 0;
    //top/bottom:
    while( x_error <= y_error) {
        ADJ_RET( &ret, HLine(posX-x, posX+x, posY-y, color) );
        ADJ_RET( &ret, HLine(posX-x, posX+x, posY+y, color) );
        x++;
        x_error += ry_sq;
        b_error += dX;
        dX += ry_sq;
        if((2*b_error + dY) > 0) {
            y--;
            y_error -= rx_sq;
            b_error += dY;
            dY += rx_sq;
        }
    }
    return ret;
}

bool gbDraw2D::EllipseSegment(int posX, int posY, int radiusX, int radiusY, bool ul, bool ur, bool ll, bool lr, GBCOLOR color)
{
    int rx_sq = 2*radiusX*radiusX;
    int ry_sq = 2*radiusY*radiusY;
    int dX = radiusY * radiusY * (1 - 2*radiusX);
    int dY = radiusX * radiusX;
    int x = radiusX;
    int y = 0;
    int x_error = ry_sq * radiusX;
    int y_error = 0;
    int b_error = 0;
    bool ret = true;
    //left/right:
    while( x_error >= y_error) {
        if(lr) { ADJ_RET( &ret, PutPixel(posX+x, posY+y, color) ); }
        if(ll) { ADJ_RET( &ret, PutPixel(posX-x, posY+y, color) ); }
        if(ul) { ADJ_RET( &ret, PutPixel(posX-x, posY-y, color) ); }
        if(ur) { ADJ_RET( &ret, PutPixel(posX+x, posY-y, color) ); }
        y++;
        y_error += rx_sq;
        b_error += dY;
        dY += rx_sq;
        if((2*b_error + dX) > 0) {
            x--;
            x_error -= ry_sq;
            b_error += dX;
            dX += ry_sq;
        }
    }

    dX = radiusY * radiusY;
    dY = radiusX * radiusX * (1 - 2*radiusY);
    x = 0;
    y = radiusY;
    x_error = 0;
    y_error = rx_sq * radiusY;        
    b_error = 0;
    //top/bottom:
    while( x_error <= y_error) {
        if(lr) { ADJ_RET( &ret, PutPixel(posX+x, posY+y, color) ); }
        if(ll) { ADJ_RET( &ret, PutPixel(posX-x, posY+y, color) ); }
        if(ul) { ADJ_RET( &ret, PutPixel(posX-x, posY-y, color) ); }
        if(ur) { ADJ_RET( &ret, PutPixel(posX+x, posY-y, color) ); }
        x++;
        x_error += ry_sq;
        b_error += dX;
        dX += ry_sq;
        if((2*b_error + dY) > 0) {
            y--;
            y_error -= rx_sq;
            b_error += dY;
            dY += rx_sq;
        }
    }
    return ret;
}

bool gbDraw2D::Rectangle(int left, int top, int right, int bottom, GBCOLOR color)
{
    //Clipping:
    bool ret = true;
    bool draw_l, draw_r, draw_t, draw_b;
    if(left < 0)             { left = 0;             draw_l = false; ret = false; } else { draw_l = true; }
    if(top < 0)              { top = 0;              draw_t = false; ret = false; } else { draw_t = true; }
    if(right > m_width-1)    { right  = m_width-1;   draw_r = false; ret = false; } else { draw_r = true; }
    if(bottom > m_height-1)  { bottom = m_height-1;  draw_b = false; ret = false; } else { draw_b = true; }
    if((left >= right) || (top >= bottom)) { 
        return false;
    }

    //Drawing:
    GBCOLOR *offset;
    //draw top:
    if(draw_t) {
        offset = &(m_Data[left + (top*m_pitch)]);
        for(int i=left; i<=right; i++) {
            *(offset++) = color; 
        }
    }
    //draw bottom:
    if(draw_b) {
        offset = &(m_Data[left + (bottom*m_pitch)]);
        for(int i=left; i<=right; i++) {
            *(offset++) = color;
        }
    }
    //draw left:
    if(draw_l) {
        for(int i=top+1; i<bottom; i++) {
            offset = &(m_Data[left + (i*m_pitch)]);
            *offset = color;
        }
    }
    //draw right:
    if(draw_r) {
        for(int i=top+1; i<bottom; i++) {
            offset = &(m_Data[right + (i*m_pitch)]);
            *offset = color;
        }
    }
    return ret;
}

bool gbDraw2D::RoundedRectangle(int left, int top, int right, int bottom, int radiusX, int radiusY, GBCOLOR color)
{
    //Clipping:
    if((left >= right) || (top >= bottom) || (2*radiusX > right - left) || (2*radiusY > bottom - top)) { 
        return false;
    }
    //further clipping is left to the underlying functions HLine, Line and EllipseSegment

    bool ret = true;
    //sides:
    //top
    ADJ_RET( &ret, HLine(left+radiusX, right-radiusX, top, color) );
    //bottom:
    ADJ_RET( &ret, HLine(left+radiusX, right-radiusX, bottom, color) );
    //left
    ADJ_RET( &ret, VLine(left, top+radiusY, bottom-radiusY, color) );
    //right
    ADJ_RET( &ret, VLine(right, top+radiusY, bottom-radiusY, color) );
    //corners:
    //ul
    ADJ_RET( &ret, EllipseSegment(left+radiusX, top+radiusY, radiusX, radiusY, true, false, false, false, color) );
    //ur
    ADJ_RET( &ret, EllipseSegment(right-radiusX, top+radiusY, radiusX, radiusY, false, true, false, false, color) );
    //ll
    ADJ_RET( &ret, EllipseSegment(left+radiusX, bottom-radiusY, radiusX, radiusY, false, false, true, false, color) );
    //lr
    ADJ_RET( &ret, EllipseSegment(right-radiusX, bottom-radiusY, radiusX, radiusY, false, false, false, true, color) );
    return ret;
}

bool gbDraw2D::RectangleGradient(int left, int top, int right, int bottom, 
                             GBCOLOR ul, GBCOLOR ur, GBCOLOR ll, GBCOLOR lr)
{
    //Clipping is left to the called drawing functions HLineGradient() and PutPixel()

    //Color interpolation requires bilinear interpolation;
    // First we calculate the gradients for the left and right borders:
    int r_tl = gbColor::GetR(ul);    //top left
    int g_tl = gbColor::GetG(ul);
    int b_tl = gbColor::GetB(ul);
    int a_tl = gbColor::GetA(ul);
    int r_bl = gbColor::GetR(ll);    //bottom left
    int g_bl = gbColor::GetG(ll);
    int b_bl = gbColor::GetB(ll);
    int a_bl = gbColor::GetA(ll);
    int r_tr = gbColor::GetR(ur);    //top right
    int g_tr = gbColor::GetG(ur);
    int b_tr = gbColor::GetB(ur);
    int a_tr = gbColor::GetA(ur);
    int r_br = gbColor::GetR(lr);    //bottom right
    int g_br = gbColor::GetG(lr);
    int b_br = gbColor::GetB(lr);
    int a_br = gbColor::GetA(lr);

    int dX = (bottom - top);
    int gr_l, gg_l, gb_l, ga_l;            //color gradients left side
    gr_l = ((r_bl - r_tl)<<16)/dX;
    gg_l = ((g_bl - g_tl)<<16)/dX;
    gb_l = ((b_bl - b_tl)<<16)/dX;
    ga_l = ((a_bl - a_tl)<<16)/dX;
    int gr_r, gg_r, gb_r, ga_r;            //color gradients right side
    gr_r = ((r_br - r_tr)<<16)/dX;
    gg_r = ((g_br - g_tr)<<16)/dX;
    gb_r = ((b_br - b_tr)<<16)/dX;
    ga_r = ((a_br - a_tr)<<16)/dX;

    //Drawing:
    bool ret = true;
    GBCOLOR c_l, c_r;
    //top:
    c_l = gbColor::ARGB(a_tl, r_tl, g_tl, b_tl);
    c_r = gbColor::ARGB(a_tr, r_tr, g_tr, b_tr);
    ADJ_RET( &ret, HLineGradient(left, right, top, c_l, c_r) );
    //bottom:
    c_l = gbColor::ARGB(a_bl, r_bl, g_bl, b_bl);
    c_r = gbColor::ARGB(a_br, r_br, g_br, b_br);
    ADJ_RET( &ret, HLineGradient(left, right, bottom, c_l, c_r) );

    for(int i=0; i<=dX; i++) {
        c_l = gbColor::ARGB( (a_tl + ((ga_l * i)>>16) ),
                             (r_tl + ((gr_l * i)>>16) ),
                             (g_tl + ((gg_l * i)>>16) ),
                             (b_tl + ((gb_l * i)>>16) ) );
        c_r = gbColor::ARGB( (a_tr + ((ga_r * i)>>16) ),
                             (r_tr + ((gr_r * i)>>16) ),
                             (g_tr + ((gg_r * i)>>16) ),
                             (b_tr + ((gb_r * i)>>16) ) );
        ADJ_RET( &ret, PutPixel(left, top+i, c_l) );
        ADJ_RET( &ret, PutPixel(right, top+i, c_r) );
    }
    return ret;
}

bool gbDraw2D::Box(int left, int top, int right, int bottom, GBCOLOR color)
{
    //Clipping:
    bool ret = true;
    if(left < 0) { left = 0;  ret = false; }
    if(top < 0)     { top = 0;   ret = false; }
    if(right > m_width-1)   { right = m_width-1;   ret = false; }
    if(bottom > m_height-1) { bottom = m_height-1; ret = false; }
    //Drawing:
    for(int i=top; i<=bottom; i++) {
        GBCOLOR* offset = &(m_Data[left + (i*m_pitch)]);
        for(int j=left; j<=right; j++) {
            *(offset++) =  color;
        }
    }
    return ret;
}

bool gbDraw2D::RoundedBox(int left, int top, int right, int bottom, int radiusX, int radiusY, GBCOLOR color) 
{
    //Clipping:
    if((left >= right) || (top >= bottom) || (2*radiusX > right - left) || (2*radiusY > bottom - top)) { 
        return false;
    }
    //further clipping is left to the underlying functions HLine, Line and EllipseSegment

    bool ret = true;
    //straight part:
    for(int i=top+radiusY; i<bottom-radiusY; ++i) {
        ADJ_RET( &ret, HLine(left, right, i, color) );
    }
    //round part:
    int rx_sq = 2*radiusX*radiusX;
    int ry_sq = 2*radiusY*radiusY;
    int dX = radiusY * radiusY * (1 - 2*radiusX);
    int dY = radiusX * radiusX;
    int x = radiusX;
    int y = 0;
    int x_error = ry_sq * radiusX;
    int y_error = 0;
    int b_error = 0;
    //left/right:
    while( x_error >= y_error) {
        ADJ_RET( &ret, HLine(left+radiusX-x, right-radiusX+x, top+radiusY-y, color) );
        ADJ_RET( &ret, HLine(left+radiusX-x, right-radiusX+x, bottom-radiusY+y, color) );
        y++;
        y_error += rx_sq;
        b_error += dY;
        dY += rx_sq;
        if((2*b_error + dX) > 0) {
            x--;
            x_error -= ry_sq;
            b_error += dX;
            dX += ry_sq;
        }
    }
    dX = radiusY * radiusY;
    dY = radiusX * radiusX * (1 - 2*radiusY);
    x = 0;
    y = radiusY;
    x_error = 0;
    y_error = rx_sq * radiusY;        
    b_error = 0;
    //top/bottom:
    while( x_error <= y_error) {
        ADJ_RET( &ret, HLine(left+radiusX-x, right-radiusX+x, top+radiusY-y, color) );
        ADJ_RET( &ret, HLine(left+radiusX-x, right-radiusX+x, bottom-radiusY+y, color) );
        x++;
        x_error += ry_sq;
        b_error += dX;
        dX += ry_sq;
        if((2*b_error + dY) > 0) {
            y--;
            y_error -= rx_sq;
            b_error += dY;
            dY += rx_sq;
        }
    }
    return ret;
}

bool gbDraw2D::BoxGradient(int left, int top, int right, int bottom, 
                        GBCOLOR ul, GBCOLOR ur, GBCOLOR ll, GBCOLOR lr)
{
    //Clipping is left to the called drawing function HLineGradient()

    //Color interpolation requires bilinear interpolation;
    // First we calculate the gradients for the left and right borders:
    int r_tl = gbColor::GetR(ul);    //top left
    int g_tl = gbColor::GetG(ul);
    int b_tl = gbColor::GetB(ul);
    int a_tl = gbColor::GetA(ul);
    int r_bl = gbColor::GetR(ll);    //bottom left
    int g_bl = gbColor::GetG(ll);
    int b_bl = gbColor::GetB(ll);
    int a_bl = gbColor::GetA(ll);
    int r_tr = gbColor::GetR(ur);    //top right
    int g_tr = gbColor::GetG(ur);
    int b_tr = gbColor::GetB(ur);
    int a_tr = gbColor::GetA(ur);
    int r_br = gbColor::GetR(lr);    //bottom right
    int g_br = gbColor::GetG(lr);
    int b_br = gbColor::GetB(lr);
    int a_br = gbColor::GetA(lr);

    int dX = (bottom - top);
    int gr_l, gg_l, gb_l, ga_l;            //color gradients left side
    gr_l = ((r_bl - r_tl)<<16)/dX;
    gg_l = ((g_bl - g_tl)<<16)/dX;
    gb_l = ((b_bl - b_tl)<<16)/dX;
    ga_l = ((a_bl - a_tl)<<16)/dX;
    int gr_r, gg_r, gb_r, ga_r;            //color gradients right side
    gr_r = ((r_br - r_tr)<<16)/dX;
    gg_r = ((g_br - g_tr)<<16)/dX;
    gb_r = ((b_br - b_tr)<<16)/dX;
    ga_r = ((a_br - a_tr)<<16)/dX;

    //the box is drawn from top to bottom:
    bool ret = true;
    GBCOLOR c_l, c_r;
    for(int i=0; i<=dX; i++) {
        c_l = gbColor::ARGB( (a_tl + ((ga_l * i)>>16) ),
                             (r_tl + ((gr_l * i)>>16) ),
                             (g_tl + ((gg_l * i)>>16) ),
                             (b_tl + ((gb_l * i)>>16) ) );
        c_r = gbColor::ARGB( (a_tr + ((ga_r * i)>>16) ),
                             (r_tr + ((gr_r * i)>>16) ),
                             (g_tr + ((gg_r * i)>>16) ),
                             (b_tr + ((gb_r * i)>>16) ) );
        //second interpolation step as well as clipping is left to HLineGradient():
        ADJ_RET( &ret, HLineGradient(left, right, top+i, c_l, c_r) );
    }
    return ret;
}
    

bool gbDraw2D::ApplyStencil(int x, int y, GBCOLOR const* stencil, int stencil_width, int stencil_height, int stencil_pitch)
{
    //Clipping:
    bool ret = true;
    if(x<0) { stencil_width  += x;  x = 0;  ret = false; }
    if(y<0) { stencil_height += y;  y = 0;  ret = false; }
    if( (x>m_width) || (y>m_height) || 
        (stencil_width <= 0) || (stencil_height <= 0) ) { return false; }
    if(x+stencil_width  > m_width)  { stencil_width  = m_width-x;   ret = false; }
    if(y+stencil_height > m_height) { stencil_height = m_height-y;  ret = false; }
        
    //Stenciling:
    GBCOLOR_COMPONENT zero = 0;
    GBCOLOR mask = gbColor::ARGB(0, 255, 255, 255);
    for(int iy=0; iy<stencil_height; ++iy) {
        GBCOLOR* offset = &(m_Data[x + (iy*m_pitch)]);
        for(int ix=0; ix<stencil_width; ++ix) {
            *offset &= mask;
            *offset |= gbColor::ARGB( gbColor::GetA(stencil[iy*stencil_pitch + ix]),
                                        zero, zero, zero );
            offset++;
        }
    }
    return ret;
}

}

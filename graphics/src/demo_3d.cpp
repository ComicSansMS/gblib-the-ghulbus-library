
#include <gbGraphics/graphics.hpp>
#include <gbGraphics/vertex_data.hpp>
#include <gbGraphics/uniform_data.hpp>
#include <gbGraphics/input_camera_spherical.hpp>
#include <gbGraphics/index_data.hpp>
#include <gbGraphics/mesh_loader_obj.hpp>
#include <gbGraphics/mesh_primitives.hpp>
#include <gbGraphics/vertex_data_op.hpp>
#include <gbMath/matrix.hpp>
#include <gbMath/matrix_op.hpp>
#include <gbMath/camera.hpp>
#include <gbMath/projection.hpp>
#include <gbMath/aabb.hpp>
#include <gbUtil/timer.hpp>
#include <diagnostic/diagnostic.hpp>

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iterator>
#include <string>
#include <vector>

typedef GhulbusGraphics::gbVertexFormat<
        GhulbusGraphics::VertexComponent::Position4f,
        GhulbusGraphics::VertexComponent::Normal3f,
        GhulbusGraphics::VertexComponent::TexCoord2f,
        GhulbusGraphics::VertexComponent::Color3f
    > VertexFormatType;
typedef GhulbusGraphics::gbVertexData<VertexFormatType> VertexDataType;

class KeyboardHandler_T: public GhulbusGraphics::Events::KeyboardInput {
private:
    GhulbusGraphics::gbDrawingContext3D* m_dc3d;
    GhulbusGraphics::gbRasterizerStatePtr m_RasterFilled;
    GhulbusGraphics::gbRasterizerStatePtr m_RasterWireframe;
public:
    bool DrawBounding;
public:
    KeyboardHandler_T(GhulbusGraphics::gbDrawingContext3D& dc3d)
        :m_dc3d(&dc3d), m_RasterFilled(dc3d.GetCurrentRasterizerState()), DrawBounding(false)
    {
        GhulbusGraphics::Rasterizer::StateDesc wireframe_desc = m_RasterFilled->GetDescriptor();
        wireframe_desc.FillMode = GhulbusGraphics::Rasterizer::FILL_WIREFRAME;
        m_RasterWireframe = m_dc3d->CreateRasterizerState(wireframe_desc);
    }
    bool Handler(GhulbusGraphics::Events::Key k, GhulbusGraphics::Events::gbEvent_Type_Keyboard type) {
        using namespace GhulbusGraphics;
        using namespace GhulbusGraphics::Events;
        switch(type) {
        case GBEVENT_KEYB_KEYDOWN:
            switch(k) {
            case GBKEY_W:
                {
                    // toggle wireframe
                    auto const& conf = m_dc3d->GetCurrentRasterizerState()->GetDescriptor();
                    if(conf.FillMode == Rasterizer::FILL_FILLED) {
                        m_dc3d->BindRasterizerState(m_RasterWireframe);
                    } else {
                        m_dc3d->BindRasterizerState(m_RasterFilled);
                    }
                    break;
                }
            case GBKEY_B:
                //toggle bounding box
                DrawBounding ^= true;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
        return false;
    }
};

void PrintDebugInfoRenderer(GhulbusGraphics::gbDrawingContext3DPtr& dc3d)
{
    using namespace GhulbusGraphics;
    auto renderer_info = dc3d->GetRendererInfo();
    DIAG_LOG(INFO, "Renderer " << renderer_info.Name);
    DIAG_LOG(INFO, "Vendor " << renderer_info.Vendor);
    DIAG_LOG(INFO, "Version " << renderer_info.VersionNumber << " (" << renderer_info.Version << ")");
    DIAG_LOG(INFO, "GLSL Ver " << renderer_info.ShadingLanguageVersionNumber << " (" << renderer_info.ShadingLanguageVersion << ")");
    auto extensions = dynamic_cast<gbDrawingContext3D_OGL&>(*dc3d).GetOGLExtensionList_OGL();
    std::for_each(begin(extensions), end(extensions), [](std::string const& str) {
        DIAG_LOG(INFO, " - " << str);
    });
}

void PrintDebugInfoShader(GhulbusGraphics::gbProgramPtr& program)
{
    DIAG_LOG(INFO, "Vertex Attributes:");
    for(int i=0; i<program->NumberOfVertexAttributes(); ++i) {
        auto const& attr_info = program->GetVertexAttributeInfo(i);
        DIAG_LOG(INFO, " #" << attr_info.LocationIndex << " is \'" << attr_info.Name << "\'");
    }
    DIAG_LOG(INFO, "Uniforms:");
    for(int i=0; i<program->NumberOfUniforms(); ++i) {
        auto const& uni_info = program->GetUniformInfo(i);
        DIAG_LOG(INFO, " #" << uni_info.LocationIndex << " is \'" << uni_info.Name << "\'");
    }
}

int main(int /* argc */, char* /* argv */[])
{
    using namespace GhulbusGraphics;
    using gbMath::Get;
    auto dev_list = gbWindow::EnumerateDevices();
    DIAG_LOG(INFO, "Display devices:");
    std::for_each(begin(dev_list), end(dev_list), [](gbWindow::DeviceIdentifier const& dev_id) {
        DIAG_LOG(INFO, " * " << dev_id.Name);
    });
    gbWindowPtr main_window = gbWindow::Create(800, 800);
    main_window->SetWindowTitle("OpenGL Demo");
    auto dc3d = main_window->CreateDrawingContext3D();

    PrintDebugInfoRenderer(dc3d);

    KeyboardHandler_T keyb_handler(*dc3d);
    main_window->GetEventManager()->AddEventHandler_KeyboardInput(&keyb_handler, Events::GBEVENT_KEYB_KEYDOWN);

    gbShaderPtr vtx_shader;
    try {
        vtx_shader = dc3d->CreateShaderFromFile(ShaderType::VERTEX_SHADER, "../../graphics/test/gfx/basic.vert");
    } catch(Exceptions::ShaderCompileError& e) {
        DIAG_LOG(ERROR, e);
    }
    if(!vtx_shader->GetShaderCompileMessage().empty()) {
        DIAG_LOG(WARNING, vtx_shader->GetShaderCompileMessage());
    }
    gbShaderPtr frag_shader;
    try {
        frag_shader = dc3d->CreateShaderFromFile(ShaderType::PIXEL_SHADER, "../../graphics/test/gfx/basic.frag");
    } catch(Exceptions::ShaderCompileError& e) {
        DIAG_LOG(ERROR, e);
    }
    if(!frag_shader->GetShaderCompileMessage().empty()) {
        DIAG_LOG(WARNING, frag_shader->GetShaderCompileMessage());
    }

    gbProgramPtr program = dc3d->CreateProgram();
    program->BindShader(vtx_shader);
    program->BindShader(frag_shader);

    try {
        program->Finalize();
    } catch(Exceptions::ShaderCompileError& e) {
        DIAG_LOG(ERROR, Diagnostic::GetExceptionInfo<Diagnostic::Exception_Info::description>(e)->c_str() << ":\n "
                        << "####" << e.what() << "####");
    }
    PrintDebugInfoShader(program);

    Mesh::Box<VertexFormatType> cube(0.5f);
    gbVertexBinding binding(program, cube.VertexData.GetVertexFormat());
    Bind<VertexComponent::Position4f, VertexFormatType>(binding, "VertexPosition");
    Bind<VertexComponent::Color3f, VertexFormatType>(binding, "VertexColor");
    Bind<VertexComponent::Normal3f, VertexFormatType>(binding, "VertexNormal");
    //Bind<VertexComponent::TexCoord2f, decltype(vertex_data)>(binding, "TexCoord");
    gbMesh cube_mesh = cube.CreateMesh(*dc3d, binding);

    DIAG_LOG(INFO, "Parsing OBJ...");
    gbMeshLoader_Obj<VertexFormatType, uint32_t> mesh_loader("../../graphics/test/meshes/teapot.obj");
    DIAG_LOG(INFO, " Vertices: " << mesh_loader.VertexData.NumberOfVertices() <<
                   "  Faces: " << mesh_loader.IndexData.NumberOfPrimitives() <<
                   " TexCoords: " << (mesh_loader.HasTextureCoordinates()?"yes":"no") <<
                   " Normals: " << (mesh_loader.HasVertexNormals()?"yes":"no"));
    DIAG_LOG(INFO, " done.");
    for(int i=0; i<mesh_loader.VertexData.NumberOfVertices(); ++i) {
        VertexData::Get<VertexComponent::Color3f>(mesh_loader.VertexData, i).r = 1.0f;
        VertexData::Get<VertexComponent::Color3f>(mesh_loader.VertexData, i).g = 1.0f;
        VertexData::Get<VertexComponent::Color3f>(mesh_loader.VertexData, i).b = 1.0f;
    }
    if(!mesh_loader.HasVertexNormals()) {
        DIAG_LOG(INFO, "Calculating per-vertex normals...");
        VertexDataOperations::GeneratePerVertexNormals(mesh_loader.VertexData, mesh_loader.IndexData);
        DIAG_LOG(INFO, " done.");
    }
    DIAG_LOG(INFO, "Building mesh...");
    gbMesh bunny_mesh = mesh_loader.CreateMesh(*dc3d, binding);
    for(int i=0; i<mesh_loader.VertexData.NumberOfVertices(); ++i) {
        gbMath::Vector4f v4(&VertexData::Get<VertexComponent::Position4f>(mesh_loader.VertexData, i).x);
        gbMath::Vector3f v3(v4.x_, v4.y_, v4.z_);
        DIAG_ASSERT(bunny_mesh.GetBoundingBox().Inside(v3));
    }
    gbMesh bound_mesh = Mesh::Box<VertexFormatType>(bunny_mesh.GetBoundingBox()).CreateMesh(*dc3d, binding);
    DIAG_LOG(INFO, " done.");
    DIAG_LOG(INFO, "Bounding " << bunny_mesh.GetBoundingBox().GetMin() << " - " << bunny_mesh.GetBoundingBox().GetMax());

    float const fov = 0.75f * gbMath::Constf::PI_1_2;
    float const aspect_ratio = static_cast<float>(main_window->GetWidth()) / static_cast<float>(main_window->GetHeight());
    float const z_near = 0.01f;
    float const z_far = 50.0f;
    gbMath::Matrix4f mat_project = gbMath::ProjectionPerspectiveFov(fov, aspect_ratio, z_near, z_far);
    program->BindUniform("uProject", mat_project);
    gbMath::Matrix4f mat_model = gbMath::ExpandToMatrix4( gbMath::Matrix::Scale3D(2.0f/std::sqrt(bunny_mesh.GetBoundingBox().GetDiagonalSquared())) );
    auto mat_trans = gbMath::Matrix::Translate4D(-bunny_mesh.GetBoundingBox().GetHalfAxes());
    program->BindUniform("uModel", mat_model * mat_trans);

    program->BindUniform("Kd", UniformData::Float3(0.8f, 0.8f, 0.8f));
    program->BindUniform("Ld", UniformData::Float3(gbColor::XRGB(255, 128, 0)));
    program->BindUniform("LightPosition", UniformData::Float4(0.0f, 10.0f, 0.0f, 1.0f));

    DIAG_LOG(INFO, "Main loop");
    unsigned int color_count = 1;
    dc3d->SetClearColor(gbColor::ARGB(255, 255, 0, 0));
    GhulbusUtil::gbTimer timer;
    Input::CameraSpherical camera(*main_window);
    auto event_man = main_window->GetEventManager();
    auto event_proc = main_window->GetEventProcessor();
    while(!event_proc->IsDone()) {
        event_proc->ProcessMessages();
        if((color_count != 0) && (event_man->GetTimestamp() > color_count*2000)) {
            switch(color_count++) {
            case 1:
                dc3d->SetClearColor(gbColor::XRGB(0, 255, 0));
                break;
            case 2:
                dc3d->SetClearColor(gbColor::XRGB(0, 0, 255));
                break;
            case 3:
                color_count = 0;
                dc3d->SetClearColor(gbColor::XRGB(128, 128, 128));
                break;
            }
        }
        timer.Mark();
        program->BindUniform("uView", camera.GetCameraMatrix());
        program->BindUniform("uNormal", camera.GetCameraMatrix());
        dc3d->Clear();
        //cube_mesh.Render();
        bunny_mesh.Render();
        if(keyb_handler.DrawBounding) { bound_mesh.Render(); }
        dc3d->Present();
    }

    return 0;
}


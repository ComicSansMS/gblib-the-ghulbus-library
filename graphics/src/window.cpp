/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/window.hpp>
#include <gbGraphics/color.hpp>
#include <diagnostic/diagnostic.hpp>

namespace GB_GRAPHICS_NAMESPACE {
#ifdef GB_GRAPHICS_HAS_SDL
    static std::shared_ptr<gbWindow_SDL::SDL_WindowEnvironment> g_SDLEnv;
#endif

gbWindowPtr gbWindow::Create(int width, int height, WINDOW_API_TYPE win_api_type)
{
    return Create(width, height, Window::WindowDesc(), win_api_type);
}

gbWindowPtr gbWindow::Create(int width, int height, Window::WindowDesc const& window_descriptor, WINDOW_API_TYPE win_api_type)
{
    switch(win_api_type) {
    case WIN_API_DEFAULT:
    case WIN_API_SDL:
#        ifdef GB_GRAPHICS_HAS_SDL
        if(!g_SDLEnv) {
            // can not use make_shared here because constructor is private
            g_SDLEnv = std::shared_ptr<gbWindow_SDL::SDL_WindowEnvironment>(new gbWindow_SDL::SDL_WindowEnvironment);
        }
        gbColor::SetActiveColorScheme(gbColor::COLOR_SCHEME_SDL);
        return std::make_shared<gbWindow_SDL>(g_SDLEnv, width, height, window_descriptor);
#        else
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Library was compiled without SDL support");
#        endif
        break;
    case WIN_API_WIN32:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Win32 API not implemented");
    default:
        DIAG_ASSERT(false);
    }
    DIAG_ASSERT(false);
    return nullptr;
}

std::vector<gbWindow::DeviceIdentifier> gbWindow::EnumerateDevices(WINDOW_API_TYPE win_api_type)
{
    switch(win_api_type) {
    case WIN_API_DEFAULT:
    case WIN_API_SDL:
#        ifdef GB_GRAPHICS_HAS_SDL
        if(!g_SDLEnv) {
            // can not use make_shared here because constructor is private
            g_SDLEnv = std::shared_ptr<gbWindow_SDL::SDL_WindowEnvironment>(new gbWindow_SDL::SDL_WindowEnvironment);
        }
        return g_SDLEnv->EnumerateDevices();
#        else
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Library was compiled without SDL support");
#        endif
        break;
    case WIN_API_WIN32:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Win32 API not implemented");
    default:
        DIAG_ASSERT(false);
    }
    DIAG_ASSERT(false);
    return std::vector<DeviceIdentifier>();
}

}


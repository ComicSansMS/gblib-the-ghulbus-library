/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/index_buffer.hpp>
#include <gbGraphics/diagnostic_OGL.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/shadow_state_OGL.hpp>

#include <diagnostic/diagnostic.hpp>

namespace GB_GRAPHICS_NAMESPACE {

gbIndexBuffer_OGL::gbIndexBuffer_OGL(GLuint buffer_id, int raw_size, gbShadowState_OGL* shadow_state)
    :m_BufferId(buffer_id), m_Size(raw_size), m_glShadowState(shadow_state), m_NumberIndices(0),
     m_IndexType(gbIndexDataBase::INDEX_TYPE_NONE), m_PrimitiveType(gbIndexDataBase::PRIMITIVE_NONE)
{
}

gbIndexBuffer_OGL::~gbIndexBuffer_OGL()
{
    ErrorMonitor_OGL monitor;
    m_glShadowState->ReleaseBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferId);
    glDeleteBuffers(1, &m_BufferId);
}

int gbIndexBuffer_OGL::GetRawSize() const
{
    return m_Size;
}

void gbIndexBuffer_OGL::SetIndexData(gbIndexDataBase const& index_data)
{
    ErrorMonitor_OGL monitor;
    if(index_data.GetRawSize() > m_Size) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Buffer too small for supplied index data" );
    }
    m_glShadowState->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_data.GetRawSize(), index_data.GetRawData(), GL_STATIC_DRAW);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
            "Unable to fill index buffer" );
    }
    m_NumberIndices = index_data.NumberOfIndices();
    m_IndexType = index_data.GetIndexType();
    m_PrimitiveType = index_data.GetPrimitiveType();
}

int gbIndexBuffer_OGL::NumberOfIndices() const
{
    return m_NumberIndices;
}

gbIndexDataBase::IndexType_T gbIndexBuffer_OGL::GetIndexType() const
{
    return m_IndexType;
}

gbIndexDataBase::Primitive_T gbIndexBuffer_OGL::GetPrimitiveType() const
{
    return m_PrimitiveType;
}

GLuint gbIndexBuffer_OGL::GetOGLIndexBufferId_OGL() const
{
    return m_BufferId;
}

}

#include <gbGraphics/graphics_2d.hpp>

#include <gbGraphics/blend_state.hpp>
#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/mesh.hpp>
#include <gbGraphics/mesh_primitives.hpp>
#include <gbGraphics/program.hpp>
#include <gbGraphics/renderable_2d.hpp>
#include <gbGraphics/sampler_state.hpp>
#include <gbGraphics/shader.hpp>
#include <gbGraphics/sprite.hpp>
#include <gbGraphics/texture2d.hpp>
#include <gbGraphics/vertex_binding.hpp>
#include <gbGraphics/vertex_format.hpp>
#include <gbGraphics/window.hpp>

#include <gbUtil/missing_features.hpp>

#include <diagnostic/diagnostic.hpp>

namespace {

    const std::string DRAW2D_VERTEX_SHADER = "\n\
        #version 150\n\
        \n\
        in vec3 VertexPosition;\n\
        in vec2 VertexTexCoord;\n\
        \n\
        out vec2 TexCoord;\n\
        \n\
        uniform mat4 uModelView;\n\
        uniform mat3 uTexCoordTransform;\n\
        \n\
        void main()\n\
        {\n\
            TexCoord = vec2(uTexCoordTransform * vec3(VertexTexCoord, 1.0));\n\
            gl_Position = uModelView * vec4(VertexPosition, 1.0);\n\
        }\n\
    ";

    const std::string DRAW2D_PIXEL_SHADER = "\n\
        #version 150\n\
        \n\
        in vec2 TexCoord;\n\
        \n\
        out vec4 FragColor;\n\
        \n\
        uniform sampler2D uTexture;\n\
        \n\
        void main()\n\
        {\n\
            FragColor = texture(uTexture, TexCoord);\n\
        }\n\
    ";
    
    typedef GB_GRAPHICS_NAMESPACE::gbVertexFormat<
        GB_GRAPHICS_NAMESPACE::VertexComponent::Position3f,
        GB_GRAPHICS_NAMESPACE::VertexComponent::TexCoord2f
    > Draw2D_VertexFormat;

    GB_GRAPHICS_NAMESPACE::Mesh::Quad<Draw2D_VertexFormat> g_QuadFactory(1.0f, 1.0f);
}

namespace GB_GRAPHICS_NAMESPACE {

gb2D::gb2D(gbDrawingContext3DPtr dc3d)
    :m_dc3d(dc3d), m_Width(dc3d->GetParentWindow()->GetWidth()), m_Height(dc3d->GetParentWindow()->GetHeight()),
     m_Overlay(nullptr)
{
    auto vtx_shader = m_dc3d->CreateShader(ShaderType::VERTEX_SHADER, DRAW2D_VERTEX_SHADER);
    auto pix_shader = m_dc3d->CreateShader(ShaderType::PIXEL_SHADER, DRAW2D_PIXEL_SHADER);
    m_Program = m_dc3d->CreateProgram();
    m_Program->BindShader(vtx_shader);
    m_Program->BindShader(pix_shader);
    m_Program->Finalize();

    m_Binding = std::make_unique<gbVertexBinding>(m_Program, Draw2D_VertexFormat());
    Bind<VertexComponent::Position3f, Draw2D_VertexFormat>(*m_Binding, "VertexPosition");
    Bind<VertexComponent::TexCoord2f, Draw2D_VertexFormat>(*m_Binding, "VertexTexCoord");

    Blending::StateDesc blend_state_desc( Blending::OPERAND_SRC_ALPHA,
                                          Blending::OPERAND_INV_SRC_ALPHA,
                                          Blending::OPERATION_ADD );
    m_BlendState = m_dc3d->CreateBlendState(blend_state_desc);

    Sampling::StateDesc sampler_state_desc( Sampling::SAMPLE_POINT,
                                            Sampling::SAMPLE_POINT,
                                            Sampling::SAMPLE_POINT,
                                            Sampling::ADDRESS_MODE_BORDER,
                                            Sampling::ADDRESS_MODE_BORDER,
                                            Sampling::ADDRESS_MODE_BORDER,
                                            gbColor::XRGB(255, 0, 255) );
    m_SamplerState = m_dc3d->CreateSamplerState(sampler_state_desc);

    m_Overlay = CreateSprite(m_Width, m_Height);
    {
        // lock overlay once to clear it
        SurfaceLock<gbSprite> lock(m_Overlay);
    }
    UnregisterFromDrawing(m_Overlay.get());
}

gb2D::~gb2D()
{
    m_Overlay.reset();
    std::vector<gbSprite*> renderables = m_Sprites.GetResources();
    std::for_each(renderables.begin(), renderables.end(), [](Renderable2D const* ptr) {
        DIAG_LOG(WARNING, "Leaked 2d renderable at " << ptr);
    });
}

void gb2D::Draw()
{
    DrawWithOptions(DRAW_OPTIONS_DEFAULT);
}

void gb2D::DrawWithOptions(DrawingOptions_T opt)
{
    if(opt != DRAW_OPTIONS_USE_CURRENT_BLENDSTATE) {
        SetBlendState();
    }
    m_dc3d->SetDepthTest(false);
    std::for_each(m_DrawingQueue.begin(), m_DrawingQueue.end(), [](DrawingQueueEntry& qel) {
        auto& obj = qel.first;
        auto const& callback_fun = qel.second;
        if(!callback_fun || (callback_fun(obj))) {
            if(obj->IsShown()) {
                obj->Render();
            }
        }
    });
    m_Overlay->Render();
    m_dc3d->SetDepthTest(true);
}

void gb2D::SetBlendState()
{
    m_dc3d->BindBlendState(m_BlendState);
}

gbSpritePtr gb2D::CreateSprite(int width, int height)
{
    gbSpritePtr ret = m_Sprites.CreateResource( m_Sprites,
                                                *this,
                                                *m_Program,
                                                g_QuadFactory.CreateMesh(*m_dc3d, *m_Binding),
                                                m_dc3d->CreateTexture(Texture::TextureDesc2D(width, height), m_SamplerState) );
    m_DrawingQueue.push_back(DrawingQueueEntry(ret.get(), RenderCallbackFunc()));
    return ret;
}

gbSpritePtr gb2D::CreateSprite(ReadableSurface& surface)
{
    gbSpritePtr ret = CreateSprite(surface.GetWidth(), surface.GetHeight());
    CopySurface(*ret, surface);
    return ret;
}

gbSprite* gb2D::GetOverlay()
{
    return m_Overlay.get();
}

void gb2D::RegisterForDrawing(Renderable2D* object)
{
    m_DrawingQueue.push_back(DrawingQueueEntry(object, RenderCallbackFunc(nullptr)));
}

void gb2D::RegisterForDrawing(Renderable2D* object, RenderCallbackFunc const& callback)
{
    m_DrawingQueue.push_back(DrawingQueueEntry(object, callback));
}

void gb2D::UnregisterFromDrawing(Renderable2D* object)
{
    auto const it = std::remove_if(m_DrawingQueue.begin(), m_DrawingQueue.end(), [object](DrawingQueueEntry const& e) -> bool {
        return e.first == object;
    });
    m_DrawingQueue.erase(it, m_DrawingQueue.end());
}

gbDrawingContext3D& gb2D::GetDrawingContext() const
{
    return *m_dc3d;
}

}

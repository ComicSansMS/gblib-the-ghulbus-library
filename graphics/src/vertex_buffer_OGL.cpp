/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/vertex_buffer.hpp>
#include <gbGraphics/diagnostic_OGL.hpp>
#include <gbGraphics/geometry.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/index_buffer.hpp>
#include <gbGraphics/shadow_state_OGL.hpp>
#include <gbGraphics/vertex_data_base.hpp>

#include <diagnostic/diagnostic.hpp>

namespace GB_GRAPHICS_NAMESPACE {

gbVertexBuffer_OGL::gbVertexBuffer_OGL(GLuint buffer_id, int size, gbShadowState_OGL* shadow_state)
    :m_BufferId(buffer_id), m_Size(size), m_glShadowState(shadow_state)
{
}

gbVertexBuffer_OGL::~gbVertexBuffer_OGL()
{
    m_glShadowState->ReleaseBuffer(GL_ARRAY_BUFFER, m_BufferId);
    glDeleteBuffers(1, &m_BufferId);
}

int gbVertexBuffer_OGL::GetSize() const
{
    return m_Size;
}

void gbVertexBuffer_OGL::SetVertexData(gbVertexDataBase const& data)
{
    if(m_VertexFormat && (*m_VertexFormat != data.GetVertexFormat())) {
        auto active_geom = m_GeometryList.GetResources();
        if(!active_geom.empty()) {
            DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                  "Cannot change vertex format of buffer bound to geometries" );
        }
    }
    if(data.GetRawSize() > m_Size) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Cannot assign vertex data: buffer too small" );
    }
    ErrorMonitor_OGL monitor;
    m_glShadowState->BindBuffer(GL_ARRAY_BUFFER, m_BufferId);
    glBufferData(GL_ARRAY_BUFFER, data.GetRawSize(), data.GetRawData(), GL_STATIC_DRAW);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
                              "Unable to fill vertex buffer" );
    }
    m_VertexFormat = data.GetVertexFormat().Clone();
}

gbGeometryPtr gbVertexBuffer_OGL::CreateGeometry(gbVertexBinding const& bindings)
{
    return m_GeometryList.CreateResource(this, bindings, m_glShadowState);
}

gbGeometryPtr gbVertexBuffer_OGL::CreateGeometry(gbVertexBinding const& bindings, gbIndexBuffer& index_buffer)
{
    gbIndexBuffer_OGL* index_buffer_ogl = dynamic_cast<gbIndexBuffer_OGL*>(&index_buffer);
    return m_GeometryList.CreateResource(this, bindings, index_buffer_ogl, m_glShadowState);
}

gbVertexFormatBase const* gbVertexBuffer_OGL::GetVertexFormat() const
{
    if(!m_VertexFormat) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "No vertex data bound to buffer" );
    }
    return m_VertexFormat.get();
}

GLuint gbVertexBuffer_OGL::GetOGLVertexBufferId_OGL() const
{
    return m_BufferId;
}

}

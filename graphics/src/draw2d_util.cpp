/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/draw2d_util.hpp>
#include <gbGraphics/event.hpp>
#include <gbGraphics/font_simplemonospaced.hpp>
#include <gbGraphics/text.hpp>
#include <gbGraphics/text_renderer_texture.hpp>

namespace GB_GRAPHICS_NAMESPACE {
namespace Draw2D_Util {

void DrawMouse(LockableSurface& sprite, Events::MouseState const* mouse_state)
{
    using namespace GhulbusGraphics::Text;
    Fonts::SimpleMonospaced font;
    gbText text = Text::Create<char>(&font, sprite);

    GBCOLOR const active = gbColor::XRGB(255, 255, 255);
    GBCOLOR const inactive = gbColor::XRGB(128, 128, 128);
    text << set_text_color(active)
        << "Mouse Position\n    X: " << ((mouse_state)?(mouse_state->posx):(-1))
        << "    Y: " << ((mouse_state)?(mouse_state->posy):(-1)) << "\n";
    text << set_text_color((mouse_state && mouse_state->lbutton)?(active):(inactive)) << "    L";
    text << set_text_color((mouse_state && mouse_state->mbutton)?(active):(inactive)) << "    M";
    text << set_text_color((mouse_state && mouse_state->rbutton)?(active):(inactive)) << "    R";
    text << set_text_color(active) << "\n    MouseWheel: " << ((mouse_state)?(mouse_state->mousewheel):(0));
}


void DrawKeyboard(LockableSurface& sprite, Events::KeyboardState const* keyb_state)
{
    using namespace GhulbusGraphics::Text;
    using namespace GhulbusGraphics::Events;
    Fonts::SimpleMonospaced font;
    gbText text = Text::Create<char>(&font, sprite);

    bool const* state = ((keyb_state)?(keyb_state->keys):(nullptr));
    GBCOLOR const active = gbColor::XRGB(255, 255, 255);
    GBCOLOR const inactive = gbColor::XRGB(128, 128, 128);
    auto PrintKey = [=,&text](Key keycode, std::string const& spaces, std::string const& key_name) {
        using namespace GhulbusGraphics::Text;
        using namespace GhulbusGraphics::Events;
        GBCOLOR const color = (state && state[keycode])?(active):(inactive);
        text << set_text_color(color)
             << spaces.c_str()
             << key_name.c_str();
    };
    text << set_cursor(0, 0) << set_text_color(inactive);
    PrintKey(GBKEY_ESCAPE, " ", "Esc");
    PrintKey(GBKEY_F1, " ", "F1");
    PrintKey(GBKEY_F2, " ", "F2");
    PrintKey(GBKEY_F3, " ", "F3");
    PrintKey(GBKEY_F4, " ", "F4");
    PrintKey(GBKEY_F5, "  ", "F5");
    PrintKey(GBKEY_F6, " ", "F6");
    PrintKey(GBKEY_F7, " ", "F7");
    PrintKey(GBKEY_F8, " ", "F8");
    PrintKey(GBKEY_F9, "   ", "F9");
    PrintKey(GBKEY_F10, " ", "F10");
    PrintKey(GBKEY_F11, " ", "F11");
    PrintKey(GBKEY_F12, " ", "F12");
    PrintKey(GBKEY_PRINT, "  ", "Prt");
    PrintKey(GBKEY_SCROLLOCK, " ", "Rol");
    PrintKey(GBKEY_BREAK, " ", "Brk");
    text << "\n\n";

    PrintKey(GBKEY_BACKQUOTE, " ", "^");
    PrintKey(GBKEY_1, "  ", "1");
    PrintKey(GBKEY_2, "  ", "2");
    PrintKey(GBKEY_3, "  ", "3");
    PrintKey(GBKEY_4, "  ", "4");
    PrintKey(GBKEY_5, "  ", "5");
    PrintKey(GBKEY_6, "  ", "6");
    PrintKey(GBKEY_7, "  ", "7");
    PrintKey(GBKEY_8, "  ", "8");
    PrintKey(GBKEY_9, "  ", "9");
    PrintKey(GBKEY_0, "  ", "0");
    PrintKey(GBKEY_MINUS, "  ", "?");
    PrintKey(GBKEY_EQUALS, "  ", "=");
    PrintKey(GBKEY_BACKSPACE, "  ", "<-----");
    PrintKey(GBKEY_INSERT, "  ", "Ins");
    PrintKey(GBKEY_HOME, " ", "Hom");
    PrintKey(GBKEY_PAGEUP, " ", "PgU");
    PrintKey(GBKEY_NUMLOCK, "  ", "Num");
    PrintKey(GBKEY_NUM_DIVIDE, "  ", "/");
    PrintKey(GBKEY_NUM_MULTIPLY, "  ", "*");
    PrintKey(GBKEY_NUM_MINUS, "  ", "-");
    text << "\n";

    PrintKey(GBKEY_TAB, " ", "Tab");
    PrintKey(GBKEY_Q, "  ", "Q");
    PrintKey(GBKEY_W, "  ", "W");
    PrintKey(GBKEY_E, "  ", "E");
    PrintKey(GBKEY_R, "  ", "R");
    PrintKey(GBKEY_T, "  ", "T");
    PrintKey(GBKEY_Y, "  ", "Y");
    PrintKey(GBKEY_U, "  ", "U");
    PrintKey(GBKEY_I, "  ", "I");
    PrintKey(GBKEY_O, "  ", "O");
    PrintKey(GBKEY_P, "  ", "P");
    PrintKey(GBKEY_LEFTBRACKET, "  ", "[");
    PrintKey(GBKEY_RIGHTBRACKET, "  ", "]");
    PrintKey(GBKEY_DELETE, "        ", "Del");
    PrintKey(GBKEY_END, " ", "End");
    PrintKey(GBKEY_PAGEDOWN, " ", "PgD");
    PrintKey(GBKEY_NUM7, "    ", "7");
    PrintKey(GBKEY_NUM8, "  ", "8");
    PrintKey(GBKEY_NUM9, "  ", "9");
    PrintKey(GBKEY_NUM_PLUS, "  ", "+");
    text << "\n";

    PrintKey(GBKEY_CAPSLOCK, " ", "Caps");
    PrintKey(GBKEY_A, "  ", "A");
    PrintKey(GBKEY_S, "  ", "S");
    PrintKey(GBKEY_D, "  ", "D");
    PrintKey(GBKEY_F, "  ", "F");
    PrintKey(GBKEY_G, "  ", "G");
    PrintKey(GBKEY_H, "  ", "H");
    PrintKey(GBKEY_J, "  ", "J");
    PrintKey(GBKEY_K, "  ", "K");
    PrintKey(GBKEY_L, "  ", "L");
    PrintKey(GBKEY_SEMICOLON, "  ", ";");
    PrintKey(GBKEY_QUOTESINGLE, "  ", "\'");
    PrintKey(GBKEY_BACKSLASH, "  ", "\\");
    PrintKey(GBKEY_RETURN, "  ", "<-|");
    PrintKey(GBKEY_NUM4, "                 ", "4");
    PrintKey(GBKEY_NUM5, "  ", "5");
    PrintKey(GBKEY_NUM6, "  ", "6");
    text << "\n";

    PrintKey(GBKEY_LSHIFT, " ", "Shft");
    PrintKey(GBKEY_LESS, " ", "<");
    PrintKey(GBKEY_Z, "  ", "Z");
    PrintKey(GBKEY_X, "  ", "X");
    PrintKey(GBKEY_C, "  ", "C");
    PrintKey(GBKEY_V, "  ", "V");
    PrintKey(GBKEY_B, "  ", "B");
    PrintKey(GBKEY_N, "  ", "N");
    PrintKey(GBKEY_M, "  ", "M");
    PrintKey(GBKEY_COMMA, "  ", ",");
    PrintKey(GBKEY_PERIOD, "  ", ".");
    PrintKey(GBKEY_SLASH, "  ", "/");
    PrintKey(GBKEY_RSHIFT, "  ", "R-Shift");
    PrintKey(GBKEY_UP, "        ", "^");
    PrintKey(GBKEY_NUM1, "        ", "1");
    PrintKey(GBKEY_NUM2, "  ", "2");
    PrintKey(GBKEY_NUM3, "  ", "3");
    PrintKey(GBKEY_NUM_ENTER, "  ", "E");
    text << "\n";

    PrintKey(GBKEY_LCTRL, " ", "Ctrl");
    PrintKey(GBKEY_LWIN, "  ", "Win");
    PrintKey(GBKEY_LALT, "  ", "Alt");
    PrintKey(GBKEY_SPACE, "   ", "Space");
    PrintKey(GBKEY_RALT, "   ", "R-Alt");
    PrintKey(GBKEY_RWIN, "  ", "Win");
    PrintKey(GBKEY_MENU, "  ", "W2");
    PrintKey(GBKEY_RCTRL, "  ", "R-Ctl");
    PrintKey(GBKEY_LEFT, "   ", "<-");
    PrintKey(GBKEY_DOWN, "  ", "|");
    PrintKey(GBKEY_RIGHT, "  ", "->");
    PrintKey(GBKEY_NUM0, "    ", "0");
    PrintKey(GBKEY_NUM_PERIOD, "     ", ".");
}


}
}

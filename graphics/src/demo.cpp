#include <SDL.h>
#include <gbGraphics/graphics.hpp>
#include <gbGraphics/draw2d_util.hpp>
#include <gbGraphics/image_loader.hpp>
#include <gbGraphics/image_sampler.hpp>
#include <gbGraphics/turtle.hpp>
#include <gbUtil/timer.hpp>
#include <diagnostic/diagnostic.hpp>
#include <sstream>
#include <cmath>

void BresenhamTest(GhulbusGraphics::gbDraw2D* draw2D) {
    //Bresenham-Test:
    draw2D->Circle(100, 100, 100, GhulbusGraphics::gbColor::XRGB(150,150,150));
    draw2D->Circle(100, 100, 56, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->Circle(100, 100, 36, GhulbusGraphics::gbColor::XRGB(0,150,150));
    draw2D->Circle(30, 100, 70, GhulbusGraphics::gbColor::XRGB(200, 100, 0));
    draw2D->Ellipse(100, 100, 36, 56, GhulbusGraphics::gbColor::XRGB(255, 128, 255));

    draw2D->Line(100, 100, 100, 0, GhulbusGraphics::gbColor::XRGB(150,150,150));
    draw2D->PutPixel(100, 0, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 125, 50, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(125, 50, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 150, 75, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(150, 75, GhulbusGraphics::gbColor::XRGB(255,128,128));

    draw2D->Line(100, 100, 200, 100, GhulbusGraphics::gbColor::XRGB(150,150,150));
    draw2D->PutPixel(200, 100, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 150, 125, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(150, 125, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 125, 150, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(125, 150, GhulbusGraphics::gbColor::XRGB(255,128,128));

    draw2D->Line(100, 100, 100, 200, GhulbusGraphics::gbColor::XRGB(150,150,150));
    draw2D->PutPixel(100, 200, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 75, 150, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(75, 150, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 50, 125, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(50, 125, GhulbusGraphics::gbColor::XRGB(255,128,128));

    draw2D->Line(100, 100, 0, 100, GhulbusGraphics::gbColor::XRGB(150,150,150));
    draw2D->PutPixel(0, 100, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 50, 75, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(50, 75, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 75, 50, GhulbusGraphics::gbColor::XRGB(255,255,255));
    draw2D->PutPixel(75, 50, GhulbusGraphics::gbColor::XRGB(255,128,128));

    //Angle bisecting line:
    draw2D->Line(100, 100, 125, 125, GhulbusGraphics::gbColor::XRGB(0,150,150));
    draw2D->PutPixel(125, 125, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 75, 75, GhulbusGraphics::gbColor::XRGB(0,150,150));
    draw2D->PutPixel(75, 75, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 125, 75, GhulbusGraphics::gbColor::XRGB(0,150,150));
    draw2D->PutPixel(125, 75, GhulbusGraphics::gbColor::XRGB(255,128,128));
    draw2D->Line(100, 100, 75, 125, GhulbusGraphics::gbColor::XRGB(0,150,150));
    draw2D->PutPixel(75, 125, GhulbusGraphics::gbColor::XRGB(255,128,128));

    draw2D->PutPixel(100,100, GhulbusGraphics::gbColor::XRGB(255,128,128));    //origin
}


void Sierpinski(GhulbusGraphics::gbTurtle* turtle, int len)
{
    if(len <= 1) return;
    turtle->Go(len>>1);
    turtle->TurnLeft(120);
    Sierpinski(turtle, len>>1);
    turtle->TurnRight(120);
    turtle->Go(len>>1);
    turtle->TurnLeft(120);
    turtle->Go(len>>1);
    turtle->TurnLeft(120);
    Sierpinski(turtle, len>>1);
    turtle->TurnRight(120);
    turtle->Go(len>>1);
    turtle->TurnLeft(120);
    turtle->Go(len>>1);
    turtle->TurnLeft(120);
    Sierpinski(turtle, len>>1);
    turtle->TurnRight(120);
    turtle->Go(len>>1);
    turtle->TurnLeft(120);
}

void TurtleTest(GhulbusGraphics::gbDraw2D* draw2D) {
    GhulbusGraphics::gbTurtle turtle(*draw2D);
    turtle.Go(80);
    turtle.TurnLeft(90);
    turtle.PenUp();
    turtle.Go(80);
    turtle.TurnLeft(90);
    turtle.SetColor(255,0,0);
    turtle.PenDown();
    turtle.Go(80);
    turtle.TurnLeft(90);
    turtle.Go(80);
    turtle.SetColor(255,255,255);
    turtle.TurnLeft(135);
    turtle.Go(113);
    turtle.PenToggle();
    turtle.Goto(-390, -290);
    turtle.PenToggle();
    turtle.SetAngleRad(0.0);
    //turtle.Show();
    turtle.SetColor(80, 40, 164);
    Sierpinski(&turtle, 128);
    turtle.PenUp();
    turtle.Goto(150, 0);
    turtle.TurnLeft(30);
    turtle.Show();
    DIAG_LOG(INFO, "Turtle at " << turtle.GetAngle());
}

class InputHandler_T: public GhulbusGraphics::Events::KeyboardInput {
public:
    ~InputHandler_T()
    {
    };
    bool Handler(GhulbusGraphics::Events::Key k, GhulbusGraphics::Events::gbEvent_Type_Keyboard type)
    {
        using namespace GhulbusGraphics::Events;
        switch(type) {
        case GBEVENT_KEYB_KEYDOWN:
            switch(k) {
            case GBKEY_UP: case GBKEY_W:
            case GBKEY_DOWN: case GBKEY_S:
            case GBKEY_LEFT: case GBKEY_A:
            case GBKEY_RIGHT: case GBKEY_D:
            default:
                break;
            };
            break;
        case GBEVENT_KEYB_KEYUP:
        default:
            break;
        }
        return false;
    }
} InputHandler;

void SamplerTest(GhulbusGraphics::gbImage& image, GhulbusGraphics::gbSpritePtr& sampler_target)
{
    using namespace GhulbusGraphics;
    {
        //ImageSampler::Point sampler(image);
        ImageSampler::Bilinear sampler(image);

        gbDraw2D draw2d(*sampler_target);
        for(int y=0; y<sampler_target->GetHeight(); ++y) {
            float const v = static_cast<float>(y) / static_cast<float>(sampler_target->GetHeight());
            for(int x=0; x<sampler_target->GetWidth(); ++x) {
                float const u = static_cast<float>(x) / static_cast<float>(sampler_target->GetWidth());
                draw2d.PutPixel(x, y, sampler.Sample(u, v));
            }
        }
    }

    gbImage mipmap = ImageSampler::MipMap::BuildMipMap(image);
}

int run_main(int /* argc */, char* /* argv */[])
{
    using namespace GhulbusGraphics;
    using namespace GhulbusGraphics::Events;

    {
        gbWindowPtr main_window = gbWindow::Create(1024, 768);
        /*
        gbWindowPtr window2 = gbWindow::Create(100, 100);
        gbWindowPtr window3 = gbWindow::Create(100, 100);
        gbWindowPtr window4 = gbWindow::Create(100, 100);
        gbWindowPtr window5 = gbWindow::Create(100, 100);
        window3->SetMouseCursorVisible(false);
        window2->SetMouseCursorVisible(false);
        main_window->SetCaptureMouseCursor(true);
        main_window->SetRelativeMouseMode(true);
        */
        //window5->GetEventManager()->ToggleMouseCursorDisplay(false);
        main_window->SetWindowTitle("Foo to the max!");
        main_window->GetEventManager()->AddEventHandler_KeyboardInput(&InputHandler, Events::GBEVENT_KEYB_ALL);
        DIAG_LOG(INFO, "Window " << main_window->GetWidth() << "x" << main_window->GetHeight());
        GhulbusUtil::gbTimer timer;

        gb2D gb2d(main_window->CreateDrawingContext3D());

        try {
            DIAG_THROW_EXCEPTION(Exceptions::SDLError(), "");
        } catch(std::exception& e) {
            DIAG_LOG(INFO, e.what());
        }

        gb2d.GetDrawingContext().SetClearColor(gbColor::XRGB(0, 0, 0));

        gbSpritePtr sprite = gb2d.CreateSprite(100, 100);
        {
            GBCOLOR* data;
            int pitch;
            SurfaceLock<gbSprite> lock(sprite, &data, &pitch);
            for(int y=0; y<sprite->GetHeight(); ++y) {
                for(int x=0; x<sprite->GetWidth(); ++x) {
                    data[y*pitch + x] = gbColor::ARGB(92, 128, x, x+y);
                }
            }
            data[50*pitch + 50] = gbColor::XRGB(0, 0, 255);
        }
        sprite->SetSizeOnScreen(main_window->GetWidth(), main_window->GetHeight());
        {
            GhulbusGraphics::gbDraw2D draw2d(*gb2d.GetOverlay());
            BresenhamTest(&draw2d);
            TurtleTest(&draw2d);
        }

        gbSpritePtr keyboard_sprite = gb2d.CreateSprite(600, 84);
        keyboard_sprite->SetPositionOnScreen(400, 50);

        gbSpritePtr mouse_sprite = gb2d.CreateSprite(200, 48);
        mouse_sprite->SetPositionOnScreen(120, 510);

        gbImageLoader img_loader("../../graphics/test/gfx/lena.png", ImageFileCodec::PNG());
        gbImage lena_img = img_loader.GetImage();
        gbSpritePtr lena_sprite = gb2d.CreateSprite(lena_img);
        lena_sprite->SetPositionOnScreen(0, 0);

        img_loader.ReadFile("../../graphics/test/gfx/asia.jpg", GhulbusGraphics::ImageFileCodec::JPG());
        gbImage asia_img = img_loader.GetImage();
        gbSpritePtr asia_sprite = gb2d.CreateSprite(asia_img);
        asia_sprite->SetSizeOnScreen(400, 400 * asia_img.GetHeight() / asia_img.GetWidth());
        asia_sprite->SetPositionOnScreen(main_window->GetWidth() - asia_sprite->GetWidthOnScreen(), main_window->GetHeight() - asia_sprite->GetHeightOnScreen());

        gbSpritePtr donkey_sprite_unsegmented;
        {
            img_loader.ReadFile("../../graphics/test/gfx/donkey_clap.png", ImageFileCodec::PNG());
            gbImage tmp = img_loader.GetImage();
            donkey_sprite_unsegmented = gb2d.CreateSprite(tmp);
        }
        gb2d.UnregisterFromDrawing(donkey_sprite_unsegmented.get());
        gbSpriteSegmenter donkey_sprite(donkey_sprite_unsegmented);
        donkey_sprite.SetSegmentationGrid(23, 1);
        donkey_sprite.SetDestinationRect( GhulbusGraphics::Rect( gbMath::Vector2i(200, 240),
                                                                 donkey_sprite.GetCellWidth()*2,
                                                                 donkey_sprite.GetCellHeight()*2 ) );
        donkey_sprite.SetActiveGridCell(0);
        gb2d.RegisterForDrawing(&donkey_sprite);

        gbSpritePtr ryu_sprite_unsegmented;
        {
            img_loader.ReadFile("../../graphics/test/gfx/ryu.png", ImageFileCodec::PNG());
            gbImage tmp = img_loader.GetImage();
            ryu_sprite_unsegmented = gb2d.CreateSprite(tmp);
        }
        gb2d.UnregisterFromDrawing(ryu_sprite_unsegmented.get());
        gbSpriteSegmenter ryu_sprite(ryu_sprite_unsegmented);
        ryu_sprite.SetSegmentationGrid(3, 2);
        ryu_sprite.SetDestinationRect( GhulbusGraphics::Rect( gbMath::Vector2i(400, 240),
                                                              ryu_sprite.GetCellWidth()*2,
                                                              ryu_sprite.GetCellHeight()*2 ) );
        ryu_sprite.SetActiveGridCell(0);
        gb2d.RegisterForDrawing(&ryu_sprite);

        {
            gbSpritePtr leak_test = gb2d.CreateSprite(100, 100);
            gbDraw2D draw2d(*leak_test);
            draw2d.SetClearColor(gbColor::XRGB(255,0,0));
            draw2d.Clear();
            leak_test->SetPositionOnScreen(800, 600);
        }

        gbSpritePtr sampler_target = gb2d.CreateSprite(lena_img.GetWidth()/3, lena_img.GetHeight()/3);
        sampler_target->SetPositionOnScreen(300, 500);

        GhulbusUtil::Timestamp time_acc = 0;
        unsigned int const target_fps = 10;
        std::vector<gbSpritePtr> testsprites;
        while(!main_window->GetEventProcessor()->IsDone())
        {
            timer.Mark();
            {
                std::ostringstream window_title;
                window_title << "gbGraphics SDL Demo  " << std::floor(timer.GetFPS() * 10.0) / 10.0 << " FPS";
                //main_window->SetWindowTitle(window_title.str().c_str());

                time_acc += timer.GetDelta();
                if(time_acc  >= 1000 / target_fps) {
                    donkey_sprite.AdvanceActiveGridCell();
                    ryu_sprite.AdvanceActiveGridCell();
                    time_acc = 0;
                }
            }
            main_window->GetEventProcessor()->ProcessMessages();
            GhulbusGraphics::Draw2D_Util::DrawKeyboard(*keyboard_sprite, main_window->GetEventManager()->QueryKeyboardState());
            GhulbusGraphics::Draw2D_Util::DrawMouse(*mouse_sprite, main_window->GetEventManager()->QueryMouseState());
            gb2d.GetDrawingContext().Clear();
            SamplerTest(lena_img, sampler_target);
            KeyboardState const* keyb = main_window->GetEventManager()->QueryKeyboardState();
            if(keyb->keys[GBKEY_UP] || keyb->keys[GBKEY_W]) {
                lena_sprite->SetPositionOnScreen( lena_sprite->GetPositionOnScreenX(), lena_sprite->GetPositionOnScreenY() - 5 );
            }
            if(keyb->keys[GBKEY_DOWN] || keyb->keys[GBKEY_S]) {
                lena_sprite->SetPositionOnScreen( lena_sprite->GetPositionOnScreenX(), lena_sprite->GetPositionOnScreenY() + 5 );
            }
            if(keyb->keys[GBKEY_LEFT] || keyb->keys[GBKEY_A]) {
                lena_sprite->SetPositionOnScreen( lena_sprite->GetPositionOnScreenX() - 5, lena_sprite->GetPositionOnScreenY() );
            }
            if(keyb->keys[GBKEY_RIGHT] || keyb->keys[GBKEY_D]) {
                lena_sprite->SetPositionOnScreen( lena_sprite->GetPositionOnScreenX() + 5, lena_sprite->GetPositionOnScreenY() );
            }
            if(keyb->keys[GBKEY_RETURN]) {
                testsprites.push_back(gb2d.CreateSprite(128, 128));
            }
            gb2d.Draw();
            gb2d.GetDrawingContext().Present();
        }
    }

#if defined WIN32 && defined _DEBUG
    system("PAUSE");
#endif
    return 0;
}

int main(int argc, char* argv[])
{
#ifndef _DEBUG
    try {
#endif
        return run_main(argc, argv);
#ifndef _DEBUG
    } catch(Diagnostic::Exception& e) {
        DIAG_LOG(ERROR, "Exception: " << e.what());
        DIAG_LOG(ERROR, "at\n" << *Diagnostic::GetExceptionInfo<Diagnostic::Exception_Info::location>(e));
        return 1;
    }
#endif
}




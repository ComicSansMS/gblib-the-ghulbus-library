/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/image_file_codec.hpp>
#include <diagnostic/diagnostic.hpp>

#include <cstdint>
#include <functional>
#include <vector>
#include <boost/lexical_cast.hpp>

/*
 * Image types
 */
enum ImageType_T {
    TGA_NO_DATA                  =  0,
    TGA_COLORMAPPED_UNCOMPRESSED =  1,
    TGA_TRUECOLOR_UNCOMPRESSED   =  2,
    TGA_BLACKWHITE_UNCOMPRESSED  =  3,
    TGA_COLORMAPPED_RLE          =  9,
    TGA_TRUECOLOR_RLE            = 10,
    TGA_BLACKWHITE_RLE           = 11
};
/*
 * currently supported: unmapped rgb 16, 24, 32 bit
 */
struct TGAHEADER {
    uint8_t  nCharIDField;            ///< Number of Chars in ID Field (1 Byte)
    uint8_t  ColorMapType;            ///< is 1 if color map specified; must be 0 for us
    uint8_t  ImageTypeCode;            ///< Data Type; must be 2 for us
    uint8_t  ColorMapSpec[5];        ///< should be 0
    uint16_t XOrigin;                ///< expected to be 0
    uint16_t YOrigin;                ///< expected to be 0
    uint16_t Width;                    ///< Picture Width
    uint16_t Height;                ///< Picture Height
    uint8_t  ImagePixelSize;        ///< Bits per Pixel
    uint8_t  ImageDescByte;            ///< Image Descriptor Byte
};

TGAHEADER ReadHeader(std::istream& is)
{
    TGAHEADER ret;
    is.read(reinterpret_cast<char*>(&ret), sizeof(TGAHEADER));
    if(is.fail()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Unable to read TGA header" );
    }
    is.seekg(ret.nCharIDField, std::ios_base::cur);
    if(is.fail()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Unable to read TGA header" );
    }
    return ret;
}

std::vector<uint8_t> ReadImageUncompressed(std::istream& is, TGAHEADER const& header)
{
    std::vector<uint8_t> data(header.Width*header.Height*(header.ImagePixelSize/8));
    is.read(reinterpret_cast<char*>(data.data()), data.size());
    if(is.fail()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Unable to read TGA image data" );
    }
    return data;
}

std::vector<uint8_t> ReadImageRLE(std::istream& is, TGAHEADER const& header)
{
    //adjust file pointer to beginning of image data:
    int total_pixel_count = 0;
    int const n_channels = header.ImagePixelSize >> 3;
    std::vector<uint8_t> buffer(n_channels);
    //loop until desired number of pixels was read
    std::vector<uint8_t> data(header.Width*header.Height*(header.ImagePixelSize/8));
    while(total_pixel_count < header.Width * header.Height) {
        uint8_t rle_packet_header;
        is.read(reinterpret_cast<char*>(&rle_packet_header), 1);
        if(rle_packet_header & 0x80) {
            //rle block
            int const repcount = (rle_packet_header & 0x7F) + 1;
            //read color value
            is.read(reinterpret_cast<char*>(buffer.data()), n_channels);
            //repeat color value repcount times
            for(int i=0; i<repcount; ++i) {
                for(int j=0; j<n_channels; ++j) {
                    data[total_pixel_count * n_channels + j] = buffer[j];
                }
                ++total_pixel_count;
            }
        } else {
            //raw block
            int const blocksize = (rle_packet_header & 0x7F) + 1;
            is.read(reinterpret_cast<char*>(&data[total_pixel_count * n_channels]), blocksize * n_channels);
            total_pixel_count += blocksize;
        }
    }
    return data;
}

std::vector<uint8_t> GetRawImageData(std::istream& is, TGAHEADER const& header)
{
    std::vector<uint8_t> ret;
    switch(header.ImageTypeCode) {
    case TGA_NO_DATA:
        //nothing to do
        break;
    case TGA_TRUECOLOR_UNCOMPRESSED:
        ret = ReadImageUncompressed(is, header);
        break;
    case TGA_TRUECOLOR_RLE:
        ret = ReadImageRLE(is, header);
        break;
    case TGA_BLACKWHITE_UNCOMPRESSED:
    case TGA_COLORMAPPED_UNCOMPRESSED:
    case TGA_COLORMAPPED_RLE:
    case TGA_BLACKWHITE_RLE:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                                "Image type not supported - True color files only" );
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                                "Invalid image type \'" + boost::lexical_cast<std::string>(header.ImageTypeCode) + "\'" );
    }
    return ret;
}

void ConvertData(std::vector<uint8_t> const& raw_data, GB_GRAPHICS_NAMESPACE::GBCOLOR* buffer, TGAHEADER const& header)
{
    using namespace GB_GRAPHICS_NAMESPACE::gbColor;
    //truvision images are flipped upside-down
    bool const is_truvision = ((header.ImageDescByte & 0x20) == 0);
    int const img_size = header.Width*header.Height;
    int const img_width = header.Width;
    int const img_height = header.Height;
    int img_x = 0;
    int img_y = (is_truvision)?(img_height-1):0;
    switch(header.ImagePixelSize) {
    case 16:
        /*  ARRR RRGG GGGB BBBB
         */
        /// @todo 16 bit TGA support
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "16bit TGA support not implemented");
        break;
    case 24:
        for(int i=0; i<img_size; ++i) {
            DIAG_ASSERT((img_x >= 0) && (img_x < img_width) && (img_y >= 0) && (img_y < img_height));
            buffer[img_y*img_width + img_x] = XRGB(raw_data[i*3+2], raw_data[i*3+1], raw_data[i*3]);
            if(++img_x == img_width) { img_y += (is_truvision)?(-1):(1); img_x = 0; }
        }
        break;
    case 32:
        for(int i=0; i<img_size; ++i) {
            DIAG_ASSERT((img_x >= 0) && (img_x < img_width) && (img_y >= 0) && (img_y < img_height));
            buffer[img_y*img_width + img_x] = ARGB(raw_data[i*4+3], raw_data[i*4+2], raw_data[i*4+1], raw_data[i*4]);
            if(++img_x == img_width) { img_y += (is_truvision)?(-1):(1); img_x = 0; }
        }
        break;
    default:
        //see check in TGA::Decode()
        DIAG_ASSERT(false);
    }
}

namespace GB_GRAPHICS_NAMESPACE {
namespace ImageFileCodec {

void TGA::Decode(std::istream& is, GBCOLOR* buffer, ImageProperties* out_img_props, BufferResizeFunction const& buffer_resize_fun) const
{
    std::istream::pos_type const old_pos = is.tellg();
    try {
        TGAHEADER header = ReadHeader(is);
        std::vector<uint8_t> raw_data = GetRawImageData(is, header);
        if(raw_data.empty()) {
            out_img_props->width = out_img_props->height = 0;
            return;
        }
        //check bit depth before buffer resize
        switch(header.ImagePixelSize) {
        case 16: case 24: case 32: break;
        default:
            DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                                  "Unsupported bit depth \'" + boost::lexical_cast<std::string>(header.ImagePixelSize) + "\'" );
        }

        out_img_props->height = header.Height;
        out_img_props->width = header.Width;
        buffer_resize_fun(&buffer, out_img_props);
        ConvertData(raw_data, buffer, header);
    } catch(...) {
        //restore file pointer on error
        is.seekg(old_pos);
        throw;
    }
}

}
}

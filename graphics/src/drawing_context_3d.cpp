/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <diagnostic/diagnostic.hpp>

#include <algorithm>
#include <fstream>
#include <iterator>
#include <ostream>

namespace GB_GRAPHICS_NAMESPACE {

gbShaderPtr gbDrawingContext3D::CreateShaderFromFile(ShaderType::ShaderType_T type, std::string const& filename)
{
    std::ifstream fin(filename.c_str());
    if(fin.fail()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError() << Diagnostic::Exception_Info::file_name(filename),
                              "Unable to open shader file" );
    }
    fin.unsetf(std::ios_base::skipws);
    std::string buffer;
    std::copy(std::istream_iterator<char>(fin), std::istream_iterator<char>(), std::back_inserter(buffer));
    try {
        return CreateShader(type, buffer);
    } catch(Exceptions::ShaderCompileError& e) {
        e << Diagnostic::Exception_Info::file_name(filename);
        throw;
    }
}

std::ostream& operator<<(std::ostream& os, gbDrawingContext3D::RendererInfo::VersionNumber_T const& v)
{
    os << v.Major;
    if(v.Minor >= 0) {
        os << "." << v.Minor;
        if(v.Release >= 0) {
            os << "." << v.Release;
        }
    }
    return os;
}

}

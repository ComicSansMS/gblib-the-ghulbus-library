/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/event_processor.hpp>
#include <gbGraphics/event_manager.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/window.hpp>
#include <diagnostic/diagnostic.hpp>

#include <cstdlib>

#include <SDL.h>

namespace GB_GRAPHICS_NAMESPACE {

gbEventProcessor_SDL::gbEventProcessor_SDL()
    :m_EventManager(nullptr), m_ActiveWindowSDL(nullptr)
{
}

gbEventProcessor_SDL::~gbEventProcessor_SDL()
{
}

void gbEventProcessor_SDL::ProcessMessages()
{
    SDL_Event msg;
    bool reset_mouse = false;
    while(SDL_PollEvent(&msg)) {
        if (!m_EventManager) { continue; }
        switch(msg.type) {
            case SDL_MOUSEMOTION:
            {
                Events::MouseState state;
                state.posx = msg.motion.x;
                state.posy = msg.motion.y;
                state.lbutton = ((msg.motion.state & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0);
                state.rbutton = ((msg.motion.state & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0);
                state.mbutton = ((msg.motion.state & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0);
                state.mousewheel = 0;
                m_EventManager->SignalEvent_MouseMove(&state);
                state.posx = msg.motion.xrel;
                state.posy = msg.motion.yrel;
                state.lbutton = ((msg.motion.state & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0);
                state.rbutton = ((msg.motion.state & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0);
                state.mbutton = ((msg.motion.state & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0);
                state.mousewheel = 0;
                m_EventManager->SignalEvent_MouseMoveRel(&state);
                if(m_ActiveWindowSDL && m_ActiveWindowSDL->IsInRelativeMouseMode()) {
                    if( (msg.motion.x <= 10) || (msg.motion.x >= m_ActiveWindowSDL->GetWidth()-10) ||
                        (msg.motion.y <= 10) || (msg.motion.y >= m_ActiveWindowSDL->GetHeight()-10) )
                    {
                        reset_mouse = true;
                    }
                }
            } break;
            case SDL_MOUSEBUTTONDOWN: case SDL_MOUSEBUTTONUP:
            {
                Events::MouseState state;
                state.posx = msg.button.x;
                state.posy = msg.button.y;
                state.lbutton = (msg.button.button == SDL_BUTTON_LEFT);
                state.rbutton = (msg.button.button == SDL_BUTTON_RIGHT);
                state.mbutton = (msg.button.button == SDL_BUTTON_MIDDLE);
                state.mousewheel = 0;
                if(msg.button.state == SDL_PRESSED) {
                    m_EventManager->SignalEvent_MouseButtonDown(&state);
                } else if(msg.button.state == SDL_RELEASED) {
                    m_EventManager->SignalEvent_MouseButtonUp(&state);
                }
            } break;
            case SDL_MOUSEWHEEL:
            {
                Events::MouseState state;
                state.posx = -1;
                state.posy = -1;
                state.lbutton = state.rbutton = state.mbutton = false;
                state.mousewheel = msg.wheel.y;
                m_EventManager->SignalEvent_MouseWheel(&state);
            } break;
            case SDL_KEYDOWN:
            {
                Events::Key k = TranslateSDLKey( msg.key.keysym.sym );
                if(m_EventManager->SignalEvent_KeyboardKeyDown(k)) { break; }
                switch(msg.key.keysym.sym) {
                    case SDLK_ESCAPE:
                        SetDone();
                        break;
                }
                if(k == Events::GBKEY_PRINT) {
                    // DIAG_LOG(INFO, "down");
                }
            } break;
            case SDL_KEYUP:
            {
                Events::Key k = TranslateSDLKey( msg.key.keysym.sym );
                m_EventManager->SignalEvent_KeyboardKeyUp(k);
                if(k == Events::GBKEY_PRINT) {
                    // DIAG_LOG(INFO, "up");
                }
            } break;
            case SDL_WINDOWEVENT:
            {
                struct Events::WindowState w;
                gbWindow_SDL* window = GetWindowById_SDL(msg.window.windowID);
                gbWindow* old_window = GetActiveWindow();
                switch(msg.window.event) {
                case SDL_WINDOWEVENT_FOCUS_GAINED:        //< Window has gained keyboard focus
                    // DIAG_LOG(INFO, "focus gain");
                    w.window = window;
                    SetActiveWindow(window);
                    m_EventManager->SignalEvent_WindowEvent(Events::GBEVENT_WINDOW_FOCUS_GAINED, &w);
                    break;
                case SDL_WINDOWEVENT_FOCUS_LOST:        //< Window has lost keyboard focus
                    // DIAG_LOG(INFO, "focus lost");
                    w.window = window;
                    if(window->IsMouseCursorCaptured()) {
                        /** @todo Workaround to fix self-draw bug of SDL_SetWindowGrab().
                         *        Normally, this should be done by gbWindow::SetCaptureMouseCursor().
                         */
                        if(window == old_window) { SetActiveWindow(nullptr); }
                        SDL_SetWindowGrab(window->GetSDLWindow_SDL(), SDL_FALSE);
                    }
                    m_EventManager->SignalEvent_WindowEvent(Events::GBEVENT_WINDOW_FOCUS_LOST, &w);
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:   //< The window size has changed, either as a result of an API call
                                                     //< or through the system or user changing the window size.
                    // DIAG_LOG(INFO, "sizechange " << msg.window.data1 << "x" << msg.window.data2);
                    w.window = window;
                    NotifyWindowResize(msg.window.windowID);
                    m_EventManager->SignalEvent_WindowEvent(Events::GBEVENT_WINDOW_RESIZE, &w);
                    break;
                case SDL_WINDOWEVENT_CLOSE:
                    // DIAG_LOG(INFO, "close");
                    m_IdWindowMap.erase(msg.window.windowID);
                    m_IdResizeCallbackMap.erase(msg.window.windowID);
                    break;
                /*
                case SDL_WINDOWEVENT_SHOWN:          //< Window has been shown
                    DIAG_LOG(INFO, "shown");
                    break;
                case SDL_WINDOWEVENT_HIDDEN:         //< Window has been hidden
                    DIAG_LOG(INFO, "hidden");
                    break;
                case SDL_WINDOWEVENT_EXPOSED:        //< Window has been exposed and should be redrawn
                    DIAG_LOG(INFO, "exposed");
                    break;
                case SDL_WINDOWEVENT_MOVED:          //< Window has been moved to data1, data2
                    DIAG_LOG(INFO, "moved");
                    break;
                case SDL_WINDOWEVENT_RESIZED:        //< Window has been resized to data1xdata2
                    DIAG_LOG(INFO, "resize " << msg.window.data1 << "x" << msg.window.data2);
                    break;
                case SDL_WINDOWEVENT_SIZE_CHANGED:   //< The window size has changed, either as a result of an API call
                                                     //< or through the system or user changing the window size.
                    DIAG_LOG(INFO, "sizechange " << msg.window.data1 << "x" << msg.window.data2);
                    break;
                case SDL_WINDOWEVENT_MINIMIZED:      //< Window has been minimized
                    DIAG_LOG(INFO, "mined");
                    break;
                case SDL_WINDOWEVENT_MAXIMIZED:      //< Window has been maximized
                    DIAG_LOG(INFO, "maxed");
                    break;
                case SDL_WINDOWEVENT_RESTORED:       //< Window has been restored to normal size and position
                    DIAG_LOG(INFO, "restored");
                    break;
                case SDL_WINDOWEVENT_ENTER:          //< Window has gained mouse focus
                    DIAG_LOG(INFO, "enter");
                    break;
                case SDL_WINDOWEVENT_LEAVE:          //< Window has lost mouse focus
                    DIAG_LOG(INFO, "leave");
                    break;
                // */
                }
                //*/
                /*
                if( (msg.active.state & SDL_APPINPUTFOCUS) && (msg.active.gain == 1) ) {
                    //(re-)gained input focus; states may be corrupted!
                    Uint8 *keystate = SDL_GetKeyboardState(NULL);
                    for(int i=0; i<SDL_NUM_SCANCODES; ++i) {
                        Events::Key k = TranslateSDLKey( SDL_SCANCODE_TO_KEYCODE(i) );
                        if(k != Events::GBKEY_UNKNOWN) {
                            m_KeyboardState.keys[k] = keystate[i];
                        }
                    }
                }
                */
                if(GetActiveWindow() != old_window) {
                    // active window has changed
                    reset_mouse = false;
                    if(GetActiveWindow()) {
                        DIAG_ASSERT(GetActiveWindow() == window);
                        m_ActiveWindowSDL = window;
                        ActiveWindowChange(window);
                    } else {
                        m_ActiveWindowSDL = nullptr;
                    }
                }
            } break;
            case SDL_QUIT:
                SetDone();
                break;
            default:
                break;
        }
    }
    if(reset_mouse) {
        // mouse reset was requested by active window
        DIAG_ASSERT(m_ActiveWindowSDL && m_ActiveWindowSDL->IsInRelativeMouseMode());
        Events::MouseState m;
        m.lbutton = m.rbutton = m.mbutton = false;
        m.mousewheel = 0;
        m.posx = m_ActiveWindowSDL->GetWidth()/2;
        m.posy = m_ActiveWindowSDL->GetHeight()/2;
        SDL_WarpMouseInWindow(m_ActiveWindowSDL->GetSDLWindow_SDL(), m.posx, m.posy);
        SDL_PumpEvents();
        SDL_FlushEvents(SDL_MOUSEMOTION, SDL_MOUSEMOTION);
        m_EventManager->SignalEvent_MouseMove(&m);
    }
}

void gbEventProcessor_SDL::RegisterWindow_SDL(gbWindow_SDL* window, uint32_t id, ResizeCallback const& resize_callback)
{
    m_IdWindowMap[id] = window;
    m_IdResizeCallbackMap[id] = resize_callback;
    /// @todo support for multiple event managers
    m_EventManager = window->GetEventManager();
}

gbWindow_SDL* gbEventProcessor_SDL::GetWindowById_SDL(uint32_t id) const
{
    auto ret = m_IdWindowMap.find(id);
    if(ret == m_IdWindowMap.end()) { return nullptr; }
    return ret->second;
}

void gbEventProcessor_SDL::NotifyWindowResize(uint32_t id) const
{
    auto cb = m_IdResizeCallbackMap.find(id);
    if(cb != m_IdResizeCallbackMap.end()) {
        cb->second();
    }
}

void gbEventProcessor_SDL::ActiveWindowChange(gbWindow_SDL* window)
{
    if(window && (m_ActiveWindowSDL == window)) {
        SDL_ShowCursor(window->GetMouseCursorVisible()?SDL_TRUE:SDL_FALSE);
        if(window->IsMouseCursorCaptured()) {
            SDL_SetWindowGrab(window->GetSDLWindow_SDL(), SDL_TRUE);
        } else {
            SDL_SetWindowGrab(window->GetSDLWindow_SDL(), SDL_FALSE);
        }
    }
    m_EventManager = (window) ? window->GetEventManager() : nullptr;
}

Events::Key gbEventProcessor_SDL::TranslateSDLKey(int32_t sdl_keycode)
{
    switch(sdl_keycode) {
    case SDLK_ESCAPE:       return Events::GBKEY_ESCAPE;
    case SDLK_1:            return Events::GBKEY_1;
    case SDLK_2:            return Events::GBKEY_2;
    case SDLK_3:            return Events::GBKEY_3;
    case SDLK_4:            return Events::GBKEY_4;
    case SDLK_5:            return Events::GBKEY_5;
    case SDLK_6:            return Events::GBKEY_6;
    case SDLK_7:            return Events::GBKEY_7;
    case SDLK_8:            return Events::GBKEY_8;
    case SDLK_9:            return Events::GBKEY_9;
    case SDLK_0:            return Events::GBKEY_0;
    case SDLK_MINUS:        return Events::GBKEY_MINUS;
    case SDLK_EQUALS:       return Events::GBKEY_EQUALS;
    case SDLK_BACKSPACE:    return Events::GBKEY_BACKSPACE;
    case SDLK_TAB:          return Events::GBKEY_TAB;
    case SDLK_q:            return Events::GBKEY_Q;
    case SDLK_w:            return Events::GBKEY_W;
    case SDLK_e:            return Events::GBKEY_E;
    case SDLK_r:            return Events::GBKEY_R;
    case SDLK_t:            return Events::GBKEY_T;
    case SDLK_y:            return Events::GBKEY_Y;
    case SDLK_u:            return Events::GBKEY_U;
    case SDLK_i:            return Events::GBKEY_I;
    case SDLK_o:            return Events::GBKEY_O;
    case SDLK_p:            return Events::GBKEY_P;
    case SDLK_LEFTBRACKET:  return Events::GBKEY_LEFTBRACKET;
    case SDLK_RIGHTBRACKET: return Events::GBKEY_RIGHTBRACKET;
    case SDLK_RETURN:       return Events::GBKEY_RETURN;
    case SDLK_LCTRL:        return Events::GBKEY_LCTRL;
    case SDLK_a:            return Events::GBKEY_A;
    case SDLK_s:            return Events::GBKEY_S;
    case SDLK_d:            return Events::GBKEY_D;
    case SDLK_f:            return Events::GBKEY_F;
    case SDLK_g:            return Events::GBKEY_G;
    case SDLK_h:            return Events::GBKEY_H;
    case SDLK_j:            return Events::GBKEY_J;
    case SDLK_k:            return Events::GBKEY_K;
    case SDLK_l:            return Events::GBKEY_L;
    case SDLK_SEMICOLON:    return Events::GBKEY_SEMICOLON;
    case SDLK_QUOTE:        return Events::GBKEY_QUOTESINGLE;
    case SDLK_BACKQUOTE:    return Events::GBKEY_BACKQUOTE;
    case SDLK_LSHIFT:       return Events::GBKEY_LSHIFT;
    case SDLK_BACKSLASH:    return Events::GBKEY_BACKSLASH;
    case SDLK_z:            return Events::GBKEY_Z;
    case SDLK_x:            return Events::GBKEY_X;
    case SDLK_c:            return Events::GBKEY_C;
    case SDLK_v:            return Events::GBKEY_V;
    case SDLK_b:            return Events::GBKEY_B;
    case SDLK_n:            return Events::GBKEY_N;
    case SDLK_m:            return Events::GBKEY_M;
    case SDLK_COMMA:        return Events::GBKEY_COMMA;
    case SDLK_PERIOD:       return Events::GBKEY_PERIOD;
    case SDLK_SLASH:        return Events::GBKEY_SLASH;
    case SDLK_RSHIFT:       return Events::GBKEY_RSHIFT;
    case SDLK_KP_MULTIPLY:  return Events::GBKEY_NUM_MULTIPLY;
    case SDLK_LALT:         return Events::GBKEY_LALT;
    case SDLK_SPACE:        return Events::GBKEY_SPACE;
    case SDLK_CAPSLOCK:     return Events::GBKEY_CAPSLOCK;
    case SDLK_F1:           return Events::GBKEY_F1;
    case SDLK_F2:           return Events::GBKEY_F2;
    case SDLK_F3:           return Events::GBKEY_F3;
    case SDLK_F4:           return Events::GBKEY_F4;
    case SDLK_F5:           return Events::GBKEY_F5;
    case SDLK_F6:           return Events::GBKEY_F6;
    case SDLK_F7:           return Events::GBKEY_F7;
    case SDLK_F8:           return Events::GBKEY_F8;
    case SDLK_F9:           return Events::GBKEY_F9;
    case SDLK_F10:          return Events::GBKEY_F10;
    case SDLK_NUMLOCKCLEAR: return Events::GBKEY_NUMLOCK;
    case SDLK_SCROLLLOCK:   return Events::GBKEY_SCROLLOCK;
    case SDLK_KP_7:         return Events::GBKEY_NUM7;
    case SDLK_KP_8:         return Events::GBKEY_NUM8;
    case SDLK_KP_9:         return Events::GBKEY_NUM9;
    case SDLK_KP_MINUS:     return Events::GBKEY_NUM_MINUS;
    case SDLK_KP_4:         return Events::GBKEY_NUM4;
    case SDLK_KP_5:         return Events::GBKEY_NUM5;
    case SDLK_KP_6:         return Events::GBKEY_NUM6;
    case SDLK_KP_PLUS:      return Events::GBKEY_NUM_PLUS;
    case SDLK_KP_1:         return Events::GBKEY_NUM1;
    case SDLK_KP_2:         return Events::GBKEY_NUM2;
    case SDLK_KP_3:         return Events::GBKEY_NUM3;
    case SDLK_KP_0:         return Events::GBKEY_NUM0;
    case SDLK_KP_PERIOD:    return Events::GBKEY_NUM_PERIOD;
    case SDLK_LESS:         return Events::GBKEY_LESS;
    case SDLK_F11:          return Events::GBKEY_F11;
    case SDLK_F12:          return Events::GBKEY_F12;
    case SDLK_F13:          return Events::GBKEY_F13;
    case SDLK_F14:          return Events::GBKEY_F14;
    case SDLK_F15:          return Events::GBKEY_F15;
    case SDLK_KP_EQUALS:    return Events::GBKEY_NUM_EQUALS;
    case SDLK_CARET:        return Events::GBKEY_CARET;
    case SDLK_AT:           return Events::GBEY_AT;
    case SDLK_COLON:        return Events::GBKEY_COLON;
    case SDLK_UNDERSCORE:   return Events::GBKEY_UNDERSCORE;
    case SDLK_KP_ENTER:     return Events::GBKEY_NUM_ENTER;
    case SDLK_RCTRL:        return Events::GBKEY_RCTRL;
    case SDLK_KP_DIVIDE:    return Events::GBKEY_NUM_DIVIDE;
    case SDLK_SYSREQ:       return Events::GBKEY_SYSREQ;
    case SDLK_PRINTSCREEN:  return Events::GBKEY_PRINT;
    case SDLK_RALT:         return Events::GBKEY_RALT;
    case SDLK_PAUSE:        return Events::GBKEY_BREAK;
    case SDLK_HOME:         return Events::GBKEY_HOME;
    case SDLK_UP:           return Events::GBKEY_UP;
    case SDLK_PAGEUP:       return Events::GBKEY_PAGEUP;
    case SDLK_LEFT:         return Events::GBKEY_LEFT;
    case SDLK_RIGHT:        return Events::GBKEY_RIGHT;
    case SDLK_END:          return Events::GBKEY_END;
    case SDLK_DOWN:         return Events::GBKEY_DOWN;
    case SDLK_PAGEDOWN:     return Events::GBKEY_PAGEDOWN;
    case SDLK_INSERT:       return Events::GBKEY_INSERT;
    case SDLK_DELETE:       return Events::GBKEY_DELETE;
    case SDLK_LGUI:         return Events::GBKEY_LWIN;
    case SDLK_RGUI:         return Events::GBKEY_RWIN;
    case SDLK_MENU:         return Events::GBKEY_MENU;
    default:                return Events::GBKEY_UNKNOWN;
    }
}

}


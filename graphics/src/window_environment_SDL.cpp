/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/window.hpp>
#include <gbGraphics/event_processor.hpp>
#include <gbGraphics/graphics_exception.hpp>

#include <diagnostic/diagnostic.hpp>
#include <gbUtil/missing_features.hpp>

#include <cstdlib>

#include <SDL.h>

namespace GB_GRAPHICS_NAMESPACE {

gbWindow_SDL::SDL_WindowEnvironment::SDL_WindowEnvironment()
    :m_EventProcessor(nullptr)
{
    //init SDL
    int ret = 0;
    Uint32 init_flags = SDL_WasInit(0);
    if(init_flags == 0) {
        ret = SDL_Init(SDL_INIT_VIDEO);
    } else if((init_flags & SDL_INIT_VIDEO) == 0) {
        ret = SDL_InitSubSystem(SDL_INIT_VIDEO);
    }
    if(ret < 0) {
        DIAG_THROW_EXCEPTION(Exceptions::SDLError(), "Unable to initialize SDL");
    }
    InitOpenGLAttributes();
    DIAG_ASSERT((SDL_WasInit(0) & SDL_INIT_VIDEO) == SDL_INIT_VIDEO);
    SDL_ClearError();

    m_EventProcessor = std::make_unique<gbEventProcessor_SDL>();
}

gbWindow_SDL::SDL_WindowEnvironment::~SDL_WindowEnvironment()
{
    SDL_QuitSubSystem(SDL_INIT_VIDEO);
    if(SDL_WasInit(0) == 0) {
        SDL_Quit();
    }
}

void gbWindow_SDL::SDL_WindowEnvironment::InitOpenGLAttributes()
{
    auto check = [](int err) {
        if(err != 0) {
            DIAG_THROW_EXCEPTION(Exceptions::SDLError(), "Unable to set SDL OpenGL Attribute");
        }
    };
    check(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3));
    check(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2));
    check(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE));
    check(SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8));
    check(SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8));
    check(SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8));
    check(SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8));
    check(SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1));
    check(SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24));
    check(SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8));
}

gbEventProcessor_SDL* gbWindow_SDL::SDL_WindowEnvironment::GetEventProcessor() const
{
    return m_EventProcessor.get();
}

void gbWindow_SDL::SDL_WindowEnvironment::RegisterWithEnvironment(gbWindow_SDL* window, uint32_t id)
{
    m_EventProcessor->RegisterWindow_SDL(window, id, [window]() { window->WindowResizeHandler(); } );
}

std::vector<gbWindow::DeviceIdentifier> gbWindow_SDL::SDL_WindowEnvironment::EnumerateDevices()
{
    int const n = SDL_GetNumVideoDrivers();
    if(n <= 0) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Error while trying to enumerate video devices" );
    }
    std::vector<gbWindow::DeviceIdentifier> ret;
    ret.reserve(n);
    for(int i=0; i<n; ++i) {
        gbWindow::DeviceIdentifier dev_id;
        dev_id.Name = SDL_GetVideoDriver(i);
        ret.push_back(std::move(dev_id));
    }
    return ret;
}

}

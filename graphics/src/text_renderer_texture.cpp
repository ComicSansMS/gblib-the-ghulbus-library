/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/text_renderer_texture.hpp>
#include <gbGraphics/font.hpp>

#include <diagnostic/assert.hpp>

#include <cstring>

namespace GB_GRAPHICS_NAMESPACE {
namespace Text {

TextRenderer_Texture::TextRenderer_Texture(gbFont* font, LockableSurface& target)
    :m_Font(font), m_TargetLock(target), m_Data(m_TargetLock.GetLockedData()),
     m_Width(target.GetWidth()), m_Height(target.GetHeight()), m_Pitch(m_TargetLock.GetPitch()),
     m_LineIndent(0), m_TabWidth(100), m_ColorTxt(gbColor::XRGB(255, 255, 255))
{
}

TextRenderer_Texture::TextRenderer_Texture(gbFont* font, SurfaceLock<LockableSurface> const& target)
    :m_Font(font), m_Data(target.GetLockedData()),
     m_Width(target->GetWidth()), m_Height(target->GetHeight()), m_Pitch(target.GetPitch()),
     m_LineIndent(0), m_TabWidth(100), m_ColorTxt(gbColor::XRGB(255, 255, 255))
{
}

TextRenderer_Texture::~TextRenderer_Texture()
{
}

bool TextRenderer_Texture::RenderChar(int c)
{
    if(!m_Font) { return false; }
    switch(c) {
    case '\b': /* Backspace */
        m_CursorPos.x_ = std::max(m_LineIndent, m_CursorPos.x_ - m_Font->GetFontMaxWidth());
        m_Font->ResetKerning();
        break;
    case '\n': /* Line feed */
        LineBreak();
        break;
    case '\r': /* Carriage return */
        m_CursorPos.x_ = m_LineIndent;
        m_Font->ResetKerning();
        break;
    case '\t': /* Horizontal tab */
        m_CursorPos.x_ = ((m_CursorPos.x_ - m_LineIndent)/m_TabWidth + 1) * m_TabWidth + m_LineIndent;
        if(m_CursorPos.x_ >= m_Width) {
            LineBreak();
        }
        m_Font->ResetKerning();
        break;
    case '\a': /* Bell */
    case '\f': /* Form feed */
    case '\v': /* Vertical tab */
    default:
        if(!RenderGlyph(c)) {
            return false;
        }
        break;
    }
    return true;
}

void TextRenderer_Texture::SetFont(gbFont* font)
{
    m_Font = font;
}

void TextRenderer_Texture::SetTextColor(GBCOLOR c)
{
    m_ColorTxt = c;
    m_ColorTxt &= ~gbColor::MaskA();
}

void TextRenderer_Texture::SetCursorPosition(int x, int y)
{
    if((x < 0) || (x >= m_Width) || (y < 0) || (y >= m_Height)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "New cursor position exceeds target bounds" );
    }
    m_CursorPos.x_ = x;
    m_CursorPos.y_ = y;
}

void TextRenderer_Texture::SetTabWidthPixels(int px_width)
{
    if(px_width < 0) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Negative tab width values not allowed" );
    }
    m_TabWidth = px_width;
}

void TextRenderer_Texture::SetNewLineIndentation(int px_indent)
{
    if(px_indent < 0) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Negative indent values not allowed" );
    }
    m_LineIndent = px_indent;
}

void TextRenderer_Texture::Clear()
{
    auto const pitch = m_TargetLock.GetPitch();
    auto data = m_TargetLock.GetLockedData();
    for(int y=0; y<m_Height; ++y) {
        std::memset(data, 0, m_Width*sizeof(GBCOLOR));
        data += pitch;
    }
}

void TextRenderer_Texture::Clear(GBCOLOR c)
{
    auto const pitch = m_TargetLock.GetPitch();
    auto data = m_TargetLock.GetLockedData();
    for(int y=0; y<m_Height; ++y) {
        std::fill(data, data + m_Width, c);
        data += pitch;
    }
}

GB_MATH_NAMESPACE::Vector2i TextRenderer_Texture::GetCursorPosition() const
{
    return m_CursorPos;
}

bool TextRenderer_Texture::RenderGlyph(int c)
{
    m_Font->LoadGlyph(c);
    m_Font->RenderGlyph();
    m_CursorPos += m_Font->GetKerning();
    auto const glyph_origin = m_Font->GetGlyphOrigin();
    int const glyph_width = m_Font->GetGlyphWidth();
    int const glyph_height = m_Font->GetGlyphHeight();
    if(m_CursorPos.x_ + glyph_origin.x_ + glyph_width >= m_Width) { LineBreak(); }
    auto const draw_cursor = m_CursorPos + glyph_origin;
    int const font_height = m_Font->GetFontHeight();
    //clipping
    if( (m_CursorPos.x_ + glyph_origin.x_ + glyph_width-1 >= m_Width) ||
        (draw_cursor.y_ + glyph_height-1 >= m_Height) ) {
            //if lower right corner of glyph is outside target surface, abort
            return false; 
    }
    //upper left glyph corner is just clipped
    int const y_loop_offs = std::max(0, -draw_cursor.y_-font_height);
    for(int y=y_loop_offs; y<glyph_height-y_loop_offs; ++y) {
        int const target_y = draw_cursor.y_+y;
        GBCOLOR* data = &(m_Data[(target_y)*m_Pitch + draw_cursor.x_]);
        int const x_loop_offs = std::max(0, -draw_cursor.x_);
        for(int x=x_loop_offs; x<glyph_width-x_loop_offs; ++x, ++data) {
            int const target_x = draw_cursor.x_ + x;
            auto const c = m_Font->SampleGlyph(x, y);
            if(c != 0) {
                DIAG_ASSERT((target_y >= 0) && (target_y < m_Height));
                DIAG_ASSERT((target_x >= 0) && (target_x < m_Width));
                GBCOLOR const draw_color = (m_ColorTxt | (c << gbColor::ACTIVE_SCHEME.Offset.A));
                *data = gbColor::Lerp(*data, draw_color, c);
            }
        }
    }
    //advance cursor
    m_CursorPos += m_Font->GetGlyphAdvance();
    if(m_CursorPos.x_ >= m_Width) {
        LineBreak();
    }
    return true;
}

void TextRenderer_Texture::LineBreak()
{
    m_CursorPos.x_ = m_LineIndent;
    m_CursorPos.y_ += m_Font->GetFontHeight();
    m_Font->ResetKerning();
}

}
}

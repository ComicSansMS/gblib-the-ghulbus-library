/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/geometry.hpp>
#include <gbGraphics/diagnostic_OGL.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/index_buffer.hpp>
#include <gbGraphics/shadow_state_OGL.hpp>
#include <gbGraphics/vertex_data_base.hpp>
#include <gbGraphics/vertex_buffer.hpp>
#include <gbGraphics/vertex_binding.hpp>
#include <gbGraphics/vertex_format_base.hpp>
#include <diagnostic/diagnostic.hpp>
#include <algorithm>
#include <iterator>

namespace GB_GRAPHICS_NAMESPACE {

GLint GetVertexAttribSize(gbVertexFormatBase::ComponentType_T type);
GLsizei GetVertexAttribType(gbVertexFormatBase::ComponentType_T type);

gbGeometry_OGL::gbGeometry_OGL(gbVertexBuffer_OGL* vertex_buffer, gbVertexBinding const& bindings, gbShadowState_OGL* shadow_state)
    :m_VaoId(0), m_VertexBuffer(vertex_buffer), m_IndexBuffer(nullptr), m_Program(bindings.GetProgram()), m_glShadowState(shadow_state)
{
    Init(bindings);
}

gbGeometry_OGL::gbGeometry_OGL(gbVertexBuffer_OGL* vertex_buffer, gbVertexBinding const& bindings, gbIndexBuffer_OGL* index_buffer, gbShadowState_OGL* shadow_state)
    :m_VaoId(0), m_VertexBuffer(vertex_buffer), m_IndexBuffer(index_buffer), m_Program(bindings.GetProgram()), m_glShadowState(shadow_state)
{
    Init(bindings);
}

void gbGeometry_OGL::Init(gbVertexBinding const& bindings)
{
    if(!m_VertexBuffer) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Vertex buffer is NULL" );
    }
    if((*bindings.GetVertexFormat()) != (*m_VertexBuffer->GetVertexFormat())) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Vertex format of buffer does not match bindings" );
    }

    ErrorMonitor_OGL monitor;
    glGenVertexArrays(1, &m_VaoId);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
                              "Unable to create vertex array object" );
    }

    m_glShadowState->BindVertexArray(m_VaoId);
    std::for_each(begin(bindings), end(bindings), [](gbVertexBinding::Binding_T const& binding) {
        glEnableVertexAttribArray(binding.ShaderIndex);
    });
    m_glShadowState->BindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer->GetOGLVertexBufferId_OGL());
    int const stride = bindings.GetDataStride();
    std::for_each(begin(bindings), end(bindings), [stride](gbVertexBinding::Binding_T const& binding) {
        glVertexAttribPointer( binding.ShaderIndex, GetVertexAttribSize(binding.Type), GetVertexAttribType(binding.Type),
                               GL_FALSE, stride, reinterpret_cast<GLvoid const*>(binding.VertexDataOffset) );
    });
    if(m_IndexBuffer) {
        m_glShadowState->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndexBuffer->GetOGLIndexBufferId_OGL());
    }
    if(monitor.CheckError()) {
        m_glShadowState->ReleaseVertexArray(m_VaoId);
        glDeleteVertexArrays(1, &m_VaoId);
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
                              "Error while applying vertex data bindings" );
    }
    m_glShadowState->BindVertexArray(0);
}

gbGeometry_OGL::~gbGeometry_OGL()
{
    m_glShadowState->ReleaseVertexArray(m_VaoId);
    glDeleteVertexArrays(1, &m_VaoId);
}

gbVertexBuffer const* gbGeometry_OGL::GetVertexBuffer() const
{
    return m_VertexBuffer;
}

gbIndexBuffer const* gbGeometry_OGL::GetIndexBuffer() const
{
    return m_IndexBuffer;
}

gbProgram const* gbGeometry_OGL::GetProgram() const
{
    return m_Program;
}

GLuint gbGeometry_OGL::GetOGLVertexAttributeArrayId_OGL() const
{
    return m_VaoId;
}

GLint GetVertexAttribSize(gbVertexFormatBase::ComponentType_T type)
{
    switch(type) {
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT:
    case gbVertexFormatBase::VERTEX_COMPONENT_INT:
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT:
        return 1;
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_INT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT2:
        return 2;
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_INT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT3:
        return 3;
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT4:
    case gbVertexFormatBase::VERTEX_COMPONENT_INT4:
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT4:
        return 4;
    /// @todo matrix vertex attributes
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2x3:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2x4:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3x2:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3x4:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4x2:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4x3:
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Unknown vertex component type" );
    }
}

GLsizei GetVertexAttribType(gbVertexFormatBase::ComponentType_T type)
{
    switch(type) {
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT4:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2x3:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2x4:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3x2:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3x4:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4x2:
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4x3:
        return GL_FLOAT;
    case gbVertexFormatBase::VERTEX_COMPONENT_INT:
    case gbVertexFormatBase::VERTEX_COMPONENT_INT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_INT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_INT4:
        return GL_INT;
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT:
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_UINT4:
        return GL_UNSIGNED_INT;
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE2:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE3:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE4:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT2:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT3:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT4:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT2x3:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT2x4:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT3x2:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT3x4:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT4x2:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE_MAT4x3:
        return GL_DOUBLE;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Unknown vertex component type" );
    }
}

}

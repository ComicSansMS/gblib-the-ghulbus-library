/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/program.hpp>
#include <gbGraphics/diagnostic_OGL.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/shader.hpp>
#include <gbGraphics/shadow_state_OGL.hpp>
#include <gbGraphics/texture2d.hpp>
#include <gbGraphics/uniform_data.hpp>

#include <diagnostic/diagnostic.hpp>

#include <algorithm>

namespace GB_GRAPHICS_NAMESPACE {

gbProgram_OGL::Shader_T::Shader_T()
    :m_ProgramId(0), m_Shader(nullptr)
{
}

gbProgram_OGL::Shader_T::Shader_T(gbShaderPtr& shader, GLuint program_id)
    :m_ProgramId(program_id)
{
    ErrorMonitor_OGL monitor;
    try {
        m_Shader = dynamic_cast<gbShader_OGL*>(shader.get());
    } catch(std::bad_cast&) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                "Only OpenGL shaders can be bound to OpenGL programs" );
    }
    m_Storage = shader;
    glAttachShader(m_ProgramId, m_Shader->GetOGLShaderId_OGL());
}

gbProgram_OGL::Shader_T::Shader_T(gbShaderPtr&& shader, GLuint program_id)
    :m_ProgramId(program_id)
{
    ErrorMonitor_OGL monitor;
    try {
        m_Shader = dynamic_cast<gbShader_OGL*>(shader.get());
    } catch(std::bad_cast&) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                "Only OpenGL shaders can be bound to OpenGL programs" );
    }
    m_Storage.swap(shader);
    glAttachShader(m_ProgramId, m_Shader->GetOGLShaderId_OGL());
}

gbProgram_OGL::Shader_T::~Shader_T()
{
    ErrorMonitor_OGL monitor;
    if(m_Shader) {
        glDetachShader(m_ProgramId, m_Shader->GetOGLShaderId_OGL());
    }
}

gbProgram_OGL::Shader_T::Shader_T(Shader_T&& rhs)
    :m_ProgramId(rhs.m_ProgramId), m_Shader(rhs.m_Shader)
{
    rhs.m_Shader = nullptr;
    m_Storage.swap(rhs.m_Storage);
}

gbProgram_OGL::Shader_T& gbProgram_OGL::Shader_T::operator=(Shader_T&& rhs)
{
    if(this != &rhs) {
        ErrorMonitor_OGL monitor;
        if(m_Shader) {
            glDetachShader(m_ProgramId, m_Shader->GetOGLShaderId_OGL());
        }
        m_Shader = rhs.m_Shader;
        rhs.m_Shader = nullptr;
        m_Storage.swap(rhs.m_Storage);
        m_ProgramId = rhs.m_ProgramId;
    }
    return *this;
}

bool gbProgram_OGL::Shader_T::Equals(gbShaderPtr const& ptr) const
{
    return (ptr == m_Storage);
}


gbProgram_OGL::gbProgram_OGL(GLuint program_id, gbShadowState_OGL* shadow_state)
    :m_ProgramId(program_id), m_IsFinalized(false), m_glShadowState(shadow_state)
{
    ReleaseAllShaders();
}

gbProgram_OGL::~gbProgram_OGL()
{
    ErrorMonitor_OGL monitor;
    m_glShadowState->ReleaseProgram(m_ProgramId);
    // detach shaders before program delete
    m_Shaders.clear();
    glDeleteProgram(m_ProgramId);
}

void gbProgram_OGL::BindShader(gbShaderPtr& shader)
{
    m_Shaders.push_back(Shader_T(shader, m_ProgramId));
    m_IsFinalized = false;
}

void gbProgram_OGL::BindShader(gbShaderPtr&& shader)
{
    m_Shaders.push_back(Shader_T(std::move(shader), m_ProgramId));
    m_IsFinalized = false;
}

void gbProgram_OGL::ReleaseShader(gbShaderPtr const& shader)
{
    m_IsFinalized = false;
    m_Shaders.erase(
        std::remove_if(m_Shaders.begin(), m_Shaders.end(), [shader](Shader_T const& s) -> bool {
            return s.Equals(shader);
        }), m_Shaders.end());
}

void gbProgram_OGL::ReleaseAllShaders()
{
    m_IsFinalized = false;
    m_Shaders.clear();
}

void gbProgram_OGL::Finalize()
{
    ErrorMonitor_OGL monitor;
    glLinkProgram(m_ProgramId);
    GLint res;
    glGetProgramiv(m_ProgramId, GL_LINK_STATUS, &res);
    if(res == GL_FALSE) {
        GLint len;
        glGetProgramiv(m_ProgramId, GL_INFO_LOG_LENGTH, &len);
        std::vector<char> errmsg(len, '\0');
        GLsizei written;
        glGetProgramInfoLog(m_ProgramId, len, &written, errmsg.data());
        DIAG_THROW_EXCEPTION( Exceptions::ShaderCompileError(errmsg.data()),
                              "Error linking program" );
    }
    PerformShaderReflection();
    m_IsFinalized = true;
}

void gbProgram_OGL::Activate() const
{
    m_glShadowState->UseProgram(m_ProgramId);
    std::for_each(begin(m_Textures2D), end(m_Textures2D), [this](Texture2D_T const& tex) {
        m_glShadowState->BindTexture(tex.TextureUnit, tex.Texture->GetOGLTextureTarget_OGL(), tex.Texture->GetOGLTextureId_OGL());
    });
}

gbVertexFormatBase::ComponentType_T ConvertAttributeType(GLenum type)
{
    switch(type) {
    case GL_FLOAT:             return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT;
    case GL_FLOAT_VEC2:        return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT2;
    case GL_FLOAT_VEC3:        return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT3;
    case GL_FLOAT_VEC4:        return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT4;
    case GL_FLOAT_MAT2:        return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2;
    case GL_FLOAT_MAT3:        return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3;
    case GL_FLOAT_MAT4:        return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4;
    case GL_FLOAT_MAT2x3:      return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2x3;
    case GL_FLOAT_MAT2x4:      return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT2x4;
    case GL_FLOAT_MAT3x2:      return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3x2;
    case GL_FLOAT_MAT3x4:      return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT3x4;
    case GL_FLOAT_MAT4x2:      return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4x2;
    case GL_FLOAT_MAT4x3:      return gbVertexFormatBase::VERTEX_COMPONENT_FLOAT_MAT4x3;
    case GL_INT:               return gbVertexFormatBase::VERTEX_COMPONENT_INT;
    case GL_INT_VEC2:          return gbVertexFormatBase::VERTEX_COMPONENT_INT2;
    case GL_INT_VEC3:          return gbVertexFormatBase::VERTEX_COMPONENT_INT3;
    case GL_INT_VEC4:          return gbVertexFormatBase::VERTEX_COMPONENT_INT4;
    case GL_UNSIGNED_INT:      return gbVertexFormatBase::VERTEX_COMPONENT_UINT;
    case GL_UNSIGNED_INT_VEC2: return gbVertexFormatBase::VERTEX_COMPONENT_UINT2;
    case GL_UNSIGNED_INT_VEC3: return gbVertexFormatBase::VERTEX_COMPONENT_UINT3;
    case GL_UNSIGNED_INT_VEC4: return gbVertexFormatBase::VERTEX_COMPONENT_UINT4;
    /* OpenGL4 only
     *
    case DOUBLE:
    case DOUBLE_VEC2:
    case DOUBLE_VEC3:
    case DOUBLE_VEC4:
    case DOUBLE_MAT2:
    case DOUBLE_MAT3:
    case DOUBLE_MAT4:
    case DOUBLE_MAT2x3:
    case DOUBLE_MAT2x4:
    case DOUBLE_MAT3x2:
    case DOUBLE_MAT3x4:
    case DOUBLE_MAT4x2:
    case DOUBLE_MAT4x3:
    */
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Unknown OpenGL attribute type" );
    }
}

UniformData::UniformType_T ConvertUniformType(GLenum type)
{
    switch(type) {
    case GL_FLOAT:                   return UniformData::UNIFORM_FLOAT;
    case GL_FLOAT_VEC2:              return UniformData::UNIFORM_FLOAT2;
    case GL_FLOAT_VEC3:              return UniformData::UNIFORM_FLOAT3;
    case GL_FLOAT_VEC4:              return UniformData::UNIFORM_FLOAT4;
    case GL_INT:                     return UniformData::UNIFORM_INT;
    case GL_INT_VEC2:                return UniformData::UNIFORM_INT2;
    case GL_INT_VEC3:                return UniformData::UNIFORM_INT3;
    case GL_INT_VEC4:                return UniformData::UNIFORM_INT4;
    case GL_UNSIGNED_INT:            return UniformData::UNIFORM_UINT;
    case GL_UNSIGNED_INT_VEC2:       return UniformData::UNIFORM_UINT2;
    case GL_UNSIGNED_INT_VEC3:       return UniformData::UNIFORM_UINT3;
    case GL_UNSIGNED_INT_VEC4:       return UniformData::UNIFORM_UINT4;
    case GL_BOOL:                    return UniformData::UNIFORM_BOOL;
    case GL_BOOL_VEC2:               return UniformData::UNIFORM_BOOL2;
    case GL_BOOL_VEC3:               return UniformData::UNIFORM_BOOL3;
    case GL_BOOL_VEC4:               return UniformData::UNIFORM_BOOL4;
    case GL_FLOAT_MAT2:              return UniformData::UNIFORM_FLOAT_MAT2;
    case GL_FLOAT_MAT3:              return UniformData::UNIFORM_FLOAT_MAT3;
    case GL_FLOAT_MAT4:              return UniformData::UNIFORM_FLOAT_MAT4;
    case GL_SAMPLER_1D:              return UniformData::UNIFORM_TEXTURE_1D;
    case GL_SAMPLER_2D:              return UniformData::UNIFORM_TEXTURE_2D;
    case GL_SAMPLER_3D:              return UniformData::UNIFORM_TEXTURE_3D;
    case GL_SAMPLER_CUBE:            return UniformData::UNIFORM_TEXTURE_CUBEMAP;
    case GL_SAMPLER_1D_ARRAY:        return UniformData::UNIFORM_TEXTURE_1D_ARRAY;
    case GL_SAMPLER_2D_ARRAY:        return UniformData::UNIFORM_TEXTURE_2D_ARRAY;
    case GL_FLOAT_MAT2x3:
    case GL_FLOAT_MAT2x4:
    case GL_FLOAT_MAT3x2:
    case GL_FLOAT_MAT3x4:
    case GL_FLOAT_MAT4x2:
    case GL_FLOAT_MAT4x3:
    case GL_SAMPLER_1D_SHADOW:
    case GL_SAMPLER_2D_SHADOW:
    case GL_SAMPLER_1D_ARRAY_SHADOW:
    case GL_SAMPLER_2D_ARRAY_SHADOW:
    case GL_SAMPLER_2D_MULTISAMPLE:
    case GL_SAMPLER_2D_MULTISAMPLE_ARRAY:
    case GL_SAMPLER_CUBE_SHADOW:
    case GL_SAMPLER_BUFFER:
    case GL_SAMPLER_2D_RECT:
    case GL_SAMPLER_2D_RECT_SHADOW:
    case GL_INT_SAMPLER_1D:
    case GL_INT_SAMPLER_2D:
    case GL_INT_SAMPLER_3D:
    case GL_INT_SAMPLER_CUBE:
    case GL_INT_SAMPLER_1D_ARRAY:
    case GL_INT_SAMPLER_2D_ARRAY:
    case GL_INT_SAMPLER_2D_MULTISAMPLE:
    case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
    case GL_INT_SAMPLER_BUFFER:
    case GL_INT_SAMPLER_2D_RECT:
    case GL_UNSIGNED_INT_SAMPLER_1D:
    case GL_UNSIGNED_INT_SAMPLER_2D:
    case GL_UNSIGNED_INT_SAMPLER_3D:
    case GL_UNSIGNED_INT_SAMPLER_CUBE:
    case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE:
    case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY:
    case GL_UNSIGNED_INT_SAMPLER_BUFFER:
    case GL_UNSIGNED_INT_SAMPLER_2D_RECT:
        return UniformData::UNIFORM_NO_TYPE;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Unknown OpenGL uniform type" );
    }
}

void gbProgram_OGL::PerformShaderReflection()
{
    ErrorMonitor_OGL monitor;
    m_VertexAttrInfo.clear();
    m_VertexAttrByName.clear();
    GLint number_attributes;
    glGetProgramiv(m_ProgramId, GL_ACTIVE_ATTRIBUTES, &number_attributes);
    m_VertexAttrInfo.reserve(number_attributes);
    GLint buffer_length;
    glGetProgramiv(m_ProgramId, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &buffer_length);
    DIAG_ASSERT(buffer_length > 0);
    std::vector<char> buffer(buffer_length, '\0');
    for(int i=0; i<number_attributes; ++i) {
        GLint out_size;
        GLenum out_type;
        glGetActiveAttrib(m_ProgramId, i, buffer_length, nullptr, &out_size, &out_type, buffer.data());
        VertexAttributeInfo info;
        info.LocationIndex = glGetAttribLocation(m_ProgramId, buffer.data());
        info.Name = buffer.data();
        info.Type = ConvertAttributeType(out_type);
        DIAG_ASSERT(out_size == 1);        /// @todo
        m_VertexAttrInfo.push_back(std::move(info));
        m_VertexAttrByName[m_VertexAttrInfo.back().Name] = static_cast<int>(m_VertexAttrInfo.size() - 1);
    }

    GLint number_uniforms;
    glGetProgramiv(m_ProgramId, GL_ACTIVE_UNIFORMS, &number_uniforms);
    if(number_uniforms > 0) {
        m_UniformInfo.reserve(number_uniforms);
        glGetProgramiv(m_ProgramId, GL_ACTIVE_UNIFORM_MAX_LENGTH, &buffer_length);
        DIAG_ASSERT(buffer_length > 0);
        buffer.resize(buffer_length, '\0');
        for(int i=0; i < number_uniforms; ++i) {
            GLint out_size;
            GLenum out_type;
            glGetActiveUniform(m_ProgramId, i, buffer_length, nullptr, &out_size, &out_type, buffer.data());
            UniformInfo info;
            info.LocationIndex = glGetUniformLocation(m_ProgramId, buffer.data());
            info.Type = ConvertUniformType(out_type);
            info.Name = buffer.data();
            DIAG_ASSERT(out_size == 1);        /// @todo arrays
            m_UniformInfo.push_back(std::move(info));
            m_UniformByName[m_UniformInfo.back().Name] = static_cast<int>(m_UniformInfo.size() - 1);
        }
    }
}

bool gbProgram_OGL::IsFinalized() const
{
    return m_IsFinalized;
}

int gbProgram_OGL::NumberOfVertexAttributes() const
{
    if(!m_IsFinalized) {
        return -1;
    }
    return static_cast<int>(m_VertexAttrInfo.size());
}

gbProgram::VertexAttributeInfo const& gbProgram_OGL::GetVertexAttributeInfo(int i) const
{
    if(!m_IsFinalized) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Vertex attribute info only available for finalized programs" );
    }
    if((i < 0) || (i >= static_cast<int>(m_VertexAttrInfo.size()))) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Attribute index out of bounds" );
    }
    return m_VertexAttrInfo[i];
}

int gbProgram_OGL::GetVertexAttributeIndex(std::string const& attribute_name) const
{
    if(!m_IsFinalized) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Vertex attribute info only available for finalized programs" );
    }
    auto it = m_VertexAttrByName.find(attribute_name);
    return (it == m_VertexAttrByName.end()) ? (-1) : (it->second);
}

int gbProgram_OGL::NumberOfUniforms() const
{
    if(!m_IsFinalized) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Uniform info only available for finalized programs" );
    }
    return static_cast<int>(m_UniformInfo.size());
}

gbProgram::UniformInfo const& gbProgram_OGL::GetUniformInfo(int i) const
{
    if(!m_IsFinalized) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Uniform info only available for finalized programs" );
    }
    if((i < 0) || (i >= static_cast<int>(m_UniformInfo.size()))) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Uniform index out of bounds" );
    }
    return m_UniformInfo[i];
}

int gbProgram_OGL::GetUniformIndex(std::string const& uniform_name) const
{
    if(!m_IsFinalized) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Uniform info only available for finalized programs" );
    }
    auto it = m_UniformByName.find(uniform_name);
    return (it == m_UniformByName.end()) ? (-1) : (it->second);
}

inline void BindPreamble(std::vector<gbProgram::UniformInfo> const& uniform_info_vec, int uniform_index, UniformData::UniformType_T data_type, gbShadowState_OGL* glShadowState, GLuint program_id)
{
    if((uniform_index < 0) || (uniform_index >= static_cast<int>(uniform_info_vec.size()))) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid uniform index" );
    }
    gbProgram::UniformInfo const& uniform_info = uniform_info_vec[uniform_index];
    if(uniform_info.Type != data_type) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Uniform type mismatch" );
    }
    glShadowState->UseProgram(program_id);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Float const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform1f(m_UniformInfo[uniform_index].LocationIndex, data.f_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Float2 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform2f(m_UniformInfo[uniform_index].LocationIndex, data.f1_, data.f2_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Float3 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform3f(m_UniformInfo[uniform_index].LocationIndex, data.f1_, data.f2_, data.f3_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Float4 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform4f(m_UniformInfo[uniform_index].LocationIndex, data.f1_, data.f2_, data.f3_, data.f4_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Int const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform1i(m_UniformInfo[uniform_index].LocationIndex, data.i_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Int2 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform2i(m_UniformInfo[uniform_index].LocationIndex, data.i1_, data.i2_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Int3 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform3i(m_UniformInfo[uniform_index].LocationIndex, data.i1_, data.i2_, data.i3_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Int4 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform4i(m_UniformInfo[uniform_index].LocationIndex, data.i1_, data.i2_, data.i3_, data.i4_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Uint const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform1ui(m_UniformInfo[uniform_index].LocationIndex, data.i_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Uint2 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform2ui(m_UniformInfo[uniform_index].LocationIndex, data.i1_, data.i2_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Uint3 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform3ui(m_UniformInfo[uniform_index].LocationIndex, data.i1_, data.i2_, data.i3_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Uint4 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform4ui(m_UniformInfo[uniform_index].LocationIndex, data.i1_, data.i2_, data.i3_, data.i4_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Bool const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform1i(m_UniformInfo[uniform_index].LocationIndex, data.b_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Bool2 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform2i(m_UniformInfo[uniform_index].LocationIndex, data.b1_, data.b2_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Bool3 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform3i(m_UniformInfo[uniform_index].LocationIndex, data.b1_, data.b2_, data.b3_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Bool4 const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniform4i(m_UniformInfo[uniform_index].LocationIndex, data.b1_, data.b2_, data.b3_, data.b4_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Mat2f const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniformMatrix2fv(m_UniformInfo[uniform_index].LocationIndex, 1, GL_TRUE, data.fv_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Mat3f const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniformMatrix3fv(m_UniformInfo[uniform_index].LocationIndex, 1, GL_TRUE, data.fv_);
}

void gbProgram_OGL::BindUniform(int uniform_index, UniformData::Mat4f const& data)
{
    BindPreamble(m_UniformInfo, uniform_index, data.Type, m_glShadowState, m_ProgramId);
    glUniformMatrix4fv(m_UniformInfo[uniform_index].LocationIndex, 1, GL_TRUE, data.fv_);
}

void gbProgram_OGL::BindUniform(int uniform_index, gbTexture2DPtr& texture)
{
    BindPreamble(m_UniformInfo, uniform_index, UniformData::UNIFORM_TEXTURE_2D, m_glShadowState, m_ProgramId);
    // remove other textures already bound to this uniform
    m_Textures2D.erase(std::remove_if(m_Textures2D.begin(), m_Textures2D.end(),
                                      [uniform_index,&texture](Texture2D_T const& tex) -> bool {
                                          return (tex.uniform_index == uniform_index) && (tex.Storage != texture);
                                      }), m_Textures2D.end());
    // add new texture to program's texture list if it's not already there
    auto it_end = m_Textures2D.cend();
    auto it = std::find_if(m_Textures2D.cbegin(), it_end, [&texture](Texture2D_T const& tex) -> bool {
        return (tex.Storage == texture);
    });
    if(it == it_end) {
        Texture2D_T tex;
        tex.Storage = texture;
        tex.Texture = dynamic_cast<gbTexture2D_OGL*>(texture.get());
        tex.TextureUnit = m_glShadowState->AssignTextureUnit(tex.Texture->GetOGLTextureId_OGL(), m_ProgramId);
        tex.uniform_index = uniform_index;
        m_Textures2D.push_back(tex);
        it = --(m_Textures2D.end());
    }
    // bind new texture
    glUniform1i(m_UniformInfo[uniform_index].LocationIndex, it->TextureUnit);
}

GLuint gbProgram_OGL::GetOGLProgramId_OGL() const
{
    return m_ProgramId;
}


}

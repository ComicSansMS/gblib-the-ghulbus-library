/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/color.hpp>

#include <cstring>

namespace GB_GRAPHICS_NAMESPACE {

    namespace gbColor {
        ColorScheme ACTIVE_SCHEME = {
            /* Mask */
            {
                // R
                0x000000FF,
                // G
                0x0000FF00,
                // B
                0x00FF0000,
                // A
                0xFF000000
            },
            /* Offset */
            {
                // R
                0,
                // G
                8,
                // B
                16,
                // A
                24
            }
        };

        void SetActiveColorScheme(ColorScheme const& scheme)
        {
            std::memcpy(&ACTIVE_SCHEME, &scheme, sizeof(ColorScheme));
        }
    }

}


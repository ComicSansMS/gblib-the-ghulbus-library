/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/mesh.hpp>

#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/geometry.hpp>
#include <gbGraphics/index_buffer.hpp>
#include <gbGraphics/index_data_base.hpp>
#include <gbGraphics/vertex_buffer.hpp>
#include <gbGraphics/vertex_data_base.hpp>

#include <diagnostic/diagnostic.hpp>


namespace GB_GRAPHICS_NAMESPACE {

GB_MATH_NAMESPACE::AABB<float> CalculateBoundingBox(gbVertexDataBase const& vertex_data);

gbMesh::gbMesh(gbDrawingContext3D& dc3d, gbVertexDataBase const& vertex_data, gbIndexDataBase const& index_data, gbVertexBinding const& binding)
    :m_Context(&dc3d), m_Bounding(CalculateBoundingBox(vertex_data))
{
    Init(vertex_data, index_data, binding);
}

gbMesh::gbMesh(gbDrawingContext3D& dc3d, gbVertexDataBase const& vertex_data, gbIndexDataBase const& index_data, gbVertexBinding const& binding, GB_MATH_NAMESPACE::AABB<float> bounding_box)
    :m_Context(&dc3d), m_Bounding(bounding_box)
{
    Init(vertex_data, index_data, binding);
}

gbMesh::~gbMesh()
{
}

gbMesh::gbMesh(gbMesh&& rhs)
    :m_Context(rhs.m_Context), m_VertexBuffer(std::move(rhs.m_VertexBuffer)), 
     m_IndexBuffer(std::move(rhs.m_IndexBuffer)), m_Geometry(std::move(rhs.m_Geometry)),
     m_Bounding(rhs.m_Bounding)
{
}

void gbMesh::Init(gbVertexDataBase const& vertex_data, gbIndexDataBase const& index_data, gbVertexBinding const& binding)
{
    m_VertexBuffer = m_Context->CreateVertexBuffer(vertex_data.GetRawSize());
    m_VertexBuffer->SetVertexData(vertex_data);

    m_IndexBuffer = m_Context->CreateIndexBuffer(index_data.GetRawSize());
    m_IndexBuffer->SetIndexData(index_data);

    m_Geometry = m_VertexBuffer->CreateGeometry(binding, *m_IndexBuffer);
}

void gbMesh::Render()
{
    m_Context->Draw(*m_Geometry);
}

GB_MATH_NAMESPACE::AABB<float> const& gbMesh::GetBoundingBox() const
{
    return m_Bounding;
}

int GetPositionComponentIndex(gbVertexFormatBase const& format)
{
    int const n_components = format.NumberOfComponents();
    for(int i=0; i<n_components; ++i) {
        if(format.GetComponentSemantics(i) == gbVertexFormatBase::COMPONENT_SEMANTICS_POSITION) {
            return i;
        }
    }
    DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "No position component in vertex format");
}

inline GB_MATH_NAMESPACE::AABB<float> CalculateBoundingBox_Float(char const* data, int const n_vertices, int const stride)
{
    using namespace GB_MATH_NAMESPACE;
    AABB<float> ret;
    for(int i=0; i<n_vertices; ++i) {
        Vector3f v(reinterpret_cast<float const*>(data));
        ret.UnionSelf(v);
        data += stride;
    }
    return ret;
}

inline GB_MATH_NAMESPACE::AABB<float> CalculateBoundingBox_Double(char const* data, int const n_vertices, int const stride)
{
    using namespace GB_MATH_NAMESPACE;
    AABB<float> ret;
    for(int i=0; i<n_vertices; ++i) {
        Vector3d v(reinterpret_cast<double const*>(data));
        ret.UnionSelf(Vector3f(v));
        data += stride;
    }
    return ret;
}

GB_MATH_NAMESPACE::AABB<float> CalculateBoundingBox(gbVertexDataBase const& vertex_data)
{
    auto const& vertex_format = vertex_data.GetVertexFormat();
    int const position_idx = GetPositionComponentIndex(vertex_format);
    auto const vertex_data_type = vertex_format.GetComponentType(position_idx);
    int const vertex_data_offset = vertex_format.ComponentRawOffset(position_idx);
    int const vertex_data_stride = vertex_format.GetStride();
    char const* data = reinterpret_cast<char const*>(vertex_data.GetRawData()) + vertex_data_offset;
    GB_MATH_NAMESPACE::AABB<float> ret;
    int const n_vertices = vertex_data.NumberOfVertices();
    switch(vertex_data_type) {
    case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT3:
        case gbVertexFormatBase::VERTEX_COMPONENT_FLOAT4:
        return CalculateBoundingBox_Float(data, n_vertices, vertex_data_stride);
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE3:
    case gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE4:
        return CalculateBoundingBox_Double(data, n_vertices, vertex_data_stride);
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Invalid mesh position layout type; Please provide the bounding volume manually" );
    }
}

}

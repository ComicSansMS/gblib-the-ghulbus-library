/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/texture_atlas.hpp>

#include <gbGraphics/draw2d.hpp>
#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/lockable_surface.hpp>
#include <gbGraphics/rect.hpp>
#include <gbUtil/missing_features.hpp>

#include <diagnostic/diagnostic.hpp>

#include <algorithm>
#include <utility>

namespace GB_GRAPHICS_NAMESPACE {

    gbTextureAtlas::gbTextureAtlas(gbDrawingContext3D& dc3d, std::vector<ReadableSurface*> const& subtextures)
    {
        TextureAtlas::SlicedPacking packing;
        m_Subtextures.reserve(subtextures.size());
        std::for_each(begin(subtextures), end(subtextures), [this,&packing](ReadableSurface const* t) {
            m_Subtextures.push_back(Rect(0, 0, t->GetWidth(), t->GetHeight()));
            packing.AddRect(&(m_Subtextures.back()));
        });
        Rect const min_area = packing.DetermineBoundingArea();
        m_Texture = dc3d.CreateTexture(Texture::TextureDesc2D(min_area.width_, min_area.height_));
        InitAtlas(subtextures, &packing);
    }

    gbTextureAtlas::gbTextureAtlas(gbDrawingContext3D& dc3d, int width, int height, std::vector<ReadableSurface*> const& subtextures)
        :m_Texture(dc3d.CreateTexture(Texture::TextureDesc2D(width, height)))
    {
        TextureAtlas::SlicedPacking packing;
        m_Subtextures.reserve(subtextures.size());
        std::for_each(begin(subtextures), end(subtextures), [this,&packing](ReadableSurface const* t) {
            m_Subtextures.push_back(Rect(0, 0, t->GetWidth(), t->GetHeight()));
            packing.AddRect(&(m_Subtextures.back()));
        });
        InitAtlas(subtextures, &packing);
    }

    gbTextureAtlas::~gbTextureAtlas()
    {
    }

    void gbTextureAtlas::InitAtlas(std::vector<ReadableSurface*> const& subtextures, TextureAtlas::PackingStrategy* packing)
    {
        auto remains = packing->CalculatePacking(Rect(0, 0, m_Texture->GetWidth(), m_Texture->GetHeight()));
        if(!remains.empty()) {
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(),
                                 "Given texture atlas too small to encompass all subtextures");
        }

        for(int i=0,size=static_cast<int>(subtextures.size()); i<size; ++i) {
            m_Texture->SetTextureSubData(m_Subtextures[i], *(subtextures[i]));
        }
    }

    gbTexture2DPtr& gbTextureAtlas::GetTexture()
    {
        return m_Texture;
    }

    int gbTextureAtlas::NumberOfSubtextures() const
    {
        return static_cast<int>(m_Subtextures.size());
    }

    Rect const& gbTextureAtlas::GetSubtextureRect(int idx) const
    {
        if((idx < 0) || (idx >= static_cast<int>(m_Subtextures.size()))) {
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Index out of bounds");
        }
        return m_Subtextures[idx];
    }

    namespace TextureAtlas {
        SlicedPacking::SlicedPacking()
            :m_Root(std::make_unique<Slice>())
        {
        }

        SlicedPacking::~SlicedPacking()
        {
        }

        void SlicedPacking::AddRect(Rect* r)
        {
            m_Rects.push_back(r);
        }

        /** For a given node in the slice tree, calculate the bounding rectangle of its left child
         * @param[in] s Current node; must have a left child node
         * @param[in] slice_rect Bounding rectangle of s
         * @return The bounding rectangle of the left child of s
         */
        Rect GetLeftChildRect(SlicedPacking::Slice const& s, Rect const& slice_rect)
        {
            DIAG_ASSERT(s.Left);
            Rect ret(slice_rect);
            switch(s.Direction)
            {
            case SlicedPacking::Slice::SLICE_VERTICAL:
                ret.width_ = s.Position - slice_rect.left_;
                break;
            case SlicedPacking::Slice::SLICE_HORIZONTAL:
                ret.height_ = s.Position - slice_rect.top_;
                break;
            default:
                DIAG_ASSERT(false);
            }
            DIAG_ASSERT((ret.left_ >= 0) && (ret.top_ >= 0) && (ret.width_ >= 0) && (ret.height_ >= 0));
            return ret;
        }

        /** For a given node in the slice tree, calculate the bounding rectangle of its right child
         * @param[in] s Current node; must have a right child node
         * @param[in] slice_rect Bounding rectangle of s
         * @return The bounding rectangle of the right child of s
         */
        Rect GetRightChildRect(SlicedPacking::Slice const& s, Rect const& slice_rect)
        {
            DIAG_ASSERT(s.Right);
            Rect ret(slice_rect);
            switch(s.Direction)
            {
            case SlicedPacking::Slice::SLICE_VERTICAL:
                ret.left_ = s.Position;
                ret.width_ -= s.Position - slice_rect.left_;
                break;
            case SlicedPacking::Slice::SLICE_HORIZONTAL:
                ret.top_ = s.Position;
                ret.height_ -= s.Position - slice_rect.top_;
                break;
            default:
                DIAG_ASSERT(false);
            }
            DIAG_ASSERT((ret.left_ >= 0) && (ret.top_ >= 0) && (ret.width_ >= 0) && (ret.height_ >= 0));
            return ret;
        }

        /** Insert a new rectangle at the given node in the slice tree
         * @param[in,out] r Rectangle that is to be inserted; the new position of the rectangle will
         *                  be written to its left_ and top_ fields
         * @param[in] s Current node; must not have any children
         * @param[in] slice_rect Bounding rectangle of s; must be large enough to encompass r
         */
        void InsertAtNode(Rect& r, SlicedPacking::Slice& s, Rect const& slice_rect)
        {
            DIAG_ASSERT((!s.Left) && (!s.Right));
            DIAG_ASSERT((r.width_ <= slice_rect.width_) && (r.height_ <= slice_rect.height_));
            r.left_ = slice_rect.left_;
            r.top_ = slice_rect.top_;
            int dWidth = slice_rect.width_ - r.width_;
            int dHeight = slice_rect.height_ - r.height_;
            // determine preferred slicing order
            if((dWidth == 0) && (dHeight == 0))
            {
                s.Direction = SlicedPacking::Slice::SLICE_LEAF;
            }
            else if(dWidth > dHeight)
            {
                s.Direction = SlicedPacking::Slice::SLICE_VERTICAL;
                s.Position = r.left_ + r.width_;
                s.Left = std::make_unique<SlicedPacking::Slice>();
                s.Right = std::make_unique<SlicedPacking::Slice>();
                s.Left->Direction = SlicedPacking::Slice::SLICE_HORIZONTAL;
                s.Left->Position = r.top_ + r.height_;
                s.Left->Left = std::make_unique<SlicedPacking::Slice>();
                s.Left->Right = std::make_unique<SlicedPacking::Slice>();
                s.Left->Left->Direction = SlicedPacking::Slice::SLICE_LEAF;
            }
            else
            {
                s.Direction = SlicedPacking::Slice::SLICE_HORIZONTAL;
                s.Position = r.top_ + r.height_;
                s.Left = std::make_unique<SlicedPacking::Slice>();
                s.Right = std::make_unique<SlicedPacking::Slice>();
                s.Left->Direction = SlicedPacking::Slice::SLICE_VERTICAL;
                s.Left->Position = r.left_ + r.width_;
                s.Left->Left = std::make_unique<SlicedPacking::Slice>();
                s.Left->Right = std::make_unique<SlicedPacking::Slice>();
                s.Left->Left->Direction = SlicedPacking::Slice::SLICE_LEAF;
            }
        }

        /** Insert a new rectangle into the slice tree
         * @param s Root node of the slice tree
         * @param[in,out] r Rectangle that is to be inserted; the new position of the rectangle will
         *                  be written to its left_ and top_ fields
         * @param[in] slice_rect Bounding rectangle of the slice tree
         * @return true if the rectangle could be inserted; false if the tree has no more room
         * @note This function is called recursively to traverse the tree
         */
        bool InsertRect(SlicedPacking::Slice& s, Rect& r, Rect const& slice_rect)
        {
            if( (r.width_ > slice_rect.width_) || (r.height_ > slice_rect.height_) ||
                (s.Direction == SlicedPacking::Slice::SLICE_LEAF) ) {
                //current slice too small; backtrack
                return false;
            }
            if((!s.Left) && (!s.Right)) {
                // no children; insert here
                InsertAtNode(r, s, slice_rect);
                return true;
            }
            // try inserting in left subtree
            if(InsertRect(*s.Left, r, GetLeftChildRect(s, slice_rect))) {
                return true;
            }
            // try inserting in right subtree
            if(InsertRect(*s.Right, r, GetRightChildRect(s, slice_rect))) {
                return true;
            }
            // everything's full
            return false;
        }

        std::vector<Rect*> SlicedPacking::CalculatePacking(Rect const& target_area)
        {
            m_Root.reset(new Slice());
            // sort rects by area (largest to smallest)
            std::sort(begin(m_Rects), end(m_Rects), [](Rect* r1, Rect* r2) -> bool {
                return (r1->width_*r1->height_) > (r2->width_*r2->height_);
            });
            // pack em!
            std::vector<Rect*> ret;
            for(Rect* r : m_Rects) {
                if(!InsertRect(*m_Root, *r, target_area))
                {
                    ret.push_back(r);
                }
            }
            return ret;
        }

        Rect SlicedPacking::DetermineBoundingArea()
        {
            if(m_Rects.empty()) {
                return Rect();
            }
            // start with an area of minimum height (i.e. max height of subtextures)
            const int subtexture_max_height =
                (*std::max_element(begin(m_Rects), end(m_Rects), [](Rect* r1, Rect* r2) -> bool {
                    return r1->height_ < r2->height_;
                }))->height_;
            auto remains = CalculatePacking(Rect(0, 0, std::numeric_limits<int>::max(), subtexture_max_height));
            DIAG_ASSERT(remains.empty());

            // determine initial search parameters
            int subtexture_max_width = 0;        // max width of subtextures (which is also min width of bounding area)
            int max_width = 0;                    // max width of bounding area (i.e. width for packing of minimum height)
            int min_area = 0;                    // minimum total area (i.e. sum of areas of subtextures)
            for(auto it = cbegin(m_Rects), it_end = cend(m_Rects); it != it_end; ++it)
            {
                subtexture_max_width = std::max(subtexture_max_width, (*it)->width_);
                max_width = std::max(max_width, ((*it)->left_ + (*it)->width_));
                min_area += (*it)->width_ * (*it)->height_;
            }

            // gradually decrease width and increase height to determine minimum packing
            int it_height = subtexture_max_height;        //height iterator; increases
            int it_width = max_width;                    //width iterator; decreases
            int best_area = it_width * it_height;        //area of the smallest bounding found so far
            if(min_area == best_area) {
                //initial bounding is already minimum; return
                return Rect(0, 0, it_width, subtexture_max_height);
            }
            --it_width;
            Rect ret(0, 0, it_width, it_height);
            while(it_width >= subtexture_max_width) {
                if((it_width * it_height) < min_area) {
                    // smallest possible packing found; return immediately
                    return ret;
                } else if((it_width * it_height) < min_area) {
                    // enclosing area smaller than sum of subtexture areas; infeasible
                    ++it_height;
                } else {
                    // try packing
                    remains = CalculatePacking(Rect(0, 0, it_width, it_height));
                    if(remains.empty()) {
                        // found a packing
                        if(it_width * it_height < best_area) {
                            // found a new minimum
                            best_area = it_width * it_height;
                            ret.width_ = it_width;
                            ret.height_ = it_height;
                        }
                        // try smaller width next time
                        --it_width;
                    } else {
                        // no packing found; area to small
                        ++it_height;
                    }
                }
            }
            return ret;
        }

        void PaintTree_rec(GhulbusGraphics::gbDraw2D& draw2d, SlicedPacking::Slice* slice, Rect const& rect)
        {
            if((!slice) || (slice->Direction == SlicedPacking::Slice::SLICE_LEAF)) {
                return;
            }
            if(slice->Direction == SlicedPacking::Slice::SLICE_VERTICAL) {
                draw2d.VLine(slice->Position, rect.top_, rect.top_+rect.height_, gbColor::XRGB(255, 255, 255));
                PaintTree_rec(draw2d, slice->Left.get(), Rect(rect.left_, rect.top_, slice->Position-rect.left_, rect.height_));
                PaintTree_rec(draw2d, slice->Right.get(), Rect(slice->Position, rect.top_, rect.left_+rect.width_-slice->Position, rect.height_));
            } else {
                draw2d.HLine(rect.left_, rect.left_+rect.width_, slice->Position, gbColor::XRGB(255, 255, 255));
                PaintTree_rec(draw2d, slice->Left.get(), Rect(rect.left_, rect.top_, rect.width_, slice->Position-rect.top_));
                PaintTree_rec(draw2d, slice->Right.get(), Rect(rect.left_, slice->Position, rect.width_, rect.top_+rect.height_-slice->Position));
            }
        }

        void SlicedPacking::PaintTree(gbDraw2D& draw2d) const
        {
            PaintTree_rec(draw2d, m_Root.get(), Rect(0, 0, draw2d.GetWidth(), draw2d.GetHeight()));
        }
    }
}

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/event_manager.hpp>

#include <diagnostic/diagnostic.hpp>

#include <algorithm>
#include <cstring>

namespace GB_GRAPHICS_NAMESPACE {

/* statics */
template<> gbEventManager::HandlerId gbEventManager::RegisteredMouseInput::next_id = 1;
template<> gbEventManager::HandlerId gbEventManager::RegisteredKeyboardInput::next_id = 1;
template<> gbEventManager::HandlerId gbEventManager::RegisteredWindowEvent::next_id = 1;

gbEventManager::gbEventManager()
{
    ClearState();
}

gbEventManager::~gbEventManager()
{
}

void gbEventManager::ClearState()
{
    std::memset(&m_MouseState, 0, sizeof(m_MouseState));
    std::memset(&m_KeyboardState, 0, sizeof(m_KeyboardState));
    std::memset(&m_Timestamps, 0, sizeof(m_Timestamps));

    m_Timer.Mark();
}

GB_UTIL_NAMESPACE::Timestamp gbEventManager::GetTimestamp() const
{
    return m_Timer.GetTimestamp();
}

auto gbEventManager::GetHandlerList_MouseInput(Events::gbEvent_Type_Mouse type)
    -> std::vector<RegisteredMouseInput>&
{
    switch(type) {
    case Events::GBEVENT_MOUSE_ALL:
        return m_InputHandlerMouse_All;
    case Events::GBEVENT_MOUSE_BUTTONDOWN:
        return m_InputHandlerMouse_ButtonDown;
    case Events::GBEVENT_MOUSE_BUTTONUP:
        return m_InputHandlerMouse_ButtonUp;
    case Events::GBEVENT_MOUSE_MOUSEWHEEL:
        return m_InputHandlerMouse_MouseWheel;
    case Events::GBEVENT_MOUSE_MOVE:
        return m_InputHandlerMouse_Move;
    case Events::GBEVENT_MOUSE_MOVE_REL:
        return m_InputHandlerMouse_MoveRel;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid event type for Mouse Input handler" );
    }

}

gbEventManager::HandlerId gbEventManager::AddEventHandler_MouseInput(Events::MouseInputHandler const& handler,
                                                                     Events::gbEvent_Type_Mouse type)
{
    std::vector<RegisteredMouseInput>& handler_list = GetHandlerList_MouseInput(type);
    handler_list.emplace_back(handler);
    return handler_list.back().getId();
}

void gbEventManager::RemoveEventHandler_MouseInput(HandlerId id)
{
    auto remove_from_list = [id](std::vector<RegisteredMouseInput>& v) {
        v.erase(std::remove_if(begin(v), end(v),
                               [id](RegisteredMouseInput const& entry) -> bool { return entry.getId() == id; }),
                end(v));
    };
    remove_from_list(GetHandlerList_MouseInput(Events::GBEVENT_MOUSE_ALL));
    remove_from_list(GetHandlerList_MouseInput(Events::GBEVENT_MOUSE_BUTTONDOWN));
    remove_from_list(GetHandlerList_MouseInput(Events::GBEVENT_MOUSE_BUTTONUP));
    remove_from_list(GetHandlerList_MouseInput(Events::GBEVENT_MOUSE_MOUSEWHEEL));
    remove_from_list(GetHandlerList_MouseInput(Events::GBEVENT_MOUSE_MOVE));
    remove_from_list(GetHandlerList_MouseInput(Events::GBEVENT_MOUSE_MOVE_REL));
}

auto gbEventManager::AddEventHandler_MouseInput(Events::MouseInput* handler, Events::gbEvent_Type_Mouse type)
    -> HandlerId
{
    return AddEventHandler_MouseInput(
        [handler](Events::MouseState const& state, Events::gbEvent_Type_Mouse type) -> Events::EventHandler_Return {
            return (handler->Handler(&state, type)) ? Events::GBHANDLER_SWALLOW_EVENT : Events::GBHANDLER_FORWARD_EVENT;
        }, type);
}

auto gbEventManager::GetHandlerList_KeyboardInput(Events::gbEvent_Type_Keyboard type)
    -> std::vector<RegisteredKeyboardInput>&
{
    switch(type)
    {
    case Events::GBEVENT_KEYB_ALL:
        return m_InputHandlerKeyboard_All;
    case Events::GBEVENT_KEYB_KEYDOWN:
        return m_InputHandlerKeyboard_KeyDown;
    case Events::GBEVENT_KEYB_KEYUP:
        return m_InputHandlerKeyboard_KeyUp;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid event type for Keyboard Input handler" );
    }
}

gbEventManager::HandlerId gbEventManager::AddEventHandler_KeyboardInput(Events::KeyboardInputHandler const& handler,
                                                                        Events::gbEvent_Type_Keyboard type)
{
    std::vector<RegisteredKeyboardInput>& handler_list = GetHandlerList_KeyboardInput(type);
    handler_list.emplace_back(handler);
    return handler_list.back().getId();
}

auto gbEventManager::AddEventHandler_KeyboardInput(Events::KeyboardInput* handler, Events::gbEvent_Type_Keyboard type)
    -> HandlerId
{
    return AddEventHandler_KeyboardInput(
        [handler](Events::Key k, Events::gbEvent_Type_Keyboard type) -> Events::EventHandler_Return {
            return (handler->Handler(k, type)) ? Events::GBHANDLER_SWALLOW_EVENT : Events::GBHANDLER_FORWARD_EVENT;
        }, type);
}

void gbEventManager::RemoveEventHandler_KeyboardInput(HandlerId id)
{
    auto remove_from_list = [id](std::vector<RegisteredKeyboardInput>& v) {
        v.erase(std::remove_if(begin(v), end(v),
                               [id](RegisteredKeyboardInput const& entry) -> bool { return entry.getId() == id; }),
                end(v));
    };
    remove_from_list(GetHandlerList_KeyboardInput(Events::GBEVENT_KEYB_ALL));
    remove_from_list(GetHandlerList_KeyboardInput(Events::GBEVENT_KEYB_KEYDOWN));
    remove_from_list(GetHandlerList_KeyboardInput(Events::GBEVENT_KEYB_KEYUP));
}

gbEventManager::HandlerId gbEventManager::AddEventHandler_WindowEvent(Events::WindowEventHandler const& handler)
{
    m_WindowEventHandler_All.emplace_back(handler);
    return m_WindowEventHandler_All.back().getId();
}

gbEventManager::HandlerId gbEventManager::AddEventHandler_WindowEvent(Events::WindowEvent* handler)
{
    return AddEventHandler_WindowEvent(
        [handler](Events::WindowState const& state, Events::gbEvent_Type_WindowEvent type)
        {
            handler->Handler(&state, type);
        });
}

void gbEventManager::RemoveEventHandler_WindowEvent(gbEventManager::HandlerId id)
{
    m_WindowEventHandler_All.erase( 
        std::remove_if(begin(m_WindowEventHandler_All), end(m_WindowEventHandler_All),
                       [id](RegisteredWindowEvent const& entry) -> bool { return entry.getId() == id; }),
        end(m_WindowEventHandler_All) );
}

Events::MouseState const* gbEventManager::QueryMouseState() const
{
    return &m_MouseState;
}

Events::KeyboardState const* gbEventManager::QueryKeyboardState() const
{
    return &m_KeyboardState;
}

bool gbEventManager::QueryKeyState(Events::Key k) const
{
    return m_KeyboardState.keys[k];
}

template<typename T_Handler, typename T_Payload, typename T_EventType>
bool InvokeEventHandlers_old(std::vector<T_Handler>& handler_list, T_Payload payload, T_EventType type)
{
    typename std::vector<T_Handler>::iterator it_end = handler_list.end();
    for(typename std::vector<T_Handler>::iterator it = handler_list.begin(); 
        it != it_end; 
        ++it)
    {
        if( (*it)->Handler(payload, type) ) { return true; }
    }
    return false;
}

template<typename T_Handler, typename T_Payload, typename T_EventType>
Events::EventHandler_Return InvokeEventHandlers(std::vector<T_Handler>& handler_list,
                                                T_Payload payload, T_EventType type)
{
    for(auto const& h : handler_list) {
        if(h.getHandler()(payload, type) == Events::GBHANDLER_SWALLOW_EVENT) { return Events::GBHANDLER_SWALLOW_EVENT; }
    }
    return Events::GBHANDLER_FORWARD_EVENT;
}

bool gbEventManager::SignalEvent_MouseMove(Events::MouseState* e)
{
    //update internal state and timestamps:
    m_MouseState.posx = e->posx;
    m_MouseState.posy = e->posy;
    e->timestamp = m_MouseState.timestamp = 
        m_Timestamps.mouse_move = m_Timer.PeekTime();

    if(InvokeEventHandlers(m_InputHandlerMouse_Move, *e, Events::GBEVENT_MOUSE_MOVE)) { return true; }
    if(InvokeEventHandlers(m_InputHandlerMouse_All, *e, Events::GBEVENT_MOUSE_MOVE)) { return true; }
    return false;
}

bool gbEventManager::SignalEvent_MouseMoveRel(Events::MouseState* e)
{
    if(InvokeEventHandlers(m_InputHandlerMouse_MoveRel, *e, Events::GBEVENT_MOUSE_MOVE_REL)) { return true; }
    if(InvokeEventHandlers(m_InputHandlerMouse_All, *e, Events::GBEVENT_MOUSE_MOVE_REL)) { return true; }
    return false;
}

bool gbEventManager::SignalEvent_MouseButtonDown(Events::MouseState* e)
{
    //update internal state and timestamps:
    m_MouseState.posx = e->posx;
    m_MouseState.posy = e->posy;
    e->timestamp = m_MouseState.timestamp = m_Timer.PeekTime();
    if(e->lbutton) {
        m_MouseState.lbutton = true;
        m_Timestamps.mouse_lbuttondown = m_MouseState.timestamp;
    } else if(e->rbutton) {
        m_MouseState.rbutton = true;
        m_Timestamps.mouse_rbuttondown = m_MouseState.timestamp;
    } else if(e->mbutton) {
        m_MouseState.mbutton = true;
        m_Timestamps.mouse_mbuttondown = m_MouseState.timestamp;
    } else {
        //no known button involved, don't call any handlers
        return true;
    }

    if(InvokeEventHandlers(m_InputHandlerMouse_ButtonDown, *e, Events::GBEVENT_MOUSE_BUTTONDOWN)) { return true; }
    if(InvokeEventHandlers(m_InputHandlerMouse_All, *e, Events::GBEVENT_MOUSE_BUTTONDOWN)) { return true; }
    return false;
}

bool gbEventManager::SignalEvent_MouseButtonUp(Events::MouseState* e)
{
    //update internal state and timestamps:
    m_MouseState.posx = e->posx;
    m_MouseState.posy = e->posy;
    e->timestamp = m_MouseState.timestamp = m_Timer.PeekTime();
    if(e->lbutton) {
        m_MouseState.lbutton = false;
        m_Timestamps.mouse_lbuttonup = m_MouseState.timestamp;
    } else if(e->rbutton) {
        m_MouseState.rbutton = false;
        m_Timestamps.mouse_rbuttonup = m_MouseState.timestamp;
    } else if(e->mbutton) {
        m_MouseState.mbutton = false;
        m_Timestamps.mouse_mbuttonup = m_MouseState.timestamp;
    } else {
        //no known button involved, don't call any handlers
        return true;
    }

    if(InvokeEventHandlers(m_InputHandlerMouse_ButtonUp, *e, Events::GBEVENT_MOUSE_BUTTONUP)) { return true; }
    if(InvokeEventHandlers(m_InputHandlerMouse_All, *e, Events::GBEVENT_MOUSE_BUTTONUP)) { return true; }
    return false;
}

bool gbEventManager::SignalEvent_MouseWheel(Events::MouseState* e)
{
    //set timestamps:
    e->timestamp = m_MouseState.timestamp =
        m_Timestamps.mouse_mousewheel = m_Timer.PeekTime();
    m_MouseState.mousewheel += e->mousewheel;

    if(InvokeEventHandlers(m_InputHandlerMouse_MouseWheel, *e, Events::GBEVENT_MOUSE_MOUSEWHEEL)) { return true; }
    if(InvokeEventHandlers(m_InputHandlerMouse_All, *e, Events::GBEVENT_MOUSE_MOUSEWHEEL)) { return true; }
    return false;
}

bool gbEventManager::SignalEvent_KeyboardKeyDown(Events::Key k)
{
    //update internal state and timestamps:
    if(k < Events::GBKEY_NUMBER_OF_KEYSTATES) {
        m_KeyboardState.keys[k] = true;
        m_KeyboardState.timestamp = m_Timestamps.keyboard[k] = m_Timer.PeekTime();
    } else {
        //unexpected keycode, do nothing
        return true;
    }

    if(InvokeEventHandlers(m_InputHandlerKeyboard_KeyDown, k, Events::GBEVENT_KEYB_KEYDOWN)) { return true; }
    if(InvokeEventHandlers(m_InputHandlerKeyboard_All, k, Events::GBEVENT_KEYB_KEYDOWN)) { return true; }
    return false;
}

bool gbEventManager::SignalEvent_KeyboardKeyUp(Events::Key k)
{
    //update internal state and timestamps:
    if(k < Events::GBKEY_NUMBER_OF_KEYSTATES) {
        m_KeyboardState.keys[k] = false;
        m_KeyboardState.timestamp = m_Timestamps.keyboard[k] = m_Timer.PeekTime();
    } else {
        //unexpected keycode, do nothing
        return true;
    }

    if(InvokeEventHandlers(m_InputHandlerKeyboard_KeyUp, k, Events::GBEVENT_KEYB_KEYUP)) { return true; }
    if(InvokeEventHandlers(m_InputHandlerKeyboard_All, k, Events::GBEVENT_KEYB_KEYUP)) { return true; }
    return false;
}

void gbEventManager::SignalEvent_WindowEvent(Events::gbEvent_Type_WindowEvent type, Events::WindowState const* w)
{
    for(auto const& handler : m_WindowEventHandler_All) {
        handler.getHandler()(*w, type);
    }
}

}


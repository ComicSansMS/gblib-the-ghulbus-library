/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/sprite_segmenter.hpp>
#include <gbGraphics/sprite.hpp>
#include <diagnostic/exception.hpp>

namespace GB_GRAPHICS_NAMESPACE {

gbSpriteSegmenter::gbSpriteSegmenter(gbSpritePtr& sprite_ptr)
    :m_Sprite(sprite_ptr), m_Grid(GB_MATH_NAMESPACE::Vector2i(-1, -1), -1, -1),
     m_nSegments(-1), m_nSegmentsPerRow(-1)
{
}

void gbSpriteSegmenter::Render()
{
    m_Sprite->RenderSegment(m_SrcRect, m_DstRect);
}

void gbSpriteSegmenter::SetSizeOnScreen(int width, int height)
{
    if((width < 0) || (height < 0)) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Sprite dimensions must not be negative");
    }
    m_DstRect.width_ = width;
    m_DstRect.height_ = height;
}

int gbSpriteSegmenter::GetWidthOnScreen() const
{
    return m_DstRect.width_;
}

int gbSpriteSegmenter::GetHeightOnScreen() const
{
    return m_DstRect.height_;
}

void gbSpriteSegmenter::SetPositionOnScreen(int x, int y)
{
    m_DstRect.left_ = x;
    m_DstRect.top_ = y;
}

int gbSpriteSegmenter::GetPositionOnScreenX() const
{
    return m_DstRect.left_;
}

int gbSpriteSegmenter::GetPositionOnScreenY() const
{
    return m_DstRect.top_;
}

void gbSpriteSegmenter::SetSourceRect(Rect const& rect)
{
    if((rect.width_ < 0) || (rect.height_ < 0)) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Sprite dimensions must not be negative");
    }
    m_SrcRect = rect;
}

void gbSpriteSegmenter::SetDestinationRect(Rect const& rect)
{
    if((rect.width_ < 0) || (rect.height_ < 0)) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Sprite dimensions must not be negative");
    }
    m_DstRect = rect;
}

void gbSpriteSegmenter::SetSegmentationGrid(int segments_per_row, int number_of_rows)
{
    if( (segments_per_row <= 0) || (m_Sprite->GetWidth() % segments_per_row != 0) ||
        (number_of_rows <= 0) || (m_Sprite->GetHeight() % number_of_rows != 0) )
    {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid segmentation grid (sprite dimension must be a multiple of grid cell dimension)" );
    }
    m_Grid.width_ = m_Sprite->GetWidth() / segments_per_row;
    m_Grid.height_ = m_Sprite->GetHeight() / number_of_rows;
    m_Grid.left_ = 0;
    m_Grid.top_ = 0;
    m_nSegmentsPerRow = segments_per_row;
    m_nSegments = segments_per_row * number_of_rows;
}

int gbSpriteSegmenter::GetNumberOfGridCells() const
{
    return m_nSegments;
}

void gbSpriteSegmenter::SetActiveGridCell(int i)
{
    if((i < 0) || (i >= m_nSegments)) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Cell index out of bounds");
    }
    div_t const tmp = std::div(i, m_nSegmentsPerRow);
    int const col_index = tmp.rem;
    int const row_index = tmp.quot;
    m_Grid.left_ = col_index;
    m_Grid.top_ = row_index;
    m_SrcRect.left_ = col_index * m_Grid.width_;
    m_SrcRect.top_ = row_index * m_Grid.height_;
    m_SrcRect.width_ = m_Grid.width_;
    m_SrcRect.height_ = m_Grid.height_;
}

int gbSpriteSegmenter::GetActiveGridCell() const
{
    return m_Grid.top_*m_nSegmentsPerRow + m_Grid.left_;
}

void gbSpriteSegmenter::AdvanceActiveGridCell()
{
    int const current_index = GetActiveGridCell();
    SetActiveGridCell((current_index == m_nSegments-1)?0:(current_index+1));
}

int gbSpriteSegmenter::GetCellWidth() const
{
    return m_Grid.width_;
}

int gbSpriteSegmenter::GetCellHeight() const
{
    return m_Grid.height_;
}

}

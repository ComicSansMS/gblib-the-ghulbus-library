/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/texture2d.hpp>
#include <gbGraphics/diagnostic_OGL.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/lockable_surface.hpp>
#include <gbGraphics/sampler_state.hpp>
#include <gbGraphics/shadow_state_OGL.hpp>

#include <gbUtil/missing_features.hpp>

#include <diagnostic/diagnostic.hpp>

#include <cstring>

namespace GB_GRAPHICS_NAMESPACE {

inline GLenum GetOGLTextureFormat(Texture::Format_T const& format)
{
    switch(format) {
    case Texture::TEXTURE_FORMAT_UINT_R8G8B8A8:
        return GL_RGBA;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Unknown texture format" );
    }
}

inline GLenum GetOGLTextureType(Texture::Format_T const& format)
{
    switch(format) {
    case Texture::TEXTURE_FORMAT_UINT_R8G8B8A8:
        return GL_UNSIGNED_INT_8_8_8_8_REV;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Unknown texture format" );
    }
}

gbTexture2D_OGL::gbTexture2D_OGL( GLuint texture_id, Texture::TextureDesc2D const& desc2d,
                                  gbSamplerStatePtr sampler_state, gbShadowState_OGL* shadow_state )
    :m_TextureId(texture_id), m_Width(desc2d.Width), m_Height(desc2d.Height),
     m_Target(GL_TEXTURE_2D), m_Format(GetOGLTextureFormat(desc2d.Format)), m_Type(GetOGLTextureType(desc2d.Format)),
     m_glShadowState(shadow_state)
{
    ErrorMonitor_OGL monitor;
    m_glShadowState->BindTexture(0, m_Target, m_TextureId);
    glTexImage2D(m_Target, 0, m_Format, m_Width, m_Height, 0, GL_RGBA, m_Type, nullptr);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
                              "Unable to allocate texture" );
    }

    BindSamplerState(sampler_state);
}

gbTexture2D_OGL::~gbTexture2D_OGL()
{
    ErrorMonitor_OGL monitor;
    m_glShadowState->ReleaseTexture(m_Target, m_TextureId);
    glDeleteTextures(1, &m_TextureId);
}

void gbTexture2D_OGL::Lock(GBCOLOR** data, int* pitch)
{
    LockWithOptions(data, pitch, Rect(0, 0, m_Width, m_Height), LOCK_OPTIONS_DEFAULT);
}

void gbTexture2D_OGL::LockRect(Rect const& rect, GBCOLOR** data, int* pitch)
{
    LockWithOptions(data, pitch, rect, LOCK_OPTIONS_DEFAULT);
}

void gbTexture2D_OGL::Unlock()
{
    if(!m_LockBuffer)
    {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Tried to unlock a texture that was not locked" );
    }
    ErrorMonitor_OGL monitor;
    m_glShadowState->BindTexture(0, m_Target, m_TextureId);
    glTexSubImage2D(m_Target, 0, m_LockedArea.left_, m_LockedArea.top_,
                    m_LockedArea.width_, m_LockedArea.height_, m_Format,
                    m_Type, m_LockBuffer.get());
    m_LockBuffer.reset();
}

bool gbTexture2D_OGL::IsLocked() const
{
    return static_cast<bool>(m_LockBuffer);
}

int gbTexture2D_OGL::GetWidth() const
{
    return m_Width;
}

int gbTexture2D_OGL::GetHeight() const
{
    return m_Height;
}

void gbTexture2D_OGL::LockWithOptions(GBCOLOR** data, int* pitch, Rect const& rect, LockOptions opt)
{
    if(m_LockBuffer)
    {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Tried to lock a texture that was already locked" );
    }
    if( (rect.top_ < 0) || (rect.left_ < 0) || (rect.width_ < 0) || (rect.height_ < 0) ||
        (rect.left_ + rect.width_ > m_Width) || (rect.top_ + rect.height_ > m_Height) )
    {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Invalid lock area specified");
    }
    m_LockBuffer.reset(new GBCOLOR[rect.width_ * rect.height_]);
    m_LockedArea = rect;
    if(opt != LOCK_OPTIONS_UNINITIALIZED) {
        std::memset(m_LockBuffer.get(), 0, rect.width_ * rect.height_ * sizeof(GBCOLOR));
    }
    *data = m_LockBuffer.get();
    *pitch = rect.width_;
}

void gbTexture2D_OGL::SetTextureData(ReadableSurface& image)
{
    SetTextureSubData(Rect(GB_MATH_NAMESPACE::Vector2i(0,0), image.GetWidth(), image.GetHeight()), image);
}

void gbTexture2D_OGL::SetTextureSubData(Rect const& dest, ReadableSurface& image)
{
    ErrorMonitor_OGL monitor;
    if( (dest.left_ < 0) || (dest.top_ < 0) ||
        (dest.left_ + dest.width_ > m_Width) || (dest.top_ + dest.height_ > m_Height) )
    {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Given target rectangle exceeds texture bounds" );
    }
    if((m_Format != GL_RGBA) || (m_Type != GL_UNSIGNED_INT_8_8_8_8_REV)) {
        ///@todo texture swizzling & manual format conversion on pre 3.3
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Texture format conversion not implemented" );
    }
    SurfaceLock<LockableSurface> lock(image);
    m_glShadowState->BindTexture(0, m_Target, m_TextureId);
    glTexSubImage2D(m_Target, 0, dest.left_, dest.top_, dest.width_, dest.height_, m_Format, m_Type, lock.GetLockedData());
}

GLfloat GetOGLMinFilter(Sampling::SamplingMode_T mode_min)
{
    switch(mode_min)
    {
    case Sampling::SAMPLE_POINT:
        return GL_NEAREST;
    case Sampling::SAMPLE_LINEAR:
        return GL_LINEAR;
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown texture min filter");
    }
}

GLfloat GetOGLMinFilterMipMapped(Sampling::SamplingMode_T mode_min, Sampling::SamplingMode_T mode_mip)
{
    switch(mode_min)
    {
    case Sampling::SAMPLE_POINT:
        switch(mode_mip)
        {
        case Sampling::SAMPLE_POINT:
            return GL_NEAREST_MIPMAP_NEAREST;
        case Sampling::SAMPLE_LINEAR:
            return GL_NEAREST_MIPMAP_LINEAR;
        default:
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown texture mip filter");
        }
    case Sampling::SAMPLE_LINEAR:
        switch(mode_mip)
        {
        case Sampling::SAMPLE_POINT:
            return GL_LINEAR_MIPMAP_NEAREST;
        case Sampling::SAMPLE_LINEAR:
            return GL_LINEAR_MIPMAP_LINEAR;
        default:
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown texture mip filter");
        }
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown texture min filter");
    }
}

GLfloat GetOGLMagFilter(Sampling::SamplingMode_T const& mode)
{
    switch(mode)
    {
    case Sampling::SAMPLE_POINT:
        return GL_NEAREST;
    case Sampling::SAMPLE_LINEAR:
        return GL_LINEAR;
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown texture mag filter");
    }
}

GLfloat GetOGLWrapMode(Sampling::AddressMode_T const& mode)
{
    switch(mode)
    {
    case Sampling::ADDRESS_MODE_WRAP:   return GL_REPEAT;
    case Sampling::ADDRESS_MODE_MIRROR: return GL_MIRRORED_REPEAT;
    case Sampling::ADDRESS_MODE_CLAMP:  return GL_CLAMP_TO_EDGE;
    case Sampling::ADDRESS_MODE_BORDER: return GL_CLAMP_TO_BORDER;
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown texture sampling address mode");
    }
}

void gbTexture2D_OGL::BindSamplerState(gbSamplerStatePtr sampler_state)
{
    if(!sampler_state) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Attempt to buind NULL sampler state to texture" );
    }
    ErrorMonitor_OGL monitor;
    m_glShadowState->BindTexture(0, m_Target, m_TextureId);
    m_SamplerState = sampler_state;
    auto const& desc = m_SamplerState->GetDescriptor();
    glTexParameterf(m_Target, GL_TEXTURE_MIN_FILTER, GetOGLMinFilter(desc.MinFilter));
    glTexParameterf(m_Target, GL_TEXTURE_MAG_FILTER, GetOGLMagFilter(desc.MagFilter));
    glTexParameterf(m_Target, GL_TEXTURE_WRAP_S, GetOGLWrapMode(desc.AddressU));
    glTexParameterf(m_Target, GL_TEXTURE_WRAP_T, GetOGLWrapMode(desc.AddressV));
    if( (desc.AddressU == Sampling::ADDRESS_MODE_BORDER) || (desc.AddressV == Sampling::ADDRESS_MODE_BORDER) ) {
        GLfloat const border_color[] = {
            static_cast<float>(gbColor::GetR(desc.BorderColor)) / 255.0f,
            static_cast<float>(gbColor::GetG(desc.BorderColor)) / 255.0f,
            static_cast<float>(gbColor::GetB(desc.BorderColor)) / 255.0f,
            static_cast<float>(gbColor::GetA(desc.BorderColor)) / 255.0f
        };
        glTexParameterfv(m_Target, GL_TEXTURE_BORDER_COLOR, border_color);
    }
}

gbSamplerStatePtr gbTexture2D_OGL::GetSamplerState() const
{
    return m_SamplerState;
}

GLuint gbTexture2D_OGL::GetOGLTextureId_OGL() const
{
    return m_TextureId;
}

GLenum gbTexture2D_OGL::GetOGLTextureTarget_OGL() const
{
    return m_Target;
}

}

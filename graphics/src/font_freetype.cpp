/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifdef GB_GRAPHICS_HAS_FREETYPE

#include <gbGraphics/font_freetype.hpp>
#include <diagnostic/exception.hpp>
#include <gbUtil/missing_features.hpp>

#include <functional>
#include <string>

#include <ft2build.h>
#include FT_FREETYPE_H

inline FT_UInt GetSystemDpi() {
#if defined _WIN32
    // Windows
    return 96;
#elif defined __APPLE__
    // Mac
    return 72;
#elif defined __linux__
    // X
    return 75;
#else
    // use freetype default (72 dpi)
    return 0;
#endif
}

namespace GB_GRAPHICS_NAMESPACE {
namespace Fonts {

struct FreetypeFont::Pimpl {
    std::string filename;
    FT_Library library;
    FT_Face face;
    typedef std::unique_ptr<FT_Library, std::function<void(FT_Library*)>> GuardLibrary_T;
    GuardLibrary_T guard_library;
    typedef std::unique_ptr<FT_Face, std::function<void(FT_Face*)>> GuardFace_T;
    GuardFace_T guard_face;
    FT_UInt prev_glyph;
    FT_Vector kerning;
};

FreetypeFont::FreetypeFont(std::string const& filename)
    :pimpl_(std::make_unique<Pimpl>())
{
    auto err = FT_Init_FreeType(&(pimpl_->library));
    if(err) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to initialize FreeType" );
    }
    pimpl_->guard_library =
        Pimpl::GuardLibrary_T( &(pimpl_->library), std::function<void(FT_Library*)>( [](FT_Library* ptr) {
            FT_Done_FreeType(*ptr);
        }) );

    pimpl_->filename = filename;
    LoadFontFace(0);
}

void FreetypeFont::LoadFontFace(int index)
{
    ResetKerning();
    auto err = FT_New_Face(pimpl_->library, pimpl_->filename.c_str(), index, &(pimpl_->face));
    if(err) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Unable to load font from " + (pimpl_->filename) );
    }
    pimpl_->guard_face =
        Pimpl::GuardFace_T( &(pimpl_->face), std::function<void(FT_Face*)>( [](FT_Face* ptr) {
            FT_Done_Face(*ptr);
        }) );
    SetSizePoints(12);
}

FreetypeFont::~FreetypeFont()
{
    /* required for pimpl destruction */
}

char const* FreetypeFont::GetFontFamilyName() const
{
    return pimpl_->face->family_name;
}

char const* FreetypeFont::GetFontStyleName() const
{
    return pimpl_->face->style_name;
}

int FreetypeFont::GetNumberOfFaces() const
{
    return static_cast<int>(pimpl_->face->num_faces);
}

void FreetypeFont::SetActiveFace(int index)
{
    if((index < 0) || (index >= pimpl_->face->num_faces)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid font face index" );
    }
    LoadFontFace(index);
}

int FreetypeFont::GetNumberOfGlyphs() const
{
    return pimpl_->face->num_glyphs;
}

bool FreetypeFont::HasFixedWidth() const
{
    return (((pimpl_->face->face_flags)&FT_FACE_FLAG_FIXED_WIDTH) != 0);
}

bool FreetypeFont::HasKerning() const
{
    return (((pimpl_->face->face_flags)&FT_FACE_FLAG_KERNING) != 0);
}

bool FreetypeFont::IsScalable() const
{
    return (((pimpl_->face->face_flags)&FT_FACE_FLAG_SCALABLE) != 0);
}

void FreetypeFont::SetSizePoints(int pt)
{
    ResetKerning();
    auto err = FT_Set_Char_Size(pimpl_->face, 0, pt*64, GetSystemDpi(), GetSystemDpi()); 
    if(err) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Unable to set font size" );
    }
}

void FreetypeFont::SetSizePixels(int px)
{
    ResetKerning();
    auto err = FT_Set_Pixel_Sizes(pimpl_->face, 0, px);
    if(err) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Unable to set font size" );
    }
}

int FreetypeFont::GetFontHeight() const
{
    return (pimpl_->face->size->metrics.height)>>6;
}

int FreetypeFont::GetFontMaxWidth() const
{
    return (pimpl_->face->size->metrics.max_advance)>>6;
}

void FreetypeFont::ResetKerning() const
{
    pimpl_->prev_glyph = 0;
}

void FreetypeFont::LoadGlyph(int c)
{
    FT_UInt glyph_index = FT_Get_Char_Index(pimpl_->face, c);
    auto err = FT_Load_Glyph(pimpl_->face, glyph_index, FT_LOAD_DEFAULT);
    if(err) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to load glyph from font" );
    }

    if(HasKerning()) {
        err = FT_Get_Kerning(pimpl_->face, pimpl_->prev_glyph, glyph_index, FT_KERNING_DEFAULT, &(pimpl_->kerning));
        if(err) {
            DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                  "Unable to determine kerning for glyph" );
        }
    }
    pimpl_->prev_glyph = glyph_index;
}

void FreetypeFont::RenderGlyph()
{
    if(pimpl_->face->glyph->format != FT_GLYPH_FORMAT_BITMAP) {
        auto err = FT_Render_Glyph(pimpl_->face->glyph, FT_RENDER_MODE_NORMAL);
        if(err) {
            DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                  "Unable to render glyph" );
        }
    }
}

GB_MATH_NAMESPACE::Vector2i FreetypeFont::GetKerning() const
{
    using GB_MATH_NAMESPACE::Vector2i;
    return Vector2i((pimpl_->kerning.x)>>6, (pimpl_->kerning.y)>>6);
}

GB_MATH_NAMESPACE::Vector2i FreetypeFont::GetGlyphAdvance() const
{
    using GB_MATH_NAMESPACE::Vector2i;
    return Vector2i((pimpl_->face->glyph->advance.x)>>6, (pimpl_->face->glyph->advance.y)>>6);
}

GB_MATH_NAMESPACE::Vector2i FreetypeFont::GetGlyphOrigin() const
{
    using GB_MATH_NAMESPACE::Vector2i;
    return Vector2i(pimpl_->face->glyph->bitmap_left, -pimpl_->face->glyph->bitmap_top);
}

int FreetypeFont::GetGlyphWidth() const
{
    return pimpl_->face->glyph->bitmap.width;
}

int FreetypeFont::GetGlyphHeight() const
{
    return pimpl_->face->glyph->bitmap.rows;
}

GBCOLOR_COMPONENT FreetypeFont::SampleGlyph(int x, int y) const
{
    return pimpl_->face->glyph->bitmap.buffer[y*(pimpl_->face->glyph->bitmap.pitch) + x];
}

}
}

#endif

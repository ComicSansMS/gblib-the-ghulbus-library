/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifdef GB_GRAPHICS_HAS_LIBJPEG_TURBO

#include <gbGraphics/image_file_codec.hpp>
#include <diagnostic/diagnostic.hpp>

#include <algorithm>
#include <array>
#include <memory>
#include <vector>

#include <stdio.h>        //libjpeg needs the c-style header
#include <jpeglib.h>
#include <jerror.h>

namespace GB_GRAPHICS_NAMESPACE {
namespace ImageFileCodec {

struct istream_src_manager {
    jpeg_source_mgr jpeg_src;
    std::istream* is;
    std::array<JOCTET, 4096> buffer;
};

void error_exit_Callback(j_common_ptr cinfo)
{
    cinfo->err->output_message(cinfo);
    DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::IOError(), "libjpeg error");
}

void output_message_Callback(j_common_ptr cinfo)
{
    DIAG_ASSERT(cinfo->is_decompressor);
    std::array<char, JMSG_LENGTH_MAX> buffer;
    cinfo->err->format_message(cinfo, buffer.data());
    DIAG_LOG(WARNING, "libjpeg warning: " << buffer.data());
}

void init_source_Callback(j_decompress_ptr cinfo)
{
    istream_src_manager* src_mgr = reinterpret_cast<istream_src_manager*>(cinfo->src);
    src_mgr->jpeg_src.bytes_in_buffer = 0;
    src_mgr->jpeg_src.next_input_byte = 0;
}

boolean fill_input_buffer_Callback(j_decompress_ptr cinfo)
{
    istream_src_manager* src_mgr = reinterpret_cast<istream_src_manager*>(cinfo->src);
    src_mgr->is->read(reinterpret_cast<char*>(src_mgr->buffer.data()), src_mgr->buffer.size());
    auto const bytes_read = src_mgr->is->gcount();
    if(bytes_read == 0) {
        // insert a fake EOI marker to indicate error
        src_mgr->buffer[0] = 0xFF;
        src_mgr->buffer[1] = JPEG_EOI;
        src_mgr->jpeg_src.bytes_in_buffer = 2;
        src_mgr->jpeg_src.next_input_byte = src_mgr->buffer.data();
        return TRUE;
    }
    src_mgr->jpeg_src.bytes_in_buffer = bytes_read;
    src_mgr->jpeg_src.next_input_byte = src_mgr->buffer.data();
    return TRUE;
}

void skip_input_data_Callback(j_decompress_ptr cinfo, long num_bytes)
{
    if(num_bytes > 0) {
        istream_src_manager* src_mgr = reinterpret_cast<istream_src_manager*>(cinfo->src);
        if(num_bytes > src_mgr->jpeg_src.bytes_in_buffer) {
            src_mgr->is->seekg(num_bytes - src_mgr->jpeg_src.bytes_in_buffer, std::ios_base::cur);
            if(src_mgr->is->fail()) {
                // insert a fake EOI marker to indicate error
                src_mgr->buffer[0] = 0xFF;
                src_mgr->buffer[1] = JPEG_EOI;
                src_mgr->jpeg_src.bytes_in_buffer = 2;
                src_mgr->jpeg_src.next_input_byte = src_mgr->buffer.data();
                return;
            }
            src_mgr->jpeg_src.bytes_in_buffer = 0;
            src_mgr->jpeg_src.next_input_byte = nullptr;
        } else {
            src_mgr->jpeg_src.bytes_in_buffer -= num_bytes;
            src_mgr->jpeg_src.next_input_byte += num_bytes;
        }
    }
}

void term_source_Callback(j_decompress_ptr /* cinfo */)
{
    // nothing to do
}

istream_src_manager CreateIstreamSrcManager(std::istream& is)
{
    istream_src_manager src_io;
    src_io.jpeg_src.init_source = init_source_Callback;
    src_io.jpeg_src.fill_input_buffer = fill_input_buffer_Callback;
    src_io.jpeg_src.skip_input_data = skip_input_data_Callback;
    src_io.jpeg_src.resync_to_restart = jpeg_resync_to_restart;
    src_io.jpeg_src.term_source = term_source_Callback;
    src_io.is = &is;
    return src_io;
}

void JPG::Decode(std::istream& is, GBCOLOR* buffer, ImageProperties* out_img_props, BufferResizeFunction const& buffer_resize_fun) const
{
    std::istream::pos_type const old_pos = is.tellg();
    try {
        jpeg_error_mgr jerr;
        jpeg_decompress_struct cinfo;

        cinfo.err = jpeg_std_error(&jerr);
        jerr.error_exit = error_exit_Callback;
        jerr.output_message = output_message_Callback;
        jpeg_create_decompress(&cinfo);
        std::unique_ptr<jpeg_decompress_struct, void(*)(jpeg_decompress_struct* cinfo)> guard_cinfo(&cinfo,
            [](jpeg_decompress_struct* cinfo) { jpeg_destroy_decompress(cinfo); });

        istream_src_manager src_io = CreateIstreamSrcManager(is);
        cinfo.src = &src_io.jpeg_src;

        if(jpeg_read_header(&cinfo, TRUE) != 1) {
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::IOError(), "Error reading jpeg header");
        }
        if(jpeg_start_decompress(&cinfo) != 1) {
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::IOError(), "Error in jpeg decompression");
        }

        if(cinfo.output_components != 3) {
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::IOError(), "Non-RGB jpegs unsupported");
        }
        out_img_props->width = cinfo.output_width;
        out_img_props->height = cinfo.output_height;

        buffer_resize_fun(&buffer, out_img_props);

        auto const row_stride = cinfo.output_width * cinfo.output_components;
        std::vector<JSAMPLE> line_buffer(row_stride);
        while(cinfo.output_scanline < cinfo.output_height) {
            // read image one line at a time
            JSAMPROW samprow[1];
            samprow[0] = line_buffer.data();
            jpeg_read_scanlines(&cinfo, samprow, 1);
            for(size_t i=0; i<cinfo.output_width; ++i) {
                buffer[(cinfo.output_scanline-1)*cinfo.output_width + i] =
                    gbColor::XRGB(line_buffer.at(i*cinfo.output_components),
                                  line_buffer.at(i*cinfo.output_components+1),
                                  line_buffer.at(i*cinfo.output_components+2));
            }
        }

        jpeg_finish_decompress(&cinfo);
    } catch(...) {
        //restore file pointer on error
        is.seekg(old_pos);
        throw;
    }
}

}
}

#endif

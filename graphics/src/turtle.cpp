/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/turtle.hpp>
#include <gbGraphics/draw2d.hpp>

#include <gbMath/constants.hpp>
#include <diagnostic/assert.hpp>

#include <cmath>

namespace GB_GRAPHICS_NAMESPACE {

gbTurtle::gbTurtle(gbDraw2D& draw_2d) 
    :m_draw2D(&draw_2d), m_posX(0.0), m_posY(0.0), m_penColor(gbColor::XRGB(255, 255, 255)),
     m_angle(0.0), m_penDown(true), m_width(static_cast<double>(m_draw2D->GetWidth())),
     m_height(static_cast<double>(m_draw2D->GetHeight()))
{
    Reset();
}

gbTurtle::~gbTurtle()
{
}

void gbTurtle::SetColor(GBCOLOR_COMPONENT r, GBCOLOR_COMPONENT g, GBCOLOR_COMPONENT b)
{
    m_penColor = gbColor::XRGB(r, g, b);
}

void gbTurtle::SetColor(GBCOLOR c)
{
    m_penColor = c;
}

void gbTurtle::Go(int units)
{
    double oldX = m_posX;
    double oldY = m_posY;
    m_posY += (static_cast<double>(units) * std::sin(m_angle));
    m_posX += (static_cast<double>(units) * std::cos(m_angle));
    if(m_penDown) {
        m_draw2D->Line( static_cast<int>(XCord(oldX)),   static_cast<int>(YCord(oldY)),
                        static_cast<int>(XCord(m_posX)), static_cast<int>(YCord(m_posY)),
                        m_penColor );
    }
}

void gbTurtle::TurnLeft(int iAngle)
{
    using namespace GB_MATH_NAMESPACE::Const;
    m_angle += static_cast<double>(iAngle % 360) * PI_1_180;
    if(m_angle >=  PI_2) { m_angle -= PI_2; }
    if(m_angle <= -PI_2) { m_angle += PI_2; }
    if(std::fabs(m_angle) < EPSILON) { m_angle = 0.0; }
    DIAG_ASSERT((m_angle >= -PI_2) && (m_angle <= PI_2));
}

void gbTurtle::TurnRight(int iAngle)
{
    using namespace GB_MATH_NAMESPACE::Const;
    m_angle -= static_cast<double>(iAngle % 360) * PI_1_180;
    if(m_angle <=  PI_2) { m_angle += PI_2; }
    if(m_angle >= -PI_2) { m_angle -= PI_2; }
    if(std::fabs(m_angle) < EPSILON) { m_angle = 0.0; }
    DIAG_ASSERT((m_angle >= -PI_2) && (m_angle <= PI_2));
}

void gbTurtle::SetAngle(int iAngle)
{
    m_angle = GB_MATH_NAMESPACE::Const::PI_1_180*static_cast<double>(iAngle % 360);
}

void gbTurtle::TurnLeftRad(double fAngle)
{
    using namespace GB_MATH_NAMESPACE::Const;
    m_angle += std::fmod(fAngle, PI_2);
    if(m_angle >=  PI_2) { m_angle -= PI_2; }
    if(m_angle <= -PI_2) { m_angle += PI_2; }
    if(std::fabs(m_angle) < EPSILON) { m_angle = 0.0; }
    DIAG_ASSERT((m_angle >= -PI_2) && (m_angle <= PI_2));
}

void gbTurtle::TurnRightRad(double fAngle)
{
    using namespace GB_MATH_NAMESPACE::Const;
    m_angle -= std::fmod(fAngle, PI_2);
    if(m_angle <=  PI_2) { m_angle += PI_2; }
    if(m_angle >= -PI_2) { m_angle -= PI_2; }
    if(std::fabs(m_angle) < EPSILON) { m_angle = 0.0; }
    DIAG_ASSERT((m_angle >= -PI_2) && (m_angle <= PI_2));
}

void gbTurtle::SetAngleRad(double fAngle)
{
    m_angle = std::fmod(fAngle, GB_MATH_NAMESPACE::Const::PI_2);
}

void gbTurtle::PenUp()
{
    m_penDown = false;
}

void gbTurtle::PenDown()
{
    m_penDown = true;
}

void gbTurtle::PenToggle()
{
    m_penDown ^= true;
}

bool gbTurtle::IsPenDown()
{
    return m_penDown;
}

void gbTurtle::Goto(int X, int Y)
{
    if(m_penDown) { 
        m_draw2D->Line( static_cast<int>(XCord(m_posX)),     static_cast<int>(YCord(m_posY)),
                        static_cast<int>(m_width/2.0) + X,   static_cast<int>(m_height/2.0) - Y,
                        m_penColor );
    }
    m_posX = static_cast<double>(X);
    m_posY = static_cast<double>(Y);
}

void gbTurtle::Reset()
{
    m_posX     = 0.0;
    m_posY     = 0.0;
    m_angle    = 0.0;
    m_penDown  = true;
    m_penColor = gbColor::XRGB(255, 255, 255);
}

void gbTurtle::Show()
{
    double x = XCord(m_posX);
    double y = YCord(m_posY);
    m_draw2D->Circle( static_cast<int>(x), static_cast<int>(y), 4, gbColor::XRGB(200, 100, 0) );
    m_draw2D->Line( static_cast<int>(x),                        static_cast<int>(y),
                    static_cast<int>(x+(10*std::cos(m_angle))), static_cast<int>(y-(10*std::sin(m_angle))),
                    gbColor::XRGB(200, 100, 0) );
    m_draw2D->Circle( static_cast<int>(x+(10*std::cos(m_angle))), static_cast<int>(y-(10*std::sin(m_angle))),
                      2, gbColor::XRGB(200, 100, 0) );
}

int gbTurtle::GetPosX()
{
    return static_cast<int>(m_posX);
}

int gbTurtle::GetPosY()
{
    return static_cast<int>(m_posY);
}

int gbTurtle::GetAngle()
{
    return static_cast<int>(GB_MATH_NAMESPACE::Const::PI_1_180_INV * m_angle + 0.5);
}

double gbTurtle::GetAngleRad()
{
    return m_angle;
}

}

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/window.hpp>
#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/event_processor.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/opengl_context.hpp>
#include <gbUtil/missing_features.hpp>

#include <diagnostic/diagnostic.hpp>

#include <algorithm>

namespace GB_GRAPHICS_NAMESPACE {

gbWindow_SDL::gbWindow_SDL(std::shared_ptr<SDL_WindowEnvironment>& the_environment, int width, int height,
                           Window::WindowDesc const& window_descriptor)
    :m_Environment(the_environment), m_Descriptor(window_descriptor), m_Window(nullptr),
     m_EventProcessor(the_environment->GetEventProcessor()), m_Width(width), m_Height(height),
     m_MouseCursorVisible(true), m_MouseCaptured(false), m_MouseModeRelative(false)
{
    Uint32 window_flags = SDL_WINDOW_OPENGL;
    window_flags |= (m_Descriptor.Borderless) ? SDL_WINDOW_BORDERLESS : 0;
    window_flags |= (m_Descriptor.Resizable) ? SDL_WINDOW_RESIZABLE : 0;
    window_flags |= (m_Descriptor.Hidden) ? SDL_WINDOW_HIDDEN : 0;
    window_flags |= (m_Descriptor.State != Window::STATE_NORMAL) ?
                    ((m_Descriptor.State == Window::STATE_MINIMIZED) ? SDL_WINDOW_MINIMIZED : SDL_WINDOW_MAXIMIZED) : 0;
    
    m_Window = SDL_CreateWindow( "SDL Window",
                                 SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
                                 width, height, 
                                 window_flags );
    if(!m_Window) {
        DIAG_THROW_EXCEPTION(Exceptions::SDLError(), "Unable to create SDL window");
    }
    m_Environment->RegisterWithEnvironment(this, SDL_GetWindowID(m_Window));
}

gbWindow_SDL::~gbWindow_SDL()
{
    CheckOpenDrawingContexts();
    SDL_DestroyWindow(m_Window);
}

void gbWindow_SDL::CheckOpenDrawingContexts()
{
    if(!m_Associated3dDrawingContext.expired()) {
        DIAG_LOG(WARNING, "Window has unreleased 3d drawing context upon destruction");
    }
}

gbEventProcessor* gbWindow_SDL::GetEventProcessor() const
{
    return m_EventProcessor;
}

gbEventManager* gbWindow_SDL::GetEventManager()
{
    return &m_EventManager;
}

void gbWindow_SDL::SetWindowTitle(char const* str)
{
    SDL_SetWindowTitle(m_Window, str);
}

void gbWindow_SDL::SetFullscreen(Window::FullscreenState_T fullscreen_state)
{
    int ret = SDL_SetWindowFullscreen(m_Window, 
                            ((fullscreen_state == Window::FS_FULLSCREEN) ? SDL_WINDOW_FULLSCREEN :
                            ((fullscreen_state == Window::FS_FULLSCREEN_WINDOW) ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0)));
    if(ret != 0) {
        DIAG_THROW_EXCEPTION(Exceptions::SDLError(), "Unable to set fullscreen state");
    }
}

int gbWindow_SDL::GetWidth() const
{
    return m_Width;
}

int gbWindow_SDL::GetHeight() const
{
    return m_Height;
}

float gbWindow_SDL::GetAspectRatio() const
{
    return static_cast<float>(m_Width)/static_cast<float>(m_Height);
}

gbDrawingContext3DPtr gbWindow_SDL::CreateDrawingContext3D()
{
    gbDrawingContext3DPtr ret = m_Associated3dDrawingContext.lock();
    if(!ret) {
        SDL_GLContext context = SDL_GL_CreateContext(m_Window);
        if(!context) {
            DIAG_THROW_EXCEPTION(Exceptions::SDLError(), "Error creating OpenGL context from SDL Window");
        }
        ret = std::make_shared<gbDrawingContext3D_OGL>(std::make_unique<gbOpenGLContext_SDL>(context, this), this);
        m_Associated3dDrawingContext = ret;
    }
    return ret;
}

gbColor::ColorScheme const& gbWindow_SDL::GetAssociatedColorScheme() const
{
    return gbColor::COLOR_SCHEME_SDL;
}

void gbWindow_SDL::SetMouseCursorVisible(bool show_cursor)
{
    m_MouseCursorVisible = show_cursor;
    m_EventProcessor->ActiveWindowChange(this);
}

bool gbWindow_SDL::GetMouseCursorVisible() const
{
    return m_MouseCursorVisible;
}

void gbWindow_SDL::SetCaptureMouseCursor(bool capture_cursor)
{
    m_MouseCaptured = capture_cursor;
    m_EventProcessor->ActiveWindowChange(this);
}

bool gbWindow_SDL::IsMouseCursorCaptured() const
{
    return m_MouseCaptured;
}

void gbWindow_SDL::SetRelativeMouseMode(bool use_relative_mouse_mode)
{
    m_MouseModeRelative = use_relative_mouse_mode;
}

bool gbWindow_SDL::IsInRelativeMouseMode() const
{
    return m_MouseModeRelative;
}

SDL_Window* gbWindow_SDL::GetSDLWindow_SDL()
{
    return m_Window;
}

void gbWindow_SDL::WindowResizeHandler()
{
    SDL_GetWindowSize(m_Window, &m_Width, &m_Height);
}

}


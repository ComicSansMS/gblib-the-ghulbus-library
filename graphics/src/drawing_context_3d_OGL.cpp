/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/blend_state.hpp>
#include <gbGraphics/diagnostic_OGL.hpp>
#include <gbGraphics/geometry.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/index_buffer.hpp>
#include <gbGraphics/opengl_context.hpp>
#include <gbGraphics/program.hpp>
#include <gbGraphics/rasterizer_state.hpp>
#include <gbGraphics/sampler_state.hpp>
#include <gbGraphics/shader.hpp>
#include <gbGraphics/shadow_state_OGL.hpp>
#include <gbGraphics/texture2d.hpp>
#include <gbGraphics/vertex_buffer.hpp>
#include <gbGraphics/window.hpp>
#include <gbUtil/cxx11_features.hpp>
#include <gbUtil/missing_features.hpp>
#include <diagnostic/diagnostic.hpp>

#include <boost/lexical_cast.hpp>
#ifdef HAS_CXX11_LIB_REGEX
#    include <regex>
#else
#    include <boost/regex.hpp>
#endif

#include <GL/glew.h>

namespace GB_GRAPHICS_NAMESPACE {

gbDrawingContext3D_OGL::gbDrawingContext3D_OGL(std::unique_ptr<gbOpenGLContext>&& gl_context, gbWindow* parent_window)
    :m_ParentWindow(parent_window), m_glShadowState(nullptr), m_ClearColor(0), m_WindowEventHandlerId(0)
{
    //call glewInit() *after* creating the OpenGL rendering context
    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "GLEW Init failed");
    }
    m_OpenGLContext = std::move(gl_context);
    m_OpenGLContext->EnableVsync(true);

    //reset error state
    glGetError();

    // create shadow state
    m_glShadowState = std::make_unique<gbShadowState_OGL>();
    m_glShadowState->SetDepthTest(true);

    // opengl initial setup
    SetClearColor(m_ClearColor);
    BindRasterizerState(CreateRasterizerState(Rasterizer::StateDesc()));
    BindBlendState(CreateBlendState(Blending::StateDesc()));
    m_DefaultSamplerState = CreateSamplerState(Sampling::StateDesc());

    m_WindowEventHandlerId = m_ParentWindow->GetEventManager()->AddEventHandler_WindowEvent(
        [this](Events::WindowState const& state, Events::gbEvent_Type_WindowEvent type) {
            if((type == Events::GBEVENT_WINDOW_RESIZE) && (state.window == m_ParentWindow)) {
                SetViewport_OGL(0, 0, m_ParentWindow->GetWidth(), m_ParentWindow->GetHeight());
            }
        });
}

gbDrawingContext3D_OGL::~gbDrawingContext3D_OGL()
{
    if(m_WindowEventHandlerId) {
        m_ParentWindow->GetEventManager()->RemoveEventHandler_WindowEvent(m_WindowEventHandlerId);
    }
}

gbWindow* gbDrawingContext3D_OGL::GetParentWindow() const
{
    return m_ParentWindow;
}

gbDrawingContext3D_OGL::RendererInfo gbDrawingContext3D_OGL::GetRendererInfo()
{
#ifdef HAS_CXX11_LIB_REGEX
    // VS 2010 bugs on using declarations (Bug ID #530705), so we use the preprocessor here
#    define LIB_REGEX_NAMESPACE std
#else
#    define LIB_REGEX_NAMESPACE boost
#endif
    ErrorMonitor_OGL monitor;
    RendererInfo ret;
    auto get_string = [](GLubyte const* str) -> char const* {
        return (str)?(reinterpret_cast<char const*>(str)):("");
    };
    ret.Name = get_string(glGetString(GL_RENDERER));
    ret.Vendor = get_string(glGetString(GL_VENDOR));
    ret.Version = get_string(glGetString(GL_VERSION));
    ret.ShadingLanguageVersion = get_string(glGetString(GL_SHADING_LANGUAGE_VERSION));
    auto parse_version_number = [](std::string const& str) -> RendererInfo::VersionNumber_T {
        gbDrawingContext3D::RendererInfo::VersionNumber_T ret;
        LIB_REGEX_NAMESPACE::regex version_number_regex("(\\d+)\\.(\\d+)(\\.(\\d+))?.*");
        LIB_REGEX_NAMESPACE::smatch match;
        if(LIB_REGEX_NAMESPACE::regex_match(str, match, version_number_regex)) {
            DIAG_ASSERT((match.position() == 0) && (match.size() == 5));
            ret.Major = boost::lexical_cast<int>(match[1].str());
            ret.Minor = boost::lexical_cast<int>(match[2].str());
            ret.Release = (match[4].length() != 0) ?
                          boost::lexical_cast<int>(match[4].str()) :
                          (-1);
        } else {
            ret.Major = ret.Minor = ret.Release = -1;
        }
        return ret;
    };
    ret.VersionNumber = parse_version_number(ret.Version);
    ret.ShadingLanguageVersionNumber = parse_version_number(ret.ShadingLanguageVersion);
    return ret;
#undef LIB_REGEX_NAMESPACE
}

std::vector<std::string> gbDrawingContext3D_OGL::GetOGLExtensionList_OGL()
{
    ErrorMonitor_OGL monitor;
    std::vector<std::string> ret;
    GLubyte const* str;
    for(int i=0; (str = glGetStringi(GL_EXTENSIONS, i)) != nullptr; ++i) {
        ret.push_back(std::string(reinterpret_cast<char const*>(str)));
    }
    // glGetStringi() sets GL_INVALID_VALUE on exhaustion of extensions list
    monitor.CheckError();
    DIAG_ASSERT(monitor.GetLastError() == GL_INVALID_VALUE);
    return ret;
}

void gbDrawingContext3D_OGL::SetViewport_OGL(int x, int y, int width, int height)
{
    ErrorMonitor_OGL monitor;
    glViewport(x, y, width, height);
}

gbRasterizerStatePtr gbDrawingContext3D_OGL::CreateRasterizerState(Rasterizer::StateDesc const& state_desc)
{
    return std::make_shared<gbRasterizerState_OGL>(state_desc);
}

void gbDrawingContext3D_OGL::BindRasterizerState(gbRasterizerStatePtr rasterizer_state)
{
    ErrorMonitor_OGL monitor;
    if(!rasterizer_state) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Attempt to bind NULL as rasterizer state" );
    }
    m_RasterizerState = rasterizer_state;
    /// @todo shadow state this
    auto const& state_desc = rasterizer_state->GetDescriptor();
    switch(state_desc.FillMode) {
    case Rasterizer::FILL_WIREFRAME: glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
    case Rasterizer::FILL_FILLED:    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;
    default: DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Invalid rasterizer fill mode");
    }
    switch(state_desc.CullMode) {
    case Rasterizer::CULL_FRONT: glCullFace(GL_FRONT); glEnable(GL_CULL_FACE); break;
    case Rasterizer::CULL_BACK:  glCullFace(GL_BACK);  glEnable(GL_CULL_FACE); break;
    case Rasterizer::CULL_NONE:  glDisable(GL_CULL_FACE); break;
    default: DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Invalid rasterizer cull mode");
    }
    switch(state_desc.FaceOrientation) {
    case Rasterizer::FACE_FRONT_CLOCKWISE:         glFrontFace(GL_CW);  break;
    case Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE: glFrontFace(GL_CCW); break;
    default: DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Invalid rasterizer face orientation");
    }
}

gbRasterizerStatePtr gbDrawingContext3D_OGL::GetCurrentRasterizerState() const
{
    return m_RasterizerState;
}

gbBlendStatePtr gbDrawingContext3D_OGL::CreateBlendState(Blending::StateDesc const& state_desc)
{
    return std::make_shared<gbBlendState_OGL>(state_desc);
}

GLenum ConvertBlendOperand(Blending::Operand_T operand)
{
    switch(operand)
    {
    case Blending::OPERAND_ONE: return GL_ONE;
    case Blending::OPERAND_ZERO: return GL_ZERO;
    case Blending::OPERAND_SRC_COLOR: return GL_SRC_COLOR;
    case Blending::OPERAND_SRC_ALPHA: return GL_SRC_ALPHA;
    case Blending::OPERAND_INV_SRC_ALPHA: return GL_ONE_MINUS_SRC_ALPHA;
    case Blending::OPERAND_DST_COLOR: return GL_DST_COLOR;
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown blend operand");
    }
}

GLenum ConvertBlendOperation(Blending::Operation_T operation)
{
    switch(operation)
    {
    case Blending::OPERATION_ADD: return GL_FUNC_ADD;
    default:
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Unknown blend operation");
    }
}

void gbDrawingContext3D_OGL::BindBlendState(gbBlendStatePtr blend_state)
{
    ErrorMonitor_OGL monitor;
    if(!blend_state) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Attempt to bind NULL as blend state" );
    }
    m_BlendState = blend_state;
    auto const& state_desc = blend_state->GetDescriptor();
    m_glShadowState->SetBlending(state_desc.Enable == Blending::ENABLED);
    m_glShadowState->BlendFunc( ConvertBlendOperand(state_desc.Source),
                                ConvertBlendOperand(state_desc.Destination) );
    m_glShadowState->BlendEquation(ConvertBlendOperation(state_desc.Operation));
    if( (state_desc.Source != state_desc.AlphaSource) || (state_desc.Destination != state_desc.AlphaDestination) ||
        (state_desc.Operation != state_desc.AlphaOperation) )
    {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Separate blend functions not implemented");
    }
}

gbBlendStatePtr gbDrawingContext3D_OGL::GetCurrentBlendState() const
{
    return m_BlendState;
}

gbSamplerStatePtr gbDrawingContext3D_OGL::CreateSamplerState(Sampling::StateDesc const& state_desc)
{
    return std::make_shared<gbSamplerState_OGL>(state_desc);
}

gbSamplerStatePtr gbDrawingContext3D_OGL::GetDefaultSamplerState() const
{
    return m_DefaultSamplerState;
}

void gbDrawingContext3D_OGL::SetClearColor(GBCOLOR c)
{
    m_ClearColor = c;
    glClearColor( static_cast<GLclampf>(gbColor::GetR(m_ClearColor)) / 255.0f,
                  static_cast<GLclampf>(gbColor::GetG(m_ClearColor)) / 255.0f,
                  static_cast<GLclampf>(gbColor::GetB(m_ClearColor)) / 255.0f,
                  static_cast<GLclampf>(gbColor::GetA(m_ClearColor)) / 255.0f );
}

GBCOLOR gbDrawingContext3D_OGL::GetClearColor() const
{
    return m_ClearColor;
}

void gbDrawingContext3D_OGL::SetDepthTest(bool enable_depth_test)
{
    m_glShadowState->SetDepthTest(enable_depth_test);
}

void gbDrawingContext3D_OGL::SetClearDepthValue(float f)
{
    if((f < 0.0f) || (f > 1.0f)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Depth clear value must be between 0 and 1" );
    }
    glClearDepth(f);
}

void gbDrawingContext3D_OGL::Clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void gbDrawingContext3D_OGL::Present()
{
    m_OpenGLContext->Swap();
}

gbShaderPtr gbDrawingContext3D_OGL::CreateShader(ShaderType::ShaderType_T type, std::string const& code)
{
    ErrorMonitor_OGL monitor;
    GLenum shader_type = 0;
    switch(type) {
    case ShaderType::VERTEX_SHADER:
        shader_type = GL_VERTEX_SHADER;
        break;
    case ShaderType::PIXEL_SHADER:
        shader_type = GL_FRAGMENT_SHADER;
        break;
    default:
        DIAG_ASSERT(false);
    }
    GLuint shader_id = glCreateShader(shader_type);
    if(!shader_id) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(),
                              "Unable to create shader" );
    }
    return std::make_shared<gbShader_OGL>(shader_id, code);
}

gbProgramPtr gbDrawingContext3D_OGL::CreateProgram()
{
    ErrorMonitor_OGL monitor;
    GLuint program_id = glCreateProgram();
    if(!program_id) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(),
                              "Unable to create program" );
    }
    return std::make_shared<gbProgram_OGL>(program_id, m_glShadowState.get());
}

gbProgramPtr gbDrawingContext3D_OGL::CreateProgramFromFile(std::string const& filename)
{
    gbShaderPtr vtx = CreateShaderFromFile(ShaderType::VERTEX_SHADER, filename + ".vert");
    gbShaderPtr pix = CreateShaderFromFile(ShaderType::PIXEL_SHADER, filename + ".frag");
    gbProgramPtr program = CreateProgram();
    program->BindShader(vtx);
    program->BindShader(pix);
    program->Finalize();
    return program;
}

gbVertexBufferPtr gbDrawingContext3D_OGL::CreateVertexBuffer(int size)
{
    ErrorMonitor_OGL monitor;
    GLuint buffer_id;
    glGenBuffers(1, &buffer_id);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
                              "Unable to create vertex buffer" );
    }
    return m_VertexBuffers.CreateResource(buffer_id, size, m_glShadowState.get());
}

gbIndexBufferPtr gbDrawingContext3D_OGL::CreateIndexBuffer(int size)
{
    ErrorMonitor_OGL monitor;
    GLuint buffer_id;
    glGenBuffers(1, &buffer_id);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
                              "Unable to create index buffer" );
    }
    return m_IndexBuffers.CreateResource(buffer_id, size, m_glShadowState.get());
}

gbTexture2DPtr gbDrawingContext3D_OGL::CreateTexture(Texture::TextureDesc2D const& desc2d)
{
    return CreateTexture(desc2d, m_DefaultSamplerState);
}

gbTexture2DPtr gbDrawingContext3D_OGL::CreateTexture(Texture::TextureDesc2D const& desc2d, gbSamplerStatePtr sampler_state)
{
    ErrorMonitor_OGL monitor;
    GLuint tex_id;
    glGenTextures(1, &tex_id);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Exceptions::OpenGLError(monitor.GetLastError()),
                              "Unable to create texture" );
    }
    return m_Textures2D.CreateResource(tex_id, desc2d, sampler_state, m_glShadowState.get());
}

void gbDrawingContext3D_OGL::InvalidateState()
{
    m_glShadowState->Invalidate();
}

GLenum ConvertPrimitiveType(gbIndexDataBase::Primitive_T type)
{
    switch(type) {
    case gbIndexDataBase::PRIMITIVE_TRIANGLE:
        return GL_TRIANGLES;
    case gbIndexDataBase::PRIMITIVE_NONE:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Index buffer is empty" );
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Unknown primitive type" );
    }
}

GLenum ConvertIndexType(gbIndexDataBase::IndexType_T type)
{
    switch(type) {
    case gbIndexDataBase::INDEX_TYPE_UINT8:
        return GL_UNSIGNED_BYTE;
    case gbIndexDataBase::INDEX_TYPE_UINT16:
        return GL_UNSIGNED_SHORT;
    case gbIndexDataBase::INDEX_TYPE_UINT32:
        return GL_UNSIGNED_INT;
    case gbIndexDataBase::INDEX_TYPE_NONE:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Index buffer is empty" );
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Unknown index type" );
    }
}

void gbDrawingContext3D_OGL::Draw(gbGeometry const& geometry)
{
    gbIndexBuffer const* index_buffer = geometry.GetIndexBuffer();
    m_glShadowState->BindVertexArray(dynamic_cast<gbGeometry_OGL const&>(geometry).GetOGLVertexAttributeArrayId_OGL());
    m_glShadowState->BindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                                dynamic_cast<gbIndexBuffer_OGL const*>(index_buffer)->GetOGLIndexBufferId_OGL());
    geometry.GetProgram()->Activate();
    if(index_buffer) {
        glDrawElements( ConvertPrimitiveType(index_buffer->GetPrimitiveType()), 
                        index_buffer->NumberOfIndices(),
                        ConvertIndexType(index_buffer->GetIndexType()),
                        0 );
    } else {
        gbVertexBuffer const* vertex_buffer = geometry.GetVertexBuffer();
        glDrawArrays(GL_TRIANGLES, 0, vertex_buffer->GetSize()/vertex_buffer->GetVertexFormat()->GetStride());
    }
}

}

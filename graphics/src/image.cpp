/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/image.hpp>
#include <gbGraphics/rect.hpp>
#include <diagnostic/diagnostic.hpp>

#include <utility>
#include <algorithm>
#include <iterator>

namespace GB_GRAPHICS_NAMESPACE {

gbImage::gbImage(int width, int height)
    :m_Width(width), m_Height(height), m_IsLocked(false)
{
    if((width < 0) || (height < 0)) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Negative Image width/height");
    }
    m_Data.resize(width*height);
}

gbImage::gbImage(int width, int height, GBCOLOR clear_color)
    :m_Width(width), m_Height(height), m_IsLocked(false)
{
    if((width < 0) || (height < 0)) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Negative Image width/height");
    }
    m_Data.resize(width*height, clear_color);
}

gbImage::gbImage(int width, int height, GBCOLOR* data)
    :m_Width(width), m_Height(height), m_IsLocked(false)
{
    if((width < 0) || (height < 0)) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Negative Image width/height");
    }
    m_Data.assign(data, data+(width*height));
}

gbImage::~gbImage()
{
    if(m_IsLocked) { Unlock(); }
}

gbImage::gbImage(gbImage const& rhs)
    :m_Data(rhs.m_Data), m_Width(rhs.m_Width), m_Height(rhs.m_Height), m_IsLocked(false)
{
}

gbImage::gbImage(gbImage&& rhs)
    :m_Data(std::move(rhs.m_Data)), m_Width(rhs.m_Width), m_Height(rhs.m_Height), m_IsLocked(false)
{
}

gbImage& gbImage::operator=(gbImage const& rhs)
{
    if(m_IsLocked) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot assign to locked Image");
    }
    if(&rhs != this) {
        m_Data   = rhs.m_Data;
        m_Width  = rhs.m_Width;
        m_Height = rhs.m_Height;
    }
    return *this;
}

gbImage& gbImage::operator=(gbImage&& rhs)
{
    if(m_IsLocked) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot assign to locked Image");
    }
    if(&rhs != this) {
        m_Data.swap(rhs.m_Data);
        m_Width  = rhs.m_Width;
        m_Height = rhs.m_Height;
    }
    return *this;
}

void gbImage::Swap(gbImage& img)
{
    if((m_IsLocked) || (img.IsLocked())) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot swap locked Image");
    }
    std::swap(m_Width, img.m_Width);
    std::swap(m_Height, img.m_Height);
    m_Data.swap(img.m_Data);
}

std::vector<GBCOLOR> gbImage::Release()
{
    if(m_IsLocked) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot release locked Image");
    }
    std::vector<GBCOLOR> ret;
    ret.swap(m_Data);
    m_Width = m_Height = 0;
    return std::move(ret);
}

void gbImage::Lock(GBCOLOR** data, int* pitch)
{
    if(m_IsLocked) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Image already locked");
    }
    m_IsLocked = true;
    *data = (m_Data.empty())?(nullptr):(&m_Data.front());
    *pitch = m_Width;
}

void gbImage::LockRect(Rect const& rect, GBCOLOR** data, int* pitch)
{
    if(m_IsLocked) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Image already locked");
    }
    if( (rect.top_ < 0) || (rect.left_ < 0) || (rect.width_ < 0) || (rect.height_ < 0) ||
        (rect.left_ + rect.width_ > m_Width) || (rect.top_ + rect.height_ > m_Height) )
    {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(), "Invalid lock area specified");
    }
    m_IsLocked = true;
    *data = (m_Data.empty())?(nullptr):(&(m_Data[rect.top_ * m_Width + rect.left_]));
    *pitch = m_Width;
}

void gbImage::Unlock()
{
    if(!m_IsLocked) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Image is not locked");
    }
    m_IsLocked = false;
}

bool gbImage::IsLocked() const
{
    return m_IsLocked;
}

int gbImage::GetWidth() const
{
    return m_Width;
}

int gbImage::GetHeight() const
{
    return m_Height;
}

}

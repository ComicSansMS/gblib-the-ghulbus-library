/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifdef GB_GRAPHICS_HAS_LIBPNG

#include <gbGraphics/image_file_codec.hpp>
#include <diagnostic/diagnostic.hpp>

#include <algorithm>
#include <vector>

#include <png.h>

const int SIZE_SIGNATURE = 8;

/** I/O read callback
 * @param[in] png_ptr Custom pointer to the read structure
 * @param[in] data Pointer to the datablock where the newly read data will go
 * @param[in] length Amount of data to be read (and thus the size of the block)
 */
void libpng_ReadCallback(png_structp png_ptr, png_bytep data, png_size_t length)
{
    ::std::istream* file = static_cast< ::std::istream* >( png_get_io_ptr(png_ptr) );
    file->read(reinterpret_cast<char*>(data), static_cast< ::std::streamsize >(length));
}

/** Error callback
 * @param[in] png_ptr Custom pointer to error handling structure
 * @param[in] error_msg Error message
 * @attention This function must only be left by throwing an exception
 */
void libpng_ErrorCallback(png_structp /* png_ptr */, png_const_charp error_msg)
{
    // we must not return from this function!
    DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                          std::string("libpng fatal error ") + error_msg );
}

/** Warning callback
 * @param[in] png_ptr Custom pointer to error handling structure
 * @param[in] warning_msg Warning message
 */
void libpng_WarningCallback(png_structp /* png_ptr */, png_const_charp warning_msg)
{
    DIAG_LOG(WARNING, "libpng warning: " << warning_msg);
}

/** check if png file is valid; throws on error
 */
void CheckSignature(std::istream& is)
{
    png_byte header_signature[SIZE_SIGNATURE];
    is.read(reinterpret_cast<char*>(header_signature), sizeof(png_byte) * SIZE_SIGNATURE);

    if(is.fail() || (png_sig_cmp(header_signature, 0, SIZE_SIGNATURE) != 0)) { 
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::IOError(), "Invalid PNG data");
    }
}

/** Sets image transformations applied by libpng
 */
void SetParseTransformations(png_structp png_ptr, png_infop info_ptr)
{
    // transformations applied by libpng while parsing
    // order of transformations is important!
    png_uint_32 color_type = png_get_color_type(png_ptr, info_ptr);
    png_byte const bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    //convert palettized data to unmapped rgb
    if(color_type == PNG_COLOR_TYPE_PALETTE) {
        png_set_palette_to_rgb(png_ptr);
    }
    //convert transparency to alpha
    if(png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) {
        png_set_tRNS_to_alpha(png_ptr);
    }
    //upscale 1,2 and 4 bit grayscale color channels to 8 bit
    if((color_type == PNG_COLOR_TYPE_GRAY) && (bit_depth < 8)) {
        png_set_expand_gray_1_2_4_to_8(png_ptr);
    }
    //strip 16 bit color channels to 8 bit
    if(bit_depth == 16) {
        png_set_strip_16(png_ptr);
    }
    // Expand packed bit channels to 1 byte per channel
    if (bit_depth < 8) {
        png_set_packing(png_ptr);
    }
    //convert grayscale images to rgb
    if(color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
        png_set_gray_to_rgb(png_ptr);
    }
    // update image info to reflect changes by transformations
    png_read_update_info(png_ptr, info_ptr);
}

void ReadImageData( png_structp& png_ptr,
                    png_infop& info_ptr,
                    GB_GRAPHICS_NAMESPACE::GBCOLOR* buffer,
                    GB_GRAPHICS_NAMESPACE::ImageFileCodec::Codec::ImageProperties* out_img_props,
                    GB_GRAPHICS_NAMESPACE::ImageFileCodec::Codec::BufferResizeFunction const& buffer_resize_fun )
{
    using namespace GB_GRAPHICS_NAMESPACE;
    //read file header:
    png_read_info(png_ptr, info_ptr);

    // set transforms
    SetParseTransformations(png_ptr, info_ptr);

    //read image data to temporary buffer
    int const image_width = png_get_image_width(png_ptr, info_ptr);
    int const image_height = png_get_image_height(png_ptr, info_ptr);
    int const image_bpp = png_get_bit_depth(png_ptr, info_ptr) * png_get_channels(png_ptr, info_ptr);
    DIAG_ASSERT((image_bpp == 24) || (image_bpp == 32));
    DIAG_ASSERT(png_get_bit_depth(png_ptr, info_ptr) == 8);
    std::vector<char> raw_buffer(image_width*image_height*image_bpp);
    int const pitch = image_width * (image_bpp >> 3);
    std::vector<png_bytep> row_pointers(image_height);
    for(uint32_t y=0; y<row_pointers.size(); ++y) {
        row_pointers[y] = reinterpret_cast<png_bytep>(&(raw_buffer[y*pitch]));
    }
    png_read_image(png_ptr, &row_pointers.front());

    png_read_end(png_ptr, info_ptr);

    //convert image data to GBCOLOR
    out_img_props->width = image_width;
    out_img_props->height = image_height;
    buffer_resize_fun(&buffer, out_img_props);

    for(int i=0; i<image_width*image_height; ++i) {
        GBCOLOR_COMPONENT r = raw_buffer[i*(image_bpp>>3)];
        GBCOLOR_COMPONENT g = raw_buffer[i*(image_bpp>>3)+1];
        GBCOLOR_COMPONENT b = raw_buffer[i*(image_bpp>>3)+2];
        GBCOLOR_COMPONENT a = (image_bpp == 32)?(raw_buffer[i*(image_bpp>>3)+3]):(255);
        buffer[i] = gbColor::ARGB(a, r, g, b);
    }
}


namespace GB_GRAPHICS_NAMESPACE {
namespace ImageFileCodec {

void PNG::Decode(std::istream& is, GBCOLOR* buffer, ImageProperties* out_img_props, BufferResizeFunction const& buffer_resize_fun) const
{
    std::istream::pos_type const old_pos = is.tellg();
    try {
        CheckSignature(is);

        //Init libpng
        png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, libpng_ErrorCallback, libpng_WarningCallback);
        if(!png_ptr) {
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "libpng could not create read struct");
        }
        std::unique_ptr<png_structp, std::function<void(png_structp*)> > png_ptr_guard(&png_ptr,
            [](png_structp* delete_me) {
                png_destroy_read_struct(delete_me, nullptr, nullptr);
            }
        );

        png_infop info_ptr = png_create_info_struct(png_ptr);
        if(!info_ptr) {
            DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "libpng could not create info struct");
        }
        std::unique_ptr<png_infop, std::function<void(png_infop*)> > info_ptr_guard(&info_ptr,
            [png_ptr](png_infop* delete_me) {
                png_destroy_info_struct(png_ptr, delete_me);
            }
        );
        png_set_read_fn(png_ptr, &is, libpng_ReadCallback);

        //file pointer was already advanced SIZE_SIGNATURE bytes by CheckFile()
        png_set_sig_bytes(png_ptr, SIZE_SIGNATURE);

        //Read image
        ReadImageData(png_ptr, info_ptr, buffer, out_img_props, buffer_resize_fun);
    } catch(...) {
        //restore file pointer on error
        is.seekg(old_pos);
        throw;
    }
}

}
}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/shader.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <diagnostic/diagnostic.hpp>

#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

gbShader_OGL::gbShader_OGL(GLuint shader_id, std::string const& code)
    :m_ShaderId(shader_id)
{
    try {
        CompileShader(code);
    } catch(std::exception&) {
        glDeleteShader(m_ShaderId);
        throw;
    }
}

void gbShader_OGL::CompileShader(std::string const& code)
{
    GLchar const* code_arr[] = { code.c_str() };
    GLint len_arr[] = { static_cast<GLint>(code.length()) };
    glShaderSource(m_ShaderId, 1, code_arr, len_arr);
    glCompileShader(m_ShaderId);

    GLint res;
    glGetShaderiv(m_ShaderId, GL_COMPILE_STATUS, &res);
    GLint len;
    glGetShaderiv(m_ShaderId, GL_INFO_LOG_LENGTH, &len);
    std::vector<char> errmsg;
    if(len > 0) {
        errmsg.resize(len, '\0');
        GLsizei written;
        glGetShaderInfoLog(m_ShaderId, len, &written, errmsg.data());
        m_CompileMessage = errmsg.data();
    }
    if(res == GL_FALSE) {
        DIAG_THROW_EXCEPTION( Exceptions::ShaderCompileError(errmsg.data()),
                              "Error compiling shader" );
    }
}

gbShader_OGL::~gbShader_OGL()
{
    glDeleteShader(m_ShaderId);
}

ShaderType::ShaderType_T gbShader_OGL::GetShaderType() const
{
    GLint res = 0;
    glGetShaderiv(m_ShaderId, GL_SHADER_TYPE, &res);
    switch(res) {
    case GL_VERTEX_SHADER:   return ShaderType::VERTEX_SHADER;
    case GL_FRAGMENT_SHADER: return ShaderType::PIXEL_SHADER;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unknown shader type" );
    }
}

std::string const& gbShader_OGL::GetShaderCompileMessage() const
{
    return m_CompileMessage;
}

GLuint gbShader_OGL::GetOGLShaderId_OGL() const
{
    return m_ShaderId;
}

}

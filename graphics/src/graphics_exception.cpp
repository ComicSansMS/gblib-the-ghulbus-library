/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/graphics_exception.hpp>
#include <diagnostic/assert.hpp>

#include <sstream>

#ifdef GB_GRAPHICS_HAS_SDL
#    include <SDL.h>
#endif

static std::string g_StaticErrStr;

namespace GB_GRAPHICS_NAMESPACE {
namespace Exceptions {

    SDLError::SDLError()
        :error_string_(nullptr)
    {
    #ifdef GB_GRAPHICS_HAS_SDL
        error_string_ = SDL_GetError();
        SDL_ClearError();
    #endif
    }

    SDLError::~SDLError() throw()
    {
    }

    char const* SDLError::what() const throw()
    {
    #ifdef GB_GRAPHICS_HAS_SDL
        std::ostringstream sstr;
        sstr << "SDL Error - " << error_string_;
        g_StaticErrStr = sstr.str();
        return g_StaticErrStr.c_str();
    #else
        return "SDL Error";
    #endif
    }

}
}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    include <gbGraphics/diagnostic_OGL.hpp>
#    include <GL/glew.h>
#endif

namespace GB_GRAPHICS_NAMESPACE {
namespace Exceptions {

    OpenGLError::OpenGLError()
    {
    #ifdef GB_GRAPHICS_HAS_OPENGL
        gl_errcode_ = glGetError();
    #endif
    }

    OpenGLError::OpenGLError(unsigned int errcode)
        :gl_errcode_(errcode)
    {
    }

    OpenGLError::~OpenGLError() throw()
    {
    }

    char const* OpenGLError::what() const throw()
    {
    #ifdef GB_GRAPHICS_HAS_OPENGL
        std::ostringstream sstr;
        sstr << "OpenGL Error - " << TranslateGLErrorCode_OGL(gl_errcode_);
        error_string_ = sstr.str();
        return error_string_.c_str();
    #else
        return "OpenGL Error";
    #endif
    }

    unsigned int OpenGLError::GetOpenGLErrorCode() const
    {
        return gl_errcode_;
    }


    ShaderCompileError::ShaderCompileError(std::string const& errmsg)
        :errmsg_(errmsg)
    {
    }

    ShaderCompileError::~ShaderCompileError() throw()
    {
    }

    char const* ShaderCompileError::what() const throw()
    {
        return errmsg_.c_str();
    }

    std::ostream& operator<<(std::ostream& os, ShaderCompileError& e)
    {
        os << Diagnostic::GetExceptionInfo<Diagnostic::Exception_Info::description>(e)->c_str() << ":\n ";
        if(Diagnostic::GetExceptionInfo<Diagnostic::Exception_Info::file_name>(e)) {
            os << "in file " << Diagnostic::GetExceptionInfo<Diagnostic::Exception_Info::file_name>(e)->c_str() << "\n";
        }
        return os << e.what();
    }

}
}


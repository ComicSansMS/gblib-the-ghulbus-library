
#include <gbGraphics/graphics.hpp>
#include <gbGraphics/vertex_data.hpp>
#include <gbGraphics/input_camera_spherical.hpp>
#include <gbGraphics/mesh_primitives.hpp>
#include <gbGraphics/image_loader.hpp>

#include <gbGraphics/fontmap.hpp>
#include <gbGraphics/font_freetype.hpp>

#include <gbMath/matrix.hpp>
#include <gbMath/projection.hpp>
#include <gbMath/matrix_op.hpp>

struct MagicConstant {};
template<> struct GhulbusGraphics::VertexComponent::Traits::SemanticComponentType<MagicConstant> {
    static const GhulbusGraphics::gbVertexFormatBase::ComponentSemantics_T value = GhulbusGraphics::gbVertexFormatBase::COMPONENT_SEMANTICS_GENERIC;
};

struct foo {
    int bar;
    int fnord;
};

typedef GhulbusGraphics::gbVertexFormat<
    GhulbusGraphics::VertexComponent::Position3f,
    GhulbusGraphics::VertexComponent::Normal3f,
    GhulbusGraphics::VertexComponent::TexCoord2f
> VertexFormat;

int main(int /* argc */, char* /* argv */[])
{
    using namespace GhulbusGraphics;
    auto main_window = gbWindow::Create(800, 800);
    main_window->SetWindowTitle("Textured Cube Demo");

    auto dc3d = main_window->CreateDrawingContext3D();
    gb2D gb2d(dc3d);

    gbProgramPtr program = dc3d->CreateProgramFromFile("../../graphics/test/gfx/textured");
    gbVertexBinding binding(program, VertexFormat());
    Bind<VertexComponent::Position3f, VertexFormat>(binding, "VertexPosition");
    Bind<VertexComponent::TexCoord2f, VertexFormat>(binding, "VertexTexCoord");

    auto cube_mesh_1st = Mesh::Box<VertexFormat>(0.2f).CreateMesh(*dc3d, binding);
    auto cube_mesh_2nd = Mesh::Box<VertexFormat>(0.5f).CreateMesh(*dc3d, binding);

    program->BindUniform("uModel", gbMath::Matrix4f());
    program->BindUniform("uView", gbMath::Matrix4f());
    program->BindUniform("uProject", gbMath::ProjectionPerspectiveFov(0.75f * gbMath::Constf::PI_1_2, 1.0f, 0.1f, 100.0f));
    program->BindUniform("uTexCoordTransform", gbMath::Matrix3f());

    gbImage img1 = gbImageLoader("../../graphics/test/gfx/lena.png", ImageFileCodec::PNG()).GetImage();
    gbImage img2 = gbImageLoader("../../graphics/test/gfx/karen.png", ImageFileCodec::PNG()).GetImage();
    gbImage img3 = gbImageLoader("../../graphics/test/gfx/kirsten.png", ImageFileCodec::PNG()).GetImage();
    gbImage img4 = gbImageLoader("../../graphics/test/gfx/olivia.png", ImageFileCodec::PNG()).GetImage();
    gbImage img5 = gbImageLoader("../../graphics/test/gfx/emma.png", ImageFileCodec::PNG()).GetImage();
    gbImage img6 = gbImageLoader("../../graphics/test/gfx/scarlett.png", ImageFileCodec::PNG()).GetImage();
    gbImage img7 = gbImageLoader("../../graphics/test/gfx/maggie.png", ImageFileCodec::PNG()).GetImage();
    //*
    gbTexture2DPtr texture1 = dc3d->CreateTexture(Texture::TextureDesc2D(img1.GetWidth(), img1.GetHeight()));
    texture1->SetTextureData(img1);
    gbTexture2DPtr texture2 = dc3d->CreateTexture(Texture::TextureDesc2D(img2.GetWidth(), img2.GetHeight()));
    texture2->SetTextureData(img2);
    //*/

    gbTexture2DPtr texture3 = dc3d->CreateTexture(Texture::TextureDesc2D(std::max(img1.GetWidth(), img2.GetWidth()), img1.GetHeight() + img2.GetHeight()));
    texture3->SetTextureSubData(Rect(0, 0, img1.GetWidth(), img1.GetHeight()), img1);
    texture3->SetTextureSubData(Rect(0, img1.GetHeight(), img2.GetWidth(), img2.GetHeight()), img2);

    std::vector<ReadableSurface*> images;
    images.push_back(&img1);
    images.push_back(&img2);
    images.push_back(&img3);
    images.push_back(&img4);
    images.push_back(&img5);
    images.push_back(&img6);
    images.push_back(&img7);
    gbTextureAtlas texture_atlas(*dc3d, images);

    Fonts::FreetypeFont font("../../graphics/test/gfx/segoesc.ttf");
    font.SetSizePoints(72);
    Text::CharacterSet<char> char_set;
    char_set.AddCharacterRange('A', 'Z');
    char_set.AddCharacterRange('a', 'z');
    char_set.AddCharacterRange('0', '9');
    gbFontmap<char> fontmap(*dc3d, char_set, &font);
    //gbTextureAtlas& texture_atlas = fontmap.m_FontmapAtlas;

    Input::CameraSpherical camera(*main_window);
    auto default_blend_state = dc3d->GetCurrentBlendState();

    int current_texture = 0;
    Util::gbTimerPtr timer = Util::gbTimer::Create();
    timer->Mark();
    auto event_man = main_window->GetEventManager();
    while(!event_man->IsDone())
    {
        event_man->ProcessMessages();
        program->BindUniform("uView", camera.GetCameraMatrix());
        dc3d->Clear();
        dc3d->BindBlendState(default_blend_state);
        program->BindUniform("uTexture", texture_atlas.GetTexture());
        program->BindUniform("uTexCoordTransform", texture_atlas.GetSubtextureTexCoordTransform<float>(current_texture));
        program->BindUniform("uModel", gbMath::Matrix::Translate4D(gbMath::Vector3f(0.0f, 0.0f, -0.5f)));
        cube_mesh_1st.Render();
        program->BindUniform("uTexCoordTransform", gbMath::Matrix3f());
        program->BindUniform("uModel", gbMath::Matrix::Translate4D(gbMath::Vector3f(0.0f, 0.0f, +0.5f)));
        cube_mesh_2nd.Render();
        gb2d.Draw();
        dc3d->Present();
        if(timer->PeekTime() > 1000) {
            current_texture = (current_texture + 1) % texture_atlas.NumberOfSubtextures();
            timer->Mark();
        }
    }
    return 0;
}


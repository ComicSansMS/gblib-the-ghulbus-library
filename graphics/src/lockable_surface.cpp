/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/lockable_surface.hpp>
#include <algorithm>
#include <cstring>

namespace GB_GRAPHICS_NAMESPACE {

void CopySurface(LockableSurface& dst, ReadableSurface& src)
{
    if( (dst.GetWidth() != src.GetWidth()) || (dst.GetHeight() != src.GetHeight()) ) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Surface dimensions must match for copy");
    }
    SurfaceLock<LockableSurface> dst_lock(dst);
    GBCOLOR* const dst_data = dst_lock.GetLockedData();
    int const dst_pitch = dst_lock.GetPitch();
    SurfaceLock<ReadableSurface> src_lock(src);
    GBCOLOR* const src_data = src_lock.GetLockedData();
    int const src_pitch = src_lock.GetPitch();
    int const bytesize = src.GetWidth()*src.GetHeight()*sizeof(GBCOLOR);
    int const linesize = src.GetWidth()*sizeof(GBCOLOR);
    if( reinterpret_cast<char*>(std::min(src_data, dst_data)) + bytesize > reinterpret_cast<char*>(std::max(src_data, dst_data)) ) {
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Surface memory overlap");
    }
    if(dst_pitch == src_pitch) {
        std::memcpy(dst_data, src_data, bytesize);
    } else {
        for(int row=0; row<src.GetHeight(); ++row) {
            std::memcpy( dst_data + row*dst_pitch,
                         src_data + row*src_pitch,
                         linesize );
        }
    }
}

}

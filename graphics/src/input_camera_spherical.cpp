/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/input_camera_spherical.hpp>
#include <gbGraphics/window.hpp>

#include <diagnostic/diagnostic.hpp>
#include <gbMath/constants.hpp>

#include <algorithm>

namespace GB_GRAPHICS_NAMESPACE {
namespace Input {

CameraSpherical::CameraSpherical(gbWindow& window)
    :m_Window(&window), m_CameraDistance(5.0f), m_CameraAngleHorizontal(0.0f), m_CameraAngleVertical(0.0f)
{
    m_HandlerIdMouseMove = m_Window->GetEventManager()->AddEventHandler_MouseInput(this, Events::GBEVENT_MOUSE_MOVE);
    m_HandlerIdMouseWheel =
        m_Window->GetEventManager()->AddEventHandler_MouseInput(this, Events::GBEVENT_MOUSE_MOUSEWHEEL);
    m_State.lbutton = m_State.mbutton = m_State.rbutton = false;
    m_State.posx = m_State.posy = 0;
    m_State.timestamp = 0;
    InitConfig();
    UpdateCamera();
}

CameraSpherical::~CameraSpherical()
{
    m_Window->GetEventManager()->RemoveEventHandler_MouseInput(m_HandlerIdMouseMove);
    m_Window->GetEventManager()->RemoveEventHandler_MouseInput(m_HandlerIdMouseWheel);
}

void CameraSpherical::InitConfig()
{
    m_Cfg.UseMouseMove = true;
    m_Cfg.UseMouseZoom = true;
    m_Cfg.UseMouseWheelZoom = true;
    m_Cfg.SpeedHorizontal = 1.0f;
    m_Cfg.SpeedVertical = 1.0f;
    m_Cfg.SpeedZoom = 1.0f;
    m_Cfg.SpeedMouseWheelZoom = 1.0f;
    m_Cfg.MouseRotateButton = Events::GBMOUSE_BUTTON_LEFT;
    m_Cfg.MouseZoomButton = Events::GBMOUSE_BUTTON_RIGHT;
    m_Cfg.CameraDistanceMin = 1.0f;
    m_Cfg.CameraDistanceMax = 100.0f;
}

bool CameraSpherical::Handler(Events::MouseState const* state, Events::gbEvent_Type_Mouse type)
{
    using namespace Events;
    m_UpdateCamera = false;
    switch(type)
    {
    case GBEVENT_MOUSE_MOUSEWHEEL:
        ProcessMouseWheel(state);
        break;
    case GBEVENT_MOUSE_MOVE:
        ProcessMouseMove(state);
        break;
    default:
        break;
    }
    m_State = *(m_Window->GetEventManager()->QueryMouseState());
    if(m_UpdateCamera) {
        UpdateCamera();
    }
    return false;
}

void CameraSpherical::ProcessMouseWheel(GhulbusGraphics::Events::MouseState const* state)
{
    if(m_Cfg.UseMouseWheelZoom) {
        m_CameraDistance += static_cast<float>(state->mousewheel) * -0.001f * m_CameraDistance * m_Cfg.SpeedMouseWheelZoom;
        m_UpdateCamera = true;
    }
}

void CameraSpherical::ProcessMouseMove(GhulbusGraphics::Events::MouseState const* state)
{
    if(m_Cfg.UseMouseMove) {
        if(Events::CheckMouseButton(*state, m_Cfg.MouseRotateButton) || (m_Cfg.MouseRotateButton == Events::GBMOUSE_BUTTON_NONE)) {
            int dX = state->posx - m_State.posx;
            m_CameraAngleHorizontal += static_cast<float>(dX) * 0.01f * m_Cfg.SpeedHorizontal;
            int dY = state->posy - m_State.posy;
            m_CameraAngleVertical += static_cast<float>(dY) * 0.01f * m_Cfg.SpeedVertical;
            m_UpdateCamera = true;
        }
    }
    if(m_Cfg.UseMouseZoom) {
        if(Events::CheckMouseButton(*state, m_Cfg.MouseZoomButton) || (m_Cfg.MouseZoomButton == Events::GBMOUSE_BUTTON_NONE)) {
            int dY = state->posy - m_State.posy;
            m_CameraDistance += static_cast<float>(dY) * 0.005f * m_CameraDistance * m_Cfg.SpeedZoom;
            m_UpdateCamera = true;
        }
    }
}

void CameraSpherical::UpdateCamera()
{
    using namespace GB_MATH_NAMESPACE::Constf;
    using GB_MATH_NAMESPACE::Vector3f;
    m_CameraDistance = std::min(m_Cfg.CameraDistanceMax, std::max(m_Cfg.CameraDistanceMin, m_CameraDistance));
    if(m_CameraAngleHorizontal <= -PI_2) { m_CameraAngleHorizontal += PI_2; }
    if(m_CameraAngleHorizontal >= PI_2)  { m_CameraAngleHorizontal -= PI_2; }
    if(m_CameraAngleVertical <= -PI_1_2)   { m_CameraAngleVertical = -PI_1_2 + EPSILON; }
    if(m_CameraAngleVertical >= PI_1_2)    { m_CameraAngleVertical = PI_1_2 - EPSILON; }
    Vector3f position( -m_CameraDistance * cos(m_CameraAngleHorizontal) * sin(m_CameraAngleVertical - PI_1_2),
                        m_CameraDistance * cos(m_CameraAngleVertical - PI_1_2),
                        m_CameraDistance * sin(m_CameraAngleHorizontal) * sin(m_CameraAngleVertical - PI_1_2) );
    m_Camera.SetPosition(position);
}

GB_MATH_NAMESPACE::Matrix4f CameraSpherical::GetCameraMatrix() const
{
    return m_Camera.GetCameraMatrix();
}

float CameraSpherical::GetCameraAngleHorizontal() const
{
    return m_CameraAngleHorizontal;
}

float CameraSpherical::GetCameraAngleVertical() const
{
    return m_CameraAngleVertical;
}

float CameraSpherical::GetCameraDistance()
{
    return m_CameraDistance;
}

void CameraSpherical::SetCameraAngleHorizontal(float phi)
{
    using namespace GB_MATH_NAMESPACE::Constf;
    if((phi < 0) || (phi >= PI_2)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Horizontal camera angle must be from range [0..2pi)");
    }
    m_CameraAngleHorizontal = phi;
}

void CameraSpherical::SetCameraAngleVertical(float theta)
{
    using namespace GB_MATH_NAMESPACE::Constf;
    if((theta < -PI_1_2) || (theta >= PI_1_2)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Vertical camera angle must be from range (-pi/2..+pi/2)");
    }
    m_CameraAngleHorizontal = theta;
}

void CameraSpherical::SetCameraDistance(float dist)
{
    m_CameraDistance = std::min(m_Cfg.CameraDistanceMax, std::max(m_Cfg.CameraDistanceMin, dist));
}

CameraSpherical::Config_T const& CameraSpherical::GetInputConfig()
{
    return m_Cfg;
}

void CameraSpherical::SetInputConfig(Config_T const& config)
{
    m_Cfg = config;
}

}
}

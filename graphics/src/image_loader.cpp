/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/image_loader.hpp>
#include <gbGraphics/image_file_codec.hpp>

#include <diagnostic/diagnostic.hpp>

#include <fstream>

namespace GB_GRAPHICS_NAMESPACE {

gbImageLoader::gbImageLoader()
    :m_Image(0, 0)
{
}

gbImageLoader::gbImageLoader(char const* filename, ImageFileCodec::Codec const& codec)
    :m_Image(0, 0)
{
    ReadFile(filename, codec);
}

gbImageLoader::~gbImageLoader()
{
}

void gbImageLoader::ReadFile(char const* filename, ImageFileCodec::Codec const& codec)
{
    std::ifstream fin(filename, std::ios_base::binary);
    if(!fin) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError() << Diagnostic::Exception_Info::file_name(filename),
                              "Could not open image file" );
    }
    try {
        ReadStream(fin, codec);
    } catch(Diagnostic::Exception& e) {
        e << Diagnostic::Exception_Info::file_name(filename);
        throw;
    }
}

void gbImageLoader::ReadStream(std::istream& is, ImageFileCodec::Codec const& codec)
{
    using ImageFileCodec::Codec;
    Codec::ImageProperties img_props;
    SurfaceLock<gbImage> image_lock;
    
    auto buffer_resize_callback =
        [this,&image_lock]( GBCOLOR** buffer,
                            Codec::ImageProperties const* img_props )
        {
            DIAG_ASSERT(*buffer == nullptr);
            gbImage img(img_props->width, img_props->height);
            m_Image.Swap(img);
            image_lock.Lock(m_Image);
            *buffer = image_lock.GetLockedData();
        };
    codec.Decode( is,
                  nullptr,
                  &img_props,
                  buffer_resize_callback );
}

int gbImageLoader::GetImageWidth() const
{
    return m_Image.GetWidth();
}

int gbImageLoader::GetImageHeight() const
{
    return m_Image.GetHeight();
}

gbImage gbImageLoader::GetImage()
{
    gbImage ret(0, 0);
    ret.Swap(m_Image);
    return std::move(ret);
}

gbImage gbImageLoader::GetImageCopy() const
{
    return m_Image;
}


gbImage LoadImageFromFile(char const* filename, ImageFileCodec::Codec const& codec)
{
    gbImageLoader loader(filename, codec);
    return std::move(loader.GetImage());
}


}

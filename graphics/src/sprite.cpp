/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/sprite.hpp>

#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/graphics_2d.hpp>
#include <gbGraphics/mesh.hpp>
#include <gbGraphics/program.hpp>
#include <gbGraphics/rect.hpp>
#include <gbGraphics/texture2d.hpp>
#include <gbGraphics/uniform_data.hpp>
#include <gbGraphics/window.hpp>

#include <gbMath/matrix4.hpp>

namespace GB_GRAPHICS_NAMESPACE {

gbSprite::gbSprite(GB_UTIL_NAMESPACE::gbResourceGuardian<gbSprite>& guardian, gb2D& gb2d,
                   gbProgram& program, gbMesh&& mesh, gbTexture2DPtr texture)
    :gbSupervisedResource(&guardian), m_gb2d(&gb2d), m_Program(&program), m_Mesh(std::move(mesh)), m_Texture(texture)
{
    m_RenderTargetDefaultArea.width_ = m_Texture->GetWidth();
    m_RenderTargetDefaultArea.height_ = m_Texture->GetHeight();
}

gbSprite::~gbSprite()
{
    m_gb2d->UnregisterFromDrawing(this);
}

void gbSprite::Lock(GBCOLOR** data, int* pitch)
{
    m_Texture->Lock(data, pitch);
}

void gbSprite::LockRect(Rect const& rect, GBCOLOR** data, int* pitch)
{
    m_Texture->LockRect(rect, data, pitch);
}

void gbSprite::Unlock()
{
    m_Texture->Unlock();
}

bool gbSprite::IsLocked() const
{
    return m_Texture->IsLocked();
}

int gbSprite::GetWidth() const
{
    return m_Texture->GetWidth();
}

int gbSprite::GetHeight() const
{
    return m_Texture->GetHeight();
}

void gbSprite::SetSizeOnScreen(int width, int height)
{
    if((width < 0) || (height < 0)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid sprite dimensions specified" );
    }
    m_RenderTargetDefaultArea.width_ = width;
    m_RenderTargetDefaultArea.height_ = height;
}

int gbSprite::GetWidthOnScreen() const
{
    return m_RenderTargetDefaultArea.width_;
}

int gbSprite::GetHeightOnScreen() const
{
    return m_RenderTargetDefaultArea.height_;
}

void gbSprite::SetPositionOnScreen(int x, int y)
{
    m_RenderTargetDefaultArea.left_ = x;
    m_RenderTargetDefaultArea.top_ = y;
}

int gbSprite::GetPositionOnScreenX() const
{
    return m_RenderTargetDefaultArea.left_;
}

int gbSprite::GetPositionOnScreenY() const
{
    return m_RenderTargetDefaultArea.top_;
}

void gbSprite::Render()
{
    auto const main_window = m_gb2d->GetDrawingContext().GetParentWindow();
    int const viewport_width = main_window->GetWidth();
    int const viewport_height = main_window->GetHeight();
    float const pixel_w = 2.0f / static_cast<float>(viewport_width);
    float const pixel_h = 2.0f / static_cast<float>(viewport_height);
    float const img_w = pixel_w * static_cast<float>(m_RenderTargetDefaultArea.width_);
    float const img_h = pixel_h * static_cast<float>(m_RenderTargetDefaultArea.height_);
    float const orig_x = pixel_w * static_cast<float>(m_RenderTargetDefaultArea.left_);
    float const orig_y = img_h + pixel_h * static_cast<float>(m_RenderTargetDefaultArea.top_);

    m_Program->BindUniform( "uModelView",
                            GB_MATH_NAMESPACE::Matrix4f(img_w,  0.0f, 0.0f, orig_x - 1.0f,
                                                         0.0f, img_h, 0.0f, 1.0f - orig_y,
                                                         0.0f,  0.0f, 1.0f,        0.0f,
                                                         0.0f,  0.0f, 0.0f,        1.0f) );
    m_Program->BindUniform("uTexCoordTransform", GB_MATH_NAMESPACE::Matrix3f());
    m_Program->BindUniform("uTexture", m_Texture);
    m_Mesh.Render();
}

void gbSprite::RenderSegment(Rect const& src_rect, Rect const& dst_rect)
{
    auto const main_window = m_gb2d->GetDrawingContext().GetParentWindow();
    int const viewport_width = main_window->GetWidth();
    int const viewport_height = main_window->GetHeight();
    float const pixel_w = 2.0f / static_cast<float>(viewport_width);
    float const pixel_h = 2.0f / static_cast<float>(viewport_height);
    float const img_w = pixel_w * static_cast<float>(dst_rect.width_);
    float const img_h = pixel_h * static_cast<float>(dst_rect.height_);
    float const orig_x = pixel_w * static_cast<float>(dst_rect.left_);
    float const orig_y = img_h + pixel_h * static_cast<float>(dst_rect.top_);

    m_Program->BindUniform( "uModelView",
                            GB_MATH_NAMESPACE::Matrix4f(img_w,  0.0f, 0.0f, orig_x - 1.0f,
                                                         0.0f, img_h, 0.0f, 1.0f - orig_y,
                                                         0.0f,  0.0f, 1.0f,        0.0f,
                                                         0.0f,  0.0f, 0.0f,        1.0f) );
    float const src_pixel_w = 1.0f / static_cast<float>(m_Texture->GetWidth());
    float const src_pixel_h = 1.0f / static_cast<float>(m_Texture->GetHeight());
    float const src_w = src_pixel_w * static_cast<float>(src_rect.width_);
    float const src_h = src_pixel_h * static_cast<float>(src_rect.height_);
    float const src_orig_x = static_cast<float>(src_rect.left_) / static_cast<float>(m_Texture->GetWidth());
    float const src_orig_y = static_cast<float>(src_rect.top_) / static_cast<float>(m_Texture->GetHeight());
    m_Program->BindUniform( "uTexCoordTransform",
                            GB_MATH_NAMESPACE::Matrix3f( src_w,  0.0f, src_orig_x,
                                                          0.0f, src_h, src_orig_y,
                                                          0.0f,  0.0f,       1.0f) );
    m_Program->BindUniform("uTexture", m_Texture);
    m_Mesh.Render();
}

}

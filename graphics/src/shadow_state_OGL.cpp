/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/shadow_state_OGL.hpp>
#include <gbGraphics/diagnostic_OGL.hpp>
#include <diagnostic/diagnostic.hpp>

#include <algorithm>

#ifdef GB_GRAPHICS_HAS_OPENGL

namespace GB_GRAPHICS_NAMESPACE {

gbShadowState_OGL::gbShadowState_OGL()
{
    m_Buffers.resize(9);
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &m_MaxTextureUnits);
    // @todo verify how to address texture units outside the enum range
    m_MaxTextureUnits = std::min(m_MaxTextureUnits, 32);
    m_Textures.resize(m_MaxTextureUnits);
    Invalidate();
}

void gbShadowState_OGL::Invalidate()
{
    std::for_each(begin(m_Buffers), end(m_Buffers), [](OpenGL_Id<GLuint>& id) { id.isValid_ = false; });
    m_VertexArray.isValid_ = false;
    m_Program.isValid_ = false;
    m_RootElementArray.isValid_ = false;
    m_ActiveTextureUnit.isValid_ = false;
    std::for_each(begin(m_Textures), end(m_Textures), [](std::array<OpenGL_Id<GLuint>, 9>& id_arr) {
        std::for_each(std::begin(id_arr), std::end(id_arr), [](gbShadowState_OGL::OpenGL_Id<GLuint>& id) {
            id.isValid_ = false;
        });
    });
    m_DepthTestEnabled.isValid_ = false;
    InvalidateBlendState(m_BlendState);
}

void gbShadowState_OGL::InvalidateBlendState(BlendState& blend_state) const
{
    blend_state.enabled.isValid_ = false;
    blend_state.mode.isValid_ = false;
    blend_state.sfactor.isValid_ = false;
    blend_state.dfactor.isValid_ = false;
}

void gbShadowState_OGL::SetDepthTest(bool enabled)
{
    if(m_DepthTestEnabled != enabled) {
        if (enabled) {
            glEnable(GL_DEPTH_TEST);
        } else {
            glDisable(GL_DEPTH_TEST);
        }
        m_DepthTestEnabled = enabled;
    }
}

void gbShadowState_OGL::SetBlending(bool enabled)
{
    if(m_BlendState.enabled != enabled)
    {
        if(enabled) {
            glEnable(GL_BLEND);
        } else {
            glDisable(GL_BLEND);
        }
        m_BlendState.enabled = enabled;
    }
}

int BufferTargetIndex(GLenum target)
{
    switch(target) {
    case GL_ARRAY_BUFFER:              return 0;
    case GL_COPY_READ_BUFFER:          return 1;
    case GL_COPY_WRITE_BUFFER:         return 2;
    case GL_ELEMENT_ARRAY_BUFFER:      return 3;
    case GL_PIXEL_PACK_BUFFER:         return 4;
    case GL_PIXEL_UNPACK_BUFFER:       return 5;
    case GL_TEXTURE_BUFFER:            return 6;
    case GL_TRANSFORM_FEEDBACK_BUFFER: return 7;
    case GL_UNIFORM_BUFFER:            return 8;
    default:
        DIAG_ASSERT(false);
        return -1;
    }
}

int TextureTargetIndex(GLenum target)
{
    switch(target) {
    case GL_TEXTURE_1D:                   return 0;
    case GL_TEXTURE_2D:                   return 1;
    case GL_TEXTURE_3D:                   return 2;
    case GL_TEXTURE_1D_ARRAY:             return 3;
    case GL_TEXTURE_2D_ARRAY:             return 4;
    case GL_TEXTURE_RECTANGLE:            return 5;
    case GL_TEXTURE_CUBE_MAP:             return 6;
    case GL_TEXTURE_2D_MULTISAMPLE:       return 7;
    case GL_TEXTURE_2D_MULTISAMPLE_ARRAY: return 8;
    default:
        DIAG_ASSERT(false);
        return -1;
    }
}

void gbShadowState_OGL::BindBuffer(GLenum target, GLuint buffer_id)
{
    int const idx = BufferTargetIndex(target);
    if(m_Buffers[idx] != buffer_id) {
        glBindBuffer(target, buffer_id);
        m_Buffers[idx] = buffer_id;
    }
}

void gbShadowState_OGL::ReleaseBuffer(GLenum target, GLuint buffer_id)
{
    int const idx = BufferTargetIndex(target);
    if(m_Buffers[idx] == buffer_id) {
        glBindBuffer(target, 0);
        m_Buffers[idx] = 0;
    }
}

void gbShadowState_OGL::BindVertexArray(GLuint arr)
{
    if(m_VertexArray != arr) {
        if(!m_VertexArray.isValid_) {
            // vertex array invalid, anything could have happened; invalidate element array buffer
            m_RootElementArray.isValid_ = false;
            m_Buffers[BufferTargetIndex(GL_ELEMENT_ARRAY_BUFFER)].isValid_ = false;
        } else if(m_VertexArray.id_ == 0) {
            // new vao is bound
            PushElementArrayBuffer();
        } else if(arr == 0) {
            // vao is reset to 0
            PopElementArrayBuffer();
        }
        glBindVertexArray(arr);
        m_VertexArray = arr;
    }
}

void gbShadowState_OGL::ReleaseVertexArray(GLuint arr)
{
    if(m_VertexArray == arr) {
        PopElementArrayBuffer();
        glBindVertexArray(0);
        m_VertexArray = 0;
    }
}

void gbShadowState_OGL::UseProgram(GLuint program)
{
    if(m_Program != program) {
        glUseProgram(program);
        m_Program = program;
    }
}

void gbShadowState_OGL::ReleaseProgram(GLuint program)
{
    if((m_Program == program) || (m_Program == 0)) {
        glUseProgram(0);
        m_Program = 0;
    }
}

void gbShadowState_OGL::ActiveTexture(GLuint texture_unit)
{
    if(texture_unit >= static_cast<GLuint>(m_MaxTextureUnits)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid texture unit requested" );
    }
    if(m_ActiveTextureUnit != texture_unit) {
        glActiveTexture(GL_TEXTURE0 + texture_unit);
        m_ActiveTextureUnit = texture_unit;
    }
}

void gbShadowState_OGL::BindTexture(GLuint texture_unit, GLenum target, GLuint texture)
{
    if(texture_unit >= static_cast<GLuint>(m_MaxTextureUnits)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Invalid texture unit requested" );
    }
    OpenGL_Id<GLuint>& tex_id = m_Textures[texture_unit][TextureTargetIndex(target)];
    if(tex_id != texture) {
        ActiveTexture(texture_unit);
        glBindTexture(target, texture);
        tex_id = texture;
    }
}

void gbShadowState_OGL::ReleaseTexture(GLenum target, GLuint texture)
{
    int const target_index = TextureTargetIndex(target);
    for(int i=0; i<m_MaxTextureUnits; ++i) {
        OpenGL_Id<GLuint>& tex_id = m_Textures[i][target_index];
        if(tex_id == texture) {
            ActiveTexture(i);
            glBindTexture(target, 0);
            tex_id = 0;
        }
    }
}

GLuint gbShadowState_OGL::AssignTextureUnit(GLuint texture, GLuint program)
{
    ///@todo assignment of texture units
    static GLuint id = 0;
    return id;
}

void gbShadowState_OGL::BlendFunc(GLenum sfactor, GLenum dfactor)
{
    if((m_BlendState.sfactor != sfactor) || (m_BlendState.dfactor != dfactor)) {
        glBlendFunc(sfactor, dfactor);
        m_BlendState.sfactor = sfactor;
        m_BlendState.dfactor = dfactor;
    }
}

void gbShadowState_OGL::BlendEquation(GLenum mode)
{
    if(m_BlendState.mode != mode) {
        glBlendEquation(mode);
        m_BlendState.mode = mode;
    }
}

void gbShadowState_OGL::PushElementArrayBuffer()
{
    m_RootElementArray = m_Buffers[BufferTargetIndex(GL_ELEMENT_ARRAY_BUFFER)];
    m_Buffers[BufferTargetIndex(GL_ELEMENT_ARRAY_BUFFER)].isValid_ = false;
}

void gbShadowState_OGL::PopElementArrayBuffer()
{
    m_Buffers[BufferTargetIndex(GL_ELEMENT_ARRAY_BUFFER)] = m_RootElementArray;
    m_RootElementArray.isValid_ = false;
}

}

#endif

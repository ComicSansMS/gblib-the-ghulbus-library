/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbGraphics/image_sampler.hpp>
#include <diagnostic/diagnostic.hpp>

#include <cmath>

namespace GB_GRAPHICS_NAMESPACE {
namespace ImageSampler {

Point::Point(ReadableSurface& surface)
    :m_Lock(surface), m_Width(surface.GetWidth()), m_Height(surface.GetHeight())
{
    m_Data = m_Lock.GetLockedData();
    m_Pitch = m_Lock.GetPitch();
}

Point::~Point()
{
}

GBCOLOR Point::Sample(float u, float v) const
{
    return m_Data[ static_cast<int>(v*m_Height)*m_Pitch + static_cast<int>(u*m_Width) ];
}

Bilinear::Bilinear(ReadableSurface& surface)
    :m_Lock(surface), m_Width(surface.GetWidth()), m_Height(surface.GetHeight())
{
    m_Data = m_Lock.GetLockedData();
    m_Pitch = m_Lock.GetPitch();
}

Bilinear::~Bilinear()
{
}

GBCOLOR Bilinear::Sample(float u, float v) const
{
    /* c1 c2
     * c3 c4
     */
    int const lx = static_cast<int>(u*m_Width);
    int const hx = lx + 1;
    int const ly = static_cast<int>(v*m_Height);
    int const hy = ly + 1;
    GBCOLOR const c1 = m_Data[ly*m_Pitch + lx];
    GBCOLOR const c2 = m_Data[ly*m_Pitch + hx];
    GBCOLOR const c3 = m_Data[hy*m_Pitch + lx];
    GBCOLOR const c4 = m_Data[hy*m_Pitch + hx];
    GBCOLOR_COMPONENT const tx = static_cast<GBCOLOR_COMPONENT>(255*(u - std::floor(u)));
    GBCOLOR_COMPONENT const ty = static_cast<GBCOLOR_COMPONENT>(255*(v - std::floor(v)));
    return gbColor::Lerp( gbColor::Lerp(c1, c2, tx), gbColor::Lerp(c3, c4, tx), ty);
}

gbImage MipMap::BuildMipMap(ReadableSurface& surface)
{
    int const width = surface.GetWidth();
    int const height = surface.GetHeight();

    if((width != height) || ((width|(width-1)) != (2*width)-1)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Mip maps can only be created for square textures with side length of power of two" );
    }


    GBCOLOR* data;
    int pitch;
    SurfaceLock<ReadableSurface> lock(surface, &data, &pitch);
    return gbImage(0, 0);
}

}
}

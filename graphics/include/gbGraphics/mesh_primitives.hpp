/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_MESH_PRIMITIVES_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_MESH_PRIMITIVES_HPP_

#include <gbGraphics/config.hpp>

#include <gbGraphics/index_data.hpp>
#include <gbGraphics/mesh.hpp>
#include <gbGraphics/rasterizer_state.hpp>
#include <gbGraphics/vertex_data.hpp>

#include <diagnostic/assert.hpp>
#include <diagnostic/exception.hpp>

#include <type_traits>

namespace GB_GRAPHICS_NAMESPACE {

    class gbDrawingContext3D;
    class gbVertexBinding;

    namespace Mesh {

        template<typename gbVertexFormat_T, typename IndexType_T=uint8_t,
                 Rasterizer::FaceOrientation_T VertexOrder=Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE>
        struct Quad {
        private:
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Position, gbVertexFormat_T>::type PositionDesc_T;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Color, gbVertexFormat_T>::type ColorDesc_T;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::TextureCoordinates, gbVertexFormat_T>::type TexCoordDesc_T;
            static_assert( !std::is_same<PositionDesc_T, VertexComponent::NilDesc>::value,
                           "Invalid vertex format: Need at least one component for Position" );
            typedef typename PositionDesc_T::Layout::value_type PositionVecValueType;
            typedef typename TexCoordDesc_T::Layout::value_type TexCoordVecValueType;
        public:
            gbVertexData<typename gbVertexFormat_T::VertexFormat> VertexData;
            gbIndexData_Triangle<IndexType_T> IndexData;
        public:
            Quad(PositionVecValueType width, PositionVecValueType height) {
                using GB_MATH_NAMESPACE::Vector2;
                VertexData.Resize(4);
                FillVertexPositions<PositionVecValueType>( Vector2<PositionVecValueType>(width, height) );
                FillTextureCoordinates<TexCoordVecValueType>();
                IndexData.Resize(2);
                FillIndexData(VertexOrder);
            }
            gbMesh CreateMesh(gbDrawingContext3D& dc3d, gbVertexBinding const& binding) {
                return gbMesh(dc3d, VertexData, IndexData, binding);
            }
        private:
            template<typename T>
            typename std::enable_if<std::is_same<typename PositionDesc_T::Layout, VertexComponent::Layout::Vec3<T>>::value, void>::type
            FillVertexPositions(GB_MATH_NAMESPACE::Vector2<T> const& dim) {
                using GB_MATH_NAMESPACE::Vector3;
                const T zero = static_cast<T>(0.0);
                VertexData::Get<PositionDesc_T>(VertexData, 0) = Vector3<T>(zero,   zero,   zero);
                VertexData::Get<PositionDesc_T>(VertexData, 1) = Vector3<T>(dim.x_, zero,   zero);
                VertexData::Get<PositionDesc_T>(VertexData, 2) = Vector3<T>(dim.x_, dim.y_, zero);
                VertexData::Get<PositionDesc_T>(VertexData, 3) = Vector3<T>(zero,   dim.y_, zero);
            }
            template<typename T>
            typename std::enable_if<std::is_same<typename PositionDesc_T::Layout, VertexComponent::Layout::Vec4<T>>::value, void>::type
            FillVertexPositions(GB_MATH_NAMESPACE::Vector2<T> const& dim) {
                using GB_MATH_NAMESPACE::Vector4;
                const T one = static_cast<T>(1.0);
                const T zero = static_cast<T>(0.0);
                VertexData::Get<PositionDesc_T>(VertexData, 0) = Vector4<T>(zero,   zero,   zero, one);
                VertexData::Get<PositionDesc_T>(VertexData, 1) = Vector4<T>(dim.x_, zero,   zero, one);
                VertexData::Get<PositionDesc_T>(VertexData, 2) = Vector4<T>(dim.x_, dim.y_, zero, one);
                VertexData::Get<PositionDesc_T>(VertexData, 3) = Vector4<T>(zero,   dim.y_, zero, one);
            }
            template<typename T>
            typename std::enable_if<
                (std::is_same<typename TexCoordDesc_T::Layout, VertexComponent::Layout::Vec2<T>>::value ||
                 std::is_same<typename TexCoordDesc_T::Layout, VertexComponent::Layout::TexCoordUV<T>>::value),
                void>::type
            FillTextureCoordinates()
            {
                using GB_MATH_NAMESPACE::Vector2;
                /*
                 (0,0)....(1,0)
                  |          |
                  |          |
                 (0,1)....(1,1)
                 */
                const T one = static_cast<T>(1.0);
                const T zero = static_cast<T>(0.0);
                VertexData::Get<TexCoordDesc_T>(VertexData, 0) = Vector2<T>(zero, one);
                VertexData::Get<TexCoordDesc_T>(VertexData, 1) = Vector2<T>(one, one);
                VertexData::Get<TexCoordDesc_T>(VertexData, 2) = Vector2<T>(one, zero);
                VertexData::Get<TexCoordDesc_T>(VertexData, 3) = Vector2<T>(zero, zero);
            }
            void FillIndexData(Rasterizer::FaceOrientation_T vertex_order) {
                if(vertex_order == Rasterizer::FACE_FRONT_CLOCKWISE) {
                    IndexData[0].i1 = 0;  IndexData[0].i2 = 1;  IndexData[0].i3 = 2;
                    IndexData[1].i1 = 0;  IndexData[1].i2 = 2;  IndexData[1].i3 = 3;
                } else {
                    DIAG_ASSERT(vertex_order == Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE);
                    IndexData[0].i1 = 0;  IndexData[0].i2 = 2;  IndexData[0].i3 = 1;
                    IndexData[1].i1 = 0;  IndexData[1].i2 = 3;  IndexData[1].i3 = 2;
                }
            }
        };

        template<typename gbVertexFormat_T, typename IndexType_T=uint8_t,
                 Rasterizer::FaceOrientation_T VertexOrder=Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE>
        struct Grid {
        private:
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Position, gbVertexFormat_T>::type PositionDesc_T;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Color, gbVertexFormat_T>::type ColorDesc_T;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::TextureCoordinates, gbVertexFormat_T>::type TexCoordDesc_T;
            static_assert( !std::is_same<PositionDesc_T, VertexComponent::NilDesc>::value,
                           "Invalid vertex format: Need at least one component for Position" );
            typedef typename PositionDesc_T::Layout::value_type PositionVecValueType;
            typedef typename TexCoordDesc_T::Layout::value_type TexCoordVecValueType;
        public:
            gbVertexData<typename gbVertexFormat_T::VertexFormat> VertexData;
            gbIndexData_Triangle<IndexType_T> IndexData;
        public:
            Grid(PositionVecValueType width, PositionVecValueType height, int n_segments_x, int n_segments_z) {
                using GB_MATH_NAMESPACE::Vector2;
                if((n_segments_x <= 0) || (n_segments_z <= 0)) {
                    DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                          "Number of grid segments must be greater 1" );
                }
                VertexData.Resize((n_segments_x+1) * (n_segments_z+1));
                FillVertexPositions<PositionVecValueType>( Vector2<PositionVecValueType>(width, height), n_segments_x+1, n_segments_z+1 );
                FillTextureCoordinates<TexCoordVecValueType>(n_segments_x+1, n_segments_z+1);
                IndexData.Resize(n_segments_x * n_segments_z * 2);
                FillIndexData(n_segments_x+1, n_segments_z+1, VertexOrder);
            }
            gbMesh CreateMesh(gbDrawingContext3D& dc3d, gbVertexBinding const& binding) {
                return gbMesh(dc3d, VertexData, IndexData, binding);
            }
        private:
            template<typename T>
            typename std::enable_if<std::is_same<typename PositionDesc_T::Layout, VertexComponent::Layout::Vec3<T>>::value, void>::type
            FillVertexPositions(GB_MATH_NAMESPACE::Vector2<T> const& dim, int seg_x, int seg_z) {
                GB_MATH_NAMESPACE::Vector3<T> v;
                T x = -dim.x_ / static_cast<T>(2.0);
                for(int i=0; i<seg_x; ++i) {
                    v.x_ = x;
                    float z = -dim.y_ / static_cast<T>(2.0);
                    for(int j=0; j<seg_z; ++j) {
                        v.z_ = z;
                        VertexData::Get<PositionDesc_T>(VertexData, seg_z*i + j) = v;
                        z += dim.y_ / static_cast<T>(seg_z-1);
                    }
                    x += dim.x_ / static_cast<T>(seg_x-1);
                }
            }
            template<typename T>
            typename std::enable_if<std::is_same<typename PositionDesc_T::Layout, VertexComponent::Layout::Vec4<T>>::value, void>::type
            FillVertexPositions(GB_MATH_NAMESPACE::Vector2<T> const& dim, int seg_x, int seg_z) {
                GB_MATH_NAMESPACE::Vector4<T> v;
                v.w_ = static_cast<T>(1.0);
                T x = -dim.x_ / static_cast<T>(2.0);
                for(int i=0; i<seg_x; ++i) {
                    v.x_ = x;
                    float z = -dim.y_ / static_cast<T>(2.0);
                    for(int j=0; j<seg_z; ++j) {
                        v.z_ = z;
                        VertexData::Get<PositionDesc_T>(VertexData, seg_z*i + j) = v;
                        z += dim.y_ / static_cast<T>(seg_z-1);
                    }
                    x += dim.x_ / static_cast<T>(seg_x-1);
                }
            }
            template<typename T>
            typename std::enable_if<
                (std::is_same<typename TexCoordDesc_T::Layout, VertexComponent::Layout::Vec2<T>>::value ||
                 std::is_same<typename TexCoordDesc_T::Layout, VertexComponent::Layout::TexCoordUV<T>>::value),
                void>::type
            FillTextureCoordinates(int seg_x, int seg_z)
            {
                for(int i=0; i<seg_x; ++i) {
                    for(int j=0; j<seg_z; ++j) {
                        VertexData::Get<TexCoordDesc_T>(VertexData, seg_z*i + j) =
                            GB_MATH_NAMESPACE::Vector2<T>( static_cast<T>(i)/static_cast<T>(seg_x-1),
                                                           static_cast<T>(j)/static_cast<T>(seg_z-1) );
                    }
                }
            }
            void FillIndexData(int seg_x, int seg_z, Rasterizer::FaceOrientation_T vertex_order) {
                if(vertex_order == Rasterizer::FACE_FRONT_CLOCKWISE) {
                    for(int i=0; i<seg_x-1; ++i) {
                        for(int j=0; j<seg_z-1; ++j) {
                            int const idx = 2 * (i*(seg_z-1) + j);
                            DIAG_ASSERT((idx >= 0) && (idx+1 < IndexData.NumberOfPrimitives()));
                            DIAG_ASSERT((i+1)*seg_z + j+1 < VertexData.NumberOfVertices());
                            IndexData[idx].i1 = static_cast<IndexType_T>(i*seg_z + j);
                            IndexData[idx].i2 = static_cast<IndexType_T>(i*seg_z + j + 1);
                            IndexData[idx].i3 = static_cast<IndexType_T>((i+1)*seg_z + j);
                            IndexData[idx+1].i1 = static_cast<IndexType_T>((i+1)*seg_z + j);
                            IndexData[idx+1].i2 = static_cast<IndexType_T>(i*seg_z + j+1);
                            IndexData[idx+1].i3 = static_cast<IndexType_T>((i+1)*seg_z + j+1);
                        }
                    }
                } else {
                    DIAG_ASSERT(vertex_order == Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE);
                    for(int i=0; i<seg_x-1; ++i) {
                        for(int j=0; j<seg_z-1; ++j) {
                            int const idx = 2 * (i*(seg_z-1) + j);
                            DIAG_ASSERT((idx >= 0) && (idx+1 < IndexData.NumberOfPrimitives()));
                            DIAG_ASSERT((i+1)*seg_z + j+1 < VertexData.NumberOfVertices());
                            IndexData[idx].i1 = static_cast<IndexType_T>(i*seg_z + j);
                            IndexData[idx].i2 = static_cast<IndexType_T>((i+1)*seg_z + j);
                            IndexData[idx].i3 = static_cast<IndexType_T>(i*seg_z + j + 1);
                            IndexData[idx+1].i1 = static_cast<IndexType_T>((i+1)*seg_z + j);
                            IndexData[idx+1].i2 = static_cast<IndexType_T>((i+1)*seg_z + j+1);
                            IndexData[idx+1].i3 = static_cast<IndexType_T>(i*seg_z + j+1);
                        }
                    }
                }
            }
        };

        template<typename gbVertexFormat_T, typename IndexType_T=uint8_t,
                 Rasterizer::FaceOrientation_T VertexOrder=Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE>
        struct Box {
        private:
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Position, gbVertexFormat_T>::type PositionDesc_T;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Color, gbVertexFormat_T>::type ColorDesc_T;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::TextureCoordinates, gbVertexFormat_T>::type TexCoordDesc_T;
            static_assert( !std::is_same<PositionDesc_T, VertexComponent::NilDesc>::value,
                           "Invalid vertex format: Need at least one component for Position" );
            typedef typename PositionDesc_T::Layout::value_type PositionVecValueType;
            typedef typename TexCoordDesc_T::Layout::value_type TexCoordVecValueType;
        public:
            gbVertexData<typename gbVertexFormat_T::VertexFormat> VertexData;
            gbIndexData_Triangle<IndexType_T> IndexData;
        public:
            Box(PositionVecValueType dim) {
                using GB_MATH_NAMESPACE::Vector3;
                Init(Vector3<PositionVecValueType>(-dim, -dim, -dim), Vector3<PositionVecValueType>(+dim, +dim, +dim));
            }
            Box(GB_MATH_NAMESPACE::AABB<PositionVecValueType> const& box) {
                Init(box.GetMin(), box.GetMax());
            }
            gbMesh CreateMesh(gbDrawingContext3D& dc3d, gbVertexBinding const& binding) {
                return gbMesh(dc3d, VertexData, IndexData, binding);
            }
        private:
            template<typename T>
            void Init(GB_MATH_NAMESPACE::Vector3<T> const& min, GB_MATH_NAMESPACE::Vector3<T> const& max)
            {
                VertexData.Resize(24);
                FillVertexPositions<PositionVecValueType>(min, max);
                FillTextureCoordinates<TexCoordVecValueType>();
                IndexData.Resize(12);
                FillIndexData(VertexOrder);
            }
            template<typename T>
            typename std::enable_if<std::is_same<typename PositionDesc_T::Layout, VertexComponent::Layout::Vec3<T>>::value, void>::type
            FillVertexPositions(GB_MATH_NAMESPACE::Vector3<T> const& min, GB_MATH_NAMESPACE::Vector3<T> const& max) {
                using GB_MATH_NAMESPACE::Vector3;
                //front
                VertexData::Get<PositionDesc_T>(VertexData,  0) = Vector3<T>(min.x_, min.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData,  1) = Vector3<T>(min.x_, max.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData,  2) = Vector3<T>(max.x_, max.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData,  3) = Vector3<T>(max.x_, min.y_, min.z_);
                //right
                VertexData::Get<PositionDesc_T>(VertexData,  4) = Vector3<T>(max.x_, min.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData,  5) = Vector3<T>(max.x_, max.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData,  6) = Vector3<T>(max.x_, max.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData,  7) = Vector3<T>(max.x_, min.y_, max.z_);
                //back
                VertexData::Get<PositionDesc_T>(VertexData,  8) = Vector3<T>(max.x_, min.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData,  9) = Vector3<T>(max.x_, max.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 10) = Vector3<T>(min.x_, max.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 11) = Vector3<T>(min.x_, min.y_, max.z_);
                //left
                VertexData::Get<PositionDesc_T>(VertexData, 12) = Vector3<T>(min.x_, min.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 13) = Vector3<T>(min.x_, max.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 14) = Vector3<T>(min.x_, max.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 15) = Vector3<T>(min.x_, min.y_, min.z_);
                //top
                VertexData::Get<PositionDesc_T>(VertexData, 16) = Vector3<T>(min.x_, max.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 17) = Vector3<T>(min.x_, max.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 18) = Vector3<T>(max.x_, max.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 19) = Vector3<T>(max.x_, max.y_, min.z_);
                //bottom
                VertexData::Get<PositionDesc_T>(VertexData, 20) = Vector3<T>(max.x_, min.y_, min.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 21) = Vector3<T>(max.x_, min.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 22) = Vector3<T>(min.x_, min.y_, max.z_);
                VertexData::Get<PositionDesc_T>(VertexData, 23) = Vector3<T>(min.x_, min.y_, min.z_);
            }
            template<typename T>
            typename std::enable_if<std::is_same<typename PositionDesc_T::Layout, VertexComponent::Layout::Vec4<T>>::value, void>::type
            FillVertexPositions(GB_MATH_NAMESPACE::Vector3<T> const& min, GB_MATH_NAMESPACE::Vector3<T> const& max) {
                using GB_MATH_NAMESPACE::Vector4;
                const T one = static_cast<T>(1.0);
                //front
                VertexData::Get<PositionDesc_T>(VertexData,  0) = Vector4<T>(min.x_, min.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData,  1) = Vector4<T>(min.x_, max.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData,  2) = Vector4<T>(max.x_, max.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData,  3) = Vector4<T>(max.x_, min.y_, min.z_, one);
                //right
                VertexData::Get<PositionDesc_T>(VertexData,  4) = Vector4<T>(max.x_, min.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData,  5) = Vector4<T>(max.x_, max.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData,  6) = Vector4<T>(max.x_, max.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData,  7) = Vector4<T>(max.x_, min.y_, max.z_, one);
                //back
                VertexData::Get<PositionDesc_T>(VertexData,  8) = Vector4<T>(max.x_, min.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData,  9) = Vector4<T>(max.x_, max.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 10) = Vector4<T>(min.x_, max.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 11) = Vector4<T>(min.x_, min.y_, max.z_, one);
                //left
                VertexData::Get<PositionDesc_T>(VertexData, 12) = Vector4<T>(min.x_, min.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 13) = Vector4<T>(min.x_, max.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 14) = Vector4<T>(min.x_, max.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 15) = Vector4<T>(min.x_, min.y_, min.z_, one);
                //top
                VertexData::Get<PositionDesc_T>(VertexData, 16) = Vector4<T>(min.x_, max.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 17) = Vector4<T>(min.x_, max.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 18) = Vector4<T>(max.x_, max.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 19) = Vector4<T>(max.x_, max.y_, min.z_, one);
                //bottom
                VertexData::Get<PositionDesc_T>(VertexData, 20) = Vector4<T>(max.x_, min.y_, min.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 21) = Vector4<T>(max.x_, min.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 22) = Vector4<T>(min.x_, min.y_, max.z_, one);
                VertexData::Get<PositionDesc_T>(VertexData, 23) = Vector4<T>(min.x_, min.y_, min.z_, one);
            }
            template<typename T>
            typename std::enable_if<
                (std::is_same<typename TexCoordDesc_T::Layout, VertexComponent::Layout::Vec2<T>>::value ||
                 std::is_same<typename TexCoordDesc_T::Layout, VertexComponent::Layout::TexCoordUV<T>>::value),
                void>::type
            FillTextureCoordinates()
            {
                using GB_MATH_NAMESPACE::Vector2;
                const T one = static_cast<T>(1.0);
                const T zero = static_cast<T>(0.0);
                for(int i=0; i<6; ++i) {
                    int const idx = i*4;
                    VertexData::Get<TexCoordDesc_T>(VertexData, idx) = Vector2<T>(zero, one);
                    VertexData::Get<TexCoordDesc_T>(VertexData, idx+1) = Vector2<T>(zero, zero);
                    VertexData::Get<TexCoordDesc_T>(VertexData, idx+2) = Vector2<T>(one, zero);
                    VertexData::Get<TexCoordDesc_T>(VertexData, idx+3) = Vector2<T>(one, one);
                }
            }
            void FillIndexData(Rasterizer::FaceOrientation_T vertex_order)
            {
                if(vertex_order == Rasterizer::FACE_FRONT_CLOCKWISE) {
                    for(IndexType_T i=0; i<6; ++i) {
                        IndexData[i*2].i1   = i*4;  IndexData[i*2].i2   = i*4+1;  IndexData[i*2].i3   = i*4+2;
                        IndexData[i*2+1].i1 = i*4;  IndexData[i*2+1].i2 = i*4+2;  IndexData[i*2+1].i3 = i*4+3;
                    }
                } else {
                    DIAG_ASSERT(vertex_order == Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE);
                    for(IndexType_T i=0; i<6; ++i) {
                        IndexData[i*2].i1   = i*4;  IndexData[i*2].i2   = i*4+2;  IndexData[i*2].i3   = i*4+1;
                        IndexData[i*2+1].i1 = i*4;  IndexData[i*2+1].i2 = i*4+3;  IndexData[i*2+1].i3 = i*4+2;
                    }
                }
            }
        };
    }

}

#endif

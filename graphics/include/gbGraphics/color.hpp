/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_COLOR_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_COLOR_HPP_

#include <gbGraphics/config.hpp>

#include <diagnostic/assert.hpp>

#include <cstdint>

namespace GB_GRAPHICS_NAMESPACE {

    /** 32bit RGBA color value
     */
    typedef uint32_t GBCOLOR;

    /** 8bit color channel value
     */
    typedef uint8_t GBCOLOR_COMPONENT;

    /** Color related operations
     */
    namespace gbColor {
        /** Structure describing the bit layout of GBCOLOR
         */
        struct ColorScheme {
            /** Color channel bitmasks
             */
            struct Mask_T {
                GBCOLOR R;        ///< Bitmask for the red color channel
                GBCOLOR G;        ///< Bitmask for the green color channel
                GBCOLOR B;        ///< Bitmask for the blue color channel
                GBCOLOR A;        ///< Bitmask for the alpha color channel
            } Mask;                ///< Bitmasks for the current color scheme
            /** Color Channel bit offsets
             */
            struct Offset_T {
                uint8_t R;        ///< Bit offset for the red color channel
                uint8_t G;        ///< Bit offset for the green color channel
                uint8_t B;        ///< Bit offset for the blue color channel
                uint8_t A;        ///< Bit offset for the alpha color channel
            } Offset;            ///< Bit offsets for the current color scheme
        };

        /** Currently active ColorScheme.
         * Results of all gbColor operations depend directly on this value.
         * Use SetActiveColorScheme() to set a new ColorScheme.
         */
        extern ColorScheme ACTIVE_SCHEME;

        /** Sets the value of ACTIVE_SCHEME, controlling all gbColor operations
         * @param[in] scheme ColorScheme that will become new ACTIVE_SCHEME
         * @attention Changes to the active color scheme affect the assumptions
         *            made on the bit layout of GBCOLOR, thus potentially invalidating
         *            previously created GBCOLOR values.
         * @note This function is automatically called by gbWindow::Create()
         *       ensuring the correct color scheme for the currently used graphics API.
         *       Call this with the value returned by gbWindow::GetAssociatedColorScheme()
         *       to ensure the correct color scheme for a window is active. Calling this
         *       function is usually unnecessary, except for when mixing incompatible
         *       color schemes from different APIs in the same application.
         */
        void SetActiveColorScheme(ColorScheme const& scheme);

        inline GBCOLOR XRGB(GBCOLOR_COMPONENT r, GBCOLOR_COMPONENT g, GBCOLOR_COMPONENT b) {
            return ( (r << ACTIVE_SCHEME.Offset.R) |
                     (g << ACTIVE_SCHEME.Offset.G) |
                     (b << ACTIVE_SCHEME.Offset.B) |
                     (0xff << ACTIVE_SCHEME.Offset.A) );
        }
        inline GBCOLOR XRGB(int r, int g, int b) {
            return ( ((r&0xff) << ACTIVE_SCHEME.Offset.R) |
                     ((g&0xff) << ACTIVE_SCHEME.Offset.G) |
                     ((b&0xff) << ACTIVE_SCHEME.Offset.B) |
                     (0xff << ACTIVE_SCHEME.Offset.A) );
        }
        inline GBCOLOR ARGB(GBCOLOR_COMPONENT a, GBCOLOR_COMPONENT r, GBCOLOR_COMPONENT g, GBCOLOR_COMPONENT b) {
            return ( (r << ACTIVE_SCHEME.Offset.R) |
                     (g << ACTIVE_SCHEME.Offset.G) |
                     (b << ACTIVE_SCHEME.Offset.B) |
                     (a << ACTIVE_SCHEME.Offset.A) );
        }
        inline GBCOLOR ARGB(int a, int r, int g, int b) {
            return ( ((r&0xff) << ACTIVE_SCHEME.Offset.R) |
                     ((g&0xff) << ACTIVE_SCHEME.Offset.G) |
                     ((b&0xff) << ACTIVE_SCHEME.Offset.B) |
                     ((a&0xff) << ACTIVE_SCHEME.Offset.A) );
        }
        inline GBCOLOR_COMPONENT GetR(GBCOLOR c) {
            return static_cast<GBCOLOR_COMPONENT>( (c>>ACTIVE_SCHEME.Offset.R) & 0xff );
        }
        inline GBCOLOR_COMPONENT GetG(GBCOLOR c) {
            return static_cast<GBCOLOR_COMPONENT>( (c>>ACTIVE_SCHEME.Offset.G) & 0xff );
        }
        inline GBCOLOR_COMPONENT GetB(GBCOLOR c) {
            return static_cast<GBCOLOR_COMPONENT>( (c>>ACTIVE_SCHEME.Offset.B) & 0xff );
        }
        inline GBCOLOR_COMPONENT GetA(GBCOLOR c) {
            return static_cast<GBCOLOR_COMPONENT>( (c>>ACTIVE_SCHEME.Offset.A) & 0xff );
        }
        inline GBCOLOR MaskR() {
            return ACTIVE_SCHEME.Mask.R;
        }
        inline GBCOLOR MaskG() {
            return ACTIVE_SCHEME.Mask.G;
        }
        inline GBCOLOR MaskB() {
            return ACTIVE_SCHEME.Mask.B;
        }
        inline GBCOLOR MaskA() {
            return ACTIVE_SCHEME.Mask.A;
        }

        inline GBCOLOR_COMPONENT fast_div_by_255(int i) {
            DIAG_ASSERT((i >= 0) && (i < 65535));
            return static_cast<GBCOLOR_COMPONENT>(((i+1) + (i>>8)) >> 8);
        }

        inline GBCOLOR Mult(GBCOLOR c, GBCOLOR_COMPONENT t)
        {
            int const R = GetR(c);
            int const G = GetG(c);
            int const B = GetB(c);
            int const A = GetA(c);
            return ARGB( fast_div_by_255(A*t),
                         fast_div_by_255(R*t),
                         fast_div_by_255(G*t),
                         fast_div_by_255(B*t) );
        }

        inline GBCOLOR Mult(GBCOLOR c, GBCOLOR_COMPONENT t, GBCOLOR mask)
        {
            int const R = GetR(c);
            int const G = GetG(c);
            int const B = GetB(c);
            int const A = GetA(c);
            GBCOLOR res = ARGB( fast_div_by_255(A*t),
                                fast_div_by_255(R*t),
                                fast_div_by_255(G*t),
                                fast_div_by_255(B*t) );
            return (res & mask) | (c & (~mask));
        }

        inline GBCOLOR Lerp(GBCOLOR c1, GBCOLOR c2, GBCOLOR_COMPONENT t)
        {
            int const R1 = GetR(c1);
            int const G1 = GetG(c1);
            int const B1 = GetB(c1);
            int const A1 = GetA(c1);

            int const R2 = GetR(c2);
            int const G2 = GetG(c2);
            int const B2 = GetB(c2);
            int const A2 = GetA(c2);

            return ARGB( fast_div_by_255((255-t)*A1 + t*A2),
                         fast_div_by_255((255-t)*R1 + t*R2),
                         fast_div_by_255((255-t)*G1 + t*G2),
                         fast_div_by_255((255-t)*B1 + t*B2) );
        }

        /** @defgroup color_conversion Macros for color space conversions
         * @{
         */
        /** Convert hsv to rgb
         * @param[in] c GBCOLOR with hsv (Hue/Saturation/Value) values
         * @return GBCOLOR with rgb values
         */
        inline GBCOLOR HSV2RGB(GBCOLOR c)
        {
            ///@todo implement without floats
            float r, g, b, f, p, q, t;;
            float h = static_cast<float>(GetR(c)) / 255.0f;
            float s = static_cast<float>(GetG(c)) / 255.0f;
            float v = static_cast<float>(GetB(c)) / 255.0f;
            float a = static_cast<float>(GetA(c)) / 255.0f;

            if(s == 0.0f) {            //gray
                r = g = b = v;
            } else {                //chromatic
                h *= 6.0f;
                f = h - static_cast<int>(h);        //the fractional part of h
                p = v * (1.0f - s);
                q = v * (1.0f - (s * f));
                t = v * (1.0f - (s * (1 - f)));
                if(h < 1.0f) {
                    r = v; g = t; b = p;
                } else if(h < 2.0f) {
                    r = q; g = v; b = p;
                } else if(h < 3.0f) {
                    r = p; g = v; b = t;
                } else if(h < 4.0f) {
                    r = p; g = q; b = v;
                } else if(h < 5.0f) {
                    r = t; g = p; b = v;
                } else {
                    r = v; g = p; b = q;
                }
            }
            return ( ARGB(static_cast<int>(a * 255.0f),
                          static_cast<int>(r * 255.0f),
                          static_cast<int>(g * 255.0f),
                          static_cast<int>(b * 255.0f)) );
        }

        /** Convert rgb to hsv
         * @param c GBCOLOR with rgb values
         * @return GBCOLOR with hsv (Hue/Saturation/Value) values
         */
        inline GBCOLOR RGB2HSV(GBCOLOR c)
        {
            ///@todo implement without floats
            float h, s, v;
            float r = static_cast<float>(GetR(c)) / 255.0f;
            float g = static_cast<float>(GetG(c)) / 255.0f;
            float b = static_cast<float>(GetB(c)) / 255.0f;
            float a = static_cast<float>(GetA(c)) / 255.0f;
            float max_color, min_color;
            if( r>g ) {
                if(r>b) {        // r>g, r>b
                    max_color = r;
                    min_color = (g<b)?g:b;
                } else {        // b>r>g
                    max_color = b;
                    min_color = g;
                }
            } else {
                if(g>b) {    // g>r, g>b
                    max_color = g;
                    min_color = (r<b)?r:b;
                } else {    // b>g>r
                    max_color = b;
                    min_color = r;
                }
            }

            v = max_color;

            if(max_color == 0.0f) {
                s = 0.0f;
            } else {
                s = (max_color - min_color) / max_color;
            }

            if(s == 0.0f) {
                h = 0.0f;
            } else {
                if(r == max_color) {
                    h = (g - b) / (max_color - min_color);
                } else if(g == max_color) {
                    h = 2.0f + (b - r) / (max_color - min_color);
                } else {
                    h = 4.0f + (r - g) / (max_color - min_color);
                }
                h /= 6.0f;
                if (h < 0.0f) { h += 1.0f; }
            }

            return ( ARGB(static_cast<int>(a * 255.0f),
                          static_cast<int>(h * 255.0f),
                          static_cast<int>(s * 255.0f),
                          static_cast<int>(v * 255.0f)) );
        }

        /** Convert hsl to rgb
         * @param c GBCOLOR with hsl (Hue/Saturation/Lightness) values
         * @return GBCOLOR with rgb values
         */
        inline GBCOLOR HSL2RGB(GBCOLOR c)
        {
            ///@todo implement without floats
            float r, g, b, t1, t2, tr, tg, tb;;
            float h = static_cast<float>(GetR(c)) / 255.0f;
            float s = static_cast<float>(GetG(c)) / 255.0f;
            float l = static_cast<float>(GetB(c)) / 255.0f;
            float a = static_cast<float>(GetA(c)) / 255.0f;

            if(s == 0.0f) {            //gray
                r = g = b = l;
            } else {                //chromatic
                if(l < 0.5f) { t2 = l * (1 + s); } else { t2 = (l + s) - (l * s); }
                t1 = 2.0f * l - t2;
                tr = h + 1.0f / 3.0f;
                tg = h;     
                tb = h - 1.0f / 3.0f;
                if(tr > 1.0f) { tr -= 1.0f; }
                if(tb < 0.0f) { tb += 1.0f; }

                //red
                if(tr < 1.0f/6.0f) { r = t1 + (t2 - t1) * 6.0f * tr; }
                else if(tr < 0.5f) { r = t2; }
                else if(tr < 2.0f/3.0f) { r = t1 + (t2 - t1) * ((2.0f / 3.0f) - tr) * 6.0f; }
                else { r = t1; }

                //green       
                if(tg < 1.0f/6.0f) { g = t1 + (t2 - t1) * 6.0f * tg; }
                else if(tg < 0.5f) { g = t2; }
                else if(tg < 2.0f/3.0f) { g = t1 + (t2 - t1) * ((2.0f / 3.0f) - tg) * 6.0f; }
                else { g = t1; }

                //blue    
                if(tb < 1.0f/6.0f) { b = t1 + (t2 - t1) * 6.0f * tb; }
                else if(tb < 0.5f) { b = t2; }
                else if(tb < 2.0f/3.0f) { b = t1 + (t2 - t1) * ((2.0f / 3.0f) - tb) * 6.0f; }
                else { b = t1; }
            }

            return ( ARGB(static_cast<int>(a * 255.0f),
                          static_cast<int>(r * 255.0f),
                          static_cast<int>(g * 255.0f),
                          static_cast<int>(b * 255.0f)) );
        }

        /** Convert rgb to hsl
         * @param c GBCOLOR with rgb values
         * @return GBCOLOR with hsl (Hue/Saturation/Lightness) values
         */
        inline GBCOLOR RGB2HSL(GBCOLOR c)
        {
            ///@todo implement without floats
            float h, s, l;
            float r = static_cast<float>(GetR(c)) / 255.0f;
            float g = static_cast<float>(GetG(c)) / 255.0f;
            float b = static_cast<float>(GetB(c)) / 255.0f;
            float a = static_cast<float>(GetA(c)) / 255.0f;
            float max_color, min_color;
            if( r>g ) {
                if(r>b) {        // r>g, r>b
                    max_color = r;
                    min_color = (g<b)?g:b;
                } else {        // b>r>g
                    max_color = b;
                    min_color = g;
                }
            } else {
                if(g>b) {    // g>r, g>b
                    max_color = g;
                    min_color = (r<b)?r:b;
                } else {    // b>g>r
                    max_color = b;
                    min_color = r;
                }
            }
            if(min_color == max_color) {        //gray
                h = 0.0f;
                s = 0.0f;
                l = r;
            } else {                            //chromatic
                l = (min_color + max_color) / 2.0f;
                if(l < 0.5f) {
                    s = (max_color - min_color) / (max_color + min_color);
                } else {
                    s = (max_color - min_color) / (2.0f - max_color - min_color);
                }

                if(max_color == r) {
                    h = (g - b) / (max_color - min_color);
                } else if(max_color == g) {
                    h = 2.0f + (b - r) / (max_color - min_color);
                } else {
                    h = 4.0f + (r - g) / (max_color - min_color);
                }
            }

            return ( ARGB(static_cast<int>(a * 255.0f),
                          static_cast<int>(h * 255.0f),
                          static_cast<int>(s * 255.0f),
                          static_cast<int>(l * 255.0f)) );
        }
        ///@}
    }

}

#ifdef GB_GRAPHICS_HAS_SDL
#    define GB_GRAPHICS_INCLUDE_TOKEN_COLOR_SDL_HPP_
#    include <gbGraphics/color_SDL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_COLOR_SDL_HPP_
#endif

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXTURE2D_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXTURE2D_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/lockable_surface.hpp>
#include <gbGraphics/rect.hpp>
#include <gbGraphics/texture_desc.hpp>

#include <memory>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward Declarations
     * @{
     */
    class gbTexture2D;
    typedef std::shared_ptr<gbTexture2D> gbTexture2DPtr;
    class gbSamplerState;
    typedef std::shared_ptr<gbSamplerState> gbSamplerStatePtr;
    /// @}

    class gbTexture2D: public LockableSurface, public boost::noncopyable {
    public:
        enum LockOptions {
            LOCK_OPTIONS_DEFAULT,
            LOCK_OPTIONS_UNINITIALIZED
        };
    public:
        virtual ~gbTexture2D() {}
        virtual void LockWithOptions(GBCOLOR** data, int* pitch, Rect const& rect, LockOptions opt)=0;
        virtual void SetTextureData(ReadableSurface& image)=0;
        virtual void SetTextureSubData(Rect const& dest, ReadableSurface& image)=0;
        virtual void BindSamplerState(gbSamplerStatePtr sampler_state)=0;
        virtual gbSamplerStatePtr GetSamplerState() const=0;
    };

}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_TEXTURE2D_OGL_HPP_
#    include <gbGraphics/texture2d_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_TEXTURE2D_OGL_HPP_
#endif

#endif

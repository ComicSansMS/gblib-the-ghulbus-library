/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_MESH_LOADER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_MESH_LOADER_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/mesh.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbMeshLoader;

    class gbDrawingContext3D;
    class gbVertexBinding;
    /// @}

    class gbMeshLoader {
    public:
        /** Destructor
         */
        virtual ~gbMeshLoader() {}
        /** Create a new mesh from the loaded data
         * @param[in] dc3d Drawing context
         * @param[in] binding Vertex format binding
         * @return New mesh
         */
        virtual gbMesh CreateMesh(gbDrawingContext3D& dc3d, gbVertexBinding const& binding) const=0;
        /** Check whether normal data is available
         * @return true indicates that a mesh generated through CreateMesh() will have
         *         normal data. On false, such a mesh will either have uninitialized
         *         normals or no normals at all, depending on the used vertex format.
         */
        virtual bool HasVertexNormals() const=0;
        /** Check whether texture coordinate data is available
         * @return true indicates that a mesh generated through CreateMesh() will have
         *         texture coordinates. On false, such a mesh will either have uninitialized
         *         texture data or no texture data at all, depending on the used vertex format.
         */
        virtual bool HasTextureCoordinates() const=0;
    };

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_BUFFER_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_BUFFER_OGL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_VERTEX_BUFFER_OGL_HPP_
#    error Do not include vertex_buffer_OGL.hpp directly; Include vertex_buffer.hpp instead
#endif

#include <GL/glew.h>
#include <gbUtil/resource_observer.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbGeometry_OGL;
    class gbShadowState_OGL;

    class gbVertexBuffer_OGL: public gbVertexBuffer {
    private:
        GLuint m_BufferId;
        int m_Size;
        gbShadowState_OGL* m_glShadowState;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbGeometry_OGL> m_GeometryList;
        std::unique_ptr<gbVertexFormatBase> m_VertexFormat;
    public:
        gbVertexBuffer_OGL(GLuint buffer_id, int size, gbShadowState_OGL* shadow_state);
        ~gbVertexBuffer_OGL();
        /** @name gbVertexBuffer
         * @{
         */
        int GetSize() const;
        void SetVertexData(gbVertexDataBase const& data);
        gbGeometryPtr CreateGeometry(gbVertexBinding const& bindings);
        gbGeometryPtr CreateGeometry(gbVertexBinding const& bindings, gbIndexBuffer& index_buffer);
        gbVertexFormatBase const* GetVertexFormat() const;
        /// @}
        GLuint GetOGLVertexBufferId_OGL() const;
    };

}

#endif

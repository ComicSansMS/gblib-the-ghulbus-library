/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_EVENT_PROCESSOR_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_EVENT_PROCESSOR_HPP_

#include <gbGraphics/config.hpp>

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbEventManager;
    class gbWindow;

    class gbEventProcessor : boost::noncopyable {
    private:
        gbWindow* m_ActiveWindow;
        bool m_done;                              ///< true after SetDone() was 
    public:
        /** Constructor.
         */
        gbEventProcessor();
        /** Destructor
         */
        virtual ~gbEventProcessor() {}
        /** A message callback that is expected to be called once every frame.
         * @note You may also call this indirectly through gbGraphicsManager::ProcessMessages().
         *       Ensure however that you do not call it more than once!
         */
        virtual void ProcessMessages() = 0;
        /** Get the status of the m_done flag.
         * @return true if SetDone() was called before, false otherwise.
         */
        bool IsDone() const;
        /** Set the m_done flag.
         */
        void SetDone();
        /** Get the window that is currently active.
         * @return The currently active window associated with all events.
         */
        gbWindow* GetActiveWindow();
    protected:
        void SetActiveWindow(gbWindow* new_active_window);
    };

}

#ifdef GB_GRAPHICS_HAS_SDL
#    define GB_GRAPHICS_INCLUDE_TOKEN_EVENT_PROCESSOR_SDL_HPP_
#    include <gbGraphics/event_processor_SDL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_EVENT_PROCESSOR_SDL_HPP_
#endif

#endif

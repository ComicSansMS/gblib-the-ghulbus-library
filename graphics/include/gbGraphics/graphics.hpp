/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_GRAPHICS_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_GRAPHICS_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/blend_state.hpp>
#include <gbGraphics/graphics_2d.hpp>
#include <gbGraphics/draw2d.hpp>
#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/event_manager.hpp>
#include <gbGraphics/event_processor.hpp>
#include <gbGraphics/font.hpp>
#include <gbGraphics/font_simplemonospaced.hpp>
#include <gbGraphics/geometry.hpp>
#include <gbGraphics/graphics_exception.hpp>
#include <gbGraphics/image.hpp>
#include <gbGraphics/index_buffer.hpp>
#include <gbGraphics/mesh.hpp>
#include <gbGraphics/program.hpp>
#include <gbGraphics/rasterizer_state.hpp>
#include <gbGraphics/rect.hpp>
#include <gbGraphics/sampler_state.hpp>
#include <gbGraphics/shader.hpp>
#include <gbGraphics/sprite.hpp>
#include <gbGraphics/sprite_segmenter.hpp>
#include <gbGraphics/text.hpp>
#include <gbGraphics/texture2d.hpp>
#include <gbGraphics/texture_atlas.hpp>
#include <gbGraphics/texture_desc.hpp>
#include <gbGraphics/uniform_data.hpp>
#include <gbGraphics/vertex_buffer.hpp>
#include <gbGraphics/vertex_binding.hpp>
#include <gbGraphics/window.hpp>

/** GhulbusGraphics namespace
 */
namespace GB_GRAPHICS_NAMESPACE {
}

#endif

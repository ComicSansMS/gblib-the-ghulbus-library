/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TURTLE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TURTLE_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward Declarations
     * @{
     */
    class gbTurtle;
    class gbDraw2D;
    /// @}

    /**
     * @brief Turtle Graphics
     * 
     * A simple plotter for Logo-style turtle graphics
     */
    class gbTurtle {
    private:
        gbDraw2D* m_draw2D;        ///< gbDraw2D structure used for painting
        double    m_posX;        ///< X position in turtle coordinates
        double    m_posY;        ///< Y position in turtle coordinates
        GBCOLOR   m_penColor;    ///< current pen-color
        double    m_angle;    ///< angle relative to positive turtle-coordinate-x-axis
        bool      m_penDown;    ///< if the pen is up, the turtle won't paint while moving
        double    m_width;
        double      m_height;
    public:
        /** Constructor.
         * @param draw_2d A gbDraw2D drawing object
         * @throw Ghulbus::gbException GB_ILLEGALPARAMETER
         */
        gbTurtle(gbDraw2D& draw_2d);
        /** Destructor
         */
        ~gbTurtle();

        /** @name Moving the turtle:
         * @{
         */
        /** Move the turtle.
         *
         * Moves the turtle in the direction it is currently facing.
         * @param units Number of pixel the turtle is moving forward.
         *              If negative, the turtle moves backwards.
         */
        void Go(int units);
        /** Turn the turtle to the left.
         *
         * @param iAngle Angle that the turtle shall turn in degrees
         */
        void TurnLeft(int iAngle);
        /** Turn the turtle to the right.
         *
         * @param iAngle Angle that the turtle shall turn in degrees
         */
        void TurnRight(int iAngle);
        /** Turn the turtle at a specific angle
         *
         * @param iAngle Angle that the turtle shall be facing in degrees
         */
        void SetAngle(int iAngle);
        /** Turn the turtle to the left.
         *
         * @param fAngle Angle that the turtle shall turn in radians
         */
        void TurnLeftRad(double fAngle);
        /** Turn the turtle to the right.
         *
         * @param fAngle Angle that the turtle shall turn in radians
         */
        void TurnRightRad(double fAngle);
        /** Turn the turtle at a specific angle
         *
         * @param fAngle Angle that the turtle shall be facing in radians
         */
        void SetAngleRad(double fAngle);
        /** Set the turtle to a specific position
         *
         * @param X New X coordinate
         * @param Y New Y coordinate
         */
        void Goto(int X, int Y);
        /** Reset the turtle to its initial position, angle and color.
         * @attention No drawing occurs when resetting the turtle!
         */
        void Reset();
        //////////////////////////////////////////////////////////////////// @}
        

        /** @name Manipulating the pen:
         * @{
         */
        /** Set the pen's color.
         *
         * @param r Specifying the new color's red component (0-255)
         * @param g Specifying the new color's green component (0-255)
         * @param b Specifying the new color's blue component (0-255)
         */
        void SetColor(GBCOLOR_COMPONENT r, GBCOLOR_COMPONENT g, GBCOLOR_COMPONENT b);
        /** Set the pen's color.
         *
         * @param c The new color as a GBCOLOR
         */
        void SetColor(GBCOLOR c);
        /** Move the pen up.
         */
        void PenUp();
        /** Move the pen down.
         */
        void PenDown();
        /** Toggle the pen.
         */
        void PenToggle();
        //////////////////////////////////////////////////////////////////// @}


        /** @name Functions for status-checking:
         * @{
         */
        /** Check the status of the pen.
         *
         * @return true if the pen is down, false if not.
         */
        bool IsPenDown();
        /** Draw a representation of the turtle showing its position and angle.
         */
        void Show();
        /** Get the position of the turtle in X.
         * 
         * @return The current X coordinate of the turtle's position.
         */
        int GetPosX();
        /** Get the position of the turtle in Y.
         * 
         * @return The current Y coordinate of the turtle's position.
         */
        int GetPosY();
        /** Get the angle in which the turtle is currently facing.
         *
         * @return The angle in which the turtle is currently directed in degrees.
         */
        int GetAngle();
        /** Get the angle in which the turtle is currently facing.
         *
         * @return The angle in which the turtle is currently directed in radians.
         */
        double GetAngleRad();
        /// @}
    private:
        /** Convert turtle-coordinates to gb2D-screen-coordinates
         */
        inline double XCord(double xRaw) { return ((m_width /2.0) + xRaw); }
        /** Convert turtle-coordinates to gb2D-screen-coordinates
         */
        inline double YCord(double yRaw) { return ((m_height/2.0) - yRaw); }
    };

}

#endif

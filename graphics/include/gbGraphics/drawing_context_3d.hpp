/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_DRAWING_CONTEXT_3D_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_DRAWING_CONTEXT_3D_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>
#include <gbGraphics/drawing_context_base.hpp>
#include <gbGraphics/shader_type.hpp>

#include <iosfwd>
#include <memory>
#include <string>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward Declarations
     * @{
     */
    class gbDrawingContext3D;
    typedef std::shared_ptr<gbDrawingContext3D> gbDrawingContext3DPtr;
    class gbShader;
    typedef std::shared_ptr<gbShader> gbShaderPtr;
    class gbProgram;
    typedef std::shared_ptr<gbProgram> gbProgramPtr;
    class gbVertexBuffer;
    typedef std::shared_ptr<gbVertexBuffer> gbVertexBufferPtr;
    class gbGeometry;
    class gbIndexBuffer;
    typedef std::shared_ptr<gbIndexBuffer> gbIndexBufferPtr;
    class gbVertexDataBase;
    class gbIndexDataBase;
    class gbTexture2D;
    typedef std::shared_ptr<gbTexture2D> gbTexture2DPtr;
    namespace Texture {
        struct TextureDesc2D;
    }
    class gbRasterizerState;
    typedef std::shared_ptr<gbRasterizerState> gbRasterizerStatePtr;
    namespace Rasterizer {
        struct StateDesc;
    }
    class gbBlendState;
    typedef std::shared_ptr<gbBlendState> gbBlendStatePtr;
    namespace Blending {
        struct StateDesc;
    }
    class gbSamplerState;
    typedef std::shared_ptr<gbSamplerState> gbSamplerStatePtr;
    namespace Sampling {
        struct StateDesc;
    }
    /// @}

    /** @defgroup gb3d 3D Graphics
     */

    /** Drawing context for 3d graphics
     * @ingroup gb3d
     */
    class gbDrawingContext3D: public boost::noncopyable, public gbDrawingContext_Base {
    public:
        /** Information about the renderer associated with a gbDrawingContext3D.
         * This information is usually supplied by the graphics card driver
         * and may vary between platforms.
         */
        struct RendererInfo {
            /** Version Number
             * A version number consists of three parts: Major.Minor.Release
             */
            struct VersionNumber_T {
                int Major;            /// Major version
                int Minor;            /// Minor version
                int Release;        /// Release version
            };
            std::string Name;                                    ///< Name of the renderer
            std::string Vendor;                                    ///< Vendor string
            std::string Version;                                ///< Version string
            VersionNumber_T VersionNumber;                        ///< Version number
            std::string ShadingLanguageVersion;                    ///< Shading Language version string
            VersionNumber_T ShadingLanguageVersionNumber;        ///< Shading Language version number
        };
    public:
        /** Destructor
         */
        virtual ~gbDrawingContext3D() {}
        /** Retrieve information about the rendering device
         * @return RendererInfo struct for the renderer associated with the drawing context
         */
        virtual RendererInfo GetRendererInfo()=0;
        virtual gbRasterizerStatePtr CreateRasterizerState(Rasterizer::StateDesc const& state_desc)=0;
        virtual void BindRasterizerState(gbRasterizerStatePtr rasterizer_state)=0;
        virtual gbRasterizerStatePtr GetCurrentRasterizerState() const=0;
        virtual gbBlendStatePtr CreateBlendState(Blending::StateDesc const& state_desc)=0;
        virtual void BindBlendState(gbBlendStatePtr blend_state)=0;
        virtual gbBlendStatePtr GetCurrentBlendState() const=0;
        virtual gbSamplerStatePtr CreateSamplerState(Sampling::StateDesc const& state_desc)=0;
        virtual gbSamplerStatePtr GetDefaultSamplerState() const=0;
        virtual void SetDepthTest(bool enable_depth_test)=0;
        /** Set the clear value for the depth buffer.
         * Calls to Clear() will fill the depth buffer with the clear value.
         * Default clear value is 1.0.
         * @param[in] f Depth clear value; must be from range [0..1]
         */
        virtual void SetClearDepthValue(float f)=0;
        /** Create a shader from source code string
         * @param[in] type Type of the new shader
         * @param[in] code Source code for the shader
         * @return New shader object
         * @note Shader creation is handled differently by 3d APIs. If you just
         *       want to load a shader from file, use CreateProgramFromFile() for
         *       maximum portability.
         */
        virtual gbShaderPtr CreateShader(ShaderType::ShaderType_T type, std::string const& code)=0;
        /** Create a shader from file
         * @param[in] type Type of the new shader
         * @param[in] filename Path the file containing shader source code
         * @return New shader object
         * @note Shader creation is handled differently by 3d APIs. If you just
         *       want to load a shader from file, use CreateProgramFromFile() for
         *       maximum portability.
         */
        gbShaderPtr CreateShaderFromFile(ShaderType::ShaderType_T type, std::string const& filename);
        /** Create a new program
         * @return A new (empty) program object
         */
        virtual gbProgramPtr CreateProgram()=0;
        /** Create a program from file.
         * The filename passed to CreateProgramFromFile() is interpreted
         * differently, depending on the used 3d API.
         * For OpenGL, it searches for the vertex shader in a file <filename>.vert
         * and for the fragment shader in a file <filename>.frag. It loads and
         * compiles all shaders from file and links them to the final program.
         * If the vertex or fragment shader files are not found, an exception is thrown.
         * @param[in] filename Path to the shader source files
         * @return New program object.
         */
        virtual gbProgramPtr CreateProgramFromFile(std::string const& filename)=0;
        /** Create a new vertex buffer
         * @param[in] size The desired size in bytes of the new vertex buffer
         * @return New vertex buffer
         */
        virtual gbVertexBufferPtr CreateVertexBuffer(int size)=0;
        /** Create a new index buffer
         * @param[in] n_indices The desired size in bytes of the new index buffer
         * @return New index buffer
         */
        virtual gbIndexBufferPtr CreateIndexBuffer(int size)=0;
        /** Create a new texture
         * @param[in] desc2d Texture descriptor
         * @return New texture
         */
        virtual gbTexture2DPtr CreateTexture(Texture::TextureDesc2D const& desc2d)=0;
        /** Create a new texture
         * @param[in] desc2d Texture descriptor
         * @param[in] sampler_state Sampler state used when sampling from the texture
         * @return New texture
         */
        virtual gbTexture2DPtr CreateTexture(Texture::TextureDesc2D const& desc2d, gbSamplerStatePtr sampler_state)=0;
        /** Invalidate the shadow state.
         * This function invalidates the underlying shadow state that is used to
         * minimize the number of state changes invoked by the drawing context. Under
         * normal circumstances, it should never be necessary to call this function.
         * If however you break encapsulation of the resources managed by gb_lib, you
         * will need to call this whenever state of the 3d API is changed through
         * a function that is not part of gb_lib.
         * @example For example, if you manually call OpenGL's glBindBuffer and
         *          do not undo your changes before invoking a function from gb_lib,
         *          you do need to call InvalidateState().
         */
        virtual void InvalidateState()=0;
        /** Manually draw geometry
         * @param[in] geometry A geometry object
         */
        virtual void Draw(gbGeometry const& geometry)=0;
    };

    /** Ostream inserter for version numbers in RendererInfo
     */
    std::ostream& operator<<(std::ostream& os, gbDrawingContext3D::RendererInfo::VersionNumber_T const& v);
}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_DRAWING_CONTEXT_3D_OGL_HPP_
#    include <gbGraphics/drawing_context_3d_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_DRAWING_CONTEXT_3D_OGL_HPP_
#endif

#endif

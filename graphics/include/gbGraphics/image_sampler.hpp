/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_IMAGE_SAMPLER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_IMAGE_SAMPLER_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>
#include <gbGraphics/image.hpp>
#include <gbGraphics/lockable_surface.hpp>


namespace GB_GRAPHICS_NAMESPACE {

    class gbImageSampler {
    public:
        virtual ~gbImageSampler() {};
        virtual GBCOLOR Sample(float u, float v) const=0;
    };

    namespace ImageSampler {
        class Point: public gbImageSampler {
        private:
            SurfaceLock<ReadableSurface> m_Lock;
            int const m_Width;
            int const m_Height;
            GBCOLOR* m_Data;
            int m_Pitch;
        public:
            Point(ReadableSurface& surface);
            ~Point();
            GBCOLOR Sample(float u, float v) const;
        };

        class Bilinear: public gbImageSampler {
        private:
            SurfaceLock<ReadableSurface> m_Lock;
            int const m_Width;
            int const m_Height;
            GBCOLOR* m_Data;
            int m_Pitch;
        public:
            Bilinear(ReadableSurface& surface);
            ~Bilinear();
            GBCOLOR Sample(float u, float v) const;
        };

        class MipMap: public gbImageSampler {
        public:
            static gbImage BuildMipMap(ReadableSurface& surface);
        };

        class MipMapBilinear: public gbImageSampler {
        };

        class MipMapTrilinear: public gbImageSampler {
        };
    }

}

#endif

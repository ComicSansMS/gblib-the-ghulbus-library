/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXT_MANIPULATORS_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXT_MANIPULATORS_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>
#include <gbGraphics/text_stream.hpp>

#include <typeinfo>

namespace GB_GRAPHICS_NAMESPACE {
    class gbFont;

    /*
     * Manipulator call chain:
     *  actual manipulator generates object of type nArgManipulator, carrying the arguments
     *  ostream inserter for nArgManipulator calls
     *  Invoke_nArgManipulator template function which calls gbText::TargetFun()
     */

    namespace Text {
        namespace ManipulatorImpl {
            /** Payload carrier for single argument manipulator
             * @tparam T argument type
             * @tparam T_tag Manipulator tag type
             */
            template<typename T, typename T_tag>
            class SingleArgManipulator {
            public:
                T arg_;
                SingleArgManipulator(T arg): arg_(arg) {}
            };

            /** Manipulator invokator. Called by ostream inserter, calls gbText::TargetFun
             * @tparam T ostream character type
             * @tparam T_ManipImpl Payload carrier type (required because tag type is unknown)
             * @tparam T_Payload Argument type
             * @tparam Target_Fun Pointer-to-member to gbText::TargetFun()
             */
            template<typename T, typename T_ManipImpl, typename T_Payload, void(GB_GRAPHICS_NAMESPACE::Text::BasicTextStream<T>::*Target_Fun)(T_Payload)>
            inline std::basic_ostream<T>& InvokeSingleArgManipulator(std::basic_ostream<T>& ostr_, T_ManipImpl const& manip)  {
                try {
                    auto stream = dynamic_cast<GB_GRAPHICS_NAMESPACE::Text::BasicTextStream<T>*>( &ostr_ );
                    stream->flush();
                    ((stream)->*(Target_Fun))(manip.arg_);
                } catch(std::bad_cast&) {
                }
                return ostr_;
            }

            template<typename T1, typename T2, typename T_tag>
            class DoubleArgManipulator {
            public:
                T1 arg1_;
                T2 arg2_;
                DoubleArgManipulator(T1 arg1, T2 arg2): arg1_(arg1), arg2_(arg2) {}
            };

            template<typename T, typename T_ManipImpl, typename T_Payload1, typename T_Payload2, void(GB_GRAPHICS_NAMESPACE::Text::BasicTextStream<T>::*Target_Fun)(T_Payload1, T_Payload2)>
            inline std::basic_ostream<T>& InvokeDoubleArgManipulator(std::basic_ostream<T>& ostr_, T_ManipImpl const& manip)  {
                try {
                    auto stream = dynamic_cast<GB_GRAPHICS_NAMESPACE::Text::BasicTextStream<T>*>( &ostr_ );
                    stream->flush();
                    ((stream)->*(Target_Fun))(manip.arg1_, manip.arg2_);
                } catch(std::bad_cast&) {
                }
                return ostr_;
            }
        }
    }
}

/** Macro for constructing a manipulator with no arguments for gbText
 * @param name Name of the manipulator
 * @param target_fun Name of the target function (must be a member function of gbText)
 */
#define ZERO_ARG_MANIPULATOR(name, target_fun) \
    namespace GB_GRAPHICS_NAMESPACE { namespace Text { \
    template<typename T> inline std::basic_ostream<T>& name(std::basic_ostream<T>& ostr_) { \
        try { \
            auto stream = dynamic_cast<GB_GRAPHICS_NAMESPACE::Text::BasicTextStream<T>*>(&ostr_); \
            stream->target_fun(); \
        } catch( std::bad_cast& ) { \
        } \
        return ostr_; \
    } } }

/** Macro instantiating the Payload carrier template.
 * This macro generates two classes: An empty tag class of type tag_##name and an instance
 * of SingleArgManipulator Impl_##name, both under the ManipulatorImpl namespace
 * @param type Argument type
 * @param name Name of the manipulator
 */
#define INSTANTIATE_SINGLE_ARG_MANIPULATOR(type, name) \
    namespace GB_GRAPHICS_NAMESPACE { namespace Text { namespace ManipulatorImpl { \
    struct tag_##name; typedef SingleArgManipulator<type, tag_##name> Impl_##name; \
    } \
    inline ManipulatorImpl::Impl_##name name(type arg) { return ManipulatorImpl::Impl_##name(arg); }  \
    } }

/** Macro instantiating the ostream inserter function.
 * This macro generates an ostream inserter function which calls InvokeSingleArgManipulator()
 * @param name Name of the manipulator
 * @param type Argument type
 * @param target_fun Name of the target function (must be a member function of gbText)
 */
#define GENERATE_SINGLE_ARG_OSTREAM_INSERTER(name, type, target_fun) template<typename T> std::basic_ostream<T>& operator<< \
    (std::basic_ostream<T>& ostr_, GB_GRAPHICS_NAMESPACE::Text::ManipulatorImpl::Impl_##name const& manip)  { \
        return GB_GRAPHICS_NAMESPACE::Text::ManipulatorImpl::InvokeSingleArgManipulator< \
                    T, \
                    GB_GRAPHICS_NAMESPACE::Text::ManipulatorImpl::Impl_##name, \
                    type, \
                    &GB_GRAPHICS_NAMESPACE::Text::BasicTextStream<T>::target_fun \
                > (ostr_, manip); \
    }

/** Macro for constructing a new single argument manipulator for gbText.
 * @param name Name of the manipulator
 * @param type Argument type
 * @param target_fun Name of the target function (must be a member function of gbText)
 */
#define SINGLE_ARG_MANIPULATOR(name, argument_type, target_fun) \
    INSTANTIATE_SINGLE_ARG_MANIPULATOR(argument_type, name) \
    GENERATE_SINGLE_ARG_OSTREAM_INSERTER(name, argument_type, target_fun)

#define INSTANTIATE_DOUBLE_ARG_MANIPULATOR(type1, type2, name) \
    namespace GB_GRAPHICS_NAMESPACE { namespace Text { namespace ManipulatorImpl { \
    struct tag_##name; typedef DoubleArgManipulator<type1, type2, tag_##name> Impl_##name; \
    } \
    inline ManipulatorImpl::Impl_##name name(type1 arg1, type2 arg2) { return ManipulatorImpl::Impl_##name(arg1, arg2); }  \
    } }

#define GENERATE_DOUBLE_ARG_OSTREAM_INSERTER(name, type1, type2, target_fun) template<typename T> std::basic_ostream<T>& operator<< \
    (std::basic_ostream<T>& ostr_, GB_GRAPHICS_NAMESPACE::Text::ManipulatorImpl::Impl_##name const& manip)  { \
        return GB_GRAPHICS_NAMESPACE::Text::ManipulatorImpl::InvokeDoubleArgManipulator< \
                    T, \
                    GB_GRAPHICS_NAMESPACE::Text::ManipulatorImpl::Impl_##name, \
                    type1, \
                    type2, \
                    &GB_GRAPHICS_NAMESPACE::Text::BasicTextStream<T>::target_fun \
                > (ostr_, manip); \
    }

#define DOUBLE_ARG_MANIPULATOR(name, argument_type1, argument_type2, target_fun) \
    INSTANTIATE_DOUBLE_ARG_MANIPULATOR(argument_type1, argument_type2, name) \
    GENERATE_DOUBLE_ARG_OSTREAM_INSERTER(name, argument_type1, argument_type2, target_fun)



ZERO_ARG_MANIPULATOR  (clear,                                           Clear)
SINGLE_ARG_MANIPULATOR(clear,           GB_GRAPHICS_NAMESPACE::GBCOLOR, Clear)
SINGLE_ARG_MANIPULATOR(set_text_color,  GB_GRAPHICS_NAMESPACE::GBCOLOR, SetTextColor)
SINGLE_ARG_MANIPULATOR(set_tab_width,   int,                            SetTabWidthPixels)
SINGLE_ARG_MANIPULATOR(set_line_indent, int,                            SetNewLineIndentation)
SINGLE_ARG_MANIPULATOR(set_font,        GB_GRAPHICS_NAMESPACE::gbFont*, SetFont)
DOUBLE_ARG_MANIPULATOR(set_cursor,      int, int,                       SetCursorPosition)



#undef ZERO_ARG_MANIPULATOR

#undef SINGLE_ARG_MANIPULATOR
#undef INSTANTIATE_SINGLE_ARG_OSTREAM_INSERTER
#undef INSTANTIATE_SINGLE_ARG_MANIPULATOR

#undef DOUBLE_ARG_MANIPULATOR
#undef INSTANTIATE_DOUBLE_ARG_OSTREAM_INSERTER
#undef INSTANTIATE_DOUBLE_ARG_MANIPULATOR


#endif

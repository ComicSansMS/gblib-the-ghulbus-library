/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_EVENT_MANAGER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_EVENT_MANAGER_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/event.hpp>

#include <gbUtil/timer.hpp>

#include <functional>
#include <memory>
#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

    class gbWindow;

    /** The Event Manager
     */
    class gbEventManager {
    public:
        typedef unsigned int HandlerId;
        template<typename Handler_T>
        class RegisteredHandler {
        private:
            static HandlerId next_id;
            HandlerId id_;
            Handler_T handler_;
        public:
            RegisteredHandler(Handler_T const& event_handler)
                : id_(next_id++), handler_(event_handler)
            {}
            HandlerId getId() const { return id_; }
            Handler_T getHandler() const { return handler_; };
        };
    private:
        /** @name Mouse Input
         * @{
         */
        typedef RegisteredHandler<Events::MouseInputHandler> RegisteredMouseInput;
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_All;         ///< list for all mouse events
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_Move;        ///< list for mouse move events
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_ButtonDown;  ///< list for mouse button down events
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_ButtonUp;    ///< list for mouse button up events
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_MouseWheel;  ///< list for mousewheel events
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_MoveRel;     ///< list for relative mouse move events
        /* @todo
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_Click;        ///< list for mouse click events
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_DClick;       ///< list for mouse double click events
        std::vector<RegisteredMouseInput> m_InputHandlerMouse_Drag;         ///< list for mouse drag events
        */
        /// @}
        Events::MouseState m_MouseState;          ///< current mouse state
        /** @name Keyboard Input
         * @{
         */
        typedef RegisteredHandler<Events::KeyboardInputHandler> RegisteredKeyboardInput;
        std::vector<RegisteredKeyboardInput> m_InputHandlerKeyboard_All;        ///< list for all keyboard events
        std::vector<RegisteredKeyboardInput> m_InputHandlerKeyboard_KeyDown;    ///< list for keyboard key down events
        std::vector<RegisteredKeyboardInput> m_InputHandlerKeyboard_KeyUp;      ///< list for keyboard key up events
        /* @todo
         textinput event
        */
        Events::KeyboardState m_KeyboardState;    ///< internal struct resembling current keyboard state
        /// @}
        /** @name Window Events
         * @{
         */
        typedef RegisteredHandler<Events::WindowEventHandler> RegisteredWindowEvent;
        std::vector<RegisteredWindowEvent> m_WindowEventHandler_All;           ///< list for window events
        /// @}
        GB_UTIL_NAMESPACE::gbTimer m_Timer;       ///< timer (for event timestamps)
        /** Timestamps for various events
         */
        struct m_Timestamps_T {
            GB_UTIL_NAMESPACE::Timestamp mouse_move;
            GB_UTIL_NAMESPACE::Timestamp mouse_lbuttondown;
            GB_UTIL_NAMESPACE::Timestamp mouse_rbuttondown;
            GB_UTIL_NAMESPACE::Timestamp mouse_mbuttondown;
            GB_UTIL_NAMESPACE::Timestamp mouse_lbuttonup;
            GB_UTIL_NAMESPACE::Timestamp mouse_rbuttonup;
            GB_UTIL_NAMESPACE::Timestamp mouse_mbuttonup;
            GB_UTIL_NAMESPACE::Timestamp mouse_mousewheel;
            GB_UTIL_NAMESPACE::Timestamp keyboard[Events::GBKEY_NUMBER_OF_KEYSTATES];
        } m_Timestamps;
        /// @}
    public:
        gbEventManager();
        /** Destructor
         */
        ~gbEventManager();
        /** Get timestamp from the gbEventManager's timer.
         * @return The current timestamp as generated by the m_Timer.
         */
        GB_UTIL_NAMESPACE::Timestamp GetTimestamp() const;
        /** Add an event handler for mouse input.
         * @param[in] handler A callback function for handling mouse input events.
         * @param[in] type The type of events that the handler will be called on (must be GBEVENT_MOUSE_*).
         * @return An id identifying the handler. Required for removing the handler.
         * @see RemoveEventHandler_MouseInput()
         * @throw Ghulbus::gbException GB_ILLEGALPARAMETER
         */
        HandlerId AddEventHandler_MouseInput(Events::MouseInputHandler const& handler, Events::gbEvent_Type_Mouse type);
        /** Add an event handler for mouse input.
         * @param[in] handler An event handler object implementing the Events::MouseInput interface.
         * @param[in] type The type of events that the handler will be called on (must be GBEVENT_MOUSE_*).
         * @return An id identifying the handler. Required for removing the handler.
         * @see RemoveEventHandler_MouseInput()
         * @throw Ghulbus::gbException GB_ILLEGALPARAMETER
         */
        HandlerId AddEventHandler_MouseInput(Events::MouseInput* handler, Events::gbEvent_Type_Mouse type);
        /** Remove an event handler for mouse input.
         * @param[in] id Id of the handler that is to be removed.
         */
        void RemoveEventHandler_MouseInput(HandlerId id);
        /** Add an event handler for keyboard input.
         * @param[in] handler A callback function for handling keyboard input events.
         * @param[in] type The type of events that the handler will be called on (must be GBEVENT_KEYB_*).
         * @return An id identifying the handler. Required for removing the handler.
         * @see RemoveEventHandler_KeyboardInput()
         * @throw Ghulbus::gbException GB_ILLEGALPARAMETER
         */
        HandlerId AddEventHandler_KeyboardInput(Events::KeyboardInputHandler const& handler, Events::gbEvent_Type_Keyboard type);
        /** Add an event handler for keyboard input.
         * @param[in] handler An event handler object implementing the Events::KeyboardInput interface.
         * @param[in] type The type of events that the handler will be called on (must be GBEVENT_KEYB_*).
         * @see RemoveEventHandler_KeyboardInput()
         * @throw Ghulbus::gbException GB_ILLEGALPARAMETER
         */
        HandlerId AddEventHandler_KeyboardInput(Events::KeyboardInput* handler, Events::gbEvent_Type_Keyboard type);
        /** Remove an event handler for keyboard input.
         * @param[in] id Id of the handler that is to be removed.
         */
        void RemoveEventHandler_KeyboardInput(HandlerId id);
        /** Add an event handler for window events.
         * @param[in] handler A callback function for handling window events.
         * @return An id identifying the handler. Required for removing the handler.
         * @see RemoveEventHandler_WindowEvent()
         */
        HandlerId AddEventHandler_WindowEvent(Events::WindowEventHandler const& handler);
        /** Add an event handler for window events.
         * @param[in] handler The handler object implementing the Events::WindowEvent interface
         * @return An id identifying the handler. Required for removing the handler.
         * @see RemoveEventHandler_WindowEvent()
         */
        HandlerId AddEventHandler_WindowEvent(Events::WindowEvent* handler);
        /** Remove an event handler for window events.
         * @param[in] id Id of the handler that is to be removed.
         */
        void RemoveEventHandler_WindowEvent(HandlerId id);
        /** Get the current mouse state
         * @return The current mouse state
         */
        Events::MouseState const* QueryMouseState() const;
        /** Get the current keyboard state
         * @return The current keyboard state
         */
        Events::KeyboardState const* QueryKeyboardState() const;
        /** Get the current state of a keyboard key
         * @param[in] k Keycode of the queried key
         * @return true if the key is currently pressed, false otherwise
         */
        bool QueryKeyState(Events::Key k) const;
        /** @name Low Level Input Events (called directly by the ProcessMessages())
         * @{
         */
        /** The mouse was moved
         * @param[in] e The new mouse position, button flags indicate which buttons are currently down
         * @return True if the event was processed, false otherwise
         */
        bool SignalEvent_MouseMove(Events::MouseState* e);
        /** The mouse was moved
         * @param[in] e Offset to the old mouse position, button flags indicate which buttons are currently down
         * @return True if the event was processed, false otherwise
         * @note The conditions for SignalEvent_MouseMoveRel() are slightly different than those for
         *       SignalEvent_MouseMove(). Relative move events are not fired if the mouse is
         *       cursor is moved by the user or by the mouse capture routine, as is
         *       expected by a handler for e.g. implementing a first-person-shooter control. Conventional
         *       mouse move events are fired always, so that a handler is at all times aware of the
         *       absolute position of the cursor. See also: @ref gbWindow::SetRelativeMouseMode().
         */
        bool SignalEvent_MouseMoveRel(Events::MouseState* e);
        /** A mouse button was pressed
         * @param[in] e The mouse position, button flags indicate which button was pressed
         * @return True if the event was processed, false otherwise
         */
        bool SignalEvent_MouseButtonDown(Events::MouseState* e);
        /** A mouse button was released
         * @param[in] e The mouse position, button flags indicate which button was released
         * @return True if the event was processed, false otherwise
         */
        bool SignalEvent_MouseButtonUp(Events::MouseState* e);
        /** The mousewheel was used
         * @param[in] e mousewheel field resembles action (e.g. +1: 1 up; -3: 3 down; etc.)
         * @return True if the event was processed, false otherwise
         */
        bool SignalEvent_MouseWheel(Events::MouseState* e);
        /** A keyboard key was pressed
         * @param[in] k Code of the key that was pressed
         * @return True if the event was processed, false otherwise
         */
        bool SignalEvent_KeyboardKeyDown(Events::Key k);
        /** A keyboard key was released
         * @param[in] k Code of the key that was released
         * @return True if the event was processed, false otherwise
         */
        bool SignalEvent_KeyboardKeyUp(Events::Key k);
        /// @}
        /** @name Higher level Input events (called by a lower level ProcessEvent_*())
         * @{
         */

        /// @}
        
        /** Simulate a window event.
         * \attention 
         */
        void SignalEvent_WindowEvent(Events::gbEvent_Type_WindowEvent type, Events::WindowState const* w);
    private:
        void ClearState();
        std::vector<RegisteredMouseInput>& GetHandlerList_MouseInput(Events::gbEvent_Type_Mouse type);
        std::vector<RegisteredKeyboardInput>& GetHandlerList_KeyboardInput(Events::gbEvent_Type_Keyboard type);
    };
}

#endif

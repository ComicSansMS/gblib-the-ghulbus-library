/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXT_RENDERER_TEXTURE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXT_RENDERER_TEXTURE_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/text_renderer.hpp>

#include <gbGraphics/lockable_surface.hpp>
#include <gbGraphics/text_stream.hpp>

#include <gbUtil/missing_features.hpp>

namespace GB_GRAPHICS_NAMESPACE {
    namespace Text {

        template<typename T> class BasicTextStream;

        class TextRenderer_Texture: public TextRenderer {
        private:
            gbFont* m_Font;
            SurfaceLock<LockableSurface> m_TargetLock;
            GBCOLOR* m_Data;
            int m_Width;
            int m_Height;
            int m_Pitch;
            int m_LineIndent;
            int m_TabWidth;
            GB_MATH_NAMESPACE::Vector2i m_CursorPos;
            GBCOLOR m_ColorTxt;
            bool m_Badbit;
        public:
            TextRenderer_Texture(gbFont* font, LockableSurface& target);
            TextRenderer_Texture(gbFont* font, SurfaceLock<LockableSurface> const& target);
            ~TextRenderer_Texture();
            /** @name TextRenderer_Base
             * @{
             */
            bool RenderChar(int c);
            void SetFont(gbFont* font);
            void SetTextColor(GBCOLOR c);
            void SetCursorPosition(int x, int y);
            void SetTabWidthPixels(int px_width);
            void SetNewLineIndentation(int px_indent);
            void Clear();
            void Clear(GBCOLOR c);
            GB_MATH_NAMESPACE::Vector2i GetCursorPosition() const;
            /// @}
        private:
            bool RenderGlyph(int c);
            void LineBreak();
        };

        template<typename T>
        BasicTextStream<T> Create(gbFont* font, LockableSurface& target)
        {
            return BasicTextStream<T>(std::make_unique<TextRenderer_Texture>(font, target));
        }

        template<typename T>
        BasicTextStream<T> Create(gbFont* font, SurfaceLock<LockableSurface> const& target)
        {
            return BasicTextStream<T>(std::make_unique<TextRenderer_Texture>(font, target));
        }

    }
}

#endif

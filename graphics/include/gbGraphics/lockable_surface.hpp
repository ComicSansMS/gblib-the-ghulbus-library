/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_LOCKABLE_SURFACE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_LOCKABLE_SURFACE_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>
#include <diagnostic/exception.hpp>

#include <memory>

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {
    class Rect;

    class LockableSurface {
    public:
        virtual ~LockableSurface() {};
        virtual void Lock(GBCOLOR** data, int* pitch)=0;
        virtual void LockRect(Rect const& rect, GBCOLOR** data, int* pitch)=0;
        virtual void Unlock()=0;
        virtual bool IsLocked() const=0;
        virtual int GetWidth() const=0;
        virtual int GetHeight() const=0;
    };

    class ReadableSurface: public LockableSurface {
    public:
        virtual ~ReadableSurface() {};
    };

    namespace Lock {
        struct adopt_lock_T {};
        adopt_lock_T const adopt_lock = {};
    }

    template<typename T>
    class SurfaceLock: boost::noncopyable {
    private:
        std::shared_ptr<T> m_Ownership;
        T* m_Surface;
        GBCOLOR* m_Data;
        int m_Pitch;
    public:
        SurfaceLock()
            :m_Ownership(nullptr), m_Surface(nullptr), m_Data(nullptr), m_Pitch(0)
        {
        }
        explicit SurfaceLock(T& surface)
            :m_Ownership(nullptr), m_Surface(nullptr), m_Data(nullptr), m_Pitch(0)
        {
            Lock(surface);
        }
        SurfaceLock(T& surface, Lock::adopt_lock_T)
            :m_Ownership(nullptr), m_Surface(nullptr), m_Data(nullptr), m_Pitch(0)
        {
            Lock(surface, Lock::adopt_lock);
        }
        explicit SurfaceLock(std::shared_ptr<T>& surface_ptr)
            :m_Ownership(nullptr), m_Surface(nullptr), m_Data(nullptr), m_Pitch(0)
        {
            Lock(surface_ptr);
        }
        SurfaceLock(std::shared_ptr<T>& surface_ptr, Lock::adopt_lock_T)
            :m_Ownership(nullptr), m_Surface(nullptr), m_Data(nullptr), m_Pitch(0)
        {
            Lock(surface_ptr, Lock::adopt_lock);
        }
        SurfaceLock(T& surface, GBCOLOR** data, int* pitch)
            :m_Ownership(nullptr), m_Surface(nullptr), m_Data(nullptr), m_Pitch(0)
        {
            Lock(surface);
            *data = m_Data;
            *pitch = m_Pitch;
        }
        SurfaceLock(std::shared_ptr<T>& surface_ptr, GBCOLOR** data, int* pitch)
            :m_Ownership(nullptr), m_Surface(nullptr), m_Data(nullptr), m_Pitch(0)
        {
            Lock(surface_ptr);
            *data = m_Data;
            *pitch = m_Pitch;
        }
        ~SurfaceLock()
        {
            if(m_Surface) {
                m_Surface->Unlock();
            }
        }
        inline T* operator->() const
        {
            return m_Surface;
        }
        inline GBCOLOR* GetLockedData() const
        {
            return m_Data;
        }
        inline int GetPitch() const
        {
            return m_Pitch;
        }
        inline void Lock(T& surface)
        {
            if(m_Surface) {
                DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot lock occupied SurfaceLock");
            }
            m_Surface = &surface;
            m_Surface->Lock(&m_Data, &m_Pitch);
        }
        inline void Lock(std::shared_ptr<T>& surface_ptr)
        {
            if(m_Surface) {
                DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot lock occupied SurfaceLock");
            }
            m_Ownership = surface_ptr;
            m_Surface = surface_ptr.get();
            m_Surface->Lock(&m_Data, &m_Pitch);
        }
        inline void Lock(T& surface, Lock::adopt_lock_T)
        {
            if(m_Surface) {
                DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot lock occupied SurfaceLock");
            }
            if(!surface.IsLocked())
            {
                DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot adapt lock to surface that was not locked");
            }
            m_Surface = &surface;
        }
        inline void Lock(std::shared_ptr<T>& surface_ptr, Lock::adopt_lock_T)
        {
            if(m_Surface) {
                DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot lock occupied SurfaceLock");
            }
            if(!surface_ptr->IsLocked())
            {
                DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidProtocol(), "Cannot adapt lock to surface that was not locked");
            }
            m_Ownership = surface_ptr;
            m_Surface = surface_ptr.get();
        }
        inline void Reset()
        {
            if(m_Surface) { m_Surface->Unlock(); }
            m_Ownership.reset();
            m_Surface = nullptr;
            m_Data = nullptr;
            m_Pitch = 0;
        }
    };

    void CopySurface(LockableSurface& dst, ReadableSurface& src);

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_CHARACTER_SET_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_CHARACTER_SET_HPP_

#include <gbGraphics/config.hpp>

#include <diagnostic/exception.hpp>

#include <iterator>
#include <set>

namespace GB_GRAPHICS_NAMESPACE {

    namespace Text {
        template<typename CharType_T>
        class CharacterSet {
        private:
            std::set<CharType_T> m_CharSet;
        public:
            CharacterSet()
            {
            }
            CharacterSet(CharacterSet const& rhs)
                :m_CharSet(rhs.m_CharSet)
            {
            }
            CharacterSet(CharacterSet&& rhs)
                :m_CharSet(std::move(rhs.m_CharSet))
            {
            }
            void AddCharacter(CharType_T c)
            {
                m_CharSet.insert(c);
            }
            void AddCharacterRange(CharType_T first, CharType_T last)
            {
                if(first > last) {
                    DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::InvalidArgument(),
                                         "Passed empty range");
                }
                for(CharType_T it = first; it <= last; ++it)
                {
                    m_CharSet.insert(it);
                }
            }
            int NumberOfCharacters() const
            {
                return static_cast<int>(m_CharSet.size());
            }
            CharType_T GetCharacter(int index) const
            {
                auto it = m_CharSet.cbegin();
                std::advance(it, index);
                return *it;
            }
            int GetCharacterIndex(CharType_T c) const
            {
                auto const it = m_CharSet.find(c);
                return (it == m_CharSet.end())?(-1):(std::distance(m_CharSet.begin(), it));
            }
        };
    }

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_FONTMAP_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_FONTMAP_HPP_

#include <gbGraphics/config.hpp>

#include <gbGraphics/character_set.hpp>
#include <gbGraphics/drawing_context_3d.hpp>
#include <gbGraphics/font.hpp>
#include <gbGraphics/image.hpp>
#include <gbGraphics/lockable_surface.hpp>
#include <gbGraphics/texture_atlas.hpp>

#include <algorithm>
#include <utility>

namespace GB_GRAPHICS_NAMESPACE {

    template<typename CharType_T>
    class gbFontmap {
    public:
        Text::CharacterSet<CharType_T> const m_Charset;
        gbTextureAtlas m_FontmapAtlas;
    public:
        gbFontmap(gbDrawingContext3D& dc3d, Text::CharacterSet<CharType_T> const& character_set, gbFont* font)
            :m_Charset(character_set), m_FontmapAtlas(dc3d, BuildFontmapSubtextures(font).first)
        {
        }
    private:
        std::pair<std::vector<ReadableSurface*>, std::vector<gbImage>> BuildFontmapSubtextures(gbFont* font)
        {
            const int n_chars = m_Charset.NumberOfCharacters();
            std::vector<gbImage> font_imgs;
            font_imgs.reserve(n_chars);
            std::vector<ReadableSurface*> ptrs;
            ptrs.reserve(n_chars);
            for(int i=0; i<n_chars; ++i)
            {
                font->ResetKerning();
                font->LoadGlyph(m_Charset.GetCharacter(i));
                font->RenderGlyph();
                int const glyph_width = font->GetGlyphWidth();
                int const glyph_height = font->GetGlyphHeight();
                font_imgs.push_back(gbImage(glyph_width, glyph_height));
                ptrs.push_back(&font_imgs.back());
                SurfaceLock<gbImage> lock(font_imgs.back());
                for(int y=0; y<glyph_height; ++y) {
                    for(int x=0; x<glyph_width; ++x) {
                        GBCOLOR_COMPONENT c = font->SampleGlyph(x, y);
                        lock.GetLockedData()[y * lock.GetPitch() + x] = gbColor::XRGB(c, c, c);
                    }
                }
            }
            return std::make_pair(std::move(ptrs), std::move(font_imgs));
        }
    };

}
#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXT_RENDERER_GEOMETRY_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXT_RENDERER_GEOMETRY_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/text_renderer.hpp>

#include <gbGraphics/fontmap.hpp>
#include <gbGraphics/text_stream.hpp>
#include <gbGraphics/vertex_data.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    namespace Text {
#if 0
        template<typename T, typename VertexFormat_T>
        class TextRenderer_Geometry: public TextRenderer {
        private:
            gbVertexData<VertexFormat_T>* m_VertexData;
            gbFontmap<T>* m_Fontmap;
            GBCOLOR m_TextColor;
            GB_MATH_NAMESPACE::Vector2i m_CursorPos;
            GBCOLOR m_ColorTxt;
        public:
            TextRenderer_Geometry(gbFontmap<T>* fontmap, gbVertexData<VertexFormat_T>& vertex_data)
                :m_VertexData(&vertex_data), m_Fontmap(fontmap), m_CursorPos(0, 0), m_ColorTxt(gbColor::XRGB(255, 255, 255))
            {
            }
            ~TextRenderer_Geometry()
            {
            }
            /** @name TextRenderer_Base
             * @{
             */
            bool RenderChar(int c)
            {
                auto rect = m_Fontmap->m_FontmapAtlas.GetSubtextureRect(m_Fontmap->m_Charset.GetCharacterIndex(static_cast<T>(c)));
                const int base = m_VertexData.NumberOfVertices();
                // todo
                m_VertexData.Resize(base + 4);
                VertexData::Get<VertexComponent::Position3f>(VertexData, base).x = 0.0f;
                VertexData::Get<VertexComponent::Position3f>(VertexData, base).y = 0.0f;
                VertexData::Get<VertexComponent::Position3f>(VertexData, base).z = 0.0f;
            }
            void SetFont(gbFont* font)
            {
                m_Font = font;
            }
            void SetTextColor(GBCOLOR c)
            {
                m_ColorTxt = c;
            }
            void SetCursorPosition(int x, int y)
            {
                m_CursorPos.x_ = x;
                m_CursorPos.y_ = y;
            }
            void SetTabWidthPixels(int /* px_width */)
            {
                /* @todo */
            }
            void SetNewLineIndentation(int /* px_indent */)
            {
                /* @todo */
            }
            void Clear()
            {
                /* @todo */
            }
            void Clear(GBCOLOR /* c */)
            {
                    /* @todo */
            }
            GB_MATH_NAMESPACE::Vector2i GetCursorPosition() const
            {
                return m_CursorPos;
            }
            /// @}
        };


        template<typename T, typename VertexFormat_T>
        BasicTextStream<T> Create(gbFontmap<T>* fontmap, gbVertexData<VertexFormat_T>& target)
        {
            return BasicTextStream<T>(std::make_unique<TextRenderer_Geometry<T, VertexFormat_T>>(fontmap, target));
        }
#endif

    }

}

#endif

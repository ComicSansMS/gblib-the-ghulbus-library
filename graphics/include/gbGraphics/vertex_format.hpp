/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_FORMAT_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_FORMAT_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/vertex_format_base.hpp>
#include <gbGraphics/vertex_data_storage.gen.hpp>

#include <gbMath/vector2.hpp>
#include <gbMath/vector3.hpp>
#include <gbMath/vector4.hpp>

#include <diagnostic/exception.hpp>

#include <gbUtil/cxx11_features.hpp>

#include <type_traits>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/distance.hpp>
#include <boost/mpl/find.hpp>
#include <boost/mpl/for_each.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /**
     * @todo Currently gcc does not support expanding variadic type list
     *       into fixed-length template argument list;
     * @internal
     * @code
#ifdef HAS_CXX11_VARIADIC_TEMPLATES
    template<typename... Ts>
    class gbVertexFormat {
    public:
        typedef VertexData::Storage<Ts::Layout...> StorageType;
        typedef boost::mpl<Ts...> DescriptorList;
        typedef typename std::vector<StorageType>::size_type IndexType;
            [...]
    };
#else
#endif
    * @endcode
    * @endinternal
    */

    /** Vertex Component namespace.
     * Vertex components are used to build gbVertexData
     */
    namespace VertexComponent {

        /** Predefined vertex layout types
         */
        namespace Layout {
            /** 4-element vector
             */
            template<typename T>
            struct Vec4 {
                static_assert(std::is_pod<T>::value, "Only POD types allowed in Vertex Data");
                typedef T value_type;
                T x,y,z,w;
                /** gbMath::Vector4 assignment
                 */
                Vec4<T>& operator=(GB_MATH_NAMESPACE::Vector4<T> const& v) {
                    x = v.x_; y = v.y_; z = v.z_; w = v.w_;
                    return *this;
                }
            };
            /** @name Vec4 typedefs
             * @{
             */
            typedef Vec4<float>  Vec4f;
            typedef Vec4<double> Vec4d;
            typedef Vec4<int>    Vec4i;
            /// @}

            /** 3-element vector
             */
            template<typename T>
            struct Vec3 {
                static_assert(std::is_pod<T>::value, "Only POD types allowed in Vertex Data");
                typedef T value_type;
                T x,y,z;
                /** gbMath::Vector3 assignment
                 */
                Vec3<T>& operator=(GB_MATH_NAMESPACE::Vector3<T> const& v) {
                    x = v.x_; y = v.y_; z = v.z_;
                    return *this;
                }
            };
            /** @name Vec3 typedefs
             * @{
             */
            typedef Vec3<float>  Vec3f;
            typedef Vec3<double> Vec3d;
            typedef Vec3<int>    Vec3i;
            /// @}

            /** 2-element vector
             */
            template<typename T>
            struct Vec2 {
                static_assert(std::is_pod<T>::value, "Only POD types allowed in Vertex Data");
                typedef T value_type;
                T x,y;
                /** gbMath::Vector2 assignment
                 */
                Vec2<T>& operator=(GB_MATH_NAMESPACE::Vector2<T> const& v) {
                    x = v.x_; y = v.y_;
                    return *this;
                }
            };
            /** @name Vec2 typedefs
             * @{
             */
            typedef Vec2<float>  Vec2f;
            typedef Vec2<double> Vec2d;
            typedef Vec2<int>    Vec2i;
            /// @}

            /** RGB Color
             * This layout is binary compatible with Vec3f
             */
            struct ColorRGB {
                typedef float value_type;
                float r,g,b;
                /** gbMath::Vector3f assignment
                 */
                ColorRGB& operator=(GB_MATH_NAMESPACE::Vector3f const& v) {
                    r = v.x_; g = v.y_; b = v.z_;
                    return *this;
                }
            };
            /** RGBA Color
             * This layout is binary compatible with Vec4f
             */
            struct ColorRGBA {
                typedef float value_type;
                float r,g,b,a;
                /** gbMath::Vector4f assignment
                 */
                ColorRGBA& operator=(GB_MATH_NAMESPACE::Vector4f const& v) {
                    r = v.x_; g = v.y_; b = v.z_; a = v.w_;
                    return *this;
                }
            };
            /** UV Texture coordinate
             * @note This layout is binary compatible with Vec2<T>
             */
            template<typename T>
            struct TexCoordUV {
                static_assert(std::is_pod<T>::value, "Only POD types allowed in Vertex Data");
                typedef T value_type;
                T u,v;
                TexCoordUV<T>& operator=(GB_MATH_NAMESPACE::Vector2<T> const& vec) {
                    u = vec.x_; v = vec.y_;
                    return *this;
                }
            };
            /** @name TexCoordUV typedefs
             * @{
             */
            typedef TexCoordUV<float>  TexCoordUVf;
            typedef TexCoordUV<double> TexCoordUVd;
            /// @}
            /** UVW Texture coordinate
             * @note This layout is binary compatible with Vec3<T>
             */
            template<typename T>
            struct TexCoordUVW {
                static_assert(std::is_pod<T>::value, "Only POD types allowed in Vertex Data");
                typedef T value_type;
                T u,v,w;
                TexCoordUV<T>& operator=(GB_MATH_NAMESPACE::Vector3<T> const& vec) {
                    u = vec.x_; v = vec.y_; w = vec.z_;
                    return *this;
                }
            };
            /** @name TexCoordUVW typedefs
             * @{
             */
            typedef TexCoordUVW<float>  TexCoordUVWf;
            typedef TexCoordUVW<double> TexCoordUVWd;
            /// @}
        }

        /** Predefined component semantics
         */
        namespace Semantics {
            /** Generic component.
             * Use for components that have no special semantics
             */
            struct Generic {};
            /** Indexed generic component.
             * A generic component that can be identified by a numerical index
             */
            template<int> struct GenericN {};
            /** Padding component.
             * A component that is not part of the actual vertex format but
             * is just used for padding the underlying storage. The padding
             * semantics is unique in the sense that a padding component
             * can be of any layout type, as long as it is a POD type.
             * Because of this, padding components cannot be bound to
             * vertex components of a shader program.
             */
            struct Padding {};
            /** Position.
             * A component storing vertex positions
             */
            struct Position {};
            /** Color.
             * A component storing vertex colors
             */
            struct Color {};
            /** TextureCoordinates.
             * A component storing per-vertex texture coordinates
             */
            struct TextureCoordinates {};
            /** Per-vertex normals
             */
            struct Normal {};
        }

        /** Nil type.
         * Used to indicate absence of a valid vertex component
         */
        struct Nil {};

        /** Vertex component descriptor.
         * Each vertex component is described by a layout and a semantics type.
         * While the layout specifies the actual binary layout of the vertex
         * format, the semantics type is an empty type that is used for identifying
         * a component through C++'s type system.
         * For example a vertex may consist of a position and a normal, both represented
         * by the same layout type Vec3f. In order to still be able to identify the
         * components uniquely, they get assigned different semantics.
         * @tparam Layout_T Component layout type; Must be a POD struct. See @ref Layout.
         * @tparam Semantics_T Component semantics type. See @ref Semantics.
         * @note See @ref gb3d_vertdata_desc for a list of predefined descriptors.
         */
        template<typename Layout_T, typename Semantics_T>
        struct Descriptor {
            /// @cond
            static_assert(std::is_pod<Layout_T>::value, "Only POD types allowed in Vertex Data");
            typedef Layout_T Layout;
            typedef Semantics_T Semantics;
            /// @endcond
        };

        /** @defgroup gb3d_vertdata_desc Predefined descriptors
         * @{
         */
        /** 3d vertex position
         */
        typedef Descriptor<Layout::Vec3f, Semantics::Position> Position3f;
        /** 4d vertex position
         */
        typedef Descriptor<Layout::Vec4f, Semantics::Position> Position4f;
        /** RGB vertex color
         */
        typedef Descriptor<Layout::ColorRGB, Semantics::Color> Color3f;
        /** RGBA vertex color
         */
        typedef Descriptor<Layout::ColorRGBA, Semantics::Color> Color4f;
        /** 3d vertex normal
         */
        typedef Descriptor<Layout::Vec3f, Semantics::Normal> Normal3f;
        /** 4d vertex normal
         */
        typedef Descriptor<Layout::Vec4f, Semantics::Normal> Normal4f;
        /** UV texture coordinate
         */
        typedef Descriptor<Layout::TexCoordUVf, Semantics::TextureCoordinates> TexCoord2f;
        /** Nil descriptor
         * @note The Nil descriptor serves as a terminator for the typelist.
         *       You usually do not want to use this.
         */
        typedef Descriptor<Nil, Nil> NilDesc;
        /// @}

        /** Type traits for vertex component classes
         */
        namespace Traits {
            /** Indicates whether a given type is the Nil type
             * @tparam T Type to be tested
             */
            template<typename T> struct IsNil;
            template<> struct IsNil<Nil> {
                enum {
                    value = true
                };
            };
            template<typename T> struct IsNil {
                enum {
                    value = false
                };
            };

            /** Maps a given layout struct to the corresponding gbVertexFormatBase::ComponentType_T.
             * This trait is used for runtime type identification of a given vertex component
             * layout. As long as you are only using the predefined layout types from the
             * VertexComponent::Layout namespace, you do not need to care about this at all.
             * If you are using a custom layout type, you must specialize this traits template
             * for your layout type accordingly and ensure the trait is available upon instantiating
             */
            template<typename Layout_T> struct LayoutComponentType {
                static_assert(sizeof(Layout_T) == 0, "LayoutComponentType trait not defined for given Layout_T type");
            };
            /// @cond
            template<> struct LayoutComponentType<float> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_FLOAT;
            };
            template<> struct LayoutComponentType<double> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE;
            };
            template<> struct LayoutComponentType<int32_t> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_INT;
            };
            template<> struct LayoutComponentType<uint32_t> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_UINT;
            };
            template<> struct LayoutComponentType<Layout::Vec4f> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_FLOAT4;
            };
            template<> struct LayoutComponentType<Layout::Vec4d> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE4;
            };
            template<> struct LayoutComponentType<Layout::Vec4i> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_INT4;
            };
            template<> struct LayoutComponentType<Layout::Vec3f> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_FLOAT3;
            };
            template<> struct LayoutComponentType<Layout::Vec3d> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE3;
            };
            template<> struct LayoutComponentType<Layout::Vec3i> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_INT3;
            };
            template<> struct LayoutComponentType<Layout::Vec2f> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_FLOAT2;
            };
            template<> struct LayoutComponentType<Layout::Vec2d> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE2;
            };
            template<> struct LayoutComponentType<Layout::Vec2i> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_INT2;
            };
            template<> struct LayoutComponentType<Layout::ColorRGB> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_FLOAT3;
            };
            template<> struct LayoutComponentType<Layout::ColorRGBA> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_FLOAT4;
            };
            template<> struct LayoutComponentType<Layout::TexCoordUVf> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_FLOAT2;
            };
            template<> struct LayoutComponentType<Layout::TexCoordUVd> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE2;
            };
            template<> struct LayoutComponentType<Layout::TexCoordUVWf> {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_DOUBLE3;
            };
            /// @endcond
            /** Maps a given semantics type to the corresponding gbVertexFormatBase::ComponentSemantics_T.
             * This trait is used for runtime type identification of a given vertex component
             * semantic. As long as you are only using the predefined semantics types from the
             * VertexComponent::Semantics namespace, you do not need to care about this at all.
             * If you are using a custom semantics, you must specialize this traits template
             * for your semantics type accordingly and ensure the trait is available upon instantiating
             */
            template<typename Semantics_T> struct SemanticComponentType {
                static_assert(sizeof(Semantics_T) == 0, "SemanticComponentType trait not defined for given Semantics_T type");
            };
            /// @cond
            template<> struct SemanticComponentType<Semantics::Generic> {
                static const gbVertexFormatBase::ComponentSemantics_T value = gbVertexFormatBase::COMPONENT_SEMANTICS_GENERIC;
            };
            template<int I> struct SemanticComponentType<Semantics::GenericN<I>> {
                static const gbVertexFormatBase::ComponentSemantics_T value = gbVertexFormatBase::COMPONENT_SEMANTICS_GENERIC;
            };
            template<> struct SemanticComponentType<Semantics::Position> {
                static const gbVertexFormatBase::ComponentSemantics_T value = gbVertexFormatBase::COMPONENT_SEMANTICS_POSITION;
            };
            template<> struct SemanticComponentType<Semantics::Color> {
                static const gbVertexFormatBase::ComponentSemantics_T value = gbVertexFormatBase::COMPONENT_SEMANTICS_COLOR;
            };
            template<> struct SemanticComponentType<Semantics::TextureCoordinates> {
                static const gbVertexFormatBase::ComponentSemantics_T value = gbVertexFormatBase::COMPONENT_SEMANTICS_TEXTURE;
            };
            template<> struct SemanticComponentType<Semantics::Padding> {
                static const gbVertexFormatBase::ComponentSemantics_T value = gbVertexFormatBase::COMPONENT_SEMANTICS_PADDING;
            };
            template<> struct SemanticComponentType<Semantics::Normal> {
                static const gbVertexFormatBase::ComponentSemantics_T value = gbVertexFormatBase::COMPONENT_SEMANTICS_NORMAL;
            };
            /// @endcond
        }

        template<typename T1, typename T2>
        void FillVector(Layout::Vec3<T1>& layout, GB_MATH_NAMESPACE::Vector3<T2> const& v, bool /* is_normal */) {
            layout.x = static_cast<T1>(v.x_);
            layout.y = static_cast<T1>(v.y_);
            layout.z = static_cast<T1>(v.z_);
        }

        template<typename T1, typename T2>
        void FillVector(Layout::Vec4<T1>& layout, GB_MATH_NAMESPACE::Vector3<T2> const& v, bool is_normal) {
            layout.x = static_cast<T1>(v.x_);
            layout.y = static_cast<T1>(v.y_);
            layout.z = static_cast<T1>(v.z_);
            layout.w = (is_normal)?(static_cast<T1>(0)):(static_cast<T1>(1));
        }
    }

    /** Vertex Format.
     * gbVertexFormat stores all information about the vertex format used by a gbVertexData
     * and allows both compile-time and runtime access. In most cases it is not
     * necessary to instantiate gbVertexFormat directly (see the documentation @ref gbVertexData 
     * for details).
     */
    template<typename T0, typename T1=VertexComponent::NilDesc, typename T2=VertexComponent::NilDesc, 
             typename T3=VertexComponent::NilDesc, typename T4=VertexComponent::NilDesc, typename T5=VertexComponent::NilDesc,
             typename T6=VertexComponent::NilDesc, typename T7=VertexComponent::NilDesc, typename T8=VertexComponent::NilDesc,
             typename T9=VertexComponent::NilDesc, typename T10=VertexComponent::NilDesc, typename T11=VertexComponent::NilDesc,
             typename T12=VertexComponent::NilDesc, typename T13=VertexComponent::NilDesc, typename T14=VertexComponent::NilDesc,
             typename T15=VertexComponent::NilDesc >
    class gbVertexFormat: public gbVertexFormatBase {
    public:
        /** Storage type
         */
        typedef VertexData::Storage< typename T0::Layout, typename T1::Layout, typename T2::Layout,
                                     typename T3::Layout, typename T4::Layout, typename T5::Layout,
                                     typename T6::Layout, typename T7::Layout, typename T8::Layout,
                                     typename T9::Layout, typename T10::Layout, typename T11::Layout,
                                     typename T12::Layout, typename T13::Layout, typename T14::Layout,
                                     typename T15::Layout > StorageType;
        static_assert(std::is_pod<StorageType>::value, "Vertex Data storage must be POD type");
        /** Typelist of VertexComponent::Descriptor
         */
        typedef boost::mpl::vector< T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10,
                                    T11, T12, T13, T14, T15 > DescriptorList;
        /** VertexFormat (typedef to self)
         */
        typedef gbVertexFormat< T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10,
                                T11, T12, T13, T14, T15 > VertexFormat;
        /** Number of vertex components; See NumberOfComponents().
         */
        enum {
            NUMBER_OF_COMPONENTS = boost::mpl::distance <
                typename boost::mpl::begin<DescriptorList>::type,
                typename boost::mpl::find<DescriptorList, VertexComponent::NilDesc>::type > ::value
        };
    private:
        /** Functor used for acquiring runtime info about the typelist through mpl::foreach
         */
        struct FillRuntimeInfo {
            RuntimeInfo_T* target_;
            int offs_acc_;
            FillRuntimeInfo(RuntimeInfo_T* target): target_(target), offs_acc_(0) {}
        private:
            struct DummyTypeForPaddingComponents {
                static const gbVertexFormatBase::ComponentType_T value = gbVertexFormatBase::VERTEX_COMPONENT_NO_TYPE;
            };
        public:
            template<typename U>
            typename std::enable_if<!std::is_same<U, VertexComponent::NilDesc>::value, void>::type
            operator()(U) {
                int const size = static_cast<int>(sizeof(typename U::Layout));
                gbVertexFormatBase::ComponentSemantics_T const rt_semantic =
                    VertexComponent::Traits::SemanticComponentType<typename U::Semantics>::value;
                gbVertexFormatBase::ComponentType_T const rt_type =
                    boost::mpl::if_<
                            std::is_same<typename U::Semantics, VertexComponent::Semantics::Padding>,
                            DummyTypeForPaddingComponents,
                            VertexComponent::Traits::LayoutComponentType<typename U::Layout>
                        >::type::value;
                target_->ComponentSize = size;
                target_->Offset = offs_acc_;
                target_->Type = rt_type;
                target_->Semantic = rt_semantic;
                ++target_;
                offs_acc_ += size;
            }
            template<typename U>
            typename std::enable_if<std::is_same<U, VertexComponent::NilDesc>::value, void>::type
            operator()(U) {
                /* nothing to do */
            }
        };
    private:
        RuntimeInfo_T m_RuntimeInfo[NUMBER_OF_COMPONENTS];        ///< runtime info about Format type
    public:
        /** Constructor
         */
        gbVertexFormat()
        {
            boost::mpl::for_each<DescriptorList>(FillRuntimeInfo(m_RuntimeInfo));
            /// @todo do padding check at compile time
            int const expected_size = m_RuntimeInfo[NUMBER_OF_COMPONENTS-1].Offset +
                                        m_RuntimeInfo[NUMBER_OF_COMPONENTS-1].ComponentSize;
            if(sizeof(StorageType) != expected_size) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                      "Unspecified padding bits in vertex format" );
            }
        }
        /** @name gbVertexFormatBase
         * @{
         */
        std::unique_ptr<gbVertexFormatBase> Clone() const {
            return std::unique_ptr<gbVertexFormatBase>(new VertexFormat(*this));
        }
        int GetStride() const {
            return sizeof(StorageType);
        }
        int NumberOfComponents() const {
            return NUMBER_OF_COMPONENTS;
        }
        int ComponentRawSize(int i) const {
            return m_RuntimeInfo[i].ComponentSize;
        }
        int ComponentRawOffset(int i) const {
            return m_RuntimeInfo[i].Offset;
        }
        ComponentType_T GetComponentType(int i) const {
            return m_RuntimeInfo[i].Type;
        }
        ComponentSemantics_T GetComponentSemantics(int i) const {
            return m_RuntimeInfo[i].Semantic;
        }
        /// @}
    };
}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXTURE_ATLAS_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXTURE_ATLAS_HPP_

#include <gbGraphics/config.hpp>

#include <gbGraphics/texture2d.hpp>

#include <gbMath/matrix3.hpp>

#include <boost/utility.hpp>

#include <functional>
#include <utility>
#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

    class Rect;
    class gbDraw2D;
    class gbDrawingContext3D;
    class ReadableSurface;
    typedef std::shared_ptr<gbDrawingContext3D> gbDrawingContext3DPtr;
    namespace TextureAtlas {
        class PackingStrategy;
    }

    /** Packs multiple small textures into a single big one.
     * Usage of a texture atlas may improve performance by reducing the numbers of texture bind operations.
     * Be aware of the drawbacks though: As each texture in the atlas shares borders with its neighbor,
     * foreign texels bleed in on sampling. This effectively prohibits use of mip-mapping and interpolating filters.
     * Furthermore, texture coordinates of the underlying geometry must be adjusted to stay within borders
     * of the respective subtexture. The texture atlas provides a transform matrix for uv texture coordinate
     * vectors which must either be baked into the geometry directly, or be considered by the shader when
     * texturing.
     * Due to these restrictions, texture atlases are usually only suitable for rendering 2d graphics.
     */
    class gbTextureAtlas: public boost::noncopyable {
    private:
        gbTexture2DPtr m_Texture;                    //!< the actual texture
        std::vector<Rect> m_Subtextures;            //!< list of subtexture areas in the texture
    public:
        /** Constructor
         * @param[in] dc3d Drawing context used for creating the texture storing the atlas
         * @param[in] subtextures List of subtexture surfaces included in the texture atlas
         * The constructor will attempt to pack all of the subtextures into a single texture.
         * The size of the storage texture is determined dynamically.
         */
        gbTextureAtlas(gbDrawingContext3D& dc3d, std::vector<ReadableSurface*> const& subtextures);
        /** Constructor
         * @param[in] dc3d Drawing context used for creating the texture storing the atlas
         * @param[in] width Width of the encompassing texture in pixels
         * @param[in] height Height of the encompassing texture in pixels
         * @param[in] subtextures List of subtexture surfaces included in the texture atlas
         * The constructor will attempt to pack all of the subtextures into a single texture of
         * the given dimensions. If no such packing can be found, the constructor will throw.
         */
        gbTextureAtlas(gbDrawingContext3D& dc3d, int width, int height, std::vector<ReadableSurface*> const& subtextures);
        /** Destructor
         */
        ~gbTextureAtlas();
        /** Retrieve the encompassing texture
         * @return A single texture encompassing all of the subtextures of the texture atlas
         */
        gbTexture2DPtr& GetTexture();
        /** Retrieve the number of subtextures stored in the atlas
         */
        int NumberOfSubtextures() const;
        /** Retrieve the area covered by a specific subtexture
         * @param[in] idx Index of the subtexture [0..NumberOfSubtextures)
         * @return A Rect describing the area in the atlas covered by the subtexture
         */
        Rect const& GetSubtextureRect(int idx) const;
        /** Retrieve the texture coordinate transformation for a specific subtexture
         * @tparam T Type of the elements of the transformation matrix
         * @param[in] idx Index of the subtexture [0..NumberOfSubtextures)
         * @return A 3x3 transformation matrix mapping the uv coordinate ranges [0..1)
         *         onto the subtexture.
         */
        template<typename T>
        GB_MATH_NAMESPACE::Matrix3<T> GetSubtextureTexCoordTransform(int idx) const
        {
            T const px = GB_MATH_NAMESPACE::FloatUtil::One<T>() / static_cast<T>(m_Texture->GetWidth());
            T const py = GB_MATH_NAMESPACE::FloatUtil::One<T>() / static_cast<T>(m_Texture->GetHeight());
            Rect const& rect = GetSubtextureRect(idx);
            return GB_MATH_NAMESPACE::Matrix3<T>(
                px * static_cast<T>(rect.width_), 0,                                 px * static_cast<T>(rect.left_),
                0,                                py * static_cast<T>(rect.height_), py * static_cast<T>(rect.top_),
                0,                                0,                                 1 
            );
        }
    private:
        void InitAtlas(std::vector<ReadableSurface*> const& subtextures, TextureAtlas::PackingStrategy* packing);
    };

    namespace TextureAtlas {

        /** Strategy for solving the rectangle packing problem
         * Rectangle packing is the problem of placing a number of small rectangles inside
         * a bigger rectangular area such that no two rectangles overlap.
         * The packing problem is NP-complete, so finding the optimal solution is only reasonable
         * for a very small number of rectangles. Furthermore, finding the size of the minimum
         * encompassing rectangle is itself an instance of the packing problem.
         */
        class PackingStrategy: public boost::noncopyable {
        public:
            /** Destructor
             */
            virtual ~PackingStrategy() {}
            /** Add a rectangle to the problem set
             * @param[in] r Rectangle that is to be included in the packing
             * @attention Output of calls to CalculatePacking() is written directly into
             *            the added rectangles. Therefore all added rectangles must be
             *            kept alive until after the last use of the PackingStrategy!
             */
            virtual void AddRect(Rect* r) = 0;
            /** Calculate a packing.
             * This function attempts to place all rectangles added via AddRect() inside the given
             * target rectangle. The top_ and left_ fields of those rectangles will be overwritten to
             * reflect the new position inside the target area.
             * @param[in] target_area Desired target area
             * @return A list of the elements that could not be placed in case the algorithm runs out of room
             *         in the target area.
             */
            virtual std::vector<Rect*> CalculatePacking(Rect const& target_area) = 0;
            /** Attempts to retrieve the minimum bounding area required for a packing.
             * This function will attempt find the smallest possible bounding area, but
             * it does not guarantee to find the actual minimum. The determined solution
             * should be reasonably close though.
             * The top_ and left_ fields of rectangles passed in via AddRect() may get
             * changed in arbitrary ways by this call.
             * @return A Rect whose width and height fields specify a bounding area
             *         sufficiently large to allow a packing
             */
            virtual Rect DetermineBoundingArea() = 0;
        };

        /** A sliced packing algorithm.
         * Sliced packing builds a kd-Tree to determine where to insert rectangles.
         * Since each node in the tree can be interpreted as a 'slice' through the
         * remaining space and no rectangle can ever be placed on a slice, this
         * algorithm may not find the optimal packing. It is however quite fast.
         * This strategy performs particularly well with power-of-two rectangles.
         */
        class SlicedPacking: public PackingStrategy {
        public:
            /** Tree node type
             */
            struct Slice {
                enum Direction_T {
                    SLICE_HORIZONTAL,                    ///< Node represents a horizontal slice
                    SLICE_VERTICAL,                        ///< Node represents a vertical slice
                    SLICE_LEAF                            ///< Node is a leaf of the tree
                } Direction;                        ///< Type of the node
                int Position;                        ///< Position of the slice (no meaning for leaf nodes)
                std::unique_ptr<Slice> Left;        ///< Left child
                std::unique_ptr<Slice> Right;        ///< Right child
            };
            std::vector<Rect*> m_Rects;            ///< list of rectangles to be packed
            std::unique_ptr<Slice> m_Root;        ///< Pointer to root node of the slice tree
        public:
            SlicedPacking();
            ~SlicedPacking();
            /** @name PackingStrategy
             * @{
             */
            void AddRect(Rect* r);
            std::vector<Rect*> CalculatePacking(Rect const& target_area);
            Rect DetermineBoundingArea();
            /// @}
            /** Paints the splitting lines from the slice tree to a surface
             * This function is intended mainly for debugging.
             * @param[in] draw2d A draw2d bound to a surface of the same dimensions as the target
             *                   area passed to the most recent call to CalculatePacking().
             */
            void PaintTree(gbDraw2D& draw2d) const;
        };
    }

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_FONT_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_FONT_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/lockable_surface.hpp>
#include <gbMath/vector2.hpp>

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** Interface for text fonts usable by gbFontRenderer
     */
    class gbFont: public boost::noncopyable {
    public:
        virtual ~gbFont() {};
        /** Check whether the font has fixed width
         * @return true for fixed-width fonts; false otherwise
         */
        virtual bool HasFixedWidth() const=0;
        /** Check whether the font provides kerning information
         * @return true for fonts with kerning; false otherwise
         */
        virtual bool HasKerning() const=0;
        /** Get the height of the font
         * @return Height of the font in pixels
         * @note The height of the font is the difference between two baselines
         */
        virtual int GetFontHeight() const=0;
        /** Get the maximum width for a glyph in the font
         * @return Maximum font width in pixels
         * @note For fixed-width fonts each glyph has exactly this width
         */
        virtual int GetFontMaxWidth() const=0;
        /** Disable kerning for the next glyph
         * @note Kerning is applied upon LoadGlyph() depending on both the previous
         *       and the newly loaded glyph. Calling ResetKerning() deletes all info
         *       on the previous glyph, thus effectively disabling kerning for the
         *       next call to LoadGlyph().
         */
        virtual void ResetKerning() const=0;
        /** Load a glyph for rendering
         * @param[in] c Unicode encoded character
         * @attention Calling this function out of order between rendering glyphs
         *            may mess up kerning. See @ref GetKerning() for details.
         */
        virtual void LoadGlyph(int c)=0;
        /** Render the currently loaded glyph.
         */
        virtual void RenderGlyph()=0;
        /** Get kerning info of the current glyph
         * @return Kerning vector for the currently loaded glyph;
         *         Null vector if no glyph was loaded or the font 
         *         does not support kerning
         * @note The kerning vector stores an offset that is to
         *       be applied to the pen position before rendering.
         *       Usually it causes a glyph to be shifted to the left
         *       in order to avoid large empty spaces caused by
         *       certain glyph combinations. The kerning vector is
         *       calculated upon calling LoadGlyph() and takes into
         *       account both the previous and the newly loaded glyph.
         *       The only other gbFont member functions that is allowed
         *       to mess with kerning besided LoadGlyph() is ResetKerning().
         *       Overloads may however manipulate kerning through functions
         *       that are not declared as part of the gbFont interface.
         * @attention The kerning vector must be added to the pen position
         *            *before* drawing the glyph. This is unlike the advance
         *            vector (see GetGlyphAdvance()) which is applied after drawing.
         */
        virtual GB_MATH_NAMESPACE::Vector2i GetKerning() const=0;
        /** Get the pen advance vector for the current glyph
         * @return Vector indicating how to advance the pen after drawing;
         *         Null vector if no glyph was loaded
         */
        virtual GB_MATH_NAMESPACE::Vector2i GetGlyphAdvance() const=0;
        /** Get the origin vector for drawing the current glyph
         * @return Origin vector; Null vector if no glyph was loaded
         */
        virtual GB_MATH_NAMESPACE::Vector2i GetGlyphOrigin() const=0;
        /** Get the width of the current glyph
         * @return Width of the currently loaded glyph; 0 if no glyph was loaded
         */
        virtual int GetGlyphWidth() const=0;
        /** Get the height of the current glyph
         * @return Height of the currently loaded glyph; 0 if no glyph was loaded
         */
        virtual int GetGlyphHeight() const=0;
        /** Sample the currently loaded glyph
         * @param[in] x X coordinate; must be in the range [0..GetGlyphWidth())
         * @param[in] y Y coordinate; must be in the range [0..GetGlyphWidth())
         * @return Color intensity at the requested coordinate
         * @attention For performance reasons this function is not required to
         *            perform sanity checks or range checks. Calling this function
         *            without loading *and* rendering a glyph first (by calling
         *            LoadGlyph() and RenderGlyph() respectively) yields
         *            undefined behavior. Giving parameters outside the valid range
         *            yields undefined behavior.
         */
        virtual GBCOLOR_COMPONENT SampleGlyph(int x, int y) const=0;
    };
}

#endif

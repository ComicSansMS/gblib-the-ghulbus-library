/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXT_RENDERER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXT_RENDERER_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>

#include <gbMath/vector2.hpp>

#include <memory>

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {
    class gbFont;
    class LockableSurface;

    namespace Text {
        class TextRenderer;
        typedef std::unique_ptr<TextRenderer> TextRendererPtr;

        class TextRenderer: boost::noncopyable {
        public:
            virtual ~TextRenderer() {}
            virtual bool RenderChar(int c)=0;
            virtual void SetFont(gbFont* font)=0;
            virtual void SetTextColor(GBCOLOR c)=0;
            virtual void SetCursorPosition(int x, int y)=0;
            virtual void SetTabWidthPixels(int px_width)=0;
            virtual void SetNewLineIndentation(int px_indent)=0;
            virtual void Clear()=0;
            virtual void Clear(GBCOLOR c)=0;
            virtual GB_MATH_NAMESPACE::Vector2i GetCursorPosition() const=0;
        };
    }

}

#endif

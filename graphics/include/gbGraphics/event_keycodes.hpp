/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_EVENT_KEYCODES_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_EVENT_KEYCODES_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_EVENT_KEYCODES_HPP_
#    error Do not include event_keycodes.hpp directly; Include event.hpp instead
#endif

namespace GB_GRAPHICS_NAMESPACE {
    namespace Events {
        /** Keyboard Keycodes
         */
        enum Key {
            GBKEY_UNKNOWN = 0,
            GBKEY_ESCAPE,
            GBKEY_1,
            GBKEY_2,
            GBKEY_3,
            GBKEY_4,
            GBKEY_5,
            GBKEY_6,
            GBKEY_7,
            GBKEY_8,
            GBKEY_9,
            GBKEY_0,
            GBKEY_MINUS,
            GBKEY_EQUALS,
            GBKEY_BACKSPACE,
            GBKEY_TAB,
            GBKEY_Q,
            GBKEY_W,
            GBKEY_E,
            GBKEY_R,
            GBKEY_T,
            GBKEY_Y,
            GBKEY_U,
            GBKEY_I,
            GBKEY_O,
            GBKEY_P,
            GBKEY_LEFTBRACKET,
            GBKEY_RIGHTBRACKET,
            GBKEY_RETURN,
            GBKEY_LCTRL,
            GBKEY_A,
            GBKEY_S,
            GBKEY_D,
            GBKEY_F,
            GBKEY_G,
            GBKEY_H,
            GBKEY_J,
            GBKEY_K,
            GBKEY_L,
            GBKEY_SEMICOLON,
            GBKEY_QUOTESINGLE,
            GBKEY_BACKQUOTE,
            GBKEY_LSHIFT,
            GBKEY_BACKSLASH,
            GBKEY_Z,
            GBKEY_X,
            GBKEY_C,
            GBKEY_V,
            GBKEY_B,
            GBKEY_N,
            GBKEY_M,
            GBKEY_COMMA,
            GBKEY_PERIOD,
            GBKEY_SLASH,
            GBKEY_RSHIFT,
            GBKEY_NUM_MULTIPLY,
            GBKEY_LALT,
            GBKEY_SPACE,
            GBKEY_CAPSLOCK,
            GBKEY_F1,
            GBKEY_F2,
            GBKEY_F3,
            GBKEY_F4,
            GBKEY_F5,
            GBKEY_F6,
            GBKEY_F7,
            GBKEY_F8,
            GBKEY_F9,
            GBKEY_F10,
            GBKEY_NUMLOCK,
            GBKEY_SCROLLOCK,
            GBKEY_NUM7,
            GBKEY_NUM8,
            GBKEY_NUM9,
            GBKEY_NUM_MINUS,
            GBKEY_NUM4,
            GBKEY_NUM5,
            GBKEY_NUM6,
            GBKEY_NUM_PLUS,
            GBKEY_NUM1,
            GBKEY_NUM2,
            GBKEY_NUM3,
            GBKEY_NUM0,
            GBKEY_NUM_PERIOD,
            GBKEY_LESS,
            GBKEY_F11,
            GBKEY_F12,
            GBKEY_F13,
            GBKEY_F14,
            GBKEY_F15,
            GBKEY_NUM_EQUALS,
            GBKEY_CARET,
            GBEY_AT,
            GBKEY_COLON,
            GBKEY_UNDERSCORE,
            GBKEY_NUM_ENTER,
            GBKEY_RCTRL,
            GBKEY_NUM_DIVIDE,
            GBKEY_SYSREQ,
            GBKEY_PRINT,
            GBKEY_RALT,
            GBKEY_BREAK,
            GBKEY_HOME,
            GBKEY_UP,
            GBKEY_PAGEUP,
            GBKEY_LEFT,
            GBKEY_RIGHT,
            GBKEY_END,
            GBKEY_DOWN,
            GBKEY_PAGEDOWN,
            GBKEY_INSERT,
            GBKEY_DELETE,
            GBKEY_LWIN,
            GBKEY_RWIN,
            GBKEY_MENU,
            /// @}
            GBKEY_LAST,
            GBKEY_NUMBER_OF_KEYSTATES = 256
        };

        enum KeyModifier {
        };
    }
}

#endif

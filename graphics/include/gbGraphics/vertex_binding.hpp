/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_BINDING_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_BINDING_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/vertex_format_base.hpp>

#include <diagnostic/exception.hpp>
#include <diagnostic/assert.hpp>

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     */
    class gbVertexBinding;

    class gbProgram;
    typedef std::shared_ptr<gbProgram> gbProgramPtr;
    namespace VertexData {
        template<typename T_Desc, class gbVertexFormat_T> struct ComponentIndex;
    }
    /// @}

    /** Vertex binding.
     * A vertex binding specifies for a specific gbProgram how data of a given gbVertexFormat
     * should be passed into the vertex shader.
     * A single binding can be reused for all vertex data specified in a matching gbVertexFormat,
     * but only for the gbProgram that it was created with.
     */
    class gbVertexBinding {
    public:
        /** Vertex data binding
         */
        struct Binding_T {
            gbVertexFormatBase::ComponentType_T Type;        ///< Type of the vertex component
            int ShaderIndex;                                ///< Index in the gbProgram shader
            int VertexDataOffset;                            ///< Offset to the component in bytes
                                                            ///   from the beginning of a vertex entry
            int VertexDataSize;                                ///< Size of the component in bytes
        };
    private:
        std::shared_ptr<const gbProgram> m_Program;                ///< associated shader
        std::unique_ptr<gbVertexFormatBase> m_VertexFormat;        ///< associated vertex format
        std::vector<Binding_T> m_Bindings;                        ///< list of bindings
    public:
        /** Constructor
         * @param[in] program Shader to bind to
         * @param[in] vertex_format Vertex format of the input data
         * @note The gbVertexBinding assumes shared ownership of the
         *       passed program.
         */
        gbVertexBinding(gbProgramPtr program, gbVertexFormatBase const& vertex_format);
        /** Move constructor
         * @todo VS 2010 fails to autogenerate this
         */
        gbVertexBinding(gbVertexBinding&& rhs);
        /** Bind a vertex component to an attribute in the vertex shader
         * @param[in] component_index Zero-based index of the vertex component to bind
         * @param[in] attrib_name Identifier for a vertex shader attribute
         * @throw InvalidArgument if if the attribute is not found in the shader or
         *                        if the formats of attribute and component do not match
         * @note Users will typically want to use the non-member @ref GhulbusGraphics::Bind()
         *       function if the vertex format type is known at compile time.
         */
        void Bind(int component_index, std::string const& attrib_name);
        /** STL-style iterator for bindings
         * @return Iterator to the first element in the list of bindings
         */
        std::vector<Binding_T>::const_iterator begin() const;
        /** STL-style iterator for bindings
         * @return Iterator to one-past-the-final element in the list of bindings
         */
        std::vector<Binding_T>::const_iterator end() const;
        /** Get the stride of the associated vertex format
         * @return Stride of the vertex format in bytes
         */
        int GetDataStride() const;
        /** Get the vertex format
         * @return Vertex format used for the binding
         */
        gbVertexFormatBase const* GetVertexFormat() const;
        /** Get the program
         * @return Program that is bound to
         */
        gbProgram const* GetProgram() const;
    private:
        gbVertexBinding(gbVertexBinding const&);
        gbVertexBinding& operator=(gbVertexBinding const&);
    };

    /** @name Non-member begin/end
     * @{
     */
    inline auto begin(gbVertexBinding const& v) -> decltype(v.begin()) { return v.begin(); }
    inline auto end(gbVertexBinding const& v)   -> decltype(v.end())   { return v.end(); }
    /// @}

    /** Bind a vertex component to an attribute in the vertex shader
     * @tparam T_Desc Descriptor of the vertex component to bind
     * @tparam gbVertexFormat_T gbVertexFormat or gbVertexData type
     * @param[in] binding gbVertexBinding object that will store the binding
     * @param[in] attrib_name Identifier for a vertex shader attribute
     */
    template<typename T_Desc, class gbVertexFormat_T>
    void Bind(gbVertexBinding& binding, std::string const& attrib_name) {
        binding.Bind(VertexData::ComponentIndex<T_Desc, gbVertexFormat_T>::value, attrib_name);
    }

}

#endif

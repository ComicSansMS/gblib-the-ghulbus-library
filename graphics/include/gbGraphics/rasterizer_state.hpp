/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_RASTERIZER_STATE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_RASTERIZER_STATE_HPP_

#include <gbGraphics/config.hpp>

#include <boost/utility.hpp>

#include <memory>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbRasterizerState;
    typedef std::shared_ptr<gbRasterizerState> gbRasterizerStatePtr;
    /// @}

    namespace Rasterizer {
        enum FillMode_T {
            FILL_WIREFRAME,
            FILL_FILLED
        };
        enum CullMode_T {
            CULL_FRONT,
            CULL_BACK,
            CULL_NONE
        };
        enum FaceOrientation_T {
            FACE_FRONT_CLOCKWISE,
            FACE_FRONT_COUNTER_CLOCKWISE
        };

        struct StateDesc {
            FillMode_T FillMode;
            CullMode_T CullMode;
            FaceOrientation_T FaceOrientation;
            StateDesc()
                :FillMode(FILL_FILLED), CullMode(CULL_NONE), FaceOrientation(FACE_FRONT_COUNTER_CLOCKWISE)
            {}
            StateDesc(FillMode_T fill_mode, CullMode_T cull_mode, FaceOrientation_T face_front)
                :FillMode(fill_mode), CullMode(cull_mode), FaceOrientation(face_front)
            {}
        };
    }

    class gbRasterizerState {
    public:
        virtual ~gbRasterizerState() {}
        virtual Rasterizer::StateDesc const& GetDescriptor() const=0;
    };

}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_RASTERIZER_STATE_OGL_HPP_
#    include <gbGraphics/rasterizer_state_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_RASTERIZER_STATE_OGL_HPP_
#endif

#endif

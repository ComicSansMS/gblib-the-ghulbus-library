/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_WINDOW_SDL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_WINDOW_SDL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_WINDOW_SDL_HPP_
#    error Do not include window_SDL.hpp directly; Include window.hpp instead
#endif

#include <gbGraphics/event_manager.hpp>

#include <cstdint>
#include <unordered_map>
#include <vector>
#include <SDL.h>

namespace GB_GRAPHICS_NAMESPACE {
    class gbEventProcessor_SDL;

    class gbWindow_SDL: public gbWindow {
    public:
        class SDL_WindowEnvironment: public boost::noncopyable {
            friend class gbWindow;
        private:
            std::unique_ptr<gbEventProcessor_SDL> m_EventProcessor;
        public:
            ~SDL_WindowEnvironment();
            gbEventProcessor_SDL* GetEventProcessor() const;
            void RegisterWithEnvironment(gbWindow_SDL* window, uint32_t id);
            std::vector<gbWindow::DeviceIdentifier> EnumerateDevices();
        private:
            SDL_WindowEnvironment();
            void InitOpenGLAttributes();
        };
    private:
        std::shared_ptr<SDL_WindowEnvironment> m_Environment;
        Window::WindowDesc m_Descriptor;
        SDL_Window* m_Window;
        gbEventProcessor_SDL* m_EventProcessor;
        gbEventManager m_EventManager;
        int m_Width;
        int m_Height;
        std::weak_ptr<gbDrawingContext3D> m_Associated3dDrawingContext;
        bool m_MouseCursorVisible;
        bool m_MouseCaptured;
        bool m_MouseModeRelative;
    public:
        gbWindow_SDL(std::shared_ptr<SDL_WindowEnvironment>& the_environment, int width, int height,
                     Window::WindowDesc const& window_descriptor);
        ~gbWindow_SDL();
        /** @name gbWindow
         * @{
         */
        gbEventProcessor* GetEventProcessor() const;
        gbEventManager* GetEventManager();
        void SetWindowTitle(char const* str);
        void SetFullscreen(Window::FullscreenState_T fullscreen_state);
        int GetWidth() const;
        int GetHeight() const;
        float GetAspectRatio() const;
        gbDrawingContext3DPtr CreateDrawingContext3D();
        gbColor::ColorScheme const& GetAssociatedColorScheme() const;
        void SetMouseCursorVisible(bool show_cursor);
        bool GetMouseCursorVisible() const;
        void SetCaptureMouseCursor(bool capture_cursor);
        bool IsMouseCursorCaptured() const;
        void SetRelativeMouseMode(bool use_relative_mouse_mode);
        bool IsInRelativeMouseMode() const;
        /// @}
        /** @name SDL specific functions
         * @{
         */
        /** Get the underlying SDL_Window object
         */
        SDL_Window* GetSDLWindow_SDL();
        /// @}
    private:
        void CheckOpenDrawingContexts();
        void WindowResizeHandler();
    };
}

#endif

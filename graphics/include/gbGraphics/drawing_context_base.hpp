/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_DRAWING_CONTEXT_BASE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_DRAWING_CONTEXT_BASE_HPP_

#include <gbGraphics/config.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbWindow;

    class gbDrawingContext_Base {
    public:
        /** Destructor
         */
        virtual ~gbDrawingContext_Base() {};
        /** Get the parent window
         * @return Pointer to the gbWindow from which the drawing context was constructed
         */
        virtual gbWindow* GetParentWindow() const=0;
        /** Set the clear color.
         * Calls to Clear() will fill the back buffer with the clear color.
         * Default clear color is opaque black.
         * @param[in] c New clear color
         */
        virtual void SetClearColor(GBCOLOR c)=0;
        /** Get the clear color
         * @return Current clear color
         */
        virtual GBCOLOR GetClearColor() const=0;
        /** Clear the backbuffer
         */
        virtual void Clear()=0;
        /** Present the back buffer.
         * The drawing context performs all draw operations on an off-screen buffer.
         * This call will swap the off-screen buffer with the display buffer, effectively
         * presenting the results of all draw operations since the last swap on screen.
         */
        virtual void Present()=0;
    };

}

#endif

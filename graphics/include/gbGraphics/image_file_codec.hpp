/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_IMAGE_FILE_CODEC_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_IMAGE_FILE_CODEC_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>

#include <functional>
#include <istream>

namespace GB_GRAPHICS_NAMESPACE {

    namespace ImageFileCodec {
        class Codec {
        public:
            struct ImageProperties {
                int width;
                int height;
            };
            typedef std::function<void(GBCOLOR**, ImageProperties const*)> BufferResizeFunction;
        public:
            virtual ~Codec() {};
            virtual void Decode(std::istream& is, GBCOLOR* buffer, ImageProperties* out_img_props, BufferResizeFunction const& buffer_resize_fun) const=0;
        };
    }

}

#define GB_GRAPHICS_INCLUDE_TOKEN_IMAGE_FILE_CODEC_TGA_HPP_
#include <gbGraphics/image_file_codec_TGA.hpp>
#undef GB_GRAPHICS_INCLUDE_TOKEN_IMAGE_FILE_CODEC_TGA_HPP_

#ifdef GB_GRAPHICS_HAS_LIBPNG
#    define GB_GRAPHICS_INCLUDE_TOKEN_IMAGE_FILE_CODEC_PNG_HPP_
#    include <gbGraphics/image_file_codec_PNG.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_IMAGE_FILE_CODEC_PNG_HPP_
#endif

#ifdef GB_GRAPHICS_HAS_LIBJPEG_TURBO
#    define GB_GRAPHICS_INCLUDE_TOKEN_IMAGE_FILE_CODEC_JPG_HPP_
#    include <gbGraphics/image_file_codec_JPG.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_IMAGE_FILE_CODEC_JPG_HPP_
#endif

#endif

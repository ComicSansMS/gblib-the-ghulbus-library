/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_MESH_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_MESH_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/renderable_3d.hpp>

#include <gbMath/aabb.hpp>

#include <memory>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbMesh;

    class gbDrawingContext3D;
    class gbVertexBuffer;
    typedef std::shared_ptr<gbVertexBuffer> gbVertexBufferPtr;
    class gbIndexBuffer;
    typedef std::shared_ptr<gbIndexBuffer> gbIndexBufferPtr;
    class gbGeometry;
    typedef std::shared_ptr<gbGeometry> gbGeometryPtr;
    class gbVertexDataBase;
    class gbIndexDataBase;
    class gbVertexBinding;
    /// @}

    class gbMesh: public Renderable3D, public boost::noncopyable {
    private:
        gbDrawingContext3D* m_Context;
        gbVertexBufferPtr m_VertexBuffer;
        gbIndexBufferPtr m_IndexBuffer;
        gbGeometryPtr m_Geometry;
        GB_MATH_NAMESPACE::AABB<float> m_Bounding;
    public:
        gbMesh(gbDrawingContext3D& dc3d, gbVertexDataBase const& vertex_data, gbIndexDataBase const& index_data, gbVertexBinding const& binding);
        gbMesh(gbDrawingContext3D& dc3d, gbVertexDataBase const& vertex_data, gbIndexDataBase const& index_data, gbVertexBinding const& binding, GB_MATH_NAMESPACE::AABB<float> bounding_box);
        gbMesh(gbMesh&& rhs);
        ~gbMesh();
        /** @name Renderable3D
         * @{
         */
        void Render();
        GB_MATH_NAMESPACE::AABB<float> const& GetBoundingBox() const;
        /// @}
    private:
        void Init(gbVertexDataBase const& vertex_data, gbIndexDataBase const& index_data, gbVertexBinding const& binding);
    };

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_GRAPHICS_EXCEPTION_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_GRAPHICS_EXCEPTION_HPP_

#include <gbGraphics/config.hpp>

#include <string>

#include <diagnostic/exception.hpp>

namespace GB_GRAPHICS_NAMESPACE {

namespace Exceptions {

        class SDLError: public virtual Diagnostic::Exception {
        private:
            char const* error_string_;
        public:
            SDLError();
            virtual ~SDLError() throw();
            virtual char const* what() const throw();
        };

        class OpenGLError: public virtual Diagnostic::Exception {
        private:
            mutable std::string error_string_;
            unsigned int gl_errcode_;
        public:
            OpenGLError();
            OpenGLError(unsigned int errcode);
            virtual ~OpenGLError() throw();
            virtual char const* what() const throw();
            unsigned int GetOpenGLErrorCode() const;
        };

        class ShaderCompileError: public virtual Diagnostic::Exception {
        private:
            std::string errmsg_;
        public:
            ShaderCompileError(std::string const& errmsg);
            ~ShaderCompileError() throw();
            char const* what() const throw();
        };

        std::ostream& operator<<(std::ostream& os, ShaderCompileError& e);
    }

    namespace Exception_InfoTags {
        //struct source_line_for_parse_error {};
    }

    namespace Exception_InfoRecords {
    }

    namespace Exception_Info {
        //typedef boost::error_info<Exception_InfoTags::description, std::string> description;
    }

}

#endif

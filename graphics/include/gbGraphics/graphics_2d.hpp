/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_DRAW2D_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_DRAW2D_HPP_

#include <gbGraphics/config.hpp>

#include <gbUtil/resource_guardian.hpp>

#include <functional>
#include <memory>
#include <utility>
#include <vector>

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gb2D;
    class gbDrawingContext3D;
    typedef std::shared_ptr<gbDrawingContext3D> gbDrawingContext3DPtr;
    class gbSprite;
    typedef std::shared_ptr<gbSprite> gbSpritePtr;
    class ReadableSurface;
    class Renderable2D;
    class gbProgram;
    typedef std::shared_ptr<gbProgram> gbProgramPtr;
    class gbVertexBinding;
    class gbBlendState;
    typedef std::shared_ptr<gbBlendState> gbBlendStatePtr;
    class gbSamplerState;
    typedef std::shared_ptr<gbSamplerState> gbSamplerStatePtr;
    /// @}

    /** Callback function for Renderable2D objects that are registered for automatic drawing
     */
    typedef std::function<bool(Renderable2D*)> RenderCallbackFunc;

    /** 2d graphics
     */
    class gb2D: public boost::noncopyable {
    public:
        enum DrawingOptions_T {
            DRAW_OPTIONS_DEFAULT=0,
            DRAW_OPTIONS_USE_CURRENT_BLENDSTATE=1
        };
    private:
        typedef std::pair<Renderable2D*, RenderCallbackFunc> DrawingQueueEntry;
        gbDrawingContext3DPtr m_dc3d;
        int m_Width;
        int m_Height;
        gbProgramPtr m_Program;
        std::unique_ptr<gbVertexBinding> m_Binding;
        gbSpritePtr m_Overlay;
        GB_UTIL_NAMESPACE::gbResourceGuardian<gbSprite> m_Sprites;
        std::vector<DrawingQueueEntry> m_DrawingQueue;
        gbBlendStatePtr m_BlendState;
        gbSamplerStatePtr m_SamplerState;
    public:
        /** Constructor
         * @param[in] dc3d 3d drawing context used for rendering.
         * @attention A gb2D object must never outlive its drawing context!
         */
        gb2D(gbDrawingContext3DPtr dc3d);
        /** Destructor
         */
        ~gb2D();
        /** Draw all Renderable2D objects that are registered with the drawing object
         * @note Objects are drawn in the order of their construction/registering
         *       with the gb2D.
         * @note This call will change the blend state and depth test of the underlying
         *       3d drawing context to ensure correct results.
         *       If this is undesired, use DrawWithOptions() instead.
         */
        void Draw();
        /** Draw all Renderable2D objects that are registered with the drawing object
         * @param[in] opt Options used for drawing
         * @note Objects are drawn in the order of their construction/registering
         *       with the gb2D.
         */
        void DrawWithOptions(DrawingOptions_T opt);
        /** Sets the blend state of the underlying drawing context to alpha blending.
         * You will probably want to call this before each call to Draw(), or before
         * calling Renderable2D::Render().
         * @note This function changes the blend state of the underlying drawing context.
         */
        void SetBlendState();
        /** Create a new sprite
         * @param[in] width Width of the sprite in pixels
         * @param[in] height Height of the sprite in pixels
         * @return New gbSprite object
         */
        gbSpritePtr CreateSprite(int width, int height);
        /** Create a new sprite from a surface
         * @param[in] surface Surface containing source image data
         * @return New gbSprite object initialized with image data from source surface
         */
        gbSpritePtr CreateSprite(ReadableSurface& surface);
        /** Get the overlay sprite
         * @return Overlay sprite of the gb2D object
         * @note The overlay sprite is a sprite that covers the whole drawing area
         *       and is drawn after all other objects when calling Draw().
         */
        gbSprite* GetOverlay();
        /** Register an object for automatic drawing
         * @param[in] object A Renderable2D object
         * @note Registered objects will be drawn automatically upon calling Draw().
         *       Objects that where created from the gb2D object (e.g. through
         *       CreateSprite()) are automatically registered for drawing upon
         *       construction.
         * @attention The gb2D object does *not* take ownership of registered objects!
         *            It is up to the user that all registered objects remain valid
         *            throughout the lifetime of the gb2D object.
         */
        void RegisterForDrawing(Renderable2D* object);
        /** Register an object for automatic drawing
         * @param[in] object A Renderable2D object
         * @param[in] callback A callback function invoked on drawing
         * @note Upon each call to draw, the callback function will be invoked.
         *       The object will only be drawn if the callback returns true.
         * @attention The gb2D object does *not* take ownership of registered objects!
         *            It is up to the user that all registered objects remain valid
         *            throughout the lifetime of the gb2D object.
         */
        void RegisterForDrawing(Renderable2D* object, RenderCallbackFunc const& callback);
        /** Unregister an object from automatic drawing
         * @param[in] object A Renderable2D object that is registered for drawing
         * @note If an object was registered multiple times, *all* referenced
         *       to that object will be removed. If the object was not registered
         *       for the drawing, the call will have no effect.
         */
        void UnregisterFromDrawing(Renderable2D* object);
        /** Retrieve the underlying drawing context
         * @return The 3d drawing context used by this object
         */
        gbDrawingContext3D& GetDrawingContext() const;
    };

}

#endif

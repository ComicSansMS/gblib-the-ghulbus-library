/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_GEOMETRY_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_GEOMETRY_OGL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_GEOMETRY_OGL_HPP_
#    error Do not include geometry_OGL.hpp directly; Include geometry.hpp instead
#endif

#include <GL/glew.h>

namespace GB_GRAPHICS_NAMESPACE {

    class gbVertexBuffer_OGL;
    class gbShadowState_OGL;
    class gbIndexBuffer_OGL;
    class gbProgram;

    class gbGeometry_OGL: public gbGeometry {
    private:
        GLuint m_VaoId;
        gbVertexBuffer_OGL* m_VertexBuffer;
        gbIndexBuffer_OGL* m_IndexBuffer;
        gbProgram const* m_Program;
        gbShadowState_OGL* m_glShadowState;
    public:
        gbGeometry_OGL(gbVertexBuffer_OGL* vertex_buffer, gbVertexBinding const& bindings, gbShadowState_OGL* shadow_state);
        gbGeometry_OGL(gbVertexBuffer_OGL* vertex_buffer, gbVertexBinding const& bindings, gbIndexBuffer_OGL* index_buffer, gbShadowState_OGL* shadow_state);
        ~gbGeometry_OGL();
        /** @name gbGeometry
         * @{
         */
        gbVertexBuffer const* GetVertexBuffer() const;
        gbIndexBuffer const* GetIndexBuffer() const;
        gbProgram const* GetProgram() const;
        /// @}
        /** Get the OpenGL Vertex Attribute Object Id
         * @return The OpenGL VAO id
         * @attention This function breaks encapsulation. Use with care!
         */
        GLuint GetOGLVertexAttributeArrayId_OGL() const;
    private:
        void Init(gbVertexBinding const& bindings);
    };

}

#endif

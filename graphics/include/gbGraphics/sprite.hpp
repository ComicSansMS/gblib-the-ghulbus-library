/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_SPRITE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_SPRITE_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/lockable_surface.hpp>
#include <gbGraphics/mesh.hpp>
#include <gbGraphics/rect.hpp>
#include <gbGraphics/renderable_2d.hpp>

#include <gbUtil/resource_guardian.hpp>

#include <memory>

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbSprite;
    typedef std::shared_ptr<gbSprite> gbSpritePtr;
    class Rect;
    class gb2D;
    class gbProgram;
    typedef std::shared_ptr<gbProgram> gbProgramPtr;
    class gbMesh;
    class gbTexture2D;
    typedef std::shared_ptr<gbTexture2D> gbTexture2DPtr;
    /// @}

    /** Sprite
     */
    class gbSprite: public LockableSurface, public Renderable2D,
                    public GB_UTIL_NAMESPACE::gbSupervisedResource<gbSprite>, public boost::noncopyable
    {
    private:
        gb2D* m_gb2d;
        gbProgram* m_Program;
        gbMesh m_Mesh;
        gbTexture2DPtr m_Texture;
        Rect m_RenderTargetDefaultArea;
    public:
        /** Constructor
         * @param[in] guardian Resource guardian for the sprite
         * @param[in] gb2d gb2D object used to create the sprite
         * @param[in] program The shader program used for rendering the sprite
         * @param[in] mesh The mesh quad used for rendering the sprite
         * @param[in] texture The texture storing the sprite's contents for rendering
         */
        gbSprite(GB_UTIL_NAMESPACE::gbResourceGuardian<gbSprite>& guardian, gb2D& gb2d,
                 gbProgram& program, gbMesh&& mesh, gbTexture2DPtr texture);
        /** Destructor
         */
        ~gbSprite();
        /** Partial rendering of sprite
         * @param[in] src_rect Source area in the sprite texture to render from
         * @param[in] dst_rect Destination area on the target surface to render to
         */
        void RenderSegment(Rect const& src_rect, Rect const& dst_rect);
        /** @name LockableSurface
         * @{
         */
        void Lock(GBCOLOR** data, int* pitch);
        void LockRect(Rect const& rect, GBCOLOR** data, int* pitch);
        void Unlock();
        bool IsLocked() const;
        int GetWidth() const;
        int GetHeight() const;
        /// @}
        /** @name Renderable2D
         * @{
         */
        void Render();
        void SetSizeOnScreen(int width, int height);
        int GetWidthOnScreen() const;
        int GetHeightOnScreen() const;
        void SetPositionOnScreen(int x, int y);
        int GetPositionOnScreenX() const;
        int GetPositionOnScreenY() const;
        /// @}
    };

}

#endif

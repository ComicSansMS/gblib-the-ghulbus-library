/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_GRAPHICS_2D_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_GRAPHICS_2D_HPP_

#include <gbGraphics/config.hpp>

#include <gbGraphics/color.hpp>
#include <gbGraphics/lockable_surface.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** 2d drawing
     */
    class gbDraw2D {
    private:
        GBCOLOR*                     m_Data;            ///< pointer to surface
        int                          m_width;            ///< width of the surface
        int                          m_height;            ///< height of the surface
        int                          m_pitch;            ///< number of pixels in a row
        GBCOLOR                      m_clearcolor;        ///< color used for clearing
        SurfaceLock<LockableSurface> m_TextureLock;        ///< lock on m_Texture
    public:
        gbDraw2D(GBCOLOR* data, int width, int height, int pitch);
        gbDraw2D(LockableSurface& tex);
        gbDraw2D(SurfaceLock<LockableSurface> const& target);
        ~gbDraw2D();
        /** Get the width of the locked surface
         * @return The surface width in pixels, -1 if no surface is present
         */
        int GetWidth() const;
        /** Get the height of the locked surface
         * @return The surface height in pixels, -1 if no surface is present
         */
        int GetHeight() const;
        /** Set the clear color
         * @param[in] c The new clear color
         */
        void SetClearColor(GBCOLOR c);

        /** @name Drawing functions:
         * @{
         */
        /** Clear the entire surface with the current clear color
         */
        void Clear();
        /** Draw a single pixel
         * @param[in] x X coordinate of the pixel on the surface
         * @param[in] y Y coordinate of the pixel on the surface
         * @param[in] c New color of the pixel
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool PutPixel(int x, int y, GBCOLOR c);
        /** Blend a single pixel with the given color
         * @param[in] x X coordinate of the pixel on the surface
         * @param[in] y Y coordinate of the pixel on the surface
         * @param[in] c Color that the pixel is to be blended with
         * @return false if any pixels were out of bounds; true otherwise
         * @attention If the surface is not a ReadableSurface, be sure to
         *            only blend on pixels that were previously written by gbDraw2d!
         */
        bool BlendPixel(int x, int y, GBCOLOR c);
        /** Draw a line between two points
         * @param[in] startX X coordinate of the start point
         * @param[in] startY Y coordinate of the start point
         * @param[in] endX X coordinate of the end point
         * @param[in] endY Y coordinate of the end point
         * @param[in] color Color of the line
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool Line(int startX, int startY, int endX, int endY, GBCOLOR color);
        /** Draw a horizontal line between two points
         * @param[in] startX X coordinate of the left point
         * @param[in] endX X coordinate of the right point
         * @param[in] y Y coordinate of the line
         * @param[in] color Color of the line
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool HLine(int startX, int endX, int y, GBCOLOR color);
        /** Draw a vertical line between two points
         * @param[in] x X coordinate of the line
         * @param[in] startY Y coordinate of the upper point
         * @param[in] endY Y coordinate of the lower point
         * @param[in] color Color of the line
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool VLine(int x, int startY, int endY, GBCOLOR color);
        /** Draw a horizontal line with interpolated colors between two points
         * @param[in] startX X coordinate of the left point
         * @param[in] endX X coordinate of the right point
         * @param[in] y Y coordinate of the line
         * @param[in] c1 Color of the left point
         * @param[in] c2 Color of the right point
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool HLineGradient(int startX, int endX, int y, GBCOLOR c1, GBCOLOR c2);
        /** Draw a vertical line with interpolated colors between two points
         * @param[in] x X coordinate of the line
         * @param[in] startY Y coordinate of the upper point
         * @param[in] endY Y coordinate of the lower point
         * @param[in] c1 Color of the upper point
         * @param[in] c2 Color of the lower point
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool VLineGradient(int x, int startY, int endY, GBCOLOR c1, GBCOLOR c2);
        /** Draw a circle
         * @param[in] posX X coordinate of the circle center
         * @param[in] posY Y coordinate of the circle center
         * @param[in] radius Circle radius
         * @param[in] color Color of the circle
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool Circle(int posX, int posY, int radius, GBCOLOR color);
        /** Draw a filled circle
         * @param[in] posX X coordinate of the disc center
         * @param[in] posY Y coordinate of the disc center
         * @param[in] radius Disc radius
         * @param[in] color Color of the disc
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool Disc(int posX, int posY, int radius, GBCOLOR color);
        /** Draw an axis-oriented ellipse
         * @param[in] posX X coordinate of the ellipse center
         * @param[in] posY Y coordinate of the ellipse center
         * @param[in] radiusX Radius in X
         * @param[in] radiusY Radius in Y
         * @param[in] color Color of the ellipse
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool Ellipse(int posX, int posY, int radiusX, int radiusY, GBCOLOR color);
        /** Draw a filled axis-oriented ellipse
         * @param[in] posX X coordinate of the ellipse center
         * @param[in] posY Y coordinate of the ellipse center
         * @param[in] radiusX Radius in X
         * @param[in] radiusY Radius in Y
         * @param[in] color Color of the ellipse
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool FilledEllipse(int posX, int posY, int radiusX, int radiusY, GBCOLOR color);
        /** Draw an axis-oriented ellipse
         * @param[in] posX X coordinate of the ellipse center
         * @param[in] posY Y coordinate of the ellipse center
         * @param[in] radiusX Radius in X
         * @param[in] radiusY Radius in Y
         * @param[in] ul If true, the upper left segment will be drawn
         * @param[in] ur If true, the upper right segment will be drawn
         * @param[in] ll If true, the lower left segment will be drawn
         * @param[in] lr If true, the lower right segment will be drawn
         * @param[in] color Color of the ellipse
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool EllipseSegment(int posX, int posY, int radiusX, int radiusY, bool ul, bool ur, bool ll, bool lr, GBCOLOR color);
        /** Draw a rectangle
         * @param[in] left Left limit of the rectangle
         * @param[in] top Upper limit of the rectangle
         * @param[in] right Right limit of the rectangle
         * @param[in] bottom Lower limit of the rectangle
         * @param[in] color Color of the rectangle
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool Rectangle(int left, int top, int right, int bottom, GBCOLOR color);
        /** Draw a rectangle with rounded corners
         * @param[in] left Left limit of the rectangle
         * @param[in] top Upper limit of the rectangle
         * @param[in] right Right limit of the rectangle
         * @param[in] bottom Lower limit of the rectangle
         * @param[in] radiusX Number of pixels that are rounded in X (must be < width/2)
         * @param[in] radiusY Number of pixels that are rounded in Y (must be < height/2)
         * @param[in] color Color of the rectangle
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool RoundedRectangle(int left, int top, int right, int bottom, int radiusX, int radiusY, GBCOLOR color);
        /** Draw a rectangle with interpolated colors
         * @param[in] left Left limit of the rectangle
         * @param[in] top Upper limit of the rectangle
         * @param[in] right Right limit of the rectangle
         * @param[in] bottom Lower limit of the rectangle
         * @param[in] ul Color of the upper left corner point
         * @param[in] ur Color of the upper right corner point
         * @param[in] ll Color of the lower left corner point
         * @param[in] lr Color of the lower right corner point
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool RectangleGradient(int left, int top, int right, int bottom, 
                               GBCOLOR ul, GBCOLOR ur, GBCOLOR ll, GBCOLOR lr);
        /** Draw a filled rectangle
         * @param[in] left Left limit of the box
         * @param[in] top Upper limit of the box
         * @param[in] right Right limit of the box
         * @param[in] bottom Lower limit of the box
         * @param[in] color Color of the box
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool Box(int left, int top, int right, int bottom, GBCOLOR color);
        /** Draw a filled rectangle with rounded corners
         * @param[in] left Left limit of the box
         * @param[in] top Upper limit of the box
         * @param[in] right Right limit of the box
         * @param[in] bottom Lower limit of the box
         * @param[in] radiusX Number of pixels that are rounded in X (must be < width/2)
         * @param[in] radiusY Number of pixels that are rounded in Y (must be < height/2)
         * @param[in] color Color of the box
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool RoundedBox(int left, int top, int right, int bottom, int radiusX, int radiusY, GBCOLOR color);
        /** Draw a filled rectangle with interpolated colors
         * @param[in] left Left limit of the box
         * @param[in] top Upper limit of the box
         * @param[in] right Right limit of the box
         * @param[in] bottom Lower limit of the box
         * @param[in] ul Color of the upper left corner point
         * @param[in] ur Color of the upper right corner point
         * @param[in] ll Color of the lower left corner point
         * @param[in] lr Color of the lower right corner point
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool BoxGradient(int left, int top, int right, int bottom, 
                         GBCOLOR ul, GBCOLOR ur, GBCOLOR ll, GBCOLOR lr);
        /// @}
        /** @name Image drawing:
         * @{
         */
        /// @}
        /** @name Stencil functions
         * @{
         */
        /** Apply a stencil mask
         * @param[in] x X coordinate of the upper left corner of the target area
         * @param[in] y Y coordinate of the upper left corner of the target area
         * @param[in] stencil Stencil data, the alpha values of this field will be applied to the target area
         * @param[in] stencil_width Width of the stencil in pixels
         * @param[in] stencil_height Height of the stencil in pixels
         * @param[in] stencil_pitch Pitch of the stencil in pixels
         * @return false if any pixels were out of bounds; true otherwise
         */
        bool ApplyStencil(int x, int y, GBCOLOR const* stencil, int stencil_width, int stencil_height, int stencil_pitch);
        /// @}
    };

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_INPUT_CAMERA_SPHERICAL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_INPUT_CAMERA_SPHERICAL_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/event.hpp>
#include <gbGraphics/event_manager.hpp>

#include <gbMath/camera.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbWindow;

    namespace Input {

        class CameraSpherical: public GhulbusGraphics::Events::MouseInput {
        public:
            struct Config_T {
                bool UseMouseMove;
                bool UseMouseZoom;
                bool UseMouseWheelZoom;
                float SpeedHorizontal;
                float SpeedVertical;
                float SpeedZoom;
                float SpeedMouseWheelZoom;
                Events::MouseButton MouseRotateButton;
                Events::MouseButton MouseZoomButton;
                float CameraDistanceMin;
                float CameraDistanceMax;
            };
        private:
            gbWindow* m_Window;
            Events::MouseState m_State;
            float m_CameraDistance;
            float m_CameraAngleHorizontal;
            float m_CameraAngleVertical;
            bool m_UpdateCamera;
            GB_MATH_NAMESPACE::Camera<float> m_Camera;
            Config_T m_Cfg;
            gbEventManager::HandlerId m_HandlerIdMouseMove;
            gbEventManager::HandlerId m_HandlerIdMouseWheel;
        public:
            CameraSpherical(gbWindow& window);
            ~CameraSpherical();
            bool Handler(Events::MouseState const* state, Events::gbEvent_Type_Mouse type);
            GB_MATH_NAMESPACE::Matrix4f GetCameraMatrix() const;
            float GetCameraAngleHorizontal() const;
            float GetCameraAngleVertical() const;
            float GetCameraDistance();
            void SetCameraAngleHorizontal(float phi);
            void SetCameraAngleVertical(float theta);
            void SetCameraDistance(float dist);
            Config_T const& GetInputConfig();
            void SetInputConfig(Config_T const& config);
        private:
            void InitConfig();
            void ProcessMouseWheel(GhulbusGraphics::Events::MouseState const* state);
            void ProcessMouseMove(GhulbusGraphics::Events::MouseState const* state);
            void UpdateCamera();
        };

    }

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_BASE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_BASE_HPP_

#include <gbGraphics/config.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbVertexFormatBase;

    /** Base class for gbVertexData.
     * @ingroup gb3d
     * This class is used for polymorphically passing vertex data into a gbVertexBuffer.
     * A user usually does not need to work with gbVertexDataBase at all.
     * Use the concrete gbVertexData instead.
     */
    class gbVertexDataBase {
    public:
        /** Destructor
         */
        virtual ~gbVertexDataBase() {};
        /** Get the size of the vertex data
         * @return Number of vertices that fit in the vertex data
         */
        virtual int NumberOfVertices() const=0;
        /** Get a pointer to the raw vertex data
         * @return Pointer to the beginning of the storage area
         * @note Users usually don't need to call this.
         */
        virtual void const* GetRawData() const=0;
        /** Get the raw size of the vertex data storage
         * @return Storage size in bytes
         * @note Users usually don't need to call this.
         */
        virtual int GetRawSize() const=0;
        /** Get the underlying vertex format
         * @return The gbVertexFormat describing the vertex data
         */
        virtual gbVertexFormatBase const& GetVertexFormat() const=0;
    };

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_SHADER_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_SHADER_OGL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_SHADER_OGL_HPP_
#    error Do not include shader_OGL.hpp directly; Include shader.hpp instead
#endif

#include <string>

#include <GL/glew.h>

namespace GB_GRAPHICS_NAMESPACE {

    class gbShader_OGL: public gbShader {
    private:
        GLuint m_ShaderId;
        std::string m_CompileMessage;
    public:
        gbShader_OGL(GLuint shader_id, std::string const& code);
        ~gbShader_OGL();
        /** @name gbShader
         * @{
         */
        ShaderType::ShaderType_T GetShaderType() const;
        std::string const& GetShaderCompileMessage() const;
        /// @}
        GLuint GetOGLShaderId_OGL() const;
    private:
        void CompileShader(std::string const& code);
    };

}

#endif

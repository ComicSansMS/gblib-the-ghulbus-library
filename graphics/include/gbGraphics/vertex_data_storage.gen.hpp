/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

/* Autogenerated from create_vertex_data_storage.hpp.lua */

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_STORAGE_GEN_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_STORAGE_GEN_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/vertex_data_fwd.hpp>

#include <type_traits>

namespace GB_GRAPHICS_NAMESPACE {

    /// @cond
    namespace VertexData {

        /* Storage */
        template<
            typename S0, 
            typename S1=VertexComponent::Nil, 
            typename S2=VertexComponent::Nil, 
            typename S3=VertexComponent::Nil, 
            typename S4=VertexComponent::Nil, 
            typename S5=VertexComponent::Nil, 
            typename S6=VertexComponent::Nil, 
            typename S7=VertexComponent::Nil, 
            typename S8=VertexComponent::Nil, 
            typename S9=VertexComponent::Nil, 
            typename S10=VertexComponent::Nil, 
            typename S11=VertexComponent::Nil, 
            typename S12=VertexComponent::Nil, 
            typename S13=VertexComponent::Nil, 
            typename S14=VertexComponent::Nil, 
            typename S15=VertexComponent::Nil >
        struct Storage;

        template<typename S0>
        struct Storage<S0, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
        };

        template<typename S0, typename S1>
        struct Storage<S0, S1, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
        };

        template<typename S0, typename S1, typename S2>
        struct Storage<S0, S1, S2, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
        };

        template<typename S0, typename S1, typename S2, typename S3>
        struct Storage<S0, S1, S2, S3, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4>
        struct Storage<S0, S1, S2, S3, S4, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, VertexComponent::Nil, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
            S9 m9;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
            S9 m9;
            S10 m10;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, VertexComponent::Nil, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
            S9 m9;
            S10 m10;
            S11 m11;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, VertexComponent::Nil, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
            S9 m9;
            S10 m10;
            S11 m11;
            S12 m12;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, VertexComponent::Nil, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
            S9 m9;
            S10 m10;
            S11 m11;
            S12 m12;
            S13 m13;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14>
        struct Storage<S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            VertexComponent::Nil>
        {
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
            S9 m9;
            S10 m10;
            S11 m11;
            S12 m12;
            S13 m13;
            S14 m14;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct Storage{
            S0 m0;
            S1 m1;
            S2 m2;
            S3 m3;
            S4 m4;
            S5 m5;
            S6 m6;
            S7 m7;
            S8 m8;
            S9 m9;
            S10 m10;
            S11 m11;
            S12 m12;
            S13 m13;
            S14 m14;
            S15 m15;
        };

        /* StorageElement */
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
        typename S5, typename S6, typename S7, typename S8, typename S9, 
        typename S10, typename S11, typename S12, typename S13, typename S14, 
        typename S15> struct StorageElement;

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<0, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S0 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<1, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S1 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<2, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S2 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<3, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S3 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<4, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S4 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<5, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S5 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<6, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S6 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<7, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S7 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<8, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S8 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<9, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S9 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<10, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S10 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<11, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S11 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<12, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S12 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<13, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S13 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<14, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S14 type;
        };

        template<typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement<15, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>
        {
            typedef S15 type;
        };

        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        struct StorageElement
        {
            static_assert(sizeof(S0) == 0, "Invalid storage index");
        };

        /* GetStorageElement */
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 0, typename StorageElement<0, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            return store.m0;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 1, typename StorageElement<1, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m1;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 2, typename StorageElement<2, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m2;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 3, typename StorageElement<3, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m3;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 4, typename StorageElement<4, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m4;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 5, typename StorageElement<5, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m5;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 6, typename StorageElement<6, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m6;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 7, typename StorageElement<7, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m7;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 8, typename StorageElement<8, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m8;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 9, typename StorageElement<9, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m9;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 10, typename StorageElement<10, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m10;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 11, typename StorageElement<11, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m11;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 12, typename StorageElement<12, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m12;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 13, typename StorageElement<13, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m13;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 14, typename StorageElement<14, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m14;
        }
        template<size_t I, typename S0, typename S1, typename S2, typename S3, typename S4, 
            typename S5, typename S6, typename S7, typename S8, typename S9, 
            typename S10, typename S11, typename S12, typename S13, typename S14, 
            typename S15>
        typename std::enable_if< I == 15, typename StorageElement<15, S0, S1, S2, S3, S4, 
            S5, S6, S7, S8, S9, 
            S10, S11, S12, S13, S14, 
            S15>::type& >::type
        GetStorageElement(Storage<S0, S1, S2, S3, S4, 
                S5, S6, S7, S8, S9, 
                S10, S11, S12, S13, S14, 
                S15>& store)
        {
            static_assert(!VertexComponent::Traits::IsNil<S1>::value, "Storage index out of bounds");
            return store.m15;
        }
    }
    /// @endcond

}

#endif

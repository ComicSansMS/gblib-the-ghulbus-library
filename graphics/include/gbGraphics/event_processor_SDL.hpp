/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_EVENT_PROCESSOR_SDL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_EVENT_PROCESSOR_SDL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_EVENT_PROCESSOR_SDL_HPP_
#    error Do not include event_processor_SDL.hpp directly; Include event_processor.hpp instead
#endif

#include <gbGraphics/event.hpp>

#include <cstdint>
#include <functional>
#include <unordered_map>

namespace GB_GRAPHICS_NAMESPACE {

    class gbWindow_SDL;

    class gbEventProcessor_SDL: public gbEventProcessor {
    public:
        typedef std::function<void()> ResizeCallback;
    private:
        std::unordered_map<uint32_t, gbWindow_SDL*> m_IdWindowMap;
        std::unordered_map<uint32_t, ResizeCallback> m_IdResizeCallbackMap;
        gbEventManager* m_EventManager;
        gbWindow_SDL* m_ActiveWindowSDL;
    public:
        gbEventProcessor_SDL();
        ~gbEventProcessor_SDL();
        /** @name gbEventProcessor
         * @{
         */
        void ProcessMessages();
        /// @}
        void RegisterWindow_SDL(gbWindow_SDL* window, uint32_t id, ResizeCallback const& resize_callback);
        gbWindow_SDL* GetWindowById_SDL(uint32_t id) const;
        void ActiveWindowChange(gbWindow_SDL* window);
        Events::Key TranslateSDLKey(int32_t sdl_keycode);
    private:
        void NotifyWindowResize(uint32_t id) const;
    };

}

#endif

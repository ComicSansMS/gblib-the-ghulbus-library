/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_IMAGE_LOADER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_IMAGE_LOADER_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/image.hpp>
#include <gbGraphics/image_file_codec.hpp>

#include <istream>
#include <memory>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbImageLoader: boost::noncopyable {
    private:
        gbImage m_Image;
    public:
        gbImageLoader();
        gbImageLoader(char const* filename, ImageFileCodec::Codec const& codec);
        ~gbImageLoader();
        void ReadFile(char const* filename, ImageFileCodec::Codec const& codec);
        void ReadStream(std::istream& is, ImageFileCodec::Codec const& codec);
        int GetImageWidth() const;
        int GetImageHeight() const;
        gbImage GetImage();
        gbImage GetImageCopy() const;
    };

    gbImage LoadImageFromFile(char const* filename, ImageFileCodec::Codec const& codec);

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_INDEX_DATA_BASE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_INDEX_DATA_BASE_HPP_

#include <gbGraphics/config.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbIndexDataBase {
    public:
        /** Primitive type
         */
        enum Primitive_T {
            PRIMITIVE_NONE,                            ///< No type
            PRIMITIVE_POINT,                        ///< Point list
            PRIMITIVE_LINE,                            ///< Line list
            PRIMITIVE_LINE_STRIP,                    ///< Line strip
            PRIMITIVE_TRIANGLE,                        ///< Triangle list
            PRIMITIVE_TRIANGLE_STRIP,                ///< Triangle strip
            PRIMITIVE_LINE_ADJACENCY,                ///< Line list with adjacency
            PRIMITIVE_LINE_STRIP_ADJACENCY,            ///< Line strip with adjacency
            PRIMITIVE_TRIANGLE_ADJACENCY,            ///< Triangle list with adjacency
            PRIMITIVE_TRIANGLE_STRIP_ADJACENCY        ///< Triangle strip with adjacency
        };
        /** Index type
         */
        enum IndexType_T {
            INDEX_TYPE_NONE,            ///< No type
            INDEX_TYPE_UINT8,            ///< 8-bit unsigned integer
            INDEX_TYPE_UINT16,            ///< 16-bit unsigned integer
            INDEX_TYPE_UINT32            ///< 32-bit unsigned integer
        };
    public:
        /** Destructor
         */
        virtual ~gbIndexDataBase() {};
        /** Get the number of primitives in the index data
         * @return Number of primitives
         */
        virtual int NumberOfPrimitives() const=0;
        /** Get the number of indices in the index data
         * @return Number of indices
         */
        virtual int NumberOfIndices() const=0;
        /** Get the raw size of index data storage
         * @return Storage size in bytes
         * @note Users usually don't need to call this.
         */
        virtual int GetRawSize() const=0;
        /** Get a pointer to the raw index data
         * @return Pointer to the beginning of the storage area
         * @note Users usually don't need to call this.
         */
        virtual void const* GetRawData() const=0;
        /** Get the primitive type of the stored index data
         * @return Index primitive type
         */
        virtual Primitive_T GetPrimitiveType() const=0;
        /** Get the type of the stored index data
         * @return Index data type
         */
        virtual IndexType_T GetIndexType() const=0;
    };

}

#endif

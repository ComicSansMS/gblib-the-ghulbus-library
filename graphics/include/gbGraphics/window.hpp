/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_WINDOW_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_WINDOW_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>
#include <gbGraphics/window_desc.hpp>

#include <memory>
#include <string>
#include <vector>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbEventProcessor;
    class gbEventManager;
    class gbDrawingContext3D;
    typedef std::shared_ptr<gbDrawingContext3D> gbDrawingContext3DPtr;

    class gbWindow;
    typedef std::shared_ptr<gbWindow> gbWindowPtr;

    /** Window
     */
    class gbWindow: public boost::noncopyable {
    public:
        enum WINDOW_API_TYPE {
            WIN_API_DEFAULT,
            WIN_API_SDL,
            WIN_API_WIN32
        };
        struct DeviceIdentifier {
            std::string Name;
        };
    public:
        /** Destructor
         */
        virtual ~gbWindow() {};
        /** Get the associated event processor
         */
        virtual gbEventProcessor* GetEventProcessor() const=0;
        /** Get the associated event manager
         */
        virtual gbEventManager* GetEventManager()=0;
        /** Set the window title
         * @param[in] str Title to be displayed in the window's title bar
         */
        virtual void SetWindowTitle(char const* str)=0;
        /** Set the window full screen state
         * @param[in] fullscreen_state Desired window fullscreen state.
         */
        virtual void SetFullscreen(Window::FullscreenState_T fullscreen_state) = 0;
        /** Get the current width of the window
         *@return Width of the window in pixels
         */
        virtual int GetWidth() const=0;
        /** Get the current height of the window
         *@return Height of the window in pixels
         */
        virtual int GetHeight() const=0;
        /** Get the current aspect ratio of the window
         * @return Aspect ratio of the window
         */
        virtual float GetAspectRatio() const=0;
        /** Retrieve a drawing context for 3D graphics
         * @param[in] window gbWindow on which the drawing context should be created
         * @return Pointer to a gbDrawingContext3D
         * @note Each window can only have a single active drawing context.
         *       If there is already an active 3d drawing context, a shared pointer
         *       to that context will be returned. If there is an active gbDrawingContext2D,
         *       this function will fail with an exception.
         */
        virtual gbDrawingContext3DPtr CreateDrawingContext3D()=0;
        /** Retrieve the color scheme for the current window
         * @return The color scheme used by the underlying window API
         */
        virtual gbColor::ColorScheme const& GetAssociatedColorScheme() const=0;
        /** Show or hide the mouse cursor inside the window
         */
        virtual void SetMouseCursorVisible(bool show_cursor)=0;
        /** Check whether the mouse cursor is shown in this window
         */
        virtual bool GetMouseCursorVisible() const=0;
        /** Toggles capturing of the mouse cursor
         * @note Capturing only prevents the cursor from leaving the window,
         *       but it will stop moving once it hits a window border. For
         *       allowing continuous mouse motions, use relative mouse mode.
         *       See also @ref SetRelativeMouseMode()
         */
        virtual void SetCaptureMouseCursor(bool capture_cursor)=0;
        /** Checks whether the window is set to capture the mouse
         */
        virtual bool IsMouseCursorCaptured() const=0;
        /** Toggle relative mouse mode
         * In relative mouse mode the cursor is automatically reset
         * to the center of the window once it gets too close to a window
         * border. Once mouse mode is activated, be sure to handle the
         * GBEVENT_MOUSE_MOVE_REL instead of the usual GBEVENT_MOUSE_MOVE
         * events, as they automatically adjust to cursor resets.
         * It is usually highly desirable to also use mouse capturing
         * in relative mouse mode, to prevent the cursor from leaving the
         * window. See @ref SetCaptureMouseCursor() for details.
         */
        virtual void SetRelativeMouseMode(bool use_relative_mouse_mode)=0;
        /** Checks whether the window is currently in relative mouse mode
         * @note See @ref SetRelativeMouseMode() for details about relative mouse mode
         */
        virtual bool IsInRelativeMouseMode() const=0;
        /** Factory method for gbWindow
         * @param[in] win_api_type Window API to be used for creating the window
         * @attention While it is possible to mix API types for different windows
         *            in the same process, doing so may lead to inconsistent
         *            color schemes between windows. 
         *            See @ref gbColor::SetActiveColorScheme() for details.
         */
        static gbWindowPtr Create(int width, int height, WINDOW_API_TYPE win_api_type=WIN_API_DEFAULT);
        /** Factory method for gbWindow
         * @param[in] window_descriptor Additional window construction options
         * @param[in] win_api_type Window API to be used for creating the window
         * @attention While it is possible to mix API types for different windows
         *            in the same process, doing so may lead to inconsistent
         *            color schemes between windows. 
         *            See @ref gbColor::SetActiveColorScheme() for details.
         */
        static gbWindowPtr Create(int width, int height, Window::WindowDesc const& window_descriptor,
                                  WINDOW_API_TYPE win_api_type=WIN_API_DEFAULT);
        /** Get a list of all devices installed on the system
         * @param[in] win_api_type Window API to be used for enumeration
         * @return A list of all available graphics devices
         * @attention Acquired DeviceIdentifiers are not portable between different window APIs
         */
        static std::vector<DeviceIdentifier> EnumerateDevices(WINDOW_API_TYPE win_api_type=WIN_API_DEFAULT);
    };
}

#ifdef GB_GRAPHICS_HAS_SDL
#    define GB_GRAPHICS_INCLUDE_TOKEN_WINDOW_SDL_HPP_
#    include <gbGraphics/window_SDL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_WINDOW_SDL_HPP_
#endif

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_RENDERABLE_3D_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_RENDERABLE_3D_HPP_

#include <gbGraphics/config.hpp>

#include <gbMath/aabb.hpp>

#include <memory>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class Renderable3D {
    private:
        bool m_IsShown;
    public:
        Renderable3D();
        virtual ~Renderable3D() {};
        virtual void Render()=0;
        virtual GB_MATH_NAMESPACE::AABB<float> const& GetBoundingBox() const=0;
        virtual void Show(bool is_shown);
        virtual bool IsShown() const;
    };

}

#endif

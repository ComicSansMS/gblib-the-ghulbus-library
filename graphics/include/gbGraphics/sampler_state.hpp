/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_SAMPLER_STATE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_SAMPLER_STATE_HPP_

#include <gbGraphics/config.hpp>

#include <gbGraphics/color.hpp>

#include <boost/utility.hpp>

#include <memory>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbSamplerState;
    typedef std::shared_ptr<gbSamplerState> gbSamplerStatePtr;
    /// @}

    namespace Sampling {
        enum SamplingMode_T {
            SAMPLE_POINT,
            SAMPLE_LINEAR
        };
        enum AddressMode_T {
            ADDRESS_MODE_WRAP,
            ADDRESS_MODE_MIRROR,
            ADDRESS_MODE_CLAMP,
            ADDRESS_MODE_BORDER
        };

        struct StateDesc {
            SamplingMode_T MinFilter;
            SamplingMode_T MagFilter;
            SamplingMode_T MipFilter;
            AddressMode_T AddressU;
            AddressMode_T AddressV;
            AddressMode_T AddressW;
            GBCOLOR BorderColor;
            StateDesc()
                :MinFilter(SAMPLE_POINT), MagFilter(SAMPLE_POINT), MipFilter(SAMPLE_LINEAR),
                 AddressU(ADDRESS_MODE_WRAP), AddressV(ADDRESS_MODE_WRAP), AddressW(ADDRESS_MODE_WRAP),
                 BorderColor(gbColor::ARGB(0, 0, 0, 0))
            {}
            StateDesc( SamplingMode_T min, SamplingMode_T mag, SamplingMode_T mip,
                       AddressMode_T u, AddressMode_T v, AddressMode_T w, GBCOLOR border)
                :MinFilter(min), MagFilter(mag), MipFilter(mip),
                 AddressU(u), AddressV(v), AddressW(w), BorderColor(border)
            {}
        };
    }

    class gbSamplerState: public boost::noncopyable {
    public:
        virtual ~gbSamplerState() {}
        virtual Sampling::StateDesc const& GetDescriptor() const=0;
    };

}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_SAMPLER_STATE_OGL_HPP_
#    include <gbGraphics/sampler_state_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_SAMPLER_STATE_OGL_HPP_
#endif

#endif

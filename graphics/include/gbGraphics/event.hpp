/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_EVENT_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_EVENT_HPP_

#include <gbGraphics/config.hpp>

#include <cstdint>
#include <functional>

//The lengthy definitions of the platform specific keycodes have
//been exported to a seperate file
#define GB_GRAPHICS_INCLUDE_TOKEN_EVENT_KEYCODES_HPP_
#    include <gbGraphics/event_keycodes.hpp>
#undef  GB_GRAPHICS_INCLUDE_TOKEN_EVENT_KEYCODES_HPP_

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbWindow;

    /** Base class for all events
     */
    class gbEvent : public boost::noncopyable {
    protected:
        virtual ~gbEvent()=0;
    };

    /** Events processed by gbEventManager
     */
    namespace Events {

        /** Return type for event handler functions.
         */
        enum EventHandler_Return {
            GBHANDLER_FORWARD_EVENT,    ///< Forward the event to the next registered handler for processing (or the
                                        ///<  gbEventManager if no handlers remain).
            GBHANDLER_SWALLOW_EVENT     ///< Swallows the event, no other handlers will get invoked for this event.
        };

        /** The different types of mouse events
         */
        enum gbEvent_Type_Mouse {
            GBEVENT_MOUSE_ALL=0,        ///< Any mouse input event.
            GBEVENT_MOUSE_MOVE,         ///< The mouse was moved.
            GBEVENT_MOUSE_MOVE_REL,     ///< Same as GBEVENT_MOUSE_MOVE, but in terms of position change instead of
                                        ///<  absolute coordinates.
            GBEVENT_MOUSE_BUTTONDOWN,   ///< A mouse button was pressed.
            GBEVENT_MOUSE_BUTTONUP,     ///< A mouse button was released.
            GBEVENT_MOUSE_MOUSEWHEEL    ///< The mousewheel was scrolled.
        };

        /** Mouse buttons
         */
        enum MouseButton {
            GBMOUSE_BUTTON_NONE=0,
            GBMOUSE_BUTTON_LEFT=1,
            GBMOUSE_BUTTON_RIGHT=2,
            GBMOUSE_BUTTON_MIDDLE=3
        };

        /** Describes the state of a mouse device.
         */
        struct MouseState {
            int posx;                      ///< X position in pixels (origin upper left corner of the viewport).
            int posy;                      ///< Y position in pixels (origin upper left corner of the viewport).
            bool lbutton;                  ///< The event concerns the left mouse button.
            bool rbutton;                  ///< The event concerns the right mouse button.
            bool mbutton;                  ///< The event concerns the middle mouse button.
            int mousewheel;                ///< Positive value means mousewheel-up, negative means mousewheel-down.
            uint64_t timestamp;            ///< Timestamp of the event.
        };

        /** Test whether a specific button is pressed in a MouseState.
         */
        inline bool CheckMouseButton(MouseState const& state, MouseButton button) {
            return ((button == GBMOUSE_BUTTON_LEFT) && state.lbutton) ||
                   ((button == GBMOUSE_BUTTON_RIGHT) && state.rbutton) ||
                   ((button == GBMOUSE_BUTTON_MIDDLE) && state.mbutton);
        }

        /** A handler function for processing mouse input events.
         */
        typedef std::function<EventHandler_Return(MouseState const& state, gbEvent_Type_Mouse type)> MouseInputHandler;

        /** An event handler for mouse input
         * Derive from this class to handle mouse input events with gbEventManager
         */
        class MouseInput: public gbEvent {
        public:
            virtual ~MouseInput() {};
            /** Event handler callback
             * @param[in] state MouseState object describing the event
             * @param[in] type gbEventType code describing the type of the event
             * @return If the handler returns true, the event will be removed from the event queue;
             *         if it returns false, the gbEventManager will redirect it to the next available handler
             *         or, if no more handlers are available, try to process the event itself.
             */
            virtual bool Handler(MouseState const* state, gbEvent_Type_Mouse type)=0;
        };

        
        /** The different types of keyboard events
         */
        enum gbEvent_Type_Keyboard {
            GBEVENT_KEYB_ALL=0,            ///< any keyboard input event
            GBEVENT_KEYB_KEYDOWN,        ///< a key on the keyboard was pressed
            GBEVENT_KEYB_KEYUP            ///< a key on the keyboard was released
        };

        /** Describes the state of a keyboard device
         * @note The size of the keyboard state struct is determined by
         *       the GBKEY__NUMBER_OF_KEYSTATES enum-constant defined in
         *       the gbEvent.impl.hpp file
         */
        struct KeyboardState {
            bool keys[GBKEY_NUMBER_OF_KEYSTATES];
            uint64_t timestamp;
        };

        /** Test whether a specific key is pressed in a KeyboardState
         */
        inline bool CheckKeyboardKey(KeyboardState const& state, Key key) {
            return state.keys[key];
        }

        /** A handler function for processing keyboard input events.
         */
        typedef std::function<EventHandler_Return(Key k, gbEvent_Type_Keyboard type)> KeyboardInputHandler;

        /** An event handler for keyboard input
         * Derive from this class to handle keyboard input events with gbEventManager
         */
        class KeyboardInput: public gbEvent {
        public:
            virtual ~KeyboardInput() {};
            /** Event handler callback
             * @param[in] k The keycode of the key that triggered the event
             * @param[in] type gbEventType code describing the type of the event
             * @return If the handler returns true, the event will be removed from the event queue;
             *         if it returns false, the gbEventManager will redirect it to the next available handler
             *         or, if no more handlers are available, try to process the event itself.
             */
            virtual bool Handler(Key k, gbEvent_Type_Keyboard type)=0;
        };

        /** The different types of window events
         */
        enum gbEvent_Type_WindowEvent {
            GBEVENT_WINDOW_FOCUS_GAINED,
            GBEVENT_WINDOW_FOCUS_LOST,
            GBEVENT_WINDOW_RESIZE
        };

        /** Describes the state of a window associated with a window event
         */
        struct WindowState {
            gbWindow* window;
        };

        /** A handler function for processing window events.
         */
        typedef std::function<void(WindowState const& state, gbEvent_Type_WindowEvent type)> WindowEventHandler;

        /** An event handler for window events
         * Derive from this class to handle window events with gbEventManager
         */
        class WindowEvent: public gbEvent {
        public:
            virtual ~WindowEvent() {};
            virtual void Handler(WindowState const* state, gbEvent_Type_WindowEvent type)=0;
        };
    }

}

#endif

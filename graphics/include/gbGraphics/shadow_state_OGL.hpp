/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_SHADOW_STATE_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_SHADOW_STATE_OGL_HPP_

#include <gbGraphics/config.hpp>

#ifdef GB_GRAPHICS_HAS_OPENGL

#include <array>
#include <vector>

#include <GL/glew.h>

namespace GB_GRAPHICS_NAMESPACE {

    class gbShadowState_OGL {
    private:
        template<typename Id_T>
        struct OpenGL_Id {
            Id_T id_;
            bool isValid_;
            inline OpenGL_Id& operator=(Id_T id) {
                id_ = id;  isValid_ = true;
                return *this;
            }
            inline bool operator==(Id_T id) {
                return (id_ == id) || (!isValid_);
            }
            inline bool operator!=(Id_T id) {
                return (id_ != id) || (!isValid_);
            }
        };
        struct BlendState {
            OpenGL_Id<bool> enabled;
            OpenGL_Id<GLenum> mode;
            OpenGL_Id<GLenum> sfactor;
            OpenGL_Id<GLenum> dfactor;
        };
        std::vector<OpenGL_Id<GLuint>> m_Buffers;
        OpenGL_Id<GLuint> m_VertexArray;
        OpenGL_Id<GLuint> m_Program;
        OpenGL_Id<GLuint> m_RootElementArray;
        GLint m_MaxTextureUnits;
        OpenGL_Id<GLuint> m_ActiveTextureUnit;
        std::vector<std::array<OpenGL_Id<GLuint>, 9>> m_Textures;
        OpenGL_Id<bool> m_DepthTestEnabled;
        BlendState m_BlendState;
    public:
        gbShadowState_OGL();
        void Invalidate();
        void SetDepthTest(bool enabled);
        void SetBlending(bool enabled);
        void BindBuffer(GLenum target, GLuint buffer_id);
        void ReleaseBuffer(GLenum target, GLuint buffer_id);
        void BindVertexArray(GLuint arr);
        void ReleaseVertexArray(GLuint arr);
        void UseProgram(GLuint program);
        void ReleaseProgram(GLuint program);
        void ActiveTexture(GLuint texture_unit);
        void BindTexture(GLuint texture_unit, GLenum target, GLuint texture);
        void ReleaseTexture(GLenum target, GLuint texture);
        GLuint AssignTextureUnit(GLuint texture, GLuint program);
        void BlendFunc(GLenum sfactor, GLenum dfactor);
        void BlendEquation(GLenum mode);
    private:
        void InvalidateBlendState(BlendState& blend_state) const;
        void PushElementArrayBuffer();
        void PopElementArrayBuffer();
    };

}

#endif

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_GEOMETRY_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_GEOMETRY_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/vertex_data_base.hpp>
#include <gbGraphics/vertex_binding.hpp>

#include <memory>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {
    
    /** @name Forward declarations
     * @{
     */
    class gbGeometry;
    typedef std::shared_ptr<gbGeometry> gbGeometryPtr;

    class gbVertexBuffer;
    class gbIndexBuffer;
    /// @}

    /** Geometry class.
     * A geometry associates specific vertex and index buffers with the vertex bindings
     * for a specific shader.
     * @note Use gbVertexBuffer::CreateGeometry() for construction
     */
    class gbGeometry: public boost::noncopyable {
    public:
        /** Destructor
         */
        virtual ~gbGeometry() {}
        /** Get a pointer to the associated vertex buffer
         * @return Pointer to vertex buffer
         */
        virtual gbVertexBuffer const* GetVertexBuffer() const=0;
        /** Get a pointer to the associated index buffer
         * @return Pointer to index buffer or nullptr for unindexed geometry
         */
        virtual gbIndexBuffer const* GetIndexBuffer() const=0;
        /** Get a  pointer to the associated program
         * @return Pointer to the program
         */
        virtual gbProgram const* GetProgram() const=0;
    };

}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_GEOMETRY_OGL_HPP_
#    include <gbGraphics/geometry_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_GEOMETRY_OGL_HPP_
#endif

#endif

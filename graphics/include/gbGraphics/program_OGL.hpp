/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_PROGRAM_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_PROGRAM_OGL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_PROGRAM_OGL_HPP_
#    error Do not include program_OGL.hpp directly; Include program.hpp instead
#endif

#include <unordered_map>
#include <vector>
#include <GL/glew.h>

namespace GB_GRAPHICS_NAMESPACE {

    class gbShader_OGL;
    class gbShadowState_OGL;
    class gbTexture2D_OGL;

    class gbProgram_OGL: public gbProgram {
    private:
        class Shader_T {
        private:
            GLuint m_ProgramId;
            gbShaderPtr m_Storage;
            gbShader_OGL* m_Shader;
        public:
            Shader_T();
            Shader_T(gbShaderPtr& shader, GLuint program_id);
            Shader_T(gbShaderPtr&& shader, GLuint program_id);
            ~Shader_T();
            Shader_T(Shader_T&& rhs);
            Shader_T& operator=(Shader_T&& rhs);
            bool Equals(gbShaderPtr const& ptr) const;
        private:
            Shader_T(Shader_T const&);
            Shader_T& operator=(Shader_T const&);
        };
        struct Texture2D_T {
            gbTexture2DPtr Storage;
            gbTexture2D_OGL* Texture;
            GLuint TextureUnit;
            int uniform_index;
        };
    private:
        GLuint m_ProgramId;
        std::vector<Shader_T> m_Shaders;
        std::vector<VertexAttributeInfo> m_VertexAttrInfo;
        std::unordered_map<std::string, int> m_VertexAttrByName;
        std::vector<UniformInfo> m_UniformInfo;
        std::unordered_map<std::string, int> m_UniformByName;
        bool m_IsFinalized;
        std::vector<Texture2D_T> m_Textures2D;
        gbShadowState_OGL* m_glShadowState;
    public:
        gbProgram_OGL(GLuint program_id, gbShadowState_OGL* shadow_state);
        ~gbProgram_OGL();
        /** @name gbProgram
         * @{
         */
        void BindShader(gbShaderPtr& shader);
        void BindShader(gbShaderPtr&& shader);
        void ReleaseShader(gbShaderPtr const& shader);
        void ReleaseAllShaders();
        void Finalize();
        bool IsFinalized() const;
        void Activate() const;
        int NumberOfVertexAttributes() const;
        VertexAttributeInfo const& GetVertexAttributeInfo(int i) const;
        int GetVertexAttributeIndex(std::string const& attribute_name) const;
        int NumberOfUniforms() const;
        UniformInfo const& GetUniformInfo(int i) const;
        int GetUniformIndex(std::string const& uniform_name) const;
        void BindUniform(int uniform_index, UniformData::Float const& data);
        void BindUniform(int uniform_index, UniformData::Float2 const& data);
        void BindUniform(int uniform_index, UniformData::Float3 const& data);
        void BindUniform(int uniform_index, UniformData::Float4 const& data);
        void BindUniform(int uniform_index, UniformData::Int const& data);
        void BindUniform(int uniform_index, UniformData::Int2 const& data);
        void BindUniform(int uniform_index, UniformData::Int3 const& data);
        void BindUniform(int uniform_index, UniformData::Int4 const& data);
        void BindUniform(int uniform_index, UniformData::Uint const& data);
        void BindUniform(int uniform_index, UniformData::Uint2 const& data);
        void BindUniform(int uniform_index, UniformData::Uint3 const& data);
        void BindUniform(int uniform_index, UniformData::Uint4 const& data);
        void BindUniform(int uniform_index, UniformData::Bool const& data);
        void BindUniform(int uniform_index, UniformData::Bool2 const& data);
        void BindUniform(int uniform_index, UniformData::Bool3 const& data);
        void BindUniform(int uniform_index, UniformData::Bool4 const& data);
        void BindUniform(int uniform_index, UniformData::Mat2f const& data);
        void BindUniform(int uniform_index, UniformData::Mat3f const& data);
        void BindUniform(int uniform_index, UniformData::Mat4f const& data);
        void BindUniform(int uniform_index, gbTexture2DPtr& texture);
        /// @}
        GLuint GetOGLProgramId_OGL() const;
    private:
        void PerformShaderReflection();
    };

}

#endif

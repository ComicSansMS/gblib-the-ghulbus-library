/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_OP_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_OP_HPP_

#include <gbGraphics/config.hpp>

#include <gbGraphics/vertex_data.hpp>
#include <gbGraphics/index_data.hpp>
#include <gbGraphics/rasterizer_state.hpp>

#include <gbMath/vector3.hpp>

#include <type_traits>
#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

    namespace VertexDataOperations {

        template<typename gbVertexFormat_T, typename IndexType_T>
        void GeneratePerVertexNormals(gbVertexData<gbVertexFormat_T>& vertex_data, gbIndexData_Triangle<IndexType_T> const& index_data,
                                      Rasterizer::FaceOrientation_T vertex_order=Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE)
        {
            using namespace GB_MATH_NAMESPACE;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Normal, gbVertexFormat_T>::type NormalDesc_T;
            static_assert(!std::is_same<NormalDesc_T, VertexComponent::NilDesc>::value, "No normal type in vertex format");
            typedef typename NormalDesc_T::Layout::value_type NormalVecValue_T;
            typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Position, gbVertexFormat_T>::type PositionDesc_T;
            static_assert(!std::is_same<PositionDesc_T, VertexComponent::NilDesc>::value, "No position type in vertex format");
            typedef typename PositionDesc_T::Layout::value_type PositionVecValue_T;

            //calculate per-face normals
            std::vector<Vector3<PositionVecValue_T>> face_normals;
            int const n_primitives = index_data.NumberOfPrimitives();
            face_normals.reserve(n_primitives);
            for(int i=0; i<n_primitives; ++i) {
                auto const& vtx1 = VertexData::Get<PositionDesc_T>(vertex_data, index_data[i].i1);
                auto const& vtx2 = VertexData::Get<PositionDesc_T>(vertex_data, index_data[i].i2);
                auto const& vtx3 = VertexData::Get<PositionDesc_T>(vertex_data, index_data[i].i3);
                Vector3<PositionVecValue_T> v1(vtx1.x, vtx1.y, vtx1.z);
                Vector3<PositionVecValue_T> v2(vtx2.x, vtx2.y, vtx2.z);
                Vector3<PositionVecValue_T> v3(vtx3.x, vtx3.y, vtx3.z);
                Vector3<PositionVecValue_T> normal(Cross(v1-v2, v2-v3));
                if(vertex_order == Rasterizer::FACE_FRONT_COUNTER_CLOCKWISE) {
                    normal *= static_cast<PositionVecValue_T>(-1);
                }
                face_normals.push_back(normal.GetNormalized());
            }

            std::vector<Vector3<NormalVecValue_T>> vertex_normals;
            vertex_normals.resize(vertex_data.NumberOfVertices(), Vector3<NormalVecValue_T>());
            //accumulate per-face normals
            for(int i=0; i<n_primitives; ++i) {
                vertex_normals[index_data[i].i1] += Vector3<NormalVecValue_T>(face_normals[i]);
                vertex_normals[index_data[i].i2] += Vector3<NormalVecValue_T>(face_normals[i]);
                vertex_normals[index_data[i].i3] += Vector3<NormalVecValue_T>(face_normals[i]);
            }

            //write per-vertex normals
            int const n_vertices = vertex_data.NumberOfVertices();
            for(int i=0; i<n_vertices; ++i) {
                VertexComponent::FillVector(VertexData::Get<NormalDesc_T>(vertex_data, i), vertex_normals[i].GetNormalized(), true);
            }
        }

    }

}

#endif

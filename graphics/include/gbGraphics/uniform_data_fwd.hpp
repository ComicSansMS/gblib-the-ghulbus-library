/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_UNIFORM_DATA_FWD_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_UNIFORM_DATA_FWD_HPP_

#include <gbGraphics/config.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    namespace UniformData {
        enum UniformType_T {
            UNIFORM_NO_TYPE,
            UNIFORM_FLOAT,
            UNIFORM_FLOAT2,
            UNIFORM_FLOAT3,
            UNIFORM_FLOAT4,
            UNIFORM_INT,
            UNIFORM_INT2,
            UNIFORM_INT3,
            UNIFORM_INT4,
            UNIFORM_UINT,
            UNIFORM_UINT2,
            UNIFORM_UINT3,
            UNIFORM_UINT4,
            UNIFORM_BOOL,
            UNIFORM_BOOL2,
            UNIFORM_BOOL3,
            UNIFORM_BOOL4,
            UNIFORM_FLOAT_MAT2,
            UNIFORM_FLOAT_MAT3,
            UNIFORM_FLOAT_MAT4,
            UNIFORM_TEXTURE_1D,
            UNIFORM_TEXTURE_2D,
            UNIFORM_TEXTURE_3D,
            UNIFORM_TEXTURE_CUBEMAP,
            UNIFORM_TEXTURE_1D_ARRAY,
            UNIFORM_TEXTURE_2D_ARRAY /*,
            UNIFORM_FLOAT_MAT2x3,
            UNIFORM_FLOAT_MAT2x4,
            UNIFORM_FLOAT_MAT3x2,
            UNIFORM_FLOAT_MAT3x4,
            UNIFORM_FLOAT_MAT4x2,
            UNIFORM_FLOAT_MAT4x3,
            UNIFORM_SAMPLER_1D_SHADOW,
            UNIFORM_SAMPLER_2D_SHADOW,
            UNIFORM_SAMPLER_1D_ARRAY_SHADOW,
            UNIFORM_SAMPLER_2D_ARRAY_SHADOW,
            UNIFORM_SAMPLER_2D_MULTISAMPLE,
            UNIFORM_SAMPLER_2D_MULTISAMPLE_ARRAY,
            UNIFORM_SAMPLER_CUBE_SHADOW,
            UNIFORM_SAMPLER_BUFFER,
            UNIFORM_SAMPLER_2D_RECT,
            UNIFORM_SAMPLER_2D_RECT_SHADOW,
            UNIFORM_INT_SAMPLER_1D,
            UNIFORM_INT_SAMPLER_2D,
            UNIFORM_INT_SAMPLER_3D,
            UNIFORM_INT_SAMPLER_CUBE,
            UNIFORM_INT_SAMPLER_1D_ARRAY,
            UNIFORM_INT_SAMPLER_2D_ARRAY,
            UNIFORM_INT_SAMPLER_2D_MULTISAMPLE,
            UNIFORM_INT_SAMPLER_2D_MULTISAMPLE_ARRAY,
            UNIFORM_INT_SAMPLER_BUFFER,
            UNIFORM_INT_SAMPLER_2D_RECT,
            UNIFORM_UINT_SAMPLER_1D,
            UNIFORM_UINT_SAMPLER_2D,
            UNIFORM_UINT_SAMPLER_3D,
            UNIFORM_UINT_SAMPLER_CUBE,
            UNIFORM_UINT_SAMPLER_1D_ARRAY,
            UNIFORM_UINT_SAMPLER_2D_ARRAY,
            UNIFORM_UINT_SAMPLER_2D_MULTISAMPLE,
            UNIFORM_UINT_SAMPLER_2D_MULTISAMPLE_ARRAY,
            UNIFORM_UINT_SAMPLER_BUFFER,
            UNIFORM_UINT_SAMPLER_2D_RECT
            */
        };

        struct Float;
        struct Float2;
        struct Float3;
        struct Float4;
        struct Int;
        struct Int2;
        struct Int3;
        struct Int4;
        struct Uint;
        struct Uint2;
        struct Uint3;
        struct Uint4;
        struct Bool;
        struct Bool2;
        struct Bool3;
        struct Bool4;
        struct Mat2f;
        struct Mat3f;
        struct Mat4f;
    }
}

#endif

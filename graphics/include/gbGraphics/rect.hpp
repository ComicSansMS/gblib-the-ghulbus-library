/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_RECT_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_RECT_HPP_

#include <gbGraphics/config.hpp>
#include <gbMath/vector2.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class Rect {
    public:
        int left_;
        int top_;
        int width_;
        int height_;
    public:
        Rect()
            :left_(0), top_(0), width_(0), height_(0)
        {
        }
        Rect(GB_MATH_NAMESPACE::Vector2i const& top_left_corner, int width, int height)
            :left_(top_left_corner.x_), top_(top_left_corner.y_),
             width_(width), height_(height)
        {
        }
        Rect(GB_MATH_NAMESPACE::Vector2i const& top_left_corner, GB_MATH_NAMESPACE::Vector2i const& lower_right_corner)
            :left_(top_left_corner.x_), top_(top_left_corner.y_),
             width_(lower_right_corner.x_ - top_left_corner.x_),
             height_(lower_right_corner.y_ - top_left_corner.y_)
        {
        }
        Rect(int left, int top, int width, int height)
            :left_(left), top_(top), width_(width), height_(height)
        {
        }
    };

}

#endif

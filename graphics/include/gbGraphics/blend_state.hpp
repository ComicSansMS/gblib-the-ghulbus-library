/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_BLEND_STATE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_BLEND_STATE_HPP_

#include <gbGraphics/config.hpp>

#include <boost/utility.hpp>

#include <memory>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbBlendState;
    typedef std::shared_ptr<gbBlendState> gbBlendStatePtr;
    /// @}

    namespace Blending {
        enum Enable_T {
            ENABLED,
            DISABLED
        };
        enum Operation_T {
            OPERATION_ADD
        };
        enum Operand_T {
            OPERAND_ONE,
            OPERAND_ZERO,
            OPERAND_SRC_COLOR,
            OPERAND_SRC_ALPHA,
            OPERAND_INV_SRC_ALPHA,
            OPERAND_DST_COLOR
        };

        struct StateDesc {
            Enable_T Enable;
            Operand_T Source;
            Operand_T Destination;
            Operation_T Operation;
            Operand_T AlphaSource;
            Operand_T AlphaDestination;
            Operation_T AlphaOperation;
            StateDesc()
                :Enable(ENABLED),
                 Source(OPERAND_ONE), Destination(OPERAND_ZERO), Operation(OPERATION_ADD),
                 AlphaSource(OPERAND_ONE), AlphaDestination(OPERAND_ZERO), AlphaOperation(OPERATION_ADD)
            {}
            StateDesc(Operand_T src, Operand_T dst, Operation_T op)
                :Enable(ENABLED),
                 Source(src), Destination(dst), Operation(op),
                 AlphaSource(src), AlphaDestination(dst), AlphaOperation(op)
            {}
            StateDesc( Operand_T src, Operand_T dst, Operation_T op,
                                  Operand_T alpha_src, Operand_T alpha_dst, Operation_T alpha_op )
                :Enable(ENABLED),
                 Source(src), Destination(dst), Operation(op),
                 AlphaSource(alpha_src), AlphaDestination(alpha_dst), AlphaOperation(alpha_op)
            {}
        };
    }

    class gbBlendState: public boost::noncopyable {
    public:
        virtual ~gbBlendState() {};
        virtual Blending::StateDesc const& GetDescriptor() const=0;
    };

    inline bool operator==(Blending::StateDesc const& lhs, Blending::StateDesc const& rhs)
    {
        return (lhs.Enable           == rhs.Enable)           &&
               (lhs.Source           == rhs.Source)           &&
               (lhs.Destination      == rhs.Destination)      &&
               (lhs.Operation        == rhs.Operation)        &&
               (lhs.AlphaSource      == rhs.AlphaSource)      &&
               (lhs.AlphaDestination == rhs.AlphaDestination) &&
               (lhs.AlphaOperation   == rhs.AlphaOperation);
    }

    inline bool operator!=(Blending::StateDesc const& lhs, Blending::StateDesc const& rhs)
    {
        return !(lhs == rhs);
    }
}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_BLEND_STATE_OGL_HPP_
#    include <gbGraphics/blend_state_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_BLEND_STATE_OGL_HPP_
#endif

#endif

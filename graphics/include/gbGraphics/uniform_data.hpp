/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_UNIFORM_DATA_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_UNIFORM_DATA_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>
#include <gbGraphics/uniform_data_fwd.hpp>

#include <gbMath/vector.hpp>
#include <gbMath/matrix.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    namespace UniformData {
        struct Float {
            static const UniformType_T Type = UNIFORM_FLOAT;
            float f_;
            Float(): f_(0.0f) {}
            Float(float f): f_(f) {}
        };

        struct Float2 {
            static const UniformType_T Type = UNIFORM_FLOAT2;
            float f1_, f2_;
            Float2(): f1_(0.0f), f2_(0.0f) {}
            Float2(float f1, float f2)
                : f1_(f1), f2_(f2) {}
            Float2(float const* fv): f1_(fv[0]), f2_(fv[1]) {}
            Float2(GB_MATH_NAMESPACE::Vector2f const& v2)
                : f1_(v2.x_), f2_(v2.y_) {}
        };

        struct Float3 {
            static const UniformType_T Type = UNIFORM_FLOAT3;
            float f1_, f2_, f3_;
            Float3(): f1_(0.0f), f2_(0.0f), f3_(0.0f) {}
            Float3(float f1, float f2, float f3)
                : f1_(f1), f2_(f2), f3_(f3) {}
            Float3(float const* fv): f1_(fv[0]), f2_(fv[1]), f3_(fv[2]) {}
            Float3(GB_MATH_NAMESPACE::Vector3f const& v3)
                : f1_(v3.x_), f2_(v3.y_), f3_(v3.z_) {}
            Float3(GBCOLOR c)
                : f1_(static_cast<float>(gbColor::GetR(c)) / 255.0f),
                  f2_(static_cast<float>(gbColor::GetG(c)) / 255.0f),
                  f3_(static_cast<float>(gbColor::GetB(c)) / 255.0f)
            { }
        };

        struct Float4 {
            static const UniformType_T Type = UNIFORM_FLOAT4;
            float f1_, f2_, f3_, f4_;
            Float4(): f1_(0.0f), f2_(0.0f), f3_(0.0f), f4_(0.0f) {}
            Float4(float f1, float f2, float f3, float f4)
                : f1_(f1), f2_(f2), f3_(f3), f4_(f4)  {}
            Float4(float const* fv): f1_(fv[0]), f2_(fv[1]), f3_(fv[2]), f4_(fv[3]) {}
            Float4(GB_MATH_NAMESPACE::Vector4f const& v4)
                : f1_(v4.x_), f2_(v4.y_), f3_(v4.z_), f4_(v4.w_) {}
            Float4(GBCOLOR c)
                : f1_(static_cast<float>(gbColor::GetR(c)) / 255.0f),
                  f2_(static_cast<float>(gbColor::GetG(c)) / 255.0f),
                  f3_(static_cast<float>(gbColor::GetB(c)) / 255.0f),
                  f4_(static_cast<float>(gbColor::GetA(c)) / 255.0f)
            { }
        };

        struct Int {
            static const UniformType_T Type = UNIFORM_INT;
            int i_;
            Int(): i_(0) {}
            Int(int i): i_(i) {}
        };

        struct Int2 {
            static const UniformType_T Type = UNIFORM_INT2;
            int i1_, i2_;
            Int2(): i1_(0), i2_(0) {}
            Int2(int i1, int i2)
                : i1_(i1), i2_(i2) {}
            Int2(int const* iv): i1_(iv[0]), i2_(iv[1]) {}
            Int2(GB_MATH_NAMESPACE::Vector2i const& v2)
                : i1_(v2.x_), i2_(v2.y_) {}
        };

        struct Int3 {
            static const UniformType_T Type = UNIFORM_INT3;
            int i1_, i2_, i3_;
            Int3(): i1_(0), i2_(0), i3_(0) {}
            Int3(int i1, int i2, int i3)
                : i1_(i1), i2_(i2), i3_(i3) {}
            Int3(int const* iv): i1_(iv[0]), i2_(iv[1]), i3_(iv[2]) {}
            Int3(GB_MATH_NAMESPACE::Vector3i const& v3)
                : i1_(v3.x_), i2_(v3.y_), i3_(v3.z_) {}
        };

        struct Int4 {
            static const UniformType_T Type = UNIFORM_INT4;
            int i1_, i2_, i3_, i4_;
            Int4(): i1_(0), i2_(0), i3_(0), i4_(0) {}
            Int4(int i1, int i2, int i3, int i4)
                : i1_(i1), i2_(i2), i3_(i3), i4_(i4)  {}
            Int4(int const* iv): i1_(iv[0]), i2_(iv[1]), i3_(iv[2]), i4_(iv[3]) {}
            Int4(GB_MATH_NAMESPACE::Vector4i const& v4)
                : i1_(v4.x_), i2_(v4.y_), i3_(v4.z_), i4_(v4.w_) {}
        };

        struct Uint {
            static const UniformType_T Type = UNIFORM_UINT;
            unsigned int i_;
            Uint(): i_(0) {}
            Uint(unsigned int i): i_(i) {}
        };

        struct Uint2 {
            static const UniformType_T Type = UNIFORM_UINT2;
            unsigned int i1_, i2_;
            Uint2(): i1_(0), i2_(0) {}
            Uint2(unsigned int i1, unsigned int i2)
                : i1_(i1), i2_(i2) {}
            Uint2(unsigned int const* iv): i1_(iv[0]), i2_(iv[1]) {}
            Uint2(GB_MATH_NAMESPACE::Vector2ui const& v2)
                : i1_(v2.x_), i2_(v2.y_) {}
        };

        struct Uint3 {
            static const UniformType_T Type = UNIFORM_UINT3;
            unsigned int i1_, i2_, i3_;
            Uint3(): i1_(0), i2_(0), i3_(0) {}
            Uint3(unsigned int i1, unsigned int i2, unsigned int i3)
                : i1_(i1), i2_(i2), i3_(i3) {}
            Uint3(unsigned int const* iv): i1_(iv[0]), i2_(iv[1]), i3_(iv[2]) {}
            Uint3(GB_MATH_NAMESPACE::Vector3ui const& v3)
                : i1_(v3.x_), i2_(v3.y_), i3_(v3.z_) {}
        };

        struct Uint4 {
            static const UniformType_T Type = UNIFORM_UINT4;
            unsigned int i1_, i2_, i3_, i4_;
            Uint4(): i1_(0), i2_(0), i3_(0), i4_(0) {}
            Uint4(unsigned int i1, unsigned int i2, unsigned int i3, unsigned int i4)
                : i1_(i1), i2_(i2), i3_(i3), i4_(i4)  {}
            Uint4(unsigned int const* iv): i1_(iv[0]), i2_(iv[1]), i3_(iv[2]), i4_(iv[3]) {}
            Uint4(GB_MATH_NAMESPACE::Vector4ui const& v4)
                : i1_(v4.x_), i2_(v4.y_), i3_(v4.z_), i4_(v4.w_) {}
        };

        struct Bool {
            static const UniformType_T Type = UNIFORM_BOOL;
            bool b_;
            Bool(): b_(0) {}
            Bool(bool b): b_(b) {}
        };

        struct Bool2 {
            static const UniformType_T Type = UNIFORM_BOOL2;
            bool b1_, b2_;
            Bool2(): b1_(0), b2_(0) {}
            Bool2(bool b1, bool b2)
                : b1_(b1), b2_(b2) {}
            Bool2(bool const* bv): b1_(bv[0]), b2_(bv[1]) {}
            Bool2(GB_MATH_NAMESPACE::Vector2<bool> const& v2)
                : b1_(v2.x_), b2_(v2.y_) {}
        };

        struct Bool3 {
            static const UniformType_T Type = UNIFORM_BOOL3;
            bool b1_, b2_, b3_;
            Bool3(): b1_(0), b2_(0), b3_(0) {}
            Bool3(bool b1, bool b2, bool b3)
                : b1_(b1), b2_(b2), b3_(b3) {}
            Bool3(bool const* bv): b1_(bv[0]), b2_(bv[1]), b3_(bv[2]) {}
            Bool3(GB_MATH_NAMESPACE::Vector3<bool> const& v3)
                : b1_(v3.x_), b2_(v3.y_), b3_(v3.z_) {}
        };

        struct Bool4 {
            static const UniformType_T Type = UNIFORM_BOOL4;
            bool b1_, b2_, b3_, b4_;
            Bool4(): b1_(0), b2_(0), b3_(0), b4_(0) {}
            Bool4(bool b1, bool b2, bool b3, bool b4)
                : b1_(b1), b2_(b2), b3_(b3), b4_(b4)  {}
            Bool4(bool const* bv): b1_(bv[0]), b2_(bv[1]), b3_(bv[2]), b4_(bv[3]) {}
            Bool4(GB_MATH_NAMESPACE::Vector4<bool> const& v4)
                : b1_(v4.x_), b2_(v4.y_), b3_(v4.z_), b4_(v4.w_) {}
        };

        struct Mat2f {
            static const UniformType_T Type = UNIFORM_FLOAT_MAT2;
            float fv_[4];
            Mat2f() { for(int i=0; i<4; ++i) { fv_[i] = 0.0f; } }
            Mat2f(float f11, float f12, float f21, float f22) {
                fv_[0] = f11; fv_[1] = f12;
                fv_[2] = f21; fv_[2] = f22;
            }
            Mat2f(float const* fv) { for(int i=0; i<4; ++i) { fv_[i] = fv[i]; } }
            Mat2f(GB_MATH_NAMESPACE::Matrix2f const& mat) {
                using namespace GB_MATH_NAMESPACE;
                fv_[0] = Get<1,1>(mat); fv_[1] = Get<1,2>(mat);
                fv_[2] = Get<2,1>(mat); fv_[2] = Get<2,2>(mat);
            }
        };

        struct Mat3f {
            static const UniformType_T Type = UNIFORM_FLOAT_MAT3;
            float fv_[9];
            Mat3f() { for(int i=0; i<9; ++i) { fv_[i] = 0.0f; } }
            Mat3f(float f11, float f12, float f13, float f21, float f22, float f23, float f31, float f32, float f33) {
                fv_[0] = f11; fv_[1] = f12; fv_[2] = f13;
                fv_[3] = f21; fv_[4] = f22; fv_[5] = f23;
                fv_[6] = f31; fv_[7] = f32; fv_[8] = f33;
            }
            Mat3f(float const* fv) { for(int i=0; i<9; ++i) { fv_[i] = fv[i]; } }
            Mat3f(GB_MATH_NAMESPACE::Matrix3f const& mat) {
                using namespace GB_MATH_NAMESPACE;
                fv_[0] = Get<1,1>(mat); fv_[1] = Get<1,2>(mat); fv_[2] = Get<1,3>(mat);
                fv_[3] = Get<2,1>(mat); fv_[4] = Get<2,2>(mat); fv_[5] = Get<2,3>(mat);
                fv_[6] = Get<3,1>(mat); fv_[7] = Get<3,2>(mat); fv_[8] = Get<3,3>(mat);
            }
        };

        struct Mat4f {
            static const UniformType_T Type = UNIFORM_FLOAT_MAT4;
            float fv_[16];
            Mat4f() { for(int i=0; i<16; ++i) { fv_[i] = 0.0f; } }
            Mat4f( float f11, float f12, float f13, float f14,
                   float f21, float f22, float f23, float f24,
                   float f31, float f32, float f33, float f34,
                   float f41, float f42, float f43, float f44 )
            {
                fv_[ 0] = f11; fv_[ 1] = f12; fv_[ 2] = f13; fv_[ 3] = f14;
                fv_[ 4] = f21; fv_[ 5] = f22; fv_[ 6] = f23; fv_[ 7] = f24;
                fv_[ 8] = f31; fv_[ 9] = f32; fv_[10] = f33; fv_[11] = f34;
                fv_[12] = f41; fv_[13] = f42; fv_[14] = f43; fv_[15] = f44;
            }
            Mat4f(float const* fv) { for(int i=0; i<16; ++i) { fv_[i] = fv[i]; } }
            Mat4f(GB_MATH_NAMESPACE::Matrix4f const& mat) {
                using namespace GB_MATH_NAMESPACE;
                fv_[ 0] = Get<1,1>(mat); fv_[ 1] = Get<1,2>(mat); fv_[ 2] = Get<1,3>(mat); fv_[ 3] = Get<1,4>(mat);
                fv_[ 4] = Get<2,1>(mat); fv_[ 5] = Get<2,2>(mat); fv_[ 6] = Get<2,3>(mat); fv_[ 7] = Get<2,4>(mat);
                fv_[ 8] = Get<3,1>(mat); fv_[ 9] = Get<3,2>(mat); fv_[10] = Get<3,3>(mat); fv_[11] = Get<3,4>(mat);
                fv_[12] = Get<4,1>(mat); fv_[13] = Get<4,2>(mat); fv_[14] = Get<4,3>(mat); fv_[15] = Get<4,4>(mat);
            }
        };
    }
}

#endif

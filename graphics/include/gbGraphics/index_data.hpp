/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_INDEX_DATA_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_INDEX_DATA_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/index_data_base.hpp>

#include <cstdint>
#include <cstring>
#include <type_traits>
#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

    /** Index Format types
     */
    namespace IndexFormat {

        /** Storage type for gbIndexData_Triangle
         * @tparam T Index data type
         */
        template<typename T>
        struct Triangle {
            T i1;
            T i2;
            T i3;
        };

    }

    /** Index data for Triangle lists
     * @tparam T Index data type; Must be one of: uint8_t, uint16_t, uint32_t
     */
    template<typename T>
    class gbIndexData_Triangle: public gbIndexDataBase {
        static_assert( std::is_same<T, uint8_t>::value || std::is_same<T, uint16_t>::value || std::is_same<T, uint32_t>::value,
                       "gbIndexData can only be initialized with uint8_t, uint16_t or uint32_t" );
    public:
        typedef IndexFormat::Triangle<T> IndexType;
    private:
        std::vector<IndexType> m_Data;            ///< Storage
    public:
        /** Set the size of the index data
         * @param[in] n_primitives Number of triangles to fit in the vertex data
         * @note Number of available indices will be n_primitives*3
         */
        void Resize(int n_primitives) {
            m_Data.resize(n_primitives);
        }
        /** Append one element to the end of the data
         * @param[in] triangle Single triangle index data element
         */
        void PushBack(IndexType const& triangle) {
            m_Data.push_back(triangle);
        }
        /** Array access
         * @param[in] idx Primitive index [0..NumberOfPrimitives)
         * @return Reference to the requested primitive's indices
         */
        IndexFormat::Triangle<T>& operator[](size_t index) {
            return m_Data[index];
        }
        /** Array access
         * @param[in] idx Primitive index [0..NumberOfPrimitives)
         * @return Reference to the requested primitive's indices
         */
        IndexFormat::Triangle<T> const& operator[](size_t index) const {
            return m_Data[index];
        }
        /** @name gbIndexDataBase
         * @{
         */
        int NumberOfPrimitives() const {
            return static_cast<int>(m_Data.size());
        }
        int NumberOfIndices() const {
            return static_cast<int>(m_Data.size() * 3);
        }
        int GetRawSize() const {
            return static_cast<int>(m_Data.size()*sizeof(IndexFormat::Triangle<T>));
        }
        void const* GetRawData() const {
            return m_Data.data();
        }
        Primitive_T GetPrimitiveType() const {
            return PRIMITIVE_TRIANGLE;
        }
        IndexType_T GetIndexType() const {
            return (std::is_same<T, uint8_t>::value) ? (INDEX_TYPE_UINT8) : (
                    (std::is_same<T, uint16_t>::value) ? (INDEX_TYPE_UINT16) : (
                     INDEX_TYPE_UINT32 ) );
        }
        /// @}
    };


    namespace IndexData {

        template<typename T>
        void Fill(gbIndexData_Triangle<T>& index_data, T* src_data) {
            std::memcpy(&(index_data[0]), src_data, index_data.GetRawSize());
        }

        template<size_t I, typename T>
        typename std::enable_if<I==0, T>::type&
        GetStorageElement(IndexFormat::Triangle<T>& triangle)
        {
            return triangle.i1;
        }

        template<size_t I, typename T>
        typename std::enable_if<I==1, T>::type&
        GetStorageElement(IndexFormat::Triangle<T>& triangle)
        {
            return triangle.i2;
        }

        template<size_t I, typename T>
        typename std::enable_if<I==2, T>::type&
        GetStorageElement(IndexFormat::Triangle<T>& triangle)
        {
            return triangle.i3;
        }

        template<size_t I, typename T>
        typename std::enable_if<(I>2), T>::type&
        GetStorageElement(IndexFormat::Triangle<T>& triangle)
        {
            static_assert(sizeof(T)==0, "Invalid triangle index component requested");
        }

        template<size_t I, typename T>
        T& Get(gbIndexData_Triangle<T>& index_data, size_t index)
        {
            return GetStorageElement<I>(index_data[index]);
        }

    }

}

#endif

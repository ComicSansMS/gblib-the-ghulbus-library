/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXT_STREAM_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXT_STREAM_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/text_renderer.hpp>
#include <gbGraphics/text_streambuffer.hpp>

#include <gbMath/vector2.hpp>

#include <ostream>

namespace GB_GRAPHICS_NAMESPACE {
    class gbFont;
    class LockableSurface;

    namespace Text {
        template<typename T>
        class BasicTextStream: public std::basic_ostream<T, std::char_traits<T> > {
        private:
            BasicStreambuffer<T> m_Streambuf;
        public:
            BasicTextStream(TextRendererPtr&& renderer)
                :std::basic_ostream<T, std::char_traits<T> >(&m_Streambuf), m_Streambuf(std::move(renderer))
            {
            }
            BasicTextStream(BasicTextStream<T>&& rhs)
                :std::basic_ostream<T, std::char_traits<T>>(&m_Streambuf), m_Streambuf(std::move(rhs.m_Streambuf))
            {
            } 
            void SetFont(gbFont* font)
            {
                m_Streambuf.SetFont(font);
            }
            void SetTextColor(GBCOLOR c)
            {
                m_Streambuf.GetRenderer().SetTextColor(c);
            }
            void SetCursorPosition(int x, int y)
            {
                m_Streambuf.GetRenderer().SetCursorPosition(x, y);
            }
            void SetTabWidthPixels(int px_width)
            {
                m_Streambuf.GetRenderer().SetTabWidthPixels(px_width);
            }
            void SetNewLineIndentation(int px_indent)
            {
                m_Streambuf.GetRenderer().SetNewLineIndentation(px_indent);
            }
            void Clear()
            {
                m_Streambuf.GetRenderer().Clear();
            }
            void Clear(GBCOLOR c)
            {
                m_Streambuf.GetRenderer().Clear(c);
            }
            GB_MATH_NAMESPACE::Vector2i GetCursorPosition() const
            {
                return m_Streambuf.GetRenderer().GetCursorPosition();
            }
        };
    }

    typedef Text::BasicTextStream<char> gbText;
    typedef Text::BasicTextStream<wchar_t> gbTextW;
}

#endif

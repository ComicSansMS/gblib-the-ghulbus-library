/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_DRAWING_CONTEXT_3D_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_DRAWING_CONTEXT_3D_OGL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_DRAWING_CONTEXT_3D_OGL_HPP_
#    error Do not include drawing_context_3d_OGL.hpp directly; Include drawing_context_3d.hpp instead
#endif

#include <gbGraphics/event_manager.hpp>
#include <gbUtil/resource_observer.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbOpenGLContext;
    class gbShader_OGL;
    class gbProgram_OGL;
    class gbVertexBuffer_OGL;
    class gbIndexBuffer_OGL;
    class gbTexture2D_OGL;
    class gbShadowState_OGL;
    /// @}

    /** @defgroup gb3d_ogl OpenGL
     */
    /** @ingroup gb3d_ogl
     */
    class gbDrawingContext3D_OGL: public gbDrawingContext3D {
    private:
        std::unique_ptr<gbOpenGLContext> m_OpenGLContext;
        gbWindow* m_ParentWindow;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbShader_OGL> m_Shaders;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbProgram_OGL> m_Programs;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbVertexBuffer_OGL> m_VertexBuffers;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbIndexBuffer_OGL> m_IndexBuffers;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbTexture2D_OGL> m_Textures2D;
        std::unique_ptr<gbShadowState_OGL> m_glShadowState;
        GBCOLOR m_ClearColor;
        gbRasterizerStatePtr m_RasterizerState;
        gbBlendStatePtr m_BlendState;
        gbSamplerStatePtr m_DefaultSamplerState;
        gbEventManager::HandlerId m_WindowEventHandlerId;
    public:
        /** Constructor
         * @param[in] gl_context OpenGL drawing context
         * @param[in] parent_window Associated parent window
         */
        gbDrawingContext3D_OGL(std::unique_ptr<gbOpenGLContext>&& gl_context, gbWindow* parent_window);
        /** Destructor
         */
        ~gbDrawingContext3D_OGL();
        /** @name gbDrawingContext_Base
         * @{
         */
        gbWindow* GetParentWindow() const;
        void SetClearColor(GBCOLOR c);
        GBCOLOR GetClearColor() const;
        void Clear();
        void Present();
        /// @}
        /** @name gbDrawingContext3D
         * @{
         */
        RendererInfo GetRendererInfo();
        gbRasterizerStatePtr CreateRasterizerState(Rasterizer::StateDesc const& state_desc);
        void BindRasterizerState(gbRasterizerStatePtr rasterizer_state);
        gbRasterizerStatePtr GetCurrentRasterizerState() const;
        gbBlendStatePtr CreateBlendState(Blending::StateDesc const& state_desc);
        void BindBlendState(gbBlendStatePtr blend_state);
        gbBlendStatePtr GetCurrentBlendState() const;
        gbSamplerStatePtr CreateSamplerState(Sampling::StateDesc const& state_desc);
        gbSamplerStatePtr GetDefaultSamplerState() const;
        void SetDepthTest(bool enable_depth_test);
        void SetClearDepthValue(float f);
        gbShaderPtr CreateShader(ShaderType::ShaderType_T type, std::string const& code);
        gbProgramPtr CreateProgram();
        gbProgramPtr CreateProgramFromFile(std::string const& filename);
        gbVertexBufferPtr CreateVertexBuffer(int size);
        gbIndexBufferPtr CreateIndexBuffer(int size);
        gbTexture2DPtr CreateTexture(Texture::TextureDesc2D const& desc2d);
        gbTexture2DPtr CreateTexture(Texture::TextureDesc2D const& desc2d, gbSamplerStatePtr sampler_state);
        void InvalidateState();
        void Draw(gbGeometry const& geometry);
        /// @}
        /** Get a list of all OpenGL extensions for the current drawing context
         * @return A vector of strings, where each entry is the name of a supported OpenGL extension
         */
        std::vector<std::string> GetOGLExtensionList_OGL();
        /** Wraps glViewport().
         */
        void SetViewport_OGL(int x, int y, int width, int height);
    };

}

#endif

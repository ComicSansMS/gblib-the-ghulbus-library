/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_PROGRAM_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_PROGRAM_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/vertex_format_base.hpp>
#include <gbGraphics/shader_type.hpp>
#include <gbGraphics/uniform_data_fwd.hpp>

#include <memory>
#include <string>
#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbShader;
    typedef std::shared_ptr<gbShader> gbShaderPtr;

    class gbProgram;
    typedef std::shared_ptr<gbProgram> gbProgramPtr;

    class gbTexture2D;
    typedef std::shared_ptr<gbTexture2D> gbTexture2DPtr;

    class gbProgram: public boost::noncopyable {
    public:
        struct VertexAttributeInfo {
            std::string Name;
            gbVertexFormatBase::ComponentType_T Type;
            int LocationIndex;
        };
        struct UniformInfo {
            std::string Name;
            UniformData::UniformType_T Type;
            int LocationIndex;
        };
    public:
        virtual ~gbProgram() {}
        virtual void BindShader(gbShaderPtr& shader)=0;
        virtual void BindShader(gbShaderPtr&& shader)=0;
        virtual void ReleaseShader(gbShaderPtr const& shader)=0;
        virtual void ReleaseAllShaders()=0;
        virtual void Finalize()=0;
        virtual bool IsFinalized() const=0;
        virtual void Activate() const=0;
        virtual int NumberOfVertexAttributes() const=0;
        virtual VertexAttributeInfo const& GetVertexAttributeInfo(int i) const=0;
        virtual int GetVertexAttributeIndex(std::string const& attribute_name) const=0;
        virtual int NumberOfUniforms() const=0;
        virtual UniformInfo const& GetUniformInfo(int i) const=0;
        virtual int GetUniformIndex(std::string const& uniform_name) const=0;
        virtual void BindUniform(int uniform_index, UniformData::Float const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Float2 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Float3 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Float4 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Int const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Int2 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Int3 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Int4 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Uint const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Uint2 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Uint3 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Uint4 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Bool const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Bool2 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Bool3 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Bool4 const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Mat2f const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Mat3f const& data)=0;
        virtual void BindUniform(int uniform_index, UniformData::Mat4f const& data)=0;
        virtual void BindUniform(int uniform_index, gbTexture2DPtr& texture)=0;
        template<typename T>
        void BindUniform(std::string const& uniform_name, T const& data) { BindUniform( GetUniformIndex(uniform_name), data ); }
        void BindUniform(std::string const& uniform_name, gbTexture2DPtr& tex) { BindUniform( GetUniformIndex(uniform_name), tex ); }
    };

}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_PROGRAM_OGL_HPP_
#    include <gbGraphics/program_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_PROGRAM_OGL_HPP_
#endif

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_OPENGL_CONTEXT_SDL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_OPENGL_CONTEXT_SDL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_OPENGL_CONTEXT_SDL_HPP_
#    error Do not include opengl_context_SDL.hpp directly; Include opengl_context.hpp instead
#endif

#include <SDL.h>

namespace GB_GRAPHICS_NAMESPACE {

    class gbWindow_SDL;

    class gbOpenGLContext_SDL: public gbOpenGLContext {
        SDL_GLContext m_GLContext;
        gbWindow_SDL* m_ParentWindow;
    public:
        gbOpenGLContext_SDL(SDL_GLContext gl_context, gbWindow_SDL* parent_window);
        ~gbOpenGLContext_SDL();
        void EnableVsync(bool vsync_enable);
        void Swap();
    };

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXTURE2D_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXTURE2D_OGL_HPP_

#ifndef GB_GRAPHICS_INCLUDE_TOKEN_TEXTURE2D_OGL_HPP_
#    error Do not include texture2d_OGL.hpp directly; Include texture2d.hpp instead
#endif

#include <GL/glew.h>

namespace GB_GRAPHICS_NAMESPACE {

    class gbShadowState_OGL;

    class gbTexture2D_OGL: public gbTexture2D {
    private:
        GLuint const m_TextureId;
        int const m_Width;
        int const m_Height;
        GLenum const m_Target;
        GLenum const m_Format;
        GLenum const m_Type;
        gbSamplerStatePtr m_SamplerState;
        gbShadowState_OGL* const m_glShadowState;
        std::unique_ptr<GBCOLOR[]> m_LockBuffer;
        Rect m_LockedArea;
    public:
        gbTexture2D_OGL( GLuint texture_id, Texture::TextureDesc2D const& desc2d,
                         gbSamplerStatePtr sampler_state, gbShadowState_OGL* shadow_state );
        ~gbTexture2D_OGL();
        /** @name LockableSurface
         * @{
         */
        void Lock(GBCOLOR** data, int* pitch);
        void LockRect(Rect const& rect, GBCOLOR** data, int* pitch);
        void Unlock();
        bool IsLocked() const;
        int GetWidth() const;
        int GetHeight() const;
        /// @}
        /** @name gbTexture2D
         * @{
         */
        void LockWithOptions(GBCOLOR** data, int* pitch, Rect const& rect, LockOptions opt);
        void SetTextureData(ReadableSurface& image);
        void SetTextureSubData(Rect const& dest, ReadableSurface& image);
        void BindSamplerState(gbSamplerStatePtr sampler_state);
        gbSamplerStatePtr GetSamplerState() const;
        /// @}
        GLuint GetOGLTextureId_OGL() const;
        GLenum GetOGLTextureTarget_OGL() const;
    };

}

#endif

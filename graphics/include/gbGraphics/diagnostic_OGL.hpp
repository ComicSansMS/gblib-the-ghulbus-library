/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_DIAGNOSTIC_OGL_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_DIAGNOSTIC_OGL_HPP_

#include <gbGraphics/config.hpp>

#ifdef GB_GRAPHICS_HAS_OPENGL

#include <diagnostic/log.hpp>
#include <GL/glew.h>

namespace GB_GRAPHICS_NAMESPACE {

    char const* TranslateGLErrorCode_OGL(GLenum e);

    /** Helper for OpenGL error handling
     * Checks for floating errors on construction and destruction
     * and allows for manual error checking in betweeen.
     */
    class ErrorMonitor_OGL {
    private:
        GLenum error_;            ///< OpenGL error code
    public:
        /** Constructor
         * Checks for floating errors and logs them to WARNING
         */
        ErrorMonitor_OGL()
        {
            if(CheckError()) {
                DIAG_LOG(WARNING, "Floating OpenGL error detected: " << TranslateGLErrorCode_OGL(error_));
            }
        }
        /** Destructor
         * Checks for floating errors and logs them to WARNING
         */
        ~ErrorMonitor_OGL()
        {
            if(CheckError()) {
                DIAG_LOG(WARNING, "Floating OpenGL error detected: " << TranslateGLErrorCode_OGL(error_));
            }
        }
        /** Checks for new OpenGL errors by calling glGetError()
         * @return true if an error was found (i.e. glGetError() did not return GL_NO_ERROR)
         * @note Calling this resets the OpenGL's error state to GL_NO_ERROR
         */
        bool CheckError()
        {
            return ((error_ = glGetError()) != GL_NO_ERROR);
        }
        /** Retrieve the last error code returned by glGetError()
         * @note A string representation for the error can be obtained through TranslateGLErrorCode_OGL()
         */
        GLenum GetLastError() const
        {
            return error_;
        }
    };

}

#endif

#endif

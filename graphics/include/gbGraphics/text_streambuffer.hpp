/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEXT_STREAMBUFFER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEXT_STREAMBUFFER_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/font.hpp>

#include <streambuf>

namespace GB_GRAPHICS_NAMESPACE {

    namespace Text {
        template<typename T>
        class BasicStreambuffer: public std::basic_streambuf<T, std::char_traits<T> > {
        private:
            typedef typename std::char_traits<T>::int_type  int_type;
            typedef typename std::char_traits<T>::char_type char_type;
            std::unique_ptr<TextRenderer> m_TextRenderer;
            gbFont* m_Font;
        public:
            BasicStreambuffer(TextRendererPtr&& renderer)
                :m_TextRenderer(std::move(renderer)), m_Font(nullptr)
            {
            }
            BasicStreambuffer(BasicStreambuffer<T>&& rhs)
                :m_TextRenderer(std::move(rhs.m_TextRenderer)), m_Font(rhs.m_Font)
            {
            }
            ~BasicStreambuffer()
            {
            }
            std::basic_streambuf<T, std::char_traits<T> >* SetFont(gbFont* font)
            {
                m_Font = font;
                m_TextRenderer->SetFont(font);
                return this;
            }
            TextRenderer& GetRenderer() const
            {
                return *m_TextRenderer;
            }
        private:
            int_type overflow(int_type c = std::char_traits<T>::eof()) {
                if(!std::char_traits<T>::eq_int_type(c, std::char_traits<T>::eof())) {
                    if( (!m_TextRenderer) || 
                        (!m_TextRenderer->RenderChar(c)) ) 
                    {
                        //error
                        return std::char_traits<T>::eof();
                    }
                }
                return std::char_traits<T>::not_eof(c);
            }
        };
    }

}

#endif

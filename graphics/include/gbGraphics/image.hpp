/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_IMAGE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_IMAGE_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/color.hpp>
#include <gbGraphics/lockable_surface.hpp>

#include <memory>
#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

    class gbImage: public ReadableSurface {
        std::vector<GBCOLOR> m_Data;
        int m_Width;
        int m_Height;
        bool m_IsLocked;
    public:
        gbImage(int width, int height);
        gbImage(int width, int height, GBCOLOR clear_color);
        gbImage(int width, int height, GBCOLOR* data);
        ~gbImage();
        gbImage(gbImage const& rhs);
        gbImage(gbImage&& rhs);
        gbImage& operator=(gbImage const& rhs);
        gbImage& operator=(gbImage&& rhs);
        void Swap(gbImage& img);
        std::vector<GBCOLOR> Release();
        /** @name LockableSurface
         * @{
         */
        void Lock(GBCOLOR** data, int* pitch);
        void LockRect(Rect const& rect, GBCOLOR** data, int* pitch);
        void Unlock();
        bool IsLocked() const;
        int GetWidth() const;
        int GetHeight() const;
        /// @}
    };

}

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_BUFFER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_BUFFER_HPP_

#include <gbGraphics/config.hpp>

#include <memory>

#include <boost/utility.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** @name Forward declarations
     * @{
     */
    class gbVertexBuffer;
    typedef std::shared_ptr<gbVertexBuffer> gbVertexBufferPtr;
    class gbVertexDataBase;
    class gbGeometry;
    typedef std::shared_ptr<gbGeometry> gbGeometryPtr;
    class gbVertexFormatBase;
    class gbVertexBinding;
    class gbIndexBuffer;
    /// @}

    /** Vertex buffer.
     * The gbVertexBuffer represents a piece of vertex data stored on the GPU.
     * The buffer is initially just a generic block of memory but is associated
     * with a gbVertexFormat once it is filled from a gbVertexData.
     */
    class gbVertexBuffer: public boost::noncopyable {
    public:
        /** Destructor
         */
        virtual ~gbVertexBuffer() {}
        /** Get the size of the buffer
         * @return Size of the buffer in bytes
         */
        virtual int GetSize() const=0;
        /** Fill the buffer with vertex data
         * @param[in] data Vertex data that is to be copied into the buffer
         * @note The buffer will become associated with the gbVertexFormat of
         *       the passed data.
         * @attention Changing to vertex data of a different vertex format will throw
         *            if any gbGeometry objects constructed from the buffer are still
         *            alive.
         * @throw InvalidProtocol if changing vertex formats while buffer is active in gbGeometry
         * @throw InvalidArgument if the vertex data is too large to fit in the buffer
         * @throw OpenGLError on OpenGL API error
         */
        virtual void SetVertexData(gbVertexDataBase const& data)=0;
        /** Create unindexed geometry from buffer
         * @param[in] bindings Bindings to use for gbGeometry
         * @return New geometry object
         * @note The vertex format of the given bindings must match those
         *       of the vertex data used to populate the buffer
         * @throw InvalidProtocol if no data was assigned to the buffer
         * @throw InvalidArgument if vertex formats of bindings and buffer do not match
         * @throw OpenGLError on OpenGL API error
         */
        virtual gbGeometryPtr CreateGeometry(gbVertexBinding const& bindings)=0;
        /** Create indexed geometry from buffer
         * @param[in] bindings Bindings to use for gbGeometry
         * @param[in] index_buffer Index buffer to use for gbGeometry
         * @return New geometry object
         * @note The vertex format of the given bindings must match those
         *       of the vertex data used to populate the buffer
         * @throw InvalidProtocol if no data was assigned to the buffer
         * @throw InvalidArgument if vertex formats of bindings and buffer do not match
         * @throw OpenGLError on OpenGL API error
         */
        virtual gbGeometryPtr CreateGeometry(gbVertexBinding const& bindings, gbIndexBuffer& index_buffer)=0;
        /** Get the vertex format for the buffer
         * @return The gbVertexFormat of the data used to populate the buffer
         * @throw InvalidProtocol if no data was assigned to the buffer
         */
        virtual gbVertexFormatBase const* GetVertexFormat() const=0;
    };

}

#ifdef GB_GRAPHICS_HAS_OPENGL
#    define GB_GRAPHICS_INCLUDE_TOKEN_VERTEX_BUFFER_OGL_HPP_
#    include <gbGraphics/vertex_buffer_OGL.hpp>
#    undef GB_GRAPHICS_INCLUDE_TOKEN_VERTEX_BUFFER_OGL_HPP_
#endif

#endif

/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_MESH_LOADER_OBJ_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_MESH_LOADER_OBJ_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/mesh_loader.hpp>
#include <gbGraphics/index_data.hpp>
#include <gbGraphics/vertex_data.hpp>
#include <gbGraphics/vertex_format.hpp>

#include <diagnostic/diagnostic.hpp>
#include <gbUtil/cxx11_features.hpp>

#include <fstream>
#include <istream>
#include <limits>
#ifdef HAS_CXX11_LIB_REGEX
#    include <regex>
#else
#    include <boost/regex.hpp>
#endif
#include <string>
#include <type_traits>
#include <unordered_map>
#include <vector>

namespace GB_GRAPHICS_NAMESPACE {

    namespace MeshLoader {
        /** Index Tuple type used for face flattening
         */
        template<typename IndexType_T>
        struct IndexTuple {
            IndexType_T VertexIndex;
            IndexType_T NormalIndex;
            IndexType_T TextureIndex;
        public:
            IndexTuple():VertexIndex(0), NormalIndex(0), TextureIndex(0)
            { }
            IndexTuple(IndexType_T v, IndexType_T n, IndexType_T t): VertexIndex(v), NormalIndex(n), TextureIndex(t)
            { }
            bool operator==(IndexTuple const& rhs) const {
                return ( (VertexIndex == rhs.VertexIndex) &&
                         (NormalIndex == rhs.NormalIndex) &&
                         (TextureIndex == rhs.TextureIndex) );
            }
            bool operator<(IndexTuple const& rhs) const {
                if(VertexIndex != rhs.VertexIndex) { return VertexIndex < rhs.VertexIndex; }
                else if(NormalIndex != rhs.NormalIndex) { return NormalIndex < rhs.NormalIndex; }
                else if(TextureIndex != rhs.TextureIndex) { return TextureIndex < rhs.TextureIndex; }
                else { return false; }
            }
        };

        template<typename IndexType_T>
        struct IndexTupleHash: std::unary_function<IndexTuple<IndexType_T>, size_t>
        {
            inline size_t operator()(IndexTuple<IndexType_T> const& t) const {
                ///@todo more clever hashing
                return static_cast<size_t>(t.VertexIndex) ^
                       (static_cast<size_t>(t.NormalIndex) << 16) ^
                       (static_cast<size_t>(t.TextureIndex) << 21);
            }
        };

        template<typename IndexType_T>
        struct IndexTupleMap {
            typedef std::unordered_map<IndexTuple<IndexType_T>, IndexType_T, IndexTupleHash<IndexType_T>> type;
        };

        /** Intermediate storage for mesh face index data during parsing
         * @note Indices are 1-based if positive and offset-based if negative.
         *       The corresponding vertex lies at (index-1) in the respective
         *       ParsedVertexData_T list.
         * @attention Currently, offset-based indices are not supported.
         * @note This class is defined outside of gbMeshLoader_Obj to keep
         *       Visual Studio from complaining about long typenames
         */
        template<typename IndexType_T>
        struct ParsedFaceData_T {
            std::vector<IndexType_T> Face_Vertex;        ///< Geometry indices
            std::vector<IndexType_T> Face_Normal;        ///< Normal indices
            std::vector<IndexType_T> Face_TexCoord;        ///< Texture coordinate indices
            int  VerticesPerFace;                        ///< Number of vertices per face
            bool hasNormal;                                ///< true iff Normal indices are available
            bool hasTexCoord;                            ///< true iff Texture coordinate indices are available
        public:
            /** Constructor
             */
            ParsedFaceData_T()
                :VerticesPerFace(0), hasNormal(false), hasTexCoord(false)
            { }
        };

        template<typename T>
        struct GetLayoutVecValueType;
        template<typename T>
        struct GetLayoutVecValueType {
            typedef typename T::Layout::value_type type;
        };
        template<>
        struct GetLayoutVecValueType<VertexComponent::NilDesc> {
            typedef void type;
        };
    }

    /** Mesh loader for Wavefront OBJ files
     */
    template<typename gbVertexFormat_T, typename IndexType_T=uint16_t>
    class gbMeshLoader_Obj: public gbMeshLoader {
    public:
        gbVertexData<gbVertexFormat_T> VertexData;            ///< Vertex data from OBJ file
        gbIndexData_Triangle<IndexType_T> IndexData;        ///< Index data from OBJ file
    private:
        /** @name Vertex component format descriptor types
         * @{
         */
        typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Position, gbVertexFormat_T>::type PositionDesc_T;
        typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::Normal, gbVertexFormat_T>::type NormalDesc_T;
        typedef typename VertexData::ComponentBySemantics<VertexComponent::Semantics::TextureCoordinates, gbVertexFormat_T>::type TexCoordDesc_T;
        static_assert( !std::is_same<PositionDesc_T, VertexComponent::NilDesc>::value,
                        "Invalid vertex format: Need at least one component for Position" );
        typedef typename MeshLoader::GetLayoutVecValueType<PositionDesc_T>::type PositionVecValueType;
        typedef typename MeshLoader::GetLayoutVecValueType<NormalDesc_T>::type NormalVecValueType;
        typedef typename MeshLoader::GetLayoutVecValueType<TexCoordDesc_T>::type TexCoordVecValueType;
        /// @}
        /** Intermediate storage for mesh vertex data during parsing
         * @note Position in vector corresponds to (index-1)
         */
        struct ParsedVertexData_T {
            std::vector<typename PositionDesc_T::Layout> Vertex;
            std::vector<typename NormalDesc_T::Layout> Normal;
            std::vector<typename TexCoordDesc_T::Layout> TexCoord;
        };
        /** Type for storing grouped FaceData
         */
        typedef std::vector< MeshLoader::ParsedFaceData_T<IndexType_T> > FaceGroups;
        /** Type for storing names for FaceGroups
         * @note Each entry holds the name for the FaceData at the same index in FaceGroups
         */
        typedef std::vector< std::string >      FaceGroupNames;
        /** Type for storing face group pointers
         */
        typedef std::vector<MeshLoader::ParsedFaceData_T<IndexType_T>*> FacePtrGroup;
    private:
        bool m_HasNormals;
        bool m_HasTexCoords;
    public:
        /** Constructor
         * @param[in] filename Path to an OBJ file on disk
         * @note The entire file will be copied into memory before parsing.
         *       For better performance, consider using the iterator
         *       constructor with iterators to a memory mapped file.
         */
        gbMeshLoader_Obj(std::string const& filename)
            :m_HasNormals(false), m_HasTexCoords(false)
        {
            std::ifstream fin(filename.c_str(), std::ios_base::binary);
            if(!fin) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                                      "Unable to open file" );
            }
            ReadData(fin);
        }
        /** Constructor
         * @param[in] is Istream to the OBJ data
         * @note The contents of the istream will be copied before parsing.
         *       For better performance, consider using the iterator
         *       constructor which operates directly on the data.
         */
        gbMeshLoader_Obj(std::istream& is)
        {
            ReadData(is);
        }
        /** Constructor
         * @tparam Iterator_T Iterator type
         * @param[in] it_begin Begin iterator to OBJ data
         * @param[in] it_end End iterator to OBJ data
         */
        template<typename Iterator_T>
        gbMeshLoader_Obj(Iterator_T it_begin, Iterator_T it_end)
        {
            ParseData(it_begin, it_end);
        }
        /** @name gbMeshLoader
         * @{
         */
        gbMesh CreateMesh(gbDrawingContext3D& dc3d, gbVertexBinding const& binding) const {
            return gbMesh(dc3d, VertexData, IndexData, binding);
        }
        bool HasVertexNormals() const {
            return m_HasNormals;
        }
        bool HasTextureCoordinates() const {
            return m_HasTexCoords;
        }
        /// @}
    private:
        /** Read data from stream
         * @param[in] is istream to OBJ data
         */
        void ReadData(std::istream& is)
        {
            is.seekg(0, std::ios_base::end);
            std::streampos const data_size = is.tellg();
            is.seekg(0, std::ios_base::beg);
            if(is.fail() || (data_size == std::streampos(0))) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                                      "Data read error" );
            }
            std::vector<char> buffer;
            buffer.resize(data_size);
            is.read(buffer.data(), data_size);
            if(is.gcount() != data_size) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                                      "Data read error" );
            }
            ParseData(buffer.cbegin(), buffer.cend());
        }
        /** Parse OBJ data
         * @tparam Iterator_T Random access iterator
         * @param[in] it_begin begin iterator
         * @param[in] it_end end iterator
         */
        template<typename Iterator_T>
        void ParseData(Iterator_T it_begin, Iterator_T it_end)
        {
            ParsedVertexData_T parser_vertex_data;
            FaceGroups face_groups;
            FaceGroupNames face_group_names;
            FacePtrGroup face_target_groups;
            std::vector<MeshLoader::IndexTuple<IndexType_T>> tmp_tuple;
            typename MeshLoader::IndexTupleMap<IndexType_T>::type index_tuple_map;
            Iterator_T it = it_begin;
            int line_index = 1;
            while(it != it_end) {
                switch(*it) {
                case '#': /* comment; skip line */  break;
                case '\0': /* end of string */  break;
                case 'g':
                    face_target_groups = GroupSwitch(it, face_groups, face_group_names);
                    if(face_target_groups.size() != 1) {
                        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "Multiple target groups not supported");
                    }
                    break;
                case 'v':
                    ParseVertex(it, &parser_vertex_data);
                    break;
                case 'f':
                    if(face_target_groups.empty()) { face_target_groups.push_back(GetFaceGroupByName("default", face_groups, face_group_names)); }
                    if(face_target_groups.front()->VerticesPerFace <= 0) {
                        ScanFaceLayout(it, it_end, face_target_groups.front());
                        tmp_tuple.resize(face_target_groups.front()->VerticesPerFace, MeshLoader::IndexTuple<IndexType_T>(0,0,0));
                    }
                    ParseFace(it, face_target_groups.front(), &tmp_tuple, parser_vertex_data);
                    DIAG_ASSERT( ((*it) == '\n') || ((*it) == '\r') || ((*SkipWhitespace(it, it_end)) == '\n') );
                    AddFlattenedFaces(tmp_tuple, parser_vertex_data, index_tuple_map);
                    break;
                default:
                    DIAG_LOG(WARNING, "Unknown label: \'" << (*it) << "\' at line " << line_index);
                    break;
                }
                //advance to next line
                it = SkipWhitespace( GetEndOfLine(it, it_end), it_end, &line_index );
            }
            //remove empty groups
            RemoveEmptyGroups(face_groups, face_group_names);
            m_HasNormals = !parser_vertex_data.Normal.empty();
            m_HasTexCoords = !parser_vertex_data.TexCoord.empty();
        }
        /** Skip to the end of the current line
         * @tparam Iterator_T Iterator type
         * @param[in] it_position Iterator to anywhere in the data
         * @param[in] it_end Iterator to the end of data
         * @return Iterator to the the next newline or it_end
         */
        template<typename Iterator_T>
        inline Iterator_T GetEndOfLine( Iterator_T const& it_position,
                                        Iterator_T const& it_end )
        {
            Iterator_T ret = it_position;
            //find next newline
            while((ret != it_end) && (*ret != '\n')) { ++ret; }
            return ret;
        }
        /** Skip whitespace
         * @tparam Iterator_T Iterator type
         * @param[in] it_position Iterator to anywhere in the data
         * @param[in] it_end Iterator to the end of data
         * @param[in,out] line_count Line count will be incremented for every encountered newline
         * @return Iterator to the the next non-whitespace or it_end
         */
        template<typename Iterator_T>
        inline Iterator_T SkipWhitespace( Iterator_T const& it_position,
                                          Iterator_T const& it_end,
                                          int* line_count=nullptr )
        {
            Iterator_T ret = it_position;
            //skip whitespace
            for(;;) {
                while((ret != it_end) && ( (*ret == '\r') || (*ret == ' ') || (*ret == '\t') )) {
                    ++ret;
                }
                if((ret == it_end) || (*ret != '\n')) { break; }
                if(line_count) { ++(*line_count); }
                ++ret;
            }
            return ret;
        }
        /** Process a group switch entry
         * @param[in,out] p Iterator to beginning of line; On return, points to new location to proceed parsing from
         * @param[in, out] face_groups Target list of FaceData
         * @param[in, out] group_names Target GroupNameList
         * @return A vector of pointers to all MeshData structs active in the current group
         */
        template<typename Iterator_T>
        inline FacePtrGroup
            GroupSwitch( Iterator_T& p,
                         FaceGroups& face_groups,
                         FaceGroupNames& group_names )
        {
            //skip 'g'
            ++p;
            FacePtrGroup ret;
            while((*p) != '\n') {
                //skip ' '
                ++p;
                //parse next group name
                Iterator_T group_name_start = p;
                while( ((*p) != ' ') && ((*p) != '\n') ) { 
                    ++p; 
                }
                std::string group_name(group_name_start, p);
                //add mesh data to return list
                ret.push_back(GetFaceGroupByName(group_name, face_groups, group_names));
            }
            if(ret.empty()) {
                //default group
                std::string group_name("default");
                ret.push_back(GetFaceGroupByName(group_name, face_groups, group_names));
            }
            return ret;
        }
        /** Retrieve a pointer to a FaceGroup from its name
         * @param[in] name Group name
         * @param[in,out] face_groups FaceGroups list; if no group of the given name exists, it will be created
         * @param[in,out] group_names FaceGroupNames list; if no group of the given name exists, it will be created
         */
        inline MeshLoader::ParsedFaceData_T<IndexType_T>*
            GetFaceGroupByName( std::string const& name,
                                FaceGroups& face_groups,
                                FaceGroupNames& group_names )
        {
            //get iterator to group name list
            FaceGroupNames::iterator it_name =
                std::find( group_names.begin(),
                           group_names.end(),
                           name );
            if(it_name == group_names.end()) {
                it_name = group_names.insert(it_name, name);
                face_groups.push_back( MeshLoader::ParsedFaceData_T<IndexType_T>() );
            }
            DIAG_ASSERT((*it_name) == name);
            //get iterator to mesh data
            typename FaceGroups::iterator it_data = face_groups.begin();
            for( FaceGroupNames::iterator it = group_names.begin();
                 it != it_name;
                 ++it ) 
            { ++it_data; }
            return &(*it_data);
        }
        /** Determine number of vertices per face and what per-vertex data is available
         * @param[in] p Iterator to the beginning of a line with face data
         * @param[in] it_end Iterator to end of data
         * @param[out] out_mesh Will receive face layout
         */
        template<typename Iterator_T>
        void ScanFaceLayout( Iterator_T const& p,
                             Iterator_T const& it_end,
                             MeshLoader::ParsedFaceData_T<IndexType_T>* const out_mesh )
        {
#ifdef HAS_CXX11_LIB_REGEX
            using std::regex;
            using std::regex_search;
            using std::regex_replace;
#else
            using boost::regex;
            using boost::regex_search;
            using boost::regex_replace;
#endif
            std::string const line(p, GetEndOfLine(p, it_end));
            std::string tmp;        //< used to get number of vertices per face
            if(regex_search(line, regex("(\\s)+(-)?(\\d)+//(-)?(\\d)+(\\s)+"))) {
                // layout: v//vn
                out_mesh->hasNormal = true;
                out_mesh->hasTexCoord = false;
                tmp = regex_replace(line, regex("(-)?(\\d)+//(-)?(\\d)+"), std::string("V"));
            } else if(regex_search(line, regex("(\\s)+(-)?(\\d)+/(-)?(\\d)+(\\s)+"))) {
                // layout: v/vt
                out_mesh->hasNormal = false;
                out_mesh->hasTexCoord = true;
                tmp = regex_replace(line, regex("(-)?(\\d)+/(-)?(\\d)+"), std::string("V"));
            } else if(regex_search(line, regex("(\\s)+(-)?(\\d)+/(-)?(\\d)+/(-)?(\\d)+(\\s)+"))) {
                // layout: v/vt/vn
                out_mesh->hasNormal = true;
                out_mesh->hasTexCoord = true;
                tmp = regex_replace(line, regex("(-)?(\\d)+/(-)?(\\d)+/(-)?(\\d)+"), std::string("V"));
            } else {
                // layout: v
                out_mesh->hasNormal = false;
                out_mesh->hasTexCoord = false;
                tmp = regex_replace(line, regex("(-)?(\\d)+"), std::string("V"));
            }
            //count the 'V's in tmp to get vertices per face:
            out_mesh->VerticesPerFace = static_cast<int>(std::count(tmp.begin(), tmp.end(), 'V'));
            DIAG_CHECK((out_mesh->VerticesPerFace == 3) || (out_mesh->VerticesPerFace == 4));
        }
        /** Parse a face entry line from OBJ
         * @param[in,out] p Iterator to beginning of line; On return, points to new location to proceed parsing from
         * @param[out] out_mesh Will receive new face data
         */
        template<typename Iterator_T>
        inline void ParseFace( Iterator_T& p,
                               MeshLoader::ParsedFaceData_T<IndexType_T>* const out_mesh,
                               std::vector<MeshLoader::IndexTuple<IndexType_T>>* const out_index_tuple,
                               ParsedVertexData_T const& vertex_data )
        {
            unsigned long const max_vertex_index = static_cast<unsigned long>(vertex_data.Vertex.size() + 1);
            unsigned long const max_texture_index = static_cast<unsigned long>(vertex_data.TexCoord.size() + 1);
            unsigned long const max_normal_index = static_cast<unsigned long>(vertex_data.Normal.size() + 1);
            unsigned long const max_type_index = static_cast<unsigned long>(std::numeric_limits<IndexType_T>::max());
            if((max_vertex_index > max_type_index) || (max_texture_index > max_type_index) || (max_normal_index > max_type_index)) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                      "Chosen index type too small to fit loaded mesh" );
            }
            //skip 'f'
            char const* pos = &(*(++p));
            //parse face indices
            char* pend = nullptr;
            for(int i=0; i<out_mesh->VerticesPerFace; ++i) {
                DIAG_ASSERT(i < static_cast<int>(out_index_tuple->size()));
                //vertex index
                unsigned long v = std::strtoul(pos, &pend, 0);
                if(v < 0) { v += max_vertex_index; }
                DIAG_ASSERT(v >= 0);
                out_mesh->Face_Vertex.push_back(static_cast<IndexType_T>(v));
                (*out_index_tuple)[i].VertexIndex = static_cast<IndexType_T>(v);
                //texcoord index (optional)
                if(out_mesh->hasTexCoord) {
                    pos = pend + 1;
                    v = std::strtoul(pos, &pend, 0);
                    if(v < 0) { v += max_texture_index; }
                    DIAG_ASSERT(v >= 0);
                    StoreFaceIndex_TexCoord<!std::is_same<TexCoordDesc_T, VertexComponent::NilDesc>::value>(out_mesh, out_index_tuple, i, v);
                } else {
                    out_index_tuple->at(i).TextureIndex = 0;
                }
                //normal index (optional)
                if(out_mesh->hasNormal) {
                    pos = pend + 1;
                    if(*pos == '/') { ++pos; }
                    v = std::strtoul(pos, &pend, 0);
                    if(v < 0) { v += max_normal_index; }
                    DIAG_ASSERT(v >= 0);
                    StoreFaceIndex_Normal<!std::is_same<NormalDesc_T, VertexComponent::NilDesc>::value>(out_mesh, out_index_tuple, i, v);
                } else {
                    out_index_tuple->at(i).NormalIndex = 0;
                }
                //adjust position
                pos = pend + 1;
            }
            //adjust iterator
            if(pend != NULL) { p += pend - &(*p); }
        }
        /** Stores texcoord index information during face parsing
         */
        template<bool HasTexCoord>
        typename std::enable_if<HasTexCoord, void>::type
        StoreFaceIndex_TexCoord( MeshLoader::ParsedFaceData_T<IndexType_T>* const out_mesh,
                                 std::vector<MeshLoader::IndexTuple<IndexType_T>>* const out_index_tuple,
                                 int i,
                                 unsigned long v ) 
        {
            IndexType_T const texcoord_index = static_cast<IndexType_T>(v);
            out_mesh->Face_TexCoord.push_back(texcoord_index);
            (*out_index_tuple)[i].TextureIndex = texcoord_index;
        }

        template<bool HasTexCoord>
        typename std::enable_if<!HasTexCoord, void>::type
        StoreFaceIndex_TexCoord( MeshLoader::ParsedFaceData_T<IndexType_T>* const /* out_mesh */,
                                 std::vector<MeshLoader::IndexTuple<IndexType_T>>* const out_index_tuple,
                                 int i,
                                 unsigned long /* v */ ) 
        {
            (*out_index_tuple)[i].TextureIndex = 0;
        }
        /** Stores normal index information during face parsing
         */
        template<bool HasNormal>
        typename std::enable_if<HasNormal, void>::type
        StoreFaceIndex_Normal( MeshLoader::ParsedFaceData_T<IndexType_T>* const out_mesh,
                               std::vector<MeshLoader::IndexTuple<IndexType_T>>* const out_index_tuple,
                               int i,
                               unsigned long v ) 
        {
            IndexType_T const normal_index = static_cast<IndexType_T>(v);
            out_mesh->Face_Normal.push_back(normal_index);
            (*out_index_tuple)[i].NormalIndex = normal_index;
        }

        template<bool HasNormal>
        typename std::enable_if<!HasNormal, void>::type
        StoreFaceIndex_Normal( MeshLoader::ParsedFaceData_T<IndexType_T>* const /* out_mesh */,
                               std::vector<MeshLoader::IndexTuple<IndexType_T>>* const out_index_tuple,
                               int i,
                               unsigned long /* v */ ) 
        {
            (*out_index_tuple)[i].NormalIndex = 0;
        }
        /** Parse a vertex entry
         * @param[in,out] p Iterator to beginning of line; On return, points to new location to proceed parsing from
         * @param[out] out_mesh Will receive new vertex data
         */
        template<typename Iterator_T>
        inline void ParseVertex( Iterator_T& p,
                                 ParsedVertexData_T* const out_parser_vertex_data )
        {
            //skip 'v' and save next char
            char c = *(++p);
            //skip next char
            ++p;
            switch(c) {
                case ' ':
                    // geometry vertex
                    ParseVertex3<PositionVecValueType>(p, &out_parser_vertex_data->Vertex, false);
                    break;
                case 't':
                    // texture vertex
                    /// @todo obj allows 3d texcoords as well
                    ParseVertex2<TexCoordVecValueType>(p, &out_parser_vertex_data->TexCoord, true);
                    break;
                case 'n':
                    // vertex normal
                    ParseVertex3<NormalVecValueType>(p, &out_parser_vertex_data->Normal, true);
                    break;
                default:
                    //unknown label
                    DIAG_LOG(WARNING, "Unknown vertex label: \'v" << c << "\'");
                    break;
                }
        }
        /** Parse a 3d vertex vector
         * @tparam VecValue_T Value type of the VertexComponent::Layout used as Layout_T
         * @tparam Iterator_T Iterator type
         * @tparam Layout_T Layout type (must be a VertexComponent::Layout type)
         * @param[in,out] p Iterator pointing to the first vertex coordinate; On return, points to new location to proceed parsing from
         * @param[out] out std::vector that will receive vertex data
         * @param[in] is_normal If parsing to a Vec4, this determines the w component (0 for normals, 1 otherwise)
         */
        template<typename VecValue_T, typename Iterator_T, typename Layout_T> inline
        typename std::enable_if<std::is_same<Layout_T, VertexComponent::Layout::Vec3<VecValue_T>>::value, void>::type
        ParseVertex3( Iterator_T& p, std::vector<Layout_T>* out, bool /* is_normal */ )
        {
            Layout_T ret;
            static_assert(std::is_same<decltype(ret.x), VecValue_T>::value, "Unexpected layout type");
            char const* pos = &(*p);
            char* pend;
            ret.x = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            ret.y = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            ret.z = ParseFloat<VecValue_T>(pos, &pend);
            out->push_back(ret);
            p += pend - &(*p);
        }
        template<typename VecValue_T, typename Iterator_T, typename Layout_T> inline
        typename std::enable_if<std::is_same<Layout_T, VertexComponent::Layout::Vec4<VecValue_T>>::value, void>::type
        ParseVertex3( Iterator_T& p, std::vector<Layout_T>* out, bool is_normal )
        {
            Layout_T ret;
            char const* pos = &(*p);
            char* pend;
            ret.x = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            ret.y = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            ret.z = ParseFloat<VecValue_T>(pos, &pend);
            ret.w = (is_normal)?(static_cast<VecValue_T>(0)):(static_cast<VecValue_T>(1));
            out->push_back(ret);
            p += pend - &(*p);
        }
        template<typename VecValue_T, typename Iterator_T, typename Layout_T> inline
        typename std::enable_if<std::is_same<Layout_T, VertexComponent::Layout::TexCoordUVW<VecValue_T>>::value, void>::type
        ParseVertex3( Iterator_T& p, std::vector<Layout_T>* out, bool /* is_normal */ )
        {
            Layout_T ret;
            char const* pos = &(*p);
            char* pend = nullptr;
            ret.u = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            ret.v = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            ret.w = ParseFloat<VecValue_T>(pos, &pend);
            out->push_back(ret);
            p += pend - &(*p);
        }
        template<typename VecValue_T, typename Iterator_T, typename Layout_T> inline
        typename std::enable_if<std::is_same<Layout_T, VertexComponent::Nil>::value, void>::type
        ParseVertex3( Iterator_T& p, std::vector<Layout_T>* /* out */, bool /* is_normal */ )
        {
            /// just advance pointer
            char const* pos = &(*p);
            char* pend = nullptr;
            ParseFloat<double>(pos, &pend);  pos = pend;
            ParseFloat<double>(pos, &pend);  pos = pend;
            ParseFloat<double>(pos, &pend);
            p += pend - &(*p);
        }
        /** Parse a 2d vertex vector
         * @tparam VecValue_T Value type of the VertexComponent::Layout used as Layout_T
         * @tparam Iterator_T Iterator type
         * @tparam Layout_T Layout type (must be a VertexComponent::Layout type)
         * @param[in,out] p Iterator pointing to the first vertex coordinate; On return, points to new location to proceed parsing from
         * @param[out] out std::vector that will receive vertex data
         * @param[in] is_normal If parsing to a Vec4, this determines the w component (0 for normals, 1 otherwise)
         */
        template<typename VecValue_T, typename Iterator_T, typename Layout_T> inline
        typename std::enable_if<std::is_same<Layout_T, VertexComponent::Layout::Vec2<VecValue_T>>::value, void>::type
        ParseVertex2( Iterator_T& p, std::vector<Layout_T>* out, bool flip_y )
        {
            Layout_T ret;
            char const* pos = &(*p);
            char* pend;
            ret.x = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            VecValue_T const y = ParseFloat<VecValue_T>(pos, &pend);
            ret.y = (flip_y) ? (static_cast<VecValue_T>(1) - y) : (y);
            out->push_back(ret);
            p += pend - &(*p);
        }
        template<typename VecValue_T, typename Iterator_T, typename Layout_T> inline
        typename std::enable_if<std::is_same<Layout_T, VertexComponent::Layout::TexCoordUV<VecValue_T>>::value, void>::type
        ParseVertex2( Iterator_T& p, std::vector<Layout_T>* out, bool flip_y )
        {
            Layout_T ret;
            char const* pos = &(*p);
            char* pend = nullptr;
            ret.u = ParseFloat<VecValue_T>(pos, &pend);
            pos = pend;
            VecValue_T const y = ParseFloat<VecValue_T>(pos, &pend);
            ret.v = (flip_y) ? (static_cast<VecValue_T>(1) - y) : (y);
            out->push_back(ret);
            p += pend - &(*p);
        }
        template<typename VecValue_T, typename Iterator_T, typename Layout_T> inline
        typename std::enable_if<std::is_same<Layout_T, VertexComponent::Nil>::value, void>::type
        ParseVertex2( Iterator_T& p, std::vector<Layout_T>* /* out */, bool /* flip_y */ )
        {
            /// just advance pointer
            char const* pos = &(*p);
            char* pend = nullptr;
            ParseFloat<double>(pos, &pend); pos = pend;
            ParseFloat<double>(pos, &pend);
            p += pend - &(*p);
        }
        /** Parse a single float value
         * @tparam T Output type (must be float or double)
         * @param[in] str C string of a float
         * @param[out] end End pointer
         * @note The interface is the same as that of the std::strtod function
         */
        template<typename T> inline
        typename std::enable_if<std::is_same<T, double>::value, T>::type
        ParseFloat(char const* str, char** end) {
            return std::strtod(str, end);
        }
        template<typename T> inline
        typename std::enable_if<std::is_same<T, float>::value, T>::type
        ParseFloat(char const* str, char** end) {
            return static_cast<float>(std::strtod(str, end));
        }
        /** Flattens temporary index data from OBJ file and adds index to IndexData member
         * @param[in] index_tuples Vector of indices for a single primitive (must be 3 or 4 elements in size)
         * @param[in] parser_vertex_data Intermediate vertex data parsed from OBJ file
         * @param[in] index_tuple_map Maps index tuples to entries in VertexData
         * @note Since OBJ allows arbitrary separate indices for position, texture and normal data,
         *       we cannot use the raw vertex data right away. Instead, we need to build vertices
         *       corresponding to the index tuples.
         */
        inline void AddFlattenedFaces( std::vector<MeshLoader::IndexTuple<IndexType_T>> const& index_tuples,
                                       ParsedVertexData_T const& parser_vertex_data,
                                       typename MeshLoader::IndexTupleMap<IndexType_T>::type& index_tuple_map )
        {
            DIAG_ASSERT((index_tuples.size() == 3) || (index_tuples.size() == 4));
            typename gbIndexData_Triangle<IndexType_T>::IndexType triangle;
            IndexData::GetStorageElement<0>(triangle) = GetIndexFromTuple(index_tuples[0], parser_vertex_data, index_tuple_map);
            IndexData::GetStorageElement<1>(triangle) = GetIndexFromTuple(index_tuples[1], parser_vertex_data, index_tuple_map);
            IndexData::GetStorageElement<2>(triangle) = GetIndexFromTuple(index_tuples[2], parser_vertex_data, index_tuple_map);
            IndexData.PushBack(triangle);
            if(index_tuples.size() == 4) {
                ///Quad mesh; Add second triangle
                typename gbIndexData_Triangle<IndexType_T>::IndexType triangle2;
                IndexData::GetStorageElement<0>(triangle2) = GetIndexFromTuple(index_tuples[3], parser_vertex_data, index_tuple_map);
                IndexData::GetStorageElement<1>(triangle2) = IndexData::GetStorageElement<0>(triangle);
                IndexData::GetStorageElement<2>(triangle2) = IndexData::GetStorageElement<1>(triangle);
                IndexData.PushBack(triangle2);
            }
        }
        /** Gets a vertex index for a given index tuple
         * @param[in] index_tuple A single index tuple describing a vertex
         * @param[in] parser_vertex_data Raw vertex data indexed by index_tuple
         * @param[in] index_tuple_map Maps index tuples to entries in VertexData
         * @return Index to a vertex that matches index_tuple in VertexData
         * @note If VertexData does not contain a matching vertex, a new vertex will be added
         */
        inline IndexType_T GetIndexFromTuple( MeshLoader::IndexTuple<IndexType_T> const& index_tuple,
                                              ParsedVertexData_T const& parser_vertex_data,
                                              typename MeshLoader::IndexTupleMap<IndexType_T>::type& index_tuple_map )
        {
            DIAG_ASSERT(index_tuple.VertexIndex <= parser_vertex_data.Vertex.size());
            DIAG_ASSERT((parser_vertex_data.Normal.empty()) || (index_tuple.NormalIndex <= parser_vertex_data.Normal.size()));
            DIAG_ASSERT((parser_vertex_data.TexCoord.empty()) || (index_tuple.TextureIndex <= parser_vertex_data.TexCoord.size()));
            auto map_it = index_tuple_map.find(index_tuple);
            if(map_it == index_tuple_map.end()) {
                // index tuple does not exist; create a new vertex
                typename gbVertexData<gbVertexFormat_T>::StorageType v;
                StoreVertexData_Position(v, parser_vertex_data, index_tuple.VertexIndex-1);
                StoreVertexData_Normal<!std::is_same<NormalDesc_T, VertexComponent::NilDesc>::value>(v, parser_vertex_data, index_tuple.NormalIndex-1);
                StoreVertexData_TexCoord<!std::is_same<TexCoordDesc_T, VertexComponent::NilDesc>::value>(v, parser_vertex_data, index_tuple.TextureIndex-1);
                VertexData.PushBack(v);
                const uint32_t vertex_index = static_cast<uint32_t>(VertexData.NumberOfVertices());
                if(!(vertex_index < static_cast<uint32_t>(std::numeric_limits<IndexType_T>::max()))) {
                    DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                                            "Index range too large to fit given index type" );
                }
                IndexType_T const index = static_cast<IndexType_T>(vertex_index - 1);
                index_tuple_map[index_tuple] = index;
                return index;
            } else {
                // index tuple does exist; reuse existing flat vertex
                return map_it->second;
            }
        }
        /** Set vertex position data for an entry in VertexData
         */
        inline void StoreVertexData_Position(typename gbVertexData<gbVertexFormat_T>::StorageType& v, ParsedVertexData_T const& vdata, IndexType_T idx)
        {
            VertexData::GetStorageElement< VertexData::ComponentIndex<PositionDesc_T, gbVertexFormat_T>::value >(v) = vdata.Vertex[idx];
        }
        /** Set vertex normal data for an entry in VertexData
         */
        template<bool HasNormal> inline
        typename std::enable_if<HasNormal, void>::type
        StoreVertexData_Normal(typename gbVertexData<gbVertexFormat_T>::StorageType& v, ParsedVertexData_T const& vdata, IndexType_T idx)
        {
            if(!vdata.Normal.empty()) {
                VertexData::GetStorageElement< VertexData::ComponentIndex<NormalDesc_T, gbVertexFormat_T>::value >(v) = vdata.Normal[idx];
            }
        }
        template<bool HasNormal> inline
        typename std::enable_if<!HasNormal, void>::type
        StoreVertexData_Normal(typename gbVertexData<gbVertexFormat_T>::StorageType& /* v */, ParsedVertexData_T const& /* vdata */, IndexType_T /* idx */)
        {
            /* do nothing */
        }
        /** Set vertex texture data for an entry in VertexData
         */
        template<bool HasTexCoord> inline
        typename std::enable_if<HasTexCoord, void>::type
        StoreVertexData_TexCoord(typename gbVertexData<gbVertexFormat_T>::StorageType& v, ParsedVertexData_T const& vdata, IndexType_T idx)
        {
            if(!vdata.TexCoord.empty()) {
                VertexData::GetStorageElement< VertexData::ComponentIndex<TexCoordDesc_T, gbVertexFormat_T>::value >(v) = vdata.TexCoord[idx];
            }
        }
        template<bool HasTexCoord> inline
        typename std::enable_if<!HasTexCoord, void>::type
        StoreVertexData_TexCoord(typename gbVertexData<gbVertexFormat_T>::StorageType& /* v */, ParsedVertexData_T const& /* vdata */, IndexType_T /* idx */)
        {
            /* do nothing */
        }
        /** Remove empty group entries
         * @param[in,out] face_groups Group list; empty group entries will be removed
         * @param[in,out] face_group_names Name list; names referring to empty groups will be removed
         */
        inline void RemoveEmptyGroups(FaceGroups& face_groups, FaceGroupNames& face_group_names)
        {
            typename FaceGroups::iterator it_face = face_groups.begin();
            typename FaceGroupNames::iterator it_name = face_group_names.begin();

            while(it_name != face_group_names.end()) {
                if(it_face->Face_Vertex.empty()) {
                    it_face = face_groups.erase(it_face);
                    it_name = face_group_names.erase(it_name);
                } else {
                    ++it_face;
                    ++it_name;
                }
            }
        }
    };

}

#endif

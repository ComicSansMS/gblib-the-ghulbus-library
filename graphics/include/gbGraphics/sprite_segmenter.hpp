/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_SPRITE_SEGMENTER_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_SPRITE_SEGMENTER_HPP_


#include <gbGraphics/config.hpp>
#include <gbGraphics/rect.hpp>
#include <gbGraphics/renderable_2d.hpp>

#include <memory>
#include <boost/noncopyable.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    class gbSprite;
    typedef std::shared_ptr<gbSprite> gbSpritePtr;

    class gbSpriteSegmenter: public Renderable2D, public boost::noncopyable {
    private:
        gbSpritePtr m_Sprite;
        Rect m_SrcRect;
        Rect m_DstRect;
        Rect m_Grid;
        int m_nSegments;
        int m_nSegmentsPerRow;
    public:
        gbSpriteSegmenter(gbSpritePtr& sprite_ptr);
        /** @name Renderable2D
         * {
         */
        void Render();
        void SetSizeOnScreen(int width, int height);
        int GetWidthOnScreen() const;
        int GetHeightOnScreen() const;
        void SetPositionOnScreen(int x, int y);
        int GetPositionOnScreenX() const;
        int GetPositionOnScreenY() const;
        /// @}
        void SetSourceRect(Rect const& rect);
        void SetDestinationRect(Rect const& rect);
        void SetSegmentationGrid(int segments_per_row, int number_of_rows);
        int GetNumberOfGridCells() const;
        void SetActiveGridCell(int i);
        int GetActiveGridCell() const;
        void AdvanceActiveGridCell();
        int GetCellWidth() const;
        int GetCellHeight() const;
    };
}

#endif

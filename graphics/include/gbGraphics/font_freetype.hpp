/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_TEMPLATE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_TEMPLATE_HPP_

#ifndef GB_GRAPHICS_HAS_FREETYPE
#    error Freetype support disabled; Install freetype and re-run cmake before using this file
#endif

#include <gbGraphics/config.hpp>
#include <gbGraphics/font.hpp>

#include <memory>

namespace GB_GRAPHICS_NAMESPACE {

    namespace Fonts {

        class FreetypeFont: public gbFont {
        private:
            struct Pimpl;
            std::unique_ptr<Pimpl> pimpl_;
        public:
            FreetypeFont(std::string const& filename);
            ~FreetypeFont();
            /** @name gbFont
             * @{
             */
            bool HasFixedWidth() const;
            bool HasKerning() const;
            int GetFontHeight() const;
            int GetFontMaxWidth() const;
            void ResetKerning() const;
            void LoadGlyph(int c);
            void RenderGlyph();
            GB_MATH_NAMESPACE::Vector2i GetKerning() const;
            GB_MATH_NAMESPACE::Vector2i GetGlyphAdvance() const;
            GB_MATH_NAMESPACE::Vector2i GetGlyphOrigin() const;
            int GetGlyphWidth() const;
            int GetGlyphHeight() const;
            GBCOLOR_COMPONENT SampleGlyph(int x, int y) const;
            /// @}
            char const* GetFontFamilyName() const;
            char const* GetFontStyleName() const;
            int GetNumberOfFaces() const;
            int GetNumberOfGlyphs() const;
            void SetActiveFace(int index);
            bool IsScalable() const;
            void SetSizePoints(int pt);
            void SetSizePixels(int px);
        private:
            void LoadFontFace(int index);
        };

    }

}

#endif

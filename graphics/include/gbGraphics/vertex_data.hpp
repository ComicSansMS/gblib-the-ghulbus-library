/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_DATA_HPP_

#include <gbGraphics/config.hpp>
#include <gbGraphics/vertex_data_fwd.hpp>
#include <gbGraphics/vertex_data_base.hpp>
#include <gbGraphics/vertex_format.hpp>

#include <gbUtil/cxx11_features.hpp>

#include <cstring>
#include <type_traits>
#include <vector>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/distance.hpp>
#include <boost/mpl/find.hpp>
#include <boost/mpl/if.hpp>

namespace GB_GRAPHICS_NAMESPACE {

    /** Interleaved vertex data.
     * @tparam VertexFormat_T An instance of the gbVertexFormat template.
     *
     * The format of the underlying data is specified by a list of VertexComponent::Descriptor types
     * specified as template parameters to the gbVertexFormat. gbVertexData acts like a std::tuple of those types.
     * @internal
     * Since it is impossible to generate a POD storage through TMP, gbVertexData uses a hybrid approach.
     * The templated VertexData::Storage struct is used for storage, while an mpl::vector stores the
     * typelist describing the vertex format. This combines the power of std::tuple with the C-compatible
     * storage of plain POD structs.
     * @endinternal
     *
     * @section sec_gbVertexData_use Usage
     * @subsection subsec_gbVertexData_use_cons Construction
     * Use the CreateInterleaved() factory methods for construction.
     * The desired vertex layout is specified through the supplied template
     * parameters. For example, to construct a gbVertexData where each vertex consists
     * of an XYZ entry for position and an RGB entry for colors, use
     * 
     @code
VertexDataType vertex_data = VertexData::CreateInterleaved<VertexComponent::Position3f, VertexComponent::Color3f>();
     @endcode
     * @subsection subsec_gbVertexData_use_tparam Meaning of the template parameters
     * The template parameters allow complete control over the final vertex layout.
     * Each component of of the vertex is described by two types: A layout type, describing the
     * binary layout of the component in storage and a semantics type describing the semantics
     * of the component. For example, both position and normal data are usually specified by
     * the same layout type (e.g. Vec3f), but may still be uniquely identified because of
     * their different semantic types.
     * The VertexData::Descriptor template is used for specifying layout and semantics of
     * a component. For a list of predefined descriptors, see @ref gb3d_vertdata_desc.
     *
     * @subsection subsec_gbVertexData_use_layout Vertex storage layout
     * The layout of the final storage is an array of struct of the vertex component
     * layout types:
     @code
CreateInterleaved<Descriptor<T1, S1>, Descriptor<T2, S2>, Descriptor<T3, S3>>();
     @endcode
     * will result in the following storage type
     @code
struct gbVertexData::StorageType {
    T1 m1;
    T2 m2;
    T3 m3;
};
     @endcode
     * The usual struct padding rules from C++ apply. To change alignment of a certain component,
     * use the Semantics::Padding semantic to insert empty components for padding.
     * @attention It is up to the user to ensure that padding in the StorageType is explicitly
     *            modeled by the vertex format. In particular, if the final storage struct 
     *            contains unnamed padding bits inserted by the compiler,
     *            the user must accordingly model those in the vertex format by specifying a
     *            corresponding component with the Semantics::Padding semantic. This is also
     *            true for paddings inserted beyond the last component of the struct.
     *            Failing to do so will result in an exception upon construction of gbVertexData.
     *            For example, the following vertex format is likely to contain padding bytes 
     *            between the first and second component:
     @code
// !warning! potentially bad code up ahead
gbVertexFormat<
    VertexComponent::Descriptor<VertexComponent::Layout::Vec3<float>, VertexComponent::Semantics::Position>,
    VertexComponent::Descriptor<VertexComponent::Layout::Vec3<double>, VertexComponent::Semantics::Generic>
> ...
     @endcode
     *            On most compilers, the double elements from the second component need to be
     *            8-byte aligned, so a total of four padding bytes will be inserted between the
     *            two components. This must be modeled explicitly by the vertex format:
     @code
gbVertexFormat<
    VertexComponent::Descriptor<VertexComponent::Layout::Vec3<float>, VertexComponent::Semantics::Position>,
    VertexComponent::Descriptor<int32_t, VertexComponent::Semantics::Padding>,
    VertexComponent::Descriptor<VertexComponent::Layout::Vec3<double>, VertexComponent::Semantics::Generic>
> ...
     @endcode
     *            Be aware that ISO C++ does not specify exact rules for padding. It is up your
     *            compiler's implementation if and how the storage struct is padded. Note that 
     *            the Padding semantics allows using of arbitrary layout types, which is not 
     *            possible with any other semantics.
     *
     * @subsection subsec_gbVertexData_use_customtypes Custom Layout types
     * In addition to the predefined descriptor, you may use the VertexData::Descriptor template
     * to define your own component types. However, be aware that the layout type must be a
     * POD type and you must provide a specialization of the VertexComponent::Traits::LayoutComponentType
     * if the component should be bindable to a vertex shader attribute.
     * The Semantics type is a pure tagging type and has no restrictions.
     *
     * @subsection subsec_gbVertexData_use_fill Filling Vertex Data
     * Use the VertexData::Fill() function to fill the vertex data:
     @code
VertexDataType vertex_data = VertexData::CreateInterleaved<VertexComponent::Position3f, VertexComponent::Color3f>();
float* positions = {
    1.0f, 2.0f, 3.0f
    3.0f, 2.0f, 1.0f
};
vertex_data.Resize(2);
VertexData::Fill<VertexComponent::Position3f>(positions);
     @endcode
     * Note that you need to call Resize() before Fill().
     *
     * @subsection subsec_gbVertexData_use_access Accessing Vertex Data
     * Use the VertexData::Get() functions to access vertex components.
     @code
VertexDataType vertex_data = VertexData::CreateInterleaved<VertexComponent::Position3f, VertexComponent::Color3f>();
vertex_data.Resize(42);
for(int i=0; i<vertex_data.NumberOfVertices(); ++i) {
    VertexData::Get<VertexComponent::Position3f>(vertex_data, i).x = 0.0f;
}
     @endcode
     * If the gbVertexData contains multiple components of the same descriptor type, Get() will only
     * return the first component of a matching type. The other components can still be accessed
     * via index:
     @code
VertexDataType vertex_data = VertexData::CreateInterleaved<VertexComponent::Position3f, VertexComponent::Position3f>();
vertex_data.Resize(42);
for(int i=0; i<vertex_data.NumberOfVertices(); ++i) {
    // first component:
    VertexData::Get<VertexComponent::Position3f>(vertex_data, i).x = 0.0f;
    // second component:
    VertexData::Get<1>(vertex_data, i).x = 0.0f;
}
     @endcode
     */
    template<typename VertexFormat_T>
    class gbVertexData: public gbVertexDataBase {
        static_assert(std::is_base_of<gbVertexFormatBase, VertexFormat_T>::value, "Invalid vertex format");
    public:
        /** Storage type
         */
        typedef typename VertexFormat_T::StorageType StorageType;
        /** Typelist of VertexComponent::Descriptor
         */
        typedef typename VertexFormat_T::DescriptorList DescriptorList;
        /** Vertex format
         */
        typedef VertexFormat_T VertexFormat;
        static_assert(std::is_same<VertexFormat, typename VertexFormat_T::VertexFormat>::value, "Invalid vertex format");
        /** Index type used for storage access
         */
        typedef typename std::vector<StorageType>::size_type IndexType;
    private:
        std::vector<StorageType> m_Storage;                ///< Vertex data storage
        VertexFormat_T m_VertexFormat;
    public:
        /** Constructor
         */
        gbVertexData()
        {
        }
        /** Set the size of the vertex data
         * @param[in] size Number of vertices to fit in the vertex data
         */
        void Resize(int n_vertices) {
            m_Storage.resize(n_vertices);
        }
        /** Clear stored vertex data
         */
        void Clear() {
            m_Storage.clear();
        }
        /** Append one element to the end of the data
         * @param[in] vertex Single vertex data element
         */
        void PushBack(StorageType const& vertex) {
            m_Storage.push_back(vertex);
        }
        /** Array access
         * @param[in] idx Vertex index [0..NumberOfVertices)
         * @return Reference to the requested vertex
         */
        StorageType& operator[](IndexType idx) { return m_Storage[idx]; }
        /** Array access
         * @param[in] idx Vertex index [0..NumberOfVertices)
         * @return Reference to the requested vertex
         */
        StorageType const& operator[](IndexType idx) const { return m_Storage[idx]; }
        /** @name gbVertexDataBase
         * @{
         */
        int NumberOfVertices() const {
            return static_cast<int>(m_Storage.size());
        }
        void const* GetRawData() const {
            return m_Storage.data();
        }
        int GetRawSize() const {
            return static_cast<int>(m_Storage.size() * sizeof(StorageType));
        }
        gbVertexFormatBase const& GetVertexFormat() const {
            return m_VertexFormat;
        }
        /// @}
    };

    /** Vertex data storage namespace
     */
    namespace VertexData {
        /** Get vertex component index by type
         * @tparam T_Desc VertexData::Descriptor of the requested component
         * @tparam gbVertexFormat_T gbVertexFormat or gbVertexData type of the target vertex format
         * @note Result will be stored in ComponentIndex::value
         */
        template<typename T_Desc, class gbVertexFormat_T>
        struct ComponentIndex {
        private:
            //typedef for mpl vector
            typedef typename gbVertexFormat_T::DescriptorList T_vector;
            //get iterator to requested type
            typedef typename boost::mpl::find<T_vector, T_Desc>::type T_it;
            static_assert( !std::is_same<T_it, typename boost::mpl::end<T_vector>::type>::value,
                           "Requested element not found in Vertex data layout" );
        public:
            //use iterator position to get index from storage struct
            enum {
                value = boost::mpl::distance<typename boost::mpl::begin<T_vector>::type, T_it>::value
            };
        };

        /** Get vertex component by semantics type
         * @tparam Semantic_T Semantics type of the requested component
         * @tparam gbVertexFormat_T gbVertexFormat or gbVertexData type of the target vertex format
         * @note Result will be stored in ComponentBySemantics::type
         */
        template<typename Semantic_T, class gbVertexFormat_T>
        struct ComponentBySemantics {
        private:
            /** Metafunction for semantic equality check
             */
            template<typename Descriptor_T>
            struct SemanticsMatch {
                enum {
                    value = std::is_same<typename Descriptor_T::Semantics, Semantic_T>::value
                };
            };
            //typedef for mpl vector
            typedef typename gbVertexFormat_T::DescriptorList T_vector;
            //iterator to result type
            typedef typename boost::mpl::find_if<T_vector, SemanticsMatch<boost::mpl::_1>>::type T_it;
            //end iterator, in case find_if failed
            typedef typename boost::mpl::end<T_vector>::type T_it_end;
        public:
            typedef typename boost::mpl::if_< std::is_same<T_it, T_it_end>,
                                              VertexComponent::NilDesc,
                                              typename boost::mpl::deref<T_it>::type >::type type;
        };

        /** Get vertex component by type
         * @tparam T_Desc VertexData::Descriptor of the requested component
         * @param[in] vdata Vertex data object
         * @param[in] idx Index of the requested vertex
         * @return Reference to the first component of the requested vertex matching the Descriptor
         * @attention Vertex index is *not* bounds-checked!
         */
        template<typename T_Desc, class gbVertexData_T>
        typename T_Desc::Layout& Get(gbVertexData_T& vdata, typename gbVertexData_T::IndexType idx)
        {
            //use component index to get element from storage struct
            return GetStorageElement<
                        ComponentIndex<T_Desc, gbVertexData_T>::value
                    > ( vdata[idx] );
        }

        /** Get vertex component by index
         * @tparam I Index (0-based) of the requested component
         * @param[in] vdata Vertex data object
         * @param[in] idx Index of the requested vertex
         * @return Reference to the indexed component of the requested vertex
         * @attention Vertex index is *not* bounds-checked!
         */
        template<size_t I, class gbVertexData_T>
        typename boost::mpl::at_c<typename gbVertexData_T::DescriptorList, I>::type::Layout&
        Get(gbVertexData_T& vdata, typename gbVertexData_T::IndexType idx)
        {
            return GetStorageElement<I>( vdata[idx] );
        }

        /** Fills the entries for a component with data from a C array
         * @tparam T_Desc VertexData::Descriptor of the requested component
         * @param[in] vdata Vertex data object
         * @param[in] src_data Pointer to an array with the source data
         */
        template<typename T_Desc, typename T, class gbVertexData_T>
        void Fill(gbVertexData_T& vdata, T* src_data) {
            int const n_verts = vdata.NumberOfVertices();
            char* src_ptr = reinterpret_cast<char*>(src_data);
            for(int i=0; i<n_verts; ++i) {
                std::memcpy(&Get<T_Desc>(vdata, i), src_ptr, sizeof(typename T_Desc::Layout));
                src_ptr += sizeof(typename T_Desc::Layout);
            }
        }

        /** @name Vertex Data factories
         * @{
         */
        /** Create interleaved vertex data
         * @tparam T0 Type descriptors for the vertex components are given as template parameters
         * @return New gbVertexData object of the desired vertex layout
         */
        template<typename T0>
        gbVertexData<gbVertexFormat<T0>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0>>();
        }
        template<typename T0, typename T1>
        gbVertexData<gbVertexFormat<T0, T1>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1>>();
        }
        template<typename T0, typename T1, typename T2>
        gbVertexData<gbVertexFormat<T0, T1, T2>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2>>();
        }
        template<typename T0, typename T1, typename T2, typename T3>
        gbVertexData<gbVertexFormat<T0, T1, T2, T3>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8, typename T9 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8, typename T9, typename T10 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8, typename T9, typename T10, typename T11 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8, typename T9, typename T10, typename T11,
                  typename T12 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8, typename T9, typename T10, typename T11,
                  typename T12, typename T13 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8, typename T9, typename T10, typename T11,
                  typename T12, typename T13, typename T14 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>>();
        }
        template< typename T0, typename T1, typename T2, typename T3, typename T4, typename T5,
                  typename T6, typename T7, typename T8, typename T9, typename T10, typename T11,
                  typename T12, typename T13, typename T14, typename T15 >
        gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>> CreateInterleaved()
        {
            return gbVertexData<gbVertexFormat<T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>>();
        }
        ///@}
    }
}

#endif

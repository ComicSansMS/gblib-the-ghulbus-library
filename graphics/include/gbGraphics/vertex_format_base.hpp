/******************************************************************************
 * Copyright (c) 2006-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_GRAPHICS_INCLUDE_GUARD_VERTEX_FORMAT_BASE_HPP_
#define GB_GRAPHICS_INCLUDE_GUARD_VERTEX_FORMAT_BASE_HPP_

#include <gbGraphics/config.hpp>

#include <memory>

namespace GB_GRAPHICS_NAMESPACE {

    class gbVertexFormatBase {
    public:
        /** Runtime type info for vertex component layouts
         */
        enum ComponentType_T {
            VERTEX_COMPONENT_NO_TYPE = 0,            ///< Typeless component (e.g. for Nil or padding fields)
            VERTEX_COMPONENT_FLOAT,                    ///< Float
            VERTEX_COMPONENT_FLOAT2,                ///< 2d float vector
            VERTEX_COMPONENT_FLOAT3,                ///< 3d float vector
            VERTEX_COMPONENT_FLOAT4,                ///< 4d float vector
            VERTEX_COMPONENT_FLOAT_MAT2,            ///< 2x2 float matrix
            VERTEX_COMPONENT_FLOAT_MAT3,            ///< 3x3 float matrix
            VERTEX_COMPONENT_FLOAT_MAT4,            ///< 4x4 float matrix
            VERTEX_COMPONENT_FLOAT_MAT2x3,            ///< 2x3 float matrix
            VERTEX_COMPONENT_FLOAT_MAT2x4,            ///< 2x4 float matrix
            VERTEX_COMPONENT_FLOAT_MAT3x2,            ///< 3x2 float matrix
            VERTEX_COMPONENT_FLOAT_MAT3x4,            ///< 3x4 float matrix
            VERTEX_COMPONENT_FLOAT_MAT4x2,            ///< 4x2 float matrix
            VERTEX_COMPONENT_FLOAT_MAT4x3,            ///< 4x3 float matrix
            VERTEX_COMPONENT_INT,                    ///< Int
            VERTEX_COMPONENT_INT2,                    ///< 2d int vector
            VERTEX_COMPONENT_INT3,                    ///< 3d int vector
            VERTEX_COMPONENT_INT4,                    ///< 4d int vector
            VERTEX_COMPONENT_UINT,                    ///< Unsigned int
            VERTEX_COMPONENT_UINT2,                    ///< 2d unsigned int vector
            VERTEX_COMPONENT_UINT3,                    ///< 3d unsigned int vector
            VERTEX_COMPONENT_UINT4,                    ///< 4d unsigned int vector
            VERTEX_COMPONENT_DOUBLE,                ///< Double
            VERTEX_COMPONENT_DOUBLE2,                ///< 2d double vector
            VERTEX_COMPONENT_DOUBLE3,                ///< 3d double vector
            VERTEX_COMPONENT_DOUBLE4,                ///< 4d double vector
            VERTEX_COMPONENT_DOUBLE_MAT2,            ///< 2x2 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT3,            ///< 3x3 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT4,            ///< 4x4 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT2x3,            ///< 2x3 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT2x4,            ///< 2x4 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT3x2,            ///< 3x2 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT3x4,            ///< 3x4 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT4x2,            ///< 4x2 double matrix
            VERTEX_COMPONENT_DOUBLE_MAT4x3            ///< 4x3 double matrix
        };
        /** Runtime type info for vertex component semantics
         */
        enum ComponentSemantics_T {
            COMPONENT_SEMANTICS_NONE = 0,            ///< No semantics (e.g. for Nil fields)
            COMPONENT_SEMANTICS_GENERIC,            ///< Generic semantics (for custom fields)
            COMPONENT_SEMANTICS_POSITION,            ///< Vertex position
            COMPONENT_SEMANTICS_COLOR,                ///< Vertex color
            COMPONENT_SEMANTICS_NORMAL,                ///< Per-vertex normal
            COMPONENT_SEMANTICS_TEXTURE,            ///< Texture coordinates
            COMPONENT_SEMANTICS_PADDING                ///< Padding field
        };
    protected:
        /* This is defined here to avoid name-truncation warnings
         * with mpl::for_each due to gbVertexFormat's lenthy type
         */
        struct RuntimeInfo_T {
            int ComponentSize;                /// size in bytes
            int Offset;                        /// offset in bytes
            ComponentType_T Type;            /// runtime type
            ComponentSemantics_T Semantic;    /// runtime semantic
            RuntimeInfo_T()
                :ComponentSize(-1), Offset(-1), Type(VERTEX_COMPONENT_NO_TYPE), Semantic(COMPONENT_SEMANTICS_NONE)
            {}
            RuntimeInfo_T(int size, int offs, ComponentType_T type, ComponentSemantics_T semantic)
                :ComponentSize(size), Offset(offs), Type(type), Semantic(semantic)
            {}
        };
    public:
        /** Destructor
         */
        virtual ~gbVertexFormatBase() {}
        /** Polymorphic copy construction
         */
        virtual std::unique_ptr<gbVertexFormatBase> Clone() const=0;
        /** Get the stride of a vertex
         * @return Size of a single vertex in storage in bytes
         * @note Users usually don't need to call this.
         */
        virtual int GetStride() const=0;
        /** Get the number of components per vertex
         * @return Number of components
         */
        virtual int NumberOfComponents() const=0;
        /** Get the raw size of a specific vertex component
         * @param[in] i Component index [0..NumberOfComponents)
         * @return Size of the component in storage in bytes
         * @note Users usually don't need to call this.
         */
        virtual int ComponentRawSize(int i) const=0;
        /** Get the offset to a specific vertex component in raw memory
         * @param[in] i Component index [0..NumberOfComponents)
         * @return Offset to the component from the beginning of a vertex in storage
         * @note Users usually don't need to call this.
         */
        virtual int ComponentRawOffset(int i) const=0;
        /** Get the runtime type of a specific vertex component
         * @param[in] i Component index [0..NumberOfComponents)
         * @return Type of the component
         * @note Users should prefer static type checks offered through the concrete
         *       gbVertexData class over runtime type identification.
         */
        virtual ComponentType_T GetComponentType(int i) const=0;
        /** Get the runtime semantics of a specific vertex component
         * @param[in] i Component index [0..NumberOfComponents)
         * @return Semantics of the component
         * @note Users should prefer static type checks offered through the concrete
         *       gbVertexData class over runtime type identification.
         */
        virtual ComponentSemantics_T GetComponentSemantics(int i) const=0;
    };

    /** Equality comparison of gbVertexFormats
     * @note Two vertex formats are considered equal if their descriptor lists specify
     *       identical runtime layouts and semantics
     */
    inline bool operator==(gbVertexFormatBase const& fmt1, gbVertexFormatBase const& fmt2) {
        if(fmt1.GetStride() != fmt2.GetStride()) { return false; }
        int const n_components = fmt1.NumberOfComponents();
        if(fmt2.NumberOfComponents() != n_components) { return false; }
        for(int i=0; i<n_components; ++i) {
            if( (fmt1.GetComponentType(i) != fmt2.GetComponentType(i)) ||
                (fmt1.GetComponentSemantics(i) != fmt2.GetComponentSemantics(i)) ||
                (fmt1.ComponentRawOffset(i) != fmt2.ComponentRawOffset(i)) ||
                (fmt1.ComponentRawSize(i) != fmt2.ComponentRawSize(i)) )
            { return false; }
        }
        return true;
    }

    /** Not-equal comparison of gbVertexFormats
     * @note Two vertex formats are considered equal if their descriptor lists specify
     *       identical runtime layouts and semantics
     */
    inline bool operator!=(gbVertexFormatBase const& fmt1, gbVertexFormatBase const& fmt2) {
        return !(fmt1 == fmt2);
    }

}

#endif

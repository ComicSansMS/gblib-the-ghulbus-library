/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_DIAGNOSTIC_INCLUDE_GUARD_DIAGNOSTIC_LOG_HPP_
#define GB_DIAGNOSTIC_INCLUDE_GUARD_DIAGNOSTIC_LOG_HPP_

#include <diagnostic/config.hpp>

#include <sstream>
#include <string>

namespace GB_DIAGNOSTIC_NAMESPACE {

    /** Available log levels
     */
    enum LOG_LEVEL {
        LOG_LEVEL_INFO = 0,
        LOG_LEVEL_LOG = 1,
        LOG_LEVEL_WARNING = 2,
        LOG_LEVEL_ERROR = 3,
        LOG_LEVEL_CRITICAL = 4
    };

    /** Deriving from this class allows to provide a user-defined log target
     */
    class CustomLogTarget {
    public:
        virtual ~CustomLogTarget() { };
        /** Callback function receiving log messages from Logger
         * @param[in] msg Log message
         */
        virtual void Log(std::string const& msg)=0;
    };

    /** Buffer class used for logging
     * @note Do not use this class directly. Use DIAG_LOG macro instead.
     */
    class Logger {
    private:
        std::ostringstream sstr_;        ///< underlying stringstream buffer
        LOG_LEVEL level_;                ///< associated log level
    public:
        /** Constructor
         * @note Sets the log level to LOG
         */
        Logger();
        /** Constructor
         * @param[in] log_level Desired log level
         */
        Logger(LOG_LEVEL log_level);
        /** Destructor
         * @note Writes Logger contents to current ostream target
         *       for the associated log level
         */
        ~Logger();
        /** Get a reference to the underlying ostream for writing
         * @return ostream buffer of the Logger
         */
        std::ostream& GetLogStream();
    private:
        /** Helper function: writes the preamble string (timestamp) preceeding
         *                   the actual log message in each line to the buffer.
         */
        void Preamble();
    private:
        /** Private copy constructor (not implemented)
         */
        Logger(Logger const&);
        /** Private copy assignment (not implemented)
         */
        Logger& operator=(Logger const&);
    };

    /** Set the target logfile for all log levels at once
     * @param[in] filename Path to the logfile
     */
    void LogSetLogfile(std::string const& filename);

    /** Set the target ostream for a specific log level
     * @param[in] log_level Log level
     * @param[in] os New target ostream; 
     *               if NULL, logs for that level will be deactivated
     */
    void LogSetTarget(LOG_LEVEL log_level, std::ostream* os);
    /** Set the target logfile for a specific log level
     * @param[in] log_level Log level
     * @param[in] filename Path to the logfile for the current level
     */
    void LogSetTarget(LOG_LEVEL log_level, std::string const& filename);
    /** Set a custom target for a specific log level
     * @param[in] log_level Log level
     * @param[in] target Pointer to an object derived from CustomLogTarget
     * @attention The user is responsible for ensuring that CustomLogTargets
     *            are not destroyed while bound.
     */
    void LogSetTarget(LOG_LEVEL log_level, CustomLogTarget& target);

    /** Set whether to include timestamp in messages for a specific log level
     * @param[in] log_level Log level
     * @param[in] include_timestamp true if timestamps are to be included in
     *                              log messages; false otherwise
     */
    void LogIncludeTimestamp(LOG_LEVEL log_level, bool include_timestamp);

    /** Set whether to include timestamp in messages for all log levels
     * @param[in] include_timestamp true if timestamps are to be included in
     *                              log messages; false otherwise
     */
    void LogIncludeTimestamp(bool include_timestamp);

    /** Ostream inserter for LOG_LEVEL
     */
    std::ostream& operator<<(std::ostream& os, LOG_LEVEL l);
}

/** Logging macro
 * @param[in] log_level Desired log level; Valid log levels include all strings
 *                      from the Diagnostic::LOG_LEVEL struct with the
 *                      'LOG_LEVEL_' prefix stripped away
 * @param[in] expr Log message; Different message elements may be concatenated
 *                 using ostream inserters
 * @note Example: DIAG_LOG(INFO, "Value of i is " << i << ".");
 * @note Logging macros will be eliminated unless GB_DIAGNOSTIC_CONF_ENABLE_LOGGING is defined.
 */
#ifdef GB_DIAGNOSTIC_CONF_ENABLE_LOGGING
#    define DIAG_LOG(log_level, expr) { \
                ::GB_DIAGNOSTIC_NAMESPACE::Logger log(::GB_DIAGNOSTIC_NAMESPACE::LOG_LEVEL_##log_level); \
                log.GetLogStream() /*<< "("##__FILE__##", " << __LINE__ << "): "*/ << expr; \
            } GB_DIAGNOSTIC_DUMMY_FOR_SEMICOLON
#else
#    define DIAG_LOG(log_level, expr) GB_DIAGNOSTIC_DUMMY_FOR_SEMICOLON
#endif

#endif

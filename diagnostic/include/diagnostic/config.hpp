/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

/** Config flags
 */

#ifndef GB_DIAGNOSTIC_INCLUDE_GUARD_CONFIG_HPP_
#define GB_DIAGNOSTIC_INCLUDE_GUARD_CONFIG_HPP_

#ifndef GB_DIAGNOSTIC_CONF_ENABLE_LOGGING
#    define GB_DIAGNOSTIC_CONF_ENABLE_LOGGING
#endif

#ifndef GB_DIAGNOSTIC_CONF_ENABLE_DIAGNOSTIC_ASSERT
#    define GB_DIAGNOSTIC_CONF_ENABLE_DIAGNOSTIC_ASSERT
#endif

#ifndef GB_DIAGNOSTIC_CONF_ENABLE_DIAGNOSTIC_CHECK
#    define GB_DIAGNOSTIC_CONF_ENABLE_DIAGNOSTIC_CHECK
#endif

#ifndef GB_DIAGNOSTIC_CONF_THROW_ON_CHECK_FAILURE
#    define GB_DIAGNOSTIC_CONF_THROW_ON_CHECK_FAILURE
#endif

/** Global defines
 */
#define GB_DIAGNOSTIC_NAMESPACE Diagnostic

#define GB_DIAGNOSTIC_DUMMY_FOR_SEMICOLON ((void)0)

#endif

/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_DIAGNOSTIC_INCLUDE_GUARD_EXCEPTION_HPP_
#define GB_DIAGNOSTIC_INCLUDE_GUARD_EXCEPTION_HPP_

#include <diagnostic/config.hpp>
#include <diagnostic/exception_base.hpp>
#include <string>

namespace GB_DIAGNOSTIC_NAMESPACE {

    namespace Exceptions {

        class ParseError: public virtual Exception {
        public:
            virtual ~ParseError() throw();
            virtual char const* what() const throw();
        };

        class InvalidArgument: public virtual Exception {
        public:
            virtual ~InvalidArgument() throw();
            virtual char const* what() const throw();
        };

        class InvalidProtocol: public virtual Exception {
        public:
            virtual ~InvalidProtocol() throw();
            virtual char const* what() const throw();
        };

        class IOError: public virtual Exception {
        public:
            virtual ~IOError() throw();
            virtual char const* what() const throw();
        };

        class NotImplemented: public virtual Exception {
        public:
            virtual ~NotImplemented() throw();
            virtual char const* what() const throw();
        };

        class DiagnosticException: public virtual Exception_Base {
        public:
            virtual ~DiagnosticException() throw();
            virtual char const* what() const throw();
        };

        class AssertFail: public virtual DiagnosticException {
        private:
            std::string expr_;
        public:
            AssertFail(char const* expr) throw();
            virtual ~AssertFail() throw();
            virtual char const* what() const throw();
        };

        class CheckFail: public virtual DiagnosticException {
        private:
            std::string expr_;
        public:
            CheckFail(char const* expr) throw();
            virtual ~CheckFail() throw();
            virtual char const* what() const throw();
        };

    }

    namespace Exception_InfoTags {
        struct source_line_for_parse_error {};
        struct io_offset {};
    }

    namespace Exception_Info {
        typedef boost::errinfo_file_name file_name;
        typedef boost::error_info<Exception_InfoTags::source_line_for_parse_error, int> source_line_for_parse_error;
        typedef boost::error_info<Exception_InfoTags::io_offset, int> io_offset;
    }

    template<typename E_Info>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    GetExceptionInfo( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e )
    {
        return ::boost::get_error_info<E_Info, ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base>(e);
    }

    template<typename E_Info>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    DecorateException( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e )
    {
        typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type ret;
        ret = GetExceptionInfo<E_Info>(e);
        if(ret) {
            return ret;
        } else {
            typename E_Info::value_type new_decorator;
            e << E_Info( new_decorator );
            return GetExceptionInfo<E_Info>(e);
        }
    }

    template<typename E_Info>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    DecorateException( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e, typename E_Info::value_type const& decorator )
    {
        typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type ret;
        ret = GetExceptionInfo<E_Info>(e);
        if(ret) {
            *ret = decorator;
            return ret;
        } else {
            e << E_Info( decorator );
            return GetExceptionInfo<E_Info>(e);
        }
    }

    template<typename E_Info, typename T_Arg1>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    DecorateException( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e, T_Arg1 arg1 )
    {
        typename E_Info::value_type new_decorator(arg1);
        return DecorateException<E_Info>(e, new_decorator);
    }

    template<typename E_Info, typename T_Arg1, typename T_Arg2>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    DecorateException( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e, T_Arg1 arg1, T_Arg2 arg2 )
    {
        typename E_Info::value_type new_decorator(arg1, arg2);
        return DecorateException<E_Info>(e, new_decorator);
    }

    template<typename E_Info, typename T_Arg1, typename T_Arg2, typename T_Arg3>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    DecorateException( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e, T_Arg1 arg1, T_Arg2 arg2, T_Arg3 arg3 )
    {
        typename E_Info::value_type new_decorator(arg1, arg2, arg3);
        return DecorateException<E_Info>(e, new_decorator);
    }

    template<typename E_Info, typename T_Arg1, typename T_Arg2, typename T_Arg3, typename T_Arg4>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    DecorateException( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e, T_Arg1 arg1, T_Arg2 arg2, T_Arg3 arg3, T_Arg4 arg4 )
    {
        typename E_Info::value_type new_decorator(arg1, arg2, arg3, arg4);
        return DecorateException<E_Info>(e, new_decorator);
    }

    template<typename E_Info, typename T_Arg1, typename T_Arg2, typename T_Arg3, typename T_Arg4, typename T_Arg5>
    inline
    typename ::boost::exception_detail::get_error_info_return_type< ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base, typename E_Info::value_type >::type
    DecorateException( ::GB_DIAGNOSTIC_NAMESPACE::Exception_Base& e, T_Arg1 arg1, T_Arg2 arg2, T_Arg3 arg3, T_Arg4 arg4, T_Arg5 arg5 )
    {
        typename E_Info::value_type new_decorator(arg1, arg2, arg3, arg4, arg5);
        return DecorateException<E_Info>(e, new_decorator);
    }
}

#endif



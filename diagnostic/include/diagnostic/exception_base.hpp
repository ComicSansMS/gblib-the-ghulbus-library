/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_DIAGNOSTIC_INCLUDE_GUARD_EXCEPTION_BASE_HPP_
#define GB_DIAGNOSTIC_INCLUDE_GUARD_EXCEPTION_BASE_HPP_

#include <diagnostic/config.hpp>
#include <diagnostic/helper.hpp>

#include <exception>
#include <iosfwd>
#include <boost/exception/all.hpp>

namespace GB_DIAGNOSTIC_NAMESPACE {

    /** Base class for all exceptions
     * This is the base class for all decorateable exceptions.
     * @note Users should *not* derive from this class directly.
     * Instead, use Exception as base class for user
     * exceptions. On a related note, avoid catching Exception_Base
     * and only ever catch Exception instead.
     * The only reason to derive from Exception_Base is when you explicitly
     * do *not* want your exception to get caught by a user's catch block.
     * For example, the DiagnosticException class of exceptions derives
     * from Exception_Base instead of Exception, to ensure that a failing
     * assertion is not catched by user code, but is passed on to the debugger
     * instead.
     */
    class Exception_Base: public virtual ::std::exception, public virtual ::boost::exception {
    public:
        virtual ~Exception_Base() throw() {};
        virtual char const* what() const throw()=0;
    };

    /** Base class for exceptions
     */
    class Exception: public Exception_Base {
    public:
        virtual ~Exception() throw() {};
        virtual char const* what() const throw()=0;
    };

    /** Tags for exception decorators
     */
    namespace Exception_InfoTags {
        struct description { };
        struct location { };
    }

    /** Storage classes for exception decorators
     */
    namespace Exception_InfoRecords {
        class Location {
        private:
            std::string File_;
            std::string Func_;
            int Line_;
        public:
            Location();
            Location(char const* file, char const* func, int line);
            Location(Location const& rhs);
            Location& operator=(Location const& rhs);
            ~Location();
            char const* GetFile() const;
            char const* GetFunc() const;
            int GetLine() const;
        };
    }

    /** Exception info decorators
     */
    namespace Exception_Info {
        typedef boost::error_info<Exception_InfoTags::description, std::string> description;
        typedef boost::error_info<Exception_InfoTags::location, Exception_InfoRecords::Location> location;
    }
}

std::ostream& operator<<(std::ostream& os, GB_DIAGNOSTIC_NAMESPACE::Exception_InfoRecords::Location const& rhs);
#define DIAG_THROW_EXCEPTION(exc, str) BOOST_THROW_EXCEPTION(( (exc) << ::boost::error_info< ::GB_DIAGNOSTIC_NAMESPACE::Exception_InfoTags::description, ::std::string >(str) \
                                                                     << ::boost::error_info< ::GB_DIAGNOSTIC_NAMESPACE::Exception_InfoTags::location, ::GB_DIAGNOSTIC_NAMESPACE::Exception_InfoRecords::Location >( ::GB_DIAGNOSTIC_NAMESPACE::Exception_InfoRecords::Location(__FILE__, DIAG_HELPER_LOCATION_INFO_FUNCTION, __LINE__) ) \
                                                            ))

#endif

/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#if defined WIN32 && !defined _CRT_SECURE_NO_WARNINGS
#    define _CRT_SECURE_NO_WARNINGS
#endif

#include <diagnostic/log.hpp>
#include <diagnostic/assert.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <vector>
#include <algorithm>
#include <boost/shared_ptr.hpp>

namespace GB_DIAGNOSTIC_NAMESPACE {

    /** LogParams struct
     * Each log level is described by one LogParams
     */
    class LogParams {
    public:
        std::ostream* TargetStream;                    ///< target ostream for logging
        CustomLogTarget* CustomTarget;                ///< user-supplied custom log target
        bool includeTimestamp;                        ///< true iff log messages should include timestamps
        boost::shared_ptr<std::ofstream> LogFile;    ///< pointer to target ostream, iff is owned by the framework
        std::string LogFilename;                    ///< filename of target logfile to allow sharing between log levels
    public:
        /** Constructor
         */
        LogParams(): TargetStream(&std::cout), includeTimestamp(false)
        { }
    };

    /** Number of elements in LOG_LEVEL enum
     */
    static const int NUMBER_OF_LOG_LEVELS = LOG_LEVEL_CRITICAL+1;
    /** Pointer to parameter struct for each log level
     */
    static std::vector<LogParams> LOG_PARAMS(NUMBER_OF_LOG_LEVELS);

    /** Helper function: Get a string representation of the current time
     */
    std::string GetTimeString()
    {
        int const DATE_STRING_SIZE = sizeof("YYYY/MM/DD-HH:MM:SS");
        std::vector<char> ret;
        ret.resize(DATE_STRING_SIZE, '\0');
        std::time_t timestamp = std::time(0);
        std::tm* timeinfo = std::localtime(&timestamp);
        size_t res = std::strftime(&ret.front(), ret.size(), "%Y/%m/%d-%H:%M:%S", timeinfo);
        DIAG_ASSERT(res == DATE_STRING_SIZE-1);
        return std::string(&ret.front());
    }

    void LogSetLogfile(std::string const& filename)
    {
        boost::shared_ptr<std::ofstream> os(new std::ofstream(filename.c_str()));
        if(os->fail()) { DIAG_THROW_EXCEPTION(GB_DIAGNOSTIC_NAMESPACE::Exceptions::IOError(), "Unable to open log file"); }
        for(int i=0; i<NUMBER_OF_LOG_LEVELS; ++i) {
            LOG_PARAMS[i].TargetStream = os.get();
            LOG_PARAMS[i].LogFile = os;
            LOG_PARAMS[i].LogFilename = filename;
        }
    }

    void LogSetTarget(LOG_LEVEL log_level, std::ostream* os)
    {
        DIAG_ASSERT((log_level >= 0) && (log_level < NUMBER_OF_LOG_LEVELS));
#    ifdef GB_DIAGNOSTIC_CONF_ENABLE_LOGGING
        LOG_PARAMS[log_level].TargetStream = os;
        LOG_PARAMS[log_level].LogFile.reset();
        LOG_PARAMS[log_level].LogFilename.clear();
        LOG_PARAMS[log_level].CustomTarget = NULL;
#    endif
    }

    void LogSetTarget(LOG_LEVEL log_level, std::string const& filename)
    {
        DIAG_ASSERT((log_level >= 0) && (log_level < NUMBER_OF_LOG_LEVELS));
#    ifdef GB_DIAGNOSTIC_CONF_ENABLE_LOGGING
        boost::shared_ptr<std::ofstream> os;
        //check whether a logfile with filename was already opened
        std::vector<LogParams>::const_iterator it_end = LOG_PARAMS.end();
        std::vector<LogParams>::const_iterator it;
        for(it = LOG_PARAMS.begin(); it != it_end; ++it) {
            if((!it->LogFilename.empty()) && (it->LogFilename == filename)) { break; }
        }
        if(it == it_end) {
            //open new logfile
            os.reset(new std::ofstream(filename.c_str()));
            if(os->fail()) { DIAG_THROW_EXCEPTION(GB_DIAGNOSTIC_NAMESPACE::Exceptions::IOError(), "Unable to open log file"); }
        } else {
            //logfile already opened; share
            os = it->LogFile;
        }
        LOG_PARAMS[log_level].TargetStream = os.get();
        LOG_PARAMS[log_level].LogFile.swap(os);
        LOG_PARAMS[log_level].LogFilename = filename;
        LOG_PARAMS[log_level].CustomTarget = NULL;
#    endif
    }

    void LogSetTarget(LOG_LEVEL log_level, CustomLogTarget& target)
    {
        DIAG_ASSERT((log_level >= 0) && (log_level < NUMBER_OF_LOG_LEVELS));
#    ifdef GB_DIAGNOSTIC_CONF_ENABLE_LOGGING
        LOG_PARAMS[log_level].CustomTarget = &target;
        LOG_PARAMS[log_level].LogFile.reset();
        LOG_PARAMS[log_level].LogFilename.clear();
        LOG_PARAMS[log_level].TargetStream = NULL;
#    endif
    }

    void LogIncludeTimestamp(LOG_LEVEL log_level, bool include_timestamp)
    {
        DIAG_ASSERT((log_level >= 0) && (log_level < NUMBER_OF_LOG_LEVELS));
        LOG_PARAMS[log_level].includeTimestamp = include_timestamp;
    }

    void LogIncludeTimestamp(bool include_timestamp)
    {
        for(int i=0; i<NUMBER_OF_LOG_LEVELS; ++i) {
            LogIncludeTimestamp(static_cast<LOG_LEVEL>(i), include_timestamp);
        }
    }

    Logger::Logger()
        :level_(LOG_LEVEL_LOG)
    {
        Preamble();
    }

    Logger::Logger(LOG_LEVEL log_level)
        :level_(log_level)
    {
        DIAG_ASSERT((log_level >= 0) && (log_level < NUMBER_OF_LOG_LEVELS));
        Preamble();
    }

    Logger::~Logger()
    {
        if(LOG_PARAMS[level_].TargetStream) {
            (*LOG_PARAMS[level_].TargetStream) << sstr_.str() << std::endl;
        } else if(LOG_PARAMS[level_].CustomTarget) {
            LOG_PARAMS[level_].CustomTarget->Log(sstr_.str());
        }
    }

    void Logger::Preamble()
    {
        sstr_ << "[" << level_ << "] ";
        if(LOG_PARAMS[level_].includeTimestamp) {
            sstr_ << GetTimeString().c_str();
        }
        sstr_ << "  ";
    }

    std::ostream& Logger::GetLogStream()
    {
        return sstr_;
    }

    std::ostream& operator<<(std::ostream& os, LOG_LEVEL l)
    {
        switch(l) {
        case LOG_LEVEL_INFO:
            os << "INFO";
            break;
        case LOG_LEVEL_LOG:
            os << "LOG ";
            break;
        case LOG_LEVEL_WARNING:
            os << "WARN";
            break;
        case LOG_LEVEL_ERROR:
            os << "ERR ";
            break;
        case LOG_LEVEL_CRITICAL:
            os << "CRIT";
            break;
        default:
            os << "?";
            break;
        }
        return os;
    }

}


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <diagnostic/exception.hpp>
#include <sstream>

static std::string g_StaticErrStr;

namespace GB_DIAGNOSTIC_NAMESPACE {

namespace Exceptions {

InvalidArgument::~InvalidArgument() throw()
{
}
char const* InvalidArgument::what() const throw()
{
    return "Invalid Argument";
}


InvalidProtocol::~InvalidProtocol() throw()
{
}
char const* InvalidProtocol::what() const throw()
{
    return "Invalid Protocol";
}


IOError::~IOError() throw()
{
}
char const* IOError::what() const throw()
{
    return "I/O Error";
}


ParseError::~ParseError() throw()
{
}
char const* ParseError::what() const throw()
{
    return "Parse Error";
}


NotImplemented::~NotImplemented() throw()
{
}
char const* NotImplemented::what() const throw()
{
    return "Not Implemented";
}


DiagnosticException::~DiagnosticException() throw()
{
}
char const* DiagnosticException::what() const throw()
{
    return "Diagnostic Exception";
}


AssertFail::AssertFail(char const* expr) throw()
    :expr_(expr)
{
}
AssertFail::~AssertFail() throw()
{
}
char const* AssertFail::what() const throw()
{
    ::std::ostringstream sstr;
    sstr << "Assertion failed: '" << expr_ << "'";
    g_StaticErrStr = sstr.str();
    return g_StaticErrStr.c_str();
}


CheckFail::CheckFail(char const* expr) throw()
    :expr_(expr)
{
}
CheckFail::~CheckFail() throw()
{
}
char const* CheckFail::what() const throw()
{
    ::std::ostringstream sstr;
    sstr << "Check failed: '" << expr_ << "'";
    g_StaticErrStr = sstr.str();
    return g_StaticErrStr.c_str();
}

}

}

/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <diagnostic/exception_base.hpp>

namespace GB_DIAGNOSTIC_NAMESPACE {

namespace Exception_InfoRecords {
    Location::Location()
        :File_("<?unknown>"), Func_("<?unknown>"), Line_(-1)
    {
    }
    Location::Location(char const* file, char const* func, int line)
        :File_(file?(file):"<?unknown>"), Func_(func?func:"<?unknown>"), Line_(line)
    {
    }
    Location::Location(Location const& rhs)
        :File_(rhs.File_), Func_(rhs.Func_), Line_(rhs.Line_)
    {
    }
    Location& Location::operator=(Location const& rhs)
    {
        if(&rhs != this) {
            File_ = rhs.File_;
            Func_ = rhs.Func_;
            Line_ = rhs.Line_;
        }
        return *this;
    }
    Location::~Location()
    {
    }
    char const* Location::GetFile() const
    {
        return File_.c_str();
    }
    char const* Location::GetFunc() const
    {
        return Func_.c_str();
    }
    int Location::GetLine() const
    {
        return Line_;
    }
}

}

std::ostream& operator<<(std::ostream& os, GB_DIAGNOSTIC_NAMESPACE::Exception_InfoRecords::Location const& rhs)
{
    return os <<   " File:     " << rhs.GetFile() 
              << "\n Line:     " << rhs.GetLine() 
              << "\n Function: " << rhs.GetFunc();
}


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <diagnostic/assert.hpp>
#include <diagnostic/log.hpp>
#include <sstream>

namespace GB_DIAGNOSTIC_NAMESPACE {
    void assert_failed(char const* expr, char const* function, char const* file, unsigned long line, char const* description)
    {
        ::std::ostringstream sstr;
        sstr << "Assertion failed: '" << expr;
        if(description) { sstr << "(" << description << ")"; }
        sstr << "' in " 
             << (function?function:"<?unknown>") 
             << " (" << file << ", line " << line << ")";
        DIAG_LOG(CRITICAL, sstr.str());
        DIAG_THROW_EXCEPTION( Exceptions::AssertFail(expr), sstr.str() );
    }

    void check_failed(char const* expr, char const* function, char const* file, unsigned long line, char const* description)
    {
        ::std::ostringstream sstr;
        sstr << "Check failed: '" << expr;
        if(description) { sstr << "(" << description << ")"; }
        sstr << "' in " 
             << (function?function:"<?unknown>") 
             << " (" << file << ", line " << line << ")";
        DIAG_LOG(ERROR, sstr.str());
#ifdef GB_DIAGNOSTIC_CONF_THROW_ON_CHECK_FAILURE
        DIAG_THROW_EXCEPTION( Exceptions::CheckFail(expr), sstr.str() );
#endif
    }
}


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gtest/gtest.h>
#include <string>
#include <vector>
#include <diagnostic/diagnostic.hpp>

struct test_deco_tag {};

TEST(Exception, Decoration)
{
    using namespace Diagnostic;
    Exceptions::DiagnosticException e;
    std::string description("lorem ipsum");
    e << Exception_Info::description(description);
    EXPECT_EQ(description, *(GetExceptionInfo<Exception_Info::description>(e)));

    *(DecorateException<Exception_Info::description>(e)) = "foo";
    EXPECT_STREQ("foo", GetExceptionInfo<Exception_Info::description>(e)->c_str());

    *(DecorateException<Exception_Info::file_name>(e)) = "bar.txt";
    EXPECT_STREQ("bar.txt", GetExceptionInfo<Exception_Info::file_name>(e)->c_str());

    DecorateException<Exception_Info::location>(e, "foofile", "barfunc", 42);
    EXPECT_STREQ("foofile", GetExceptionInfo<Exception_Info::location>(e)->GetFile());
    EXPECT_STREQ("barfunc", GetExceptionInfo<Exception_Info::location>(e)->GetFunc());
    EXPECT_EQ(42, GetExceptionInfo<Exception_Info::location>(e)->GetLine());

    DecorateException<Exception_Info::location>(e, "barfile", "foofunc", 12345);
    EXPECT_STREQ("barfile", GetExceptionInfo<Exception_Info::location>(e)->GetFile());
    EXPECT_STREQ("foofunc", GetExceptionInfo<Exception_Info::location>(e)->GetFunc());
    EXPECT_EQ(12345, GetExceptionInfo<Exception_Info::location>(e)->GetLine());

    DecorateException<Exception_Info::io_offset>(e, 314159);
    EXPECT_EQ(314159, *(GetExceptionInfo<Exception_Info::io_offset>(e)));
    
    DecorateException< boost::error_info<test_deco_tag, std::vector<int> > >(e, 5);
    EXPECT_EQ(5u, (GetExceptionInfo< boost::error_info<test_deco_tag, std::vector<int> > >(e)->size()));

    std::vector<int> foo(1, 15);
    DecorateException< boost::error_info<test_deco_tag, std::vector<int> > >(e, foo);
    EXPECT_EQ(1u, (GetExceptionInfo< boost::error_info<test_deco_tag, std::vector<int> > >(e)->size()));
    EXPECT_EQ(15, (GetExceptionInfo< boost::error_info<test_deco_tag, std::vector<int> > >(e)->front()));


    DecorateException< boost::error_info<test_deco_tag, std::vector<int> > >(e, 3, 123);
    ASSERT_EQ(3u, (GetExceptionInfo< boost::error_info<test_deco_tag, std::vector<int> > >(e)->size()));
    EXPECT_EQ(123, (GetExceptionInfo< boost::error_info<test_deco_tag, std::vector<int> > >(e)->front()));
    EXPECT_EQ(123, *(GetExceptionInfo< boost::error_info<test_deco_tag, std::vector<int> > >(e)->begin() + 1));
    EXPECT_EQ(123, *(GetExceptionInfo< boost::error_info<test_deco_tag, std::vector<int> > >(e)->begin() + 2));

}

TEST(Exception, Throw)
{
    using namespace Diagnostic;
    EXPECT_THROW(DIAG_THROW_EXCEPTION(Exceptions::InvalidProtocol(), "foo"), Diagnostic::Exception);
    EXPECT_THROW(DIAG_THROW_EXCEPTION(Exceptions::DiagnosticException(), "foo"), Diagnostic::Exception_Base);
    try {
        DIAG_THROW_EXCEPTION(Exceptions::DiagnosticException(), "foo");
    } catch(Exceptions::DiagnosticException& e) {
        EXPECT_STREQ(e.what(), "Diagnostic Exception");
        EXPECT_STREQ( GetExceptionInfo<Exception_Info::location>(e)->GetFunc(),
                      ((static_cast<char const*>(DIAG_HELPER_LOCATION_INFO_FUNCTION)!=NULL)?DIAG_HELPER_LOCATION_INFO_FUNCTION:"<?unknown>"));
        EXPECT_STREQ( GetExceptionInfo<Exception_Info::location>(e)->GetFile(), __FILE__);
    }
    try {
        DIAG_THROW_EXCEPTION(Exceptions::DiagnosticException(), "");
    } catch(Exception&) {
        EXPECT_TRUE(false);
    } catch(std::exception&) {
    }
    try {
        DIAG_THROW_EXCEPTION(Exceptions::InvalidProtocol(), "");
    } catch(Exception&) {
    } catch(std::exception&) {
        EXPECT_TRUE(false);
    }
}

TEST(Exception_LocationInfo, Constructor)
{
    using namespace Diagnostic;
    {
        Exception_InfoRecords::Location loc;
        EXPECT_STREQ(loc.GetFile(), "<?unknown>");
        EXPECT_STREQ(loc.GetFunc(), "<?unknown>");
        EXPECT_EQ(loc.GetLine(), -1);
    }
    {
        Exception_InfoRecords::Location loc(NULL, NULL, 42);
        EXPECT_STREQ(loc.GetFile(), "<?unknown>");
        EXPECT_STREQ(loc.GetFunc(), "<?unknown>");
        EXPECT_EQ(loc.GetLine(), 42);
    }
    {
        Exception_InfoRecords::Location loc("foo", NULL, 1);
        EXPECT_STREQ(loc.GetFile(), "foo");
        EXPECT_STREQ(loc.GetFunc(), "<?unknown>");
        EXPECT_EQ(loc.GetLine(), 1);
    }
    {
        Exception_InfoRecords::Location loc(NULL, "foo", 2);
        EXPECT_STREQ(loc.GetFile(), "<?unknown>");
        EXPECT_STREQ(loc.GetFunc(), "foo");
        EXPECT_EQ(loc.GetLine(), 2);
    }
    {
        Exception_InfoRecords::Location loc("foo", "bar", 3);
        EXPECT_STREQ(loc.GetFile(), "foo");
        EXPECT_STREQ(loc.GetFunc(), "bar");
        EXPECT_EQ(loc.GetLine(), 3);
    }
}

TEST(Exception_LocationInfo, CopyConstructor)
{
    using namespace Diagnostic;
    {
        Exception_InfoRecords::Location loc;
        Exception_InfoRecords::Location copy(loc);
        EXPECT_STREQ(loc.GetFile(), copy.GetFile());
        EXPECT_STREQ(loc.GetFunc(), copy.GetFunc());
        EXPECT_EQ(loc.GetLine(), copy.GetLine());
    }
    {
        Exception_InfoRecords::Location loc("foo", "bar", 42);
        Exception_InfoRecords::Location copy(loc);
        EXPECT_STREQ(loc.GetFile(), copy.GetFile());
        EXPECT_STREQ(loc.GetFunc(), copy.GetFunc());
        EXPECT_EQ(loc.GetLine(), copy.GetLine());
    }
}

TEST(Exception_LocationInfo, Assignment)
{
    using namespace Diagnostic;
    {
        Exception_InfoRecords::Location loc;
        EXPECT_STREQ(loc.GetFile(), "<?unknown>");
        EXPECT_STREQ(loc.GetFunc(), "<?unknown>");
        EXPECT_EQ(loc.GetLine(), -1);
        Exception_InfoRecords::Location loc2("foo", "bar", 42);
        loc = loc2;
        EXPECT_STREQ(loc.GetFile(), loc2.GetFile());
        EXPECT_STREQ(loc.GetFunc(), loc2.GetFunc());
        EXPECT_EQ(loc.GetLine(), loc2.GetLine());
    }
}

TEST(Assert, Throw)
{
    Diagnostic::LogSetTarget(Diagnostic::LOG_LEVEL_CRITICAL, NULL);
    ASSERT_THROW(DIAG_ASSERT(false), Diagnostic::Exceptions::AssertFail);
    ASSERT_NO_THROW(DIAG_ASSERT(true));
}

TEST(Check, Throw)
{
#ifdef GB_DIAGNOSTIC_CONF_THROW_ON_CHECK_FAILURE
    Diagnostic::LogSetTarget(Diagnostic::LOG_LEVEL_ERROR, NULL);
    ASSERT_THROW(DIAG_CHECK(false), Diagnostic::Exceptions::CheckFail);
    ASSERT_NO_THROW(DIAG_CHECK(true));
#endif
}

class UltimateTestLogTarget: public Diagnostic::CustomLogTarget {
private:
    std::string str_;
    bool was_called;
public:
    void ExpectString(std::string const& str) {
        str_ = str;
        was_called = false;
    }
    void Log(std::string const& msg) {
        EXPECT_STREQ(str_.c_str(), msg.c_str());
        was_called = true;
    }
    bool WasCalled() const {
        return was_called;
    }
};

TEST(Log, CustomLogTarget)
{
    UltimateTestLogTarget target;
    Diagnostic::LogSetTarget(Diagnostic::LOG_LEVEL_INFO, target);
    Diagnostic::LogIncludeTimestamp(Diagnostic::LOG_LEVEL_INFO, false);
    target.ExpectString("[INFO]   Look ma, I'm logging!");
    EXPECT_FALSE(target.WasCalled());
    DIAG_LOG(INFO, "Look ma, I'm logging!");
    EXPECT_TRUE(target.WasCalled());
}


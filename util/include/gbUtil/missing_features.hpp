/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_UTIL_INCLUDE_GUARD_MISSING_FEATURES_HPP_
#define GB_UTIL_INCLUDE_GUARD_MISSING_FEATURES_HPP_

#include <gbUtil/config.hpp>
#include <gbUtil/cxx11_features.hpp>
#include <memory>

#ifdef GB_UTIL_CONF_DONT_USE_NAMESPACE_STD
#    define GB_UTIL_FIXES_NAMESPACE GB_UTIL_NAMESPACE
#else
#    define GB_UTIL_FIXES_NAMESPACE std
#endif

#ifdef _MSC_VER
#   if _MSC_VER >= 1800
#       define GB_UTIL_CONFIG_NO_CBEGIN_CEND
#       define GB_UTIL_CONFIG_NO_MAKE_UNIQUE
#   endif
#endif

#ifndef GB_UTIL_CONFIG_NO_CBEGIN_CEND
#    ifdef HAS_CXX11_AUTO_RET_TYPE
        namespace GB_UTIL_FIXES_NAMESPACE {
            template<class C> auto cbegin(C const& c) -> decltype(c.cbegin()) { return c.cbegin(); }
            template<class C> auto cend(C const& c)   -> decltype(c.cend())   { return c.cend(); }
        }
#    endif
#endif

#ifndef GB_UTIL_CONFIG_NO_MAKE_UNIQUE
    namespace GB_UTIL_FIXES_NAMESPACE {
#    ifdef HAS_CXX11_VARIADIC_TEMPLATES
        template<typename T, typename... Args>
        std::unique_ptr<T> make_unique(Args&&... args) {
            return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
        }
#    else
        template<typename T> 
        std::unique_ptr<T> make_unique() {
            return std::unique_ptr<T>(new T());
        }
        template<typename T, typename Arg1>
        std::unique_ptr<T> make_unique(Arg1&& arg1) {
            return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1)));
        }
        template<typename T, typename Arg1, typename Arg2>
        std::unique_ptr<T> make_unique(Arg1&& arg1, Arg2&& arg2) {
            return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2)));
        }
        template<typename T, typename Arg1, typename Arg2, typename Arg3>
        std::unique_ptr<T> make_unique(Arg1&& arg1, Arg2&& arg2, Arg3&& arg3) {
            return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2), std::forward<Arg3>(arg3)));
        }
        template<typename T, typename Arg1, typename Arg2, typename Arg3, typename Arg4>
        std::unique_ptr<T> make_unique(Arg1&& arg1, Arg2&& arg2, Arg3&& arg3, Arg4&& arg4) {
            return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2), std::forward<Arg3>(arg3), std::forward<Arg4>(arg4)));
        }
        template<typename T, typename Arg1, typename Arg2, typename Arg3, typename Arg4, typename Arg5>
        std::unique_ptr<T> make_unique(Arg1&& arg1, Arg2&& arg2, Arg3&& arg3, Arg4&& arg4, Arg5&& arg5) {
            return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2), std::forward<Arg3>(arg3), std::forward<Arg4>(arg4), std::forward<Arg5>(arg5)));
        }
        template<typename T, typename Arg1, typename Arg2, typename Arg3, typename Arg4, typename Arg5, typename Arg6>
        std::unique_ptr<T> make_unique(Arg1&& arg1, Arg2&& arg2, Arg3&& arg3, Arg4&& arg4, Arg5&& arg5, Arg6&& arg6) {
            return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2), std::forward<Arg3>(arg3), std::forward<Arg4>(arg4), std::forward<Arg5>(arg5), std::forward<Arg6>(arg6)));
        }
        template<typename T, typename Arg1, typename Arg2, typename Arg3, typename Arg4, typename Arg5, typename Arg6, typename Arg7>
        std::unique_ptr<T> make_unique(Arg1&& arg1, Arg2&& arg2, Arg3&& arg3, Arg4&& arg4, Arg5&& arg5, Arg6&& arg6, Arg7&& arg7) {
            return std::unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2), std::forward<Arg3>(arg3), std::forward<Arg4>(arg4), std::forward<Arg5>(arg5), std::forward<Arg6>(arg6), std::forward<Arg7>(arg7)));
        }
#    endif
}
#endif

#endif

/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_UTIL_INCLUDE_GUARD_RESOURCE_GUARDIAN_HPP_
#define GB_UTIL_INCLUDE_GUARD_RESOURCE_GUARDIAN_HPP_

#include <gbUtil/config.hpp>

#include <algorithm>
#include <functional>
#include <iterator>
#include <memory>
#include <type_traits>
#include <vector>

namespace GB_UTIL_NAMESPACE {

    template<typename T> class gbResourceGuardian;
    template<typename T> class gbSupervisedResource;

    template<typename T>
    class gbResourceGuardian {
    private:
        std::vector<T*> m_ResourceList;
    public:
        std::shared_ptr<T> CreateResource() {
            auto ret = std::make_shared<T>();
            m_ResourceList.push_back(ret.get());
            return ret;
        }
        template<typename Arg_T>
        std::shared_ptr<T> CreateResource(Arg_T&& arg1) {
            auto ret = std::make_shared<T>(std::forward<Arg_T>(arg1));
            m_ResourceList.push_back(ret.get());
            return ret;
        }
        template<typename Arg1_T, typename Arg2_T>
        std::shared_ptr<T> CreateResource(Arg1_T&& arg1, Arg2_T&& arg2) {
            auto ret = std::make_shared<T>(std::forward<Arg1_T>(arg1), std::forward<Arg2_T>(arg2));
            m_ResourceList.push_back(ret.get());
            return ret;
        }
        template<typename Arg1_T, typename Arg2_T, typename Arg3_T>
        std::shared_ptr<T> CreateResource(Arg1_T&& arg1, Arg2_T&& arg2, Arg3_T&& arg3) {
            auto ret = std::make_shared<T>(std::forward<Arg1_T>(arg1), std::forward<Arg2_T>(arg2),
                                           std::forward<Arg3_T>(arg3));
            m_ResourceList.push_back(ret.get());
            return ret;
        }
        template<typename Arg1_T, typename Arg2_T, typename Arg3_T, typename Arg4_T>
        std::shared_ptr<T> CreateResource(Arg1_T&& arg1, Arg2_T&& arg2, Arg3_T&& arg3, Arg4_T&& arg4) {
            auto ret = std::make_shared<T>(std::forward<Arg1_T>(arg1), std::forward<Arg2_T>(arg2),
                                           std::forward<Arg3_T>(arg3), std::forward<Arg4_T>(arg4));
            m_ResourceList.push_back(ret.get());
            return ret;
        }
        template<typename Arg1_T, typename Arg2_T, typename Arg3_T, typename Arg4_T, typename Arg5_T>
        std::shared_ptr<T> CreateResource(Arg1_T&& arg1, Arg2_T&& arg2, Arg3_T&& arg3, Arg4_T&& arg4, Arg5_T&& arg5) {
            auto ret = std::make_shared<T>(std::forward<Arg1_T>(arg1), std::forward<Arg2_T>(arg2),
                                           std::forward<Arg3_T>(arg3), std::forward<Arg4_T>(arg4),
                                           std::forward<Arg5_T>(arg5));
            m_ResourceList.push_back(ret.get());
            return ret;
        }
        template<typename Arg1_T, typename Arg2_T, typename Arg3_T, typename Arg4_T, typename Arg5_T, typename Arg6_T>
        std::shared_ptr<T> CreateResource(Arg1_T&& arg1, Arg2_T&& arg2, Arg3_T&& arg3, Arg4_T&& arg4, Arg5_T&& arg5, Arg6_T&& arg6) {
            auto ret = std::make_shared<T>(std::forward<Arg1_T>(arg1), std::forward<Arg2_T>(arg2),
                                           std::forward<Arg3_T>(arg3), std::forward<Arg4_T>(arg4),
                                           std::forward<Arg5_T>(arg5), std::forward<Arg6_T>(arg6));
            m_ResourceList.push_back(ret.get());
            return ret;
        }
        std::vector<T*> GetResources() {
            return m_ResourceList;
        }
        void Apply(std::function<void(T&)> const& fun)
        {
            std::for_each(begin(m_ResourceList), end(m_ResourceList), [&fun](T* ptr) {
                fun(*ptr);
            });
        }
        void ReportDestruction(gbSupervisedResource<T>* res) {
            m_ResourceList.erase(
                std::remove_if(begin(m_ResourceList), end(m_ResourceList),
                    [res](T* ptr) -> bool {
                        return (static_cast<gbSupervisedResource<T>*>(ptr) == res);
                    }),
                m_ResourceList.end());
        }
    };

    template<typename T>
    class gbSupervisedResource {
    private:
        gbResourceGuardian<T>* ResourceGuardian_;
    public:
        gbSupervisedResource(gbResourceGuardian<T>* guardian): ResourceGuardian_(guardian) {
            static_assert( std::is_base_of<gbSupervisedResource<T>, T>::value,
                           "gbSupervisedResource must be only be instantiated on types that inherit from it" );
        }
        virtual ~gbSupervisedResource() {
            if(ResourceGuardian_) {
                ResourceGuardian_->ReportDestruction(this);
            }
        }
    };
}

#endif

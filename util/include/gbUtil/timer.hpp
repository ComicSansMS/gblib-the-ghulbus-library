/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_UTIL_INCLUDE_GUARD_TIMER_HPP_
#define GB_UTIL_INCLUDE_GUARD_TIMER_HPP_

#include <gbUtil/config.hpp>
#include <gbUtil/cxx11_features.hpp>

#include <cstdint>
#if defined HAS_CXX11_LIB_CHRONO && defined HAS_CXX11_LIB_THREAD
#    include <chrono>
#    include <thread>
#    define LIB_CHRONO_NAMESPACE std
#    define LIB_THREAD_NAMESPACE std
#else
#    include <boost/chrono.hpp>
#    include <boost/thread.hpp>
#    define LIB_CHRONO_NAMESPACE boost
#    define LIB_THREAD_NAMESPACE boost
#endif


namespace GB_UTIL_NAMESPACE {

    /** Timestamp
     */
    typedef uint64_t Timestamp;

    class gbTimer {
    private:
        typedef LIB_CHRONO_NAMESPACE::chrono::steady_clock Clock;
        Clock::time_point m_Current;        ///< Timestamp from the most recent call to Mark()
        Clock::time_point m_Last;            ///< Timestamp from the next-to-last call to Mark()
    public:
        gbTimer()
        {
            m_Current = m_Last = Clock::now();
        }
        /** Get an absolute timestamp
         * @return Absolute timestamp with milisecond granularity
         */
        Timestamp GetTimestamp() const {
            return toMilliseconds(Clock::now().time_since_epoch());
        }
        /** Suspend execution for at least the given amount of time
         * @param[in] Time to sleep in miliseconds
         */
        void Sleep(Timestamp const& time) const {
            return LIB_THREAD_NAMESPACE::this_thread::sleep_for(LIB_CHRONO_NAMESPACE::chrono::milliseconds(time));
        }
        /** Call this once every frame
         */
        void Mark() {
            std::swap(m_Last, m_Current);
            m_Current = Clock::now();
        }
        /** Get the time passed between the last two calls to Mark()
         * @return The elapsed time in miliseconds
         */
        Timestamp GetDelta() {
            return toMilliseconds(m_Current - m_Last);
        }
        /** Get the frames per second
         * @return Frames per second
         */
        double GetFPS() {
            if(m_Current == m_Last) { return 0.0; }
            return 1000.0 / static_cast<double>(toMilliseconds(m_Current - m_Last));
        }
        /** Get the time passed since the last call to Mark()
         * @return The elapsed time in miliseconds
         */
        Timestamp PeekTime() {
            return toMilliseconds(Clock::now() - m_Current);
        }
    private:
        inline Timestamp toMilliseconds(Clock::duration const& dur) const {
            return LIB_CHRONO_NAMESPACE::chrono::duration_cast<LIB_CHRONO_NAMESPACE::chrono::milliseconds>(dur).count();
        }
    };

}

#undef LIB_CHRONO_NAMESPACE
#undef LIB_THREAD_NAMESPACE

#endif

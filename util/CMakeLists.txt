
project("gbUtil")

cmake_minimum_required(VERSION 2.8.11)

if(NOT gbLib_VERSION)
  message(STATUS "Building gbLib Util standalone.")
  set(gbLib_UTIL_STANDALONE TRUE)
  # find cmake script directory
  find_path(gbLib_CMAKE_MODULE_DIR 
    gbLib.cmake
    PATHS "${PROJECT_SOURCE_DIR}" "${PROJECT_SOURCE_DIR}/.." $ENV{GBLIB_ROOT}
    PATH_SUFFIXES cmake
    NO_DEFAULT_PATH)
  if(NOT gbLib_CMAKE_MODULE_DIR)
    message(FATAL_ERROR "gbLib cmake module directory not found")
  endif()
  list(APPEND CMAKE_MODULE_PATH ${gbLib_CMAKE_MODULE_DIR})
  include(gbLib)
endif()

gbLib_SetVersionNumber(gbLib_UTIL 0 1 0)

# dependencies
find_package(Boost REQUIRED chrono system thread)

# sources
set(gbLib_UTIL_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)
set(gbLib_UTIL_SRC_DIR ${PROJECT_SOURCE_DIR}/src)
set(gbLib_UTIL_TEST_SRC_DIR ${PROJECT_SOURCE_DIR}/test)

include(cmake/CheckCXX11Features.cmake)
configure_file(
  "${gbLib_UTIL_INCLUDE_DIR}/gbUtil/cxx11_features.hpp.in"
  "${PROJECT_BINARY_DIR}/include/gbUtil/cxx11_features.hpp")

if(NOT ${HAS_CXX11_LIB_CHRONO} OR NOT ${HAS_CXX11_LIB_THREAD})
  list(APPEND gbLib_UTIL_ADDITIONAL_LIBRARIES ${Boost_CHRONO_LIBRARIES} ${Boost_SYSTEM_LIBRARIES} ${Boost_THREAD_LIBRARIES})
  gbLib_getDllFromLib(BOOST_CHRONO_DLLS "${Boost_CHRONO_LIBRARIES}" "")
  gbLib_getDllFromLib(BOOST_SYSTEM_DLLS "${Boost_SYSTEM_LIBRARIES}" "")
  gbLib_getDllFromLib(BOOST_THREAD_DLLS "${Boost_THREAD_LIBRARIES}" "")
  list(APPEND gbLib_UTIL_ADDITIONAL_DLLS ${BOOST_CHRONO_DLLS} ${BOOST_SYSTEM_DLLS} ${BOOST_THREAD_DLLS})
endif()

set(gbLib_UTIL_HEADERS
  ${gbLib_UTIL_INCLUDE_DIR}/gbUtil/config.hpp
  ${PROJECT_BINARY_DIR}/include/gbUtil/cxx11_features.hpp
  ${gbLib_UTIL_INCLUDE_DIR}/gbUtil/command_line.hpp
  ${gbLib_UTIL_INCLUDE_DIR}/gbUtil/missing_features.hpp
  ${gbLib_UTIL_INCLUDE_DIR}/gbUtil/resource_guardian.hpp
  ${gbLib_UTIL_INCLUDE_DIR}/gbUtil/resource_observer.hpp
  ${gbLib_UTIL_INCLUDE_DIR}/gbUtil/timer.hpp
  ${gbLib_UTIL_INCLUDE_DIR}/gbUtil/util.hpp
)

source_group(gbUtil FILES ${gbLib_UTIL_HEADERS})

set(gbLib_UTIL_TEST_SOURCES
  ${gbLib_UTIL_TEST_SRC_DIR}/util.t.cpp
  ${gbLib_UTIL_TEST_SRC_DIR}/timer.t.cpp
)

# targets
gbLib_AddLibrary(gbUtil STATIC
  HEADERS ${gbLib_UTIL_HEADERS}
  SOURCES ${PROJECT_SOURCE_DIR}/src/dummy.cpp
  TESTS ${gbLib_UTIL_TEST_SOURCES}
  PUBLIC_INCLUDE_DIRECTORIES ${gbLib_UTIL_INCLUDE_DIR} "${PROJECT_BINARY_DIR}/include"
  DEPENDENCIES diagnostic
  LIBRARIES ${gbLib_UTIL_ADDITIONAL_LIBRARIES}
  DLLS ${gbLib_UTIL_ADDITIONAL_DLLS}
)  

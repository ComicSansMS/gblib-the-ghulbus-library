#include <chrono>

int main()
{
    auto timestamp = std::chrono::steady_clock::now();
    auto msecs = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - timestamp);
    return 0;
}

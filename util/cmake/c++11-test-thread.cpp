#include <thread>

void some_fun()
{
}

int main()
{
    std::thread t(some_fun);
    t.join();
    return 0;
}

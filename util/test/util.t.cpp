/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gtest/gtest.h>

#include <gbUtil/util.hpp>
#include <gbUtil/resource_observer.hpp>

#include <algorithm>
#include <memory>
#include <vector>

TEST(gbUtil, ResourceManager)
{
    using namespace GhulbusUtil;
    gbResourceObserver<int> obs;
    auto ptr = obs.CreateResource(42);
    EXPECT_EQ(42, *ptr);
    {
        auto ptr2 = obs.CreateResource();
        EXPECT_EQ(2u, obs.GetResources().size());
    }
    EXPECT_EQ(1u, obs.GetResources().size());
}

TEST(gbUtil, Fixes)
{
    std::vector<int> v1;
    v1.push_back(1);
    v1.push_back(2);
    v1.push_back(3);

    std::vector<int> v2;
    std::copy(cbegin(v1), cend(v1), std::back_inserter(v2));
    EXPECT_TRUE(v1 == v2);

    auto ptr_i = std::make_unique<int>(42);
    EXPECT_EQ(42, *ptr_i);
}


/******************************************************************************
 * Copyright (c) 2011-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/
#include <gtest/gtest.h>

#include <gbUtil/timer.hpp>

TEST(gbUtil, Timer)
{
    using namespace GhulbusUtil;
    gbTimer timer;
    Timestamp ts = timer.GetTimestamp();
    
    EXPECT_EQ(0u, timer.GetDelta());
    EXPECT_EQ(0.0f, timer.GetFPS());
    EXPECT_GE(0u, timer.PeekTime());
    EXPECT_GE(timer.GetTimestamp(), ts);
    Timestamp const sleep_time = 10;
    timer.Mark();
    timer.Sleep(sleep_time);
    timer.Mark();
    EXPECT_GE(timer.GetDelta(), sleep_time);
    EXPECT_LE(timer.GetFPS(), 100.0f);
    EXPECT_GE(timer.PeekTime(), 0u);
}


/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_SOUND_BUFFER_OAL_HPP_
#define GB_AUDIO_INCLUDE_GUARD_SOUND_BUFFER_OAL_HPP_

#ifndef GB_AUDIO_INCLUDE_TOKEN_SOUND_BUFFER_OAL_HPP_
#    error Do not include sound_buffer_OAL.hpp directly; Include sound_buffer.hpp instead
#endif

#include <al.h>

namespace GB_AUDIO_NAMESPACE {

    class gbSoundBuffer_OAL: public gbSoundBuffer {
        ALuint m_Buffer;            ///< OpenAL buffer id
    public:
        /** Constructor
         */
        gbSoundBuffer_OAL();
        /** Destructor
         */
        ~gbSoundBuffer_OAL();
        /** @name gbSoundBuffer
         * @{
         */
        void SetSoundData(gbSoundDataVariant const& data);
        /// @}
        /** Get the id of the underlying OpenAL buffer
         * @return OpenAL buffer id of the associated buffer
         * @attention This function breaks encapsulation. Use with care!
         */
        ALuint OAL_GetBufferId();
    };

}

#endif

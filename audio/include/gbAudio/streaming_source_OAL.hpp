/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_STREAMING_SOURCE_OAL_HPP_
#define GB_AUDIO_INCLUDE_GUARD_STREAMING_SOURCE_OAL_HPP_

#ifndef GB_AUDIO_INCLUDE_TOKEN_STREAMING_SOURCE_OAL_HPP_
#    error Do not include streaming_source_OAL.hpp directly; Include streaming_source.hpp instead
#endif

#define GB_AUDIO_INCLUDE_TOKEN_SOURCE_BASE_OAL_HPP_
#    include <gbAudio/source_base_OAL.hpp>
#undef GB_AUDIO_INCLUDE_TOKEN_SOURCE_BASE_OAL_HPP_

#include <deque>
#include <tuple>
#include <vector>

#include <al.h>

namespace GB_AUDIO_NAMESPACE {

    class gbStreamingSource_OAL: public gbStreamingSource, public GB_UTIL_NAMESPACE::gbSupervisedResource<gbStreamingSource_OAL> {
    private:
        struct SoundDataInfo {
            ALenum format;
            int sample_size;
            int sampling_frequency;
        };
        typedef std::tuple<gbSoundDataVariant*, BufferProcessedCallback, std::unique_ptr<gbSoundDataVariant>> DataQueueEntry;
    private:
        std::vector<ALuint> m_Buffers;
        std::vector<ALuint> m_FreeBuffers;
        ALuint m_Source;
        int m_BufferSize;
        gbSoundDataVariant const* m_SoundData;
        std::deque<DataQueueEntry> m_SoundDataQueue;
        int m_DataIndex;
        SoundDataInfo m_DataInfo;
        bool m_DoLoop;
        PlayState_T m_PlayState;
        gbSourceBase_OAL m_Base;
    public:
        gbStreamingSource_OAL(GB_UTIL_NAMESPACE::gbResourceGuardian<gbStreamingSource_OAL>* guardian);
        ~gbStreamingSource_OAL();
        /** @name Locatable
         * @{
         */
        void SetPosition(GB_MATH_NAMESPACE::Vector3f const& p);
        GB_MATH_NAMESPACE::Vector3f GetPosition() const;
        void SetVelocity(GB_MATH_NAMESPACE::Vector3f const& v);
        GB_MATH_NAMESPACE::Vector3f GetVelocity() const;
        /// @}
        /** @name gbStreamingSource
         * @{
         */
        void SetNumberOfBuffers(int n);
        void SetBufferSize(int size);
        void EnqueueSoundData(gbSoundDataVariant& data);
        void EnqueueSoundData(gbSoundDataVariant& data, BufferProcessedCallback fun_processed);
        void ClaimSoundData(gbSoundDataVariant data);
        void ClaimSoundData(gbSoundDataVariant data, BufferProcessedCallback fun_processed);
        void ClearSoundData();
        int QueueSize() const;
        int UnplayedSamples() const;
        /// @}
        /** @name Playable
         * @{
         */
        void Play();
        void Pause();
        void Stop();
        void Rewind();
        PlayState_T GetPlayState() const;
        void Loop(bool do_loop);
        bool IsLooping() const;
        /// @}
        /** @name WindUpSound
         * @{
         */
        void WindUp();
        /// @}
    private:
        void SetSoundDataInfo();
        void CheckSoundData(gbSoundDataVariant const& data);
        void FillSoundBuffers(int nbuffers);
        std::vector<char> BuildTransitionalBuffer(int* nsamples);
        void AdvanceQueue();
    };

}

#endif

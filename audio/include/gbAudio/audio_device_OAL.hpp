/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_AUDIO_DEVICE_OAL_HPP_
#define GB_AUDIO_INCLUDE_GUARD_AUDIO_DEVICE_OAL_HPP_

#ifndef GB_AUDIO_INCLUDE_TOKEN_AUDIO_DEVICE_OAL_HPP_
#    error Do not include audio_device_OAL.hpp directly; Include audio_device.hpp instead
#endif

#include <functional>
#include <gbUtil/resource_guardian.hpp>
#include <gbUtil/resource_observer.hpp>

#include <al.h>
#include <alc.h>

namespace GB_AUDIO_NAMESPACE {

    class gbSoundBuffer_OAL;
    class gbSource_OAL;
    class gbStreamingSource_OAL;

    class gbAudioDevice_OAL: public gbAudioDevice {
    private:
        typedef std::unique_ptr<ALCdevice, std::function<void(ALCdevice*)>> Guard_ALCdevice_T;
        typedef std::unique_ptr<ALCcontext, std::function<void(ALCcontext*)>> Guard_ALCcontext_T;
    private:
        ALCdevice* m_Device;
        ALCcontext* m_Context;
        bool m_HasXRam;
        Guard_ALCdevice_T m_GuardDevice;
        Guard_ALCcontext_T m_GuardContext;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbSoundBuffer_OAL> m_Buffers;
        GB_UTIL_NAMESPACE::gbResourceObserver<gbSource_OAL> m_Sources;
        GB_UTIL_NAMESPACE::gbResourceGuardian<gbStreamingSource_OAL> m_StreamingSources;
        gbListenerPtr m_Listener;
    public:
        gbAudioDevice_OAL(DeviceIdentifier const& device_id);
        ~gbAudioDevice_OAL();
        /** @name gbAudioDevice
         * @{
         */
        std::vector<CHANNEL_FORMAT_TYPE> GetSupportedChannelFormats() const;
        gbSoundBufferPtr CreateSoundBuffer();
        gbSourcePtr CreateSource();
        gbStreamingSourcePtr CreateStreamingSource();
        gbListenerPtr GetListener();
        void Update();
        /// @}
        static std::vector<DeviceIdentifier> EnumerateDevices_OAL();
        /** Get the total amount of X-Ram
         * @return Total size of X-Ram in bytes
         */
        int OAL_GetXRamSize() const;
        /** Get the amount of free X-Ram
         * @return Size of available X-Ram in bytes
         */
        int OAL_GetXRamFreeSize() const;
    private:
        /** Init the X-Ram OpenAL extension if available
         */
        void InitXRam();
        /** Init the multi-channel buffer OpenAL extension if available
         */
        void InitMultiChannelBuffers();
    };

}

#endif

/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_SOURCE_BASE_OAL_HPP_
#define GB_AUDIO_INCLUDE_GUARD_SOURCE_BASE_OAL_HPP_

#ifndef GB_AUDIO_INCLUDE_TOKEN_SOURCE_BASE_OAL_HPP_
#    error Do not include source_base_OAL.hpp directly; Include source.hpp instead
#endif

#include <gbAudio/locatable.hpp>
#include <al.h>

namespace GB_AUDIO_NAMESPACE {

    /** Delegate class implementing Locatable for gbSource_OAL and gbStreamingSource_OAL
     */
    class gbSourceBase_OAL: public Locatable {
    private:
        ALuint m_Source;            ///< OpenAL source id
    public:
        /** Constructor
         */
        gbSourceBase_OAL();
        /** Set the underlying source id
         * @note The gbSource*_OAL only aquires the source id *after*
         *       constructing the gbSourceBase_OAL
         */
        void OAL_SetSource(int source_id);
        /** @name Locatable
         * @{
         */
        void SetPosition(GB_MATH_NAMESPACE::Vector3f const& p);
        GB_MATH_NAMESPACE::Vector3f GetPosition() const;
        void SetVelocity(GB_MATH_NAMESPACE::Vector3f const& v);
        GB_MATH_NAMESPACE::Vector3f GetVelocity() const;
        /// @}
    };

}

#endif

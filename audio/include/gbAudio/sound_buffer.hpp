/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_SOUND_BUFFER_HPP_
#define GB_AUDIO_INCLUDE_GUARD_SOUND_BUFFER_HPP_

#include <gbAudio/config.hpp>
#include <gbAudio/sound_data_fwd.hpp>

#include <memory>
#include <boost/utility.hpp>

namespace GB_AUDIO_NAMESPACE {

    class gbSoundBuffer;
    typedef std::shared_ptr<gbSoundBuffer> gbSoundBufferPtr;

    /** Buffer for sound playback through gbSource
     * The gbSoundBuffer represents the hardware buffer used for binding
     * static sound data to the gbSource. While gbSoundDataVariant
     * is a general container for sound data in main memory, gbSoundBuffer
     * represents data in device memory of the sound card.
     * Since device memory is very limited, gbSoundBuffer may not be the
     * best choice for large amounts of sound data.
     * Check out gbStreamingSource for a more flexible alternative.
     */
    class gbSoundBuffer: public boost::noncopyable {
    public:
        /** Destructor
         */
        virtual ~gbSoundBuffer() {};
        /** Set the sound data for the buffer
         * @param[in] data New sound data. This is copied into the hardware
         *                 buffer, so the original gbSoundDataVariant may
         *                 be destroyed by the user after calling.
         * @throw InvalidProtocol if sound buffer could not be created
         */
        virtual void SetSoundData(gbSoundDataVariant const& data)=0;
    };

}

#ifdef GB_AUDIO_HAS_OPENAL
#    define GB_AUDIO_INCLUDE_TOKEN_SOUND_BUFFER_OAL_HPP_
#    include <gbAudio/sound_buffer_OAL.hpp>
#    undef GB_AUDIO_INCLUDE_TOKEN_SOUND_BUFFER_OAL_HPP_
#endif

#endif

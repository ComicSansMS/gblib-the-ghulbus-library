/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_SOUND_DATA_FWD_HPP_
#define GB_AUDIO_INCLUDE_GUARD_SOUND_DATA_FWD_HPP_

#include <gbAudio/config.hpp>
#include <boost/variant/variant_fwd.hpp>

namespace GB_AUDIO_NAMESPACE {

    /*
     * Forward declarations for all gbSoundData types
     */

    struct SoundDataProxy;
    template<typename Format_T> class gbSoundData;
    namespace SoundDataFormat {
        /** Sound data format descriptor
         */
        enum SOUND_DATA_FORMAT_T {
            FORMAT_UNKNOWN = 0,                ///< Unknown format; Used to indicate absence of a valid format
            FORMAT_MONO_8,                    ///< 8-bit Mono
            FORMAT_STEREO_8,                ///< 8-bit Stereo
            FORMAT_MONO_16,                    ///< 16-bit Mono
            FORMAT_STEREO_16                ///< 16-bit Stereo
        };
        struct Mono8;
        struct Stereo8;
        struct Mono16;
        struct Stereo16;
    }
    typedef gbSoundData<SoundDataFormat::Mono8> gbSoundDataMono8Bit;
    typedef gbSoundData<SoundDataFormat::Stereo8> gbSoundDataStereo8Bit;
    typedef gbSoundData<SoundDataFormat::Mono16> gbSoundDataMono16Bit;
    typedef gbSoundData<SoundDataFormat::Stereo16> gbSoundDataStereo16Bit;
    typedef boost::variant<gbSoundDataMono8Bit, gbSoundDataStereo8Bit, gbSoundDataMono16Bit, gbSoundDataStereo16Bit> gbSoundDataVariant;

}

#endif

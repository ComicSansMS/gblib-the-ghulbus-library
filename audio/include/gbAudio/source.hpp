/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_SOURCE_HPP_
#define GB_AUDIO_INCLUDE_GUARD_SOURCE_HPP_

#include <gbAudio/config.hpp>
#include <gbAudio/locatable.hpp>
#include <gbAudio/playable.hpp>

#include <memory>
#include <boost/utility.hpp>

namespace GB_AUDIO_NAMESPACE {

    class Playable;

    class gbSource;
    typedef std::shared_ptr<gbSource> gbSourcePtr;

    class gbSoundBuffer;

    /** Source for buffered audio playback
     * The gbSource supports simple buffered playback. It binds a single
     * gbSoundBuffer for playback. This requires the whole audio data
     * to be loaded into the gbSoundBuffer before playback.
     * For more advanced playback options, use gbStreamingSource.
     */
    class gbSource: public Locatable, public Playable, public boost::noncopyable {
    public:
        /** Destructor
         */
        virtual ~gbSource() {}
        /** Bind a gbSoundBuffer for playback
         * @param[in] buffer The buffer that will be used for playback
         * @throw InvalidProtocol if trying to change the bound buffer during playback
         */
        virtual void BindBuffer(gbSoundBuffer& buffer)=0;
    };

}

#ifdef GB_AUDIO_HAS_OPENAL
#    define GB_AUDIO_INCLUDE_TOKEN_SOURCE_OAL_HPP_
#    include <gbAudio/source_OAL.hpp>
#    undef GB_AUDIO_INCLUDE_TOKEN_SOURCE_OAL_HPP_
#endif

#endif

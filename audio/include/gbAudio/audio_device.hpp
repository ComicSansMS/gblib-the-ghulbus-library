/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_AUDIO_DEVICE_HPP_
#define GB_AUDIO_INCLUDE_GUARD_AUDIO_DEVICE_HPP_

#include <gbAudio/config.hpp>

#include <iosfwd>
#include <memory>
#include <string>
#include <vector>
#include <boost/utility.hpp>

namespace GB_AUDIO_NAMESPACE {

    class gbAudioDevice;
    typedef std::shared_ptr<gbAudioDevice> gbAudioDevicePtr;
    class gbListener;
    typedef std::shared_ptr<gbListener> gbListenerPtr;
    class gbSoundBuffer;
    typedef std::shared_ptr<gbSoundBuffer> gbSoundBufferPtr;
    class gbSource;
    typedef std::shared_ptr<gbSource> gbSourcePtr;
    class gbStreamingSource;
    typedef std::shared_ptr<gbStreamingSource> gbStreamingSourcePtr;

    /**
     * @todo: Support for OpenAL Effexts Extensions (EFX)
     */

    class gbAudioDevice: public boost::noncopyable {
    public:
        enum AUDIO_API_TYPE {
            AUDIO_API_DEFAULT,
            AUDIO_API_OPENAL
        };
        struct DeviceIdentifier {
            std::string Name;
        };
        enum CHANNEL_FORMAT_TYPE {
            CHANNEL_FORMAT_MONO,
            CHANNEL_FORMAT_STEREO,
            CHANNEL_FORMAT_QUAD,
            CHANNEL_FORMAT_5_1,
            CHANNEL_FORMAT_6_1,
            CHANNEL_FORMAT_7_1
        };
    public:
        virtual ~gbAudioDevice() {};
        virtual std::vector<CHANNEL_FORMAT_TYPE> GetSupportedChannelFormats() const=0;
        virtual gbSoundBufferPtr CreateSoundBuffer()=0;
        virtual gbSourcePtr CreateSource()=0;
        virtual gbStreamingSourcePtr CreateStreamingSource()=0;
        virtual gbListenerPtr GetListener()=0;
        virtual void Update()=0;
        static gbAudioDevicePtr Create(AUDIO_API_TYPE audio_api_type=AUDIO_API_DEFAULT);
        static gbAudioDevicePtr Create(DeviceIdentifier const& device_id, AUDIO_API_TYPE audio_api_type=AUDIO_API_DEFAULT);
        static std::vector<DeviceIdentifier> EnumerateDevices(AUDIO_API_TYPE audio_api_type=AUDIO_API_DEFAULT);
    };

    std::ostream& operator<<(std::ostream& os, gbAudioDevice::CHANNEL_FORMAT_TYPE channel_format);

}

#ifdef GB_AUDIO_HAS_OPENAL
#    define GB_AUDIO_INCLUDE_TOKEN_AUDIO_DEVICE_OAL_HPP_
#    include <gbAudio/audio_device_OAL.hpp>
#    undef GB_AUDIO_INCLUDE_TOKEN_AUDIO_DEVICE_OAL_HPP_
#endif

#endif

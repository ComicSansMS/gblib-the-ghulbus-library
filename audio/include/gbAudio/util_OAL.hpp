/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_UTIL_OAL_HPP_
#define GB_AUDIO_INCLUDE_GUARD_UTIL_OAL_HPP_

#ifndef GB_AUDIO_INCLUDE_TOKEN_UTIL_OAL_HPP_
#    error Do not include util_OAL.hpp directly; Include util.hpp instead
#endif

#include <gbAudio/sound_data_fwd.hpp>
#include <diagnostic/exception.hpp>
#include <diagnostic/log.hpp>

#include <al.h>

namespace GB_AUDIO_NAMESPACE {

    /** Get the corresponding string to an OpenAL error code
     */
    char const* TranslateErrorCode_OAL(ALenum err);

    /** Get the OpenAL format value for a SoundDataFormat::SOUND_DATA_FORMAT_T
     */
    inline ALenum GetOALSoundDataFormat_OAL(SoundDataFormat::SOUND_DATA_FORMAT_T fmt)
    {
        switch(fmt) {
        case SoundDataFormat::FORMAT_MONO_8:    return AL_FORMAT_MONO8;
        case SoundDataFormat::FORMAT_STEREO_8:  return AL_FORMAT_STEREO8;
        case SoundDataFormat::FORMAT_MONO_16:   return AL_FORMAT_MONO16;
        case SoundDataFormat::FORMAT_STEREO_16: return AL_FORMAT_STEREO16;
        default:
            DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                  "Unknown sound format specified" );
        }
    }

    /** Helper for OpenAL error handling
     * Checks for floating errors on construction and destruction
     * and allows for manual error checking in betweeen.
     */
    class ErrorMonitor_OAL {
    private:
        ALenum error_;            ///< OpenAL error code
    public:
        /** Constructor
         * Checks for floating errors and logs them to WARNING
         */
        ErrorMonitor_OAL()
        {
            if(CheckError()) { DIAG_LOG(WARNING, "Floating OpenAL error detected: " << TranslateErrorCode_OAL(error_)); }
        }
        /** Destructor
         * Checks for floating errors and logs them to WARNING
         */
        ~ErrorMonitor_OAL()
        {
            if(CheckError()) { DIAG_LOG(WARNING, "Floating OpenAL error detected: " << TranslateErrorCode_OAL(error_)); }
        }
        /** Checks for new OpenAL errors by calling alGetError()
         * @return true if an error was found (i.e. alGetError() did not return AL_NO_ERROR)
         * @note Calling this resets the OpenAL's error state to AL_NO_ERROR
         */
        bool CheckError()
        {
            return ((error_ = alGetError()) != AL_NO_ERROR);
        }
        /** Retrieve the last error code returned by alGetError()
         * @note A string representation for the error can be obtained through TranslateErrorCode_OAL()
         */
        ALenum GetLastError() const
        {
            return error_;
        }
    };

}

#endif

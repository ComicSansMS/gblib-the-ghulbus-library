/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_STREAMING_SOURCE_HPP_
#define GB_AUDIO_INCLUDE_GUARD_STREAMING_SOURCE_HPP_

#include <gbAudio/config.hpp>
#include <gbAudio/locatable.hpp>
#include <gbAudio/playable.hpp>
#include <gbAudio/sound_data_fwd.hpp>
#include <gbAudio/wind_up_source.hpp>

#include <gbUtil/resource_guardian.hpp>

#include <functional>
#include <memory>
#include <boost/utility.hpp>

namespace GB_AUDIO_NAMESPACE {


    class gbStreamingSource;
    typedef std::shared_ptr<gbStreamingSource> gbStreamingSourcePtr;

    /** Source for flexible output of dynamic sound data
     * In contrast to the gbSource, this class does not copy
     * data to a static hardware buffer before playback, but
     * instead streams data to the device on-demand.
     *
     * @section sec_gbStreamingSource_use Usage
     * The gbStreamingSource supports different modes of operation:
     *
     *  @subsection subsec_gbStreamingSource_use_static Playback of static sound data
     *  This mode is similar to playback with gbSource, but instead of loading all of
     *  the sound data into one large hardware buffer before playback, the data is copied
     *  to the hardware buffers of the gbStreamingSource on-demand.
     @code
     gbAudioDevicePtr audio_device = gbAudioDevice::Create();
     gbStreamingSourcePtr stream_source = audio_device->CreateStreamingSource();
     stream_source->ClaimSoundData(get_sound_data());
     stream_source->Play();
     while(true) {
        audio_device->Update();
     }
     @endcode
     *   @li Use this mode when the sound data is available before playback, but too
     *       large for playback through gbSource
     *   @li When only a single gbSoundData element is placed in the processing queue,
     *       gbStreamingSource behaves exactly as a gbSource for all methods specified
     *       in the Playable interface.
     *
     *  @subsection subsec_gbStreamingSource_use_streamsingle Streamed playback from a growing data buffer
     *  Use this mode if the sound data is not available in its entirety upon starting playback, e.g.
     *  when loading data asynchronously from a file.
     @code
     gbAudioDevicePtr audio_device = gbAudioDevice::Create();
     gbStreamingSourcePtr stream_source = audio_device->CreateStreamingSource();
     stream_source
     gbSoundDataMono8Bit data = get_sound_data();
     gbSoundDataVariant vardata = data;
     stream_source->EnqueueSoundData(vardata);
     stream_source->Play();
     while(true) {
        audio_device->Update();
        gbSoundDataMono8Bit new_data = get_more_sound_data();
        data.Append(new_data);
     }
     @endcode
     *   @li Use this mode when sound data becomes available on-demand during playback, but
     *       should not be discarded after processing
     *   @li The underlying gbSoundData may safely grow during playback, but it must not shrink.
     *   @li Inserting data asynchronously is safe, as long as they are serialized with the
     *       calls to gbAudioDevice::Update()
     *   @li For continuous playback, you must ensure that the underlying gbSoundData is
     *       growing faster than it is processed for playback by gbAudioDevice::Update()
     *   @li When only a single gbSoundData element is placed in the processing queue,
     *       gbStreamingSource behaves exactly as a gbSource for all methods specified
     *       in the Playable interface.
     *
     *  @subsection subsec_gbStreamingSource_use_streammulti Streamed playback from multiple data buffers
     *  Use this mode for streamed sound data that becomes available on demand, e.g. a webstream of
     *  sound data or a sound stream in a media player application. In contrast to the growing source
     *  approach, sound data is discarded after it was processed. Activate this mode by inserting
     *  more than one gbSoundData element in the processing queue.
     @code
     gbAudioDevicePtr audio_device = gbAudioDevice::Create();
     gbStreamingSourcePtr stream_source = audio_device->CreateStreamingSource();
     auto buffer_refill_callback = [](gbSoundDataVariant* data) -> gbStreamingSource::CALLBACK_RETURN_T {
        if(new_data_available()) {
            update_with_new_data(data);
            return gbStreamingSource::CALLBACK_RETURN_REINSERT_TO_QUEUE;
        } else {
            return gbStreamingSource::CALLBACK_RETURN_REMOVE_FROM_QUEUE;
        }
     };
     for(int i=0; i<NUMBER_OF_BUFFERS; ++i) {
        stream_source->ClaimSoundData(get_sound_data(), buffer_refill_callback);
     }
     stream_source->Play();
     while(true) {
        audio_device->Update();
     }
     @endcode
     *   @li Use this mode when streaming sound data dynamically for playback and discard
     *       it after processing
     *   @li When updating the gbSoundData in the BufferProcessedCallback, size and contents
     *       may be changed freely. The sampling frequency however must be the same for all
     *       enqueued buffers.
     *   @li Inserting an empty gbSoundData element into the queue will cause playback to stop
     *       as soon as it reaches the element. Upon the next call to Play(), the empty element
     *       will be skipped in the queue and playback will continue as before.
     *   @li Additional buffers may be enqueued during playback. Note however, that the
     *       end of the queue moves when automatically reinserting buffers.
     *   @li The semantics for looping and rewinding through the Playable interface do not
     *       apply for this mode. Playback will continue as long as data is available in the
     *       processing queue, so there is no concept of looping.
     *       Rewinding will only rewind to the beginning of the current element in the processing
     *       queue, which is rather useless.
     */
    class gbStreamingSource: public Locatable, public Playable, public WindUpSource, public boost::noncopyable {
    public:
        /** Return type for the BufferProcessedCallback function
         */
        enum CALLBACK_RETURN_T {
            CALLBACK_RETURN_REINSERT_TO_QUEUE,        ///< reinsert the processed element at the end of the queue
            CALLBACK_RETURN_REMOVE_FROM_QUEUE        ///< remove the element from the queue
        };
        /** Callback function for refilling processed data segments
         * This function will be invoked when an enqueued data segment was processed and
         * is ready for removal. You may either refill the segment with new data through
         * the provided pointer and insert it back into the queue, or remove it from the
         * queue, depending on the function's return value.
         */
        typedef std::function<CALLBACK_RETURN_T(gbSoundDataVariant*)> BufferProcessedCallback;
    public:
        /** Destructor
         */
        virtual ~gbStreamingSource() {};
        /** Set the number of hardware buffers used for playback
         * @param[in] n Desired number of buffers (at least 2; default: 3)
         * @throws InvalidArgument if requested number of buffers is less than 2;
         * @throws InvalidProtocol if called during playback
         * @note Increasing the number of buffers effectively increases
         *       the overall buffer size, while allowing smaller processing
         *       chunks when updating through gbAudioDevice::Update().
         */
        virtual void SetNumberOfBuffers(int n)=0;
        /** Set the buffer size of hardware buffers used for playback
         * @param[in] size Desired number of samples in each buffer (default: 1470)
         * @throws InvalidArgument if requested buffer size was less-equal 0;
         * @throws InvalidProtocol if trying if called during playback
         * @note All hardware buffers are of the same size.
         * @note Note that the actual size of the hardware buffer in bytes
         *       varies with the SoundDataFormat used for playback.
         */
        virtual void SetBufferSize(int size)=0;
        /** Add sound data to the playback queue
         * @param[in] data Sound data that is to be enqueued
         * @attention gbStreamingSound does not assume ownership of the passed data.
         *            Any enqueued gbSoundData must not be deleted by the user.
         *            Use ClaimSoundData() to have gbStreamingSound assume ownership
         *            of the sound data.
         * @note The enqueued sound data will not be removed automatically from the sound queue.
         */
        virtual void EnqueueSoundData(gbSoundDataVariant& data)=0;
        /** Add sound data to the playback queue
         * @param[in] data Sound data that is to be enqueued
         * @param[in] fun_processed Callback function invoked after the sound data was processed
         * @attention gbStreamingSound does not assume ownership of the passed data.
         *            Any enqueued gbSoundData must not be deleted by the user.
         *            Use ClaimSoundData() to have gbStreamingSound assume ownership
         *            of the sound data.
         */
        virtual void EnqueueSoundData(gbSoundDataVariant& data, BufferProcessedCallback fun_processed)=0;
        /** Add sound data to the playback queue
         * @param[in] data Sound data that is to be enqueued
         * @note The enqueued sound data will not be removed automatically from the sound queue.
         * @note The claimed data will be destroyed upon calling ClearSoundData().
         */
        virtual void ClaimSoundData(gbSoundDataVariant data)=0;
        /** Add sound data to the playback queue
         * @param[in] data Sound data that is to be enqueued
         * @param[in] fun_processed Callback function invoked after the sound data was processed
         * @note The claimed data will be destroyed once the respective element is 
         *       removed from the processing queue.
         */
        virtual void ClaimSoundData(gbSoundDataVariant data, BufferProcessedCallback fun_processed)=0;
        /** Remove all enqueued sound data from the processing queue
         * @throws InvalidProtocol if called during playback
         */
        virtual void ClearSoundData()=0;
        /** Get the number of elements in the processing queue
         * @return The number of elements enqueued through EnqueueSoundData or ClaimSoundData
         *         that are still in the processing queue
         */
        virtual int QueueSize() const=0;
        /** Get the total number of samples remaining in the processing queue
         * @retunr The sum of all samples enqueued through EnqueueSoundData or ClaimSoundData
         *         that have not yet been send to the hardware buffers.
         * @note This number will not necessarily decrease monotonically during playback, 
         *       as data elements may be automatically reinserted into the queue after processing.
         */
        virtual int UnplayedSamples() const=0;
    };

}

#ifdef GB_AUDIO_HAS_OPENAL
#    define GB_AUDIO_INCLUDE_TOKEN_STREAMING_SOURCE_OAL_HPP_
#    include <gbAudio/streaming_source_OAL.hpp>
#    undef GB_AUDIO_INCLUDE_TOKEN_STREAMING_SOURCE_OAL_HPP_
#endif

#endif

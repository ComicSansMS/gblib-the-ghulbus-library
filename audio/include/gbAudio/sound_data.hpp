/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_SOUND_DATA_HPP_
#define GB_AUDIO_INCLUDE_GUARD_SOUND_DATA_HPP_

#include <gbAudio/config.hpp>
#include <gbAudio/sound_data_fwd.hpp>

#include <diagnostic/exception.hpp>

#include <vector>
#include <boost/chrono.hpp>
#include <boost/type_traits.hpp>
#include <boost/variant.hpp>

namespace GB_AUDIO_NAMESPACE {

    /** Namespace for sound data format descriptors and layout types
     */
    namespace SoundDataFormat {

        /** Retrieves the corresponding sound data descriptor value for a given layout type
         * @tparam T Format type; This may be any of the layout type from the SoundDataFormat namespace
         * @return Sound data format value for the given layout type
         */
        template<typename T> inline
        SOUND_DATA_FORMAT_T GetSoundDataFormat() {
            static_assert(sizeof(T) == 0, "Unsupported sound format");
            return FORMAT_UNKNOWN;
        }

        /** Layout type for 8-bit Mono sound data
         * 8-bit data is expressed as an unsigned value over the range 0 to 255,
         * 128 being an audio output level of zero.
         */
        struct Mono8 {
            uint8_t sample;
        };
        template<> inline
        SOUND_DATA_FORMAT_T GetSoundDataFormat<Mono8>() {
            return FORMAT_MONO_8;
        }

        /** Layout type for 8-bit Stereo sound data
         * 8-bit data is expressed as an unsigned value over the range 0 to 255,
         * 128 being an audio output level of zero.
         */
        struct Stereo8 {
            uint8_t left;
            uint8_t right;
        };
        template<> inline
        SOUND_DATA_FORMAT_T GetSoundDataFormat<Stereo8>() {
            return FORMAT_STEREO_8;
        }

        /* Layout type for 16-bit Mono sound data
         * 16-bit data is expressed as a signed value over the range -32768 to 32767,
         * 0 being an audio output level of zero.
         */
        struct Mono16 {
            int16_t sample;
        };
        template<> inline
        SOUND_DATA_FORMAT_T GetSoundDataFormat<Mono16>() {
            return FORMAT_MONO_16;
        }

        /** Layout type for 16-bit Stereo sound data
         * 16-bit data is expressed as a signed value over the range -32768 to 32767,
         * 0 being an audio output level of zero.
         */
        struct Stereo16 {
            int16_t left;
            int16_t right;
        };
        template<> inline
        SOUND_DATA_FORMAT_T GetSoundDataFormat<Stereo16>() {
            return FORMAT_STEREO_16;
        }
    }

    /** Namespace for operations and generic typedefs associated with the gbSoundData class template
     */
    namespace SoundData {
        typedef boost::chrono::milliseconds Time;            ///< Time duration type for gbSoundData
    }

    /** Base type for carrying sound data
     * @tparam Format_T Sound data layout type; This may be any of the layout type from the SoundDataFormat namespace
     */
    template<typename Format_T>
    class gbSoundData {
    public:
        typedef Format_T FormatType;            ///< Alias for the data layout type given as template parameter
    private:
        std::vector<char> m_Data;                ///< Sound data storage
        int m_SamplingFrequency;                ///< Sampling frequency
    public:
        /** Constructor
         * Constructs an empty gbSoundData object
         * @param[in] sampling_frequency Sampling frequency of the sound data
         */
        gbSoundData(int sampling_frequency)
            :m_SamplingFrequency(sampling_frequency)
        {
        }
        /** Constructor
         * @param[in] sampling_frequency Sampling frequency of the sound data
         * @param[in] data Movable sound data storage as provided by gbSoundLoader
         */
        gbSoundData(int sampling_frequency, std::vector<char>&& data)
            :m_Data(std::move(data)), m_SamplingFrequency(sampling_frequency)
        {
        }
        /** Get the current sampling frequency for the sound data
         * @return Sampling frequency
         */
        int GetSamplingFrequency() const
        {
            return m_SamplingFrequency;
        }
        /** Get the size of a single sample in bytes
         * @return Bytesize of one sample
         */
        int GetSampleSize() const
        {
            return sizeof(FormatType);
        }
        /** Get the total number of samples present
         * @return Number of samples
         */
        int GetNumberOfSamples() const
        {
            return static_cast<int>(m_Data.size() / sizeof(FormatType));
        }
        /** Get the total length of the sound data when played
         * @return Playtime of the sound data
         * @note This depends on both the number of samples and the sampling frequency.
         */
        SoundData::Time GetTotalLength() const
        {
            int const sample_size = GetSampleSize();
            if(sample_size == 0) { return SoundData::Time(0); }
            return SoundData::Time((1000 * m_Data.size()) / (sample_size * m_SamplingFrequency));
        }
        /** Adjust the number of samples to a new size
         * @param[in] new_size New number of samples
         * @note If the new size is smaller than the original one, the data will be truncated.
         *       If the new size is larger, all newly appended entries will be uninitialized.
         *
         */
        void Resize(int new_size)
        {
            if(new_size <= 0) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Invalid target size for sound data" );
            }
            m_Data.resize(GetSampleSize() * new_size);
        }
        /** Append sound data
         * @param[in] data Additional sound data that will be inserted after the present data
         */
        void Append(gbSoundData<Format_T> const& data)
        {
            m_Data.reserve(m_Data.size() + data.m_Data.size());
            std::copy(data.m_Data.begin(), data.m_Data.end(), std::back_inserter(m_Data));
        }
        /** Set the sampling frequency to a new value
         * @param[in] freq New sampling frequency
         * @attention This does not apply any changes to the actual sound data, it simply changes
         *            the associated sampling frequency. Applying garbage sampling frequencies
         *            will mess up sound playback up to the point of crashing the program.
         */
        void SetSamplingFrequency(int freq)
        {
            m_SamplingFrequency = freq;
        }
        /** Access the actual sound data
         * @param[in] i Sample index [0..NumberOfSamples)
         * @return Sample at the given index
         * @throw InvalidArgument - If index is out of bounds
         */
        FormatType* operator[](int i)
        {
            int const index = i * GetSampleSize();
            if((index < 0) || (index >= static_cast<int>(m_Data.size()))) {
                DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                                      "Sample index out of bounds" );
            }
            return reinterpret_cast<FormatType*>(m_Data.data() + index);
        }
        /** Get the corresponding sound data descriptor value
         * @return Sound data format value for the data
         */
        SoundDataFormat::SOUND_DATA_FORMAT_T GetFormat() const
        {
            return SoundDataFormat::GetSoundDataFormat<FormatType>();
        }
        /** Get a raw pointer to the sound data storage
         * @return Pointer to the beginning of the sound data storage
         * @note This is mainly used for passing data to the underlying sound API and
         *       should not be called manually.
         * @note The total size of the data storage in bytes is GetNumberOfSamples()*GetSampleSize()
         */
        void const* GetRawData() const
        {
            return m_Data.data();
        }
        /** Get a raw pointer to the sound data storage at a given index
         * @param[in] index Index into the sound data storage [0..NumberOfSamples)
         * @return Pointer to the requested storage entry
         * @note This is mainly used for passing data to the underlying sound API and
         *       should not be called manually. Use operator[] instead.
         * @attention The index is *not* checked for out-of-bounds access
         */
        void const* GetRawDataAt(int index) const
        {
            return m_Data.data() + index*GetSampleSize();
        }
    };

    /** 8-bit Mono sound data
     */
    typedef gbSoundData<SoundDataFormat::Mono8> gbSoundDataMono8Bit;
    /** 8-bit Stereo sound data
     */
    typedef gbSoundData<SoundDataFormat::Stereo8> gbSoundDataStereo8Bit;
    /** 16-bit Mono sound data
     */
    typedef gbSoundData<SoundDataFormat::Mono16> gbSoundDataMono16Bit;
    /** 16-bit Stereo sound data
     */
    typedef gbSoundData<SoundDataFormat::Stereo16> gbSoundDataStereo16Bit;

    /** Sound data variant
     * This type holds exactly one of the instantiated sound data types above,
     * using the boost::variant facility. The main advantage of boost::variant
     * is that it allows both compile-time checked and run-time checked polymorphism
     * of sound data types.
     * Use SoundData::Get() to retrieve the original sound data type from the variant,
     * e.g. if you want to apply manual changes to the sound data.
     * Use the utility functions from the SoundData namespace to retrieve data provided
     * by the generic member functions of gbSoundData.
     */
    typedef boost::variant<gbSoundDataMono8Bit, gbSoundDataStereo8Bit, gbSoundDataMono16Bit, gbSoundDataStereo16Bit> gbSoundDataVariant;

    namespace SoundData {
        template<typename Format_T> inline gbSoundData<Format_T>& Get(gbSoundDataVariant& v) { return boost::get<gbSoundData<Format_T>>(v); }
        template<typename Format_T> inline gbSoundData<Format_T> const& Get(gbSoundDataVariant const& v) { return boost::get<gbSoundData<Format_T>>(v); }
    }

    namespace SoundData {
        class Visitor_GetRawDataAt: public boost::static_visitor<void const*> {
        private:
            int index_;
        public:
            Visitor_GetRawDataAt(int index): index_(index) {}
            template<typename T>
            void const* operator()(gbSoundData<T> const& operand) const { return operand.GetRawDataAt(index_); }
        };
        inline void const* GetRawDataAt(gbSoundDataVariant const& v, int index) { return boost::apply_visitor(Visitor_GetRawDataAt(index), v); }
    }

#    define SOUND_DATA_VARIANT_VISITOR(return_type, func) \
        namespace SoundData { \
            class Visitor_##func: public boost::static_visitor<return_type> { \
            public: \
                template<typename T> inline result_type operator()(gbSoundData<T> const& operand) const { return operand.func(); } \
            }; \
            inline return_type func(gbSoundDataVariant const& v) { return boost::apply_visitor(Visitor_##func(), v); } \
        }

    SOUND_DATA_VARIANT_VISITOR(int,                                  GetSamplingFrequency)
    SOUND_DATA_VARIANT_VISITOR(int,                                  GetSampleSize)
    SOUND_DATA_VARIANT_VISITOR(int,                                  GetNumberOfSamples)
    SOUND_DATA_VARIANT_VISITOR(SoundData::Time,                      GetTotalLength)
    SOUND_DATA_VARIANT_VISITOR(SoundDataFormat::SOUND_DATA_FORMAT_T, GetFormat)
    SOUND_DATA_VARIANT_VISITOR(void const*,                          GetRawData)

#    undef SOUND_DATA_VARIANT_VISITOR
}

namespace boost {
    template<typename Format_T>
    struct has_nothrow_copy< GB_AUDIO_NAMESPACE::gbSoundData<Format_T> >
        : mpl::true_
    {};
}

#endif

/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_SOUND_FILE_CODEC_HPP_
#define GB_AUDIO_INCLUDE_GUARD_SOUND_FILE_CODEC_HPP_

#include <gbAudio/config.hpp>
#include <gbAudio/sound_data_fwd.hpp>

#include <functional>
#include <iosfwd>

namespace GB_AUDIO_NAMESPACE {

    namespace SoundFileCodec {
        class Codec {
        public:
            typedef std::function<void(char**, size_t)> BufferResizeFunction;
        public:
            virtual ~Codec() {};
            virtual void Decode( std::istream& is, 
                                 char* buffer, BufferResizeFunction const& buffer_resize_fun,
                                 int* sampling_frequency,
                                 SoundDataFormat::SOUND_DATA_FORMAT_T* format) const=0;
        };
    }

}

#define GB_AUDIO_INCLUDE_TOKEN_SOUND_FILE_CODEC_RAW_HPP_
#    include <gbAudio/sound_file_codec_RAW.hpp>
#undef GB_AUDIO_INCLUDE_TOKEN_SOUND_FILE_CODEC_RAW_HPP_


#define GB_AUDIO_INCLUDE_TOKEN_SOUND_FILE_CODEC_WAV_HPP_
#    include <gbAudio/sound_file_codec_WAV.hpp>
#undef GB_AUDIO_INCLUDE_TOKEN_SOUND_FILE_CODEC_WAV_HPP_

#endif

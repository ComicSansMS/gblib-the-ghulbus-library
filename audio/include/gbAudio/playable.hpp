/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_PLAYABLE_HPP_
#define GB_AUDIO_INCLUDE_GUARD_PLAYABLE_HPP_

#include <gbAudio/config.hpp>

namespace GB_AUDIO_NAMESPACE {

    /** Interface for sources that can play audio
     */
    class Playable {
    public:
        /** State of audio playback
         */
        enum PlayState_T {
            PLAY_STATE_STOPPED,        ///< Stopped
            PLAY_STATE_PLAYING,        ///< Playing
            PLAY_STATE_PAUSED        ///< Paused
        };
    public:
        /** Destructor
         */
        virtual ~Playable() {};
        /** Start playback
         * If the source is already playing, calling Play()
         * will cause it to restart from the beginning. If the
         * source is paused, it will resume playing where it left
         * off.
         */
        virtual void Play()=0;
        /** Pause playback
         * Playback will be paused until the next call to Play().
         * Calling Pause on a source that is paused or stopped
         * has no effect.
         */
        virtual void Pause()=0;
        /** Stop playback
         * Playback will be stopped. On the next call to Play()
         * playback will restart from the beginning. Calling
         * on a stopped source has no effect.
         */
        virtual void Stop()=0;
        /** Rewind playback
         * If called on a playing or paused source, playback
         * will restart at the beginning. Has no effect on
         * a stopped source.
         */
        virtual void Rewind()=0;
        /** Retrieve the current play state
         * @return Current state of playback
         */
        virtual PlayState_T GetPlayState() const=0;
        /** Toggle loop repeat
         * @param[in] If true, the source will automatically rewind when reaching the end of audio data
         */
        virtual void Loop(bool do_loop)=0;
        /** Check loop status
         * @return true if loop repeat is enabled; false otherwise
         */
        virtual bool IsLooping() const=0;
    };
}

#endif

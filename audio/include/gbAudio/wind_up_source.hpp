/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#ifndef GB_AUDIO_INCLUDE_GUARD_WIND_UP_SOURCE_HPP_
#define GB_AUDIO_INCLUDE_GUARD_WIND_UP_SOURCE_HPP_

#include <gbAudio/config.hpp>

namespace GB_AUDIO_NAMESPACE {

    /** Interface for wind-up sources
     * Some sources require continous updates during playback, which
     * is handled by calling gbAudioDevice::Update(). Each call to
     * Update() will invoke the WindUp() method of all WindUpSources
     * registered with the respective gbAudioDevice.
     */
    class WindUpSource {
    public:
        /** Destructor
         */
        virtual ~WindUpSource() {};
        /** Update the source to ensure continous playback
         */
        virtual void WindUp()=0;
    };

}

#endif

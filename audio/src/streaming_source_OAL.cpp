/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/streaming_source.hpp>
#include <gbAudio/sound_data.hpp>
#include <gbAudio/util.hpp>

#include <diagnostic/diagnostic.hpp>
#include <gbUtil/missing_features.hpp>

namespace GB_AUDIO_NAMESPACE {

gbStreamingSource_OAL::gbStreamingSource_OAL(GB_UTIL_NAMESPACE::gbResourceGuardian<gbStreamingSource_OAL>* guardian)
    :gbSupervisedResource(guardian), m_Source(0), m_BufferSize(0), m_SoundData(nullptr),
     m_DataIndex(0), m_DoLoop(false), m_PlayState(PLAY_STATE_STOPPED)
{
    ErrorMonitor_OAL monitor;
    alGenSources(1, &m_Source);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to create OpenAL source" );
    }
    m_Base.OAL_SetSource(m_Source);
    SetNumberOfBuffers(3);
    SetBufferSize(1470);
}

gbStreamingSource_OAL::~gbStreamingSource_OAL()
{
    ErrorMonitor_OAL monitor;
    alDeleteSources(1, &m_Source);
    alDeleteBuffers(static_cast<ALsizei>(m_Buffers.size()), m_Buffers.data());
}

void gbStreamingSource_OAL::SetPosition(GB_MATH_NAMESPACE::Vector3f const& p)
{
    m_Base.SetPosition(p);
}

GB_MATH_NAMESPACE::Vector3f gbStreamingSource_OAL::GetPosition() const
{
    return m_Base.GetPosition();
}

void gbStreamingSource_OAL::SetVelocity(GB_MATH_NAMESPACE::Vector3f const& v)
{
    m_Base.SetVelocity(v);
}

GB_MATH_NAMESPACE::Vector3f gbStreamingSource_OAL::GetVelocity() const
{
    return m_Base.GetVelocity();
}

void gbStreamingSource_OAL::SetNumberOfBuffers(int n)
{
    if(n <= 1) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Streaming sound requires at least 2 buffers" );
    }
    if(GetPlayState() != PLAY_STATE_STOPPED) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Source must be stopped before changing buffer parameters" );
    }
    ErrorMonitor_OAL monitor;
    if(!m_Buffers.empty()) {
        alDeleteBuffers(static_cast<ALsizei>(m_Buffers.size()), m_Buffers.data());
    }
    m_Buffers.resize(n);
    m_FreeBuffers.resize(n);
    alGenBuffers(n, m_Buffers.data());
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to create sound buffer" );
    }
}

void gbStreamingSource_OAL::SetBufferSize(int size)
{
    if(size <= 0) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Buffer size must be greater than 0" );
    }
    if(GetPlayState() != PLAY_STATE_STOPPED) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Source must be stopped before changing buffer parameters" );
    }
    m_BufferSize = size;
}

void gbStreamingSource_OAL::SetSoundDataInfo()
{
    DIAG_ASSERT(!m_SoundDataQueue.empty());
    m_SoundData = std::get<0>(m_SoundDataQueue.front());
    m_DataInfo.format             = GetOALSoundDataFormat_OAL(SoundData::GetFormat(*m_SoundData));
    m_DataInfo.sample_size        = SoundData::GetSampleSize(*m_SoundData);
    m_DataInfo.sampling_frequency = SoundData::GetSamplingFrequency(*m_SoundData);
}

void gbStreamingSource_OAL::CheckSoundData(gbSoundDataVariant const& data)
{
    if(m_DataInfo.format != GetOALSoundDataFormat_OAL(SoundData::GetFormat(data))) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Sound data format mismatch" );
    }
    if(m_DataInfo.sampling_frequency != SoundData::GetSamplingFrequency(data)) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidArgument(),
                              "Sound data sampling frequency mismatch" );
    }
}

void gbStreamingSource_OAL::EnqueueSoundData(gbSoundDataVariant& data)
{
    EnqueueSoundData(data, BufferProcessedCallback());
}

void gbStreamingSource_OAL::EnqueueSoundData(gbSoundDataVariant& data, BufferProcessedCallback fun_processed)
{
    bool const is_first = m_SoundDataQueue.empty();
    if(!is_first) {
        CheckSoundData(data);
    }
    m_SoundDataQueue.push_back(DataQueueEntry(&data, fun_processed, nullptr));
    if(is_first) {
        SetSoundDataInfo();
    }
}

void gbStreamingSource_OAL::ClaimSoundData(gbSoundDataVariant data)
{
    ClaimSoundData(data, BufferProcessedCallback());
}

void gbStreamingSource_OAL::ClaimSoundData(gbSoundDataVariant data, BufferProcessedCallback fun_processed)
{
    auto storage = std::make_unique<gbSoundDataVariant>(std::move(data));
    bool const is_first = m_SoundDataQueue.empty();
    if(!is_first) {
        CheckSoundData(data);
    }
    m_SoundDataQueue.push_back(DataQueueEntry(storage.get(), fun_processed, std::move(storage)));
    if(is_first) {
        SetSoundDataInfo();
    }
}

void gbStreamingSource_OAL::ClearSoundData()
{
    if(GetPlayState() != PLAY_STATE_STOPPED) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Source must be stopped before sound data can be cleared" );
    }
    m_SoundDataQueue.clear();
}

int gbStreamingSource_OAL::QueueSize() const
{
    return static_cast<int>(m_SoundDataQueue.size());
}

int gbStreamingSource_OAL::UnplayedSamples() const
{
    if(!m_SoundData) { return 0; }
    int acc = SoundData::GetNumberOfSamples(*m_SoundData) - m_DataIndex;
    int const q_size = QueueSize();
    for(int i=1; i<q_size; ++i) {
        acc += SoundData::GetNumberOfSamples(*std::get<0>(m_SoundDataQueue[i]));
    }
    return acc;
}

void gbStreamingSource_OAL::Play()
{
    switch(GetPlayState()) {
    case PLAY_STATE_PAUSED:
        m_PlayState = PLAY_STATE_PLAYING;
        alSourcePlay(m_Source);
        break;
    case PLAY_STATE_PLAYING:
        Stop();
        //intentional fallthrough
    default: 
        {
            int n_buffers = static_cast<int>(m_Buffers.size());
            m_FreeBuffers = m_Buffers;
            if(m_DataIndex == SoundData::GetNumberOfSamples(*m_SoundData)) { m_DataIndex = 0; }
            FillSoundBuffers(n_buffers);
            alSourcePlay(m_Source);
            m_PlayState = PLAY_STATE_PLAYING;
        } break;
    }
    
}

void gbStreamingSource_OAL::Pause()
{
    alSourcePause(m_Source);
    m_PlayState = PLAY_STATE_PAUSED;
}

void gbStreamingSource_OAL::Stop()
{
    m_DataIndex = 0;
    alSourceStop(m_Source);
    alSourcei(m_Source, AL_BUFFER, 0);
    m_PlayState = PLAY_STATE_STOPPED;
}

void gbStreamingSource_OAL::Rewind()
{
    m_DataIndex = 0;
}

Playable::PlayState_T gbStreamingSource_OAL::GetPlayState() const
{
    if(m_PlayState == PLAY_STATE_STOPPED) {
        //source may still be playing, if stopped state was
        //triggered by an empty queue but the source is still
        //busy processing the hardware buffers; 
        //this behavior is consistent with the semantics
        //of the Playable interface
        ALint state;
        alGetSourcei(m_Source, AL_SOURCE_STATE, &state);
        if(state == AL_PLAYING) {
            return PLAY_STATE_PLAYING;
        } else {
            DIAG_ASSERT((state == AL_STOPPED) || (state == AL_INITIAL));
            return PLAY_STATE_STOPPED;
        }
    }
    return m_PlayState;
}

void gbStreamingSource_OAL::Loop(bool do_loop)
{
    m_DoLoop = do_loop;
}

bool gbStreamingSource_OAL::IsLooping() const
{
    return m_DoLoop;
}

void gbStreamingSource_OAL::WindUp()
{
    if(m_PlayState == PLAY_STATE_PLAYING) {
        ALint buffers_processed;
        alGetSourcei(m_Source, AL_BUFFERS_PROCESSED, &buffers_processed);
        if(buffers_processed == 0) { return; }
        DIAG_ASSERT(buffers_processed <= static_cast<int>(m_FreeBuffers.size()));
        alSourceUnqueueBuffers(m_Source, buffers_processed, m_FreeBuffers.data());
        FillSoundBuffers(buffers_processed);
        //on buffer underrun, manually resume playback
        ALint state;
        alGetSourcei(m_Source, AL_SOURCE_STATE, &state);
        if(state != AL_PLAYING) { alSourcePlay(m_Source); }
    }
}

void gbStreamingSource_OAL::FillSoundBuffers(int nbuffers)
{
    if(!m_SoundData) { return; }
    int nsamples = SoundData::GetNumberOfSamples(*m_SoundData);
    for(int i=0; i<nbuffers; ++i) {
        if(m_DataIndex + m_BufferSize < nsamples) {
            alBufferData( m_FreeBuffers[i], m_DataInfo.format,
                          SoundData::GetRawDataAt(*m_SoundData, m_DataIndex), m_BufferSize*m_DataInfo.sample_size,
                          m_DataInfo.sampling_frequency );
            m_DataIndex += m_BufferSize;
        } else {
            //transition to next element in data queue
            std::vector<char> buffer = BuildTransitionalBuffer(&nsamples);
            alBufferData( m_FreeBuffers[i], m_DataInfo.format,
                          buffer.data(), static_cast<ALsizei>(buffer.size()),
                          m_DataInfo.sampling_frequency );
            if((nsamples == 0) || (m_DataIndex == nsamples)) {
                // stop playback
                nbuffers = i+1;
                m_PlayState = PLAY_STATE_STOPPED;
                break;
            }
        }
    }
    alSourceQueueBuffers(m_Source, nbuffers, m_FreeBuffers.data());
}

std::vector<char> gbStreamingSource_OAL::BuildTransitionalBuffer(int* nsamples)
{
    int remaining_size = m_BufferSize;
    std::vector<char> buffer;
    buffer.reserve(remaining_size);
    while(remaining_size > 0) {
        char const* data_ptr = reinterpret_cast<char const*>(SoundData::GetRawDataAt(*m_SoundData, m_DataIndex));
        auto const copy_size = std::min(remaining_size, (*nsamples)-m_DataIndex);
        DIAG_ASSERT(m_DataIndex+copy_size <= SoundData::GetNumberOfSamples(*m_SoundData));
        std::copy(data_ptr, data_ptr+(copy_size*m_DataInfo.sample_size), std::back_inserter(buffer));
        m_DataIndex += copy_size;
        remaining_size -= copy_size;
        if(m_DataIndex == (*nsamples)) {
            if((m_SoundDataQueue.size() == 1) && (!m_DoLoop)) { break; }
            AdvanceQueue();
            if(!m_SoundData) {
                // no more data
                *nsamples = 0;
                break;
            }
            *nsamples = SoundData::GetNumberOfSamples(*m_SoundData);
            if((*nsamples) == 0) {
                //empty data element in queue; stop playback
                break;
            }
        }
    }
    return buffer;
}

void gbStreamingSource_OAL::AdvanceQueue()
{
    auto cb_fun = std::get<1>(m_SoundDataQueue.front());
    bool do_reinsert = false;
    if( (cb_fun) && (cb_fun(std::get<0>(m_SoundDataQueue.front())) == CALLBACK_RETURN_REINSERT_TO_QUEUE) ) {
        CheckSoundData(*std::get<0>(m_SoundDataQueue.front()));
        do_reinsert = true;
    }
    if(do_reinsert) {
        //element will be re-inserted at back of queue
        switch(m_SoundDataQueue.size()) {
        default:
            {
                auto& e = m_SoundDataQueue.front();
                m_SoundDataQueue.push_back( DataQueueEntry(std::get<0>(e), std::get<1>(e), std::unique_ptr<gbSoundDataVariant>(std::get<2>(e).release())) );
                m_SoundDataQueue.pop_front();
            } break;
        case 1:
            //nothing to do
            break;
        case 2:
            std::swap(m_SoundDataQueue.front(), m_SoundDataQueue.back());
            break;
        case 3:
            std::swap(m_SoundDataQueue[0], m_SoundDataQueue[2]);
            std::swap(m_SoundDataQueue[0], m_SoundDataQueue[1]);
            break;
        }
    } else {
        //free element from queue
        m_SoundDataQueue.pop_front();
    }
    //update sound data pointer
    m_SoundData = (m_SoundDataQueue.empty()) ? (nullptr) : (std::get<0>(m_SoundDataQueue.front()));
    m_DataIndex = 0;
}

}

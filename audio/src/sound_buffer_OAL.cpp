/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/sound_buffer.hpp>
#include <gbAudio/sound_data.hpp>
#include <gbAudio/util.hpp>
#include <diagnostic/diagnostic.hpp>

namespace GB_AUDIO_NAMESPACE {

gbSoundBuffer_OAL::gbSoundBuffer_OAL()
    :m_Buffer(0)
{
    ErrorMonitor_OAL monitor;
    alGenBuffers(1, &m_Buffer);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to create sound buffer" );
    }
}

gbSoundBuffer_OAL::~gbSoundBuffer_OAL()
{
    alDeleteBuffers(1, &m_Buffer);
}

void gbSoundBuffer_OAL::SetSoundData(gbSoundDataVariant const& data)
{
    ErrorMonitor_OAL monitor;
    alBufferData( m_Buffer, GetOALSoundDataFormat_OAL(SoundData::GetFormat(data)), SoundData::GetRawData(data),
                  static_cast<ALsizei>(SoundData::GetNumberOfSamples(data)*SoundData::GetSampleSize(data)),
                  SoundData::GetSamplingFrequency(data) );
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to write sound buffer" );
    }
}

ALuint gbSoundBuffer_OAL::OAL_GetBufferId()
{
    return m_Buffer;
}

}

/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/sound_loader.hpp>
#include <diagnostic/diagnostic.hpp>

#include <fstream>

namespace GB_AUDIO_NAMESPACE {

gbSoundLoader::gbSoundLoader()
    :m_Format(SoundDataFormat::FORMAT_UNKNOWN), m_SamplingFrequency(0)
{
}

gbSoundLoader::gbSoundLoader(std::string const& filename, SoundFileCodec::Codec const& codec)
{
    ReadFile(filename, codec);
}

void gbSoundLoader::ReadFile(std::string const& filename, SoundFileCodec::Codec const& codec)
{
    std::ifstream fin(filename.c_str(), std::ios_base::binary);
    if(!fin) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError() << Diagnostic::Exception_Info::file_name(filename),
                              "Could not open sound file" );
    }

    SoundFileCodec::Codec::BufferResizeFunction resize_fun = [this](char** buffer, size_t new_size) {
        m_Data.resize(new_size);
        *buffer = m_Data.data();
    };
    try {
    codec.Decode(fin, m_Data.data(), resize_fun, &m_SamplingFrequency, &m_Format);
    } catch(Diagnostic::Exception_Base& e) { e << Diagnostic::Exception_Info::file_name(filename); throw; }
    if(m_Format == SoundDataFormat::FORMAT_UNKNOWN) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol()  << Diagnostic::Exception_Info::file_name(filename),
                              "Codec error while processing sound file" );
    }
}

gbSoundDataVariant gbSoundLoader::GetSoundData()
{
    SoundDataFormat::SOUND_DATA_FORMAT_T format = m_Format;
    int freq = m_SamplingFrequency;
    m_Format = SoundDataFormat::FORMAT_UNKNOWN;
    m_SamplingFrequency = 0;
    switch(format) {
    case SoundDataFormat::FORMAT_MONO_8:    return gbSoundData<   SoundDataFormat::Mono8>(freq, std::move(m_Data));
    case SoundDataFormat::FORMAT_STEREO_8:  return gbSoundData< SoundDataFormat::Stereo8>(freq, std::move(m_Data));
    case SoundDataFormat::FORMAT_MONO_16:   return gbSoundData<  SoundDataFormat::Mono16>(freq, std::move(m_Data));
    case SoundDataFormat::FORMAT_STEREO_16: return gbSoundData<SoundDataFormat::Stereo16>(freq, std::move(m_Data));
    case SoundDataFormat::FORMAT_UNKNOWN:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "No sound data loaded" );
    default:
        DIAG_ASSERT(false);
        throw 0;
    }
}

gbSoundDataVariant gbSoundLoader::GetSoundDataCopy() const
{
    std::vector<char> data_copy(m_Data);
    switch(m_Format) {
    case SoundDataFormat::FORMAT_MONO_8:    return gbSoundData<   SoundDataFormat::Mono8>(m_SamplingFrequency, std::move(data_copy));
    case SoundDataFormat::FORMAT_STEREO_8:  return gbSoundData< SoundDataFormat::Stereo8>(m_SamplingFrequency, std::move(data_copy));
    case SoundDataFormat::FORMAT_MONO_16:   return gbSoundData<  SoundDataFormat::Mono16>(m_SamplingFrequency, std::move(data_copy));
    case SoundDataFormat::FORMAT_STEREO_16: return gbSoundData<SoundDataFormat::Stereo16>(m_SamplingFrequency, std::move(data_copy));
    case SoundDataFormat::FORMAT_UNKNOWN:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "No sound data loaded" );
    default:
        DIAG_ASSERT(false);
        throw 0;
    }
}

SoundDataFormat::SOUND_DATA_FORMAT_T gbSoundLoader::GetSoundDataFormat() const
{
    return m_Format;
}

}

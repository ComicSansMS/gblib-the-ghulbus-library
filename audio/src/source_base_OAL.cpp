/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#define GB_AUDIO_INCLUDE_TOKEN_SOURCE_BASE_OAL_HPP_
#include <gbAudio/source_base_OAL.hpp>
#include <gbAudio/util.hpp>

namespace GB_AUDIO_NAMESPACE {

gbSourceBase_OAL::gbSourceBase_OAL()
    :m_Source(0)
{
}

void gbSourceBase_OAL::OAL_SetSource(int source_id)
{
    m_Source = source_id;
}

void gbSourceBase_OAL::SetPosition(GB_MATH_NAMESPACE::Vector3f const& p)
{
    ErrorMonitor_OAL monitor;
    alSource3f(m_Source, AL_POSITION, p.x_, p.y_, p.z_);
}

GB_MATH_NAMESPACE::Vector3f gbSourceBase_OAL::GetPosition() const
{
    ErrorMonitor_OAL monitor;
    GB_MATH_NAMESPACE::Vector3f ret;
    alGetSource3f(m_Source, AL_POSITION, &ret.x_, &ret.y_, &ret.z_);
    return ret;
}

void gbSourceBase_OAL::SetVelocity(GB_MATH_NAMESPACE::Vector3f const& v)
{
    ErrorMonitor_OAL monitor;
    alSource3f(m_Source, AL_VELOCITY, v.x_, v.y_, v.z_);
}

GB_MATH_NAMESPACE::Vector3f gbSourceBase_OAL::GetVelocity() const
{
    ErrorMonitor_OAL monitor;
    GB_MATH_NAMESPACE::Vector3f ret;
    alGetSource3f(m_Source, AL_VELOCITY, &ret.x_, &ret.y_, &ret.z_);
    return ret;
}

}

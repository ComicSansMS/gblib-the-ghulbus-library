/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/source.hpp>
#include <gbAudio/sound_buffer.hpp>
#include <gbAudio/util.hpp>

#include <diagnostic/diagnostic.hpp>

namespace GB_AUDIO_NAMESPACE {

gbSource_OAL::gbSource_OAL()
    :m_Source(0), m_PlayState(PLAY_STATE_STOPPED)
{
    ErrorMonitor_OAL monitor;
    alGenSources(1, &m_Source);
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to create OpenAL source" );
    }
    m_Base.OAL_SetSource(m_Source);
}

gbSource_OAL::~gbSource_OAL()
{
    alDeleteSources(1, &m_Source);
}

void gbSource_OAL::SetPosition(GB_MATH_NAMESPACE::Vector3f const& p)
{
    m_Base.SetPosition(p);
}

GB_MATH_NAMESPACE::Vector3f gbSource_OAL::GetPosition() const
{
    return m_Base.GetPosition();
}

void gbSource_OAL::SetVelocity(GB_MATH_NAMESPACE::Vector3f const& v)
{
    m_Base.SetVelocity(v);
}

GB_MATH_NAMESPACE::Vector3f gbSource_OAL::GetVelocity() const
{
    return m_Base.GetVelocity();
}

void gbSource_OAL::Play()
{
    alSourcePlay(m_Source);
    m_PlayState = PLAY_STATE_PLAYING;
}

void gbSource_OAL::Pause()
{
    alSourcePause(m_Source);
    m_PlayState = PLAY_STATE_PAUSED;
}

void gbSource_OAL::Stop()
{
    alSourceStop(m_Source);
    m_PlayState = PLAY_STATE_STOPPED;
}

void gbSource_OAL::Rewind()
{
    alSourceRewind(m_Source);
}

Playable::PlayState_T gbSource_OAL::GetPlayState() const
{
    if(m_PlayState == PLAY_STATE_PLAYING) {
        ALint state;
        alGetSourcei(m_Source, AL_SOURCE_STATE, &state);
        if(state != AL_PLAYING) {
            DIAG_ASSERT(state == AL_STOPPED);
            return PLAY_STATE_STOPPED;
        }
    }
    return m_PlayState;
}

void gbSource_OAL::Loop(bool do_loop)
{
    alSourcei(m_Source, AL_LOOPING, (do_loop)?AL_TRUE:AL_FALSE);
}

bool gbSource_OAL::IsLooping() const
{
    ALint ret;
    alGetSourcei(m_Source, AL_LOOPING, &ret);
    return (ret == AL_TRUE);
}

void gbSource_OAL::BindBuffer(gbSoundBuffer& buffer)
{
    if(GetPlayState() != PLAY_STATE_STOPPED) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Source must be stopped before binding new data" );
    }
    gbSoundBuffer_OAL* buffer_oal = nullptr;
    try {
        buffer_oal = dynamic_cast<gbSoundBuffer_OAL*>(&buffer);
    } catch(std::bad_cast&) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Only OpenAL buffers can be bound to OpenAL sources" );
    }
    ErrorMonitor_OAL monitor;
    alSourcei(m_Source, AL_BUFFER, buffer_oal->OAL_GetBufferId());
    if(monitor.CheckError()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to bind OpenAL buffer" );
    }
}

}

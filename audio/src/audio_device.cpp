/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/audio_device.hpp>
#include <diagnostic/diagnostic.hpp>

#include <ostream>

namespace GB_AUDIO_NAMESPACE {

gbAudioDevicePtr gbAudioDevice::Create(AUDIO_API_TYPE audio_api_type)
{
    DeviceIdentifier dev_default;
    return Create(dev_default, audio_api_type);
}

gbAudioDevicePtr gbAudioDevice::Create(DeviceIdentifier const& device_id, AUDIO_API_TYPE audio_api_type)
{
    switch(audio_api_type) {
    case AUDIO_API_DEFAULT:
    case AUDIO_API_OPENAL:
#        ifdef GB_AUDIO_HAS_OPENAL
        return std::make_shared<gbAudioDevice_OAL>(device_id);
#        else
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "gbAudio was compiled without OpenAL support");
#        endif
    default:
        DIAG_ASSERT(false);
    }
    DIAG_ASSERT(false);
    return nullptr;
}

std::vector<gbAudioDevice::DeviceIdentifier> gbAudioDevice::EnumerateDevices(AUDIO_API_TYPE audio_api_type)
{
    switch(audio_api_type) {
    case AUDIO_API_DEFAULT:
    case AUDIO_API_OPENAL:
#        ifdef GB_AUDIO_HAS_OPENAL
        return gbAudioDevice_OAL::EnumerateDevices_OAL();
#        else
        DIAG_THROW_EXCEPTION(Diagnostic::Exceptions::NotImplemented(), "gbAudio was compiled without OpenAL support");
#        endif
    default:
        DIAG_ASSERT(false);
    }
    DIAG_ASSERT(false);
    return std::vector<DeviceIdentifier>();
}

std::ostream& operator<<(std::ostream& os, gbAudioDevice::CHANNEL_FORMAT_TYPE channel_format)
{
    switch(channel_format) {
    case gbAudioDevice::CHANNEL_FORMAT_MONO:   os << "Mono"; break;
    case gbAudioDevice::CHANNEL_FORMAT_STEREO: os << "Stereo (2/2.1)"; break;
    case gbAudioDevice::CHANNEL_FORMAT_QUAD:   os << "Multi-Channel (4/4.1)"; break;
    case gbAudioDevice::CHANNEL_FORMAT_5_1:    os << "Multi-Channel (5.1)"; break;
    case gbAudioDevice::CHANNEL_FORMAT_6_1:    os << "Multi-Channel (6.1)"; break;
    case gbAudioDevice::CHANNEL_FORMAT_7_1:    os << "Multi-Channel (7.1)"; break;
    default:
        DIAG_ASSERT(false);
    }
    return os;
}

}

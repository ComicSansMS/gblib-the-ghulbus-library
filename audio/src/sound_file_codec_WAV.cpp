/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/sound_file_codec.hpp>
#include <diagnostic/diagnostic.hpp>

#include <istream>
#include <string>
#include <boost/lexical_cast.hpp>

namespace GB_AUDIO_NAMESPACE {
namespace SoundFileCodec {

inline unsigned int string_to_hex(char const str[4]) {
    return ( (str[3] << 24) | (str[2] << 16) | (str[1] << 8) | (str[0]) );
}

inline WAV::RIFFHeader ParseHeader(std::istream& is)
{
    WAV::RIFFHeader header;
    is.read(reinterpret_cast<char*>(&header), sizeof(header));
    if((is.fail()) || (header.ChunkID != string_to_hex("RIFF")) || (header.Format != string_to_hex("WAVE"))) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Invalid file header" );
    }
    return header;
}

inline WAV::FmtChunk ParseFormatChunk(std::istream& is)
{
    WAV::FmtChunk format_chunk;
    is.read(reinterpret_cast<char*>(&format_chunk), sizeof(format_chunk));
    if((is.fail()) || (format_chunk.Subchunk1ID != string_to_hex("fmt "))) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Invalid format chunk" );
    }
    if(format_chunk.AudioFormat != 1) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              "Only uncompressed PCM files supported" );
    }
    if( (format_chunk.BlockAlign != format_chunk.NumChannels * (format_chunk.BitsPerSample >> 3)) ||
        (format_chunk.ByteRate != format_chunk.SampleRate * format_chunk.BlockAlign) )
    {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Format information corrupted" );
    }
    return format_chunk;
}

inline WAV::DataChunk ParseDataChunk(std::istream& is)
{
    WAV::DataChunk data_chunk;
    is.read(reinterpret_cast<char*>(&data_chunk), sizeof(data_chunk));
    if((is.fail()) || (data_chunk.Subchunk2ID != string_to_hex("data"))) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Invalid data chunk" );
    }
    return data_chunk;
}

inline SoundDataFormat::SOUND_DATA_FORMAT_T GetDataFormat(WAV::FmtChunk const& format_chunk)
{
    bool is_8_bit;
    switch(format_chunk.BitsPerSample) {
    case 8:  is_8_bit = true;  break;
    case 16: is_8_bit = false; break;
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              std::string("Unsupported bitrate \'") + 
                              boost::lexical_cast<std::string>(format_chunk.BitsPerSample) +
                              "\'" );
    }
    switch(format_chunk.NumChannels) {
    case 1: return (is_8_bit)?(SoundDataFormat::FORMAT_MONO_8):(SoundDataFormat::FORMAT_MONO_16);
    case 2: return (is_8_bit)?(SoundDataFormat::FORMAT_STEREO_8):(SoundDataFormat::FORMAT_STEREO_16);
    default:
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::NotImplemented(),
                              std::string("Unsupported number of channels") +
                              boost::lexical_cast<std::string>(format_chunk.NumChannels) +
                              "\'" );
    }
}

void WAV::Decode( std::istream& is, char* buffer, BufferResizeFunction const& buffer_resize_fun, int* sampling_frequency, SoundDataFormat::SOUND_DATA_FORMAT_T* format) const
{
    ParseHeader(is);
    FmtChunk format_chunk = ParseFormatChunk(is);
    DataChunk data_chunk = ParseDataChunk(is);

    *format = GetDataFormat(format_chunk);
    *sampling_frequency = format_chunk.SampleRate;

    buffer_resize_fun(&buffer, data_chunk.Subchunk2Size);
    is.read(buffer, data_chunk.Subchunk2Size);
    if(is.fail()) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Data read error" );
    }
}

}
}

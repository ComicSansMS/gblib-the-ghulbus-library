
#include <gbAudio/audio.hpp>
#include <gbAudio/sound_loader.hpp>
#include <diagnostic/log.hpp>
#include <algorithm>
#include <iostream>
#include <iterator>

#include <boost/thread.hpp>

void TestData()
{
    using namespace GhulbusAudio;
    gbSoundData<int> d1(123);
    std::cout << "Freq: " << d1.GetSamplingFrequency() << "\n";
    std::cout << "D1: " << d1.GetSampleSize() << "\n";
    gbSoundData<SoundDataFormat::Stereo8> d2(44100);
    std::cout << "D2: " << d2.GetSampleSize() << "\n";
    d2.Resize(1);
    auto field = d2[0];
    field->left = 42;
    field->right = 23;
    std::cout << "Sample: " << static_cast<int>(d2[0]->left) << " " << static_cast<int>(d2[0]->right) << "\n";
    //auto ret = d1.GetSample(0);
}

int main(int /* argc */, char* /* argv */[])
{
    /*
    TestData();
    /*/
    std::cout << "Audio devices:\n";
    auto device_list = GhulbusAudio::gbAudioDevice::EnumerateDevices();
    std::for_each(begin(device_list), end(device_list), [](GhulbusAudio::gbAudioDevice::DeviceIdentifier const& dev_id) {
        std::cout << " * " << dev_id.Name.c_str() << "\n";
    });

    GhulbusAudio::gbAudioDevicePtr audio = GhulbusAudio::gbAudioDevice::Create();
    auto channel_format_list = audio->GetSupportedChannelFormats();
    std::cout << "Supported channel formats:\n";
    std::for_each(begin(channel_format_list), end(channel_format_list), [](GhulbusAudio::gbAudioDevice::CHANNEL_FORMAT_TYPE const& fmt) {
        std::cout << " * " << fmt << "\n";
    });

    GhulbusAudio::gbSoundLoader sound_loader;
    //sound_loader.ReadFile("../../audio/test/raw_out.raw", GhulbusAudio::SoundFileCodec::RAW(44100, GhulbusAudio::SoundDataFormat::FORMAT_STEREO_16));
    DIAG_LOG(INFO, "Reading file...");
    sound_loader.ReadFile("../../audio/test/wav_out.wav", GhulbusAudio::SoundFileCodec::WAV());

    DIAG_LOG(INFO, "Writing gbSoundData...");
    GhulbusAudio::gbSoundDataVariant vardata(sound_loader.GetSoundData());
    std::cout << "Type: " << vardata.type().name() << "\n";

    std::cout << "Time: " << boost::chrono::duration_cast<boost::chrono::seconds>(
        GhulbusAudio::SoundData::GetTotalLength(vardata)
        ) << "\n";
    std::cout << "Time: " << GhulbusAudio::SoundData::GetTotalLength(vardata) << "\n";
    std::cout << "samplesize: " << GhulbusAudio::SoundData::GetSampleSize(vardata) << "\n";

    DIAG_LOG(INFO, "Creating buffer...");
    GhulbusAudio::gbSoundBufferPtr buffer = audio->CreateSoundBuffer();
    DIAG_LOG(INFO, "Binding gbSoundData to buffer...");
    buffer->SetSoundData(vardata);

    /*
    DIAG_LOG(INFO, "Create source...");
    GhulbusAudio::gbSourcePtr source = audio->CreateSource();
    DIAG_LOG(INFO, "Binding buffer to source...");
    source->BindBuffer(*buffer);
    source->Play();
    while(source->GetPlayState() == GhulbusAudio::Playable::PLAY_STATE_PLAYING) {
        boost::this_thread::sleep(boost::posix_time::milliseconds(10));
    }
    //boost::this_thread::sleep(boost::posix_time::milliseconds(GhulbusAudio::SoundData::GetTotalLength(vardata).count()));
    /*/

    DIAG_LOG(INFO, "Create stream source...");
    GhulbusAudio::gbStreamingSourcePtr stream_source = audio->CreateStreamingSource();
    DIAG_LOG(INFO, "Binding data to stream source...");
    stream_source->EnqueueSoundData(vardata);
    stream_source->Loop(false);
    stream_source->Play();
    int i = 0;
    while(stream_source->UnplayedSamples() > 0) {
        boost::this_thread::sleep(boost::posix_time::milliseconds(30));
        audio->Update();
        ++i;
        if(i == 70) {
            //DIAG_LOG(INFO, "Rew");
            //stream_source->Rewind();
        }
    }
    stream_source->Play();
    while(stream_source->GetPlayState() != GhulbusAudio::Playable::PLAY_STATE_STOPPED) {
        boost::this_thread::sleep(boost::posix_time::milliseconds(30));
        audio->Update();
    }
    //*/

#if defined _WIN32 && defined _DEBUG
    system("PAUSE");
#endif
    return 0;
}

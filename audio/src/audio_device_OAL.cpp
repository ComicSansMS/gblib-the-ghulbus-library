/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/audio_device.hpp>
#include <gbAudio/listener.hpp>
#include <gbAudio/sound_buffer.hpp>
#include <gbAudio/source.hpp>
#include <gbAudio/streaming_source.hpp>
#include <gbAudio/util.hpp>
#include <diagnostic/diagnostic.hpp>

#include <cstring>

static ALboolean (*g_EAXSetBufferMode)(ALsizei n, ALuint *buffers, ALint value) = nullptr;
static ALenum (*g_EAXGetBufferMode)(ALuint buffer, ALint *value) = nullptr;
static ALenum g_AL_EAX_RAM_SIZE = 0;
static ALenum g_AL_EAX_RAM_FREE = 0;
static ALenum g_AL_STORAGE_AUTOMATIC = 0;
static ALenum g_AL_STORAGE_HARDWARE = 0;
static ALenum g_AL_STORAGE_ACCESSIBLE = 0;
static ALenum g_AL_FORMAT_QUAD16 = 0;
static ALenum g_AL_FORMAT_51CHN16 = 0;
static ALenum g_AL_FORMAT_61CHN16 = 0;
static ALenum g_AL_FORMAT_71CHN16 = 0;


namespace GB_AUDIO_NAMESPACE {

gbAudioDevice_OAL::gbAudioDevice_OAL(DeviceIdentifier const& device_id)
    :m_Device(nullptr), m_Context(nullptr), m_HasXRam(false)
{
    m_Device = alcOpenDevice((device_id.Name.empty())?nullptr:(device_id.Name.c_str()));
    if(!m_Device) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to open audio device \"" + device_id.Name + "\"");
    }
    m_GuardDevice = Guard_ALCdevice_T(m_Device, [this](ALCdevice* dev) {
        m_Device = nullptr;
        ALCboolean ret = alcCloseDevice(dev);
        if(!ret) { DIAG_LOG(ERROR, "Unable to close audio device (resource leak?)"); }
    });
    m_Context = alcCreateContext(m_Device, nullptr);
    if(!m_Context) {
        alcCloseDevice(m_Device);
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to create context on audio device \"" + device_id.Name + "\"" );
    }
    m_GuardContext = Guard_ALCcontext_T(m_Context, [this](ALCcontext* ctx) {
        m_Context = nullptr;
        alcMakeContextCurrent(nullptr);
        alcDestroyContext(ctx);
    });
    if(alcMakeContextCurrent(m_Context) != ALC_TRUE) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::InvalidProtocol(),
                              "Unable to activate created context on audio device \"" + device_id.Name + "\"" );
    }
    m_Listener = std::make_shared<gbListener_OAL>();
    //init extensions
    ErrorMonitor_OAL monitor;
    InitXRam();
    InitMultiChannelBuffers();
}

gbAudioDevice_OAL::~gbAudioDevice_OAL()
{
}

template<typename T>
T safe_alGetProcAddress(char const* name)
{
    //casting from void* to function pointer is not allowed in ISO C++,
    //but is guaranteed to work in POSIX; we use memcpy to avoid the compiler
    //warning here
    void* ptr = alGetProcAddress(name);
    static_assert(sizeof(void*) == sizeof(T),
                  "Non-POSIX behavior: Function pointer has different size than pointer-to-object");
    T ret;
    std::memcpy(&ret, &ptr, sizeof(T));
    return ret;
}

void gbAudioDevice_OAL::InitXRam()
{
    if(alIsExtensionPresent("EAX-RAM") != AL_TRUE) {
        m_HasXRam = false;
        return;
    }
    g_EAXSetBufferMode = safe_alGetProcAddress<decltype(g_EAXSetBufferMode)>("EAXSetBufferMode");
    g_EAXGetBufferMode = safe_alGetProcAddress<decltype(g_EAXGetBufferMode)>("EAXGetBufferMode");
    g_AL_EAX_RAM_SIZE       = alGetEnumValue("AL_EAX_RAM_SIZE");
    g_AL_EAX_RAM_FREE       = alGetEnumValue("AL_EAX_RAM_FREE");
    g_AL_STORAGE_AUTOMATIC  = alGetEnumValue("AL_STORAGE_AUTOMATIC");
    g_AL_STORAGE_HARDWARE   = alGetEnumValue("AL_STORAGE_HARDWARE");
    g_AL_STORAGE_ACCESSIBLE = alGetEnumValue("AL_STORAGE_ACCESSIBLE");
    m_HasXRam = true;
}

void gbAudioDevice_OAL::InitMultiChannelBuffers()
{
    g_AL_FORMAT_QUAD16 = alGetEnumValue("AL_FORMAT_QUAD16");
    g_AL_FORMAT_51CHN16 = alGetEnumValue("AL_FORMAT_51CHN16");
    g_AL_FORMAT_61CHN16 = alGetEnumValue("AL_FORMAT_61CHN16");
    g_AL_FORMAT_71CHN16 = alGetEnumValue("AL_FORMAT_71CHN16");
}

std::vector<gbAudioDevice::DeviceIdentifier> gbAudioDevice_OAL::EnumerateDevices_OAL()
{
    if(alcIsExtensionPresent(nullptr, "ALC_ENUMERATION_EXT") != ALC_TRUE) {
        DIAG_THROW_EXCEPTION( Diagnostic::Exceptions::IOError(),
                              "Device enumeration not supported" );
    }
    ALCchar const* device_names = 
        (alcIsExtensionPresent(nullptr, "ALC_ENUMERATE_ALL_EXT") == ALC_TRUE) ?
        alcGetString(nullptr, ALC_ALL_DEVICES_SPECIFIER) :
        alcGetString(nullptr, ALC_DEVICE_SPECIFIER);
    std::vector<DeviceIdentifier> ret;
    for(;;) {
        DeviceIdentifier devid;
        devid.Name = device_names;
        if(devid.Name.empty()) { break; }
        ret.push_back(devid);
        device_names += ret.back().Name.length()+1;
    }
    return ret;
}

std::vector<gbAudioDevice::CHANNEL_FORMAT_TYPE> gbAudioDevice_OAL::GetSupportedChannelFormats() const
{
    std::vector<CHANNEL_FORMAT_TYPE> ret;
    ret.push_back(CHANNEL_FORMAT_MONO);
    ret.push_back(CHANNEL_FORMAT_STEREO);
    if(g_AL_FORMAT_QUAD16) { ret.push_back(CHANNEL_FORMAT_QUAD); }
    if(g_AL_FORMAT_51CHN16) { ret.push_back(CHANNEL_FORMAT_5_1); }
    if(g_AL_FORMAT_61CHN16) { ret.push_back(CHANNEL_FORMAT_6_1); }
    if(g_AL_FORMAT_71CHN16) { ret.push_back(CHANNEL_FORMAT_7_1); }
    return ret;
}

gbSoundBufferPtr gbAudioDevice_OAL::CreateSoundBuffer()
{
    return m_Buffers.CreateResource();
}

gbSourcePtr gbAudioDevice_OAL::CreateSource()
{
    return m_Sources.CreateResource();
}

gbStreamingSourcePtr gbAudioDevice_OAL::CreateStreamingSource()
{
    return m_StreamingSources.CreateResource(&m_StreamingSources);
}

gbListenerPtr gbAudioDevice_OAL::GetListener()
{
    return m_Listener;
}

void gbAudioDevice_OAL::Update()
{
    m_StreamingSources.Apply([](gbStreamingSource& src) {
        src.WindUp();
    });
}

int gbAudioDevice_OAL::OAL_GetXRamSize() const
{
    return (m_HasXRam) ?
        alGetInteger(g_AL_EAX_RAM_SIZE) :
        0;
}

int gbAudioDevice_OAL::OAL_GetXRamFreeSize() const
{
    return (m_HasXRam) ?
        alGetInteger(g_AL_EAX_RAM_FREE) :
        0;
}

}

/******************************************************************************
 * Copyright (c) 2008-2013 Andreas Weis <der_ghulbus@ghulbus-inc.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *****************************************************************************/

#include <gbAudio/sound_file_codec.hpp>
#include <istream>

namespace GB_AUDIO_NAMESPACE {
namespace SoundFileCodec {

RAW::RAW(int sampling_frequency, SoundDataFormat::SOUND_DATA_FORMAT_T format)
    :m_SamplingFrequency(sampling_frequency), m_Format(format)
{
}

void RAW::Decode(std::istream& is, char* buffer, BufferResizeFunction const& buffer_resize_fun, int* sampling_frequency, SoundDataFormat::SOUND_DATA_FORMAT_T* format) const
{
    auto const stream_start = is.tellg();
    is.seekg(0, std::ios_base::end);
    auto const stream_end = is.tellg();
    is.seekg(stream_start);
    auto const stream_size = stream_end - stream_start;

    buffer_resize_fun(&buffer, static_cast<size_t>(stream_size));
    is.read(buffer, stream_size);
    *sampling_frequency = m_SamplingFrequency;
    *format = m_Format;
}

}
}

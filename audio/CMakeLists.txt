
project("gbAudio")

cmake_minimum_required(VERSION 2.8.11)

if(NOT gbLib_VERSION)
  message(FATAL_ERROR "Standalone build not supported for gbLib Audio")
endif()

gbLib_SetVersionNumber(gbLib_AUDIO 0 1 0)

#dependencies
find_package(Boost REQUIRED system thread chrono)
gbLib_getDllFromLib(BOOST_CHRONO_DLLS "${Boost_CHRONO_LIBRARIES}" "")
gbLib_getDllFromLib(BOOST_SYSTEM_DLLS "${Boost_SYSTEM_LIBRARIES}" "")
gbLib_getDllFromLib(BOOST_THREAD_DLLS "${Boost_THREAD_LIBRARIES}" "")

find_package(OpenAL REQUIRED)

# sources
set(gbLib_AUDIO_SRC_DIR "${PROJECT_SOURCE_DIR}/src")
set(gbLib_AUDIO_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/include")

set(gbLib_AUDIO_HEADERS
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/config.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/audio.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/audio_device.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/audio_device_OAL.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/listener.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/listener_OAL.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/locatable.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/playable.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_buffer.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_buffer_OAL.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_data.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_data_fwd.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_file_codec.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_file_codec_RAW.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_file_codec_WAV.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/sound_loader.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/source.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/source_OAL.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/source_base_OAL.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/streaming_source.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/streaming_source_OAL.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/util.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/util_OAL.hpp
  ${gbLib_AUDIO_INCLUDE_DIR}/gbAudio/wind_up_source.hpp
)
source_group(gbAudio FILES ${gbLib_AUDIO_HEADERS})

set(gbLib_AUDIO_SOURCES
  ${gbLib_AUDIO_SRC_DIR}/audio_device.cpp
  ${gbLib_AUDIO_SRC_DIR}/audio_device_OAL.cpp
  ${gbLib_AUDIO_SRC_DIR}/listener_OAL.cpp
  ${gbLib_AUDIO_SRC_DIR}/sound_buffer_OAL.cpp
  ${gbLib_AUDIO_SRC_DIR}/sound_file_codec_RAW.cpp
  ${gbLib_AUDIO_SRC_DIR}/sound_file_codec_WAV.cpp
  ${gbLib_AUDIO_SRC_DIR}/sound_loader.cpp
  ${gbLib_AUDIO_SRC_DIR}/source_OAL.cpp
  ${gbLib_AUDIO_SRC_DIR}/source_base_OAL.cpp
  ${gbLib_AUDIO_SRC_DIR}/streaming_source_OAL.cpp
  ${gbLib_AUDIO_SRC_DIR}/util_OAL.cpp
)

set(gbLib_AUDIO_TEST_SOURCES
  # feed me!
)

gbLib_AddLibrary(gbAudio
  STATIC
  SOURCES ${gbLib_AUDIO_SOURCES}
  HEADERS ${gbLib_AUDIO_HEADERS}
#  TESTS ${gbLib_AUDIO_TEST_SOURCES}
  PUBLIC_INCLUDE_DIRECTORIES ${gbLib_AUDIO_INCLUDE_DIR}
  INCLUDE_DIRECTORIES ${OPENAL_INCLUDE_DIR} ${Boost_INCLUDE_DIRS}
  DEPENDENCIES diagnostic gbUtil gbMath
  LIBRARIES ${Boost_CHRONO_LIBRARIES} ${Boost_SYSTEM_LIBRARIES} ${Boost_THREAD_LIBRARIES} ${OPENAL_LIBRARY}
  DLLS ${BOOST_CHRONO_DLLS} ${BOOST_SYSTEM_DLLS} ${BOOST_THREAD_DLLS}
)

add_executable(gbAudio_Demo ${gbLib_AUDIO_SRC_DIR}/demo.cpp)
gbLib_ResolveDependencies(gbAudio_Demo gbAudio)
